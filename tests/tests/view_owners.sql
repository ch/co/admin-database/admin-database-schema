BEGIN;

CREATE TEMPORARY TABLE views_to_test AS
SELECT table_name, table_schema
FROM information_schema.tables
WHERE table_type = 'VIEW'
    AND (( table_schema NOT IN ('_replication','pg_catalog','information_schema','cache','swcnf','public')) -- exclude schemas with special arrangements
    OR ( table_schema = 'public' AND table_name NOT IN ('tap_funky', 'pg_all_foreign_keys'))); -- exclude the pgtap views from the public schema but include everything else

SELECT plan(count(*)::int) FROM views_to_test;

-- view should be owned by dev
SELECT view_owner_is(table_schema,table_name,'dev','view ' || table_schema || '.' || table_name || ' should be owned by dev')
FROM views_to_test;

SELECT * FROM finish();
