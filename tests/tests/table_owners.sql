BEGIN;

CREATE TEMPORARY TABLE tables_to_test AS
SELECT table_name, table_schema
FROM information_schema.tables
WHERE table_type = 'BASE TABLE'
    AND table_schema NOT IN ('_replication','pg_catalog','information_schema','cache');

SELECT plan(count(*)::int) FROM tables_to_test;

-- tables should be owned by dev
SELECT table_owner_is(table_schema,table_name,'dev','table ' || table_schema || '.' || table_name || ' should be owned by dev')
FROM tables_to_test;

SELECT * FROM finish();
