BEGIN;

CREATE TEMPORARY TABLE tables_to_test AS
SELECT table_name, table_schema
FROM information_schema.tables
WHERE table_type = 'BASE TABLE'
    AND (
            ( table_schema NOT IN ('_replication','pg_catalog','information_schema','cache','swcnf','public')) -- exclude schemas with special arrangements
            OR
            ( table_schema = 'public'
                AND table_name NOT LIKE '_audit%'  -- exclude the audit tables in the public schema
                AND table_name NOT IN ('_physical_status_changelog', 'university_card', 'person_photo', '_leavers_log')
            )
    );


SELECT plan(count(*)::int) FROM tables_to_test;

-- tables should have primary keys
SELECT has_pk(table_schema,table_name,'table ' || table_schema || '.' || table_name || ' does not have primary key')
FROM tables_to_test;

SELECT * FROM finish();
