DROP VIEW hotwire3."10_View/People/SPRI_People";
DROP FUNCTION hotwire3.spri_people_del_trig();

CREATE VIEW hotwire3."10_View/People/SPRI_People" AS
SELECT
    id,
    id as person_id,
    surname,
    first_names,
    title_id,
    email_address,
    crsid,
    array(
        select room_id
        from mm_person_room
        where mm_person_room.person_id = person.id
    ) as room_id,
    remaining_computers.remaining AS ro_number_of_computers_associated,
    case
        when pg_has_role('cos','MEMBER') then hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware','_owner_id','10_View/Computers/Hardware','hardware_id','id')
        else
            hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware','_owner_id',null,null,null)
    end AS computers_owned,
    case
        when pg_has_role('cos','MEMBER') then hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances','_user_id','10_View/Computers/System_Instances','system_image_id','id')
        else
            hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances','_user_id',null,null,null) 
        end AS computers_used
FROM person
LEFT JOIN (
    SELECT person_id, count(id) as remaining
    FROM ( 
        SELECT owner_id AS person_id, id FROM hardware
        UNION ALL
        SELECT user_id AS person_id, id FROM system_image
    ) it_objects
    GROUP BY person_id
) remaining_computers ON remaining_computers.person_id = person.id
WHERE person.is_spri = 't';

ALTER VIEW hotwire3."10_View/People/SPRI_People" OWNER TO dev;

GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/People/SPRI_People" TO spri_hr;

CREATE RULE spri_people_del AS ON DELETE TO hotwire3."10_View/People/SPRI_People" DO INSTEAD DELETE FROM person WHERE id = OLD.id;

CREATE TRIGGER spri_people_trig INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/People/SPRI_People" FOR EACH ROW EXECUTE PROCEDURE hotwire3.spri_people_trig();

