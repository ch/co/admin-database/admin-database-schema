ALTER TABLE fume_hood OWNER TO dev;
GRANT ALL ON fume_hood TO cen1001;

ALTER VIEW _room_fume_hoods OWNER TO dev;
GRANT SELECT ON _room_fume_hoods TO cen1001;

DROP VIEW fume_hoods_view;
DROP VIEW room_occupancy_view;
DROP VIEW rooms_by_research_group_report;
DROP VIEW space_used_by_research_group_report;
