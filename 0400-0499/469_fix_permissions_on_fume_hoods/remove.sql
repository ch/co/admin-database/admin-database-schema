ALTER TABLE fume_hood OWNER TO cen1001;
REVOKE ALL ON fume_hood FROM dev;

ALTER VIEW _room_fume_hoods OWNER TO cen1001;
GRANT SELECT ON _room_fume_hoods TO dev;

CREATE OR REPLACE VIEW fume_hoods_view AS 
 SELECT fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id, fume_hood.test_date, fume_hood.average_face_velocity_reading, fume_hood.test_notes, fume_hood.test_passed
   FROM fume_hood;

ALTER TABLE fume_hoods_view OWNER TO cen1001;
GRANT ALL ON TABLE fume_hoods_view TO cen1001;
GRANT SELECT ON TABLE fume_hoods_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE fume_hoods_view TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE fume_hoods_view TO dev;

CREATE OR REPLACE RULE fume_hood_ins AS
    ON INSERT TO fume_hoods_view DO INSTEAD  INSERT INTO fume_hood (room_id, fume_hood_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, fume_hood_type_id, pdh, notes, research_group_id, test_date, average_face_velocity_reading, test_notes, test_passed) 
  VALUES (new.room_id, new.fume_hood_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.fume_hood_type_id, new.pdh, new.notes, new.research_group_id, new.test_date, new.average_face_velocity_reading, new.test_notes, new.test_passed)
  RETURNING fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id, fume_hood.test_date, fume_hood.average_face_velocity_reading, fume_hood.test_notes, fume_hood.test_passed;

CREATE OR REPLACE RULE fume_hoods_del AS
    ON DELETE TO fume_hoods_view DO INSTEAD  DELETE FROM fume_hood
  WHERE fume_hood.id = old.id;

CREATE OR REPLACE RULE fume_hoods_upd AS
    ON UPDATE TO fume_hoods_view DO INSTEAD  UPDATE fume_hood SET room_id = new.room_id, fume_hood_device_type_id = new.fume_hood_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, fume_hood_type_id = new.fume_hood_type_id, pdh = new.pdh, notes = new.notes, research_group_id = new.research_group_id, test_date = new.test_date, average_face_velocity_reading = new.average_face_velocity_reading, test_notes = new.test_notes, test_passed = new.test_passed
  WHERE fume_hood.id = old.id;


CREATE OR REPLACE VIEW room_occupancy_view AS 
 SELECT room.id, room.name, room.room_type_id, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, _room_current_occupancy.current_occupancy AS ro_current_occupancy, room.number_of_desks, room.maximum_occupancy, _room_fume_hoods.number_of_fumehoods AS ro_number_of_fumehoods, room.area, room.last_refurbished_date, room.comments
   FROM room
   LEFT JOIN research_group ON room.responsible_group_id = research_group.id
   LEFT JOIN _room_current_occupancy ON room.id = _room_current_occupancy.id
   JOIN _room_fume_hoods ON room.id = _room_fume_hoods.id
  ORDER BY room.name;

ALTER TABLE room_occupancy_view
  OWNER TO cen1001;
GRANT ALL ON TABLE room_occupancy_view TO cen1001;
GRANT ALL ON TABLE room_occupancy_view TO dev;
GRANT SELECT ON TABLE room_occupancy_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT ON TABLE room_occupancy_view TO space_management;

-- Rule: room_occupancy_del ON room_occupancy_view

-- DROP RULE room_occupancy_del ON room_occupancy_view;

CREATE OR REPLACE RULE room_occupancy_del AS
    ON DELETE TO room_occupancy_view DO INSTEAD  DELETE FROM room
  WHERE room.id = old.id;

-- Rule: room_occupancy_ins ON room_occupancy_view

-- DROP RULE room_occupancy_ins ON room_occupancy_view;

CREATE OR REPLACE RULE room_occupancy_ins AS
    ON INSERT TO room_occupancy_view DO INSTEAD  INSERT INTO room (name, room_type_id, responsible_group_id, number_of_desks, maximum_occupancy, area, last_refurbished_date, comments) 
  VALUES (new.name, new.room_type_id, new.responsible_group_id, new.number_of_desks, new.maximum_occupancy, new.area, new.last_refurbished_date, new.comments)
  RETURNING room.id, room.name, room.room_type_id, room.responsible_group_id, NULL::bigint AS int8, NULL::bigint AS int8, room.number_of_desks, room.maximum_occupancy, NULL::bigint AS int8, room.area, room.last_refurbished_date, room.comments;

-- Rule: room_occupancy_upd ON room_occupancy_view

-- DROP RULE room_occupancy_upd ON room_occupancy_view;

CREATE OR REPLACE RULE room_occupancy_upd AS
    ON UPDATE TO room_occupancy_view DO INSTEAD  UPDATE room SET name = new.name, room_type_id = new.room_type_id, responsible_group_id = new.responsible_group_id, number_of_desks = new.number_of_desks, maximum_occupancy = new.maximum_occupancy, area = new.area, last_refurbished_date = new.last_refurbished_date, comments = new.comments
  WHERE room.id = old.id;



CREATE OR REPLACE VIEW rooms_by_research_group_report AS 
 SELECT DISTINCT ((((room.id || '-'::text) || research_group.id) || '-'::text) || COALESCE(mm_person_room.person_id, 0::bigint)) || COALESCE(occupant_group.id, 0::bigint) AS id, research_group.id AS responsible_group_id, research_group.head_of_group_id, room.name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods, mm_person_room.person_id, occupant_group.id AS occupant_group_id
   FROM research_group
   LEFT JOIN room ON room.responsible_group_id = research_group.id
   LEFT JOIN _room_current_occupancy ON room.id = _room_current_occupancy.id
   LEFT JOIN _room_fume_hoods ON room.id = _room_fume_hoods.id
   LEFT JOIN mm_person_room ON room.id = mm_person_room.room_id
   LEFT JOIN _physical_status_v2 USING (person_id)
   LEFT JOIN mm_person_research_group USING (person_id)
   LEFT JOIN research_group occupant_group ON mm_person_research_group.research_group_id = occupant_group.id
  WHERE _physical_status_v2.status_id::text = 'Current'::text OR _physical_status_v2.status_id IS NULL
  ORDER BY ((((room.id || '-'::text) || research_group.id) || '-'::text) || COALESCE(mm_person_room.person_id, 0::bigint)) || COALESCE(occupant_group.id, 0::bigint), research_group.id, research_group.head_of_group_id, room.name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods, mm_person_room.person_id, occupant_group.id;

ALTER TABLE rooms_by_research_group_report
  OWNER TO cen1001;
GRANT ALL ON TABLE rooms_by_research_group_report TO cen1001;
GRANT SELECT ON TABLE rooms_by_research_group_report TO space_management;
GRANT SELECT ON TABLE rooms_by_research_group_report TO mgmt_ro;
GRANT SELECT ON TABLE rooms_by_research_group_report TO dev;


CREATE OR REPLACE VIEW space_used_by_research_group_report AS 
 SELECT DISTINCT (((research_group.id || '-'::text) || mm_person_research_group.person_id) || '-'::text) || mm_person_room.room_id AS id, research_group.id AS research_group_id, research_group.head_of_group_id, mm_person_research_group.person_id, room.name AS room_name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods
   FROM research_group
   LEFT JOIN mm_person_research_group ON mm_person_research_group.research_group_id = research_group.id
   JOIN mm_person_room USING (person_id)
   JOIN _physical_status_v2 USING (person_id)
   JOIN room ON mm_person_room.room_id = room.id
   LEFT JOIN _room_current_occupancy ON room.id = _room_current_occupancy.id
   LEFT JOIN _room_fume_hoods ON room.id = _room_fume_hoods.id
  WHERE _physical_status_v2.status_id::text = 'Current'::text OR _physical_status_v2.status_id IS NULL
  ORDER BY (((research_group.id || '-'::text) || mm_person_research_group.person_id) || '-'::text) || mm_person_room.room_id, research_group.id, research_group.head_of_group_id, mm_person_research_group.person_id, room.name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods;

ALTER TABLE space_used_by_research_group_report
  OWNER TO cen1001;
GRANT ALL ON TABLE space_used_by_research_group_report TO cen1001;
GRANT SELECT ON TABLE space_used_by_research_group_report TO space_management;
GRANT SELECT ON TABLE space_used_by_research_group_report TO mgmt_ro;
GRANT SELECT ON TABLE space_used_by_research_group_report TO dev;

