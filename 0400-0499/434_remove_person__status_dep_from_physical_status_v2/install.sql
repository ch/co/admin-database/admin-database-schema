CREATE OR REPLACE VIEW _physical_status_v2 AS 
 SELECT person.id, person.id AS person_id, 
        CASE
            WHEN person.leaving_date < 'now'::text::date THEN 'Past'::character varying(20)
            WHEN person.leaving_date IS NULL AND person.left_but_no_leaving_date_given = true THEN 'Past'::character varying(20)
            WHEN person.arrival_date > person.leaving_date THEN 'Inconsistent'::character varying(20)
            WHEN person.arrival_date <= 'now'::text::date AND (person.leaving_date >= 'now'::text::date OR person.leaving_date IS NULL) THEN 'Current'::character varying(20)
            WHEN person.arrival_date > 'now'::text::date THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS status_id, NULL::varchar(20) AS _status
   FROM person;

ALTER TABLE _physical_status_v2
  OWNER TO dev;
GRANT ALL ON TABLE _physical_status_v2 TO dev;
GRANT SELECT ON TABLE _physical_status_v2 TO web_editors;
GRANT ALL ON TABLE _physical_status_v2 TO cen1001;
GRANT SELECT ON TABLE _physical_status_v2 TO gdpr_purge;
COMMENT ON VIEW _physical_status_v2
  IS 'Obsoleted by _physical_status_v3 table';

