ALTER TABLE public.fume_hood_type_hid OWNER TO dev;
GRANT ALL ON fume_hood_type_hid TO cen1001;

ALTER TABLE fume_hood_device_type_hid OWNER TO dev;
GRANT ALL ON TABLE fume_hood_device_type_hid TO cen1001;

ALTER TABLE firetrace_type_hid OWNER TO dev;
GRANT ALL ON TABLE firetrace_type_hid TO cen1001;

ALTER TABLE recirc_or_ducted_hid OWNER TO dev;
GRANT ALL ON TABLE recirc_or_ducted_hid TO cen1001;
