ALTER TABLE public.fume_hood_type_hid OWNER TO cen1001;
REVOKE ALL ON fume_hood_type_hid FROM dev;

ALTER TABLE fume_hood_device_type_hid OWNER TO cen1001;
REVOKE ALL ON TABLE fume_hood_device_type_hid FROM dev;

ALTER TABLE firetrace_type_hid OWNER TO cen1001;
REVOKE ALL ON TABLE firetrace_type_hid FROM dev;

ALTER TABLE recirc_or_ducted_hid OWNER TO cen1001;
REVOKE ALL ON TABLE recirc_or_ducted_hid FROM dev;
