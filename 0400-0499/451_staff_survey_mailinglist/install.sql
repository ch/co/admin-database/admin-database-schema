CREATE VIEW _chem_staff_survey AS
SELECT person.email_address, person.crsid, person_hid, le.status
FROM _latest_employment le
JOIN person ON le.person_id = person.id
JOIN _physical_status_v3 ps using (person_id)
JOIN person_hid using (person_id)
WHERE 
  le.paid_by_university ='t'
AND
  le.status = 'Current'
AND
  ps.status_id = 'Current'
;

ALTER VIEW _chem_staff_survey OWNER TO dev;
GRANT SELECT ON _chem_staff_survey TO mailinglists, www_sites, leavers_trigger;
