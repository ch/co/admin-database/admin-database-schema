ALTER TABLE cable_type_hid OWNER TO dev;
GRANT ALL ON cable_type_hid TO cen1001;

ALTER TABLE connector_type_hid OWNER TO dev;
GRANT ALL ON connector_type_hid TO cen1001;

ALTER TABLE switchport OWNER TO dev;
GRANT ALL ON switchport TO cen1001;
