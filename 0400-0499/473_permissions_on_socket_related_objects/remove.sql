ALTER TABLE cable_type_hid OWNER TO cen1001;
REVOKE ALL ON cable_type_hid FROM dev;

ALTER TABLE connector_type_hid OWNER TO cen1001;
REVOKE ALL ON connector_type_hid FROM dev;

ALTER TABLE switchport OWNER TO cen1001;
GRANT ALL ON switchport TO dev;
