CREATE VIEW hotwire3."30_Report/Inconsistent_Status" AS 
SELECT 
  _physical_status.person_id AS id, 
  _physical_status.person_id, 
  _latest_role.post_category_id, 
  _physical_status.status_id AS physical_status, 
  person.arrival_date AS physical_arrival_date, 
  person.leaving_date AS physical_leaving_date, 
  _latest_role.status AS newest_role_status, 
  _latest_role.start_date AS newest_role_start_date, 
  _latest_role.intended_end_date AS newest_role_intended_end_date, 
  _latest_role.end_date AS newest_role_end_date 
FROM 
  _physical_status_v3 _physical_status 
  LEFT JOIN _latest_role_v12 _latest_role USING (person_id) 
  LEFT JOIN person ON person.id = _physical_status.person_id 
WHERE 
  _physical_status.status_id :: text <> _latest_role.status :: text;
ALTER TABLE hotwire3."30_Report/Inconsistent_Status" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."30_Report/Inconsistent_Status" TO dev;
GRANT SELECT ON TABLE hotwire3."30_Report/Inconsistent_Status" TO hr;
GRANT SELECT ON TABLE hotwire3."30_Report/Inconsistent_Status" TO student_management;
GRANT SELECT ON TABLE hotwire3."30_Report/Inconsistent_Status" TO mgmt_ro;

