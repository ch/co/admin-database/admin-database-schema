-- Installing 001_add_hotwire3_schema

CREATE SCHEMA hotwire3;
ALTER SCHEMA hotwire3 OWNER to dev;
GRANT USAGE on SCHEMA hotwire3 to PUBLIC;
GRANT USAGE on SCHEMA hotwire3 to _pgbackup;
-- Installing 002_add_hw3_preferences_tables

CREATE TABLE hotwire3.hw_preference_type_hid (
    hw_preference_type_id bigserial NOT NULL,
    hw_preference_type_hid character varying
);
ALTER TABLE hotwire3.hw_preference_type_hid OWNER TO dev;

INSERT INTO hotwire3.hw_preference_type_hid VALUES (1, 'boolean');
INSERT INTO hotwire3.hw_preference_type_hid VALUES (2, 'integer');
INSERT INTO hotwire3.hw_preference_type_hid VALUES (3, 'string');

ALTER TABLE ONLY hotwire3.hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);

GRANT ALL ON TABLE hotwire3.hw_preference_type_hid TO postgres;
GRANT ALL ON TABLE hotwire3.hw_preference_type_hid TO dev;

SELECT setval('hotwire3.hw_preference_type_hid_hw_preference_type_id_seq',3);

CREATE TABLE hotwire3."hw_Preferences"
( 
  id serial NOT NULL ,
  hw_preference_name character varying,
  hw_preference_type_id integer,
  hw_preference_const varchar,
  CONSTRAINT "Preferences_pkey" PRIMARY KEY (id),
  CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id)
      REFERENCES hotwire3.hw_preference_type_hid (hw_preference_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

)
WITH (
  OIDS=FALSE
);
ALTER TABLE hotwire3."hw_Preferences" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."hw_Preferences" TO postgres;
GRANT ALL ON TABLE hotwire3."hw_Preferences" TO dev;

INSERT INTO hotwire3."hw_Preferences" VALUES (1, 'Floating table headers', 1, 'FLOATHEAD');
INSERT INTO hotwire3."hw_Preferences" VALUES (2, 'Jump on a single search result', 1, 'JUMPSEARCH');
INSERT INTO hotwire3."hw_Preferences" VALUES (3, 'Jump on a single view result', 1, 'JUMPVIEW');
INSERT INTO hotwire3."hw_Preferences" VALUES (4, 'CSS path', 3, 'CSSPATH');
INSERT INTO hotwire3."hw_Preferences" VALUES (5, 'Warn if record data changes', 1, 'CHANGE');
INSERT INTO hotwire3."hw_Preferences" VALUES (6, 'AJAX min dd size', 2, 'AJAXMIN');
INSERT INTO hotwire3."hw_Preferences" VALUES (7, 'JS path', 3, 'JSPATH');
INSERT INTO hotwire3."hw_Preferences" VALUES (9, 'Default view', 3, 'DEFVIEW');
INSERT INTO hotwire3."hw_Preferences" VALUES (10, 'Warn if view data changes', 1, 'CHECKSUM');
INSERT INTO hotwire3."hw_Preferences" VALUES (11, 'Alternative views enabled', 1, 'ALTVIEW');
INSERT INTO hotwire3."hw_Preferences" VALUES (12, 'Related Records enabled', 1, 'RELREC');
INSERT INTO hotwire3."hw_Preferences" VALUES (13, 'Resizable Table columns', 1, 'RESIZECOLS');
INSERT INTO hotwire3."hw_Preferences" VALUES (14, 'Sort NULLS in numerical order', 1, 'NUMNULL');
INSERT INTO hotwire3."hw_Preferences" VALUES (15, 'Hideable Table columns', 1, 'HIDECOLS');
INSERT INTO hotwire3."hw_Preferences" VALUES (16, 'Reorderable Table columns', 1, 'COLREORDER');
INSERT INTO hotwire3."hw_Preferences" VALUES (17, 'Show old search', 1, 'OLDSEARCH');
INSERT INTO hotwire3."hw_Preferences" VALUES (18, 'Include "... show next" buttons', 1, 'SHOWNEXT');
INSERT INTO hotwire3."hw_Preferences" VALUES (19, 'Warn on missing rules', 1, 'WARNRULES');
INSERT INTO hotwire3."hw_Preferences" VALUES (20, 'Cache menu structure', 1, 'CACHEMENU');
INSERT INTO hotwire3."hw_Preferences" VALUES (21, 'Remove accents from search', 1, 'UNACCENT');
INSERT INTO hotwire3."hw_Preferences" VALUES (22, 'Date format', 3, 'DATEFORMAT');
INSERT INTO hotwire3."hw_Preferences" VALUES (23, 'Show web interface hints', 1, 'SHOWHINTS');
INSERT INTO hotwire3."hw_Preferences" VALUES (24, 'Show post-update options', 1, 'POSTUPDATEOPT');
INSERT INTO hotwire3."hw_Preferences" VALUES (25, 'Show post-insert options', 1, 'POSTINSERTOPT');
INSERT INTO hotwire3."hw_Preferences" VALUES (26, 'Offer xdebug option to developers', 1, 'XDEBUG');
INSERT INTO hotwire3."hw_Preferences" VALUES (27, 'DB Development group', 3, 'DBDEV');
INSERT INTO hotwire3."hw_Preferences" VALUES (28, 'Database internal date format', 3, 'DBDATEFORMAT');
INSERT INTO hotwire3."hw_Preferences" VALUES (29, 'Use Ajax many-many selects', 1, 'MM_UI_AJAX');
INSERT INTO hotwire3."hw_Preferences" VALUES (30, 'Use many-many tag interface', 1, 'MM_UI_TAG');
INSERT INTO hotwire3."hw_Preferences" VALUES (31, 'No JS on many-many selects', 1, 'MM_UI_NOJS');
INSERT INTO hotwire3."hw_Preferences" VALUES (32, 'First day of the week', 2, 'JS_UI_DAYSTART');

SELECT setval('hotwire3."hw_Preferences_id_seq"',(select max(id) from hotwire3."hw_Preferences"));

CREATE TABLE hotwire3."hw_User Preferences" (
    id serial NOT NULL,
    preference_id integer not null,
    preference_value character varying not null,
    user_id bigint,
    CONSTRAINT "User_Preferences_pkey" PRIMARY KEY (id),
    CONSTRAINT "User_Preferences_fk_preference_id" FOREIGN KEY (preference_id) REFERENCES hotwire3."hw_Preferences"(id) ON DELETE CASCADE
);
ALTER TABLE hotwire3."hw_User Preferences" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."hw_User Preferences" TO postgres;
GRANT ALL ON TABLE hotwire3."hw_User Preferences" TO dev;

INSERT INTO hotwire3."hw_User Preferences" ( preference_id, preference_value, user_id ) SELECT preference_id, preference_value, user_id FROM hotwire."hw_User Preferences" ;

-- these two don't exist in the the old hotwire schema as they're for a much, much newer version
INSERT INTO hotwire3."hw_User Preferences" ( preference_id, preference_value, user_id ) VALUES ( 29,'TRUE',NULL );
INSERT INTO hotwire3."hw_User Preferences" ( preference_id, preference_value, user_id ) VALUES ( 30,'TRUE',NULL );

SELECT setval('hotwire3."hw_User Preferences_id_seq"',(select max(id) from hotwire3."hw_User Preferences"));
-- Installing 003_add_hw3_view_data

CREATE TABLE hotwire3._primary_table (
	view_name varchar primary key,
	primary_table varchar);

ALTER TABLE hotwire3._primary_table OWNER TO dev;
GRANT SELECT ON hotwire3._primary_table TO PUBLIC;


CREATE VIEW hotwire3._view_data AS 
 SELECT 
	pgns.nspname AS schema, 
	pg_class.relname AS view, 
	pg_class.relacl AS perms, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'insert'::text) AS insert, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'update'::text) AS update, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'delete'::text) AS delete, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'select'::text) AS "select", 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '4'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS delete_rule, 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS insert_rule, 
	( 
		SELECT r.ev_action::text ~~ '%returningList:%'::text
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	) AS insert_returning, 
	(( SELECT count(*) AS count
           FROM pg_rewrite r
      JOIN pg_class c ON c.oid = r.ev_class
   LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE r.ev_type = '2'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace)) > 0 AS update_rule, pg_views.definition, 
	_primary_table.primary_table, 
        CASE
            WHEN regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ '%ORDER BY%'::character varying::text THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text)
            ELSE NULL::text
        END AS order_by
   FROM pg_class
   JOIN pg_namespace pgns ON pg_class.relnamespace = pgns.oid
   JOIN pg_views ON pg_class.relname = pg_views.viewname AND pgns.nspname = pg_views.schemaname
   LEFT JOIN hotwire3._primary_table on pg_class.relname = _primary_table.view_name
  WHERE pg_class.relname ~~ '%/%'::text AND (pgns.nspname = ANY (ARRAY['hotwire3'::name, 'public'::name]));


ALTER TABLE hotwire3._view_data OWNER TO dev;
GRANT ALL ON TABLE hotwire3._view_data TO dev;
GRANT SELECT ON TABLE hotwire3._view_data TO public;

-- Installing 004_add_hw3_role_data

-- What permissions/roles do we have?
CREATE OR REPLACE VIEW hotwire3._role_data AS 
 SELECT member.rolname AS member, role.rolname AS role
   FROM pg_roles role
   LEFT JOIN pg_auth_members ON role.oid = pg_auth_members.roleid
   RIGHT JOIN pg_roles member ON member.oid = pg_auth_members.member;

ALTER TABLE hotwire3._role_data OWNER TO dev;
GRANT ALL ON TABLE hotwire3._role_data TO dev;
GRANT SELECT ON TABLE hotwire3._role_data TO public;
-- Installing 005_add_hw3_column_data

CREATE VIEW hotwire3._column_data AS 
 SELECT pg_attribute.attname, pg_type.typname, pg_attribute.atttypmod, pg_class.relname, pg_attribute.attnotnull, pg_attribute.atthasdef, pg_type.typelem > 0::oid AS "array", ( SELECT child.typname
           FROM pg_type child
          WHERE child.oid = pg_type.typelem) AS elementtype
   FROM pg_attribute
   JOIN pg_type ON pg_attribute.atttypid = pg_type.oid
   JOIN pg_class ON pg_attribute.attrelid = pg_class.oid
   JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
  WHERE (pg_namespace.nspname = 'public'::name OR pg_namespace.nspname = 'hotwire3'::name) AND pg_attribute.attnum > 0;

ALTER TABLE hotwire3._column_data OWNER TO dev;
GRANT ALL ON TABLE hotwire3._column_data TO dev;
GRANT SELECT ON TABLE hotwire3._column_data TO public;
-- Installing 006_add_hw3_preferences_views

CREATE OR REPLACE VIEW hotwire3."90_Action/Hotwire/All Possible Preferences" AS 
 SELECT "hw_Preferences".id, "hw_Preferences".hw_preference_name AS preference_name, "hw_Preferences".hw_preference_type_id
   FROM hotwire3."hw_Preferences";
ALTER TABLE hotwire3."90_Action/Hotwire/All Possible Preferences" OWNER TO dev;

CREATE OR REPLACE RULE hw3_super_admin_preferences_del AS
    ON DELETE TO hotwire3."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  DELETE FROM hotwire3."hw_Preferences"
  WHERE "hw_Preferences".id = old.id;

CREATE OR REPLACE RULE hw3_super_admin_preferences_ins AS
    ON INSERT TO hotwire3."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  INSERT INTO hotwire3."hw_Preferences" (hw_preference_name, hw_preference_type_id) 
  VALUES (new.preference_name, new.hw_preference_type_id) RETURNING "hw_Preferences".id, "hw_Preferences".hw_preference_name, "hw_Preferences".hw_preference_type_id;

CREATE OR REPLACE RULE hw3_super_admin_preferences_upd AS
    ON UPDATE TO hotwire3."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  UPDATE hotwire3."hw_Preferences" SET hw_preference_name = new.preference_name, hw_preference_type_id = new.hw_preference_type_id
  WHERE "hw_Preferences".id = old.id;


CREATE OR REPLACE VIEW hotwire3."90_Action/Hotwire/User Preferences" AS 
 SELECT "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id as "Postgres_User_id"
   FROM hotwire3."hw_User Preferences";

ALTER TABLE hotwire3."90_Action/Hotwire/User Preferences" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."90_Action/Hotwire/User Preferences" TO dev;

CREATE OR REPLACE RULE hw3_user_preferences_del AS
    ON DELETE TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  DELETE FROM hotwire3."hw_User Preferences"
  WHERE "hw_User Preferences".id = old.id;

CREATE OR REPLACE RULE hw3_user_preferences_ins AS
    ON INSERT TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  INSERT INTO hotwire3."hw_User Preferences" (preference_id, preference_value, user_id) 
  VALUES (new.preference_id, new.preference_value, new."Postgres_User_id") RETURNING "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id;

CREATE OR REPLACE RULE hw3_user_preferences_upd AS
    ON UPDATE TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  UPDATE hotwire3."hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new."Postgres_User_id"
  WHERE "hw_User Preferences".id = old.id;


CREATE OR REPLACE VIEW hotwire3.preference_hid AS 
 SELECT "Preferences".id AS preference_id, "Preferences".hw_preference_name AS preference_hid
   FROM hotwire3."hw_Preferences" "Preferences";

ALTER TABLE hotwire3.preference_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.preference_hid TO dev;

CREATE OR REPLACE VIEW hotwire3."Postgres_User_hid" AS
 SELECT pg_roles.oid::bigint AS "Postgres_User_id", pg_roles.rolname AS "Postgres_User_hid"
   FROM pg_roles;

ALTER TABLE hotwire3."Postgres_User_hid" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."Postgres_User_hid" TO dev;

CREATE VIEW hotwire3._preferences AS 
 SELECT "Preferences".hw_preference_const AS preference_name, a.preference_value, hw_preference_type_hid.hw_preference_type_hid AS preference_type
   FROM (        (         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM hotwire3."hw_User Preferences" "User Preferences"
                          WHERE "User Preferences".user_id IS NULL
                UNION ALL 
                         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM pg_auth_members m
                      JOIN pg_roles b ON m.roleid = b.oid
                 JOIN pg_roles r ON m.member = r.oid
            JOIN hotwire3."hw_User Preferences" "User Preferences" ON b.oid = "User Preferences".user_id::oid
           WHERE r.rolname = "current_user"())
        UNION ALL 
                 SELECT "User Preferences".preference_id, "User Preferences".preference_value
                   FROM pg_roles
              JOIN hotwire3."hw_User Preferences" "User Preferences" ON pg_roles.oid = "User Preferences".user_id::oid
             WHERE pg_roles.rolname = "current_user"()) a
   JOIN hotwire3."hw_Preferences" "Preferences" ON a.preference_id = "Preferences".id
   JOIN hotwire3.hw_preference_type_hid USING (hw_preference_type_id);

ALTER TABLE hotwire3._preferences OWNER TO dev;
GRANT ALL ON TABLE hotwire3._preferences TO dev;
GRANT SELECT ON TABLE hotwire3._preferences TO public;

-- Installing 007_hw3_suggest_view


CREATE OR REPLACE FUNCTION hotwire3.suggest_view(
	itablename character varying,
	ischemaname character varying,
	omenuname character varying,
	oviewname character varying)
RETURNS SETOF character varying 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
  hw_schema varchar:='hotwire3'; -- name of schema
  colcount bigint;
  keycount bigint;
  colinf record;
  query text;
  pkeyfield varchar;
  fullname varchar;
  colalias varchar;
  -- Type of table/view
  qtype varchar;
  updrule text;
  delrule text;
  insrule text;
  insruleb text:=') values (';
  insrulec text:=') RETURNING ';
  hidtable varchar;
  viewname varchar:=coalesce(oviewname,itablename);
  menuname varchar:=coalesce(omenuname,'Menu');
  testschema varchar;
  istable boolean;
  roid integer;
  rtable varchar;
  rfieldnum integer;
  s_name varchar;
  t_name varchar;
  c_name varchar;
  o_column varchar; 
  qfields varchar[]; -- Fields to go into the query
  qfields_late varchar[]; -- Fields to go at the /end/ of the query
  upd_rule_fields varchar[]; -- To be handled in the main part of the update rule
  upd_rule_mm varchar[]; -- To be handled in the later part of the update rule
  seen_fields varchar[]; -- Keep track of fields which have to be automatically aliased because we have two of them. Somehow.
  field_count integer:=0; -- A unique integer for each field. Meh.
  ins_rule_fields varchar[]; -- To handle the fields in the main part of the trigger function.
  ins_rule_values varchar[]; -- the 'new.X' values to be inserted
  ins_trig_sql varchar[]; -- A list of SQL to go into the insert trigger function.
begin
 return next '-- [II] Using suggest_view2 '::varchar; 
 -- Is this a table or a view?
 select count(*) from pg_catalog.pg_tables 
  where pg_tables.schemaname=ischemaname 
    and pg_tables.tablename=  itablename 
   into colcount;
 istable=(colcount=1);
 if (istable) THEN
  qtype='table';
 else
  qtype='view';
 END IF;

 return next '-- [II]  Hotwire suggesting a view definition for the '||qtype||' "'||itablename||'" ';
 return next '-- [II]  in the "'||ischemaname||'" schema'::varchar;
 -- Check that this table exists
 select count(*) from information_schema.columns 
  where table_name=itablename 
    and table_schema=ischemaname 
   into colcount;
 if (colcount<1) THEN
  return next '-- [EE]  Hotwire was unable to find a definition for that '||qtype||'. Sorry.';
  raise notice ' Hotwire was unable to find a definition for that %', qtype ;
  return;
  EXIT;
 END IF;
 return next '-- [II]  Found '||colcount||' fields in that '||qtype;

-- Check that this table has a primary key
if (istable) THEN
  SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.constraint_schema = ischemaname
                   AND tc.table_name=itablename into pkeyfield;
  if (pkeyfield IS NULL) THEN
   return next '-- [EE]  Hotwire was unable to find a primary key for that '||qtype||'. Perhaps you need to add one?';
   return;
   EXIT;
  else
   return next '-- [II] Primary key on this '||qtype||' is '||pkeyfield;
  END IF;
ELSE
  -- How do we find a primary key for a view?
  return next '-- [II]  Attempting to identify primary key on the view '||itablename;
  -- Find the _RETURN rule for this view
  select distinct objid from pg_depend 
                  inner join pg_rewrite on pg_depend.objid=pg_rewrite.oid 
                   where refobjid=itablename::regclass 
                         and classid='pg_rewrite'::regclass  
                         and rulename='_RETURN'into roid;
  select count(*) from (select distinct refobjid::regclass 
                                   from pg_depend 
                                  where objid=roid 
                                    and deptype='n' 
                                    and refobjsubid!=0) a into colcount;
  if (colcount>1) THEN
    return next '-- [EE]  Sorry, hotwire3 cannot (yet) suggest definitions for views coming from more';
    return next '--       than one table.';
    return;
    exit;
  else 

    select distinct refobjid::regclass from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 limit 1 into rtable;
    
    return next '-- [II]  Good, this view uses a single table ('||rtable||')';
    SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=rtable into pkeyfield;
    return next '--       Checking for presence of that table''s primary key ('||pkeyfield||')';
    return next '         in the columns of the view';
    -- Find column number
    select attnum from pg_attribute where attrelid='person'::regclass and attname='id' into rfieldnum;
    -- Is column number in view?
    select count(*) from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 and refobjsubid=rfieldnum into colcount;
    if (colcount=1) then
      return next '-- [II]  Good, the primary key ('||pkeyfield||') on that table appears in the view.';
    else
      return next '-- [EE]  Oh dear, the primary key ('||pkeyfield||') doesn''t appear in the view. ';
      return next '--       Try adding it to the view and repeating this operation...';
      return;
      exit;
    end if;
  END IF;
--   execute select distinct objid from pg_depend inner join pg_rewrite on 
-- pg_depend.objid=pg_rewrite.oid where refobjid='tview'::regclass 
-- and classid='pg_rewrite'::regclass  and rulename='_RETURN';
END IF;
fullname=menuname||'/'||viewname;

query='CREATE VIEW hotwire3."'||fullname||'" AS SELECT ';
updrule='CREATE RULE '||quote_ident(hw_schema||'_'||fullname||'_upd')||' AS '||
        'ON UPDATE TO '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' DO INSTEAD ( '||
        'UPDATE '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||' SET ';
delrule='CREATE RULE '||quote_ident(hw_schema||'_'||fullname||'_del')||' AS ON DELETE TO hotwire3."'||fullname||'" DO INSTEAD '||
        'DELETE FROM "'||ischemaname||'"."'||itablename||'" WHERE "'||pkeyfield||'" = old.id;';
insrule='CREATE RULE "hotwire3_'||fullname||'_ins" AS ON INSERT TO hotwire3."'||fullname||'" DO INSTEAD '||
        'INSERT INTO "'||ischemaname||'"."'||itablename||'" (';

-- Iterate over all fields in the table
for colinf in (select * from information_schema.columns where table_name=itablename and table_schema=ischemaname) LOOP
 colalias=NULL;
 -- return next '-- Considering field '||colinf.column_name;
  -- Only tables have foreign keys. (Well, views sort of do but...)
  IF (istable) THEN
    -- Is this a foreign key? 
    SELECT count(*) FROM information_schema.table_constraints tc 
                      LEFT JOIN information_schema.key_column_usage kcu 
                        ON tc.constraint_catalog = kcu.constraint_catalog 
                          AND tc.constraint_schema = kcu.constraint_schema 
                          AND tc.constraint_name = kcu.constraint_name 
                      LEFT JOIN information_schema.referential_constraints rc 
                        ON tc.constraint_catalog = rc.constraint_catalog 
                          AND tc.constraint_schema = rc.constraint_schema 
                          AND tc.constraint_name = rc.constraint_name 
                      LEFT JOIN information_schema.constraint_column_usage ccu 
                        ON rc.unique_constraint_catalog = ccu.constraint_catalog 
                          AND rc.unique_constraint_schema = ccu.constraint_schema 
                          AND rc.unique_constraint_name = ccu.constraint_name
                  WHERE lower(tc.constraint_type) in ('foreign key') 
                    AND kcu.column_name=colinf.column_name
                    AND tc.table_name=itablename into keycount;
    IF (keycount>0) THEN
      if ((length(colinf.column_name)>3) AND 
          (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)!='_id')) then
        colalias=colinf.column_name||'_id';
      end if;
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire3';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
        into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire3 or public.
        return next '-- [EE]  No HID table or view hotwire3.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire3 to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire3."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire3 schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema and column_name=hidtable 
          into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire3 schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF; -- Is this a foreign key?  
  ELSE
    if (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)='_id') THEN
      return next '-- [II]  The column '||colinf.column_name||' will be treated as a foreign key';
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire3';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire3 or public.
        return next '-- [EE]  No HID table or view hotwire3.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire3 to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire3."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire3 schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=hidtable into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire3 schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF;
  END IF;
 -- Is this the primary key?
 if (colinf.column_name=ANY(seen_fields)) THEN
  colalias=colinf.column_name||'_'||field_count;
  seen_fields=array_append(seen_fields,colalias);
 else
  seen_fields=array_append(seen_fields,colinf.column_name::varchar);
 end if;
 if (colinf.column_name=pkeyfield) THEN
  if (pkeyfield!='id') THEN
   colalias='id';
  END IF;
 ELSE
  -- We don't insert to the primary key of a table.
  ins_rule_fields=array_append(ins_rule_fields,quote_ident(colinf.column_name)::varchar);
  ins_rule_values=array_append(ins_rule_values,('new.'||quote_ident(coalesce(colalias,colinf.column_name)))::varchar);
  insrule=insrule||' "'||colinf.column_name||'", ';
  insruleb=insruleb||' new."'||coalesce(colalias, colinf.column_name)||'", ';
 END IF;
 insrulec=insrulec||' "'||itablename||'"."'||colinf.column_name||'", ';
 -- DEL ME query=query||' "'||colinf.column_name||'"'||coalesce(' AS "'||colalias||'"','')||',';
 qfields=array_append(qfields,(quote_ident(colinf.column_name)||coalesce(' AS '||quote_ident(colalias),''))::varchar);
 -- Let's not update the primary key
 if (colinf.column_name!=pkeyfield) then
  upd_rule_fields=array_append(upd_rule_fields,(quote_ident(colinf.column_name)||'=new.'||quote_ident(coalesce(colalias,colinf.column_name)))::varchar);
 end if;
 -- updrule=updrule||'"'||colinf.column_name||'"=new."'||coalesce(colalias,colinf.column_name)||'", ';
 field_count=field_count+1;
END LOOP;
-- Our first part of the insert trigger is built from the field names:
ins_trig_sql=array_append(ins_trig_sql,(
 'INSERT INTO '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||' ('||array_to_string(ins_rule_fields,',')||') '||
 'VALUES ('||array_to_string(ins_rule_values,',')||') RETURNING '||quote_ident(pkeyfield)||' into _new_id')::varchar);
-- Look for foreign keys into this table.
for s_name,t_name,c_name in (SELECT
    tc.table_schema,  tc.table_name, kcu.column_name 
FROM 
    information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_schema=ischemaname and ccu.table_name=itablename and ccu.column_name=pkeyfield) loop
 colalias=null;
 return next '-- [II] '||t_name||'.'||c_name||' is a key'; 
 -- Is this table more than an mm_ table (i.e. has > two columns?
 if (select count(*) from information_schema.columns where table_schema=s_name and table_name=t_name)>2 then
  return next '-- [II] '||t_name||' is a record';
  -- Create a dummy hw_subviewc
  qfields_late=array_append(qfields_late,($$hotwire3._to_hwsubviewb('A_hotwire3_summary_view_on_$$||t_name||$$','$$||itablename||$$_id','A_hotwire3_detailed_view_on_$$||t_name||$$',NULL,NULL) as ro_$$||t_name)::varchar);
 else
  -- the /other/ field is:
  return next '-- [II] '||t_name||' is an MM table';
  select column_name from information_schema.columns where table_schema=s_name and table_name=t_name and column_name !=c_name into o_column;
  if (o_column=ANY(seen_fields)) THEN
   colalias=o_column||'_'||field_count;
  end if;
  seen_fields=array_append(seen_fields,coalesce(colalias,o_column));
  return next '-- [II] '||o_column||' is the other column';
  qfields=array_append(qfields,('array(select '||quote_ident(o_column)||' from '||quote_ident(s_name)||'.'||quote_ident(t_name)||' where '||quote_ident(c_name)||'='||quote_ident(itablename)||'.id) as '||quote_ident(coalesce(colalias,o_column)))::varchar);
  -- Create an update rule
  --upd_rule_mm=array_append(upd_rule_mm,(
-- 'DELETE FROM '||quote_ident(t_name)||' WHERE '||quote_ident(c_name)||' = old.id and NOT ('||quote_ident(o_column)||' = ANY (COALESCE(new.'||quote_ident(coalesce(colalias,o_column))||$$,'{}'$$||'::integer[])))')::varchar);
  --upd_rule_mm=array_append(upd_rule_mm,(
-- $$INSERT INTO $$||quote_ident(t_name)||$$ ($$||quote_ident(c_name)||$$, $$||quote_ident(o_column)||$$) SELECT old.id, dummy.col FROM ( SELECT unnest(new.$$||quote_ident(coalesce(colalias,o_column))||$$) AS col) dummy WHERE NOT (dummy.col=ANY(COALESCE(old.$$||quote_ident(coalesce(colalias,o_column))||$$, '{}'::integer[])))$$)::varchar);
  -- Create an update rule
   upd_rule_mm=array_append(upd_rule_mm,
    ('select hotwire3.fn_mm_set('||quote_literal(t_name)||','||quote_literal(c_name)||','||quote_literal(o_column)||',old.id,new.'||quote_ident(coalesce(colalias,o_column))||',old.'||quote_ident(coalesce(colalias,o_column))||')')::varchar);
  -- Create some SQL to handle inserts
  ins_trig_sql=array_append(ins_trig_sql,('INSERT INTO '||quote_ident(t_name)||' ('||quote_ident(c_name)||', '||quote_ident(o_column)||') SELECT _new_id, dummy.col FROM (SELECT unnest(new.'||quote_ident(coalesce(colalias,o_column))||') as col) dummy'
)::varchar);
 end if;
 field_count=field_count+1;
end loop;

updrule=updrule||array_to_string(upd_rule_fields,',')||' WHERE "'||itablename||'"."'||pkeyfield||'"=old.id;'||array_to_string(upd_rule_mm,';')||';);';
insrule=rtrim(insrule,', ')||rtrim(insruleb,', ')||rtrim(insrulec,', ')||';';
query='CREATE VIEW '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' AS SELECT '||array_to_string(qfields||qfields_late,',')||' FROM '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||';';
return next '-- Hotwire suggests the following query definition:';
return next 'begin;';
return next query::varchar;
return next '-- Rules:';
return next updrule::varchar;
return next delrule::varchar;
-- return next insrule::varchar;
return next 'ALTER TABLE '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' OWNER TO postgres;';

return next 'CREATE OR REPLACE FUNCTION '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'() RETURNS trigger as $RULE$ DECLARE _new_id integer; BEGIN ' || array_to_string(ins_trig_sql,';')||' ;return null;END;$RULE$ LANGUAGE plpgsql VOLATILE COST 100;';
return next 'ALTER FUNCTION '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'() OWNER TO postgres;';
return next 'CREATE TRIGGER '||quote_ident(hw_schema||'_'||fullname||'_ins_trig')||' INSTEAD OF INSERT ON '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' FOR EACH ROW EXECUTE PROCEDURE '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'();';
return next 'commit;';
end

$BODY$;

ALTER FUNCTION hotwire3.suggest_view(character varying, character varying, character varying, character varying)
    OWNER TO dev;

COMMENT ON FUNCTION hotwire3.suggest_view(character varying, character varying, character varying, character varying)
    IS 'To suggest a hotwire view from a table''s definition';

-- Installing 008_add_hw3_hid_views

-- hardware
CREATE VIEW hotwire3.hardware_hid AS
	SELECT 
		hardware.id AS hardware_id, 
		COALESCE(hardware.name::text || ''::text) || COALESCE(((' ('::text || hardware.model::text) || ')'::text) || ''::text) AS hardware_hid
   	FROM hardware
  	ORDER BY COALESCE(hardware.name::text || ''::text) || COALESCE(((' ('::text || hardware.model::text) || ')'::text) || ''::text);

ALTER TABLE hotwire3.hardware_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.hardware_hid TO dev;
GRANT SELECT ON TABLE hotwire3.hardware_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.hardware_hid TO groupitreps,headsofgroup;

-- hardware_type
CREATE VIEW hotwire3.hardware_type_hid AS 
	SELECT 
		hardware_type.id AS hardware_type_id, 
		hardware_type.name AS hardware_type_hid
   	FROM hardware_type;

ALTER TABLE hotwire3.hardware_type_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.hardware_type_hid TO dev;
GRANT SELECT ON TABLE hotwire3.hardware_type_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.hardware_type_hid TO groupitreps,headsofgroup;

-- operating_system
CREATE VIEW hotwire3.operating_system_hid AS 
	SELECT
		operating_system.id AS operating_system_id,
		operating_system.os AS operating_system_hid
   	FROM operating_system
  	ORDER BY operating_system.os;

ALTER TABLE hotwire3.operating_system_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.operating_system_hid TO dev;
GRANT SELECT ON TABLE hotwire3.operating_system_hid TO PUBLIC;

-- title
CREATE VIEW hotwire3.title_hid AS 
	SELECT
		title_hid.title_id,
		title_hid.title_hid
   FROM title_hid;

ALTER TABLE hotwire3.title_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.title_hid TO dev;
GRANT SELECT ON TABLE hotwire3.title_hid TO public;

-- person
CREATE VIEW hotwire3.person_hid AS 
	SELECT 
		person.id AS person_id, 
		((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS person_hid
	FROM person
	LEFT JOIN public.title_hid USING (title_id)
	ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.title_hid::text || ' '::text, ''::text);

ALTER TABLE hotwire3.person_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.person_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.person_hid TO groupitreps,headsofgroup;
GRANT SELECT ON TABLE hotwire3.person_hid TO ad_accounts;

-- user
CREATE VIEW hotwire3.user_hid AS 
	SELECT 
		person.id AS user_id, 
		(person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS user_hid
	FROM person
	ORDER BY person.surname, COALESCE(person.known_as, person.first_names);

ALTER TABLE hotwire3.user_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.user_hid TO dev;
GRANT SELECT ON TABLE hotwire3.user_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.user_hid TO groupitreps,headsofgroup;

-- owner
CREATE OR REPLACE VIEW hotwire3.owner_hid AS 
	SELECT 
		person.id AS owner_id, 
		(person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS owner_hid
   	FROM person
  	ORDER BY person.surname, COALESCE(person.known_as, person.first_names);

ALTER TABLE hotwire3.owner_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.owner_hid TO dev;
GRANT SELECT ON TABLE hotwire3.owner_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.owner_hid TO groupitreps,headsofgroup;

-- research group
CREATE OR REPLACE VIEW hotwire3.research_group_hid AS 
 SELECT research_group.id AS research_group_id, research_group.name AS research_group_hid
   FROM research_group
  ORDER BY research_group.name;

ALTER TABLE hotwire3.research_group_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.research_group_hid TO dev;
GRANT SELECT ON TABLE hotwire3.research_group_hid TO PUBLIC;

-- host_system_image
CREATE OR REPLACE VIEW hotwire3.host_system_image_hid AS 
 SELECT system_image.id AS host_system_image_id, ip_address.hostname AS host_system_image_hid
   FROM system_image
   JOIN mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = system_image.id
   JOIN ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
  WHERE ip_address.id = (( SELECT ip_address.id
   FROM system_image s2
   JOIN mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = s2.id
   JOIN ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
  WHERE s2.id = system_image.id
 LIMIT 1));

ALTER TABLE hotwire3.host_system_image_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.host_system_image_hid TO dev;
GRANT SELECT ON TABLE hotwire3.host_system_image_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.host_system_image_hid TO groupitreps,headsofgroup;

-- ip_address
CREATE OR REPLACE VIEW hotwire3.ip_address_hid AS 
 SELECT ip_address.id AS ip_address_id, COALESCE(((ip_address.hostname::text || ' ('::text) || ip_address.ip) || ')'::text, ip_address.ip::text) AS ip_address_hid
   FROM ip_address
  ORDER BY ip_address.hostname, ip_address.ip;

ALTER TABLE hotwire3.ip_address_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.ip_address_hid TO dev;
GRANT SELECT ON TABLE hotwire3.ip_address_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.ip_address_hid TO groupitreps,headsofgroup;

-- room
CREATE OR REPLACE VIEW hotwire3.room_hid AS 
	SELECT 
		room.id AS room_id, 
		((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48) AS room_hid
   	FROM room
   	LEFT JOIN public.building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
   	LEFT JOIN public.building_region_hid ON building_region_hid.building_region_id = room.building_region_id
   	LEFT JOIN public.building_hid ON building_hid.building_id = room.building_id
  	ORDER BY ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48);

ALTER TABLE hotwire3.room_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.room_hid TO dev;
GRANT SELECT ON TABLE hotwire3.room_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.room_hid TO groupitreps,headsofgroup;

-- dept_telephone_numer
CREATE OR REPLACE VIEW hotwire3.dept_telephone_number_hid AS 
 SELECT dept_telephone_number.id AS dept_telephone_number_id, dept_telephone_number.extension_number::text || 
        CASE
            WHEN dept_telephone_number.fax = true THEN ' (fax)'::text
            WHEN dept_telephone_number.personal_line = false THEN ' (shared)'::text
            ELSE ''::text
        END AS dept_telephone_number_hid
   FROM dept_telephone_number
  ORDER BY dept_telephone_number.extension_number;

ALTER TABLE hotwire3.dept_telephone_number_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.dept_telephone_number_hid TO dev;
GRANT SELECT ON TABLE hotwire3.dept_telephone_number_hid TO ro_hid;

-- system_image
CREATE OR REPLACE VIEW hotwire3.system_image_hid AS 
 SELECT system_image.id AS system_image_id, ((COALESCE(COALESCE(system_image.friendly_name, first_ip.hostname::character varying), (COALESCE(hardware.name::text || ' '::text, ' '::text) || COALESCE(hardware.manufacturer::text, ' '::text))::character varying)::text || ' ('::text) || operating_system.os::text) || ')'::text AS system_image_hid
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN ( SELECT si.id, min(ip.hostname::text) AS hostname
   FROM system_image si
   JOIN mm_system_image_ip_address mm ON si.id = mm.system_image_id
   JOIN ip_address ip ON ip.id = mm.ip_address_id
  GROUP BY si.id) first_ip ON system_image.id = first_ip.id
  GROUP BY system_image.id, system_image.friendly_name, first_ip.hostname, hardware.name, hardware.manufacturer, operating_system.os;

ALTER TABLE hotwire3.system_image_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.system_image_hid TO dev;
GRANT SELECT ON TABLE hotwire3.system_image_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.system_image_hid TO wpkguser;

-- Installing 009_add_subview_things

CREATE FUNCTION hotwire3.to_hwsubviewb(
    character varying,
    character varying,
    character varying,
    character varying,
    character varying)
  RETURNS _hwsubviewb AS
' select row($1,$2,$3,$4,$5)::_hwsubviewb; '
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.to_hwsubviewb(character varying, character varying, character varying, character varying, character varying)
  OWNER TO dev;
-- Installing 010_add_system_image_all

CREATE VIEW hotwire3."10_View/Computers/System_Image/_Contact_Details_ro" AS 
 SELECT all_people.person_id || all_people.role::text AS id, system_image.id AS system_image_id, all_people.person_id, all_people.role, person.email_address, dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone, room_hid.room_hid AS mm_room
   FROM system_image
   JOIN (        (         SELECT system_image.id, hardware.owner_id AS person_id, 'Hardware owner'::character varying(20) AS role
                           FROM system_image
                      JOIN hardware ON system_image.hardware_id = hardware.id
                UNION 
                         SELECT system_image.id, system_image.user_id AS person_id, 'System user'::character varying(20) AS role
                           FROM system_image
                          WHERE system_image.user_id IS NOT NULL)
        UNION 
                 SELECT system_image.id, mm_research_group_computer_rep.computer_rep_id AS person_id, 'Group computer rep'::character varying(20) AS role
                   FROM system_image
              JOIN research_group ON system_image.research_group_id = research_group.id
         JOIN mm_research_group_computer_rep ON mm_research_group_computer_rep.research_group_id = research_group.id) all_people USING (id)
   JOIN person ON all_people.person_id = person.id
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid USING (room_id);

ALTER TABLE hotwire3."10_View/Computers/System_Image/_Contact_Details_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Image/_Contact_Details_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Computers/System_Image/_Contact_Details_ro" TO cos;

CREATE VIEW hotwire3."10_View/Computers/System_Image/_IP_address_ro" AS 
 SELECT ip_address.id, system_image.id AS system_image_id, ip_address.ip AS "IP", ip_address.hostname
   FROM ip_address
   JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
   JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id;

ALTER TABLE hotwire3."10_View/Computers/System_Image/_IP_address_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Image/_IP_address_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Computers/System_Image/_IP_address_ro" TO cos;


CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Image/_Hardware_ro" AS 
 SELECT hardware.id, system_image.id AS system_image_id, hardware.id AS hardware_id, hardware.name, hardware.manufacturer, hardware.model, room_hid.room_hid
   FROM hardware
   JOIN system_image ON system_image.hardware_id = hardware.id
   LEFT JOIN room_hid USING (room_id);

ALTER TABLE hotwire3."10_View/Computers/System_Image/_Hardware_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Image/_Hardware_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Computers/System_Image/_Hardware_ro" TO cos;



CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Instances" AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, ARRAY( SELECT mm_dom0_domu.dom0_id
           FROM mm_dom0_domu
          WHERE mm_dom0_domu.domu_id = system_image.id) AS host_system_image_id, system_image.comments::text AS system_image_comments, hardware.comments::text AS hardware_comments, hardware.date_purchased as date_hardware_purchased, hardware.warranty_end_date as hardware_warranty_end_date, system_image.hobbit_tier, system_image.hobbit_flags, system_image.is_development_system, system_image.expiry_date as system_image_expiry_date, hardware.delete_after_date as hardware_delete_after_date, system_image.is_managed_mac, hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image
   JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/System_Instances" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/System_Instances" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.system_image_all_upd()
  RETURNS TRIGGER AS
$BODY$
 
  declare
          v_system_image_id BIGINT;
          v_hardware_id BIGINT;
  begin
 
  IF NEW.id IS NOT NULL THEN
 
    v_system_image_id := NEW.id;
    v_hardware_id := NEW._hardware_id;
 
    UPDATE hardware SET
         manufacturer=NEW.manufacturer, 
         model=NEW.model, 
         name=NEW.hardware_name,
         hardware_type_id=NEW.hardware_type_id, 
         asset_tag = NEW.asset_tag,
         serial_number = NEW.serial_number,
         monitor_serial_number = NEW.monitor_serial_number,
         room_id=NEW.room_id,
         owner_id=NEW.owner_id,
         comments=NEW.hardware_comments::varchar(1000),
         date_purchased = NEW.date_hardware_purchased,
         warranty_end_date = NEW.hardware_warranty_end_date,
         delete_after_date = NEW.hardware_delete_after_date
    WHERE hardware.id = NEW._hardware_id;
 
    UPDATE system_image SET
          user_id=NEW.user_id,
          research_group_id=NEW.research_group_id,
          operating_system_id = NEW.operating_system_id,
          wired_mac_1 = NEW.wired_mac_1, 
          wired_mac_2 = NEW.wired_mac_2,
          wireless_mac = NEW.wireless_mac,
          comments = NEW.system_image_comments::varchar(1000),
          hobbit_tier = NEW.hobbit_tier,
          hobbit_flags = NEW.hobbit_flags,
          is_development_system = NEW.is_development_system,
          expiry_date = NEW.system_image_expiry_date,
          is_managed_mac = NEW.is_managed_mac
    WHERE system_image.id = NEW.id;
 
  ELSE
 
    v_system_image_id := nextval('system_image_id_seq');
    v_hardware_id := nextval('hardware_id_seq');
 
    INSERT INTO hardware
          (manufacturer, model, name, hardware_type_id, asset_tag, serial_number, monitor_serial_number, room_id, owner_id, comments, date_purchased, warranty_end_date, delete_after_date )
    VALUES
          (NEW.manufacturer, NEW.model, NEW.hardware_name, NEW.hardware_type_id, NEW.asset_tag, NEW.serial_number, NEW.monitor_serial_number,
           NEW.room_id, NEW.owner_id, NEW.hardware_comments::varchar(1000), NEW.date_hardware_purchased, NEW.hardware_warranty_end_date, NEW.hardware_delete_after_date) returning id INTO v_hardware_id;
 
    INSERT INTO system_image
          (hardware_id, operating_system_id, wired_mac_1, wired_mac_2,
  wireless_mac,
  comments,user_id,research_group_id,hobbit_tier,hobbit_flags,is_development_system,expiry_date,is_managed_mac)
    VALUES
          (v_hardware_id, NEW.operating_system_id,
           NEW.wired_mac_1, NEW.wired_mac_2, NEW.wireless_mac,
           NEW.system_image_comments::varchar(1000),NEW.user_id,NEW.research_group_id,NEW.hobbit_tier,NEW.hobbit_flags,NEW.is_development_system,NEW.system_image_expiry_date,NEW.is_managed_mac) RETURNING id into v_system_image_id;
 
  END IF;
  PERFORM fn_mm_array_update(NEW.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,v_system_image_id);
 
  return NEW;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.system_image_all_upd() OWNER TO dev;

CREATE OR REPLACE RULE hotwire3_view_system_image_all_del AS
    ON DELETE TO hotwire3."10_View/Computers/System_Instances" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

CREATE TRIGGER hotwire3_view_system_image_all_ins INSTEAD OF INSERT ON hotwire3."10_View/Computers/System_Instances" FOR EACH ROW EXECUTE PROCEDURE hotwire3.system_image_all_upd();
CREATE TRIGGER hotwire3_view_system_image_all_upd INSTEAD OF UPDATE ON hotwire3."10_View/Computers/System_Instances" FOR EACH ROW EXECUTE PROCEDURE hotwire3.system_image_all_upd();

UPDATE hotwire3."hw_User Preferences" SET preference_value = '10_View/Computers/System_Instances' where preference_value = '10_View/System_Image_All';

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/System_Instances', 'system_image');
-- Installing 011_add_it_tasks

CREATE VIEW hotwire3.it_task_hid AS
SELECT it_task.id AS it_task_id, it_task.name AS it_task_hid
   FROM it_task;
ALTER VIEW hotwire3.it_task_hid OWNER TO dev;
GRANT SELECT ON hotwire3.it_task_hid TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3.predecessor_task_hid AS
SELECT it_task.id AS predecessor_task_id, it_task.name AS predecessor_task_hid
   FROM it_task;
ALTER VIEW hotwire3.predecessor_task_hid OWNER TO dev;
GRANT SELECT ON hotwire3.predecessor_task_hid TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3."IT_status_hid" AS
SELECT it_task_status.id AS "IT_status_id", it_task_status.name AS "IT_status_hid"
   FROM it_task_status;
ALTER VIEW hotwire3."IT_status_hid" OWNER TO dev;
GRANT SELECT ON hotwire3."IT_status_hid" TO public;

CREATE VIEW hotwire3.time_estimate_hid AS
SELECT it_task_time_estimate.id AS time_estimate_id, it_task_time_estimate.name AS time_estimate_hid
   FROM it_task_time_estimate;
ALTER VIEW hotwire3.time_estimate_hid OWNER TO dev;
GRANT SELECT ON hotwire3.time_estimate_hid TO public;

CREATE VIEW hotwire3.it_project_hid AS
SELECT it_project.id AS it_project_id, it_project.name AS it_project_hid
   FROM it_project;
ALTER VIEW hotwire3.it_project_hid OWNER TO dev;
GRANT SELECT ON hotwire3.it_project_hid TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3.it_strategic_goal_hid AS
SELECT it_strategic_goal.id AS it_strategic_goal_id, it_strategic_goal.name AS it_strategic_goal_hid
FROM it_strategic_goal;
ALTER VIEW hotwire3.it_strategic_goal_hid OWNER TO dev;
GRANT SELECT ON hotwire3.it_strategic_goal_hid TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3."IT_strategic_goal_hid" AS
SELECT it_strategic_goal.id AS "IT_strategic_goal_id", it_strategic_goal.name AS "IT_strategic_goal_hid"
FROM it_strategic_goal;
ALTER VIEW hotwire3."IT_strategic_goal_hid" OWNER TO dev;
GRANT SELECT ON hotwire3."IT_strategic_goal_hid" TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3."Primary_strategic_goal_hid" AS
SELECT "IT_strategic_goal_hid"."IT_strategic_goal_id" AS "Primary_strategic_goal_id", "IT_strategic_goal_hid"."IT_strategic_goal_hid" AS "Primary_strategic_goal_hid"
FROM hotwire3."IT_strategic_goal_hid";
ALTER VIEW hotwire3."Primary_strategic_goal_hid" OWNER TO dev;
GRANT SELECT ON hotwire3."Primary_strategic_goal_hid" TO ro_hid,headsofgroup,groupitreps;

CREATE VIEW hotwire3.computer_officer_hid as
SELECT
	person_hid.person_id AS computer_officer_id, person_hid.person_hid AS computer_officer_hid
FROM hotwire3.person_hid
JOIN mm_person_research_group USING (person_id)
JOIN research_group ON research_group.id = mm_person_research_group.research_group_id
WHERE research_group.name::text = 'COs'::text
ORDER BY person_hid.person_hid;
ALTER VIEW hotwire3.computer_officer_hid OWNER TO dev;
GRANT SELECT ON hotwire3.computer_officer_hid TO public;

CREATE VIEW hotwire3."IT_task_leader_hid" AS 
SELECT computer_officer_hid.computer_officer_id AS "IT_task_leader_id", computer_officer_hid.computer_officer_hid AS "IT_task_leader_hid"
   FROM hotwire3.computer_officer_hid;
ALTER VIEW hotwire3."IT_task_leader_hid" OWNER TO dev;
GRANT SELECT ON hotwire3."IT_task_leader_hid" TO public;

CREATE VIEW hotwire3."10_View/10_IT_Tasks/Tasks" AS
SELECT
  a.id,
  a.task_number,
  a.name,
  a.task_description::text,
  a.task_notes::text,
  a."Primary_strategic_goal_id",
  a.it_strategic_goal_id,
  a.it_project_id,
  a.person_id,
  a.all_cos,
  a."IT_task_leader_id",
  a.computer_officer_id,
  a.created,
  a.completed,
  a.needed_by,
  a.time_estimate_id,
  a."IT_status_id",
  a.priority,
  a."order",
  a.progress,
  a.predecessor_task_id,
  a._cssclass,
  a.changes,
  a.anticipated_completion_date
FROM
  (
    SELECT
      it_task.id,
      it_task.task_number,
      it_task.name,
      it_task.task_description,
      it_task.task_notes,
      it_task.anticipated_completion_date,
      it_task."Primary_strategic_goal_id",
      ARRAY(
        SELECT
          mm_it_task_strategic_goal.it_strategic_goal_id
        FROM
          mm_it_task_strategic_goal
        WHERE
          mm_it_task_strategic_goal.it_task_id = it_task.id
      ) AS it_strategic_goal_id,
      it_task.it_project_id,
      ARRAY(
        SELECT
          mm_it_task_interested_parties.person_id
        FROM
          mm_it_task_interested_parties
        WHERE
          mm_it_task_interested_parties.it_task_id = it_task.id
      ) AS person_id,
      (
        (
          SELECT
            computer_officer_hid.computer_officer_hid
          FROM
            hotwire3.computer_officer_hid
          WHERE
            computer_officer_hid.computer_officer_id = it_task."IT_task_leader_id"
          LIMIT
            1
        )
      ) || COALESCE(
        '; ' :: text || array_to_string(
          ARRAY(
            SELECT
              computer_officer_hid.computer_officer_hid
            FROM
              mm_it_task_other_cos
              JOIN hotwire3.computer_officer_hid USING (computer_officer_id)
            WHERE
              mm_it_task_other_cos.it_task_id = it_task.id
          ),
          '; ' :: text
        ),
        '' :: text
      ) AS all_cos,
      it_task."IT_task_leader_id",
      ARRAY(
        SELECT
          mm_it_task_other_cos.computer_officer_id
        FROM
          mm_it_task_other_cos
        WHERE
          mm_it_task_other_cos.it_task_id = it_task.id
      ) AS computer_officer_id,
      it_task.created,
      it_task.completed,
      it_task.needed_by,
      it_task.time_estimate_id,
      it_task."IT_status_id",
      it_task.priority,
      it_task."order",
      trunc(
        (10 :: numeric * it_task.progress) :: integer :: numeric * 10.0,
        0
      ) AS progress,
      ARRAY(
        SELECT
          mm_it_task_depends_on.it_predecessor_task_id
        FROM
          mm_it_task_depends_on
        WHERE
          mm_it_task_depends_on.it_successor_task_id = it_task.id
      ) AS predecessor_task_id,
      (
        (
          'complete' :: text || trunc(
            10 :: numeric * COALESCE(it_task.progress, 0 :: numeric),
            0
          )
        ) || ' status' :: text
      ) || COALESCE(it_task."IT_status_id", 0 :: bigint) AS _cssclass,
      hotwire3.to_hwsubviewb(
        '10_View/10_IT_Tasks/_audit_ro' :: character varying,
        'it_task_id' :: character varying,
        NULL :: character varying,
        NULL :: character varying,
        NULL :: character varying
      ) AS changes
    FROM
      it_task
    WHERE
      it_task.deleted IS NULL
    ORDER BY
      COALESCE(it_task.priority, 5),
      it_task.weight
  ) a;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Tasks" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO groupitreps;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO it_committee;

CREATE OR REPLACE FUNCTION hotwire3.it_tasks_upd() RETURNS trigger AS
$BODY$
 
  declare
          v_task_id BIGINT;
  begin

  -- Update
  IF NEW.id IS NOT NULL THEN
 
    v_task_id := NEW.id;

    UPDATE it_task SET
          name=NEW.name,
          task_number=NEW.task_number,
          task_description=NEW.task_description::varchar,
          task_notes = NEW.task_notes::varchar,
          "Primary_strategic_goal_id" = NEW."Primary_strategic_goal_id",
          it_project_id=NEW.it_project_id, 
          "IT_task_leader_id" = NEW."IT_task_leader_id",
          created=NEW.created,
          completed=NEW.completed,
          needed_by =NEW.needed_by,
          time_estimate_id=NEW.time_estimate_id,
          "IT_status_id"=NEW."IT_status_id",
          priority=NEW.priority,
          "order"=NEW."order",
          progress=NEW.progress/100.0,
          anticipated_completion_date=NEW.anticipated_completion_date
    WHERE it_task.id = OLD.id;
 
  ELSE
 
    insert into it_task (name, task_number,task_description, task_notes, "IT_task_leader_id",
     created,completed,needed_by,time_estimate_id,"IT_status_id", priority,"order",progress,"Primary_strategic_goal_id",it_project_id,anticipated_completion_date) values (
     NEW.name,coalesce(NEW.task_number, (select max(task_number)+1 from it_task)), NEW.task_description::varchar, NEW.task_notes::varchar, NEW."IT_task_leader_id",
     coalesce(NEW.created,now()), NEW.completed, NEW.needed_by, NEW.time_estimate_id,
     NEW."IT_status_id", NEW.priority, NEW."order", NEW.progress/100.0,NEW."Primary_strategic_goal_id",NEW.it_project_id,NEW.anticipated_completion_date
    ) returning id into v_task_id;
 
  END IF;
  -- strategic goals
  PERFORM fn_mm_array_update(NEW.it_strategic_goal_id, 'mm_it_task_strategic_goal'::varchar,'it_task_id'::varchar,'it_strategic_goal_id'::varchar,v_task_id);
  -- interested parties
  PERFORM fn_mm_array_update(NEW.person_id, 'mm_it_task_interested_parties'::varchar,'it_task_id'::varchar,'person_id'::varchar,v_task_id);
  -- Dependant tasks
  PERFORM fn_mm_array_update(NEW.predecessor_task_id, 'mm_it_task_depends_on'::varchar,'it_successor_task_id'::varchar,'it_predecessor_task_id'::varchar,v_task_id);  
  -- other COs 
  PERFORM fn_mm_array_update(NEW.computer_officer_id, 'mm_it_task_other_cos'::varchar,'it_task_id'::varchar,'computer_officer_id'::varchar,v_task_id);
 
  NEW.id := v_task_id;
  return NEW;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.it_tasks_upd()
  OWNER TO dev;



CREATE OR REPLACE RULE hotwire3_10_view_10_it_tasks_tasks_del AS
    ON DELETE TO hotwire3."10_View/10_IT_Tasks/Tasks" DO INSTEAD  UPDATE it_task SET deleted = now()
  WHERE it_task.id = old.id;

CREATE TRIGGER hotwire3_10_view_10_it_tasks_tasks_ins INSTEAD OF INSERT ON hotwire3."10_View/10_IT_Tasks/Tasks"  FOR EACH ROW EXECUTE PROCEDURE hotwire3.it_tasks_upd();

CREATE TRIGGER hotwire3_10_view_10_it_tasks_tasks_upd INSTEAD OF UPDATE ON hotwire3."10_View/10_IT_Tasks/Tasks" FOR EACH ROW EXECUTE PROCEDURE hotwire3.it_tasks_upd();


CREATE VIEW hotwire3."10_View/10_IT_Tasks/_audit_ro" AS
SELECT it_task_audit.id, it_task_audit.operation, it_task_audit.stamp, it_task_audit.username, it_task_audit.oldcols, it_task_audit.newcols, it_task_audit.task_id AS it_task_id
   FROM it_task_audit
  WHERE it_task_audit.tablename = 'it_task'::text AND NOT (it_task_audit.oldcols = ''::text AND it_task_audit.newcols = ''::text);
ALTER VIEW hotwire3."10_View/10_IT_Tasks/_audit_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/10_IT_Tasks/_audit_ro" TO cos,headsofgroup,groupitreps;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/10_IT_Tasks/Tasks', 'it_task' );

-- Installing 012_add_system_image_basic

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Virtual_Machines" AS 
 SELECT 
 system_image.id,
  system_image.hardware_id,
  system_image.operating_system_id,
  system_image.wired_mac_1,
  system_image.wired_mac_2,
  system_image.wired_mac_3,
  system_image.wired_mac_4,
  system_image.wireless_mac,
  ARRAY(
   SELECT ip_address_id
   FROM mm_system_image_ip_address
   WHERE mm_system_image_ip_address.system_image_id = system_image.id
  ) as ip_address_id,
  system_image.comments::text,
  hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
  hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware_Full'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
  hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image;

ALTER TABLE hotwire3."10_View/Computers/Virtual_Machines"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Virtual_Machines" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/Virtual_Machines" TO cos;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_del AS
    ON DELETE TO hotwire3."10_View/Computers/Virtual_Machines" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_ins AS
    ON INSERT TO hotwire3."10_View/Computers/Virtual_Machines" DO INSTEAD  INSERT INTO system_image (hardware_id, operating_system_id, wired_mac_1, wired_mac_2, wired_mac_3, wired_mac_4, wireless_mac, comments) 
  VALUES (new.hardware_id, new.operating_system_id, new.wired_mac_1, new.wired_mac_2, new.wired_mac_3, new.wired_mac_4, new.wireless_mac, new.comments::varchar(1000))
  RETURNING system_image.id, NULL::bigint AS int8, NULL::bigint AS int8, NULL::macaddr, NULL::macaddr, NULL::macaddr, NULL::macaddr, NULL::macaddr, ARRAY[NULL::bigint], system_image.comments::text, null::_hwsubviewb, null::_hwsubviewb, null::_hwsubviewb;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Virtual_Machines" DO INSTEAD  UPDATE system_image SET hardware_id = new.hardware_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wired_mac_3 = new.wired_mac_3, wired_mac_4 = new.wired_mac_4, wireless_mac = new.wireless_mac, comments = new.comments::varchar(1000)
  WHERE system_image.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Virtual_Machines', 'system_image');
-- Installing 013_add_system_image_autoinstaller

CREATE VIEW hotwire3.installer_tag_hid AS
SELECT installer_tag_id, installer_tag_hid FROM public.installer_tag_hid;
ALTER VIEW hotwire3.installer_tag_hid OWNER TO dev;
GRANT SELECT ON hotwire3.installer_tag_hid TO PUBLIC;

CREATE VIEW hotwire3.architecture_hid AS
SELECT architectures.id AS architecture_id, architectures.arch AS architecture_hid
   FROM architectures;
ALTER VIEW hotwire3.architecture_hid OWNER TO dev;
GRANT SELECT ON hotwire3.architecture_hid TO PUBLIC;

CREATE VIEW hotwire3.software_package_hid AS 
SELECT software_package.id AS software_package_id, ((('['::text || array_to_string(ARRAY( SELECT DISTINCT os_class_hid.abbrev
           FROM mm_software_package_can_install_on_os
      JOIN operating_system ON mm_software_package_can_install_on_os.operating_system_id = operating_system.id
   JOIN os_class_hid USING (os_class_id)
  WHERE mm_software_package_can_install_on_os.software_package_id = software_package.id), ','::text)) || '] '::text) || ((((software_package.name::text || ' ['::text) || COALESCE(software_package.wpkg_package_name::text || ', '::text, ''::text)) || ' '::text) || COALESCE(software_package.reporting_version, ''::character varying)::text)) || ']'::text AS software_package_hid
   FROM software_package
  ORDER BY software_package.name;
ALTER VIEW hotwire3.software_package_hid OWNER TO dev;
GRANT SELECT ON hotwire3.software_package_hid TO PUBLIC;

CREATE VIEW hotwire3.fai_class_hid AS
SELECT fai_class_hid.fai_class_id, fai_class_hid.fai_class_hid
   FROM fai_class_hid;
ALTER VIEW hotwire3.fai_class_hid OWNER TO dev;
GRANT SELECT ON hotwire3.fai_class_hid TO PUBLIC;

CREATE VIEW hotwire3.netboot_hid AS
SELECT netboot_options.id AS netboot_id, netboot_options."desc" AS netboot_hid
   FROM netboot_options;
ALTER VIEW hotwire3.netboot_hid OWNER TO dev;
GRANT SELECT ON hotwire3.netboot_hid TO PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation" AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, system_image.architecture_id, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id, ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id, system_image.reinstall_on_next_boot, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, research_group.fai_class_id AS ro_fai_class_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.netboot_id,
	hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", 
	hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware_Full'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", 
	hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image
   JOIN hardware ON hardware.id = system_image.hardware_id
   JOIN research_group ON system_image.research_group_id = research_group.id;

ALTER TABLE hotwire3."10_View/Computers/Autoinstallation" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Autoinstallation" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Autoinstallation" TO cos;

CREATE OR REPLACE RULE hotwire3_view_system_image_ai_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Autoinstallation" DO INSTEAD ( UPDATE hardware SET name = new.hardware_name, hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments, research_group_id = new.research_group_id, user_id = new.user_id, netboot_id = new.netboot_id
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.software_package_id, old.software_package_id, 'mm_system_image_software_package_jumpstart'::character varying, 'system_image_id'::character varying, 'software_package_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Autoinstallation', 'system_image');
-- Installing 014_add_system_image_chemnet

CREATE VIEW hotwire3."10_View/Computers/Machine_ChemNet_Tokens" AS
SELECT machine_account.id, machine_account.system_image_id, machine_account.name AS token_name, machine_account.chemnet_token FROM machine_account;
ALTER VIEW hotwire3."10_View/Computers/Machine_ChemNet_Tokens" OWNER TO dev;
GRANT SELECT,UPDATE ON hotwire3."10_View/Computers/Machine_ChemNet_Tokens" TO cos;

CREATE RULE hotwire3_chemnet_tokens_update AS ON UPDATE TO hotwire3."10_View/Computers/Machine_ChemNet_Tokens" DO INSTEAD  UPDATE machine_account SET name = new.token_name, chemnet_token = new.chemnet_token, system_image_id = new.system_image_id
  WHERE machine_account.id = old.id;

CREATE RULE hotwire3_chemnet_tokens_insert AS ON INSERT TO hotwire3."10_View/Computers/Machine_ChemNet_Tokens" DO INSTEAD  INSERT INTO machine_account ( name, chemnet_token, system_image_id ) VALUES ( new.token_name, new.chemnet_token,new.system_image_id) RETURNING machine_account.id, machine_account.system_image_id, machine_account.name, machine_account.chemnet_token;

CREATE RULE hotwire3_chemnet_tokens_delete AS ON DELETE TO hotwire3."10_View/Computers/Machine_ChemNet_Tokens" DO INSTEAD DELETE FROM machine_account WHERE id = old.id;


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Machine_ChemNet_Tokens', 'system_image');
-- Installing 015_add_ip_address

CREATE VIEW hotwire3.subnet_hid AS
SELECT subnet.id AS subnet_id, ((((subnet.network_address || ' ('::text) || subnet.notes::text) || ', '::text) || (( SELECT count(*) AS count
    FROM ip_address
    WHERE ip_address.subnet_id = subnet.id AND NOT ip_address.reserved AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)))) || ' free)'::text AS subnet_hid
FROM subnet
ORDER BY subnet.notes;

ALTER TABLE hotwire3.subnet_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.subnet_hid TO dev;
GRANT SELECT ON TABLE hotwire3.subnet_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.subnet_hid TO cos;


CREATE OR REPLACE VIEW hotwire3."10_View/Network/IP_addresses" AS 
 SELECT ip_address.id, ip_address.ip, ip_address.hostname, ip_address.subnet_id, ip_address.hobbit_flags
   FROM ip_address
  WHERE NOT ip_address.reserved
  ORDER BY ip_address.ip;

ALTER TABLE hotwire3."10_View/Network/IP_addresses" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/IP_addresses" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Network/IP_addresses" TO dns;
GRANT SELECT ON TABLE hotwire3."10_View/Network/IP_addresses" TO osbuilder;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Network/IP_addresses" TO cos;

-- DROP RULE hotwire_view_ip_address_upd ON hotwire3."10_View/Network/IP_addresses";

CREATE OR REPLACE RULE hotwire3_view_ip_address_upd AS
    ON UPDATE TO hotwire3."10_View/Network/IP_addresses" DO INSTEAD UPDATE ip_address SET ip = new.ip, hostname = new.hostname, subnet_id = new.subnet_id, hobbit_flags = new.hobbit_flags
  WHERE ip_address.id = old.id RETURNING NEW.*;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/IP_Addresses', 'ip_address');

-- Installing 016_add_operating_systems

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Operating_Systems" AS 
 SELECT operating_system.id, operating_system.os, operating_system.nmap_identifies_as, operating_system.path_to_autoinstaller, operating_system.default_key
   FROM operating_system;

ALTER TABLE hotwire3."10_View/Computers/Operating_Systems" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Computers/Operating_Systems" TO cos;

CREATE OR REPLACE RULE hotwire3_view_edit_operating_systems_ins AS
    ON INSERT TO hotwire3."10_View/Computers/Operating_Systems" DO INSTEAD  INSERT INTO operating_system (os, nmap_identifies_as, path_to_autoinstaller, default_key) 
  VALUES (new.os, new.nmap_identifies_as, new.path_to_autoinstaller, new.default_key)
  RETURNING operating_system.id, operating_system.os, operating_system.nmap_identifies_as, operating_system.path_to_autoinstaller, operating_system.default_key;

CREATE OR REPLACE RULE hotwire3_view_edit_operating_systems_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Operating_Systems" DO INSTEAD  UPDATE operating_system SET os = new.os, nmap_identifies_as = new.nmap_identifies_as, path_to_autoinstaller = new.path_to_autoinstaller, default_key = new.default_key
  WHERE operating_system.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Operating_Systems', 'operating_system');

-- Installing 017_add_hardware

CREATE VIEW hotwire3."10_View/Computers/System_Image/_System_Instances_ro" AS
SELECT
  system_image.id || '-' || system_image.hardware_id as id,
  system_image.hardware_id,
  system_image.id as system_image_id,
  system_image.wired_mac_1,
  system_image.operating_system_id
FROM system_image;

ALTER TABLE hotwire3."10_View/Computers/System_Image/_System_Instances_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Computers/System_Image/_System_Instances_ro"  TO cos;

CREATE VIEW hotwire3."10_View/Computers/Hardware" AS 
SELECT hardware.id,
 hardware.manufacturer,
 hardware.model,
 hardware.name,
 hardware.comments::text,
 hardware.serial_number,
 hardware.monitor_serial_number,
 hardware.asset_tag,
 hardware.value_when_new,
 hardware.date_purchased,
 hardware.date_configured,
 hardware.date_decommissioned,
 hardware.delete_after_date,
 hardware.warranty_end_date,
 hardware.warranty_details,
 hardware.room_id,
 hardware.hardware_type_id,
 hardware.owner_id AS person_id,
 hardware.rackmount,
 hardware.redundant_psu,
 hardware.power,
 hardware.height,
 hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_System_Instances_ro'::character varying, 'hardware_id'::character varying, '10_View/Computers/System_Instances'::character varying, 'system_image_id'::character varying, 'id'::character varying) AS "System_Instances"
FROM hardware;

ALTER TABLE hotwire3."10_View/Computers/Hardware" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/Hardware" TO cos;

CREATE OR REPLACE RULE hotwire3_view_hardware_full_del AS
    ON DELETE TO hotwire3."10_View/Computers/Hardware" DO INSTEAD  DELETE FROM hardware
  WHERE hardware.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_hardware_full_ins AS
    ON INSERT TO hotwire3."10_View/Computers/Hardware" DO INSTEAD  INSERT INTO hardware (manufacturer, model, name, comments, serial_number, monitor_serial_number, asset_tag, value_when_new, date_purchased, date_configured, date_decommissioned, delete_after_date, warranty_end_date, warranty_details, room_id, hardware_type_id, owner_id, rackmount, redundant_psu, power, height) 
  VALUES (new.manufacturer, new.model, new.name, new.comments::varchar(1000), new.serial_number, new.monitor_serial_number, new.asset_tag, new.value_when_new, new.date_purchased, new.date_configured, new.date_decommissioned, new.delete_after_date, new.warranty_end_date, new.warranty_details, new.room_id, new.hardware_type_id, new.person_id, new.rackmount, new.redundant_psu, new.power, new.height)
  RETURNING hardware.id, hardware.manufacturer, hardware.model, hardware.name, hardware.comments::text, hardware.serial_number, hardware.monitor_serial_number, hardware.asset_tag, hardware.value_when_new, hardware.date_purchased, hardware.date_configured, hardware.date_decommissioned, hardware.delete_after_date, hardware.warranty_end_date, hardware.warranty_details, hardware.room_id, hardware.hardware_type_id, hardware.owner_id, hardware.rackmount, hardware.redundant_psu, hardware.power, hardware.height, row(null,null,null,null,null)::_hwsubviewb;

CREATE OR REPLACE RULE hotwire3_view_hardware_full_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Hardware" DO INSTEAD  UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, comments = new.comments::varchar(1000), serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, value_when_new = new.value_when_new, date_purchased = new.date_purchased, date_configured = new.date_configured, date_decommissioned = new.date_decommissioned, delete_after_date = new.delete_after_date, warranty_end_date = new.warranty_end_date, warranty_details = new.warranty_details, room_id = new.room_id, hardware_type_id = new.hardware_type_id, owner_id = new.person_id, asset_tag = new.asset_tag, rackmount = new.rackmount, redundant_psu = new.redundant_psu, power = new.power, height = new.height
  WHERE hardware.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ( '10_View/Computers/Hardware', 'hardware');
-- Installing 018_add_it_projects

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/IT_Projects" AS 
 SELECT it_project.id, it_project.project_number, it_project.name, it_project.project_purpose, ARRAY( SELECT it_task.id
           FROM it_task
          WHERE it_task.it_project_id = it_project.id) AS it_task_id
   FROM it_project
  ORDER BY it_project.project_number;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/IT_Projects" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/IT_Projects" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/10_IT_Tasks/IT_Projects" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/IT_Projects" TO groupitreps;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/IT_Projects" TO it_committee;

CREATE OR REPLACE RULE "hotwire_10_View/10_IT_Tasks/IT_Projects_del" AS
    ON DELETE TO hotwire3."10_View/10_IT_Tasks/IT_Projects" DO INSTEAD  DELETE FROM it_project
  WHERE it_project.id = old.id;

CREATE OR REPLACE FUNCTION hotwire3.it_projects_upd() RETURNS TRIGGER AS
$BODY$
 
  declare
          v_project_id BIGINT;
  begin

  -- Update
  IF NEW.id IS NOT NULL THEN
 
    v_project_id := NEW.id;

    UPDATE it_project SET
          name=NEW.name,
          project_number=NEW.project_number,
          project_purpose=NEW.project_purpose
    WHERE it_project.id = NEW.id;
 
  ELSE
 
    insert into it_project (name, project_number,project_purpose) values (
     NEW.name,NEW.project_number,NEW.project_purpose
    ) returning id into v_project_id;
    NEW.id := v_project_id;
 
  END IF;
  -- it tasks
  update it_task set it_project_id = null where it_task.it_project_id = v_project_id;
  update it_task set it_project_id = v_project_id where it_task.id = any(NEW.it_task_id);
  
  
  return NEW;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION hotwire3.it_projects_upd() OWNER TO dev;


CREATE TRIGGER "hotwire3_view_it_projects_ins" INSTEAD OF INSERT ON hotwire3."10_View/10_IT_Tasks/IT_Projects" FOR EACH ROW EXECUTE PROCEDURE hotwire3.it_projects_upd() ;

CREATE TRIGGER "hotwire3_view_it_projects_upd" INSTEAD OF UPDATE ON hotwire3."10_View/10_IT_Tasks/IT_Projects" FOR EACH ROW EXECUTE PROCEDURE  hotwire3.it_projects_upd() ;

insert into hotwire3._primary_table ( view_name, primary_table ) values ( '10_View/10_IT_Tasks/IT_Projects', 'it_project');
-- Installing 019_add_school_it_report

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header" AS 
 SELECT it_strategic_goal.id, (it_strategic_goal.id || ' '::text) || it_strategic_goal.name::text AS "Goals"
   FROM it_strategic_goal;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" AS 
 SELECT it_strategic_goal.id, it_strategic_goal.name
   FROM it_strategic_goal
  WHERE it_strategic_goal.id = 1;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects" AS 
 SELECT it_task.id, (it_task.task_number || ' '::text) || it_task.name::text AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", (('complete'::text || trunc(10::numeric * COALESCE(it_task.progress, 0::numeric), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", 0::bigint) AS _cssclass
   FROM it_task
   JOIN it_strategic_goal ON it_strategic_goal.id = it_task."Primary_strategic_goal_id"
  WHERE it_task."Primary_strategic_goal_id" = 1
  ORDER BY it_strategic_goal.name, COALESCE(it_task.priority, 5)::numeric + COALESCE(it_task.weight, 0.9);

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" AS 
 SELECT it_strategic_goal.id, it_strategic_goal.name
   FROM it_strategic_goal
  WHERE it_strategic_goal.id = 2;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects" AS 
 SELECT it_task.id, (it_task.task_number || ' '::text) || it_task.name::text AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", (('complete'::text || trunc(10::numeric * COALESCE(it_task.progress, 0::numeric), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", 0::bigint) AS _cssclass
   FROM it_task
   JOIN it_strategic_goal ON it_strategic_goal.id = it_task."Primary_strategic_goal_id"
  WHERE it_task."Primary_strategic_goal_id" = 2
  ORDER BY it_strategic_goal.name, COALESCE(it_task.priority, 5)::numeric + COALESCE(it_task.weight, 0.9);

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" AS 
 SELECT it_strategic_goal.id, it_strategic_goal.name
   FROM it_strategic_goal
  WHERE it_strategic_goal.id = 3;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects" AS 
 SELECT it_task.id, (it_task.task_number || ' '::text) || it_task.name::text AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", (('complete'::text || trunc(10::numeric * COALESCE(it_task.progress, 0::numeric), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", 0::bigint) AS _cssclass
   FROM it_task
   JOIN it_strategic_goal ON it_strategic_goal.id = it_task."Primary_strategic_goal_id"
  WHERE it_task."Primary_strategic_goal_id" = 3
  ORDER BY it_strategic_goal.name, COALESCE(it_task.priority, 5)::numeric + COALESCE(it_task.weight, 0.9);

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" AS 
 SELECT 0::bigint AS id, 'Completed projects'::character varying AS description;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO it_committee;

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed" AS 
 SELECT it_task.id, (it_task.task_number || ' '::text) || it_task.name::text AS "Description", it_task.completed AS "Date Completed", (('complete'::text || trunc(10::numeric * COALESCE(it_task.progress, 0::numeric), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", 0::bigint) AS _cssclass
   FROM it_task
  WHERE it_task."IT_status_id" = 3;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO it_committee;

-- Installing 020_add_strategic_goals

CREATE OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Strategic Goals" AS 
 SELECT it_strategic_goal.id, it_strategic_goal.name
   FROM it_strategic_goal;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Strategic Goals" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Strategic Goals" TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/10_IT_Tasks/Strategic Goals" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_Views/10_IT_Tasks/Strategic Goals_del" AS
    ON DELETE TO hotwire3."10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD  DELETE FROM it_strategic_goal
  WHERE it_strategic_goal.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_Views/10_IT_Tasks/Strategic Goals_ins" AS
    ON INSERT TO hotwire3."10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD  INSERT INTO it_strategic_goal (name) 
  VALUES (new.name)
  RETURNING it_strategic_goal.id, it_strategic_goal.name;

CREATE OR REPLACE RULE "hotwire3_10_Views/10_IT_Tasks/Strategic Goals_upd" AS
    ON UPDATE TO hotwire3."10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD  UPDATE it_strategic_goal SET id = new.id, name = new.name
  WHERE it_strategic_goal.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) values ( '10_View/10_IT_Tasks/Strategic Goals','it_strategic_goal');
-- Installing 021_add_buildings_building

CREATE OR REPLACE VIEW hotwire3."10_View/Buildings/Buildings" AS 
 SELECT building_hid.building_id AS id, building_hid.building_hid AS building, building_hid.embs_building_code, building_hid.email_abbreviation as ro_email_abbreviation
   FROM building_hid;

ALTER TABLE hotwire3."10_View/Buildings/Buildings" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Buildings/Buildings" TO space_management;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Buildings/Buildings" TO dev;

CREATE OR REPLACE RULE hotwire3_view_buildings_ins AS
    ON INSERT TO hotwire3."10_View/Buildings/Buildings" DO INSTEAD  INSERT INTO building_hid (building_hid, embs_building_code) 
  VALUES (new.building, new.embs_building_code) RETURNING building_hid.building_id, building_hid.building_hid, building_hid.embs_building_code, building_hid.email_abbreviation;

CREATE OR REPLACE RULE hotwire3_view_buildings_upd AS
    ON UPDATE TO hotwire3."10_View/Buildings/Buildings" DO INSTEAD  UPDATE building_hid SET building_hid = new.building, embs_building_code = new.embs_building_code
  WHERE building_hid.building_id = old.id;
-- Installing 022_add_buildings_floor

CREATE OR REPLACE VIEW hotwire3."10_View/Buildings/Floors" AS 
 SELECT building_floor_hid.building_floor_id AS id, building_floor_hid.building_floor_hid AS building_floor
   FROM building_floor_hid;

ALTER TABLE hotwire3."10_View/Buildings/Floors" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Buildings/Floors" TO space_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Buildings/Floors" TO dev;

CREATE OR REPLACE RULE hotwire3_view_building_floors_ins AS
    ON INSERT TO hotwire3."10_View/Buildings/Floors" DO INSTEAD  INSERT INTO building_floor_hid (building_floor_hid) 
  VALUES (new.building_floor)
  RETURNING building_floor_hid.building_floor_id, building_floor_hid.building_floor_hid;

CREATE OR REPLACE RULE hotwire3_view_building_floors_upd AS
    ON UPDATE TO hotwire3."10_View/Buildings/Floors" DO INSTEAD  UPDATE building_floor_hid SET building_floor_hid = new.building_floor
  WHERE building_floor_hid.building_floor_id = old.id;
-- Installing 023_add_buildings_region

CREATE OR REPLACE VIEW hotwire3."10_View/Buildings/Regions" AS 
 SELECT building_region_hid.building_region_id AS id, building_region_hid.building_region_hid AS building_region
   FROM building_region_hid;

ALTER TABLE hotwire3."10_View/Buildings/Regions" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Buildings/Regions" TO space_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Buildings/Regions" TO dev;

CREATE OR REPLACE RULE hotwire3_view_building_regions_ins AS
    ON INSERT TO hotwire3."10_View/Buildings/Regions" DO INSTEAD  INSERT INTO building_region_hid (building_region_hid) 
  VALUES (new.building_region)
  RETURNING building_region_hid.building_region_id, building_region_hid.building_region_hid;

CREATE OR REPLACE RULE hotwire3_view_building_regions_upd AS
    ON UPDATE TO hotwire3."10_View/Buildings/Regions" DO INSTEAD  UPDATE building_region_hid SET building_region_hid = new.building_region
  WHERE building_region_hid.building_region_id = old.id;

-- Installing 024_add_dns_submenu

CREATE VIEW hotwire3.dns_domain_hid AS
SELECT dns_domains.id AS dns_domain_id, dns_domains.name AS dns_domain_hid
FROM dns_domains;

ALTER TABLE hotwire3.dns_domain_hid OWNER TO dev;
GRANT SELECT ON hotwire3.dns_domain_hid TO cos,ro_hid;

CREATE OR REPLACE VIEW hotwire3."10_View/DNS/A_Records" AS 
 SELECT dns_anames.id, dns_anames.name, dns_anames.dns_domain_id, dns_anames.ip_address_id
   FROM dns_anames;

ALTER TABLE hotwire3."10_View/DNS/A_Records" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/DNS/A_Records" TO cos;

CREATE OR REPLACE RULE hotwire3_view_dns_anames_del AS
    ON DELETE TO hotwire3."10_View/DNS/A_Records" DO INSTEAD  DELETE FROM dns_anames
  WHERE dns_anames.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_dns_anames_ins AS
    ON INSERT TO hotwire3."10_View/DNS/A_Records" DO INSTEAD  INSERT INTO dns_anames (name, dns_domain_id, ip_address_id) 
  VALUES (new.name, new.dns_domain_id, new.ip_address_id)
  RETURNING dns_anames.id, dns_anames.name, dns_anames.dns_domain_id, dns_anames.ip_address_id;

CREATE OR REPLACE RULE hotwire3_view_dns_anames_upd AS
    ON UPDATE TO hotwire3."10_View/DNS/A_Records" DO INSTEAD  UPDATE dns_anames SET name = new.name, dns_domain_id = new.dns_domain_id, ip_address_id = new.ip_address_id
  WHERE dns_anames.id = old.id;



CREATE OR REPLACE VIEW hotwire3."10_View/DNS/CNames" AS 
 SELECT dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date, dns_cnames.research_group_id
   FROM dns_cnames;

ALTER TABLE hotwire3."10_View/DNS/CNames" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/DNS/CNames" TO cos;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_del AS
    ON DELETE TO hotwire3."10_View/DNS/CNames" DO INSTEAD  DELETE FROM dns_cnames
  WHERE dns_cnames.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_ins AS
    ON INSERT TO hotwire3."10_View/DNS/CNames" DO INSTEAD  INSERT INTO dns_cnames (name, dns_domain_id, toname, fromname, expiry_date, research_group_id) 
  VALUES (new.name, new.dns_domain_id, new.toname, (((new.name::text || '.'::text) || ((( SELECT dns_domains.name
           FROM dns_domains
          WHERE dns_domains.id = new.dns_domain_id))::text))), new.expiry_date, new.research_group_id)
  RETURNING dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date, dns_cnames.research_group_id;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_upd AS
    ON UPDATE TO hotwire3."10_View/DNS/CNames" DO INSTEAD  UPDATE dns_cnames SET name = new.name, dns_domain_id = new.dns_domain_id, research_group_id = new.research_group_id, toname = new.toname, expiry_date = new.expiry_date, fromname = (((new.name::text || '.'::text) || ((( SELECT dns_domains.name
           FROM dns_domains
          WHERE dns_domains.id = new.dns_domain_id))::text)))
  WHERE dns_cnames.id = old.id;




CREATE OR REPLACE VIEW hotwire3."10_View/DNS/MX_Records" AS 
 SELECT dns_mxrecords.id, dns_mxrecords.domainname, dns_mxrecords.mailhost
   FROM dns_mxrecords;

ALTER TABLE hotwire3."10_View/DNS/MX_Records" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/DNS/MX_Records" TO cos;

CREATE OR REPLACE RULE hotwire3_view_dns_mx_records_del AS
    ON DELETE TO hotwire3."10_View/DNS/MX_Records" DO INSTEAD  DELETE FROM dns_mxrecords
  WHERE dns_mxrecords.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_dns_mx_records_ins AS
    ON INSERT TO hotwire3."10_View/DNS/MX_Records" DO INSTEAD  INSERT INTO dns_mxrecords (id, domainname, mailhost) 
  VALUES (nextval('dns_mxrecords_id_seq'::regclass), new.domainname, new.mailhost)
  RETURNING dns_mxrecords.id, dns_mxrecords.domainname, dns_mxrecords.mailhost;

CREATE OR REPLACE RULE hotwire3_view_dns_mx_records_upd AS
    ON UPDATE TO hotwire3."10_View/DNS/MX_Records" DO INSTEAD  UPDATE dns_mxrecords SET domainname = new.domainname, mailhost = new.mailhost
  WHERE dns_mxrecords.id = old.id;

-- Installing 025_add_data_protection_submenu

create view hotwire3."10_View/Data_Protection/Access_Control_Lists" as
with acls as 
	(
	SELECT
		c.relname as "Name",
		pg_catalog.array_to_string(c.relacl, E'\n') AS "Access privileges"
	FROM pg_catalog.pg_class c
	LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
	WHERE c.relkind = 'v'
		AND 
	n.nspname ~ '^(hotwire3)$'
        ORDER BY 1, 2 
	) 
select 
	row_number() over(order by "Name") as id, 
	trim(leading '0123456789_' from "Name") as "Name", 
	"Access privileges" 
from acls;

alter view hotwire3."10_View/Data_Protection/Access_Control_Lists" owner to dev;
grant select on hotwire3."10_View/Data_Protection/Access_Control_Lists" to hr,cos;

create view hotwire3."10_View/Data_Protection/Access_Control_Groups" as
with acl_groups as (
    select 
        r.rolname as role,
        member.rolname as member,
        person.id as person_id
    from pg_catalog.pg_roles r 
    join pg_catalog.pg_auth_members m on m.roleid = r.oid
    join pg_catalog.pg_roles member on m.member = member.oid
    left join person on member.rolname = person.crsid
)
select
    row_number() over(order by role, member, person_id) as id,
    role,
    member,
    person_id
from acl_groups
where role <> '_autoadded'
order by role, member
;

alter view hotwire3."10_View/Data_Protection/Access_Control_Groups" owner to dev;
grant select on hotwire3."10_View/Data_Protection/Access_Control_Groups"  to hr, cos;

create view hotwire3."10_View/Data_Protection/Database_Users" as 
with dbusers as (
select 
    r.rolname as rolename,
    ARRAY(
        SELECT b.rolname
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = r.oid and b.rolname <> '_autoadded'
    ) as member_of,
    person_hid.person_hid as person
from pg_catalog.pg_roles r
left join person on rolname = person.crsid
left join person_hid on person.id = person_hid.person_id -- this is to work around a hotwire3 sorting bug
)
select row_number() over () as id,
    rolename,
    member_of,
    person
from dbusers
order by rolename,member_of,person
;

alter view hotwire3."10_View/Data_Protection/Database_Users" owner to dev;
grant select on hotwire3."10_View/Data_Protection/Database_Users" to cos,hr;
-- Installing 026_add_easy_add_machine_view

CREATE VIEW hotwire3.easy_addable_subnet_hid AS
SELECT 
  subnet.id AS easy_addable_subnet_id, 
    (
      (((subnet.network_address || ' ('::text) || subnet.notes::text) || ', '::text) || 
      ( 
        SELECT count(*) AS count
        FROM ip_address
        WHERE 
          ip_address.subnet_id = subnet.id 
        AND NOT 
          ip_address.reserved 
        AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
      )
    ) || 
    ' free)'::text AS easy_addable_subnet_hid
FROM subnet
WHERE subnet.can_easy_add = true
ORDER BY subnet.notes;

ALTER TABLE hotwire3.easy_addable_subnet_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.easy_addable_subnet_hid TO dev;
GRANT SELECT ON TABLE hotwire3.easy_addable_subnet_hid TO cos;

CREATE VIEW hotwire3."10_View/Computers/Easy_Add_Machine" AS 
SELECT 
  (
    SELECT 
      person.id 
    FROM 
      person 
    WHERE 
      person.crsid :: name = "current_user"()
  ) AS id, 
  '' :: character varying(63) AS hostname, 
  (
    SELECT 
      easy_addable_subnet_hid.easy_addable_subnet_id 
    FROM 
      hotwire3.easy_addable_subnet_hid 
    LIMIT 
      1
  ) AS easy_addable_subnet_id, 
  NULL :: macaddr AS wired_mac_1, 
  (
    SELECT 
      operating_system_hid.operating_system_id 
    FROM 
      hotwire3.operating_system_hid 
    WHERE 
      operating_system_hid.operating_system_hid :: text = 'Windows 10' :: text 
    ORDER BY 
      operating_system_hid.operating_system_id DESC 
    LIMIT 
      1
  ) AS operating_system_id, 
  'Unknown' :: character varying(20) AS manufacturer, 
  'Unknown' :: character varying(20) AS model, 
  '' :: character varying(20) AS hardware_name, 
  (
    SELECT 
      hardware_type_hid.hardware_type_id 
    FROM 
      hotwire3.hardware_type_hid 
    WHERE 
      hardware_type_hid.hardware_type_hid :: text = 'PC' :: text 
    LIMIT 
      1
  ) AS hardware_type_id, 
  '' :: character varying(80) AS system_image_comments, 
  NULL :: integer AS host_system_image_id, 
  (
    SELECT 
      mm_person_room.room_id 
    FROM 
      person 
      JOIN mm_person_room ON person.id = mm_person_room.person_id 
    WHERE 
      person.crsid :: name = "current_user"() 
    LIMIT 
      1
  ) AS room_id, 
  NULL :: macaddr AS wired_mac_2, 
  (
    SELECT 
      person.id 
    FROM 
      person 
    WHERE 
      person.crsid :: name = "current_user"()
  ) AS user_id, 
  (
    SELECT 
      person.id 
    FROM 
      person 
    WHERE 
      person.crsid :: name = "current_user"()
  ) AS owner_id, 
  (
    SELECT 
      mm_person_research_group.research_group_id 
    FROM 
      person 
      JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id 
    WHERE 
      person.crsid :: name = "current_user"() 
    LIMIT 
      1
  ) AS research_group_id;
ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Machine" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Machine" TO dev;
GRANT SELECT,UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Machine" TO cos;

-- This is normally bad (an 'instead of insert' rule without a returning clause) but it's what we want for this view,
-- where we don't go back and edit the entry afterwards.
CREATE RULE hotwire3_view_easy_add_machine_view_upd AS ON UPDATE TO hotwire3."10_View/Computers/Easy_Add_Machine" DO INSTEAD (
  INSERT INTO hardware (
    manufacturer, model, room_id, hardware_type_id, 
    owner_id, name
  ) 
  VALUES 
    (
      new.manufacturer, new.model, new.room_id, 
      new.hardware_type_id, new.owner_id, 
      new.hardware_name
    );
INSERT INTO system_image (
  wired_mac_1, wired_mac_2, hardware_id, 
  operating_system_id, comments, host_system_image_id, 
  user_id, research_group_id
) 
VALUES 
  (
    new.wired_mac_1, 
    new.wired_mac_2, 
    currval('hardware_id_seq' :: regclass), 
    new.operating_system_id, 
    new.system_image_comments, 
    new.host_system_image_id, 
    new.user_id, 
    new.research_group_id
  );
INSERT INTO mm_system_image_ip_address (ip_address_id, system_image_id) 
VALUES 
  (
    (
      SELECT 
        ip_address.id 
      FROM 
        ip_address 
      WHERE 
        ip_address.subnet_id = new.easy_addable_subnet_id 
        AND ip_address.reserved <> true 
        AND (
          ip_address.hostname IS NULL 
          OR ip_address.hostname :: text = '' :: text
        ) 
      LIMIT 
        1
    ), currval(
      'system_image_id_seq' :: regclass
    )
  );
UPDATE 
  ip_address 
SET 
  hostname = new.hostname 
WHERE 
  (
    ip_address.id IN (
      SELECT 
        mm_system_image_ip_address.ip_address_id 
      FROM 
        mm_system_image_ip_address 
      WHERE 
        mm_system_image_ip_address.system_image_id = currval(
          'system_image_id_seq' :: regclass
        )
    )
  );
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Computers/Easy_Add_Machine', 'system_image');
-- Installing 027_add_easy_add_virtual_machine_view

CREATE VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" AS 
SELECT 
  (
    SELECT 
      person.id 
    FROM 
      person 
    WHERE 
      person.crsid :: name = "current_user"()
  ) AS id, 
  '' :: character varying(63) AS hostname, 
  NULL :: integer AS subnet_id, 
  NULL :: integer AS hardware_id, 
  NULL :: macaddr AS wired_mac_1, 
  (
    SELECT 
      operating_system_hid.operating_system_id 
    FROM 
      hotwire3.operating_system_hid 
    WHERE 
      operating_system_hid.operating_system_hid :: text ~* '%debian%' :: text 
    ORDER BY 
      operating_system_hid.operating_system_id DESC 
    LIMIT 1
  ) AS operating_system_id, 
  '' :: character varying AS system_image_comments, 
  NULL :: integer AS host_system_image_id, 
  NULL :: macaddr AS wired_mac_2, 
  (
    SELECT 
      person.id 
    FROM 
      person 
    WHERE 
      person.crsid :: name = "current_user"()
  ) AS user_id, 
  (
    SELECT 
      mm_person_research_group.research_group_id 
    FROM 
      person 
      JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id 
    WHERE 
      person.crsid :: name = "current_user"() 
    LIMIT 
      1
  ) AS research_group_id;
ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO dev;
GRANT SELECT,UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.upd_easy_add_virtual_machine() RETURNS TRIGGER AS
$BODY$

     declare
                      ip_id BIGINT;                                                                                        
     begin                                                                                                              
	ip_id =  (SELECT ip_address.id
			FROM ip_address
			WHERE ip_address.subnet_id = NEW.subnet_id 
				AND ip_address.reserved <> true 
				AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
			LIMIT 1);

	INSERT INTO system_image 
		(wired_mac_1, wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
		VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.system_image_comments, NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id);
	update ip_address set hostname = NEW.hostname where id = ip_id;
	insert into mm_system_image_ip_address ( ip_address_id, system_image_id )
		values ( ip_id, currval('system_image_id_seq'::regclass));
	return NEW;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.upd_easy_add_virtual_machine() OWNER TO dev;


CREATE TRIGGER hotwire3_view_easy_add_virtual_machine_view_upd INSTEAD OF UPDATE ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" FOR EACH ROW EXECUTE PROCEDURE hotwire3.upd_easy_add_virtual_machine();

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Computers/Easy_Add_Virtual_Machine', 'system_image');

-- Installing 028_add_switch_model_hid

CREATE VIEW hotwire3.switch_model_hid AS
SELECT switch_model.id AS switch_model_id, switch_model.description AS switch_model_hid
FROM switch_model;
ALTER TABLE hotwire3.switch_model_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.switch_model_hid TO cos;

-- Installing 029_add_hardware_types

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Hardware_Types" AS 
 SELECT hardware_type.id, hardware_type.name
   FROM hardware_type;

ALTER TABLE hotwire3."10_View/Computers/Hardware_Types" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Computers/Hardware_Types" TO cos;

CREATE OR REPLACE RULE hotwire3_view_edit_hardware_types_del AS
    ON DELETE TO hotwire3."10_View/Computers/Hardware_Types" DO INSTEAD  DELETE FROM hardware_type
  WHERE hardware_type.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_edit_hardware_types_ins AS
    ON INSERT TO hotwire3."10_View/Computers/Hardware_Types" DO INSTEAD  INSERT INTO hardware_type (name) 
  VALUES (new.name)
  RETURNING hardware_type.id, hardware_type.name;

CREATE OR REPLACE RULE hotwire3_view_edit_hardware_types_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Hardware_Types" DO INSTEAD  UPDATE hardware_type SET name = new.name
  WHERE hardware_type.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table ) VALUES ('10_View/Computers/Hardware_Types', 'hardware_type');
-- Installing 030_add_titles

CREATE OR REPLACE VIEW hotwire3."10_View/People/Titles" AS 
 SELECT title_hid.title_id AS id, title_hid.title_hid as title, title_hid.long_title_hid AS long_title, title_hid.website_title_hid as title_shown_on_websites
   FROM title_hid;

ALTER TABLE hotwire3."10_View/People/Titles" OWNER TO dev;
-- Because this can have a dramatic effect on people's website descriptions I am not
-- sure I want it to be very editable 
-- GRANT SELECT, INSERT, UPDATE ON TABLE hotwire3."10_View/People/Titles" TO hr;
GRANT SELECT, INSERT, UPDATE ON TABLE hotwire3."10_View/People/Titles" TO dev;

CREATE OR REPLACE RULE hotwire3_people_titles_ins AS
    ON INSERT TO hotwire3."10_View/People/Titles" DO INSTEAD  INSERT INTO title_hid (title_hid, long_title_hid, website_title_hid) 
  VALUES (new.title, new.long_title, new.title_shown_on_websites) RETURNING title_hid.title_id, title_hid.title_hid, title_hid.long_title_hid, title_hid.website_title_hid;

CREATE RULE hotwire3_people_titles_upd AS 
    ON UPDATE TO hotwire3."10_View/People/Titles" DO INSTEAD UPDATE title_hid set title_hid = new.title, long_title_hid = new.long_title, website_title_hid = new.title_shown_on_websites WHERE title_id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Titles', 'title_hid');
-- Installing 031_add_group_computers_view

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Computers" AS 
 SELECT DISTINCT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, array_to_string(ARRAY(SELECT ip_address_hid from mm_system_image_ip_address join hotwire3.ip_address_hid using (ip_address_id) where system_image_id = system_image.id ),',') as ro_ip_address, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   JOIN mm_research_group_computer_rep USING (research_group_id)
   JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
   JOIN research_group ON system_image.research_group_id = research_group.id
   JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
   LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE hotwire3."10_View/My_Groups/Computers" OWNER TO dev;
GRANT SELECT, UPDATE, DELETE, REFERENCES, TRIGGER ON TABLE hotwire3."10_View/My_Groups/Computers" TO groupitreps, headsofgroup;

CREATE OR REPLACE RULE group_computers_view_del AS
    ON DELETE TO hotwire3."10_View/My_Groups/Computers" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

CREATE OR REPLACE RULE group_computers_view_upd AS
    ON UPDATE TO hotwire3."10_View/My_Groups/Computers" DO INSTEAD ( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET user_id = new.user_id, research_group_id = new.research_group_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments
  WHERE system_image.id = old.id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ('10_View/My_Groups/Computers','system_image');
-- Installing 032_add_group_computers_autoinstaller

CREATE VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation" AS 
 SELECT DISTINCT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.architecture_id, system_image.reinstall_on_next_boot, array_to_string(ARRAY(SELECT ip_address_hid from mm_system_image_ip_address join hotwire3.ip_address_hid using (ip_address_id) where system_image_id = system_image.id ),',') as ro_ip_address, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, research_group.fai_class_id AS ro_fai_class_id
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   JOIN mm_research_group_computer_rep USING (research_group_id)
   JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
   JOIN research_group ON system_image.research_group_id = research_group.id
   JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
   LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation"
  OWNER TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO groupitreps, headsofgroup;

CREATE OR REPLACE RULE group_computers_view_del AS
    ON DELETE TO hotwire3."10_View/My_Groups/Computer_Autoinstallation" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

CREATE OR REPLACE RULE group_computers_view_upd AS
    ON UPDATE TO hotwire3."10_View/My_Groups/Computer_Autoinstallation" DO INSTEAD ( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET user_id = new.user_id, research_group_id = new.research_group_id, operating_system_id = new.operating_system_id, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ('10_View/My_Groups/Computer_Autoinstallation','system_image');

-- Installing 033_add_group_fileservers

CREATE VIEW hotwire3.os_class_hid AS
SELECT os_class_hid.os_class_id, os_class_hid.os_class_hid
   FROM os_class_hid;
ALTER VIEW hotwire3.os_class_hid OWNER TO dev;
GRANT SELECT ON hotwire3.os_class_hid TO PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Group_Fileservers" AS 
 SELECT group_fileserver.id, group_fileserver.hostname_for_users, group_fileserver.system_image_id, group_fileserver.homepath, group_fileserver.localhomepath, group_fileserver.profilepath, group_fileserver.localprofilepath, operating_system.os_class_id
   FROM group_fileserver
   JOIN system_image ON group_fileserver.system_image_id = system_image.id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id;

ALTER TABLE hotwire3."10_View/Groups/Group_Fileservers" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Group_Fileservers" TO cos;

CREATE OR REPLACE RULE hotwire3_view_group_fileservers_del AS
    ON DELETE TO hotwire3."10_View/Groups/Group_Fileservers" DO INSTEAD  DELETE FROM group_fileserver
  WHERE group_fileserver.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_group_fileservers_ins AS
    ON INSERT TO hotwire3."10_View/Groups/Group_Fileservers" DO INSTEAD  INSERT INTO group_fileserver (hostname_for_users, system_image_id, homepath, localhomepath, profilepath, localprofilepath) 
  VALUES (new.hostname_for_users, new.system_image_id, new.homepath, new.localhomepath, new.profilepath, new.localprofilepath)
  RETURNING group_fileserver.id, group_fileserver.hostname_for_users, group_fileserver.system_image_id, group_fileserver.homepath, group_fileserver.localhomepath, group_fileserver.profilepath, group_fileserver.localprofilepath, NULL::bigint AS int8;

CREATE OR REPLACE RULE hotwire3_view_group_fileservers_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Group_Fileservers" DO INSTEAD  UPDATE group_fileserver SET hostname_for_users = new.hostname_for_users, system_image_id = new.system_image_id, homepath = new.homepath, localhomepath = new.localhomepath, profilepath = new.profilepath, localprofilepath = new.localprofilepath
  WHERE group_fileserver.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Groups/Group_Fileservers', 'group_fileserver');
-- Installing 034_add_xymon_client_cfg_view

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg" AS 
 SELECT hobbit_clients_config_stanza.id, hobbit_clients_config_stanza.system_image_id, hobbit_clients_config_stanza.handle, hobbit_clients_config_stanza.rules::text, hobbit_clients_config_stanza.settings::text
   FROM hobbit_clients_config_stanza;

ALTER TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_del" AS
    ON DELETE TO hotwire3."10_View/Computers/Xymon_Clients_Cfg" DO INSTEAD  DELETE FROM hobbit_clients_config_stanza
  WHERE hobbit_clients_config_stanza.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_ins" AS
    ON INSERT TO hotwire3."10_View/Computers/Xymon_Clients_Cfg" DO INSTEAD  INSERT INTO hobbit_clients_config_stanza (rules, settings, handle, system_image_id) 
  VALUES (new.rules::varchar, new.settings::varchar, new.handle, new.system_image_id)
  RETURNING hobbit_clients_config_stanza.id, hobbit_clients_config_stanza.system_image_id, hobbit_clients_config_stanza.handle, hobbit_clients_config_stanza.rules::text, hobbit_clients_config_stanza.settings::text;

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_upd" AS
    ON UPDATE TO hotwire3."10_View/Computers/Xymon_Clients_Cfg" DO INSTEAD  UPDATE hobbit_clients_config_stanza SET rules = new.rules::varchar, settings = new.settings::varchar, handle = new.handle, system_image_id = new.system_image_id
  WHERE hobbit_clients_config_stanza.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Computers/Xymon_Clients_Cfg', 'hobbit_clients_config_stanza');
-- Installing 035_add_it_sales_view

CREATE VIEW hotwire3.itsales_type_hid AS
SELECT itsales_type_hid.id AS itsales_type_id, itsales_type_hid.type AS itsales_type_hid
FROM itsales_type_hid;
ALTER VIEW hotwire3.itsales_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.itsales_type_hid TO PUBLIC;

CREATE VIEW hotwire3.itsales_unit_hid AS
SELECT itsales_unit_hid.id AS itsales_unit_id, itsales_unit_hid.unit AS itsales_unit_hid
FROM itsales_unit_hid;
ALTER VIEW hotwire3.itsales_unit_hid OWNER TO dev;
GRANT SELECT ON hotwire3.itsales_unit_hid TO PUBLIC;

CREATE VIEW hotwire3.itsales_status_hid AS
SELECT itsales_status_hid.id AS itsales_status_id, itsales_status_hid.status AS itsales_status_hid
FROM itsales_status_hid;
ALTER VIEW hotwire3.itsales_status_hid OWNER TO dev;
GRANT SELECT ON hotwire3.itsales_status_hid TO PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/IT_Sales/Sales" AS 
 SELECT itsales_item.id, itsales_item.itsales_type_id, itsales_item.research_group_id, itsales_item.person_id::bigint, itsales_item.quantity, itsales_item.itsales_unit_id, itsales_item.delivery_date, itsales_item.expires_date, itsales_item.notes, itsales_item.itsales_status_id, itsales_item.unit_cost, itsales_item.charge_account
   FROM itsales_item;

ALTER TABLE hotwire3."10_View/IT_Sales/Sales"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/IT_Sales/Sales" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/IT_Sales/Sales" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/IT_Sales/Sales_del" AS
    ON DELETE TO hotwire3."10_View/IT_Sales/Sales" DO INSTEAD  DELETE FROM itsales_item
  WHERE itsales_item.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/IT_Sales/Sales_ins" AS
    ON INSERT TO hotwire3."10_View/IT_Sales/Sales" DO INSTEAD  INSERT INTO itsales_item (itsales_type_id, research_group_id, person_id, quantity, itsales_unit_id, delivery_date, expires_date, notes, itsales_status_id, unit_cost, charge_account) 
  VALUES (new.itsales_type_id, new.research_group_id, new.person_id::int, new.quantity, new.itsales_unit_id, new.delivery_date, new.expires_date, new.notes, new.itsales_status_id, new.unit_cost, new.charge_account)
  RETURNING itsales_item.id, itsales_item.itsales_type_id, itsales_item.research_group_id, itsales_item.person_id::bigint, itsales_item.quantity, itsales_item.itsales_unit_id, itsales_item.delivery_date, itsales_item.expires_date, itsales_item.notes, itsales_item.itsales_status_id, itsales_item.unit_cost, itsales_item.charge_account;

CREATE OR REPLACE RULE "hotwire3_10_View/IT_Sales/Sales_upd" AS
    ON UPDATE TO hotwire3."10_View/IT_Sales/Sales" DO INSTEAD  UPDATE itsales_item SET id = new.id, itsales_type_id = new.itsales_type_id, research_group_id = new.research_group_id, person_id = new.person_id::int, quantity = new.quantity, itsales_unit_id = new.itsales_unit_id, delivery_date = new.delivery_date, expires_date = new.expires_date, notes = new.notes, itsales_status_id = new.itsales_status_id, unit_cost = new.unit_cost, charge_account = new.charge_account
  WHERE itsales_item.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/IT_Sales/Sales', 'itsales_item');
-- Installing 036_add_managed_mail_domain_view

CREATE OR REPLACE VIEW hotwire3."10_View/Email/Chemistry_Mail_Domain" AS 
 SELECT mail_alias.mail_alias_id AS id, mail_alias.alias, mail_alias.addresses, mail_alias.comment::text
   FROM mail_alias
  ORDER BY mail_alias.alias;

ALTER TABLE hotwire3."10_View/Email/Chemistry_Mail_Domain" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Email/Chemistry_Mail_Domain" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_del" AS
    ON DELETE TO hotwire3."10_View/Email/Chemistry_Mail_Domain" DO INSTEAD  DELETE FROM mail_alias
  WHERE mail_alias.mail_alias_id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_ins" AS
    ON INSERT TO hotwire3."10_View/Email/Chemistry_Mail_Domain" DO INSTEAD  INSERT INTO mail_alias (comment, alias, addresses) 
  VALUES (new.comment::varchar, new.alias, new.addresses)
  RETURNING mail_alias.mail_alias_id, mail_alias.alias, mail_alias.addresses, mail_alias.comment::text;

CREATE OR REPLACE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_upd" AS
    ON UPDATE TO hotwire3."10_View/Email/Chemistry_Mail_Domain" DO INSTEAD  UPDATE mail_alias SET mail_alias_id = new.id, comment = new.comment::varchar, alias = new.alias, addresses = new.addresses
  WHERE mail_alias.mail_alias_id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Email/Chemistry_Mail_Domain', 'mail_alias');
-- Installing 037_add_mailing_list_views

CREATE VIEW hotwire3.include_person_hid AS
SELECT person.id AS include_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS include_person_hid
   FROM person
      LEFT JOIN title_hid USING (title_id)
        ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;
ALTER VIEW hotwire3.include_person_hid OWNER TO dev;
GRANT SELECT ON hotwire3.include_person_hid TO cos;

CREATE VIEW hotwire3.exclude_person_hid AS 
 SELECT person.id AS exclude_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS exclude_person_hid
    FROM person
       LEFT JOIN title_hid USING (title_id)
         ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;
ALTER VIEW hotwire3.exclude_person_hid OWNER TO dev;
GRANT SELECT ON  hotwire3.exclude_person_hid TO cos;


CREATE OR REPLACE VIEW hotwire3."10_View/Email/Mailing_List_OptIns" AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.generation_view_name AS ro_generation_view_name, mailinglist.notes::text, pg_views.definition::text AS ro_generation_view_code, mailinglist.extra_addresses AS extra_addresses, ARRAY( SELECT mm_mailinglist_include_person.include_person_id
           FROM mm_mailinglist_include_person
          WHERE mm_mailinglist_include_person.mailinglist_id = mailinglist.id) AS include_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE hotwire3."10_View/Email/Mailing_List_OptIns" OWNER TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Email/Mailing_List_OptIns" TO cos;

-- Rule: hotwire3_view_mailing_lists_optins_upd ON hotwire."10_View/Email/Mailing_List_OptIns"

-- DROP RULE hotwire3_view_mailing_lists_optins_upd ON hotwire."10_View/Email/Mailing_List_OptIns";

CREATE OR REPLACE RULE hotwire3_view_mailing_lists_optins_upd AS
    ON UPDATE TO hotwire3."10_View/Email/Mailing_List_OptIns" DO INSTEAD ( SELECT fn_mm_array_update(new.include_person_id, old.include_person_id, 'mm_mailinglist_include_person'::character varying, 'mailinglist_id'::character varying, 'include_person_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE mailinglist SET name = new.name, notes = new.notes::varchar(500), extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

CREATE OR REPLACE VIEW hotwire3."10_View/Email/Mailing_List_OptOuts" AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.notes::text, mailinglist.generation_view_name AS ro_generation_view_name, pg_views.definition::text AS ro_generation_view_code, mailinglist.extra_addresses AS extra_addresses, ARRAY( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = mailinglist.id) AS exclude_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE hotwire3."10_View/Email/Mailing_List_OptOuts" OWNER TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Email/Mailing_List_OptOuts" TO cos;

CREATE OR REPLACE RULE hotwire3_view_mailing_lists_optouts_upd AS
    ON UPDATE TO hotwire3."10_View/Email/Mailing_List_OptOuts" DO INSTEAD ( SELECT fn_mm_array_update(new.exclude_person_id, old.exclude_person_id, 'mm_mailinglist_exclude_person'::character varying, 'mailinglist_id'::character varying, 'exclude_person_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE mailinglist SET name = new.name, notes = new.notes::varchar(500), extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Email/Mailing_List_OptOuts', 'mailinglist');
INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Email/Mailing_List_OptIns', 'mailinglist');
-- Installing 038_add_cabinet_view

-- fibre type hid
CREATE OR REPLACE VIEW hotwire3.fibre_type_hid AS 
 SELECT fibre_type_hid.fibre_type_id, fibre_type_hid.fibre_type_hid, fibre_type_hid.fibre_type_abbrev
    FROM fibre_type_hid;

ALTER TABLE hotwire3.fibre_type_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_type_hid TO public;

-- cabinet_hid
CREATE OR REPLACE VIEW hotwire3.cabinet_hid AS 
 SELECT cabinet.id AS cabinet_id, cabinet.name AS cabinet_hid
   FROM cabinet;

ALTER TABLE hotwire3.cabinet_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.cabinet_hid TO cos;

-- fibre_termination_hid
CREATE OR REPLACE VIEW hotwire3.fibre_termination_hid AS 
 SELECT fibre_termination.id AS fibre_termination_id, fibre_termination.name AS fibre_termination_hid
   FROM fibre_termination;

ALTER TABLE hotwire3.fibre_termination_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_termination_hid TO cos;

-- fibre_panel_a_hid
CREATE OR REPLACE VIEW hotwire3.fibre_panel_a_hid AS 
 SELECT fibre_panel.id AS fibre_panel_a_id, (cabinet.cabname::text || '.'::text) || fibre_panel.name::text AS fibre_panel_a_hid
   FROM fibre_panel
   JOIN cabinet ON fibre_panel.cabinet_id = cabinet.id;

ALTER TABLE hotwire3.fibre_panel_a_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_panel_a_hid TO public;

-- fibre_panel_b_hid
CREATE OR REPLACE VIEW hotwire3.fibre_panel_b_hid AS 
 SELECT fibre_panel.id AS fibre_panel_b_id, (cabinet.cabname::text || '.'::text) || fibre_panel.name::text AS fibre_panel_b_hid
   FROM fibre_panel
   JOIN cabinet ON fibre_panel.cabinet_id = cabinet.id;

ALTER TABLE hotwire3.fibre_panel_b_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_panel_b_hid TO public;

-- fibre_termination_a_hid
CREATE OR REPLACE VIEW hotwire3.fibre_termination_a_hid AS 
 SELECT fibre_termination.id AS fibre_termination_a_id, fibre_termination.name AS fibre_termination_a_hid
   FROM fibre_termination;

ALTER TABLE hotwire3.fibre_termination_a_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_termination_a_hid TO public;

-- fibre_termination_b_hid
CREATE OR REPLACE VIEW hotwire3.fibre_termination_b_hid AS 
 SELECT fibre_termination.id AS fibre_termination_b_id, fibre_termination.name AS fibre_termination_b_hid
   FROM fibre_termination;

ALTER TABLE hotwire3.fibre_termination_b_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_termination_b_hid TO public;

-- hotwire3.fibre_core_type_hid
CREATE OR REPLACE VIEW hotwire3.fibre_core_type_hid AS 
 SELECT fibre_core_type.id AS fibre_core_type_id, fibre_core_type.name AS fibre_core_type_hid
   FROM fibre_core_type;

ALTER TABLE hotwire3.fibre_core_type_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_core_type_hid TO public;

-- hotwire3.fibre_core_hid
CREATE OR REPLACE VIEW hotwire3.fibre_core_hid AS 
 SELECT fibre_core.id AS fibre_core_id, (((((((fibre_panel_a_hid.fibre_panel_a_hid || '.'::text) || fibre_core.label_a::text) || '-'::text) || fibre_panel_b_hid.fibre_panel_b_hid) || '.'::text) || fibre_core.label_b::text) || ' - '::text) || fibre_core_type_hid.fibre_core_type_hid::text AS fibre_core_hid
   FROM fibre_core
   JOIN hotwire3.fibre_core_type_hid USING (fibre_core_type_id)
   JOIN fibre_run ON fibre_run.id = fibre_core.fibre_run_id
   JOIN hotwire3.fibre_panel_a_hid ON fibre_run.panel_a_id = fibre_panel_a_hid.fibre_panel_a_id
   JOIN hotwire3.fibre_panel_b_hid ON fibre_run.panel_b_id = fibre_panel_b_hid.fibre_panel_b_id;

ALTER TABLE hotwire3.fibre_core_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_core_hid TO public;

-- hotwire3.fibre_run_hid
CREATE OR REPLACE VIEW hotwire3.fibre_run_hid AS 
 SELECT fibre_run.id AS fibre_run_id, ((((fibre_panel_a_hid.fibre_panel_a_hid || '-'::text) || fibre_panel_b_hid.fibre_panel_b_hid) || ' ['::text) || (( SELECT array_to_string(ARRAY( SELECT (count(*) || '/'::text) || fibre_type_hid.fibre_type_abbrev::text
                   FROM fibre_core
              JOIN fibre_core_type ON fibre_core.fibre_core_type_id = fibre_core_type.id
         JOIN fibre_type_hid USING (fibre_type_id)
        WHERE fibre_core.fibre_run_id = fibre_run.id
        GROUP BY fibre_type_hid.fibre_type_abbrev), ','::text) AS array_to_string))) || ']'::text AS fibre_run_hid
   FROM fibre_run
   JOIN hotwire3.fibre_termination_a_hid ON fibre_termination_a_hid.fibre_termination_a_id = fibre_run.term_a_id
   JOIN hotwire3.fibre_termination_b_hid ON fibre_termination_b_hid.fibre_termination_b_id = fibre_run.term_b_id
   JOIN hotwire3.fibre_panel_a_hid ON fibre_panel_a_hid.fibre_panel_a_id = fibre_run.panel_a_id
   JOIN hotwire3.fibre_panel_b_hid ON fibre_panel_b_hid.fibre_panel_b_id = fibre_run.panel_b_id;

ALTER TABLE hotwire3.fibre_run_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fibre_run_hid TO public;

-- hotwire3.fibre_test_type_hid
CREATE OR REPLACE VIEW hotwire3.fibre_test_type_hid AS 
 SELECT fibre_test_type.id AS fibre_test_type_id, fibre_test_type.test::text || COALESCE(fibre_test_type.gbs || 'GB/s'::text, ''::text) AS fibre_test_type_hid
    FROM fibre_test_type;

ALTER TABLE hotwire3.fibre_test_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.fibre_test_type_hid TO cos;

-- Network/Fibres/_FibreRuns_ro
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/_FibreRuns_ro" AS 
 SELECT fibre_run.id, fibre_panel.cabinet_id, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_run.fibre_type, fibre_run.quantity, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id
   FROM fibre_panel
   JOIN fibre_run ON fibre_panel.id = fibre_run.panel_a_id OR fibre_panel.id = fibre_run.panel_b_id;

ALTER TABLE hotwire3."10_View/Network/Fibres/_FibreRuns_ro" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Network/Fibres/_FibreRuns_ro" TO cos;

-- Network/Fibre_Panel
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibre_Panels" AS 
 SELECT fibre_panel.id, fibre_panel.cabinet_id, fibre_panel.name, fibre_panel.termination_type_id AS fibre_termination_id
   FROM fibre_panel;

ALTER TABLE hotwire3."10_View/Network/Fibre_Panels" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Fibre_Panels" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre_Panels_del" AS
    ON DELETE TO hotwire3."10_View/Network/Fibre_Panels" DO INSTEAD  DELETE FROM fibre_panel
  WHERE fibre_panel.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre_Panels_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Fibre_Panels" DO INSTEAD  INSERT INTO fibre_panel (cabinet_id, name, termination_type_id) 
  VALUES (new.cabinet_id, new.name, new.fibre_termination_id)
  RETURNING fibre_panel.id, fibre_panel.cabinet_id, fibre_panel.name, fibre_panel.termination_type_id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre_Panels_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Fibre_Panels" DO INSTEAD  UPDATE fibre_panel SET id = new.id, cabinet_id = new.cabinet_id, name = new.name, termination_type_id = new.fibre_termination_id
  WHERE fibre_panel.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Fibre_Panels', 'fibre_panel');

-- 10_View/Network/Fibres/_FibreCore_ro
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/_FibreCore_ro" AS 
 SELECT fibre_core_hid.fibre_core_id AS id, fibre_core.fibre_run_id, fibre_core_hid.fibre_core_hid, fibre_core.label_a, fibre_core.label_b, ( SELECT fibre_test.fibre_test_time
           FROM fibre_test
          WHERE fibre_test.fibre_core_id = fibre_core.id
          ORDER BY fibre_test.fibre_test_time DESC
         LIMIT 1) AS fibre_test_time, ( SELECT fibre_test.fibre_test_type_id
           FROM fibre_test
          WHERE fibre_test.fibre_core_id = fibre_core.id
          ORDER BY fibre_test.fibre_test_time DESC
         LIMIT 1) AS fibre_test_type_id, ( SELECT fibre_test.passed
           FROM fibre_test
          WHERE fibre_test.fibre_core_id = fibre_core.id
          ORDER BY fibre_test.fibre_test_time DESC
         LIMIT 1) AS passed, fibre_core.illuminate
   FROM fibre_core
   JOIN hotwire3.fibre_core_hid ON fibre_core_hid.fibre_core_id = fibre_core.id;

ALTER TABLE hotwire3."10_View/Network/Fibres/_FibreCore_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/Fibres/_FibreCore_ro" TO cos;

-- Network/Fibres/10_Fibre_run
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/10_Fibre_Runs" AS 
 SELECT fibre_run.id, fibre_run.fibre_type, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id, fibre_run.quantity, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_run.notes, hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_ro'::character varying, 'fibre_run_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Cores"
   FROM fibre_run;

ALTER TABLE hotwire3."10_View/Network/Fibres/10_Fibre_Runs" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Fibres/10_Fibre_Runs" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibres/10_Fibre_Runs_del" AS
    ON DELETE TO hotwire3."10_View/Network/Fibres/10_Fibre_Runs" DO INSTEAD  DELETE FROM fibre_run
  WHERE fibre_run.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibres/10_Fibre_Runs_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Fibres/10_Fibre_Runs" DO INSTEAD  INSERT INTO fibre_run (fibre_type, term_a_id, term_b_id, quantity, panel_a_id, panel_b_id, notes) 
  VALUES (new.fibre_type, new.fibre_termination_a_id, new.fibre_termination_b_id, new.quantity, new.fibre_panel_a_id, new.fibre_panel_b_id, new.notes) RETURNING fibre_run.id, fibre_run.fibre_type, fibre_run.term_a_id, fibre_run.term_b_id, fibre_run.quantity, fibre_run.panel_a_id, fibre_run.panel_b_id, fibre_run.notes, hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_ro'::character varying, 'fibre_run_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying);

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibres/10_Fibre_Runs_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Fibres/10_Fibre_Runs" DO INSTEAD  UPDATE fibre_run SET fibre_type = new.fibre_type, term_a_id = new.fibre_termination_a_id, term_b_id = new.fibre_termination_b_id, quantity = new.quantity, panel_a_id = new.fibre_panel_a_id, panel_b_id = new.fibre_panel_b_id, notes = new.notes
  WHERE fibre_run.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Fibres/10_Fibre_Runs', 'fibre_run');


-- 10_View/Network/Fibres/_FibreCore_for_Cabinet_ro
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/_FibreCore_for_Cabinet_ro" AS 
 SELECT fibre_core.id, fibre_panel.cabinet_id, fibre_core.id AS fibre_core_id, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_core.fibre_core_type_id, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id, fibre_run.notes, ( SELECT fibre_test.passed
           FROM fibre_test
          WHERE fibre_test.fibre_core_id = fibre_core.id
          ORDER BY fibre_test.fibre_test_time DESC
         LIMIT 1) AS passed, fibre_core.illuminate
   FROM fibre_panel
   JOIN fibre_run ON fibre_panel.id = fibre_run.panel_a_id OR fibre_panel.id = fibre_run.panel_b_id
   JOIN fibre_core ON fibre_core.fibre_run_id = fibre_run.id;

ALTER TABLE hotwire3."10_View/Network/Fibres/_FibreCore_for_Cabinet_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/Fibres/_FibreCore_for_Cabinet_ro" TO cos;

-- 10_View/Network/Fibres/15_Fibre_Cores
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/15_Fibre_Cores" AS 
 SELECT fibre_core.id, fibre_core.fibre_run_id, fibre_core.fibre_core_type_id, fibre_core.label_a, fibre_core.label_b, fibre_core.illuminate, hotwire3.to_hwsubviewb('10_View/Network/Fibres/Tests'::character varying, 'fibre_core_id'::character varying, '10_View/Network/Fibres/Tests'::character varying, NULL::character varying, NULL::character varying) AS "Tests"
   FROM fibre_core;

ALTER TABLE hotwire3."10_View/Network/Fibres/15_Fibre_Cores" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Fibres/15_Fibre_Cores" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_Network/Fibres/15_Fibre_Core_del" AS
    ON DELETE TO hotwire3."10_View/Network/Fibres/15_Fibre_Cores" DO INSTEAD  DELETE FROM fibre_core
  WHERE fibre_core.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_Network/Fibres/15_Fibre_Core_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Fibres/15_Fibre_Cores" DO INSTEAD  INSERT INTO fibre_core (fibre_run_id, fibre_core_type_id, label_a, label_b, illuminate) 
  VALUES (new.fibre_run_id, new.fibre_core_type_id, new.label_a, new.label_b, new.illuminate)
  RETURNING fibre_core.id, fibre_core.fibre_run_id, fibre_core.fibre_core_type_id, fibre_core.label_a, fibre_core.label_b, fibre_core.illuminate, hotwire3.to_hwsubviewb('10_View/Network/Fibres/Tests'::character varying, 'fibre_core_id'::character varying, '10_View/Network/Fibres/Tests'::character varying, NULL::character varying, NULL::character varying) AS "Tests";

CREATE OR REPLACE RULE "hotwire3_10_Network/Fibres/15_Fibre_Core_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Fibres/15_Fibre_Cores" DO INSTEAD  UPDATE fibre_core SET id = new.id, fibre_run_id = new.fibre_run_id, fibre_core_type_id = new.fibre_core_type_id, label_a = new.label_a, label_b = new.label_b, illuminate = new.illuminate
  WHERE fibre_core.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Fibres/15_Fibre_Cores', 'fibre_core');

-- Network/Cabinet
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Cabinets" AS 
 SELECT cabinet.id, cabinet.name, cabinet.email_local_part, cabinet.notes, cabinet.room_id, cabinet.cabno, cabinet.in_use, cabinet.depth, cabinet.fibre_u, cabinet.patch_u, cabinet.pots_u, cabinet.other_u, cabinet.height_u, cabinet.ports, cabinet.max_ports, cabinet.sw48, cabinet.sw24, cabinet.cabname, hotwire3.to_hwsubviewb('10_View/Network/Fibre_Panels'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panels'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panel", hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/10_Fibre_Runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs", hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores"
   FROM cabinet;

ALTER TABLE hotwire3."10_View/Network/Cabinets" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Cabinets" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_del" AS
    ON DELETE TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  DELETE FROM cabinet
  WHERE cabinet.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  INSERT INTO cabinet (name, email_local_part, notes, room_id, cabno, in_use, depth, fibre_u, patch_u, pots_u, other_u, height_u, ports, max_ports, sw48, sw24, cabname) 
  VALUES (new.name, new.email_local_part, new.notes, new.room_id, new.cabno, new.in_use, new.depth, new.fibre_u, new.patch_u, new.pots_u, new.other_u, new.height_u, new.ports, new.max_ports, new.sw48, new.sw24, new.cabname)
  RETURNING cabinet.id, cabinet.name, cabinet.email_local_part, cabinet.notes, cabinet.room_id, cabinet.cabno, cabinet.in_use, cabinet.depth, cabinet.fibre_u, cabinet.patch_u, cabinet.pots_u, cabinet.other_u, cabinet.height_u, cabinet.ports, cabinet.max_ports, cabinet.sw48, cabinet.sw24, cabinet.cabname, hotwire3.to_hwsubviewb('10_View/Network/Fibre_Panels'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panels'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panels", hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/10_Fibre_runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs", hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores";

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  UPDATE cabinet SET id = new.id, name = new.name, email_local_part = new.email_local_part, notes = new.notes, room_id = new.room_id, cabno = new.cabno, in_use = new.in_use, depth = new.depth, fibre_u = new.fibre_u, pots_u = new.pots_u, other_u = new.other_u, height_u = new.height_u, ports = new.ports, max_ports = new.max_ports, sw48 = new.sw48, sw24 = new.sw24, patch_u = new.patch_u, cabname = new.cabname
  WHERE cabinet.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Cabinets', 'cabinet');

-- 10_View/Network/Fibres/Tests
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Fibres/Tests" AS 
 SELECT fibre_test.fibre_core_id, fibre_test.fibre_test_time, fibre_test.fibre_test_type_id, fibre_test.fibre_test_result, fibre_test.passed, fibre_test.id
   FROM fibre_test;

ALTER TABLE hotwire3."10_View/Network/Fibres/Tests" OWNER TO dev;
GRANT SELECT,UPDATE,INSERT,DELETE ON hotwire3."10_View/Network/Fibres/Tests" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre/Tests_del" AS
    ON DELETE TO hotwire3."10_View/Network/Fibres/Tests" DO INSTEAD  DELETE FROM fibre_test
  WHERE fibre_test.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre/Tests_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Fibres/Tests" DO INSTEAD  INSERT INTO fibre_test (fibre_core_id, fibre_test_time, fibre_test_type_id, fibre_test_result, passed) 
  VALUES (new.fibre_core_id, new.fibre_test_time, new.fibre_test_type_id, new.fibre_test_result, new.passed)
  RETURNING fibre_test.fibre_core_id, fibre_test.fibre_test_time, fibre_test.fibre_test_type_id, fibre_test.fibre_test_result, fibre_test.passed, fibre_test.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Fibre/Tests_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Fibres/Tests" DO INSTEAD  UPDATE fibre_test SET fibre_core_id = new.fibre_core_id, fibre_test_time = new.fibre_test_time, fibre_test_type_id = new.fibre_test_type_id, fibre_test_result = new.fibre_test_result, passed = new.passed, id = new.id
  WHERE fibre_test.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ('10_View/Network/Fibres/Tests', 'fibre_test');
-- Installing 039_add_network_gbic_view

CREATE VIEW hotwire3.network_electrical_connection_hid AS
SELECT network_electrical_connection_hid.network_electrical_connection_id, network_electrical_connection_hid.network_electrical_connection_hid
FROM network_electrical_connection_hid;

ALTER TABLE hotwire3.network_electrical_connection_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.network_electrical_connection_hid TO cos;

CREATE VIEW hotwire3."10_View/Network/GBICs" AS 
 SELECT network_gbic.id, network_gbic.name, network_gbic.description, network_gbic.network_electrical_connection_id, network_gbic.fibre_type_id
   FROM network_gbic;

ALTER TABLE hotwire3."10_View/Network/GBICs" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Network/GBICs" TO cos;

CREATE RULE "hotwire3_10_View/Network/GBICs_del" AS
    ON DELETE TO hotwire3."10_View/Network/GBICs" DO INSTEAD  DELETE FROM network_gbic
  WHERE network_gbic.id = old.id;

CREATE RULE "hotwire3_10_View/Network/GBICs_ins" AS
    ON INSERT TO hotwire3."10_View/Network/GBICs" DO INSTEAD  INSERT INTO network_gbic (name, description, network_electrical_connection_id, fibre_type_id) 
  VALUES (new.name, new.description, new.network_electrical_connection_id, new.fibre_type_id)
  RETURNING network_gbic.id, network_gbic.name, network_gbic.description, network_gbic.network_electrical_connection_id, network_gbic.fibre_type_id;

CREATE RULE "hotwire3_10_View/Network/GBICs_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/GBICs" DO INSTEAD  UPDATE network_gbic SET id = new.id, name = new.name, description = new.description, network_electrical_connection_id = new.network_electrical_connection_id, fibre_type_id = new.fibre_type_id
  WHERE network_gbic.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/GBICs', 'network_gbic');
-- Installing 040_add_mac_to_vlan_view

CREATE VIEW hotwire3.vlan_vid_hid AS
SELECT vlan.vid AS vlan_vid_id, ((vlan.name::text || ' ['::text) || vlan.description::text) || ']'::text AS vlan_vid_hid
   FROM vlan;
ALTER TABLE hotwire3.vlan_vid_hid OWNER TO dev;
GRANT SELECT ON hotwire3.vlan_vid_hid TO cos;

CREATE VIEW hotwire3.infraction_type_hid AS
SELECT infraction_type_hid.infraction_type_id, infraction_type_hid.infraction_type_hid
   FROM infraction_type_hid;
ALTER TABLE hotwire3.infraction_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.infraction_type_hid TO cos;

CREATE VIEW hotwire3."10_View/Network/_vlanchange_ro" AS
SELECT a.id, a._shmacid, a.infraction_type_id, a.ticket, a.notes, a.actor, a.time_stamp, a.old_vid, a.new_vid
   FROM ( SELECT network_vlan_change.id, network_vlan_change.shadow_mac_to_vlan_id AS _shmacid, network_vlan_change.infraction_type_id, network_vlan_change.ticket, network_vlan_change.notes, network_vlan_change.actor, network_vlan_change."timestamp" AS time_stamp, network_vlan_change.old_vid, network_vlan_change.new_vid
FROM network_vlan_change) a
ORDER BY a.time_stamp DESC;
ALTER VIEW hotwire3."10_View/Network/_vlanchange_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/_vlanchange_ro" TO cos;


CREATE OR REPLACE VIEW hotwire3."10_View/Network/MAC_to_VLAN" AS 
 SELECT a.id, a.mac, a.vlan_vid_id, a.infraction_type_id, a.ticket, a."Infraction notes", a.last_change, a."Logged change"
   FROM ( SELECT shadow_mac_to_vlan.id, shadow_mac_to_vlan.mac, shadow_mac_to_vlan.vid AS vlan_vid_id, NULL::integer AS infraction_type_id, NULL::integer AS ticket, NULL::character varying AS "Infraction notes", ( SELECT (((((to_char(network_vlan_change."timestamp", 'YYYY-MM-DD HH24:MI:SS'::text) || ' : '::text) || infraction_type_hid.infraction_type_hid::text) || ' ['::text) || network_vlan_change.actor::text) || ']'::text)::character varying AS history
                   FROM network_vlan_change
              JOIN infraction_type_hid USING (infraction_type_id)
             WHERE network_vlan_change.mac = shadow_mac_to_vlan.mac
             ORDER BY network_vlan_change."timestamp" DESC
            LIMIT 1) AS last_change, hotwire3.to_hwsubviewb('10_View/Network/_vlanchange_ro'::character varying, '_shmacid'::character varying, NULL::character varying, NULL::character varying, NULL::character varying) AS "Logged change"
           FROM shadow_mac_to_vlan) a
  ORDER BY a.last_change DESC NULLS LAST;

ALTER TABLE hotwire3."10_View/Network/MAC_to_VLAN" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/MAC_to_VLAN" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_del" AS
    ON DELETE TO hotwire3."10_View/Network/MAC_to_VLAN" DO INSTEAD  DELETE FROM shadow_mac_to_vlan
  WHERE shadow_mac_to_vlan.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_ins" AS
    ON INSERT TO hotwire3."10_View/Network/MAC_to_VLAN" DO INSTEAD  INSERT INTO shadow_mac_to_vlan (mac, vid) 
  VALUES (new.mac, new.vlan_vid_id)
  RETURNING shadow_mac_to_vlan.id, shadow_mac_to_vlan.mac, shadow_mac_to_vlan.vid, NULL::integer AS int4, NULL::integer AS int4, NULL::character varying AS "varchar", NULL::character varying AS "varchar", NULL::_hwsubviewb AS _hwsubviewb;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/MAC_to_VLAN" DO INSTEAD ( INSERT INTO network_vlan_change (shadow_mac_to_vlan_id, mac, infraction_type_id, ticket, notes, old_vid, new_vid) 
  VALUES (old.id, new.mac, new.infraction_type_id, new.ticket, new."Infraction notes", old.vlan_vid_id, new.vlan_vid_id);
 UPDATE shadow_mac_to_vlan SET id = new.id, mac = new.mac, vid = new.vlan_vid_id
  WHERE shadow_mac_to_vlan.id = old.id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/MAC_to_VLAN', 'shadow_mac_to_vlan');
-- Installing 041_add_subnet_view

CREATE VIEW hotwire3.vlan_hid AS
SELECT vlan.id AS vlan_id, vlan.vid || COALESCE((' ('::text || vlan.name::text) || ')'::text, ''::text) AS vlan_hid
FROM vlan;
ALTER TABLE hotwire3.vlan_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.vlan_hid TO public;

CREATE VIEW hotwire3.domain_for_dns_server_hid AS
SELECT dns_domain_hid.dns_domain_id AS domain_for_dns_server_id, dns_domain_hid.dns_domain_hid AS domain_for_dns_server_hid
   FROM hotwire3.dns_domain_hid;
ALTER VIEW hotwire3.domain_for_dns_server_hid OWNER TO dev;
GRANT SELECT ON hotwire3.domain_for_dns_server_hid TO PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Subnets" AS 
 SELECT subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id AS domain_for_dns_server_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1 AS ro_live_dns1, subnet.dns2 AS ro_live_dns2, subnet.dns3 AS ro_live_dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes
   FROM subnet;

ALTER TABLE hotwire3."10_View/Network/Subnets" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Network/Subnets" TO cos;

CREATE OR REPLACE RULE hotwire3_view_subnet_ins AS
    ON INSERT TO hotwire3."10_View/Network/Subnets" DO INSTEAD  INSERT INTO subnet (network_address, router, domain_name, dns_domain_id, preferred_dns1, preferred_dns2, preferred_dns3, provide_dhcp_service, vlan_id, notes) 
  VALUES (new.network_address, new.router, new.domain_name, new.domain_for_dns_server_id, new.preferred_dns1, new.preferred_dns2, new.preferred_dns3, new.provide_dhcp_service, new.vlan_id, new.notes)
  RETURNING subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1, subnet.dns2, subnet.dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes;

CREATE OR REPLACE RULE hotwire3_view_subnet_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Subnets" DO INSTEAD  UPDATE subnet SET network_address = new.network_address, router = new.router, domain_name = new.domain_name, dns_domain_id = new.domain_for_dns_server_id, preferred_dns1 = new.preferred_dns1, preferred_dns2 = new.preferred_dns2, preferred_dns3 = new.preferred_dns3, provide_dhcp_service = new.provide_dhcp_service, vlan_id = new.vlan_id, notes = new.notes
  WHERE subnet.id = old.id
  RETURNING subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1, subnet.dns2, subnet.dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes;

INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Network/Subnets', 'subnet' );
-- Installing 042_add_switch_view

CREATE VIEW hotwire3.switch_port_goal_hid AS
SELECT switch_port_config_goal.id AS switch_port_goal_id, switch_port_config_goal.name AS switch_port_goal_hid
FROM switch_port_config_goal
WHERE switch_port_config_goal.goal_class IS NULL;
ALTER VIEW hotwire3.switch_port_goal_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_port_goal_hid TO cos;

CREATE VIEW hotwire3.switch_auth_hid AS
SELECT switch_port_config_goal.id AS switch_auth_id, switch_port_config_goal.name AS switch_auth_hid
FROM switch_port_config_goal
WHERE switch_port_config_goal.goal_class = 1;
ALTER VIEW hotwire3.switch_auth_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_auth_hid TO cos;

CREATE VIEW hotwire3.speed_hid AS
SELECT switch_port_config_goal.id AS speed_id, switch_port_config_goal.name AS speed_hid
FROM switch_port_config_goal
WHERE switch_port_config_goal.goal_class = 2;
ALTER VIEW hotwire3.speed_hid OWNER TO dev;
GRANT SELECT ON hotwire3.speed_hid TO cos;

CREATE VIEW hotwire3.patch_panel_hid AS
SELECT patch_panel.id AS patch_panel_id, (cabinet.name::text || ' - '::text) || patch_panel.name::text AS patch_panel_hid
FROM patch_panel
LEFT JOIN cabinet ON patch_panel.cabinet_id = cabinet.id;
ALTER VIEW hotwire3.patch_panel_hid OWNER TO dev;
GRANT SELECT ON hotwire3.patch_panel_hid TO cos;


CREATE VIEW hotwire3.socket_hid AS
SELECT socket.id AS socket_id, (patch_panel_hid.patch_panel_hid || ' - '::text) || socket.panel_label::text AS socket_hid
FROM socket
JOIN hotwire3.patch_panel_hid USING (patch_panel_id)
ORDER BY (patch_panel_hid.patch_panel_hid || ' - '::text) || socket.panel_label::text;
ALTER VIEW hotwire3.socket_hid OWNER TO dev;
GRANT SELECT ON hotwire3.socket_hid TO cos;


CREATE VIEW hotwire3.switch_hid AS
SELECT switch.id AS switch_id, hardware.name::text || COALESCE((' ('::text || switchstack.name::text) || ')'::text, ''::text) AS switch_hid
FROM switch
LEFT JOIN switchstack ON switch.switchstack_id = switchstack.id
JOIN hardware ON switch.hardware_id = hardware.id;
ALTER VIEW hotwire3.switch_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_hid TO cos;


CREATE VIEW hotwire3.switchstack_hid AS
SELECT switchstack.id AS switchstack_id, switchstack.name AS switchstack_hid
FROM switchstack;
ALTER VIEW hotwire3.switchstack_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switchstack_hid TO cos;

CREATE VIEW hotwire3.switch_config_goal_hid AS
SELECT switch_config_goal.id AS switch_config_goal_id, switch_config_goal.name AS switch_config_goal_hid
FROM switch_config_goal
ORDER BY switch_config_goal.name;
ALTER VIEW hotwire3.switch_config_goal_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_config_goal_hid TO cos;

CREATE VIEW hotwire3."10_View/Network/Switches/_Switchport_ro" AS
SELECT switchport.id, switchport.switch_id, switchport.name, switchport.socket_id, switchport.ifacename, switch_auth_hid.switch_auth_hid, speed_hid.speed_hid
   FROM switchport
   LEFT JOIN hotwire3.switch_auth_hid USING (switch_auth_id)
   LEFT JOIN hotwire3.speed_hid USING (speed_id)
  ORDER BY 
CASE
    WHEN switchport.name::text ~ '^[A-Za-z]+'::text THEN 10 * ascii(substr(switchport.name::text, 1, 1)) + regexp_replace(switchport.name::text, '[A-Za-z]+'::text, ''::text)::integer
    ELSE switchport.name::integer
END;
ALTER VIEW hotwire3."10_View/Network/Switches/_Switchport_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/Switches/_Switchport_ro" TO cos;

CREATE VIEW hotwire3."10_View/Network/Switches/_switch_config_subview" AS
SELECT switch.id, 'Config for switch - click here to show'::character varying AS "Link to config"
FROM switch;

ALTER TABLE hotwire3."10_View/Network/Switches/_switch_config_subview" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switch_config_subview" TO cos;

CREATE VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro" AS
SELECT switch.id, switch.id as switch_id, array_to_string(ARRAY( SELECT _switch_config_b(switch.id) AS _switch_config), E'\n')::text AS ro_config
FROM switch;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" TO cos;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/Network/Switches/Switch_Configs_ro','switch');

CREATE VIEW hotwire3."10_View/Network/Switches/Switch_Ports" AS
SELECT switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id FROM mm_switch_config_switch_port WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id
FROM switchport;
ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Ports" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Network/Switches/Switch_Ports" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  DELETE FROM switchport
      WHERE switchport.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  INSERT INTO switchport (name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id) 
      VALUES (new.name, new.socket_id, new.switch_id, new.ifacename, new.disabled, new.speed_id, new.switch_auth_id)
        RETURNING switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
                   FROM mm_switch_config_switch_port
                             WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD ( UPDATE switchport SET id = new.id, name = new.name, socket_id = new.socket_id, switch_id = new.switch_id, ifacename = new.ifacename, disabled = new.disabled, speed_id = new.speed_id, switch_auth_id = new.switch_auth_id
      WHERE switchport.id = old.id;
       SELECT fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
);


INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Network/Switches/Switch_Ports', 'switchport');

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switches" AS 
 SELECT switch.id, hardware.name::character varying(40) AS name, switch.switchstack_id, hardware.id AS _hardware_id, system_image.id AS _system_image_id, hardware.manufacturer::character varying(60) AS manufacturer, hardware.hardware_type_id, system_image.wired_mac_1, switch.switch_model_id, switch.ignore_config, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, hardware.serial_number::character varying(40) AS serial_number, hardware.asset_tag::character varying(40) AS asset_tag, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id, hardware.owner_id, system_image.research_group_id, hardware.comments, ARRAY( SELECT mm_switch_goal_applies_to_switch.switch_config_goal_id
           FROM mm_switch_goal_applies_to_switch
          WHERE mm_switch_goal_applies_to_switch.switch_id = switch.id) AS switch_config_goal_id, switch.extraconfig, hotwire3.to_hwsubviewb('10_View/Network/Switches/_Switchport_ro'::character varying, 'switch_id'::character varying, '10_View/Network/Switches/Switch_Ports'::character varying, NULL::character varying, NULL::character varying) AS "Ports", hotwire3.to_hwsubviewb('10_View/Network/Switches/_switch_config_subview'::character varying, 'id'::character varying, '10_View/Network/Switches/Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config", ('<a href="http://netdisco2.ch.private.cam.ac.uk:5000/search?tab=device&ip='::text || (( SELECT ip_address.ip
           FROM mm_system_image_ip_address
      JOIN ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
     WHERE mm_system_image_ip_address.system_image_id = system_image.id
    LIMIT 1))) || '"> Netdisco</a>'::text AS "Netdisco view_html"
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id
   JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Network/Switches/Switches" OWNER TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switches" TO cos;

CREATE OR REPLACE RULE hotwire3_view_network_switch_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switches" DO INSTEAD  DELETE FROM switch
  WHERE switch.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_network_switch_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switches" DO INSTEAD ( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id, comments = new.comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1
  WHERE system_image.id = old._system_image_id;
 SELECT fn_mm_array_update(new.switch_config_goal_id::bigint[], old.switch_config_goal_id::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update;
 UPDATE switch SET ignore_config = new.ignore_config, switchstack_id = new.switchstack_id, switch_model_id = new.switch_model_id, extraconfig = new.extraconfig
  WHERE switch.id = old.id;
);


INSERT INTO hotwire3._primary_table ( view_name, primary_table) VALUES ( '10_View/Network/Switches/Switches', 'switch');
-- Installing 043_add_switch_config_fragments_view

CREATE VIEW hotwire3."10_View/Network/Switches/Switch_Config_Fragments" AS 
 SELECT switch_config_fragment.id, switch_config_fragment.name, switch_config_fragment.config, switch_config_fragment.switch_config_goal_id, ARRAY( SELECT mm_switch_config_fragment_switch_model.switch_model_id
           FROM mm_switch_config_fragment_switch_model
          WHERE mm_switch_config_fragment_switch_model.switch_config_fragment_id = switch_config_fragment.id) AS switch_model_id
   FROM switch_config_fragment
  ORDER BY switch_config_fragment.name;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Config_Fragments" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Config_Fragments" TO cos;

CREATE FUNCTION hotwire3.switch_config_fragments_ins() RETURNS TRIGGER AS
$$
  
  BEGIN

  NEW.id := nextval('switch_config_fragments_id_seq');

  INSERT INTO switch_config_fragment 
        ( id, name, config, switch_config_goal_id ) 
        VALUES 
        ( NEW.id, NEW.name, NEW.config, NEW.switch_config_goal_id );

  PERFORM fn_mm_array_update(new.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, new.id) ;

  RETURN NEW;
  END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.switch_config_fragments_ins() OWNER TO dev;

CREATE TRIGGER switch_config_fragments_ins INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments" FOR EACH ROW EXECUTE PROCEDURE hotwire3.switch_config_fragments_ins() ;

CREATE RULE del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Config_Fragments" DO INSTEAD  DELETE FROM switch_config_fragment
  WHERE switch_config_fragment.id = old.id;

CREATE RULE upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Config_Fragments" DO INSTEAD ( UPDATE switch_config_fragment SET name = new.name, config = new.config, switch_config_goal_id = new.switch_config_goal_id
  WHERE switch_config_fragment.id = old.id;
 SELECT fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE switch_config_fragment SET id = switch_config_fragment.id
  WHERE false
  RETURNING new.id, new.name, new.config, new.switch_config_goal_id, new.switch_model_id;
);

INSERT INTO hotwire3._primary_table (view_name, primary_table ) VALUES ('10_View/Network/Switches/Switch_Config_Fragments', 'switch_config_fragment');
-- Installing 044_add_switch_models_view

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Models" AS 
SELECT switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports
FROM switch_model;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Models" OWNER TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE hotwire3."10_View/Network/Switches/Switch_Models" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Models_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Models" DO INSTEAD  DELETE FROM switch_model
  WHERE switch_model.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Models_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Models" DO INSTEAD  INSERT INTO switch_model (model, description, ports, uplink_ports) 
  VALUES (new.model, new.description, new.ports, new.uplink_ports)
  RETURNING switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Models_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Models" DO INSTEAD  UPDATE switch_model SET id = new.id, model = new.model, description = new.description, ports = new.ports, uplink_ports = new.uplink_ports
  WHERE switch_model.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Switches/Switch_Models', 'switch_model');
-- Installing 045_add_switch_config_goals_view

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Config_Goals" AS 
 SELECT switch_config_goal.id, switch_config_goal.name, ARRAY( SELECT mm_switch_goal_applies_to_switch.switch_id
           FROM mm_switch_goal_applies_to_switch
          WHERE mm_switch_goal_applies_to_switch.switch_config_goal_id = switch_config_goal.id) AS switch_id
   FROM switch_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Config_Goals" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Config_Goals" TO cos;

CREATE FUNCTION hotwire3.switch_config_goals_ins() RETURNS TRIGGER AS
$$
begin
  new.id := nextval('swtich_config_goal_id_seq');
  INSERT INTO switch_config_goal ( id, name ) VALUES ( new.id, new.name );
  PERFORM fn_mm_array_update(new.switch_id,array[]::bigint[],'mm_switch_goal_applies_to_switch'::varchar,'switch_config_goal_id'::varchar,'switch_id'::varchar,new.id);
  return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.switch_config_goals_ins() OWNER TO dev;

CREATE OR REPLACE RULE hotwire3_view_network_switch_config_goal_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Config_Goals" DO INSTEAD  DELETE FROM switch_config_goal
  WHERE switch_config_goal.id = old.id;

CREATE TRIGGER hotwire3_view_network_switch_config_goal_ins INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/Switch_Config_Goals" FOR EACH ROW EXECUTE PROCEDURE hotwire3.switch_config_goals_ins() ;

CREATE OR REPLACE RULE hotwire3_view_network_switch_config_goal_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Config_Goals" DO INSTEAD ( UPDATE switch_config_goal SET name = new.name
  WHERE switch_config_goal.id = old.id;
 SELECT fn_mm_array_update(new.switch_id::bigint[], old.switch_id::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_config_goal_id'::character varying, 'switch_id'::character varying, old.id::bigint) AS fn_mm_array_update2;
);

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/Network/Switches/Switch_Config_Goals','switch_config_goal');
-- Installing 046_add_switch_port_config_fragments_view

CREATE VIEW hotwire3.switch_port_config_goal_hid AS
SELECT switch_port_config_goal.id AS switch_port_config_goal_id, switch_port_config_goal.name AS switch_port_config_goal_hid
FROM switch_port_config_goal;
ALTER VIEW hotwire3.switch_port_config_goal_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_port_config_goal_hid TO public;

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" AS 
 SELECT switch_port_config_fragment.id, switch_port_config_fragment.switch_port_config_goal_id, switch_port_config_fragment.config, ARRAY( SELECT mm_switch_port_config_fragment_switch_model.switch_model_id
           FROM mm_switch_port_config_fragment_switch_model
          WHERE mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id) AS switch_model_id
   FROM switch_port_config_fragment;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" TO cos;

CREATE FUNCTION hotwire3.network_switch_port_config_fragment_ins() RETURNS TRIGGER AS
$$
begin
  new.id := nextval('switch_port_config_fragment_id_seq');
  INSERT INTO switch_port_config_fragment ( id, config, switch_port_config_goal_id ) VALUES ( new.id, new.config, new.switch_port_config_goal_id );
  PERFORM fn_mm_array_update(new.switch_model_id, array[]::bigint[], 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, new.id::bigint);
  RETURN new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.network_switch_port_config_fragment_ins() OWNER TO dev;

CREATE TRIGGER hotwire3_view_network_switch_port_config_fragment_ins INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" FOR EACH ROW EXECUTE PROCEDURE hotwire3.network_switch_port_config_fragment_ins();

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_fragment_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" DO INSTEAD  DELETE FROM switch_port_config_fragment
  WHERE switch_port_config_fragment.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_fragment_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" DO INSTEAD ( UPDATE switch_port_config_fragment SET switch_port_config_goal_id = new.switch_port_config_goal_id, config = new.config
  WHERE switch_port_config_fragment.id = old.id;
 SELECT fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id::bigint) AS fn_mm_array_update2;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Switches/Switch_Port_Config_Fragments', 'switch_port_config_fragment');
-- Installing 047_add_switch_port_config_goals

CREATE VIEW hotwire3.goal_class_hid AS
SELECT goal_class_hid.id AS goal_class_id, goal_class_hid.hid AS goal_class_hid
   FROM goal_class_hid;
ALTER TABLE hotwire3.goal_class_hid OWNER TO dev;
GRANT SELECT ON TABLE  hotwire3.goal_class_hid TO public;

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" AS 
 SELECT switch_port_config_goal.id, switch_port_config_goal.name, switch_port_config_goal.goal_class AS goal_class_id
   FROM switch_port_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" OWNER TO dev;
GRANT SELECT,UPDATE,INSERT,DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" TO cos;

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_goal_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" DO INSTEAD  DELETE FROM switch_port_config_goal
  WHERE switch_port_config_goal.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_goal_ins AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" DO INSTEAD  INSERT INTO switch_port_config_goal (name, goal_class) 
  VALUES (new.name, new.goal_class_id) RETURNING switch_port_config_goal.id, switch_port_config_goal.name, switch_port_config_goal.goal_class;

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_goal_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" DO INSTEAD  UPDATE switch_port_config_goal SET name = new.name, goal_class = new.goal_class_id
  WHERE switch_port_config_goal.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Switches/Switch_Port_Config_Goals', 'switch_port_config_goal');
-- Installing 048_add_vlan_view

CREATE OR REPLACE VIEW hotwire3."10_View/Network/VLANs" AS 
 SELECT vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid
   FROM vlan
  ORDER BY vlan.vid;

ALTER TABLE hotwire3."10_View/Network/VLANs" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/VLANs" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/VLANs" TO cos;

CREATE OR REPLACE RULE hotwire3_view_vlan_del AS
    ON DELETE TO hotwire3."10_View/Network/VLANs" DO INSTEAD  DELETE FROM vlan
  WHERE vlan.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_vlan_ins AS
    ON INSERT TO hotwire3."10_View/Network/VLANs" DO INSTEAD  INSERT INTO vlan (vid, name, description, vrrpid) 
  VALUES (new.vid, new.name, new.description, new.vrrpid)
  RETURNING vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid;

CREATE OR REPLACE RULE hotwire3_view_vlan_upd AS
    ON UPDATE TO hotwire3."10_View/Network/VLANs" DO INSTEAD  UPDATE vlan SET vid = new.vid, name = new.name, description = new.description, vrrpid = new.vrrpid
  WHERE vlan.id = old.id
  RETURNING vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/VLANs' , 'vlan');
-- Installing 049_add_patch_panel_view

CREATE VIEW hotwire3.patch_panel_type_hid AS
SELECT patch_panel_type_hid.patch_panel_type_id, patch_panel_type_hid.patch_panel_type_hid
   FROM patch_panel_type_hid;
ALTER VIEW hotwire3.patch_panel_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.patch_panel_type_hid to PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Patch_Panels" AS 
 SELECT patch_panel.id, patch_panel.name, patch_panel.cabinet_id, patch_panel.patch_panel_type_id
   FROM patch_panel;

ALTER TABLE hotwire3."10_View/Network/Patch_Panels" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Patch_Panels" TO cos;

CREATE OR REPLACE RULE hotwire3_view_patch_panel_del AS
    ON DELETE TO hotwire3."10_View/Network/Patch_Panels" DO INSTEAD  DELETE FROM patch_panel
  WHERE patch_panel.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_patch_panel_ins AS
    ON INSERT TO hotwire3."10_View/Network/Patch_Panels" DO INSTEAD  INSERT INTO patch_panel (name, cabinet_id, patch_panel_type_id) 
  VALUES (new.name, new.cabinet_id, new.patch_panel_type_id)
  RETURNING patch_panel.id, patch_panel.name, patch_panel.cabinet_id, patch_panel.patch_panel_type_id;

CREATE OR REPLACE RULE hotwire3_view_patch_panel_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Patch_Panels" DO INSTEAD  UPDATE patch_panel SET name = new.name, cabinet_id = new.cabinet_id, patch_panel_type_id = new.patch_panel_type_id
  WHERE patch_panel.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/Network/Patch_Panels','patch_panel');
-- Installing 050_add_people_all_contact_details

CREATE VIEW hotwire3.gender_hid AS
 SELECT gender_hid.gender_id, gender_hid.gender_hid
    FROM gender_hid;
ALTER VIEW hotwire3.gender_hid OWNER TO dev;
GRANT SELECT ON hotwire3.gender_hid TO PUBLIC;

CREATE VIEW hotwire3.post_category_hid AS
  SELECT 'p3-1'::text AS post_category_id, 'Part III'::text AS post_category_hid
UNION 
  SELECT 'e-'::text || erasmus_type_hid.erasmus_type_id::text AS post_category_id, erasmus_type_hid.erasmus_type_hid::text AS post_category_hid
  FROM erasmus_type_hid
UNION 
  SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS post_category_id, visitor_type_hid.visitor_type_hid AS post_category_hid
  FROM visitor_type_hid
UNION 
  SELECT 'pg-'::text || postgraduate_studentship_type_hid.postgraduate_studentship_type_id::text AS post_category_id, postgraduate_studentship_type_hid.postgraduate_studentship_type_hid AS post_category_hid
  FROM postgraduate_studentship_type_hid
UNION 
  SELECT 'sc-'::text || staff_category_hid.staff_category_id::text AS post_category_id, staff_category_hid.staff_category_hid AS post_category_hid
FROM staff_category_hid;
ALTER VIEW hotwire3.post_category_hid OWNER TO dev;
GRANT SELECT ON hotwire3.post_category_hid TO PUBLIC;

CREATE VIEW hotwire3.physical_status_hid AS
SELECT status.status_id AS physical_status_id, status.status_id AS physical_status_hid
FROM status;
ALTER VIEW hotwire3.physical_status_hid OWNER TO dev;
GRANT SELECT ON hotwire3.physical_status_hid TO PUBLIC;

CREATE VIEW hotwire3.cambridge_college_hid AS
SELECT cambridge_college.id AS cambridge_college_id, cambridge_college.name AS cambridge_college_hid
FROM cambridge_college;
ALTER VIEW hotwire3.cambridge_college_hid OWNER TO dev;
GRANT SELECT ON hotwire3.cambridge_college_hid TO PUBLIC;

CREATE VIEW hotwire3.nationality_hid AS
SELECT nationality.id AS nationality_id, nationality.nationality AS nationality_hid
FROM nationality
ORDER BY nationality.nationality;
ALTER VIEW hotwire3.nationality_hid OWNER TO dev;
GRANT SELECT ON hotwire3.nationality_hid TO PUBLIC;


CREATE OR REPLACE VIEW hotwire3."10_View/People/All_Contact_Details" AS 
 WITH contact_details AS (
         SELECT person.id, person.image_lo AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.previous_surname, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, person.crsid, person.email_address, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.external_work_phone_numbers, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.emergency_contact, person.location, person.forwarding_address, person.new_employer_address, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE person.id = mm_person_nationality.person_id) AS nationality_id, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
        )
 SELECT contact_details.id, contact_details.id AS ro_person_id, contact_details.image_oid, contact_details.surname, contact_details.first_names, contact_details.title_id, contact_details.known_as, contact_details.name_suffix, contact_details.previous_surname, contact_details.gender_id, contact_details.ro_post_category_id, contact_details.crsid, contact_details.email_address, contact_details.arrival_date, contact_details.leaving_date, contact_details.ro_physical_status_id, contact_details.left_but_no_leaving_date_given, contact_details.dept_telephone_number_id, contact_details.room_id, contact_details.research_group_id, contact_details.external_work_phone_numbers, contact_details.cambridge_address::text AS home_address, contact_details.cambridge_phone_number AS home_phone_number, contact_details.cambridge_college_id, contact_details.mobile_number, contact_details.emergency_contact::text, contact_details.location, contact_details.forwarding_address::text, contact_details.new_employer_address::text, contact_details.nationality_id, contact_details._cssclass
   FROM contact_details
  ORDER BY contact_details.surname, contact_details.first_names;

ALTER TABLE hotwire3."10_View/People/All_Contact_Details"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/All_Contact_Details" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/All_Contact_Details" TO reception;

-- Rule: hw_all_contact_details_upd ON hotwire3."10_View/People/All_Contact_Details"

-- DROP RULE hw_all_contact_details_upd ON hotwire3."10_View/People/All_Contact_Details";

CREATE OR REPLACE RULE hw_all_contact_details_upd AS
    ON UPDATE TO hotwire3."10_View/People/All_Contact_Details" DO INSTEAD ( UPDATE person SET image_oid = new.image_oid::bigint, surname = new.surname, first_names = new.first_names, title_id = new.title_id, known_as = new.known_as, name_suffix = new.name_suffix, previous_surname = new.previous_surname, gender_id = new.gender_id, crsid = new.crsid, email_address = new.email_address, arrival_date = new.arrival_date, leaving_date = new.leaving_date, left_but_no_leaving_date_given = new.left_but_no_leaving_date_given, external_work_phone_numbers = new.external_work_phone_numbers, cambridge_address = new.home_address::varchar(500), cambridge_phone_number = new.home_phone_number, cambridge_college_id = new.cambridge_college_id, mobile_number = new.mobile_number, emergency_contact = new.emergency_contact::varchar(500), location = new.location, forwarding_address = new.forwarding_address::varchar(500), new_employer_address = new.new_employer_address::varchar(500)
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.research_group_id, old.research_group_id, 'mm_person_research_group'::character varying, 'person_id'::character varying, 'research_group_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.nationality_id, old.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, old.id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/All_Contact_Details', 'person');

-- Installing 051_add_cos_people_view

CREATE VIEW hotwire3.supervisor_hid AS
SELECT person_hid.person_id AS supervisor_id, person_hid.person_hid AS supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.supervisor_hid TO ro_hid;

CREATE VIEW hotwire3."10_View/People/COs_View" AS
SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, a.name_suffix, a.email_address, a.crsid, a.date_of_birth, a.ro_post_category_id, a.ro_supervisor_id, a.research_group_id, a.dept_telephone_number_id, a.room_id, a.location, a.ro_physical_status_id, a.ro_estimated_leaving_date, a.is_spri, a._cssclass
   FROM ( SELECT person.id, person.id AS ro_person_id, person.image_lo as image_oid, person.surname, person.first_names, person.title_id, person.name_suffix, person.email_address, person.crsid, person.date_of_birth, person.is_spri, _latest_role.post_category_id AS ro_post_category_id, _latest_role.supervisor_id as ro_supervisor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, person.location, _physical_status.status_id AS ro_physical_status_id, LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN apps.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
   LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id ) a
  ORDER BY a.surname, a.first_names;

ALTER VIEW hotwire3."10_View/People/COs_View" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  hotwire3."10_View/People/COs_View" TO cos;
GRANT SELECT ON TABLE hotwire3."10_View/People/COs_View" TO phonebook_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO hr;

CREATE FUNCTION hotwire3.people_cos_view_trig() RETURNS TRIGGER AS 
$$
begin
    if new.id is not null then
	update person set
            surname = new.surname,
            image_oid = new.image_oid::bigint,
            first_names = new.first_names,
            title_id = new.title_id,
            name_suffix = new.name_suffix,
            email_address = new.email_address,
            crsid = new.crsid,
            date_of_birth = new.date_of_birth,
            location = new.location,
            is_spri = new.is_spri
        where person.id = new.id;
    else
        new.id := nextval('person_id_seq');
        insert into person (
            id,
            surname,
            image_oid,
            first_names,
            title_id,
            name_suffix,
            email_address,
            crsid,
            date_of_birth,
            location,
            is_spri
        ) values (
            new.id,
            new.surname,
            new.image_oid::bigint,
            new.first_names,
            new.title_id,
            new.name_suffix,
            new.email_address,
            new.crsid,
            new.date_of_birth,
            new.location,
            new.is_spri
        );
    end if;
    perform fn_mm_array_update(new.dept_telephone_number_id,
                               'mm_person_dept_telephone_number'::varchar,
			       'person_id'::varchar,
                               'dept_telephone_number_id'::varchar,
                               new.id);

    perform fn_mm_array_update(new.research_group_id,
                               'mm_person_research_group'::varchar,
			       'person_id'::varchar,
                               'research_group_id'::varchar,
                               new.id);

    perform fn_mm_array_update(new.room_id,
                               'mm_person_room'::varchar,
                               'person_id'::varchar,
                               'room_id'::varchar,
                               new.id);
    return NEW;
end;
$$ LANGUAGE plpgsql;

CREATE RULE hotwire3_people_cos_view_del AS ON DELETE TO hotwire3."10_View/People/COs_View" DO INSTEAD DELETE FROM person WHERE person.id = old.id;

CREATE TRIGGER hotwire3_people_cos_view_ins INSTEAD OF INSERT ON hotwire3."10_View/People/COs_View" FOR EACH ROW EXECUTE PROCEDURE hotwire3.people_cos_view_trig();

CREATE TRIGGER hotwire3_people_cos_view_upd INSTEAD OF UPDATE ON hotwire3."10_View/People/COs_View" FOR EACH ROW EXECUTE PROCEDURE hotwire3.people_cos_view_trig();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/COs_View', 'person' );
-- Installing 052_add_personnel_data_entry_view

CREATE VIEW hotwire3.co_supervisor_hid AS
SELECT person_hid.person_id AS co_supervisor_id, person_hid.person_hid AS co_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.co_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.co_supervisor_hid TO ro_hid;


CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Data_Entry" AS 
 SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, a.known_as, a.name_suffix, a.previous_surname, a.date_of_birth, a.ro_age, a.gender_id, a.ro_post_category_id, a.ro_chem, a.crsid, a.email_address, a.hide_email, a.arrival_date, a.leaving_date, a.ro_physical_status_id, a.left_but_no_leaving_date_given, a.dept_telephone_number_id, a.hide_phone_no_from_website, a.hide_from_website, a.room_id, a.ro_supervisor_id, a.ro_co_supervisor_id, a.research_group_id, a.home_address, a.home_phone_number, a.cambridge_college_id, a.mobile_number, a.nationality_id, a.emergency_contact, a.location, a.other_information, a.notes, a.forwarding_address, a.new_employer_address, a.chem_at_cam AS "chem_@_cam", a.retired_staff_mailing_list, a.continuous_employment_start_date, a.paper_file_details AS paper_file_status, a.registration_completed, a.clearance_cert_signed, a._cssclass, a.treat_as_academic_staff
   FROM ( SELECT person.id, person.id AS ro_person_id, person.image_lo AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.previous_surname, person.date_of_birth, date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, _latest_role.chem AS ro_chem, person.crsid, person.email_address, person.hide_email, person.do_not_show_on_website AS hide_from_website, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.co_supervisor_id AS ro_co_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.cambridge_address::text AS home_address, person.cambridge_phone_number AS home_phone_number, person.cambridge_college_id, person.mobile_number, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE mm_person_nationality.person_id = person.id) AS nationality_id, person.emergency_contact::text, person.location, person.other_information::text AS other_information, person.notes::text AS notes, person.forwarding_address::text, person.new_employer_address::text, person.chem_at_cam, 
                CASE
                    WHEN retired_staff_list.include_person_id IS NOT NULL THEN true
                    ELSE false
                END AS retired_staff_mailing_list, person.continuous_employment_start_date, person.paper_file_details, person.registration_completed, person.clearance_cert_signed, person.counts_as_academic AS treat_as_academic_staff, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
   LEFT JOIN ( SELECT mm_mailinglist_include_person.mailinglist_id, mm_mailinglist_include_person.include_person_id
         FROM mm_mailinglist_include_person
    JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
   WHERE mailinglist.name::text = 'chem-retiredstaff'::text) retired_staff_list ON person.id = retired_staff_list.include_person_id) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_Data_Entry" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO tkd25;

CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry_upd(hotwire3."10_View/People/Personnel_Data_Entry")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_oid = v.image_oid::bigint,
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as,
                                name_suffix = v.name_suffix,
                                previous_surname = v.previous_surname,
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                cambridge_address = v.home_address::varchar(500),
                                cambridge_phone_number = v.home_phone_number,
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number,
                                emergency_contact = v.emergency_contact::varchar(500), 
                                location = v.location,
                                other_information = v.other_information,
                                notes = v.notes,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                chem_at_cam = v."chem_@_cam",
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_status, 
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                        image_oid, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        previous_surname, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        chem_at_cam,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        v.image_oid::bigint,
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as,
                                        v.name_suffix,
                                        v.previous_surname,
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.home_address::varchar(500),
                                        v.home_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number,
                                        v.emergency_contact::varchar(500), 
                                        v.location,
                                        v.other_information,
                                        v.notes,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v."chem_@_cam",
                                        v.continuous_employment_start_date,
                                        v.paper_file_status, 
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.personnel_data_entry_upd(hotwire3."10_View/People/Personnel_Data_Entry") OWNER TO dev;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_del AS
    ON DELETE TO hotwire3."10_View/People/Personnel_Data_Entry" DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_ins AS
    ON INSERT TO hotwire3."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire3.personnel_data_entry_upd(new.*) AS id;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire3.personnel_data_entry_upd(new.*) AS id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/People/Personnel_Data_Entry','person');
-- Installing 053_add_personnel_history_view

CREATE VIEW hotwire3.reviewer_hid AS
SELECT person_hid.person_id AS reviewer_id, person_hid.person_hid AS reviewer_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.reviewer_hid OWNER TO dev;
GRANT SELECT ON hotwire3.reviewer_hid TO ro_hid;


CREATE VIEW hotwire3."10_View/People/_Staff_Review_History" AS
SELECT staff_review_meeting.id, person.id AS person_id, staff_review_meeting.date_of_meeting, person.next_review_due, staff_review_meeting.reviewer_id, staff_review_meeting.notes
   FROM staff_review_meeting
JOIN person ON staff_review_meeting.person_id = person.id;

ALTER TABLE hotwire3."10_View/People/_Staff_Review_History" OWNER TO dev;
  GRANT ALL ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO dev;
  GRANT SELECT ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO hr;
  GRANT SELECT ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO personnel_history_ro;


CREATE VIEW hotwire3."10_View/Roles/_Cambridge_History_V2" AS 
 SELECT a.id, a.person_id, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.post_category, a.job_title, a.supervisor_id, a.funding, a.fees_funding, a.research_grant_number, a.paid_through_payroll, a.role_id AS _role_xid, a.role_target_viewname AS _target_viewname
   FROM ( SELECT _all_roles.role_id AS id, _all_roles.person_id, _all_roles.start_date, _all_roles.intended_end_date, _all_roles.end_date, _all_roles.funding_end_date, _all_roles.post_category, _all_roles.job_title, _all_roles.supervisor_id, _all_roles.funding, _all_roles.fees_funding, _all_roles.research_grant_number, _all_roles.paid_by_university AS paid_through_payroll, _all_roles.role_id, _all_roles.role_target_viewname
           FROM _all_roles_v13 _all_roles) a
  ORDER BY a.start_date DESC;

ALTER TABLE hotwire3."10_View/Roles/_Cambridge_History_V2"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO reception;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO accounts;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/_Cambridge_History_V2" TO personnel_history_ro;

CREATE VIEW hotwire3."10_View/People/Personnel_History" AS 
 WITH personnel_history AS (
         SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text AS ro_length_of_continuous_service, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text AS ro_length_of_service_in_current_contract, age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone) AS ro_final_length_of_continuous_service, age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone) AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, person.other_information, person.notes, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass, _to_hwsubviewb('10_View/People/_Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Reviews'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, _to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
        )
 SELECT personnel_history.id, personnel_history.ro_person, personnel_history.surname, personnel_history.first_names, personnel_history.title_id, personnel_history.ro_age, personnel_history.ro_post_category_id, personnel_history.continuous_employment_start_date, personnel_history.ro_latest_employment_start_date, personnel_history.ro_employment_end_date, personnel_history.ro_length_of_continuous_service, personnel_history.ro_length_of_service_in_current_contract, personnel_history.ro_final_length_of_continuous_service, personnel_history.ro_final_length_of_service_in_last_contract, personnel_history.ro_supervisor_id, personnel_history.other_information, personnel_history.notes, personnel_history._cssclass, personnel_history.staff_review_history_subview, personnel_history.cambridge_history_subview
   FROM personnel_history
  ORDER BY personnel_history.surname, personnel_history.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_History" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO accounts;

CREATE RULE hotwire3_view_personnel_history_v2_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_History" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, other_information = new.other_information, notes = new.notes
  WHERE person.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/People/Personnel_History','person');
-- Installing 054_add_personnel_phone_view

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Phone" AS 
 SELECT a.id, a.ro_person, a.image, a.surname, a.first_names, a.dept_telephone_number_id, a.external_work_numbers, a.room_id, a.ro_supervisor_id, a.email_address, a.location, a.ro_post_category_id, a.ro_physical_status_id, a.is_external, a._cssclass, a._title_hid, a._known_as
   FROM ( SELECT person.id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS ro_person, person.image_lo AS image, person.surname, person.first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.external_work_phone_numbers AS external_work_numbers, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, person.email_address, person.location, _latest_role.post_category_id AS ro_post_category_id, _physical_status.status_id AS ro_physical_status_id, person.is_external, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN person.is_external = true THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass, title_hid.title_hid AS _title_hid, person.known_as AS _known_as
           FROM person
      LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id) a
  ORDER BY a.surname, a._title_hid, a._known_as;

ALTER TABLE hotwire3."10_View/People/Personnel_Phone"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Phone" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO accounts;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO reception;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO phones;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO student_management_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO building_security;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO interviewtest;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO chematcam;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO phonebook_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO reception_cover;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO safety_management;

CREATE OR REPLACE RULE hotwire3_view_personnel_phone_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_Phone" DO INSTEAD ( UPDATE person SET image_oid = new.image::bigint, surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address, is_external = new.is_external
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ( '10_View/People/Personnel_Phone', 'person');
-- Installing 055_add_photography_registration_view

CREATE OR REPLACE VIEW hotwire3."10_View/People/Photography_Registration" AS 
 SELECT person.id, person.crsid AS ro_crsid, person.gender_id, person.first_names, person.known_as, person.surname, person.date_of_birth, person.arrival_date AS ro_arrival_date, ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.image_lo AS image
   FROM person
   LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Photography_Registration" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Photography_Registration" TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/People/Photography_Registration" TO photography;

CREATE OR REPLACE FUNCTION hotwire3.photography_registration_update() RETURNS TRIGGER AS
$BODY$
	begin
		if NEW.id is not null
		then
			update person set
				gender_id = NEW.gender_id,
				first_names = NEW.first_names,
				known_as = NEW.known_as,
				surname = NEW.surname,
				date_of_birth = NEW.date_of_birth,
				image_oid = NEW.image::bigint
			where person.id = NEW.id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
				image_oid
			) values (
				NEW.gender_id,
				NEW.first_names,
				NEW.known_as,
				NEW.surname,
				NEW.date_of_birth,
				NEW.image::bigint
			) returning id into NEW.id;
		end if;
		perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);	
		return NEW;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.photography_registration_update() OWNER TO dev;

CREATE TRIGGER hotwire3_photography_registration_ins INSTEAD OF INSERT ON hotwire3."10_View/People/Photography_Registration" FOR EACH ROW EXECUTE PROCEDURE hotwire3.photography_registration_update();

CREATE TRIGGER hotwire3_photography_registration_upd INSTEAD OF UPDATE ON hotwire3."10_View/People/Photography_Registration" FOR EACH ROW EXECUTE PROCEDURE hotwire3.photography_registration_update();

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ( '10_View/People/Photography_Registration', 'person');

-- Installing 056_add_research_group_contact_list_view

CREATE OR REPLACE VIEW hotwire3."10_View/People/Contact_Details_By_Group" AS 
WITH group_contact_details AS (
    SELECT
        person.id as id,
        research_group.name as research_group,
        person_hid.person_hid as person,
        _latest_role.post_category_id,
        person.arrival_date,
        COALESCE(_latest_role.end_date, _latest_role.intended_end_date) AS end_date,
        ARRAY(
            SELECT room_id
            FROM mm_person_room
            WHERE mm_person_room.person_id = person.id
        ) room_id,
        ARRAY(
            SELECT dept_telephone_number_id
            FROM mm_person_dept_telephone_number
            WHERE mm_person_dept_telephone_number.person_id = person.id
        ) dept_telephone_number_id,
        person.email_address,
        person.surname as _surname,
        COALESCE(person.known_as,person.first_names) as _first_names
    FROM person
    JOIN person_hid on person.id = person_hid.person_id
    LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
    LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
    JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
    WHERE _physical_status.status_id <> 'Past'
) 
SELECT id, research_group, person, post_category_id, arrival_date, end_date, room_id, dept_telephone_number_id, email_address, _surname, _first_names
FROM group_contact_details
ORDER BY research_group, _surname, _first_names;

ALTER TABLE hotwire3."10_View/People/Contact_Details_By_Group" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Contact_Details_By_Group" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Contact_Details_By_Group" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Contact_Details_By_Group" TO reception;
GRANT SELECT ON TABLE hotwire3."10_View/People/Contact_Details_By_Group" TO student_management;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Contact_Details_By_Group', 'person');
-- Installing 057_add_swipe_cards_view

CREATE OR REPLACE VIEW hotwire3."10_View/People/Swipe_Cards" AS 
 SELECT misd_card.card_id AS id, misd_card.surname, misd_card.forenames, misd_card.card_id AS card, misd_card.dob, misd_card.grade, misd_card.crsid, misd_card.usn, misd_card.issued, misd_card.expires, misd_card.issuenum, misd_card.mifarenum, misd_card.mifareid, misd_card.barcode
   FROM misd_card;

ALTER TABLE hotwire3."10_View/People/Swipe_Cards" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO accounts;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO smb28;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO dev;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Swipe_Cards', 'misd_card' );
-- Installing 058_add_website_view

CREATE VIEW hotwire3.website_staff_category_hid AS
SELECT website_staff_category_hid.website_staff_category_id, website_staff_category_hid.website_staff_category_hid
FROM website_staff_category_hid;
ALTER VIEW hotwire3.website_staff_category_hid OWNER TO dev;
GRANT SELECT ON hotwire3.website_staff_category_hid TO PUBLIC;


CREATE OR REPLACE VIEW hotwire3."10_View/People/Website_View" AS 
 SELECT person.id, person.crsid AS ro_crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS ro_email_address, person.image_lo as image, www_person_hid_v2.www_person_hid AS ro_display_name, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS ro_name_for_sorting, cambridge_college.name AS ro_college_name, cambridge_college.website AS ro_college_website, telephone_numbers.telephone_numbers AS ro_telephone_numbers, 
        CASE
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS ro_post_category, person.website_staff_category_id, person.counts_as_academic AS ro_counts_as_academic, s.status_id AS ro_physical_status, r.rigs AS ro_rigs, pr.primary_rig AS ro_primary_rig, rgs.research_groups AS ro_research_groups, rgs.website_addresses AS ro_website_addresses, post_history.job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE AND _latest_role.role_tablename = 'post_history'::text
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE hotwire3."10_View/People/Website_View"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Website_View" TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/People/Website_View" TO web_editors;

CREATE OR REPLACE RULE people_website_upd AS
    ON UPDATE TO hotwire3."10_View/People/Website_View" DO INSTEAD ( UPDATE person SET website_staff_category_id = new.website_staff_category_id
  WHERE person.id = old.id;
 UPDATE post_history SET job_title = new.job_title
  WHERE post_history.id = (( SELECT ph.id
           FROM person p
      JOIN cache._latest_role lr ON p.id = lr.person_id
   JOIN post_history ph ON lr.role_id = ph.id
  WHERE p.id = old.id));
);


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Website_View', 'person');
-- Installing 059_add_printers_view

CREATE VIEW hotwire3.printer_class_hid AS
SELECT printer_class.id AS printer_class_id, printer_class.name AS printer_class_hid
   FROM printer_class
  ORDER BY printer_class.name;

ALTER VIEW hotwire3.printer_class_hid OWNER TO dev;
GRANT SELECT ON hotwire3.printer_class_hid TO PUBLIC;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Printers" AS 
 SELECT hardware.id as id, printer.id as _printer_id, hardware.id AS _hardware_id, hardware.name, hardware.manufacturer, hardware.model, hardware.date_purchased, hardware.serial_number, hardware.asset_tag, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id, hardware.owner_id, printer.printer_class_id, printer.cups_options, hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_System_Instances_ro'::character varying, 'hardware_id'::character varying, '10_View/Computers/System_Instances'::character varying, 'system_image_id'::character varying, 'id'::character varying) AS "System_Instances"
   FROM printer
   JOIN hardware ON printer.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Computers/Printers" OWNER TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/Printers" TO cos;

CREATE OR REPLACE RULE hotwire3_view_printers_del AS
    ON DELETE TO hotwire3."10_View/Computers/Printers" DO INSTEAD ( DELETE FROM printer
  WHERE printer.id = old._printer_id;
 DELETE FROM hardware
  WHERE hardware.id = old._hardware_id;
);

CREATE OR REPLACE RULE hotwire3_view_printers_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Printers" DO INSTEAD ( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, date_purchased = new.date_purchased, serial_number = new.serial_number, asset_tag = new.asset_tag, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id
  WHERE hardware.id = old._hardware_id;
 UPDATE printer SET printer_class_id = new.printer_class_id, cups_options = new.cups_options
  WHERE printer.id = old._printer_id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Printers', 'hardware');
-- Installing 060_add_printer_classes_view

CREATE VIEW hotwire3."10_View/Computers/_Printers_summary" AS
SELECT
    hardware.id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    printer.printer_class_id,
    hardware.owner_id,
    hardware.room_id
FROM printer
JOIN hardware ON printer.hardware_id = hardware.id;
ALTER VIEW hotwire3."10_View/Computers/_Printers_summary" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Computers/_Printers_summary" TO cos;

CREATE VIEW hotwire3."10_View/Computers/Printer_Classes" AS 
SELECT
    id,
    name,
    ppd_name,
    model_uri,
    linux_duplex_options,
    mac_ppd,
    win_inf_filename,
    win_inf_desc,
    hotwire3.to_hwsubviewb('10_View/Computers/_Printers_summary', 'printer_class_id', '10_View/Computers/Printers', NULL, NULL) as printers_with_this_class FROM public.printer_class;
ALTER VIEW hotwire3."10_View/Computers/Printer_Classes" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Computers/Printer_Classes" TO cos;
 
CREATE RULE "hotwire3_10_View/Computers/Printer_Classes_del" AS ON DELETE TO hotwire3."10_View/Computers/Printer_Classes" DO INSTEAD DELETE FROM "public"."printer_class" WHERE "id" = old.id;
 ALTER TABLE hotwire3."10_View/Computers/Printer_Classes" OWNER TO dev;

CREATE RULE hotwire3_printer_classes_upd AS ON UPDATE TO hotwire3."10_View/Computers/Printer_Classes" DO INSTEAD (
UPDATE printer_class SET
    name = new.name,
    ppd_name = new.ppd_name,
    model_uri = new.model_uri,
    linux_duplex_options = new.linux_duplex_options,
    win_inf_desc = new.win_inf_desc,
    win_inf_filename = new.win_inf_filename,
    mac_ppd = new.mac_ppd
WHERE id = old.id
);

CREATE OR REPLACE FUNCTION "hotwire3.printer_classes_ins_trig_fn"() RETURNS trigger as 
$RULE$ 
    DECLARE _new_id bigint;
    BEGIN
        INSERT INTO public.printer_class (model_uri,mac_ppd,linux_duplex_options,win_inf_filename,win_inf_desc,ppd_name,name) VALUES (new.model_uri,new.mac_ppd,new.linux_duplex_options,new.win_inf_filename,new.win_inf_desc,new.ppd_name,new.name) RETURNING id into _new_id ;
        NEW.id := _new_id;
        return NEW;
    END;
$RULE$ LANGUAGE plpgsql VOLATILE COST 100;
 ALTER FUNCTION "hotwire3.printer_classes_ins_trig_fn"() OWNER TO dev;

 CREATE TRIGGER "hotwire3_10_View/Computers/Printer_Classes_ins_trig" INSTEAD OF INSERT ON hotwire3."10_View/Computers/Printer_Classes" FOR EACH ROW EXECUTE PROCEDURE "hotwire3.printer_classes_ins_trig_fn"();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Printer_Classes','printer_class');
-- Installing 061_add_research_groups_view

CREATE VIEW hotwire3.head_of_group_hid AS
SELECT person_id as head_of_group_id, person_hid as head_of_group_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.head_of_group_hid OWNER TO dev;
GRANT SELECT ON hotwire3.head_of_group_hid TO ro_hid, groupitreps, headsofgroup;

CREATE VIEW hotwire3.deputy_head_of_group_hid AS
SELECT person_id as deputy_head_of_group_id, person_hid as deputy_head_of_group_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.deputy_head_of_group_hid OWNER TO dev;
GRANT SELECT ON hotwire3.deputy_head_of_group_hid TO ro_hid, groupitreps, headsofgroup;

CREATE VIEW hotwire3.alternate_admin_contact_hid AS
SELECT person_id as alternate_admin_contact_id, person_hid as alternate_admin_contact_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.alternate_admin_contact_hid OWNER TO dev;
GRANT SELECT ON hotwire3.alternate_admin_contact_hid TO ro_hid, groupitreps, headsofgroup;

CREATE VIEW hotwire3.computer_rep_hid AS
SELECT person_id as computer_rep_id, person_hid as computer_rep_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.computer_rep_hid OWNER TO dev;
GRANT SELECT ON hotwire3.computer_rep_hid TO ro_hid, groupitreps, headsofgroup;

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Groups" AS 
SELECT
    research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.deputy_head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    research_group.website_address,
    research_group.internal_use_only as hide_from_websites,
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id
    ) AS person_id,
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id
    ) AS computer_rep_id
FROM research_group
ORDER BY research_group.name;

ALTER TABLE hotwire3."10_View/Groups/Research_Groups" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Groups/Research_Groups" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups" TO cos;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups" TO interviewtest;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Groups/Research_Groups" TO chematcam;

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_del AS
    ON DELETE TO hotwire3."10_View/Groups/Research_Groups" DO INSTEAD  DELETE FROM research_group
  WHERE research_group.id = old.id;

CREATE FUNCTION hotwire3.groups_research_groups_ins() RETURNS TRIGGER AS
$$
begin
    insert into research_group ( name, head_of_group_id, deputy_head_of_group_id, administrator_id, website_address, internal_use_only ) VALUES ( new.name, new.head_of_group_id, new.deputy_head_of_group_id, new.alternate_admin_contact_id, new.website_address, coalesce(new.hide_from_websites,'f') ) returning id into new.id;
    perform fn_mm_array_update(new.person_id, array[]::bigint[], 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, new.id) AS fn_mm_array_update;
    perform fn_mm_array_update(new.computer_rep_id, array[]::bigint[], 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, new.id) AS fn_mm_array_update;
    return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.groups_research_groups_ins() OWNER TO dev;

CREATE TRIGGER hotwire3_groups_research_groups_ins INSTEAD OF INSERT ON hotwire3."10_View/Groups/Research_Groups" FOR EACH ROW EXECUTE PROCEDURE hotwire3.groups_research_groups_ins();

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Groups" DO INSTEAD ( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, deputy_head_of_group_id = new.deputy_head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, internal_use_only = new.hide_from_websites
  WHERE research_group.id = old.id;
 SELECT fn_mm_array_update(new.person_id, old.person_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) ;
 SELECT fn_mm_array_update(new.computer_rep_id, old.computer_rep_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) ;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Groups/Research_Groups', 'research_group');
-- Installing 062_add_research_groups_computing_view

CREATE VIEW hotwire3.group_fileserver_hid AS
SELECT group_fileserver.id AS group_fileserver_id, group_fileserver.hostname_for_users AS group_fileserver_hid
FROM group_fileserver;
ALTER VIEW hotwire3.group_fileserver_hid OWNER TO dev;
GRANT SELECT ON hotwire3.group_fileserver_hid TO PUBLIC;


CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Groups_Computing" AS 
SELECT
    research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.deputy_head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    research_group.website_address,
    research_group.internal_use_only as hide_from_websites,
    research_group.group_fileserver_id,
    research_group.active_directory_container,
    research_group.fai_class_id,
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id
    ) AS person_id,
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id
    ) AS computer_rep_id
FROM research_group
ORDER BY research_group.name;

ALTER TABLE hotwire3."10_View/Groups/Research_Groups_Computing" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups_Computing" TO cos;

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_del AS
    ON DELETE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD  DELETE FROM research_group
  WHERE research_group.id = old.id;

CREATE FUNCTION hotwire3.groups_research_groups_computing_ins() RETURNS TRIGGER AS
$$
begin
    insert into research_group ( name, head_of_group_id, deputy_head_of_group_id, administrator_id, website_address, internal_use_only, group_fileserver_id, active_directory_container, fai_class_id ) VALUES ( new.name, new.head_of_group_id, new.deputy_head_of_group_id, new.alternate_admin_contact_id, new.website_address, coalesce(new.hide_from_websites,'f'), new.group_fileserver_id, new.active_directory_container, new.fai_class_id ) returning id into new.id;
    perform fn_mm_array_update(new.person_id, array[]::bigint[], 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, new.id) AS fn_mm_array_update;
    perform fn_mm_array_update(new.computer_rep_id, array[]::bigint[], 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, new.id) AS fn_mm_array_update;
    return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.groups_research_groups_computing_ins() OWNER TO dev;

CREATE TRIGGER hotwire3_groups_research_groups_ins INSTEAD OF INSERT ON hotwire3."10_View/Groups/Research_Groups_Computing" FOR EACH ROW EXECUTE PROCEDURE hotwire3.groups_research_groups_computing_ins();

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD ( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, deputy_head_of_group_id = new.deputy_head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, internal_use_only = new.hide_from_websites, group_fileserver_id = new.group_fileserver_id, active_directory_container = new.active_directory_container, fai_class_id = new.fai_class_id
  WHERE research_group.id = old.id;
 SELECT fn_mm_array_update(new.person_id, old.person_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) ;
 SELECT fn_mm_array_update(new.computer_rep_id, old.computer_rep_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) ;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Groups/Research_Groups_Computing', 'research_group');
-- Installing 063_add_groups_computer_reps_view

GRANT SELECT ON hotwire3.head_of_group_hid,hotwire3.computer_rep_hid to groupitreps,headsofgroup;

-- This should be a with (secueity_barrier) view but not supported in 9.1
CREATE VIEW hotwire3."10_View/My_Groups/Computer_reps"  AS
SELECT
    rg.id,
    rg.name as group_name,
    rg.head_of_group_id,
    ARRAY(
        SELECT computer_rep_id
        from mm_research_group_computer_rep mm
        where mm.research_group_id = rg.id
    ) as computer_rep_id
FROM research_group rg
JOIN person head_of_group on rg.head_of_group_id = head_of_group.id
LEFT JOIN person deputy_head_of_group on rg.deputy_head_of_group_id = deputy_head_of_group.id
LEFT JOIN person administrator on rg.administrator_id = administrator.id
WHERE 
    head_of_group.crsid = current_user 
    OR
    current_user = ANY (
            ARRAY(
                SELECT rep.crsid
                from mm_research_group_computer_rep mm
                join person rep on mm.computer_rep_id = rep.id
                where mm.research_group_id = rg.id
                )
            )
;

ALTER VIEW hotwire3."10_View/My_Groups/Computer_reps" OWNER TO dev;
-- The view will only contain rows that people are allowed to see by virtue of their
-- being a computer rep or head of group. 
GRANT SELECT,UPDATE ON hotwire3."10_View/My_Groups/Computer_reps" TO groupitreps,headsofgroup;

CREATE RULE hw_rg_comp_reps_upd AS ON UPDATE TO hotwire3."10_View/My_Groups/Computer_reps" DO INSTEAD
(
SELECT fn_mm_array_update(NEW.computer_rep_id,'mm_research_group_computer_rep','research_group_id','computer_rep_id',NEW.id);
UPDATE research_group SET
        name = NEW.group_name,
        head_of_group_id = NEW.head_of_group_id
    WHERE id = OLD.id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/My_Groups/Computer_reps', 'research_group');
-- Installing 064_add_groups_software_licence_declarations

-- Heads of group see their own groups
-- COs see everything
CREATE VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration" AS
SELECT
    research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    CASE
        WHEN latest_software_declaration.declaration_date IS NULL THEN 'f'::boolean
        WHEN latest_software_declaration.declaration_date + interval '1 year' > current_date THEN 't'::boolean
        ELSE 'f'::boolean
    END AS all_software_used_by_this_group_is_correctly_licenced,
    latest_software_declaration.declaration_date AS ro_date_of_last_declaration,
    latest_software_declaration.username_of_signer AS ro_declared_by
FROM research_group
LEFT JOIN (
    SELECT 
        research_group_id,
        max(declaration_date) as declaration_date
    FROM group_software_licence_declarations
    GROUP BY research_group_id 
    ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id
LEFT JOIN group_software_licence_declarations latest_software_declaration ON (latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id)
JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id
WHERE current_user = head_of_group.crsid OR pg_has_role(current_user,'cos','member')
ORDER BY research_group.name
;

ALTER TABLE hotwire3."10_View/My_Groups/Software_Licence_Declaration" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire3."10_View/My_Groups/Software_Licence_Declaration" to headsofgroup,cos;
GRANT SELECT ON hotwire3."10_View/My_Groups/Software_Licence_Declaration" to cos;

CREATE FUNCTION hotwire3.group_software_licence_decl_trig() RETURNS TRIGGER AS
$$
    BEGIN
        IF (NEW.all_software_used_by_this_group_is_correctly_licenced = 't') 
            AND
           ((current_date <> OLD.ro_date_of_last_declaration) OR OLD.ro_date_of_last_declaration IS NULL)
        THEN
            INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( NEW.id, current_date, current_user );
        END IF;
        RETURN NEW;
    END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER hw_rg_software_licence_decl_trigger INSTEAD OF UPDATE ON hotwire3."10_View/My_Groups/Software_Licence_Declaration" FOR EACH ROW EXECUTE PROCEDURE hotwire3.group_software_licence_decl_trig();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ('10_View/My_Groups/Software_Licence_Declaration', 'group_software_licence_declarations');
-- Installing 065_add_research_interest_group_view

CREATE VIEW hotwire3.rig_member_hid AS
SELECT person_hid.person_id AS rig_member_id, person_hid.person_hid AS rig_member_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.rig_member_hid OWNER TO dev;
GRANT SELECT ON hotwire3.rig_member_hid TO ro_hid;

CREATE VIEW hotwire3.primary_member_hid AS
SELECT person_hid.person_id AS primary_member_id, person_hid.person_hid AS primary_member_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.primary_member_hid OWNER TO dev;
GRANT SELECT ON hotwire3.primary_member_hid TO ro_hid;

CREATE VIEW hotwire3.chair_hid AS
SELECT person_hid.person_id AS chair_id, person_hid.person_hid AS chair_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.chair_hid OWNER TO dev;
GRANT SELECT ON hotwire3.chair_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Interest_Group" AS 
 SELECT research_interest_group.id, research_interest_group.name::character varying(80) AS name, research_interest_group.chair_id, research_interest_group.website, research_interest_group.mailing_list_address, ARRAY( SELECT mm_member_research_interest_group.member_id
           FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id) AS rig_member_id, ARRAY( SELECT mm_member_research_interest_group.member_id
           FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id AND mm_member_research_interest_group.is_primary) AS primary_member_id
   FROM research_interest_group;

ALTER TABLE hotwire3."10_View/Groups/Research_Interest_Group" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO rig_management;

CREATE FUNCTION hotwire3.primary_rig_update(newval bigint[],oldval bigint[],myid bigint) RETURNS VOID AS
$$
declare
 addarr bigint[];
 delarr bigint[];
 addid bigint;
begin
 addarr = fn_array_diff_left(newval,oldval);
 delarr = fn_array_diff_right(newval,oldval);
 -- remove old values
 if fn_array_count(delarr)>0 then
  execute 'update mm_member_research_interest_group set is_primary = false where "research_interest_group_id"='||myid||' AND "member_id" in ('||array_to_string(delarr,',')||')';
 end if;
 -- add new values
 if fn_array_count(addarr)>0 then
  for i in 1 .. array_upper(addarr,1)
  loop
      execute 'update mm_member_research_interest_group set is_primary = false where member_id = '||addarr[i] ;
      execute 'update mm_member_research_interest_group set is_primary = true where member_id = '||addarr[i]||' and research_interest_group_id = '||myid ;
  end loop;
 end if;

end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.primary_rig_update(bigint[], bigint[], bigint) OWNER TO dev;

CREATE FUNCTION hotwire3.research_interest_group_ins() RETURNS TRIGGER AS
$$
begin
    insert into research_interest_group (  name,chair_id,website,mailing_list_address ) values ( new.name,new.chair_id,new.website,new.mailing_list_address) returning id into new.id;
    perform fn_mm_array_update(new.rig_member_id,
                               array[]::bigint[],
                               'mm_member_research_interest_group',
                               'research_interest_group_id',
                               'member_id',
                               new.id);
    perform hotwire3.primary_rig_update(new.primary_member_id,array[]::bigint[],new.id);
                               
    return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.research_interest_group_ins() OWNER TO dev;

CREATE TRIGGER hotwire3_view_research_interest_group_ins INSTEAD OF INSERT ON hotwire3."10_View/Groups/Research_Interest_Group" FOR EACH ROW EXECUTE PROCEDURE hotwire3.research_interest_group_ins();

CREATE OR REPLACE RULE hotwire3_view_research_interest_group_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Interest_Group" DO INSTEAD ( UPDATE research_interest_group SET name = new.name, chair_id = new.chair_id, website = new.website, mailing_list_address = new.mailing_list_address
  WHERE research_interest_group.id = old.id;
 SELECT fn_mm_array_update(new.rig_member_id, old.rig_member_id, 'mm_member_research_interest_group'::character varying, 'research_interest_group_id'::character varying, 'member_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT hotwire3.primary_rig_update(new.primary_member_id, old.primary_member_id, old.id) AS fn_primary_rig_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Groups/Research_Interest_Group', 'research_interest_group');
-- Installing 066_add_erasmus_students_view

CREATE VIEW hotwire3.erasmus_type_hid AS
SELECT erasmus_type_hid.erasmus_type_id, erasmus_type_hid.erasmus_type_hid
   FROM erasmus_type_hid;
ALTER VIEW hotwire3.erasmus_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.erasmus_type_hid TO PUBLIC;

CREATE VIEW hotwire3.role_status_hid AS
SELECT status.status_id AS role_status_id, status.status_id AS role_status_hid
FROM status;
ALTER VIEW hotwire3.role_status_hid OWNER TO dev;
GRANT SELECT ON hotwire3.role_status_hid TO PUBLIC;


CREATE VIEW hotwire3."10_View/Roles/Erasmus_Students" AS 
 SELECT a.id, a.person_id, a.erasmus_type_id, a.supervisor_id, a.email_address, a.start_date, a.intended_end_date, a.end_date, a.cambridge_college_id, a.university, a.ro_role_status, a.notes, a.force_role_status_to_past, a._surname, a._first_names
   FROM ( SELECT erasmus_socrates_studentship.id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.supervisor_id, person.email_address, erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, person.cambridge_college_id, erasmus_socrates_studentship.university, _all_roles.status AS ro_role_status, erasmus_socrates_studentship.notes::text, erasmus_socrates_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
           FROM erasmus_socrates_studentship
      JOIN person ON person.id = erasmus_socrates_studentship.person_id
   LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = erasmus_socrates_studentship.id AND _all_roles.role_tablename = 'erasmus_socrates_studentship'::text) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire3."10_View/Roles/Erasmus_Students" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Erasmus_Students" TO student_management; 

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD  DELETE FROM erasmus_socrates_studentship
  WHERE erasmus_socrates_studentship.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD  INSERT INTO erasmus_socrates_studentship (person_id, erasmus_type_id, supervisor_id, intended_end_date, end_date, start_date, university, notes, force_role_status_to_past) 
  VALUES (new.person_id, new.erasmus_type_id, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.university, new.notes::varchar(160), new.force_role_status_to_past)
  RETURNING erasmus_socrates_studentship.id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.supervisor_id, NULL::character varying(48) AS "varchar", erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, NULL::bigint AS int8, erasmus_socrates_studentship.university, NULL::character varying(10) AS "varchar", erasmus_socrates_studentship.notes::text, erasmus_socrates_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD ( UPDATE erasmus_socrates_studentship SET person_id = new.person_id, erasmus_type_id = new.erasmus_type_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, university = new.university, notes = new.notes::varchar(160), force_role_status_to_past = new.force_role_status_to_past
  WHERE erasmus_socrates_studentship.id = old.id;
 UPDATE person SET email_address = new.email_address, cambridge_college_id = new.cambridge_college_id
  WHERE person.id = old.person_id;
);


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Roles/Erasmus_Students', 'erasmus_socrates_studentship');
-- Installing 067_add_bulk_supervisors_update


CREATE VIEW hotwire3.old_supervisor_hid AS
SELECT person_id as old_supervisor_id, person_hid as old_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.old_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.old_supervisor_hid TO ro_hid;

CREATE VIEW hotwire3.new_supervisor_hid AS
SELECT person_id as new_supervisor_id, person_hid as new_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.new_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.new_supervisor_hid TO ro_hid;


CREATE VIEW hotwire3."10_View/Roles/_current_people_supervised_by" AS
SELECT
    role_id as id,
    person_id,
    start_date,
    intended_end_date,
    end_date,
    funding_end_date,
    post_category,
    supervisor_id,
    funding,
    fees_funding,
    research_grant_number,
    paid_by_university,
    role_target_viewname as _target_viewname,
    role_id as _role_xid
FROM _latest_role_v12 lr
JOIN _physical_status_v3 ps using (person_id)
WHERE ps.status_id = 'Current' and lr.status = 'Current';
ALTER VIEW hotwire3."10_View/Roles/_current_people_supervised_by" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Roles/_current_people_supervised_by" TO hr;

CREATE VIEW hotwire3."10_View/Roles/Bulk_update_supervisors" AS
SELECT 
    person.id,
    person.id as old_supervisor_id,
    null::bigint as new_supervisor_id,
    _to_hwsubviewb('10_View/Roles/_current_people_supervised_by'::varchar,'supervisor_id'::varchar,'_target_viewname'::varchar, '_role_xid'::varchar,null::varchar) as current_people_supervised_by_old_supervisor_subview
FROM person;
ALTER VIEW hotwire3."10_View/Roles/Bulk_update_supervisors" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire3."10_View/Roles/Bulk_update_supervisors" TO hr;

CREATE FUNCTION hotwire3.update_supervisors(hotwire3."10_View/Roles/Bulk_update_supervisors") returns bigint AS
$$
declare 
    v alias for $1;
begin
    update post_history set supervisor_id = v.new_supervisor_id where post_history.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'post_history' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update visitorship set host_person_id = v.new_supervisor_id where visitorship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'visitorship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update postgraduate_studentship set first_supervisor_id = v.new_supervisor_id where postgraduate_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'postgraduate_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update erasmus_socrates_studentship set supervisor_id = v.new_supervisor_id where erasmus_socrates_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'erasmus_socrates_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update part_iii_studentship set supervisor_id = v.new_supervisor_id where part_iii_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'part_iii_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    return v.old_supervisor_id;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.update_supervisors(hotwire3."10_View/Roles/Bulk_update_supervisors") OWNER TO dev;

CREATE RULE update_supervisors AS ON UPDATE TO hotwire3."10_View/Roles/Bulk_update_supervisors" DO INSTEAD SELECT hotwire3.update_supervisors(new.*) as id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Roles/Bulk_update_supervisors', 'person');
-- Installing 068_add_part_iii_view

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Part_III_Students" AS 
 SELECT a.id, a.person_id, a.project_title_html, a.supervisor_id, a.intended_end_date, a.end_date, a.start_date, a.ro_role_status, a.notes, a.force_role_status_to_past, a._surname, a._first_names
   FROM ( SELECT part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title AS project_title_html, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, _all_roles.status AS ro_role_status, part_iii_studentship.notes::text, part_iii_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
           FROM part_iii_studentship
      LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = part_iii_studentship.id
   LEFT JOIN person ON person.id = part_iii_studentship.person_id
  WHERE _all_roles.role_tablename = 'part_iii_studentship'::text) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire3."10_View/Roles/Part_III_Students" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Part_III_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Part_III_Students" TO mgmt_ro;

CREATE OR REPLACE RULE hotwire3_view_part_iii_del AS
    ON DELETE TO hotwire3."10_View/Roles/Part_III_Students" DO INSTEAD  DELETE FROM part_iii_studentship
  WHERE part_iii_studentship.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_part_iii_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Part_III_Students" DO INSTEAD  INSERT INTO part_iii_studentship (person_id, project_title, supervisor_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past) 
  VALUES (new.person_id, new.project_title_html, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.notes::varchar(80), new.force_role_status_to_past)
  RETURNING part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, NULL::character varying(10) AS "varchar", part_iii_studentship.notes::text, part_iii_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE hotwire3_view_part_iii_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Part_III_Students" DO INSTEAD  UPDATE part_iii_studentship SET project_title = new.project_title_html, person_id = new.person_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = new.notes::varchar(80), force_role_status_to_past = new.force_role_status_to_past
  WHERE part_iii_studentship.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Roles/Part_III_Students', 'part_iii_studentship');
-- Installing 069_add_post_history_view

CREATE VIEW hotwire3.staff_category_hid AS
SELECT staff_category.id AS staff_category_id, staff_category.category AS staff_category_hid
   FROM staff_category;
ALTER VIEW hotwire3.staff_category_hid OWNER TO dev;
GRANT SELECT ON hotwire3.staff_category_hid TO PUBLIC;

CREATE VIEW hotwire3.mentor_hid AS
SELECT person_id as mentor_id, person_hid as mentor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.mentor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.mentor_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Post_History" AS 
 SELECT a.id, a.staff_category_id, a.person_id, a.ro_role_status, a.supervisor_id, a.mentor_id, a.external_mentor, a.start_date_for_continuous_employment_purposes, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.force_role_status_to_past, a.chem, a.paid_through_payroll, a.hours_worked, a.percentage_of_fulltime_hours, a.research_grant_number, a.sponsor, a.probation_period, a.date_of_first_probation_meeting, a.date_of_second_probation_meeting, a.date_of_third_probation_meeting, a.date_of_fourth_probation_meeting, a.first_probation_meeting_comments, a.second_probation_meeting_comments, a.third_probation_meeting_comments, a.fourth_probation_meeting_comments, a.probation_outcome, a.date_probation_letters_sent, a.job_title, a._person
   FROM ( SELECT post_history.id, post_history.staff_category_id, post_history.person_id, _all_roles.status AS ro_role_status, post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university AS paid_through_payroll, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period::text, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments::text, post_history.second_probation_meeting_comments::text, post_history.third_probation_meeting_comments::text, post_history.fourth_probation_meeting_comments::text, post_history.probation_outcome::text, post_history.date_probation_letters_sent, post_history.research_grant_number::text, post_history.filemaker_funding::text AS sponsor, post_history.job_title, post_history.force_role_status_to_past, person_hid.person_hid AS _person
           FROM post_history
      LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id AND _all_roles.role_tablename = 'post_history'::text
   JOIN person ON post_history.person_id = person.id
   JOIN person_hid ON post_history.person_id = person_hid.person_id) a
  ORDER BY a._person, a.start_date DESC;

ALTER TABLE hotwire3."10_View/Roles/Post_History" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Roles/Post_History" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Post_History" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Post_History" TO hr;

CREATE OR REPLACE RULE hotwire3_view_post_history_del AS
    ON DELETE TO hotwire3."10_View/Roles/Post_History" DO INSTEAD  DELETE FROM post_history
  WHERE post_history.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_post_history_update_rules AS
    ON UPDATE TO hotwire3."10_View/Roles/Post_History" DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 UPDATE post_history SET staff_category_id = new.staff_category_id, supervisor_id = new.supervisor_id, mentor_id = new.mentor_id, external_mentor = new.external_mentor, start_date = new.start_date, intended_end_date = new.intended_end_date, end_date = new.end_date, funding_end_date = new.funding_end_date, chem = new.chem, paid_by_university = new.paid_through_payroll, hours_worked = new.hours_worked, percentage_of_fulltime_hours = new.percentage_of_fulltime_hours, probation_period = new.probation_period::varchar(80), date_of_first_probation_meeting = new.date_of_first_probation_meeting, date_of_second_probation_meeting = new.date_of_second_probation_meeting, date_of_third_probation_meeting = new.date_of_third_probation_meeting, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments::varchar(500), second_probation_meeting_comments = new.second_probation_meeting_comments::varchar(500), third_probation_meeting_comments = new.third_probation_meeting_comments::varchar(500), fourth_probation_meeting_comments = new.fourth_probation_meeting_comments::varchar(500), probation_outcome = new.probation_outcome::varchar(200), date_probation_letters_sent = new.date_probation_letters_sent, research_grant_number = new.research_grant_number::varchar(500), filemaker_funding = new.sponsor::varchar(500), job_title = new.job_title, force_role_status_to_past = new.force_role_status_to_past
  WHERE post_history.id = old.id;
);

CREATE OR REPLACE RULE post_history_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Post_History" DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 INSERT INTO post_history (staff_category_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, job_title, force_role_status_to_past) 
  VALUES (new.staff_category_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_through_payroll, new.hours_worked, new.percentage_of_fulltime_hours, new.probation_period::varchar(80), new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments::varchar(500), new.second_probation_meeting_comments::varchar(500), new.third_probation_meeting_comments::varchar(500), new.fourth_probation_meeting_comments::varchar(500), new.probation_outcome::varchar(200), new.date_probation_letters_sent, new.research_grant_number::varchar(500), new.sponsor::varchar(500), new.job_title, new.force_role_status_to_past)
  RETURNING post_history.id, post_history.staff_category_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.force_role_status_to_past, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.research_grant_number::text, post_history.filemaker_funding::text, post_history.probation_period::text, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments::text, post_history.second_probation_meeting_comments::text, post_history.third_probation_meeting_comments::text, post_history.fourth_probation_meeting_comments::text, post_history.probation_outcome::text, post_history.date_probation_letters_sent, post_history.job_title, NULL::text AS text;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Roles/Post_History', 'post_history');
-- Installing 070_add_postgrad_students_view

CREATE VIEW hotwire3.co_mentor_hid AS
SELECT person_id as co_mentor_id, person_hid as co_mentor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.co_mentor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.co_mentor_hid TO ro_hid;

CREATE VIEW hotwire3.first_mentor_hid AS
SELECT person_id as first_mentor_id, person_hid as first_mentor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.first_mentor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.first_mentor_hid TO ro_hid;

CREATE VIEW hotwire3.second_mentor_hid AS
SELECT person_id as second_mentor_id, person_hid as second_mentor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.second_mentor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.second_mentor_hid TO ro_hid;

CREATE VIEW hotwire3.first_supervisor_hid AS
SELECT person_id as first_supervisor_id, person_hid as first_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.first_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.first_supervisor_hid TO ro_hid;

CREATE VIEW hotwire3.second_supervisor_hid AS
SELECT person_id as second_supervisor_id, person_hid as second_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.second_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.second_supervisor_hid TO ro_hid;

CREATE VIEW hotwire3.substitute_supervisor_hid AS
SELECT person_id as substitute_supervisor_id, person_hid as substitute_supervisor_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.substitute_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.substitute_supervisor_hid TO ro_hid;

CREATE VIEW hotwire3.postgraduate_studentship_type_hid AS
SELECT postgraduate_studentship_type.id AS postgraduate_studentship_type_id, postgraduate_studentship_type.name AS postgraduate_studentship_type_hid
   FROM postgraduate_studentship_type;
ALTER VIEW hotwire3.postgraduate_studentship_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.postgraduate_studentship_type_hid TO PUBLIC;


CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Postgrad_Students" AS 
WITH a AS (
SELECT 
     postgraduate_studentship.id,
     postgraduate_studentship.person_id,
     person.surname AS ro_surname,
     person.first_names AS ro_first_names,
     _postgrad_end_dates_v5.status_id AS ro_role_status,
     person.email_address AS ro_email_address,
     postgraduate_studentship.postgraduate_studentship_type_id,
     postgraduate_studentship.part_time,
     postgraduate_studentship.first_supervisor_id AS supervisor_id,
     postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
     postgraduate_studentship.external_co_supervisor,
     postgraduate_studentship.substitute_supervisor_id,
     postgraduate_studentship.first_mentor_id AS mentor_id,
     postgraduate_studentship.second_mentor_id AS co_mentor_id,
     postgraduate_studentship.external_mentor,
     postgraduate_studentship.next_mentor_meeting_due,
     postgraduate_studentship.university,
     person.cambridge_college_id,
     postgraduate_studentship.college_history::text,
     postgraduate_studentship.cpgs_title::text AS cpgs_title_html,
     postgraduate_studentship.msc_title::text AS msc_title_html,
     postgraduate_studentship.mphil_title::text AS mphil_title_html,
     postgraduate_studentship.phd_thesis_title::text AS phd_thesis_title_html,
     postgraduate_studentship.first_year_probationary_report_title::text AS first_year_probationary_report_title_html,
     postgraduate_studentship.paid_through_payroll,
     postgraduate_studentship.emplid_number,
     postgraduate_studentship.gaf_number,
     postgraduate_studentship.start_date,
     person.leaving_date,
     postgraduate_studentship.cpgs_or_mphil_date_submission_due,
     postgraduate_studentship.cpgs_or_mphil_date_submitted,
     postgraduate_studentship.cpgs_or_mphil_date_awarded,
     postgraduate_studentship.first_year_probationary_report_due,
     postgraduate_studentship.first_year_probationary_report_submitted,
     postgraduate_studentship.first_year_probationary_report_approved,
     postgraduate_studentship.mphil_date_submission_due,
     postgraduate_studentship.mphil_date_submitted,
     postgraduate_studentship.mphil_date_awarded,
     postgraduate_studentship.msc_date_submission_due,
     postgraduate_studentship.msc_date_submitted,
     postgraduate_studentship.msc_date_awarded,
     postgraduate_studentship.date_registered_for_phd,
     postgraduate_studentship.phd_date_title_approved,
     postgraduate_studentship.thesis_submission_due_date AS "PhD_three_year_submission_date",
     postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
     postgraduate_studentship.date_phd_submission_due AS "Revised_PhD_submission_date",
     postgraduate_studentship.phd_date_submitted,
     postgraduate_studentship.phd_date_examiners_appointed,
     postgraduate_studentship.phd_date_of_viva,
     postgraduate_studentship.phd_date_awarded,
     postgraduate_studentship.date_withdrawn_from_register,
     postgraduate_studentship.date_reinstated_on_register,
     postgraduate_studentship.intended_end_date,
     postgraduate_studentship.filemaker_funding::text AS funding,
     postgraduate_studentship.filemaker_fees_funding::text AS fees_funding,
     postgraduate_studentship.funding_end_date,
     _postgrad_end_dates_v5.phd_duration AS ro_phd_duration,
     _postgrad_end_dates_v5.phd_duration_days AS ro_phd_duration_days,
     postgraduate_studentship.progress_notes::text,
     postgraduate_studentship.force_role_status_to_past,
     ARRAY( SELECT mm2.nationality_id
                   FROM person p3
              JOIN mm_person_nationality mm2 ON p3.id = mm2.person_id
             WHERE p3.id = postgraduate_studentship.person_id
     ) AS nationality_id, 
     CASE
         WHEN _postgrad_end_dates_v5.status_id::text = 'Past'::text THEN 'orange'::text
         ELSE NULL::text
     END AS _cssclass 
FROM postgraduate_studentship
JOIN _postgrad_end_dates_v5 USING (id)
JOIN person ON postgraduate_studentship.person_id = person.id
)
SELECT a.id,
     a.person_id,
     a.ro_surname,
     a.ro_first_names,
     a.ro_role_status,
     a.ro_email_address,
     a.postgraduate_studentship_type_id,
     a.part_time,
     a.supervisor_id,
     a.co_supervisor_id,
     a.external_co_supervisor,
     a.substitute_supervisor_id,
     a.mentor_id,
     a.co_mentor_id,
     a.external_mentor,
     a.next_mentor_meeting_due,
     a.university,
     a.cambridge_college_id,
     a.college_history,
     a.cpgs_title_html,
     a.first_year_probationary_report_title_html,
     a.msc_title_html,
     a.mphil_title_html,
     a.phd_thesis_title_html,
     a.paid_through_payroll,
     a.emplid_number,
     a.gaf_number,
     a.nationality_id,
     a.start_date,
     a.leaving_date,
     a.cpgs_or_mphil_date_submission_due,
     a.cpgs_or_mphil_date_submitted,
     a.cpgs_or_mphil_date_awarded,
     a.first_year_probationary_report_due,
     a.first_year_probationary_report_submitted,
     a.first_year_probationary_report_approved,
     a.mphil_date_submission_due,
     a.mphil_date_submitted,
     a.mphil_date_awarded,
     a.msc_date_submission_due,
     a.msc_date_submitted,
     a.msc_date_awarded,
     a.date_registered_for_phd,
     a.phd_date_title_approved,
     a."PhD_three_year_submission_date",
     a.phd_four_year_submission_date,
     a."Revised_PhD_submission_date",
     a.phd_date_submitted,
     a.phd_date_examiners_appointed,
     a.phd_date_of_viva,
     a.phd_date_awarded,
     a.date_withdrawn_from_register,
     a.date_reinstated_on_register,
     a.intended_end_date,
     a.funding,
     a.fees_funding,
     a.funding_end_date,
     a.ro_phd_duration,
     a.ro_phd_duration_days,
     a.progress_notes,
     a.force_role_status_to_past,
     a._cssclass
FROM a
ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire3."10_View/Roles/Postgrad_Students" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management_ro;

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Postgrad_Students" DO INSTEAD  DELETE FROM postgraduate_studentship
  WHERE postgraduate_studentship.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_ins AS ON INSERT TO hotwire3."10_View/Roles/Postgrad_Students" DO INSTEAD ( 
    SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
    UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date WHERE person.id = new.person_id;
    INSERT INTO postgraduate_studentship (
        id,
         person_id,
         postgraduate_studentship_type_id,
         part_time,
         first_supervisor_id,
         second_supervisor_id,
         external_co_supervisor,
         substitute_supervisor_id,
         first_mentor_id,
         second_mentor_id,
         external_mentor,
         next_mentor_meeting_due,
         university,
         college_history,
         cpgs_title,
         msc_title,
         mphil_title,
         phd_thesis_title,
         paid_through_payroll,
         emplid_number,
         gaf_number,
         start_date,
         cpgs_or_mphil_date_submission_due,
         cpgs_or_mphil_date_submitted,
         cpgs_or_mphil_date_awarded,
         mphil_date_submission_due,
         mphil_date_submitted,
         mphil_date_awarded,
         msc_date_submission_due,
         msc_date_submitted,
         msc_date_awarded,
         date_registered_for_phd,
         phd_date_title_approved,
         thesis_submission_due_date,
         end_of_registration_date,
         date_phd_submission_due,
         phd_date_submitted,
         phd_date_examiners_appointed,
         phd_date_of_viva,
         phd_date_awarded,
         date_withdrawn_from_register,
         date_reinstated_on_register,
         intended_end_date,
         filemaker_funding,
         filemaker_fees_funding,
         funding_end_date,
         progress_notes,
         force_role_status_to_past,
         first_year_probationary_report_title,
         first_year_probationary_report_due,
         first_year_probationary_report_submitted,
         first_year_probationary_report_approved
    ) VALUES (
         nextval('postgraduate_studentship_id_seq'::regclass),
         new.person_id,
         new.postgraduate_studentship_type_id,
         new.part_time,
         new.supervisor_id,
         new.co_supervisor_id,
         new.external_co_supervisor,
         new.substitute_supervisor_id,
         new.mentor_id,
         new.co_mentor_id,
         new.external_mentor,
         new.next_mentor_meeting_due,
         new.university,
         new.college_history::varchar(500),
         new.cpgs_title_html::varchar(500),
         new.msc_title_html::varchar(300),
         new.mphil_title_html::varchar(300),
         new.phd_thesis_title_html::varchar(500),
         new.paid_through_payroll,
         new.emplid_number,
         new.gaf_number,
         new.start_date,
         new.cpgs_or_mphil_date_submission_due,
         new.cpgs_or_mphil_date_submitted,
         new.cpgs_or_mphil_date_awarded,
         new.mphil_date_submission_due,
         new.mphil_date_submitted,
         new.mphil_date_awarded,
         new.msc_date_submission_due,
         new.msc_date_submitted,
         new.msc_date_awarded,
         new.date_registered_for_phd,
         new.phd_date_title_approved,
         new."PhD_three_year_submission_date",
         new.phd_four_year_submission_date,
         new."Revised_PhD_submission_date",
         new.phd_date_submitted,
         new.phd_date_examiners_appointed,
         new.phd_date_of_viva,
         new.phd_date_awarded,
         new.date_withdrawn_from_register,
         new.date_reinstated_on_register,
         new.intended_end_date,
         new.funding::varchar(500),
         new.fees_funding::varchar(80),
         new.funding_end_date,
         new.progress_notes::varchar,
         new.force_role_status_to_past,
         new.first_year_probationary_report_title_html,
         new.first_year_probationary_report_due,
         new.first_year_probationary_report_submitted,
         new.first_year_probationary_report_approved
    ) RETURNING 
    currval('postgraduate_studentship_id_seq'::regclass) AS currval,
     postgraduate_studentship.person_id,
     NULL::character varying(32) AS "varchar",
     NULL::character varying(32) AS "varchar",
     NULL::character varying AS text,
     NULL::character varying(48) AS "varchar",
     postgraduate_studentship.postgraduate_studentship_type_id,
     postgraduate_studentship.part_time,
     postgraduate_studentship.first_supervisor_id,
     postgraduate_studentship.second_supervisor_id,
     postgraduate_studentship.external_co_supervisor,
     postgraduate_studentship.substitute_supervisor_id,
     postgraduate_studentship.first_mentor_id,
     postgraduate_studentship.second_mentor_id,
     postgraduate_studentship.external_mentor,
     postgraduate_studentship.next_mentor_meeting_due,
     postgraduate_studentship.university,
     NULL::bigint AS int8,
     postgraduate_studentship.college_history::text,
     postgraduate_studentship.cpgs_title::text,
     postgraduate_studentship.first_year_probationary_report_title::text,
     postgraduate_studentship.msc_title::text,
     postgraduate_studentship.mphil_title::text,
     postgraduate_studentship.phd_thesis_title::text,
     postgraduate_studentship.paid_through_payroll,
     postgraduate_studentship.emplid_number,
     postgraduate_studentship.gaf_number,
     NULL::bigint[] AS int8,
     postgraduate_studentship.start_date,
     NULL::date AS date,
     postgraduate_studentship.cpgs_or_mphil_date_submission_due,
     postgraduate_studentship.cpgs_or_mphil_date_submitted,
     postgraduate_studentship.cpgs_or_mphil_date_awarded,
     postgraduate_studentship.first_year_probationary_report_due,
     postgraduate_studentship.first_year_probationary_report_submitted,
     postgraduate_studentship.first_year_probationary_report_approved,
     postgraduate_studentship.mphil_date_submission_due,
     postgraduate_studentship.mphil_date_submitted,
     postgraduate_studentship.mphil_date_awarded,
     postgraduate_studentship.msc_date_submission_due,
     postgraduate_studentship.msc_date_submitted,
     postgraduate_studentship.msc_date_awarded,
     postgraduate_studentship.date_registered_for_phd,
     postgraduate_studentship.phd_date_title_approved,
     postgraduate_studentship.thesis_submission_due_date AS "PhD_three_year_submission_date",
     postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
     postgraduate_studentship.date_phd_submission_due AS "Revised_PhD_submission_date",
     postgraduate_studentship.phd_date_submitted,
     postgraduate_studentship.phd_date_examiners_appointed,
     postgraduate_studentship.phd_date_of_viva,
     postgraduate_studentship.phd_date_awarded,
     postgraduate_studentship.date_withdrawn_from_register,
     postgraduate_studentship.date_reinstated_on_register,
     postgraduate_studentship.intended_end_date,
     postgraduate_studentship.filemaker_funding::text,
     postgraduate_studentship.filemaker_fees_funding::text,
     postgraduate_studentship.funding_end_date,
     btrim(age(postgraduate_studentship.phd_date_submitted::timestamp with time zone,
     postgraduate_studentship.date_registered_for_phd::timestamp with time zone)::text,
     '@ '::text)::character varying(30) AS btrim,
     NULL::integer AS int4,
     postgraduate_studentship.progress_notes::text,
     postgraduate_studentship.force_role_status_to_past,
     NULL::text AS text;
);

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_upd AS ON UPDATE TO hotwire3."10_View/Roles/Postgrad_Students" DO INSTEAD ( 
    UPDATE postgraduate_studentship SET 
        postgraduate_studentship_type_id = new.postgraduate_studentship_type_id,
         part_time = new.part_time,
         first_supervisor_id = new.supervisor_id,
         second_supervisor_id = new.co_supervisor_id,
         external_co_supervisor = new.external_co_supervisor,
         substitute_supervisor_id = new.substitute_supervisor_id,
         first_mentor_id = new.mentor_id,
         second_mentor_id = new.co_mentor_id,
         external_mentor = new.external_mentor,
         next_mentor_meeting_due = new.next_mentor_meeting_due,
         university = new.university,
         college_history = new.college_history::varchar(500),
         cpgs_title = new.cpgs_title_html::varchar(500),
         msc_title = new.msc_title_html::varchar(300),
         mphil_title = new.mphil_title_html::varchar(300),
         phd_thesis_title = new.phd_thesis_title_html::varchar(500),
         paid_through_payroll = new.paid_through_payroll,
         emplid_number = new.emplid_number,
         gaf_number = new.gaf_number,
         start_date = new.start_date,
         cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due,
         cpgs_or_mphil_date_submitted = new.cpgs_or_mphil_date_submitted,
         cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded,
         mphil_date_submission_due = new.mphil_date_submission_due,
         mphil_date_submitted = new.mphil_date_submitted,
         mphil_date_awarded = new.mphil_date_awarded,
         msc_date_submission_due = new.msc_date_submission_due,
         msc_date_submitted = new.msc_date_submitted,
         msc_date_awarded = new.msc_date_awarded,
         date_registered_for_phd = new.date_registered_for_phd,
         phd_date_title_approved = new.phd_date_title_approved,
         thesis_submission_due_date = new."PhD_three_year_submission_date",
         end_of_registration_date = new.phd_four_year_submission_date,
         date_phd_submission_due = new."Revised_PhD_submission_date",
         phd_date_submitted = new.phd_date_submitted,
         phd_date_examiners_appointed = new.phd_date_examiners_appointed,
         phd_date_of_viva = new.phd_date_of_viva,
         phd_date_awarded = new.phd_date_awarded,
         date_withdrawn_from_register = new.date_withdrawn_from_register,
         date_reinstated_on_register = new.date_reinstated_on_register,
         intended_end_date = new.intended_end_date,
         filemaker_funding = new.funding::varchar(500),
         filemaker_fees_funding = new.fees_funding::varchar(80),
         funding_end_date = new.funding_end_date,
         progress_notes = new.progress_notes::varchar,
         force_role_status_to_past = new.force_role_status_to_past,
         first_year_probationary_report_title = new.first_year_probationary_report_title_html::varchar(500),
         first_year_probationary_report_due = new.first_year_probationary_report_due,
         first_year_probationary_report_submitted = new.first_year_probationary_report_submitted,
         first_year_probationary_report_approved = new.first_year_probationary_report_approved
     WHERE postgraduate_studentship.id = old.id;
     UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date WHERE person.id = new.person_id;
     SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Roles/Postgrad_Students', 'postgraduate_studentship');
-- Installing 071_add_visitors_view

CREATE VIEW hotwire3.visitor_type_hid AS
SELECT visitor_type_hid.visitor_type_id, visitor_type_hid.visitor_type_hid
   FROM visitor_type_hid;
ALTER VIEW  hotwire3.visitor_type_hid OWNER TO dev;
GRANT SELECT ON  hotwire3.visitor_type_hid TO PUBLIC;

CREATE VIEW hotwire3.host_hid AS
SELECT person_hid.person_id as host_id, person_hid.person_hid as host_hid
   FROM hotwire3.person_hid;
ALTER VIEW  hotwire3.host_hid OWNER TO dev;
GRANT SELECT ON  hotwire3.host_hid TO ro_hid;


CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Visitors" AS 
 WITH person_visitor AS (
         SELECT visitorship.id, visitorship.person_id, _all_roles.status AS ro_role_status, visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.notes::text, visitorship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
           FROM visitorship
      LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = visitorship.id AND _all_roles.role_tablename = 'visitorship'::text
   LEFT JOIN person ON person.id = visitorship.person_id
        )
 SELECT person_visitor.id, person_visitor.person_id, person_visitor.ro_role_status, person_visitor.home_institution, person_visitor.visitor_type_id, person_visitor.host_id, person_visitor.start_date, person_visitor.intended_end_date, person_visitor.end_date, person_visitor.notes, person_visitor.force_role_status_to_past, person_visitor._surname, person_visitor._first_names
   FROM person_visitor
  ORDER BY person_visitor._surname, person_visitor._first_names;

ALTER TABLE hotwire3."10_View/Roles/Visitors"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Roles/Visitors" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Visitors" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Visitors" TO hr;

-- Rule: hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors"

-- DROP RULE hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire3_view_visitors_del AS
    ON DELETE TO hotwire3."10_View/Roles/Visitors" DO INSTEAD  DELETE FROM visitorship
  WHERE visitorship.id = old.id;

-- Rule: hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors"

-- DROP RULE hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire3_view_visitors_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Visitors" DO INSTEAD  INSERT INTO visitorship (home_institution, visitor_type_id, person_id, host_person_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past) 
  VALUES (new.home_institution, new.visitor_type_id, new.person_id, new.host_id, new.intended_end_date, new.end_date, new.start_date, new.notes::varchar(80), new.force_role_status_to_past)
  RETURNING visitorship.id, visitorship.person_id, NULL::character varying(10) AS "varchar", visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.notes::text, visitorship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

-- Rule: hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors"

-- DROP RULE hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire3_view_visitors_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Visitors" DO INSTEAD  UPDATE visitorship SET home_institution = new.home_institution, visitor_type_id = new.visitor_type_id, person_id = new.person_id, host_person_id = new.host_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = new.notes::varchar(80), force_role_status_to_past = new.force_role_status_to_past
  WHERE visitorship.id = old.id;

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ( '10_View/Roles/Visitors', 'visitorship');
-- Installing 072_add_rooms_attributes

CREATE VIEW hotwire3.building_hid AS
SELECT building_hid.building_id, building_hid.building_hid
   FROM building_hid;
ALTER VIEW hotwire3.building_hid OWNER TO dev;
GRANT SELECT ON hotwire3.building_hid TO PUBLIC;

CREATE VIEW hotwire3.building_floor_hid AS
SELECT building_floor_hid.building_floor_id, building_floor_hid.building_floor_hid
   FROM building_floor_hid;
ALTER VIEW hotwire3.building_floor_hid OWNER TO dev;
GRANT SELECT ON hotwire3.building_floor_hid TO PUBLIC;

CREATE VIEW hotwire3.building_region_hid AS
SELECT building_region_hid.building_region_id, building_region_hid.building_region_hid
   FROM building_region_hid;
ALTER VIEW hotwire3.building_region_hid OWNER TO dev;
GRANT SELECT ON hotwire3.building_region_hid TO PUBLIC;

CREATE VIEW hotwire3.room_type_hid AS
SELECT room_type.id AS room_type_id, room_type.name AS room_type_hid
   FROM room_type;
ALTER VIEW hotwire3.room_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.room_type_hid TO PUBLIC;

CREATE VIEW hotwire3.responsible_group_hid AS
SELECT research_group_id as responsible_group_id, research_group_hid as responsible_group_hid 
FROM hotwire3.research_group_hid;
ALTER VIEW hotwire3.responsible_group_hid OWNER TO dev;
GRANT SELECT ON hotwire3.responsible_group_hid TO PUBLIC;

CREATE VIEW hotwire3.responsible_person_hid AS
SELECT person_id as responsible_person_id, person_hid as responsible_person_hid 
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.responsible_person_hid OWNER TO dev;
GRANT SELECT ON hotwire3.responsible_person_hid TO ro_hid,headsofgroup;

CREATE OR REPLACE VIEW hotwire3."10_View/Rooms/Rooms_Attributes" AS 
 SELECT room.id, room.name, room.embs_name, room.building_id, room.building_region_id, room.building_floor_id, room.area, room.room_type_id, room.number_of_desks, room.number_of_benches, room.maximum_occupancy, _room_fume_hoods.number_of_fumehoods as ro_number_of_fumehoods, room.last_refurbished_date, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, room.comments::text, room.cooling_power, room.power_supply, (room.email_local_part || '@ch.cam.ac.uk')::varchar as ro_email_alias
   FROM room
   LEFT JOIN research_group ON room.responsible_group_id = research_group.id
JOIN _room_fume_hoods on room.id = _room_fume_hoods.id
  ORDER BY room.name;

ALTER TABLE hotwire3."10_View/Rooms/Rooms_Attributes" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Rooms/Rooms_Attributes" TO mgmt_ro,safety_management,headsofgroup,cos,building_security;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Rooms/Rooms_Attributes" TO space_management;
GRANT SELECT ON TABLE hotwire3."10_View/Rooms/Rooms_Attributes" TO space_viewers;

CREATE OR REPLACE RULE hotwire3_view_room_attributes_del AS
    ON DELETE TO hotwire3."10_View/Rooms/Rooms_Attributes" DO INSTEAD  DELETE FROM room
  WHERE room.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_room_attributes_ins AS
    ON INSERT TO hotwire3."10_View/Rooms/Rooms_Attributes" DO INSTEAD  INSERT INTO room (name, embs_name, building_id, building_region_id, building_floor_id, area, room_type_id, number_of_desks, number_of_benches, maximum_occupancy, last_refurbished_date, responsible_group_id, comments, power_supply, cooling_power) 
  VALUES (new.name, new.embs_name, new.building_id, new.building_region_id, new.building_floor_id, new.area, new.room_type_id, new.number_of_desks, new.number_of_benches, new.maximum_occupancy, new.last_refurbished_date, new.responsible_group_id, new.comments::varchar(500), new.power_supply, new.cooling_power)
  RETURNING room.id, room.name, room.embs_name, room.building_id, room.building_region_id, room.building_floor_id, room.area, room.room_type_id, room.number_of_desks, room.number_of_benches, room.maximum_occupancy, null::bigint, room.last_refurbished_date, room.responsible_group_id, null::bigint, room.comments::text, room.cooling_power, room.power_supply, null::varchar;
CREATE OR REPLACE RULE hotwire3_view_room_attributes_upd AS
    ON UPDATE TO hotwire3."10_View/Rooms/Rooms_Attributes" DO INSTEAD  UPDATE room SET name = new.name, embs_name = new.embs_name, building_id = new.building_id, building_region_id = new.building_region_id, building_floor_id = new.building_floor_id, number_of_desks = new.number_of_desks, number_of_benches = new.number_of_benches, area = new.area, room_type_id = new.room_type_id, maximum_occupancy = new.maximum_occupancy, last_refurbished_date = new.last_refurbished_date, responsible_group_id = new.responsible_group_id, comments = new.comments, power_supply = new.power_supply, cooling_power = new.cooling_power
  WHERE room.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Rooms/Rooms_Attributes', 'room');
-- Installing 073_add_rooms_occupancy

CREATE VIEW hotwire3.fume_hood_device_type_hid AS
SELECT fume_hood_device_type_id, fume_hood_device_type_hid
FROM public.fume_hood_device_type_hid;
ALTER VIEW hotwire3.fume_hood_device_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.fume_hood_device_type_hid to PUBLIC;

CREATE VIEW hotwire3.fume_hood_type_hid AS
SELECT fume_hood_type_id, fume_hood_type_hid
FROM public.fume_hood_type_hid;
ALTER VIEW hotwire3.fume_hood_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.fume_hood_type_hid to PUBLIC;


CREATE VIEW hotwire3."10_View/Rooms/_contact_details" AS
SELECT
    person.id,
    mm_person_room.room_id,
    person.id as person_id,
    person.email_address,
    lr.supervisor_id
FROM person
JOIN mm_person_room on person.id = mm_person_room.person_id
JOIN _physical_status_v3 ps using (person_id)
left join _latest_role_v12 lr using (person_id)
WHERE ps.status_id = 'Current';
ALTER VIEW hotwire3."10_View/Rooms/_contact_details" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Rooms/_contact_details" TO mgmt_ro,space_management,space_viewers;

CREATE VIEW hotwire3."10_View/Rooms/_fume_hoods" AS
SELECT
    id,
    room_id,
    fume_hood_type_id,
    fume_hood_device_type_id,
    device_number,
    manufacturer
FROM fume_hood;
ALTER VIEW hotwire3."10_View/Rooms/_fume_hoods" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Rooms/_fume_hoods" TO mgmt_ro,space_management,space_viewers;

CREATE VIEW hotwire3."10_View/Rooms/Room_Occupants" AS 
 SELECT room.id, room.name, room.room_type_id, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, o.current_occupancy AS ro_current_occupancy, o.person_id, (room.email_local_part || '@ch.cam.ac.uk')::varchar as ro_email_alias, room.number_of_desks, room.maximum_occupancy, _room_fume_hoods.number_of_fumehoods AS ro_number_of_fumehoods, room.area, room.last_refurbished_date, room.comments::text, hotwire3.to_hwsubviewb('10_View/Rooms/_contact_details'::varchar,'room_id'::varchar,'10_View/People/Personnel_Phone'::varchar,null::varchar,null::varchar) as contact_details, hotwire3.to_hwsubviewb('10_View/Rooms/_fume_hoods'::varchar,'room_id'::varchar,'10_View/Safety/Fume_Hoods'::varchar,null::varchar,null::varchar) as fume_hoods
   FROM room
   LEFT JOIN research_group ON room.responsible_group_id = research_group.id
   LEFT JOIN ( SELECT r2.id, count(mm.person_id) AS current_occupancy, array_agg(mm.person_id) AS person_id
      FROM room r2
   LEFT JOIN mm_person_room mm ON mm.room_id = r2.id
   LEFT JOIN _physical_status_v3 USING (person_id)
  WHERE _physical_status_v3.status_id::text = 'Current'::text
  GROUP BY r2.id) o ON o.id = room.id
   JOIN _room_fume_hoods on room.id = _room_fume_hoods.id
  ORDER BY room.name;

ALTER TABLE hotwire3."10_View/Rooms/Room_Occupants" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Rooms/Room_Occupants" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Rooms/Room_Occupants" TO space_management;
GRANT SELECT ON TABLE hotwire3."10_View/Rooms/Room_Occupants" TO space_viewers;

CREATE OR REPLACE RULE hotwire3_view_room_occupants_upd AS
    ON UPDATE TO hotwire3."10_View/Rooms/Room_Occupants" DO INSTEAD ( UPDATE room SET name = new.name, room_type_id = new.room_type_id, responsible_group_id = new.responsible_group_id, number_of_desks = new.number_of_desks, maximum_occupancy = new.maximum_occupancy, area = new.area, last_refurbished_date = new.last_refurbished_date, comments = new.comments
  WHERE room.id = old.id;
 SELECT fn_mm_array_update(new.person_id, old.person_id, 'mm_person_room'::character varying, 'room_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Rooms/Room_Occupants', 'room');
-- Installing 074_add_safety_views

CREATE VIEW hotwire3.firetrace_type_hid AS
SELECT firetrace_type_id, firetrace_type_hid
FROM public.firetrace_type_hid;
ALTER VIEW hotwire3.firetrace_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.firetrace_type_hid TO PUBLIC;

CREATE VIEW hotwire3.recirc_or_ducted_hid AS
SELECT recirc_or_ducted_id, recirc_or_ducted_hid
FROM public.recirc_or_ducted_hid;
ALTER VIEW hotwire3.recirc_or_ducted_hid OWNER TO dev;
GRANT SELECT ON hotwire3.recirc_or_ducted_hid TO PUBLIC;

CREATE VIEW hotwire3.extract_device_type_hid AS
SELECT extract_device_type_id, extract_device_type_hid
FROM public.extract_device_type_hid;
ALTER VIEW hotwire3.extract_device_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.extract_device_type_hid TO PUBLIC;

CREATE VIEW hotwire3.extract_arm_canopy_type_hid AS
SELECT extract_arm_canopy_type_id, extract_arm_canopy_type_hid
FROM public.extract_arm_canopy_type_hid;
ALTER VIEW hotwire3.extract_arm_canopy_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.extract_arm_canopy_type_hid TO PUBLIC;

CREATE VIEW hotwire3.vented_enclosure_device_type_hid AS
SELECT vented_enclosure_device_type_id, vented_enclosure_device_type_hid
FROM public.vented_enclosure_device_type_hid;
ALTER VIEW hotwire3.vented_enclosure_device_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.vented_enclosure_device_type_hid TO PUBLIC;


CREATE VIEW hotwire3."10_View/Safety/Extract_Arms_and_Canopies" AS 
 SELECT extract_arm_canopy.id, extract_arm_canopy.room_id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id
   FROM extract_arm_canopy;

ALTER TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" TO safety_management;



CREATE RULE hotwire3_view_extract_arm_canopies_del AS
    ON DELETE TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  DELETE FROM extract_arm_canopy
  WHERE extract_arm_canopy.id = old.id;



CREATE RULE hotwire3_view_extract_arm_canopies_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  UPDATE extract_arm_canopy SET room_id = new.room_id, extract_device_type_id = new.extract_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes, extract_arm_canopy_type_id = new.extract_arm_canopy_type_id
  WHERE extract_arm_canopy.id = old.id;



CREATE RULE hotwire3_view_extract_arm_canopy_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  INSERT INTO extract_arm_canopy (room_id, extract_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes, extract_arm_canopy_type_id) 
  VALUES (new.room_id, new.extract_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes, new.extract_arm_canopy_type_id)
  RETURNING extract_arm_canopy.id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.room_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id;

CREATE VIEW hotwire3."10_View/Safety/Fume_Hoods" AS 
 SELECT fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id
   FROM fume_hood;

ALTER TABLE hotwire3."10_View/Safety/Fume_Hoods" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fume_Hoods" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Fume_Hoods" TO safety_management;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Safety/Fume_Hoods" TO space_management;


CREATE RULE hotwire3_view_fume_hood_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Fume_Hoods" DO INSTEAD  INSERT INTO fume_hood (room_id, fume_hood_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, fume_hood_type_id, pdh, notes, research_group_id) 
  VALUES (new.room_id, new.fume_hood_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.fume_hood_type_id, new.pdh, new.notes, new.research_group_id)
  RETURNING fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id;



CREATE RULE hotwire3_view_fume_hoods_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fume_Hoods" DO INSTEAD  DELETE FROM fume_hood
  WHERE fume_hood.id = old.id;



CREATE RULE hotwire3_view_fume_hoods_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Fume_Hoods" DO INSTEAD  UPDATE fume_hood SET room_id = new.room_id, fume_hood_device_type_id = new.fume_hood_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, fume_hood_type_id = new.fume_hood_type_id, pdh = new.pdh, notes = new.notes, research_group_id = new.research_group_id
  WHERE fume_hood.id = old.id;

CREATE VIEW hotwire3."10_View/Safety/Glove_Boxes" AS 
 SELECT glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes
   FROM glove_box;

ALTER TABLE hotwire3."10_View/Safety/Glove_Boxes" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Glove_Boxes" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Glove_Boxes" TO safety_management;



CREATE RULE hotwire3_view_glove_box_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  INSERT INTO glove_box (room_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, notes) 
  VALUES (new.room_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.notes)
  RETURNING glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes;



CREATE RULE hotwire3_view_glove_boxes_del AS
    ON DELETE TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  DELETE FROM glove_box
  WHERE glove_box.id = old.id;



CREATE RULE hotwire3_view_glove_boxes_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  UPDATE glove_box SET room_id = new.room_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, notes = new.notes
  WHERE glove_box.id = old.id;

CREATE VIEW hotwire3."10_View/Safety/Vented_Enclosures" AS 
 SELECT vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes
   FROM vented_enclosure;

ALTER TABLE hotwire3."10_View/Safety/Vented_Enclosures" OWNER TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Vented_Enclosures" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Vented_Enclosures" TO safety_management;


CREATE RULE hotwire3_view_vented_enclosure_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  INSERT INTO vented_enclosure (room_id, vented_enclosure_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes) 
  VALUES (new.room_id, new.vented_enclosure_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes)
  RETURNING vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes;



CREATE RULE hotwire3_view_vented_enclosures_del AS
    ON DELETE TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  DELETE FROM vented_enclosure
  WHERE vented_enclosure.id = old.id;



CREATE RULE hotwire3_view_vented_enclosures_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  UPDATE vented_enclosure SET room_id = new.room_id, vented_enclosure_device_type_id = new.vented_enclosure_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes
  WHERE vented_enclosure.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Vented_Enclosures', 'vented_enclosure');
INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Extract_Arms_and_Canopies', 'extract_arm_canopy');
INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Glove_Boxes', 'glove_box');
-- Installing 075_add_fire_warden_views

CREATE VIEW hotwire3.fire_warden_area_hid AS
SELECT fire_warden_area.fire_warden_area_id, fire_warden_area.name AS fire_warden_area_hid
   FROM fire_warden_area;
ALTER VIEW hotwire3.fire_warden_area_hid OWNER TO dev;
GRANT SELECT ON hotwire3.fire_warden_area_hid TO PUBLIC;

CREATE VIEW hotwire3.fire_warden_hid AS 
 SELECT fire_warden.fire_warden_id, person_hid.person_hid AS fire_warden_hid
   FROM fire_warden
   JOIN hotwire3.person_hid USING (person_id);
ALTER TABLE hotwire3.fire_warden_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.fire_warden_hid TO public;

CREATE VIEW hotwire3.deputy_fire_warden_hid AS 
 SELECT fire_warden_hid.fire_warden_id AS deputy_fire_warden_id, fire_warden_hid.fire_warden_hid AS deputy_fire_warden_hid
   FROM hotwire3.fire_warden_hid;
ALTER TABLE hotwire3.deputy_fire_warden_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.deputy_fire_warden_hid TO public;

CREATE VIEW hotwire3."10_View/Safety/Fire_warden_areas" AS 
 SELECT fire_warden_area.fire_warden_area_id AS id, fire_warden_area.name, fire_warden_area.location, fire_warden_area.building_id, fire_warden_area.building_region_id, fire_warden_area.building_floor_id, primary_fw.fire_warden_id, ARRAY( SELECT fw3.fire_warden_id
           FROM fire_warden fw3
          WHERE fw3.fire_warden_area_id = fire_warden_area.fire_warden_area_id AND fw3.is_primary = false) AS deputy_fire_warden_id
   FROM fire_warden_area
   LEFT JOIN ( SELECT a2.fire_warden_area_id, fw2.fire_warden_id
           FROM fire_warden_area a2
      JOIN fire_warden fw2 USING (fire_warden_area_id)
     WHERE fw2.is_primary = true) primary_fw USING (fire_warden_area_id);

ALTER TABLE hotwire3."10_View/Safety/Fire_warden_areas" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Fire_warden_areas" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fire_warden_areas" TO mgmt_ro;

CREATE RULE fire_warden_area_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_warden_areas" DO INSTEAD  DELETE FROM fire_warden_area
  WHERE fire_warden_area.fire_warden_area_id = old.id;

CREATE FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas")
  RETURNS bigint AS
$BODY$
declare
    v alias for $1;
    v_fire_warden_area_id bigint;
begin
    if v.id is not null 
    then
        v_fire_warden_area_id = v.id;
        update fire_warden_area set 
            name = v.name,
            location = v.location,
            building_id = v.building_id,
            building_region_id = v.building_region_id,
            building_floor_id = v.building_floor_id
        where fire_warden_area_id = v_fire_warden_area_id;
    else
        insert into fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) values (
            v.name,
            v.location,
            v.building_id,
            v.building_region_id,
            v.building_floor_id
        ) returning fire_warden_area_id into v_fire_warden_area_id;
    end if;
    -- remove all fire wardens for this area
    update fire_warden set fire_warden_area_id = null where fire_warden.fire_warden_area_id = v_fire_warden_area_id;
    -- update the primary fire warden
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 't' where fire_warden.fire_warden_id = v.fire_warden_id;
    -- update secondary fire wardens
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 'f' where fire_warden.fire_warden_id = any(v.deputy_fire_warden_id);
    return v_fire_warden_area_id;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas")
  OWNER TO dev;


CREATE RULE fire_warden_area_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Fire_warden_areas" DO INSTEAD  SELECT hotwire3.fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;



CREATE RULE fire_warden_area_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Fire_warden_areas" DO INSTEAD  SELECT hotwire3.fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Fire_warden_areas', 'fire_warden_area');

CREATE VIEW hotwire3."10_View/Safety/Fire_wardens" AS 
 SELECT fire_warden.fire_warden_id AS id, fire_warden.person_id, fire_warden.qualification_date, fire_warden_area.fire_warden_area_id, fire_warden_area.building_id AS ro_building_id, fire_warden_area.building_region_id AS ro_building_region_id, fire_warden_area.building_floor_id AS ro_building_floor_id, fire_warden.is_primary AS main_firewarden_for_area, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE fire_warden.person_id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, _physical_status_v3.status_id AS ro_physical_status, _fire_warden_status.requalification_date::date AS ro_requalification_date, _fire_warden_status.in_date AS ro_qualification_in_date, _latest_role_v12.post_category AS ro_post_category, 
        CASE
            WHEN _physical_status_v3.status_id::text <> 'Current'::text THEN 'orange'::text
            WHEN _fire_warden_status.in_date = false THEN 'red'::text
            WHEN _fire_warden_status.in_date IS NULL THEN 'blue'::text
            ELSE NULL::text
        END AS _cssclass
   FROM fire_warden
   LEFT JOIN _latest_role_v12 USING (person_id)
   LEFT JOIN fire_warden_area USING (fire_warden_area_id)
   LEFT JOIN _physical_status_v3 USING (person_id)
   LEFT JOIN _fire_warden_status USING (fire_warden_id);

ALTER TABLE hotwire3."10_View/Safety/Fire_wardens"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO mgmt_ro;

CREATE OR REPLACE FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens")
  RETURNS bigint AS
$BODY$
declare
    v alias for $1;
    v_fire_warden_id bigint;
begin
    if v.main_firewarden_for_area = 't' and v.fire_warden_area_id is not null
    then
        update fire_warden set is_primary = 'f' where fire_warden_area_id = v.fire_warden_area_id;
    end if;
    if v.id is not null 
    then
        v_fire_warden_id = v.id;
        update fire_warden set 
            qualification_date = v.qualification_date,
            fire_warden_area_id = v.fire_warden_area_id,
            is_primary = v.main_firewarden_for_area
        where fire_warden_id = v_fire_warden_id;
    else
        insert into fire_warden ( 
            qualification_date,
            person_id,
            fire_warden_area_id,
            is_primary
        ) values (
            v.qualification_date,
            v.person_id,
            v.fire_warden_area_id,
            coalesce(v.main_firewarden_for_area,'t')
        ) returning fire_warden_id into v_fire_warden_id;
    end if;
    return v_fire_warden_id;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens")
  OWNER TO dev;


CREATE RULE fire_warden_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  DELETE FROM fire_warden
  WHERE fire_warden.fire_warden_id = old.id;



CREATE RULE fire_warden_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  SELECT hotwire3.fire_warden_upd(new.*) AS fn_fire_warden_upd;


CREATE RULE fire_warden_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  SELECT hotwire3.fire_warden_upd(new.*) AS fn_fire_warden_upd;


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Fire_wardens', 'fire_warden');
-- Installing 076_add_firstaiders_view

CREATE VIEW hotwire3.firstaider_funding_hid AS
SELECT firstaider_funding.id AS firstaider_funding_id, firstaider_funding.funding AS firstaider_funding_hid
FROM firstaider_funding;
ALTER TABLE hotwire3.firstaider_funding_hid OWNER TO dev;
GRANT SELECT ON TABLE hotwire3.firstaider_funding_hid TO public;

CREATE VIEW hotwire3."10_View/Safety/Firstaiders" AS 
 SELECT firstaider.id, person.image_lo AS ro_image, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, firstaider.defibrillator_trained, array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
   LEFT JOIN hotwire3.building_hid USING (building_id)
  WHERE p2.id = firstaider.person_id
  ORDER BY building_hid.building_hid), '/'::text) AS ro_building, array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
   LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id)
  WHERE p2.id = firstaider.person_id
  ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor, array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
  WHERE p2.id = firstaider.person_id
  ORDER BY room.name), '/'::text) AS ro_room, array_to_string(ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
      JOIN mm_person_dept_telephone_number ON mm_person_dept_telephone_number.person_id = p2.id
   JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
  WHERE p2.id = firstaider.person_id), ' / '::text) AS ro_dept_telephone_numbers, _physical_status.status_id AS status, firstaider.qualification_up_to_date AS ro_qualification_up_to_date, _latest_role.post_category AS ro_post_category, 
        CASE
            WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN firstaider.qualification_up_to_date = false THEN 'red'::text
            ELSE NULL::text
        END AS _cssclass
   FROM firstaider
   JOIN person ON firstaider.person_id = person.id
   LEFT JOIN _latest_role_v12 _latest_role ON firstaider.person_id = _latest_role.person_id
   JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = firstaider.person_id;

ALTER TABLE hotwire3."10_View/Safety/Firstaiders" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Firstaiders" TO www_sites;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO firstaiders;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Safety/Firstaiders" TO hr;



CREATE RULE firstaiders_del AS
    ON DELETE TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  DELETE FROM firstaider
  WHERE firstaider.id = old.id;



CREATE RULE firstaiders_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  INSERT INTO firstaider (person_id, firstaider_funding_id, qualification_date, requalify_date, hf_cn_trained, defibrillator_trained) 
  VALUES (new.person_id, new.firstaider_funding_id, new.qualification_date, new.requalify_date, new.hf_cn_trained, new.defibrillator_trained)
  RETURNING firstaider.id, NULL::oid, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, firstaider.defibrillator_trained, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::character varying(20) AS "varchar", NULL::boolean AS bool, NULL::character varying(80) AS "varchar", NULL::text AS text;



CREATE RULE firstaiders_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  UPDATE firstaider SET person_id = new.person_id, firstaider_funding_id = new.firstaider_funding_id, qualification_date = new.qualification_date, requalify_date = new.requalify_date, hf_cn_trained = new.hf_cn_trained, defibrillator_trained = new.defibrillator_trained
  WHERE firstaider.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Safety/Firstaiders', 'firstaider');
-- Installing 077_network_sockets

CREATE VIEW hotwire3.cable_type_hid AS
SELECT cable_type_hid.cable_type_id, cable_type_hid.cable_type_hid
   FROM cable_type_hid;
ALTER TABLE hotwire3.cable_type_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.cable_type_hid TO public;

CREATE VIEW hotwire3.connector_type_hid AS
SELECT connector_type_hid.connector_type_id, connector_type_hid.connector_type_hid
   FROM connector_type_hid;
ALTER TABLE hotwire3.connector_type_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.connector_type_hid TO public;

CREATE VIEW hotwire3.linked_socket_hid AS
SELECT socket_id as linked_socket_id, socket_hid as linked_socket_hid
FROM hotwire3.socket_hid;
ALTER VIEW hotwire3.linked_socket_hid OWNER TO dev;
GRANT SELECT ON hotwire3.linked_socket_hid TO PUBLIC;

CREATE VIEW hotwire3.switch_port_hid AS
SELECT switchport.id AS switch_port_id, (hardware.name::text || ' '::text) || switchport.name::text AS switch_port_hid
FROM switchport
JOIN switch ON switchport.switch_id = switch.id
JOIN hardware ON switch.hardware_id = hardware.id;
ALTER VIEW hotwire3.switch_port_hid OWNER TO dev;
GRANT SELECT ON hotwire3.switch_port_hid TO PUBLIC;

CREATE VIEW hotwire3."10_View/Network/Sockets" AS 
 SELECT socket.id, socket.room_label, socket.panel_label, socket.room_id, socket.patch_panel_id, socket.cable_type_id, socket.connector_type_id, socket.linked_socket_id, switchport.id AS switch_port_id
FROM socket
LEFT JOIN switchport ON switchport.socket_id = socket.id
ORDER BY socket.patch_panel_id, socket.panel_label;

ALTER TABLE hotwire3."10_View/Network/Sockets" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Sockets" TO cos;

CREATE FUNCTION hotwire3.network_socket() RETURNS TRIGGER AS $$
begin
    if new.id is not null then
        update socket set
            room_label = new.room_label,
            panel_label = new.panel_label,
            room_id = new.room_id,
            patch_panel_id = new.patch_panel_id,
            cable_type_id = new.cable_type_id,
            connector_type_id = new.connector_type_id,
            linked_socket_id = new.linked_socket_id
        where id = new.id;
    else
        new.id := nextval('socket_id_seq');
        insert into socket (
            id,
            room_label,
            panel_label,
            room_id,
            patch_panel_id,
            cable_type_id,
            connector_type_id,
            linked_socket_id
        ) values (
            new.id,
            new.room_label,
            new.panel_label,
            new.room_id,
            new.patch_panel_id,
            new.cable_type_id,
            new.connector_type_id,
            new.linked_socket_id
        );
    end if;
    if new.switch_port_id is not null then
        update switchport set
            socket_id = new.id
        where switchport.id = new.switch_port_id;
    end if;
    if new.linked_socket_id is not null then
        update socket set linked_socket_id = new.id 
        where id = new.linked_socket_id;
    else
        update socket set linked_socket_id = null 
        where linked_socket_id = new.id;
    end if;
    
    return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.network_socket() OWNER TO dev;

CREATE RULE hotwire3_view_socket_del AS
    ON DELETE TO hotwire3."10_View/Network/Sockets" DO INSTEAD  DELETE FROM socket
  WHERE socket.id = old.id;



CREATE TRIGGER hotwire3_view_socket_ins INSTEAD OF INSERT ON hotwire3."10_View/Network/Sockets" FOR EACH ROW EXECUTE PROCEDURE hotwire3.network_socket();

CREATE TRIGGER hotwire3_view_socket_upd INSTEAD OF update ON hotwire3."10_View/Network/Sockets" FOR EACH ROW EXECUTE PROCEDURE hotwire3.network_socket();


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Network/Sockets', 'socket');
-- Installing 078_add_staff_reviews

CREATE VIEW hotwire3.usual_reviewer_hid AS
SELECT person_id AS usual_reviewer_id, person_hid as usual_reviewer_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.usual_reviewer_hid OWNER TO dev;
GRANT SELECT ON hotwire3.usual_reviewer_hid TO ro_hid;

-- the reason for having two last_update columns, one timestamp and one varchar, is
-- that Hotwire has grown a very intrusive js widget for handling timestamps in 
-- edit pages that doesn't go away when the column is read only. So we display a 
-- varchar version of the field to avoid the widget but use the timestamp version 
-- for ORDER BY to get the sorting correct.
CREATE VIEW hotwire3."10_View/People/Staff_Reviews" AS 
with a as (
 SELECT staff_review_meeting.id, staff_review_meeting.person_id, person.usual_reviewer_id, _latest_role.supervisor_id as ro_supervisor_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, person.next_review_due, staff_review_meeting.notes::text, staff_review_meeting.last_updated::varchar as ro_last_updated, staff_review_meeting.last_updated::timestamp(0) as _last_updated
   FROM staff_review_meeting
   JOIN person ON person.id = staff_review_meeting.person_id
   JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id ) 
select id, person_id, usual_reviewer_id, ro_supervisor_id, date_of_meeting, reviewer_id, next_review_due, notes, ro_last_updated, _last_updated
from a
ORDER BY _last_updated DESC NULLS LAST;

ALTER TABLE hotwire3."10_View/People/Staff_Reviews" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/People/Staff_Reviews" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Staff_Reviews" TO mgmt_ro;



CREATE RULE hotwire3_view_staff_reviews_del AS
    ON DELETE TO hotwire3."10_View/People/Staff_Reviews" DO INSTEAD  DELETE FROM staff_review_meeting
  WHERE staff_review_meeting.id = old.id;



CREATE RULE hotwire3_view_staff_reviews_ins AS
    ON INSERT TO hotwire3."10_View/People/Staff_Reviews" DO INSTEAD ( UPDATE person SET next_review_due = new.next_review_due
  WHERE person.id = new.person_id;
 INSERT INTO staff_review_meeting (person_id, date_of_meeting, reviewer_id, notes, last_updated) 
  VALUES (new.person_id, new.date_of_meeting, COALESCE(new.reviewer_id, ( SELECT person.usual_reviewer_id
           FROM person
          WHERE person.id = new.person_id)), new.notes::varchar(500), now())
  RETURNING staff_review_meeting.id, NULL::bigint AS int8, NULL::bigint AS int8, NULL::bigint AS int8, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, NULL::date AS date, staff_review_meeting.notes::text, null::varchar, NULL::timestamp(0) AS "timestamp";
);



CREATE RULE hotwire3_view_staff_reviews_upd AS
    ON UPDATE TO hotwire3."10_View/People/Staff_Reviews" DO INSTEAD ( UPDATE staff_review_meeting SET date_of_meeting = new.date_of_meeting, reviewer_id = COALESCE(new.reviewer_id, new.usual_reviewer_id), notes = new.notes::varchar(500), last_updated = now()
  WHERE staff_review_meeting.id = old.id;
 UPDATE person SET next_review_due = new.next_review_due, usual_reviewer_id = new.usual_reviewer_id
  WHERE person.id = old.person_id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Staff_Reviews', 'staff_review' );
-- Installing 079_add_phones_view

CREATE VIEW hotwire3."10_View/Phones/Phones" AS 
 SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, dept_telephone_number.fax, dept_telephone_number.personal_line, ARRAY( SELECT mm_person_dept_telephone_number.person_id
           FROM mm_person_dept_telephone_number
          WHERE dept_telephone_number.id = mm_person_dept_telephone_number.dept_telephone_number_id) AS person_id
   FROM dept_telephone_number;

ALTER TABLE hotwire3."10_View/Phones/Phones" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Phones/Phones" TO phones_management, cos;



CREATE RULE hotwire3_view_telephone_management_del AS
    ON DELETE TO hotwire3."10_View/Phones/Phones" DO INSTEAD ( DELETE FROM mm_person_dept_telephone_number
  WHERE mm_person_dept_telephone_number.dept_telephone_number_id = old.id;
 DELETE FROM dept_telephone_number
  WHERE dept_telephone_number.id = old.id;
);

CREATE FUNCTION hotwire3.phones() RETURNS TRIGGER AS 
$$
begin
    if new.id is null then
        new.id := nextval('dept_telephone_number_id_seq');
        insert into dept_telephone_number ( id, extension_number, room_id, fax, personal_line ) VALUES ( new.id, new.extension_number, new.room_id, new.fax, new.personal_line );
    perform fn_mm_array_update(new.person_id,array[]::bigint[],'mm_person_dept_telephone_number'::varchar,'dept_telephone_number_id','person_id',new.id);
    else
        update dept_telephone_number set
            extension_number = new.extension_number,
            room_id = new.room_id,
            fax = new.fax,
            personal_line = new.personal_line
        where id = old.id;
    perform fn_mm_array_update(new.person_id,old.person_id,'mm_person_dept_telephone_number'::varchar,'dept_telephone_number_id','person_id',new.id);
    end if;
    return new;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.phones() OWNER TO dev;

CREATE TRIGGER hotwire3_view_telephone_management_ins INSTEAD OF INSERT OR UPDATE ON hotwire3."10_View/Phones/Phones" FOR EACH ROW EXECUTE PROCEDURE hotwire3.phones() ;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Phones/Phones', 'dept_telephone_number');
-- Installing 080_add_unknown_group_computers

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Unknown_Group_Computers" AS 
 SELECT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer as ro_manufacturer, hardware.model as ro_model, hardware.name AS ro_hardware_name, hardware.hardware_type_id as ro_hardware_type_id, system_image.operating_system_id as ro_operating_system_id, system_image.wired_mac_1 as ro_wired_mac_1, system_image.wired_mac_2 as ro_wired_mac_2, system_image.wireless_mac as ro_wireless_mac, ip_address_hid.ip_address_hid AS ro_mm_ip_address, hardware.asset_tag as ro_asset_tag, hardware.serial_number as ro_serial_number, hardware.monitor_serial_number as ro_monitor_serial_number, hardware.room_id as ro_room_id, system_image.user_id as ro_user_id, hardware.owner_id as ro_owner_id, system_image.research_group_id, system_image.host_system_image_id as ro_host_system_image_id, system_image.comments::text AS ro_system_image_comments, hardware.comments::text AS ro_hardware_comments
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address_hid USING (ip_address_id)
  WHERE system_image.research_group_id = 2 AND ip_address_hid.ip_address_hid !~~ '%personal.private%'::text;

ALTER TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" OWNER TO dev;
GRANT select,update ON TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" TO cos,groupitreps,headsofgroup;

CREATE RULE hotwire3_unknown_group_computers_upd AS ON UPDATE TO hotwire3."10_View/My_Groups/Unknown_Group_Computers" DO INSTEAD UPDATE system_image SET research_group_id = new.research_group_id WHERE id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/My_Groups/Unknown_Group_Computers', 'system_image');
-- Installing 081_add_chemnet_accounts_view

CREATE OR REPLACE VIEW hotwire3."10_View/People/ChemNet_Accounts" AS 
 SELECT user_account.id, coalesce(person.id, person_id) AS ro_person_id, user_account.username as ro_username, user_account.expiry_date as ro_expiry_date, user_account.is_disabled
   FROM user_account
left join person on person.crsid = user_account.username;

ALTER TABLE hotwire3."10_View/People/ChemNet_Accounts" OWNER TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/ChemNet_Accounts" TO cos;

CREATE OR REPLACE RULE hotwire3_view_user_account_upd AS
    ON UPDATE TO hotwire3."10_View/People/ChemNet_Accounts" DO INSTEAD  UPDATE user_account SET is_disabled = new.is_disabled
  WHERE user_account.id = old.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/ChemNet_Accounts', 'user_account' );
-- Installing 082_add_software_package_view

CREATE VIEW hotwire3.reboot_hid AS
SELECT reboot_hid.reboot_id, reboot_hid.reboot_hid
   FROM reboot_hid;
ALTER VIEW hotwire3.reboot_hid OWNER TO dev;
GRANT SELECT ON hotwire3.reboot_hid TO PUBLIC;

CREATE VIEW hotwire3.depended_software_package_hid AS
SELECT software_package_hid.software_package_id AS depended_software_package_id, software_package_hid.software_package_hid AS depended_software_package_hid
FROM hotwire3.software_package_hid;
ALTER VIEW hotwire3.depended_software_package_hid OWNER TO dev;
GRANT SELECT ON hotwire3.depended_software_package_hid TO PUBLIC;

CREATE VIEW hotwire3.updated_software_package_hid AS
SELECT software_package_hid.software_package_id AS updated_software_package_id, software_package_hid.software_package_hid AS updated_software_package_hid
FROM hotwire3.software_package_hid;
ALTER VIEW hotwire3.updated_software_package_hid OWNER TO dev;
GRANT SELECT ON hotwire3.updated_software_package_hid TO PUBLIC;

CREATE VIEW hotwire3.licence_hid AS
SELECT licence.id AS licence_id, (licence.short_name::text || 
        CASE
            WHEN licence.is_upgrade THEN ' [upgrade]'::text
            ELSE ''::text
        END) || 
        CASE
            WHEN licence.can_install_on_any_managed_machine THEN ' [global]'::text
            ELSE ''::text
        END AS licence_hid
   FROM licence;
ALTER VIEW hotwire3.licence_hid OWNER TO dev;
GRANT SELECT ON hotwire3.licence_hid TO PUBLIC;

CREATE VIEW hotwire3."10_View/Software/Packages" AS 
 SELECT software_package.id, software_package.name, software_package.wpkg_package_name, software_package.munki_package_name, software_package.revision, software_package.priority, software_package.reporting_string, software_package.reporting_version, software_package.reporting_path, software_package.min_os_ver, software_package.max_os_ver, software_package.reboot_id, software_package.sitelicence, software_package.on_all_macs, ARRAY( SELECT mm_software_package_can_install_on_os.operating_system_id
           FROM mm_software_package_can_install_on_os
          WHERE mm_software_package_can_install_on_os.software_package_id = software_package.id) AS operating_system_id, ARRAY( SELECT mm_software_package_depends_on.depended_software_package_id
           FROM mm_software_package_depends_on
          WHERE mm_software_package_depends_on.depending_software_package_id = software_package.id) AS depended_software_package_id, ARRAY( SELECT mm_software_package_installs_on_architecture.architecture_id
           FROM mm_software_package_installs_on_architecture
          WHERE mm_software_package_installs_on_architecture.software_package_id = software_package.id) AS architecture_id, ARRAY( SELECT mm_software_package_updates_software_package.updated_software_package_id
           FROM mm_software_package_updates_software_package
          WHERE mm_software_package_updates_software_package.updating_software_package_id = software_package.id) AS updated_software_package_id, ARRAY( SELECT mm_software_package_is_licensed_by_licence.licence_id
           FROM mm_software_package_is_licensed_by_licence
          WHERE mm_software_package_is_licensed_by_licence.software_package_id = software_package.id) AS licence_id
   FROM software_package;

ALTER TABLE hotwire3."10_View/Software/Packages" OWNER TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Software/Packages" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_id bigint;

        begin
                -- Detect update
	if v.id is not null then
		v_id = v.id;
		update software_package set
			reporting_string=v.reporting_string,
			name=v.name,
			revision=v.revision,
			priority=v.priority,
			reporting_version=v.reporting_version,
			wpkg_package_name=v.wpkg_package_name,
			munki_package_name=v.munki_package_name,
			min_os_ver=v.min_os_ver,
			max_os_ver=v.max_os_ver,
			reporting_path=v.reporting_path,
			reboot_id=v.reboot_id,
			sitelicence=v.sitelicence,
			on_all_macs=v.on_all_macs
		where id=v.id;
	else
		v_id=nextval('software_package_id_seq');
		insert into software_package (
			id,
			reporting_string,
			name,
			revision,
			priority,
			reporting_version,
			wpkg_package_name,
			munki_package_name,
			min_os_ver,
			max_os_ver,
			reporting_path,
			reboot_id,
			sitelicence,
			on_all_macs
		) values (
			v_id,
			v.reporting_string,
			v.name,
			v.revision,
			v.priority,
			v.reporting_version,
			v.wpkg_package_name,
			v.munki_package_name,
			v.min_os_ver,
			v.max_os_ver,
			v.reporting_path,
			v.reboot_id,
			v.sitelicence,
			v.on_all_macs
		);
	end if;
	perform fn_mm_update('mm_software_package_can_install_on_os'::varchar,
			     'operating_system_id'::varchar,v.operating_system_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_depends_on'::varchar,
	                     'depended_software_package_id'::varchar, v.depended_software_package_id,
	                     'depending_software_package_id'::varchar, v_id);
	perform fn_mm_update('mm_software_package_installs_on_architecture'::varchar,
			     'architecture_id'::varchar,v.architecture_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_updates_software_package'::varchar,
			     'updated_software_package_id'::varchar,v.updated_software_package_id,
			     'updating_software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_is_licensed_by_licence'::varchar,
			     'licence_id'::varchar,v.licence_id,
			     'software_package_id'::varchar,v_id);
-- 	perform fn_mm_update('mm_system_image_has_installed_software_package'::varchar,
-- 			     'system_image_id'::varchar,v.system_image_id,
-- 			     'software_package_id'::varchar,v_id);
        return v_id;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages") OWNER TO dev;


CREATE RULE wpkg_software_del AS
    ON DELETE TO hotwire3."10_View/Software/Packages" DO INSTEAD  DELETE FROM software_package
  WHERE software_package.id = old.id;

CREATE RULE wpkg_software_ins AS
    ON INSERT TO hotwire3."10_View/Software/Packages" DO INSTEAD  SELECT hotwire3.software_package_insupd(new.*) as id;

CREATE RULE wpkg_software_upd AS
    ON UPDATE TO hotwire3."10_View/Software/Packages" DO INSTEAD  SELECT hotwire3.software_package_insupd(new.*) as id;


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Software/Packages', 'software_package' );
