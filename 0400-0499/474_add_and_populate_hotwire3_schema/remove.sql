-- removing 082_add_software_package_view

DROP VIEW hotwire3.reboot_hid;
DROP VIEW hotwire3.depended_software_package_hid;
DROP VIEW hotwire3.updated_software_package_hid;
DROP VIEW hotwire3.licence_hid;
DROP RULE wpkg_software_ins ON hotwire3."10_View/Software/Packages";
DROP RULE wpkg_software_upd ON hotwire3."10_View/Software/Packages";
DROP FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages");
DROP VIEW hotwire3."10_View/Software/Packages"; 
DELETE FROM hotwire3._primary_table where view_name = '10_View/Software/Packages';
-- removing 081_add_chemnet_accounts_view

DROP VIEW hotwire3."10_View/People/ChemNet_Accounts";
DELETE FROM hotwire3._primary_table where view_name = '10_View/People/ChemNet_Accounts';
-- removing 080_add_unknown_group_computers

DROP VIEW hotwire3."10_View/My_Groups/Unknown_Group_Computers";
DELETE from hotwire3._primary_table where view_name = '10_View/My_Groups/Unknown_Group_Computers';
-- removing 079_add_phones_view

DROP VIEW hotwire3."10_View/Phones/Phones";
DROP FUNCTION hotwire3.phones();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Phones/Phones';
-- removing 078_add_staff_reviews

DROP VIEW hotwire3.usual_reviewer_hid;
DROP VIEW hotwire3."10_View/People/Staff_Reviews"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Staff_Reviews';
-- removing 077_network_sockets

DROP VIEW hotwire3.cable_type_hid;
DROP VIEW hotwire3.connector_type_hid;
DROP VIEW hotwire3.linked_socket_hid;
DROP VIEW hotwire3.switch_port_hid;
DROP VIEW hotwire3."10_View/Network/Sockets"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Sockets';
DROP FUNCTION hotwire3.network_socket() ;
-- removing 076_add_firstaiders_view

DROP VIEW hotwire3.firstaider_funding_hid;
DROP VIEW hotwire3."10_View/Safety/Firstaiders"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Firstaiders';
-- removing 075_add_fire_warden_views

DROP RULE fire_warden_area_ins ON hotwire3."10_View/Safety/Fire_warden_areas";
DROP RULE fire_warden_area_upd ON hotwire3."10_View/Safety/Fire_warden_areas";
DROP FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas");
DROP VIEW hotwire3."10_View/Safety/Fire_warden_areas";

DROP RULE fire_warden_ins ON hotwire3."10_View/Safety/Fire_wardens";
DROP RULE fire_warden_upd ON hotwire3."10_View/Safety/Fire_wardens";
DROP FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens");
DROP VIEW hotwire3."10_View/Safety/Fire_wardens";

DROP VIEW hotwire3.fire_warden_area_hid;
DROP VIEW hotwire3.deputy_fire_warden_hid;
DROP VIEW hotwire3.fire_warden_hid;

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Fire_wardens';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Fire_warden_areas';
-- removing 074_add_safety_views

DROP VIEW hotwire3.recirc_or_ducted_hid;
DROP VIEW hotwire3.firetrace_type_hid;
DROP VIEW hotwire3.extract_device_type_hid;
DROP VIEW hotwire3.extract_arm_canopy_type_hid;
DROP VIEW hotwire3.vented_enclosure_device_type_hid;
DROP VIEW hotwire3."10_View/Safety/Extract_Arms_and_Canopies"; 
DROP VIEW hotwire3."10_View/Safety/Fume_Hoods"; 
DROP VIEW hotwire3."10_View/Safety/Glove_Boxes"; 
DROP VIEW hotwire3."10_View/Safety/Vented_Enclosures"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Vented_Enclosures';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Extract_Arms_and_Canopies';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Safety/Glove_Boxes';
-- removing 073_add_rooms_occupancy

DROP VIEW hotwire3."10_View/Rooms/_contact_details";
DROP VIEW hotwire3."10_View/Rooms/_fume_hoods";
DROP VIEW hotwire3."10_View/Rooms/Room_Occupants"; 
DELETE FROM hotwire3._primary_table where view_name = '10_View/Rooms/Room_Occupants';
DROP VIEW hotwire3.fume_hood_type_hid;
DROP VIEW hotwire3.fume_hood_device_type_hid;
-- removing 072_add_rooms_attributes

DROP VIEW hotwire3.building_hid;
DROP VIEW hotwire3.building_floor_hid;
DROP VIEW hotwire3.building_region_hid;
DROP VIEW hotwire3.room_type_hid;
DROP VIEW hotwire3.responsible_group_hid;
DROP VIEW hotwire3.responsible_person_hid;
DROP VIEW hotwire3."10_View/Rooms/Rooms_Attributes"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Rooms/Rooms_Attributes';
-- removing 071_add_visitors_view

DROP VIEW hotwire3."10_View/Roles/Visitors";
DROP VIEW hotwire3.visitor_type_hid;
DROP VIEW hotwire3.host_hid;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Visitors';
-- removing 070_add_postgrad_students_view

DROP VIEW hotwire3.co_mentor_hid;
DROP VIEW hotwire3.first_mentor_hid;
DROP VIEW hotwire3.second_mentor_hid;
DROP VIEW hotwire3.first_supervisor_hid;
DROP VIEW hotwire3.second_supervisor_hid;
DROP VIEW hotwire3.substitute_supervisor_hid;
DROP VIEW hotwire3.postgraduate_studentship_type_hid;

DROP VIEW hotwire3."10_View/Roles/Postgrad_Students"; 

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Postgrad_Students';
-- removing 069_add_post_history_view

DROP VIEW hotwire3."10_View/Roles/Post_History";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Post_History';
DROP VIEW hotwire3.mentor_hid;
DROP VIEW hotwire3.staff_category_hid;
-- removing 068_add_part_iii_view

DROP VIEW hotwire3."10_View/Roles/Part_III_Students";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Part_III_Students';
-- removing 067_add_bulk_supervisors_update

DROP VIEW hotwire3.old_supervisor_hid;
DROP VIEW hotwire3.new_supervisor_hid;

DROP RULE update_supervisors ON hotwire3."10_View/Roles/Bulk_update_supervisors";
DROP FUNCTION hotwire3.update_supervisors(hotwire3."10_View/Roles/Bulk_update_supervisors");
DROP VIEW hotwire3."10_View/Roles/Bulk_update_supervisors";
DROP VIEW hotwire3."10_View/Roles/_current_people_supervised_by";

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Bulk_update_supervisors';
-- removing 066_add_erasmus_students_view

DROP VIEW hotwire3."10_View/Roles/Erasmus_Students" ;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Roles/Erasmus_Students';
DROP VIEW hotwire3.erasmus_type_hid;
DROP VIEW hotwire3.role_status_hid;
-- removing 065_add_research_interest_group_view


DROP VIEW hotwire3."10_View/Groups/Research_Interest_Group";

DROP FUNCTION hotwire3.primary_rig_update(newval bigint[],oldval bigint[],myid bigint);

DROP FUNCTION hotwire3.research_interest_group_ins();

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Groups/Research_Interest_Group';

DROP VIEW hotwire3.rig_member_hid;
DROP VIEW hotwire3.primary_member_hid;
DROP VIEW hotwire3.chair_hid;
-- removing 064_add_groups_software_licence_declarations

DROP VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration";
DROP FUNCTION hotwire3.group_software_licence_decl_trig();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/My_Groups/Software_Licence_Declaration';
-- removing 063_add_groups_computer_reps_view

REVOKE SELECT ON hotwire3.head_of_group_hid,hotwire3.computer_rep_hid FROM groupitreps,headsofgroup;

DROP VIEW  hotwire3."10_View/My_Groups/Computer_reps";

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/My_Groups/Computer_reps';
-- removing 062_add_research_groups_computing_view

DROP VIEW hotwire3."10_View/Groups/Research_Groups_Computing";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Groups/Research_Groups_Computing';
DROP FUNCTION hotwire3.groups_research_groups_computing_ins();
DROP VIEW hotwire3.group_fileserver_hid;
-- removing 061_add_research_groups_view

DROP VIEW hotwire3."10_View/Groups/Research_Groups";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Groups/Research_Groups';
DROP FUNCTION hotwire3.groups_research_groups_ins();
DROP VIEW hotwire3.head_of_group_hid;
DROP VIEW hotwire3.deputy_head_of_group_hid;
DROP VIEW hotwire3.alternate_admin_contact_hid;
DROP VIEW hotwire3.computer_rep_hid;
-- removing 060_add_printer_classes_view

DROP VIEW hotwire3."10_View/Computers/Printer_Classes";
DROP FUNCTION "hotwire3.printer_classes_ins_trig_fn"();
DROP VIEW hotwire3."10_View/Computers/_Printers_summary";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Printer_Classes';
-- removing 059_add_printers_view

DROP VIEW hotwire3."10_View/Computers/Printers";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Printers';
DROP VIEW hotwire3.printer_class_hid;
-- removing 058_add_website_view

DROP VIEW hotwire3."10_View/People/Website_View";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Website_View';
DROP VIEW hotwire3.website_staff_category_hid;
-- removing 057_add_swipe_cards_view

DROP VIEW hotwire3."10_View/People/Swipe_Cards";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Swipe_Cards';
-- removing 056_add_research_group_contact_list_view

DROP VIEW hotwire3."10_View/People/Contact_Details_By_Group";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Contact_Details_By_Group';
-- removing 055_add_photography_registration_view

DROP VIEW hotwire3."10_View/People/Photography_Registration";
DROP FUNCTION hotwire3.photography_registration_update();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Photography_Registration';
-- removing 054_add_personnel_phone_view

DROP VIEW hotwire3."10_View/People/Personnel_Phone" ;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Personnel_Phone';
-- removing 053_add_personnel_history_view

DROP VIEW hotwire3."10_View/People/Personnel_History";
DROP VIEW hotwire3."10_View/Roles/_Cambridge_History_V2";
DROP VIEW hotwire3."10_View/People/_Staff_Review_History";
DROP VIEW hotwire3.reviewer_hid;
DELETE FROm hotwire3._primary_table WHERE view_name = '10_View/People/Personnel_History';
-- removing 052_add_personnel_data_entry_view

DROP RULE hotwire3_view_personnel_data_entry_ins ON hotwire3."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire3_view_personnel_data_entry_del ON hotwire3."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire3_view_personnel_data_entry_upd ON hotwire3."10_View/People/Personnel_Data_Entry";

DROP FUNCTION hotwire3.personnel_data_entry_upd(hotwire3."10_View/People/Personnel_Data_Entry");

DROP VIEW hotwire3."10_View/People/Personnel_Data_Entry";

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Personnel_Data_Entry';

DROP VIEW hotwire3.co_supervisor_hid;
-- removing 051_add_cos_people_view

DROP VIEW hotwire3."10_View/People/COs_View";
DROP FUNCTION hotwire3.people_cos_view_trig();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/COs_View';
DROP VIEW hotwire3.supervisor_hid;
-- removing 050_add_people_all_contact_details

DROP VIEW hotwire3."10_View/People/All_Contact_Details";
DELETE FROM hotwire3._primary_table where view_name = '10_View/People/All_Contact_Details';
DROP VIEW hotwire3.gender_hid;
DROP VIEW hotwire3.post_category_hid;
DROP VIEW hotwire3.physical_status_hid;
DROP VIEW hotwire3.cambridge_college_hid;
DROP VIEW hotwire3.nationality_hid;
-- removing 049_add_patch_panel_view

DROP VIEW hotwire3."10_View/Network/Patch_Panels";
DELETE FROM hotwire3._primary_table where view_name = '10_View/Network/Patch_Panels';
DROP VIEW hotwire3.patch_panel_type_hid;
-- removing 048_add_vlan_view

DROP VIEW hotwire3."10_View/Network/VLANs";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/VLANs';
-- removing 047_add_switch_port_config_goals

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Port_Config_Goals';
DROP VIEW hotwire3.goal_class_hid;
-- removing 046_add_switch_port_config_fragments_view

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments";
DROP FUNCTION hotwire3.network_switch_port_config_fragment_ins();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Port_Config_Fragments';
DROP VIEW hotwire3.switch_port_config_goal_hid;
-- removing 045_add_switch_config_goals_view

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Config_Goals";
DROP FUNCTION hotwire3.switch_config_goals_ins();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Config_Goals';

-- removing 044_add_switch_models_view

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Models";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Models';
-- removing 043_add_switch_config_fragments_view

DROP TRIGGER switch_config_fragments_ins ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments";
DROP FUNCTION hotwire3.switch_config_fragments_ins();
DROP VIEW hotwire3."10_View/Network/Switches/Switch_Config_Fragments";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Config_Fragments';
-- removing 042_add_switch_view

DROP VIEW hotwire3."10_View/Network/Switches/Switches";
DROP VIEW hotwire3."10_View/Network/Switches/_Switchport_ro";
DROP VIEW hotwire3."10_View/Network/Switches/_switch_config_subview";
DROP VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro";
DROP VIEW hotwire3."10_View/Network/Switches/Switch_Ports";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switches';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Configs_ro';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Switches/Switch_Ports';
DROP VIEW hotwire3.switchstack_hid;
DROP VIEW hotwire3.switch_config_goal_hid;
DROP VIEW hotwire3.switch_hid;
DROP VIEW hotwire3.socket_hid;
DROP VIEW hotwire3.patch_panel_hid;
DROP VIEW hotwire3.speed_hid;
DROP VIEW hotwire3.switch_auth_hid;
DROP VIEW hotwire3.switch_port_goal_hid;
-- removing 041_add_subnet_view

DROP VIEW hotwire3.vlan_hid;
DROP VIEW hotwire3.domain_for_dns_server_hid;
DROP VIEW hotwire3."10_View/Network/Subnets";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Subnets';
-- removing 040_add_mac_to_vlan_view

DROP VIEW hotwire3.vlan_vid_hid;
DROP VIEW hotwire3.infraction_type_hid;
DROP VIEW hotwire3."10_View/Network/MAC_to_VLAN";
DROP VIEW hotwire3."10_View/Network/_vlanchange_ro";
DELETE FROm hotwire3._primary_table WHERE view_name = '10_View/Network/MAC_to_VLAN';
-- removing 039_add_network_gbic_view

DROP VIEW hotwire3.network_electrical_connection_hid;
DROP VIEW hotwire3."10_View/Network/GBICs";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/GBICs';
-- removing 038_add_cabinet_view

DROP VIEW hotwire3.fibre_test_type_hid;
DROP VIEW hotwire3."10_View/Network/Fibres/_FibreCore_ro";
DROP VIEW hotwire3.fibre_run_hid;
DROP VIEW hotwire3.cabinet_hid;
DROP VIEW hotwire3.fibre_termination_hid;
DROP VIEW hotwire3.fibre_core_hid;
DROP VIEW hotwire3.fibre_panel_a_hid;
DROP VIEW hotwire3.fibre_panel_b_hid;
DROP VIEW hotwire3.fibre_termination_a_hid;
DROP VIEW hotwire3.fibre_termination_b_hid;
DROP VIEW hotwire3.fibre_core_type_hid;
DROP VIEW hotwire3.fibre_type_hid;

DROP VIEW hotwire3."10_View/Network/Fibres/_FibreRuns_ro";

DROP VIEW hotwire3."10_View/Network/Fibres/Tests";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Fibres/Tests';

DROP VIEW hotwire3."10_View/Network/Fibre_Panels";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Fibre_Panels';

DROP VIEW hotwire3."10_View/Network/Fibres/10_Fibre_Runs";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Fibres/10_Fibre_Runs';

DROP VIEW hotwire3."10_View/Network/Fibres/_FibreCore_for_Cabinet_ro";

DROP VIEW hotwire3."10_View/Network/Fibres/15_Fibre_Cores";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Fibres/15_Fibre_Cores';

DROP VIEW hotwire3."10_View/Network/Cabinets";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/Cabinets';

-- removing 037_add_mailing_list_views

DROP VIEW hotwire3."10_View/Email/Mailing_List_OptIns";
DROP VIEW hotwire3."10_View/Email/Mailing_List_OptOuts";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Mailing_List_OptIns';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Mailing_List_OptOuts';
DROP VIEW hotwire3.include_person_hid;
DROP VIEW hotwire3.exclude_person_hid;
-- removing 036_add_managed_mail_domain_view

DROP VIEW hotwire3."10_View/Email/Chemistry_Mail_Domain";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Chemistry_Mail_Domain';

-- removing 035_add_it_sales_view

DROP VIEW hotwire3.itsales_type_hid;
DROP VIEW hotwire3.itsales_unit_hid;
DROP VIEW hotwire3.itsales_status_hid;
DROP VIEW hotwire3."10_View/IT_Sales/Sales"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/IT_Sales/Sales';
-- removing 034_add_xymon_client_cfg_view

DROP VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg" ;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Xymon_Clients_Cfg';
-- removing 033_add_group_fileservers

DROP VIEW hotwire3."10_View/Groups/Group_Fileservers";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Groups/Group_Fileservers';
DROP VIEW hotwire3.os_class_hid;
-- removing 032_add_group_computers_autoinstaller

DROP VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/My_Groups/Computer_Autoinstallation';

-- removing 031_add_group_computers_view

DROP VIEW hotwire3."10_View/My_Groups/Computers";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/My_Groups/Computers';
-- removing 030_add_titles

DROP VIEW hotwire3."10_View/People/Titles";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Titles';
-- removing 029_add_hardware_types

DROP VIEW hotwire3."10_View/Computers/Hardware_Types";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Hardware_Types';
-- removing 028_add_switch_model_hid

DROP VIEW hotwire3.switch_model_hid;
-- removing 027_add_easy_add_virtual_machine_view

DROP VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";
DROP FUNCTION hotwire3.upd_easy_add_virtual_machine();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Easy_Add_Virtual_Machine';
-- removing 026_add_easy_add_machine_view

DROP VIEW hotwire3."10_View/Computers/Easy_Add_Machine";
DROP VIEW hotwire3.easy_addable_subnet_hid;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Easy_Add_Machine';
-- removing 025_add_data_protection_submenu

drop view hotwire3."10_View/Data_Protection/Access_Control_Lists";
drop view hotwire3."10_View/Data_Protection/Access_Control_Groups";
drop view hotwire3."10_View/Data_Protection/Database_Users"; 
-- removing 024_add_dns_submenu

DROP VIEW hotwire3."10_View/DNS/MX_Records";
DROP VIEW hotwire3."10_View/DNS/CNames";
DROP VIEW hotwire3."10_View/DNS/A_Records";
DROP VIEW hotwire3.dns_domain_hid;
-- removing 023_add_buildings_region

DROP VIEW hotwire3."10_View/Buildings/Regions";
-- removing 022_add_buildings_floor

DROP VIEW hotwire3."10_View/Buildings/Floors";
-- removing 021_add_buildings_building

DROP VIEW hotwire3."10_View/Buildings/Buildings";
-- removing 020_add_strategic_goals

DROP VIEW hotwire3."10_View/10_IT_Tasks/Strategic Goals";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/10_IT_Tasks/Strategic Goals';
-- removing 019_add_school_it_report

DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//01_header";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//10_projects";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//20_projects";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//30_projects";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr";
DROP VIEW hotwire3."10_View/10_IT_Tasks/Report_for_School_IT//90_completed";

-- removing 018_add_it_projects

DROP VIEW hotwire3."10_View/10_IT_Tasks/IT_Projects";
DROP FUNCTION hotwire3.it_projects_upd();
delete from hotwire3._primary_table where view_name = '10_View/10_IT_Tasks/IT_Projects';

-- removing 017_add_hardware

DROP VIEW hotwire3."10_View/Computers/Hardware" ;
DROP VIEW hotwire3."10_View/Computers/System_Image/_System_Instances_ro";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Hardware';
-- removing 016_add_operating_systems

DROP VIEW hotwire3."10_View/Computers/Operating_Systems";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Operating_Systems';

-- removing 015_add_ip_address

DROP VIEW hotwire3."10_View/Network/IP_addresses";
DROP VIEW hotwire3.subnet_hid ;
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Network/IP_Addresses';
-- removing 014_add_system_image_chemnet

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Machine_ChemNet_Tokens';
DROP VIEW hotwire3."10_View/Computers/Machine_ChemNet_Tokens";
-- removing 013_add_system_image_autoinstaller

DROP VIEW hotwire3.installer_tag_hid ;
DROP VIEW hotwire3.architecture_hid ;
DROP VIEW hotwire3.software_package_hid ;
DROP VIEW hotwire3.fai_class_hid ;
DROP VIEW hotwire3.netboot_hid ;
DROP VIEW hotwire3."10_View/Computers/Autoinstallation"; 
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Autoinstallation';
-- removing 012_add_system_image_basic

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Virtual_Machines';
DROP VIEW hotwire3."10_View/Computers/Virtual_Machines";

-- removing 011_add_it_tasks

DELETE FROM hotwire3._primary_table where view_name = '10_View/10_IT_Tasks/Tasks';
DROP VIEW hotwire3."10_View/10_IT_Tasks/_audit_ro";
DROP TRIGGER hotwire3_10_view_10_it_tasks_tasks_ins on hotwire3."10_View/10_IT_Tasks/Tasks";
DROP TRIGGER hotwire3_10_view_10_it_tasks_tasks_upd on hotwire3."10_View/10_IT_Tasks/Tasks";
DROP RULE hotwire3_10_view_10_it_tasks_tasks_del on hotwire3."10_View/10_IT_Tasks/Tasks";
DROP FUNCTION hotwire3.it_tasks_upd();
DROP VIEW hotwire3."10_View/10_IT_Tasks/Tasks";
DROP VIEW hotwire3."IT_task_leader_hid";
DROP VIEW hotwire3.computer_officer_hid;
DROP VIEW hotwire3."Primary_strategic_goal_hid";
DROP VIEW hotwire3."IT_strategic_goal_hid";
DROP VIEW hotwire3.it_strategic_goal_hid;
DROP VIEW hotwire3.it_project_hid;
DROP VIEW hotwire3.time_estimate_hid;
DROP VIEW hotwire3."IT_status_hid";
DROP VIEW hotwire3.predecessor_task_hid;
DROP VIEW hotwire3.it_task_hid;
-- removing 010_add_system_image_all

UPDATE hotwire3."hw_User Preferences" SET preference_value = '10_View/System_Image_All' where
 preference_value = '10_View/Computers/System_Instances';
DROP TRIGGER hotwire3_view_system_image_all_upd ON hotwire3."10_View/Computers/System_Instances";
DROP TRIGGER hotwire3_view_system_image_all_ins ON hotwire3."10_View/Computers/System_Instances";
DROP RULE hotwire3_view_system_image_all_del ON hotwire3."10_View/Computers/System_Instances";
DROP FUNCTION hotwire3.system_image_all_upd();
DROP VIEW hotwire3."10_View/Computers/System_Instances";
DROP VIEW hotwire3."10_View/Computers/System_Image/_Hardware_ro";
DROP VIEW hotwire3."10_View/Computers/System_Image/_Contact_Details_ro";
DROP VIEW hotwire3."10_View/Computers/System_Image/_IP_address_ro";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/System_Instances';
-- removing 009_add_subview_things

DROP FUNCTION hotwire3.to_hwsubviewb(varchar,varchar,varchar,varchar,varchar);
-- removing 008_add_hw3_hid_views

DROP VIEW hotwire3.hardware_hid;
DROP VIEW hotwire3.hardware_type_hid;
DROP VIEW hotwire3.operating_system_hid;
DROP VIEW hotwire3.person_hid;
DROP VIEW hotwire3.title_hid;
DROP VIEW hotwire3.user_hid;
DROP VIEW hotwire3.owner_hid;
DROP VIEW hotwire3.research_group_hid;
DROP VIEW hotwire3.host_system_image_hid;
DROP VIEW hotwire3.ip_address_hid;
DROP VIEW hotwire3.room_hid;
DROP VIEW hotwire3.dept_telephone_number_hid;
DROP VIEW hotwire3.system_image_hid;
-- removing 007_hw3_suggest_view

DROP FUNCTION hotwire3.suggest_view(character varying, character varying, character varying, character varying);
-- removing 006_add_hw3_preferences_views

DROP VIEW hotwire3._preferences;
DROP VIEW hotwire3."90_Action/Hotwire/All Possible Preferences";
DROP VIEW hotwire3."90_Action/Hotwire/User Preferences";
DROP VIEW hotwire3.preference_hid;
DROP VIEW hotwire3."Postgres_User_hid";
-- removing 005_add_hw3_column_data

DROP VIEW hotwire3._column_data;
-- removing 004_add_hw3_role_data

DROP VIEW hotwire3._role_data;
-- removing 003_add_hw3_view_data

DROP VIEW hotwire3._view_data;
DROP TABLE hotwire3._primary_table;
-- removing 002_add_hw3_preferences_tables

DROP TABLE hotwire3."hw_User Preferences";
DROP TABLE hotwire3."hw_Preferences";
DROP TABLE hotwire3.hw_preference_type_hid;
-- removing 001_add_hotwire3_schema

DROP SCHEMA hotwire3;
