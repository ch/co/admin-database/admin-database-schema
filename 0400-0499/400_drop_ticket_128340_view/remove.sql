CREATE OR REPLACE VIEW apps.ticket_128340 AS 
 SELECT DISTINCT p.surname, p.first_names, p.crsid, p.email_address, p.forwarding_address, p.new_employer_address, p.arrival_date, p.leaving_date, r.post_category, s.person_hid AS supervisor, p.date_of_birth, p.permission_to_give_out_details, p.notes
   FROM person p
   LEFT JOIN _all_roles_v11 r ON p.id = r.person_id
   LEFT JOIN person_hid s ON r.supervisor_id = s.person_id
   LEFT JOIN _physical_status_v3 ps ON p.id = ps.person_id
  WHERE (r.post_category_id = 'sc-7'::text OR r.post_category_id = 'sc-5'::text OR r.post_category_id = 'v-12'::text OR r.post_category_id = 'sc-6'::text OR r.post_category_id = 'sc-4'::text) AND ps.status_id::text = 'Past'::text
  ORDER BY p.surname, p.first_names;

ALTER TABLE apps.ticket_128340
  OWNER TO cen1001;

