CREATE OR REPLACE VIEW personnel_basic_view AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname, person.first_names, person.title_id, person.name_suffix, person.email_address, person.crsid, person.date_of_birth, _latest_role_v8.post_category_id AS ro_post_category_id, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, person.location, _physical_status_v2.status_id AS ro_physical_status_id, person.extra_filemaker_data, person.is_spri, 
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id
   LEFT JOIN _physical_status_v2 USING (id)
   LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   LEFT JOIN research_group_hid USING (research_group_id)
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid USING (room_id)
   LEFT JOIN person_hid ON person_hid.person_id = person.id
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_basic_view
  OWNER TO cen1001;
GRANT ALL ON TABLE personnel_basic_view TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE personnel_basic_view TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE personnel_basic_view TO cos;

CREATE OR REPLACE FUNCTION fn_personnel_basic_upd(personnel_basic_view)
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    
                      v_person_id BIGINT;                                                                                        
      begin                                                                                                              
      IF v.id IS NOT NULL THEN                                                                                           
        v_person_id= v.id;                                                                                               
        UPDATE person SET                                                                                                
      surname=v.surname, first_names=v.first_names, title_id=v.title_id,                                                       
      name_suffix = v.name_suffix,
      email_address=v.email_address, crsid=v.crsid, date_of_birth=v.date_of_birth, location=v.location,                                                 
      extra_filemaker_data=v.extra_filemaker_data,is_spri=v.is_spri                                                                        
        WHERE person.id = v.id;                                                                                          
                                                                                                          ELSE    
          v_person_id= nextval('person_id_seq');                                                
          INSERT INTO person (id, surname, first_names, title_id, name_suffix, email_address,crsid, date_of_birth, location, extra_filemaker_data,is_spri) VALUES (v_person_id, v.surname, v.first_names, v.title_id, v.name_suffix, v.email_address,v.crsid, v.date_of_birth, v.location, v.extra_filemaker_data,v.is_spri); 

        END IF; 

      PERFORM fn_upd_many_many (v_person_id, v.mm_research_group, 'person',                                              
      'research_group');                                                                                                 
      PERFORM fn_upd_many_many (v_person_id, v.mm_dept_telephone_number, 'person',                                       
      'dept_telephone_number');                                                                                          
      PERFORM fn_upd_many_many (v_person_id, v.mm_room, 'person', 'room');                                               
      return v_person_id;                                                                                                
      end;                                                

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_personnel_basic_upd(personnel_basic_view)
  OWNER TO cen1001;

CREATE OR REPLACE RULE personnel_basic_del AS
    ON DELETE TO personnel_basic_view DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

CREATE OR REPLACE RULE personnel_basic_ins AS
    ON INSERT TO personnel_basic_view DO INSTEAD  SELECT fn_personnel_basic_upd(new.*) AS id;

CREATE OR REPLACE RULE personnel_basic_upd AS
    ON UPDATE TO personnel_basic_view DO INSTEAD  SELECT fn_personnel_basic_upd(new.*) AS id;


