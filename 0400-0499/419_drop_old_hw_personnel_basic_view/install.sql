DROP RULE personnel_basic_del ON personnel_basic_view;
DROP RULE personnel_basic_ins ON personnel_basic_view;
DROP RULE personnel_basic_upd ON personnel_basic_view;
DROP FUNCTION fn_personnel_basic_upd(personnel_basic_view);
DROP VIEW public.personnel_basic_view;
