-- View: www._all_roles_v1

DROP VIEW www._all_roles_v1;

CREATE OR REPLACE VIEW www._all_roles_v1 AS 
 SELECT _all_roles_v12.person_id, _all_roles_v12.start_date, _all_roles_v12.intended_end_date, _all_roles_v12.funding_end_date, _all_roles_v12.estimated_leaving_date, _all_roles_v12.end_date, _all_roles_v12.post_category_id, _all_roles_v12.supervisor_id, _all_roles_v12.weight, _all_roles_v12.status
   FROM _all_roles_v12
   JOIN person ON _all_roles_v12.person_id = person.id
  WHERE person.do_not_show_on_website IS NOT TRUE;

ALTER TABLE www._all_roles_v1
  OWNER TO dev;
GRANT ALL ON TABLE www._all_roles_v1 TO dev;
GRANT SELECT ON TABLE www._all_roles_v1 TO www_sites;


-- Function: www.past_people_supervised_by(text)

-- DROP FUNCTION www.past_people_supervised_by(text);

CREATE OR REPLACE FUNCTION www.past_people_supervised_by(IN supervisor_crsid text)
  RETURNS TABLE(id bigint, crsid character varying, post_category_hid text, startdate date, enddate date) AS
$BODY$
BEGIN
return query
select person.id, person.crsid, h.post_category_hid, roles.start_date, roles.end_date from 
(select person_id, max(post_category_weight) from www._latest_role_v1 group by person_id) as lr 
inner join www._latest_role_v1 ls on lr.person_id=ls.person_id and lr.max=ls.post_category_weight 
inner join (select min(start_date) as start_date, max(coalesce(nullif(end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date)) as end_date, person_id from www._all_roles_v1 join person on person.id=supervisor_id and person.crsid = $1  group by person_id, supervisor_id) as roles on lr.person_id=roles.person_id 
inner join person on person.id=lr.person_id
inner join www.post_category_hid as h on h.post_category_id=ls.post_category_id
where roles.end_date<now() and person.crsid is not null and person.do_not_show_on_website IS FALSE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION www.past_people_supervised_by(text)
  OWNER TO alt36;

-- Function: www.latest_role_for_supervisor(text, text)

CREATE OR REPLACE FUNCTION www.latest_role_for_supervisor(
    IN _person text,
    IN _supervisor text,
    OUT post_category character varying,
    OUT post_category_weight integer)
  RETURNS record AS
$BODY$
declare
  _person_id int;
  _supervisor_id int;
  _category_id varchar;
begin

  _person_id := (select id from person where crsid=_person);
  _supervisor_id := (select id from person where crsid=_supervisor);
  _category_id := (select www._all_roles_v1.post_category_id from www._all_roles_v1 left join www.post_category_weights on post_category_weights.post_category_id=_all_roles_v1.post_category_id where person_id=_person_id and supervisor_id=_supervisor_id
                 order by coalesce(nullif(end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date) desc, post_category_weights.weight desc limit 1);
                 
  post_category := (select post_category_hid from www.post_category_hid where post_category_id=_category_id);
  post_category_weight := (select weight from www.post_category_weights where post_category_id=_category_id);

  -- What if _person = _supervisor? A person does not supervise themselves...
  -- Or, indeed, if e.g. we have a person on a website not supervised by one of the group leaders for some reason
  -- Fall back to whatever their research_group_post_category is if we get here

  -- NB we absolutely need this to return just one row. We have people who have e.g. two roles with the same
  -- start/end dates and the same post_category_weight, which leads to multiple rows in 
  -- www.person_info_v8 due to the join on _latest_role views. This is a dirty workaround but its the
  -- best I've come up with so far

  IF post_category is null then
    post_category := (select research_group_post_category from www.person_info_v8 where crsid=_person limit 1);
    post_category_weight := (select max(weight) from www.post_category_weights join www.post_category_hid using (post_category_id) where post_category_hid=post_category);
  END IF;

  IF post_category_weight is null then
    post_category_weight := 0;
  END IF;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION www.latest_role_for_supervisor(text, text)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO dev;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO public;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO www_sites;



-- Function: www.current_people_supervised_by(text)

-- DROP FUNCTION www.current_people_supervised_by(text);

CREATE OR REPLACE FUNCTION www.current_people_supervised_by(IN supervisor_crsid text)
  RETURNS TABLE(id bigint, crsid character varying, post_category_hid text) AS
$BODY$
BEGIN
return query
select person.id, person.crsid, h.post_category_hid from 
(select person_id, max(post_category_weight) from www._latest_role_v1 group by person_id) as lr 
inner join www._latest_role_v1 ls on lr.person_id=ls.person_id and lr.max=ls.post_category_weight 
inner join (select max(coalesce(nullif(end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date)) as end_date, person_id from www._all_roles_v1 join person on person.id=supervisor_id 
and person.crsid = $1 group by person_id, supervisor_id) as roles on lr.person_id=roles.person_id 
inner join person on person.id=lr.person_id
inner join www.post_category_hid as h on h.post_category_id=ls.post_category_id
where roles.end_date>now() and person.crsid is not null and person.do_not_show_on_website IS FALSE
UNION
select person.id, person.crsid, research_group_post_category from person
join www.person_info_v8 on person.id=www.person_info_v8.id
where person.crsid = $1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION www.current_people_supervised_by(text)
  OWNER TO alt36;

-- Function: www.person_status_for_supervisor(text, text)

-- DROP FUNCTION www.person_status_for_supervisor(text, text);

CREATE OR REPLACE FUNCTION www.person_status_for_supervisor(
    _person text,
    _supervisor text)
  RETURNS character varying AS
$BODY$
declare
begin
   IF _person = _supervisor THEN return 'Current'; END IF;


IF EXISTS ( select person_id from www._all_roles_v1  join person p on person_id=p.id join person s on supervisor_id=s.id where p.crsid=_person and s.crsid=_supervisor and status='Current' group by person_id, supervisor_id) THEN
  return 'Current';
ELSIF EXISTS ( select person_id from www._all_roles_v1  join person p on person_id=p.id join person s on supervisor_id=s.id where p.crsid=_person and s.crsid=_supervisor and status='Past' group by person_id, supervisor_id) THEN
  return 'Past';
ELSE
  return 'Unknown';
END IF;
  
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION www.person_status_for_supervisor(text, text)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO public;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO www_sites;

