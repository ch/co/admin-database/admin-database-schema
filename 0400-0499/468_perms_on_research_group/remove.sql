ALTER TABLE research_group OWNER TO cen1001;
GRANT ALL ON research_group TO dev;
GRANT SELECT,UPDATE,INSERT,DELETE ON research_group to old_cos;
ALTER SEQUENCE research_group_id_seq OWNER TO cen1001;
GRANT ALL ON research_group_id_seq TO dev;
GRANT USAGE ON research_group_id_seq TO old_cos;
REVOKE ALL ON research_group_id_seq FROM cos;
