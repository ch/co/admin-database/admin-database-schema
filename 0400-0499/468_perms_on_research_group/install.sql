ALTER TABLE research_group OWNER TO dev;
GRANT ALL ON research_group TO cen1001;
REVOKE ALL ON research_group FROM old_cos;
ALTER SEQUENCE research_group_id_seq OWNER TO dev;
GRANT ALL ON research_group_id_seq TO cen1001;
GRANT USAGE ON research_group_id_seq TO cos;
REVOKE ALL ON research_group_id_seq FROM old_cos;
