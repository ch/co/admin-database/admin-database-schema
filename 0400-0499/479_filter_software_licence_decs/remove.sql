CREATE OR REPLACE VIEW hotwire."10_View/Research_Groups/Software_Licence_Declaration" AS 
SELECT 
  research_group.id, 
  research_group.name, 
  research_group.head_of_group_id, 
  CASE WHEN latest_software_declaration.declaration_date IS NULL THEN false WHEN (
    latest_software_declaration.declaration_date + '1 year' :: interval
  ) > 'now' :: text :: date THEN true ELSE false END AS all_software_used_by_this_group_is_correctly_licenced, 
  latest_software_declaration.declaration_date AS ro_date_of_last_declaration, 
  latest_software_declaration.username_of_signer AS ro_declared_by 
FROM 
  research_group 
  LEFT JOIN (
    SELECT 
      group_software_licence_declarations.research_group_id, 
      max(
        group_software_licence_declarations.declaration_date
      ) AS declaration_date 
    FROM 
      group_software_licence_declarations 
    GROUP BY 
      group_software_licence_declarations.research_group_id
  ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id 
  LEFT JOIN group_software_licence_declarations latest_software_declaration ON latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date 
  AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id 
  JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id 
WHERE 
  "current_user"() = head_of_group.crsid :: name 
  OR pg_has_role(
    "current_user"(), 
    'cos' :: name, 
    'member' :: text
  ) 
ORDER BY 
  research_group.name;



CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration" AS 
SELECT 
  research_group.id, 
  research_group.name, 
  research_group.head_of_group_id, 
  CASE WHEN latest_software_declaration.declaration_date IS NULL THEN false WHEN (
    latest_software_declaration.declaration_date + '1 year' :: interval
  ) > 'now' :: text :: date THEN true ELSE false END AS all_software_used_by_this_group_is_correctly_licenced, 
  latest_software_declaration.declaration_date AS ro_date_of_last_declaration, 
  latest_software_declaration.username_of_signer AS ro_declared_by 
FROM 
  research_group 
  LEFT JOIN (
    SELECT 
      group_software_licence_declarations.research_group_id, 
      max(
        group_software_licence_declarations.declaration_date
      ) AS declaration_date 
    FROM 
      group_software_licence_declarations 
    GROUP BY 
      group_software_licence_declarations.research_group_id
  ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id 
  LEFT JOIN group_software_licence_declarations latest_software_declaration ON latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date 
  AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id 
  JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id 
WHERE 
  "current_user"() = head_of_group.crsid :: name 
  OR pg_has_role(
    "current_user"(), 
    'cos' :: name, 
    'member' :: text
  ) 
ORDER BY 
  research_group.name;

