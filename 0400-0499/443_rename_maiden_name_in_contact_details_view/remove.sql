DROP VIEW hotwire."10_View/People/All_Contact_Details";
CREATE VIEW hotwire."10_View/People/All_Contact_Details" AS 
 WITH contact_details AS (
         SELECT person.id, ROW('image/jpeg'::character varying, person.image_lo::bigint)::blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, person.crsid, person.email_address, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.external_work_phone_numbers, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.emergency_contact, person.location, person.forwarding_address, person.new_employer_address, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE person.id = mm_person_nationality.person_id) AS nationality_id, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
        )
 SELECT contact_details.id, contact_details.id AS ro_person_id, contact_details.image_oid, contact_details.surname, contact_details.first_names, contact_details.title_id, contact_details.known_as, contact_details.name_suffix, contact_details.maiden_name, contact_details.gender_id, contact_details.ro_post_category_id, contact_details.crsid, contact_details.email_address, contact_details.arrival_date, contact_details.leaving_date, contact_details.ro_physical_status_id, contact_details.left_but_no_leaving_date_given, contact_details.dept_telephone_number_id, contact_details.room_id, contact_details.research_group_id, contact_details.external_work_phone_numbers, contact_details.cambridge_address AS home_address, contact_details.cambridge_phone_number AS home_phone_number, contact_details.cambridge_college_id, contact_details.mobile_number, contact_details.emergency_contact, contact_details.location, contact_details.forwarding_address, contact_details.new_employer_address, contact_details.nationality_id, contact_details._cssclass
   FROM contact_details
  ORDER BY contact_details.surname, contact_details.first_names;

ALTER TABLE hotwire."10_View/People/All_Contact_Details"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/All_Contact_Details" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/All_Contact_Details" TO reception;

-- Rule: hw_all_contact_details_upd ON hotwire."10_View/People/All_Contact_Details"

-- DROP RULE hw_all_contact_details_upd ON hotwire."10_View/People/All_Contact_Details";

CREATE OR REPLACE RULE hw_all_contact_details_upd AS
    ON UPDATE TO hotwire."10_View/People/All_Contact_Details" DO INSTEAD ( UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, known_as = new.known_as, name_suffix = new.name_suffix, maiden_name = new.maiden_name, gender_id = new.gender_id, crsid = new.crsid, email_address = new.email_address, arrival_date = new.arrival_date, leaving_date = new.leaving_date, left_but_no_leaving_date_given = new.left_but_no_leaving_date_given, external_work_phone_numbers = new.external_work_phone_numbers, cambridge_address = new.home_address, cambridge_phone_number = new.home_phone_number, cambridge_college_id = new.cambridge_college_id, mobile_number = new.mobile_number, emergency_contact = new.emergency_contact, location = new.location, forwarding_address = new.forwarding_address, new_employer_address = new.new_employer_address
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.research_group_id, old.research_group_id, 'mm_person_research_group'::character varying, 'person_id'::character varying, 'research_group_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.nationality_id, old.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, old.id) AS fn_mm_array_update;
);


