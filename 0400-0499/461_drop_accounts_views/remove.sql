CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Accounts" AS 
 SELECT a.id, a.ro_person, a._image_oid, a.ro_surname, a.ro_first_names, a.dept_telephone_number_id, a.room_id, a.ro_crsid, a.ro_email_address, a.ni_number, a.payroll_personal_reference_number, a.ro_post_category_id, a.ro_emplid_number, a.ro_gaf_number, a.ro_date_registered_for_phd, a.ro_cpgs_or_mphil_date_submission_due, a.ro_mphil_date_submission_due, a.ro_date_phd_submission_due, a.ro_phd_four_year_submission_date, a._hl_status
   FROM ( SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname AS ro_surname, person.first_names AS ro_first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, person.crsid AS ro_crsid, person.email_address AS ro_email_address, person.ni_number, person.payroll_personal_reference_number, _latest_role.post_category_id AS ro_post_category_id, postgraduate_studentship.emplid_number AS ro_emplid_number, postgraduate_studentship.gaf_number AS ro_gaf_number, postgraduate_studentship.date_registered_for_phd AS ro_date_registered_for_phd, postgraduate_studentship.cpgs_or_mphil_date_submission_due AS ro_cpgs_or_mphil_date_submission_due, postgraduate_studentship.mphil_date_submission_due AS ro_mphil_date_submission_due, postgraduate_studentship.date_phd_submission_due AS ro_date_phd_submission_due, postgraduate_studentship.end_of_registration_date AS ro_phd_four_year_submission_date, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _hl_status
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
   LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN postgraduate_studentship ON postgraduate_studentship.person_id = person.id) a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Accounts"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Accounts" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO accounts;

-- Rule: hotwire_view_personnel_accounts_upd ON hotwire."10_View/People/Personnel_Accounts"

-- DROP RULE hotwire_view_personnel_accounts_upd ON hotwire."10_View/People/Personnel_Accounts";

CREATE OR REPLACE RULE hotwire_view_personnel_accounts_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Accounts" DO INSTEAD  UPDATE person SET payroll_personal_reference_number = new.payroll_personal_reference_number, ni_number = new.ni_number
  WHERE person.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Payroll_Numbers_Outstanding" AS 
 SELECT a.id, a.person_id, a.payroll_number, a.ni_number, a.ro_post_category_id, a.ro_status, a._surname, a._first_names
   FROM ( SELECT person.id, person.id AS person_id, person.payroll_personal_reference_number AS payroll_number, person.ni_number, _latest_role_v8.post_category_id AS ro_post_category_id, _latest_role_v8.status AS ro_status, person.surname AS _surname, person.first_names AS _first_names
           FROM (         SELECT post_history.person_id
                           FROM post_history
                          WHERE post_history.paid_by_university = true
                UNION 
                         SELECT postgraduate_studentship.person_id
                           FROM postgraduate_studentship
                          WHERE postgraduate_studentship.paid_through_payroll = true) potential_payees
      JOIN person ON potential_payees.person_id = person.id
   JOIN _latest_role_v8 USING (person_id)
  WHERE person.payroll_personal_reference_number IS NULL OR person.ni_number IS NULL) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."10_View/Payroll_Numbers_Outstanding"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Payroll_Numbers_Outstanding" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Payroll_Numbers_Outstanding" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Payroll_Numbers_Outstanding" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Payroll_Numbers_Outstanding" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Payroll_Numbers_Outstanding" TO accounts;

-- Rule: hotwire_view_payroll_numbers_outstanding_upd ON hotwire."10_View/Payroll_Numbers_Outstanding"

-- DROP RULE hotwire_view_payroll_numbers_outstanding_upd ON hotwire."10_View/Payroll_Numbers_Outstanding";

CREATE OR REPLACE RULE hotwire_view_payroll_numbers_outstanding_upd AS
    ON UPDATE TO hotwire."10_View/Payroll_Numbers_Outstanding" DO INSTEAD  UPDATE person SET payroll_personal_reference_number = new.payroll_number, ni_number = new.ni_number
  WHERE person.id = old.id;

