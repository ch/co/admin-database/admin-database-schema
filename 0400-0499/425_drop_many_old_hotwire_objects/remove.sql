--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.24
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

--
-- Name: cos_research_group_members_subview; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.cos_research_group_members_subview AS
SELECT person.id, mm_person_research_group.research_group_id, person_hid.person_hid AS fullname, _latest_role_v8.post_category_id, person.arrival_date AS start_date, COALESCE(_latest_role_v8.end_date, _latest_role_v8.intended_end_date) AS end_date, room_hid.room_hid AS mm_room, dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone, person.email_address, 'personnel_basic_view'::text AS _targetview FROM ((((((((public.person JOIN public.person_hid ON ((person.id = person_hid.person_id))) JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public._latest_role_v8 ON ((person.id = _latest_role_v8.person_id))) LEFT JOIN public._physical_status USING (id)) WHERE ((_physical_status.physical_status)::text <> 'Past'::text) ORDER BY person_hid.person_hid;


ALTER TABLE public.cos_research_group_members_subview OWNER TO cen1001;

--
-- Name: current_people_by_sector_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.current_people_by_sector_report AS
SELECT person.id, person.surname, person.first_names, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, _latest_role_v8.supervisor_id, CASE WHEN (person.hide_email = true) THEN (NULL::text)::character varying ELSE person.email_address END AS email_address, _latest_role_v8.post_category_id, _latest_role_v8.chem, sector.name AS mm_sector, sector.id AS mm_sector_id FROM ((((((((((public.person LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) LEFT JOIN public._physical_status ON ((_physical_status.id = person.id))) LEFT JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN public.research_group_hid USING (research_group_id)) LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id))) LEFT JOIN public.sector ON ((research_group.sector_id = sector.id))) WHERE ((_physical_status.physical_status)::text <> 'Past'::text) GROUP BY person.surname, person.first_names, person.id, dept_telephone_number_hid.dept_telephone_number_hid, mm_person_dept_telephone_number.dept_telephone_number_id, room_hid.room_hid, mm_person_room.room_id, research_group_hid.research_group_hid, mm_person_research_group.research_group_id, _latest_role_v8.supervisor_id, person.email_address, person.hide_email, _latest_role_v8.post_category_id, sector.name, sector.id, _latest_role_v8.chem ORDER BY person.surname, person.first_names;


ALTER TABLE public.current_people_by_sector_report OWNER TO cen1001;

--
-- Name: female_researchers_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.female_researchers_report AS
SELECT person.id, person.surname, person.first_names, person.email_address, _latest_role_v8.post_category, _latest_role_v8.supervisor_id FROM ((public.person JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) JOIN public._physical_status_v2 ON ((_physical_status_v2.person_id = person.id))) WHERE (((person.gender_id = 1) AND ((_physical_status_v2.status_id)::text = 'Current'::text)) AND (((_latest_role_v8.role_tablename = 'postgraduate_studentship'::text) OR (_latest_role_v8.role_tablename = 'erasmus_socrates_studentship'::text)) OR (((_latest_role_v8.role_tablename = 'post_history'::text) AND ((_latest_role_v8.post_category)::text <> 'Academic-related staff'::text)) AND ((_latest_role_v8.post_category)::text <> 'Assistant staff'::text)))) ORDER BY person.surname, person.first_names;


ALTER TABLE public.female_researchers_report OWNER TO cen1001;

--
-- Name: firstaiders_view; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.firstaiders_view AS
SELECT firstaider.id, person.image_oid AS _image_oid, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, array_to_string(ARRAY(SELECT DISTINCT building_hid.building_hid FROM (((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) LEFT JOIN public.building_hid USING (building_id)) WHERE (p2.id = firstaider.person_id) ORDER BY building_hid.building_hid), '/'::text) AS ro_building, array_to_string(ARRAY(SELECT DISTINCT building_floor_hid.building_floor_hid FROM (((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) LEFT JOIN public.building_floor_hid USING (building_floor_id)) WHERE (p2.id = firstaider.person_id) ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor, array_to_string(ARRAY(SELECT DISTINCT room.name FROM ((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) WHERE (p2.id = firstaider.person_id) ORDER BY room.name), '/'::text) AS ro_room, array_to_string(ARRAY(SELECT dept_telephone_number_hid.dept_telephone_number_hid FROM ((public.person p2 JOIN public.mm_person_dept_telephone_number ON ((mm_person_dept_telephone_number.person_id = p2.id))) JOIN public.dept_telephone_number_hid ON ((dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id))) WHERE (p2.id = firstaider.person_id)), ' / '::text) AS ro_dept_telephone_numbers, _physical_status_v2.status_id AS status, CASE WHEN (firstaider.requalify_date IS NULL) THEN NULL::boolean WHEN (firstaider.requalify_date > ('now'::text)::date) THEN true ELSE false END AS ro_qualification_up_to_date, _latest_role_v8.post_category AS ro_post_category, CASE WHEN ((_physical_status_v2.status_id)::text = 'Past'::text) THEN 'orange'::text WHEN (firstaider.requalify_date < ('now'::text)::date) THEN 'red'::text ELSE NULL::text END AS _hl_status FROM (((public.firstaider JOIN public.person ON ((firstaider.person_id = person.id))) LEFT JOIN public._latest_role_v8 ON ((firstaider.person_id = _latest_role_v8.person_id))) JOIN public._physical_status_v2 ON ((_physical_status_v2.person_id = firstaider.person_id)));


ALTER TABLE public.firstaiders_view OWNER TO cen1001;

--
-- Name: inconsistent_status_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.inconsistent_status_report AS
SELECT _physical_status_v2.person_id AS id, _physical_status_v2.person_id, _latest_role_v8.post_category_id, _physical_status_v2.status_id AS physical_status, person.arrival_date AS physical_arrival_date, person.leaving_date AS physical_leaving_date, _latest_role_v8.status AS newest_role_status, _latest_role_v8.start_date AS newest_role_start_date, _latest_role_v8.intended_end_date AS newest_role_intended_end_date, _latest_role_v8.end_date AS newest_role_end_date FROM ((public._physical_status_v2 LEFT JOIN public._latest_role_v8 USING (person_id)) LEFT JOIN public.person ON ((person.id = _physical_status_v2.person_id))) WHERE ((_physical_status_v2.status_id)::text <> (_latest_role_v8.status)::text);


ALTER TABLE public.inconsistent_status_report OWNER TO cen1001;

--
-- Name: leavers_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.leavers_report AS
SELECT ((_latest_role_v8.person_id || '-'::text) || _latest_role_v8.role_id) AS id, _latest_role_v8.person_id, _latest_role_v8.intended_end_date, _latest_role_v8.end_date, _latest_role_v8.supervisor_id, _latest_role_v8.post_category_id, _latest_role_v8.status AS role_status_id, _physical_status.physical_status AS physical_status_id, person.surname AS _surname, person.first_names AS _first_names FROM ((public._latest_role_v8 LEFT JOIN public._physical_status ON ((_physical_status.id = _latest_role_v8.person_id))) LEFT JOIN public.person ON ((person.id = _latest_role_v8.person_id))) WHERE ((_latest_role_v8.intended_end_date < ('now'::text)::date) AND ((_physical_status.physical_status)::text = 'Current'::text)) ORDER BY person.surname, person.first_names;


ALTER TABLE public.leavers_report OWNER TO cen1001;

--
-- Name: payroll_numbers_outstanding_view; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.payroll_numbers_outstanding_view AS
SELECT person.id, person.id AS person_id, person.payroll_personal_reference_number AS payroll_number, person.ni_number, _latest_role_v8.post_category_id AS ro_post_category_id, _latest_role_v8.status AS ro_status, person.surname AS _surname, person.first_names AS _first_names FROM (((SELECT post_history.person_id FROM public.post_history WHERE (post_history.paid_by_university = true) UNION SELECT postgraduate_studentship.person_id FROM public.postgraduate_studentship WHERE (postgraduate_studentship.paid_through_payroll = true)) potential_payees JOIN public.person ON ((potential_payees.person_id = person.id))) JOIN public._latest_role_v8 USING (person_id)) WHERE ((person.payroll_personal_reference_number IS NULL) OR (person.ni_number IS NULL)) ORDER BY person.surname, person.first_names;


ALTER TABLE public.payroll_numbers_outstanding_view OWNER TO cen1001;

--
-- Name: people_without_staff_reviews_oldreport; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.people_without_staff_reviews_oldreport AS
SELECT people_without_reviews.person_id AS id, people_without_reviews.person_id, _latest_employment.supervisor_id, person_reviewer.reviewer_id, _latest_employment.post_category FROM (((SELECT _latest_employment.person_id FROM (public._latest_employment JOIN public._physical_status_v2 USING (person_id)) WHERE ((((((((_latest_employment.post_category)::text = 'PDRA'::text) OR ((_latest_employment.post_category)::text = 'Senior PDRA'::text)) OR ((_latest_employment.post_category)::text = 'Research Fellow'::text)) OR ((_latest_employment.post_category)::text = 'Teaching Fellow'::text)) OR ((_latest_employment.post_category)::text = 'Research Associate'::text)) AND ((_physical_status_v2.status_id)::text = 'Current'::text)) AND ((_latest_employment.status)::text = 'Current'::text)) EXCEPT SELECT person.id FROM ((public.person JOIN public.post_history ON ((post_history.person_id = person.id))) JOIN public.staff_review_meeting ON ((staff_review_meeting.post_history_id = post_history.id))) WHERE (staff_review_meeting.date_of_meeting > '2010-09-01'::date)) people_without_reviews LEFT JOIN (SELECT person.id AS person_id, post_history.reviewer_id FROM (public.person LEFT JOIN public.post_history ON ((person.id = post_history.person_id))) WHERE (post_history.reviewer_id IS NOT NULL)) person_reviewer USING (person_id)) LEFT JOIN public._latest_employment USING (person_id));


ALTER TABLE public.people_without_staff_reviews_oldreport OWNER TO cen1001;

--
-- Name: personnel_phone_view; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.personnel_phone_view AS
SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname, person.first_names, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, person.external_work_phone_numbers AS external_work_numbers, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, person.email_address, person.location, _latest_role_v8.post_category_id AS ro_post_category_id, _physical_status_v2.status_id AS ro_physical_status_id, CASE WHEN ((_physical_status_v2.status_id)::text = 'Past'::text) THEN 'orange'::text WHEN (((_physical_status_v2.status_id)::text = 'Unknown'::text) AND ((person._status)::text = 'Past'::text)) THEN 'orange'::text ELSE NULL::text END AS _hl_status FROM (((((((public.person LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) LEFT JOIN public._physical_status_v2 ON ((_physical_status_v2.id = person.id))) LEFT JOIN public.person_hid ON ((person.id = person_hid.person_id))) GROUP BY person.surname, person.first_names, person.id, person_hid.person_hid, person.image_oid, dept_telephone_number_hid.dept_telephone_number_hid, mm_person_dept_telephone_number.dept_telephone_number_id, room_hid.room_hid, mm_person_room.room_id, _latest_role_v8.supervisor_id, person.email_address, person.location, person.hide_email, _latest_role_v8.post_category_id, _physical_status_v2.status_id, person._status, person.external_work_phone_numbers ORDER BY person.surname, person.first_names;


ALTER TABLE public.personnel_phone_view OWNER TO cen1001;

--
-- Name: phone_directory_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.phone_directory_report AS
SELECT person.id, person.surname, person.first_names, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, _latest_role_v8.supervisor_id, CASE WHEN (person.hide_email = true) THEN (NULL::text)::character varying ELSE person.email_address END AS email_address, _latest_role_v8.post_category_id, sector.name AS mm_sector, sector.id AS mm_sector_id FROM ((((((((((public.person LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) LEFT JOIN public._physical_status ON ((_physical_status.id = person.id))) LEFT JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN public.research_group_hid USING (research_group_id)) LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id))) LEFT JOIN public.sector ON ((research_group.sector_id = sector.id))) WHERE ((_physical_status.physical_status)::text <> 'Past'::text) GROUP BY person.surname, person.first_names, person.id, dept_telephone_number_hid.dept_telephone_number_hid, mm_person_dept_telephone_number.dept_telephone_number_id, room_hid.room_hid, mm_person_room.room_id, research_group_hid.research_group_hid, mm_person_research_group.research_group_id, _latest_role_v8.supervisor_id, person.email_address, person.hide_email, _latest_role_v8.post_category_id, sector.name, sector.id ORDER BY person.surname, person.first_names;


ALTER TABLE public.phone_directory_report OWNER TO cen1001;

--
-- Name: postdoc_mentors_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.postdoc_mentors_report AS
SELECT post_history.id, post_history.person_id, COALESCE(post_history.job_title, _latest_role_v8.post_category) AS job_title, person.email_address, post_history.supervisor_id, supervisor.email_address AS supervisor_email_address, post_history.mentor_id, mentor.email_address AS mentor_email_address, person.surname AS _surname, person.first_names AS _first_names FROM (((((public._latest_role_v8 LEFT JOIN public.post_history ON (((_latest_role_v8.role_id = post_history.id) AND (_latest_role_v8.role_tablename = 'post_history'::text)))) LEFT JOIN public.person ON ((post_history.person_id = person.id))) LEFT JOIN public.person mentor ON ((post_history.mentor_id = mentor.id))) LEFT JOIN public.person supervisor ON ((post_history.supervisor_id = supervisor.id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) WHERE (((_latest_role_v8.post_category)::text = ANY (ARRAY[('PDRA'::character varying)::text, ('Senior PDRA'::character varying)::text, ('Research
Fellow'::character varying)::text])) AND ((_physical_status_v2.status_id)::text = 'Current'::text)) ORDER BY person.surname, person.first_names;


ALTER TABLE public.postdoc_mentors_report OWNER TO cen1001;

--
-- Name: postgrad_mentors_report; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.postgrad_mentors_report AS
SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, postgraduate_studentship.postgraduate_studentship_type_id, person.email_address, postgraduate_studentship.first_supervisor_id AS supervisor_id, supervisor.email_address AS supervisor_email_address, postgraduate_studentship.first_mentor_id AS mentor_id, mentor.email_address AS mentor_email_address, postgraduate_studentship.second_mentor_id AS co_mentor_id, second_mentor.email_address AS co_mentor_email_address, postgraduate_studentship.external_mentor FROM ((((((public.postgraduate_studentship LEFT JOIN public.person ON ((postgraduate_studentship.person_id = person.id))) LEFT JOIN public.person mentor ON ((postgraduate_studentship.first_mentor_id = mentor.id))) LEFT JOIN public.person second_mentor ON ((postgraduate_studentship.second_mentor_id = second_mentor.id))) LEFT JOIN public.person supervisor ON ((postgraduate_studentship.first_supervisor_id = supervisor.id))) JOIN public._latest_role_v8 ON (((_latest_role_v8.role_id = postgraduate_studentship.id) AND (_latest_role_v8.role_tablename = 'postgraduate_studentship'::text)))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) WHERE ((_physical_status_v2.status_id)::text = 'Current'::text);


ALTER TABLE public.postgrad_mentors_report OWNER TO cen1001;

--
-- Name: research_group_members_subview; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.research_group_members_subview AS
SELECT person.id, mm_person_research_group.research_group_id, person_hid.person_hid AS person, _latest_role_v8.post_category_id, person.arrival_date AS start_date, COALESCE(_latest_role_v8.end_date, _latest_role_v8.intended_end_date) AS end_date, room_hid.room_hid AS mm_room, dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone, person.email_address, 'personnel_data_entry_view'::text AS _targetview, person.surname AS _surname, person.first_names AS _first_names FROM ((((((((public.person JOIN public.person_hid ON ((person.id = person_hid.person_id))) JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public._latest_role_v8 ON ((person.id = _latest_role_v8.person_id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text) ORDER BY person.surname, person.first_names;


ALTER TABLE public.research_group_members_subview OWNER TO cen1001;

--
-- Name: room_occupants_subview; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.room_occupants_subview AS
SELECT person.id, mm_person_room.room_id, person_hid.person_hid, _latest_role_v8.supervisor_id, _latest_role_v8.post_category_id, person.arrival_date AS start_date, COALESCE(_latest_role_v8.end_date, _latest_role_v8.intended_end_date) AS end_date, 'personnel_phone_view'::text AS _targetview, supervisor_hid.person_hid AS _supervisor FROM (((((public.person LEFT JOIN public.person_hid ON ((person.id = person_hid.person_id))) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public._latest_role_v8 ON ((person.id = _latest_role_v8.person_id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) LEFT JOIN public.person_hid supervisor_hid ON ((_latest_role_v8.supervisor_id = person_hid.person_id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text) ORDER BY supervisor_hid.person_hid;


ALTER TABLE public.room_occupants_subview OWNER TO cen1001;

--
-- Name: staff_review_and_development2_oldreport; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.staff_review_and_development2_oldreport AS
SELECT person.id, person.id AS person_id, _latest_employment.post_category_id, _latest_employment.supervisor_id, latest_ph.reviewer_id AS usual_reviewer_id, latest_ph.next_review_due, person.continuous_employment_start_date, latest_ph.funding_end_date, latest_meeting_details.reviewer_id, latest_meeting_details.date_of_meeting, latest_meeting_details.notes, other_dates.other_dates FROM (((((public.person JOIN public._latest_employment ON ((person.id = _latest_employment.person_id))) JOIN public.post_history latest_ph ON ((_latest_employment.role_id = latest_ph.id))) JOIN public._physical_status_v2 ON ((_physical_status_v2.person_id = person.id))) LEFT JOIN (SELECT ph.person_id, staff_review_meeting.reviewer_id, staff_review_meeting.notes, staff_review_meeting.date_of_meeting FROM ((public.staff_review_meeting JOIN public.post_history ph ON ((staff_review_meeting.post_history_id = ph.id))) JOIN (SELECT ph.person_id, max(srm.date_of_meeting) AS latest_date FROM (public.staff_review_meeting srm JOIN public.post_history ph ON ((srm.post_history_id = ph.id))) GROUP BY ph.person_id) latest_meeting_plus_person ON (((latest_meeting_plus_person.person_id = ph.person_id) AND (latest_meeting_plus_person.latest_date = staff_review_meeting.date_of_meeting))))) latest_meeting_details ON ((latest_meeting_details.person_id = person.id))) LEFT JOIN (SELECT o1.person_id, ARRAY(SELECT o2.date_of_meeting FROM public._old_review_meetings o2 WHERE (o2.person_id = o1.person_id)) AS other_dates FROM public._old_review_meetings o1) other_dates ON ((other_dates.person_id = person.id))) WHERE ((_physical_status_v2.status_id)::text = 'Current'::text);


ALTER TABLE public.staff_review_and_development2_oldreport OWNER TO cen1001;

--
-- Name: staff_review_and_development_oldreport; Type: VIEW; Schema: public; Owner: cen1001
--

CREATE VIEW public.staff_review_and_development_oldreport AS
SELECT person.id, person.id AS person_id, _latest_employment.supervisor_id, latest_ph.reviewer_id AS usual_reviewer_id, latest_ph.next_review_due, person.continuous_employment_start_date, latest_ph.funding_end_date, latest_meeting_details.reviewer_id, latest_meeting_details.date_of_meeting, latest_meeting_details.notes, other_dates.other_dates FROM (((((public.person JOIN public._latest_employment ON ((person.id = _latest_employment.person_id))) JOIN public.post_history latest_ph ON ((_latest_employment.role_id = latest_ph.id))) JOIN public._physical_status_v2 ON ((_physical_status_v2.person_id = person.id))) LEFT JOIN (SELECT ph.person_id, staff_review_meeting.reviewer_id, staff_review_meeting.notes, staff_review_meeting.date_of_meeting FROM ((public.staff_review_meeting JOIN public.post_history ph ON ((staff_review_meeting.post_history_id = ph.id))) JOIN (SELECT ph.person_id, max(srm.date_of_meeting) AS latest_date FROM (public.staff_review_meeting srm JOIN public.post_history ph ON ((srm.post_history_id = ph.id))) GROUP BY ph.person_id) latest_meeting_plus_person ON (((latest_meeting_plus_person.person_id = ph.person_id) AND (latest_meeting_plus_person.latest_date = staff_review_meeting.date_of_meeting))))) latest_meeting_details ON ((latest_meeting_details.person_id = person.id))) LEFT JOIN (SELECT o1.person_id, ARRAY(SELECT o2.date_of_meeting FROM public._old_review_meetings o2 WHERE (o2.person_id = o1.person_id)) AS other_dates FROM public._old_review_meetings o1) other_dates ON ((other_dates.person_id = person.id))) WHERE ((_physical_status_v2.status_id)::text = 'Current'::text);


ALTER TABLE public.staff_review_and_development_oldreport OWNER TO cen1001;

--
-- Name: firstaiders_view firstaiders_del; Type: RULE; Schema: public; Owner: cen1001
--

CREATE RULE firstaiders_del AS ON DELETE TO public.firstaiders_view DO INSTEAD DELETE FROM public.firstaider WHERE (firstaider.id = old.id);


--
-- Name: firstaiders_view firstaiders_ins; Type: RULE; Schema: public; Owner: cen1001
--

CREATE RULE firstaiders_ins AS ON INSERT TO public.firstaiders_view DO INSTEAD INSERT INTO public.firstaider (person_id, firstaider_funding_id, qualification_date, requalify_date, hf_cn_trained) VALUES (new.person_id, new.firstaider_funding_id, new.qualification_date, new.requalify_date, new.hf_cn_trained) RETURNING firstaider.id, NULL::bigint AS int8, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::character varying(20) AS "varchar", NULL::boolean AS bool, NULL::character varying(80) AS "varchar", NULL::text AS text;


--
-- Name: firstaiders_view firstaiders_upd; Type: RULE; Schema: public; Owner: cen1001
--

CREATE RULE firstaiders_upd AS ON UPDATE TO public.firstaiders_view DO INSTEAD UPDATE public.firstaider SET person_id = new.person_id, firstaider_funding_id = new.firstaider_funding_id, qualification_date = new.qualification_date, requalify_date = new.requalify_date, hf_cn_trained = new.hf_cn_trained WHERE (firstaider.id = old.id);


--
-- Name: payroll_numbers_outstanding_view payroll_numbers_outstanding_upd; Type: RULE; Schema: public; Owner: cen1001
--

CREATE RULE payroll_numbers_outstanding_upd AS ON UPDATE TO public.payroll_numbers_outstanding_view DO INSTEAD UPDATE public.person SET payroll_personal_reference_number = new.payroll_number, ni_number = new.ni_number WHERE (person.id = old.id);


--
-- Name: personnel_phone_view personnel_phone_upd; Type: RULE; Schema: public; Owner: cen1001
--

CREATE RULE personnel_phone_upd AS ON UPDATE TO public.personnel_phone_view DO INSTEAD (UPDATE public.person SET surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address WHERE (person.id = old.id); SELECT public.fn_upd_many_many(old.id, new.mm_dept_telephone_number, 'person'::text, 'dept_telephone_number'::text) AS fn_upd_many_many; SELECT public.fn_upd_many_many(old.id, (new.mm_room)::text, 'person'::text, 'room'::text) AS fn_upd_many_many; );


--
-- Name: TABLE cos_research_group_members_subview; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.cos_research_group_members_subview FROM PUBLIC;
REVOKE ALL ON TABLE public.cos_research_group_members_subview FROM cen1001;
GRANT ALL ON TABLE public.cos_research_group_members_subview TO cen1001;
GRANT ALL ON TABLE public.cos_research_group_members_subview TO dev;
GRANT SELECT ON TABLE public.cos_research_group_members_subview TO cos;
GRANT SELECT ON TABLE public.cos_research_group_members_subview TO phones;
GRANT SELECT ON TABLE public.cos_research_group_members_subview TO old_cos;


--
-- Name: TABLE current_people_by_sector_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.current_people_by_sector_report FROM PUBLIC;
REVOKE ALL ON TABLE public.current_people_by_sector_report FROM cen1001;
GRANT ALL ON TABLE public.current_people_by_sector_report TO cen1001;
GRANT SELECT ON TABLE public.current_people_by_sector_report TO dev;
GRANT SELECT ON TABLE public.current_people_by_sector_report TO mgmt_ro;
GRANT SELECT ON TABLE public.current_people_by_sector_report TO hr;
GRANT SELECT ON TABLE public.current_people_by_sector_report TO reception;
GRANT SELECT ON TABLE public.current_people_by_sector_report TO phones;


--
-- Name: TABLE female_researchers_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.female_researchers_report FROM PUBLIC;
REVOKE ALL ON TABLE public.female_researchers_report FROM cen1001;
GRANT ALL ON TABLE public.female_researchers_report TO cen1001;
GRANT SELECT ON TABLE public.female_researchers_report TO mgmt_ro;
GRANT SELECT ON TABLE public.female_researchers_report TO hr;
GRANT SELECT ON TABLE public.female_researchers_report TO dev;


--
-- Name: TABLE firstaiders_view; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.firstaiders_view FROM PUBLIC;
REVOKE ALL ON TABLE public.firstaiders_view FROM cen1001;
GRANT ALL ON TABLE public.firstaiders_view TO cen1001;
GRANT ALL ON TABLE public.firstaiders_view TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.firstaiders_view TO safety_management;
GRANT SELECT ON TABLE public.firstaiders_view TO www_sites;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.firstaiders_view TO firstaiders;


--
-- Name: TABLE inconsistent_status_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.inconsistent_status_report FROM PUBLIC;
REVOKE ALL ON TABLE public.inconsistent_status_report FROM cen1001;
GRANT ALL ON TABLE public.inconsistent_status_report TO cen1001;
GRANT SELECT ON TABLE public.inconsistent_status_report TO dev;
GRANT SELECT ON TABLE public.inconsistent_status_report TO hr;
GRANT SELECT ON TABLE public.inconsistent_status_report TO student_management;
GRANT SELECT ON TABLE public.inconsistent_status_report TO mgmt_ro;


--
-- Name: TABLE leavers_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.leavers_report FROM PUBLIC;
REVOKE ALL ON TABLE public.leavers_report FROM cen1001;
GRANT ALL ON TABLE public.leavers_report TO cen1001;
GRANT SELECT ON TABLE public.leavers_report TO dev;
GRANT SELECT ON TABLE public.leavers_report TO mgmt_ro;
GRANT SELECT ON TABLE public.leavers_report TO hr;
GRANT SELECT ON TABLE public.leavers_report TO reception;


--
-- Name: TABLE payroll_numbers_outstanding_view; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.payroll_numbers_outstanding_view FROM PUBLIC;
REVOKE ALL ON TABLE public.payroll_numbers_outstanding_view FROM cen1001;
GRANT ALL ON TABLE public.payroll_numbers_outstanding_view TO cen1001;
GRANT SELECT ON TABLE public.payroll_numbers_outstanding_view TO dev;
GRANT SELECT ON TABLE public.payroll_numbers_outstanding_view TO mgmt_ro;
GRANT SELECT ON TABLE public.payroll_numbers_outstanding_view TO hr;
GRANT SELECT,UPDATE ON TABLE public.payroll_numbers_outstanding_view TO accounts;


--
-- Name: TABLE people_without_staff_reviews_oldreport; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.people_without_staff_reviews_oldreport FROM PUBLIC;
REVOKE ALL ON TABLE public.people_without_staff_reviews_oldreport FROM cen1001;
GRANT ALL ON TABLE public.people_without_staff_reviews_oldreport TO cen1001;
GRANT SELECT ON TABLE public.people_without_staff_reviews_oldreport TO dev;
GRANT SELECT ON TABLE public.people_without_staff_reviews_oldreport TO hr;


--
-- Name: TABLE personnel_phone_view; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.personnel_phone_view FROM PUBLIC;
REVOKE ALL ON TABLE public.personnel_phone_view FROM cen1001;
GRANT ALL ON TABLE public.personnel_phone_view TO cen1001;
GRANT SELECT,UPDATE ON TABLE public.personnel_phone_view TO dev;
GRANT SELECT ON TABLE public.personnel_phone_view TO mgmt_ro;
GRANT SELECT ON TABLE public.personnel_phone_view TO accounts;
GRANT SELECT,UPDATE ON TABLE public.personnel_phone_view TO reception;
GRANT SELECT,UPDATE ON TABLE public.personnel_phone_view TO hr;
GRANT SELECT,UPDATE ON TABLE public.personnel_phone_view TO phones;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.personnel_phone_view TO old_cos;
GRANT SELECT ON TABLE public.personnel_phone_view TO student_management_ro;


--
-- Name: TABLE phone_directory_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.phone_directory_report FROM PUBLIC;
REVOKE ALL ON TABLE public.phone_directory_report FROM cen1001;
GRANT ALL ON TABLE public.phone_directory_report TO cen1001;
GRANT ALL ON TABLE public.phone_directory_report TO dev;
GRANT SELECT ON TABLE public.phone_directory_report TO reception;
GRANT SELECT ON TABLE public.phone_directory_report TO hr;
GRANT SELECT ON TABLE public.phone_directory_report TO phones;
GRANT SELECT ON TABLE public.phone_directory_report TO mgmt_ro;


--
-- Name: TABLE postdoc_mentors_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.postdoc_mentors_report FROM PUBLIC;
REVOKE ALL ON TABLE public.postdoc_mentors_report FROM cen1001;
GRANT ALL ON TABLE public.postdoc_mentors_report TO cen1001;
GRANT SELECT ON TABLE public.postdoc_mentors_report TO hr;
GRANT SELECT ON TABLE public.postdoc_mentors_report TO mgmt_ro;
GRANT SELECT ON TABLE public.postdoc_mentors_report TO dev;


--
-- Name: TABLE postgrad_mentors_report; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.postgrad_mentors_report FROM PUBLIC;
REVOKE ALL ON TABLE public.postgrad_mentors_report FROM cen1001;
GRANT ALL ON TABLE public.postgrad_mentors_report TO cen1001;
GRANT SELECT ON TABLE public.postgrad_mentors_report TO hr;
GRANT SELECT ON TABLE public.postgrad_mentors_report TO mgmt_ro;
GRANT SELECT ON TABLE public.postgrad_mentors_report TO dev;


--
-- Name: TABLE research_group_members_subview; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.research_group_members_subview FROM PUBLIC;
REVOKE ALL ON TABLE public.research_group_members_subview FROM cen1001;
GRANT ALL ON TABLE public.research_group_members_subview TO cen1001;
GRANT ALL ON TABLE public.research_group_members_subview TO dev;
GRANT SELECT ON TABLE public.research_group_members_subview TO mgmt_ro;
GRANT SELECT ON TABLE public.research_group_members_subview TO hr;
GRANT SELECT ON TABLE public.research_group_members_subview TO reception;
GRANT SELECT ON TABLE public.research_group_members_subview TO student_management;
GRANT SELECT ON TABLE public.research_group_members_subview TO www_sites;


--
-- Name: TABLE room_occupants_subview; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.room_occupants_subview FROM PUBLIC;
REVOKE ALL ON TABLE public.room_occupants_subview FROM cen1001;
GRANT ALL ON TABLE public.room_occupants_subview TO cen1001;
GRANT SELECT ON TABLE public.room_occupants_subview TO dev;
GRANT SELECT ON TABLE public.room_occupants_subview TO space_management;
GRANT SELECT ON TABLE public.room_occupants_subview TO mgmt_ro;


--
-- Name: TABLE staff_review_and_development2_oldreport; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.staff_review_and_development2_oldreport FROM PUBLIC;
REVOKE ALL ON TABLE public.staff_review_and_development2_oldreport FROM cen1001;
GRANT ALL ON TABLE public.staff_review_and_development2_oldreport TO cen1001;
GRANT SELECT ON TABLE public.staff_review_and_development2_oldreport TO dev;
GRANT SELECT ON TABLE public.staff_review_and_development2_oldreport TO hr;


--
-- Name: TABLE staff_review_and_development_oldreport; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.staff_review_and_development_oldreport FROM PUBLIC;
REVOKE ALL ON TABLE public.staff_review_and_development_oldreport FROM cen1001;
GRANT ALL ON TABLE public.staff_review_and_development_oldreport TO cen1001;
GRANT SELECT ON TABLE public.staff_review_and_development_oldreport TO hr;
GRANT SELECT ON TABLE public.staff_review_and_development_oldreport TO dev;


--
-- PostgreSQL database dump complete
--

