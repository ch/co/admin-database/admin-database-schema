-- people without roles drop and create
DROP VIEW hotwire."30_Report/People_Without_Roles";
CREATE VIEW hotwire."30_Report/People_Without_Roles" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.email_address, 
  a.crsid, 
  a.arrival_date, 
  a.leaving_date, 
  a.location, 
  a.date_of_birth, 
  a.notes, 
  a.extra_filemaker_data, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      role_count.id, 
      role_count.id AS person_id, 
      person.email_address, 
      person.crsid, 
      person.arrival_date, 
      person.leaving_date, 
      person.location, 
      person.date_of_birth, 
      person.notes_and_other_information::varchar(5000) as notes, 
      person.extra_filemaker_data, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      (
        SELECT 
          person.id, 
          count(_all_roles.person_id) AS number_of_roles 
        FROM 
          person 
          LEFT JOIN _all_roles_v12 _all_roles ON person.id = _all_roles.person_id 
        WHERE 
          person.is_spri IS NOT TRUE 
        GROUP BY 
          person.id
      ) role_count 
      JOIN person USING (id) 
    WHERE 
      role_count.number_of_roles = 0
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;


ALTER TABLE hotwire."30_Report/People_Without_Roles" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/People_Without_Roles" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/People_Without_Roles" TO mgmt_ro;
GRANT ALL ON TABLE hotwire."30_Report/People_Without_Roles" TO cen1001;

-- short searching drop
DROP VIEW hotwire."30_Report/Short_Searching";
-- source of funds drop
DROP VIEW hotwire."30_Report/Source_of_Funds";

-- searching report drop and create
DROP VIEW hotwire."30_Report/Searching";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Searching" AS 
SELECT 
  (
    person.id || COALESCE(
      roles.post_category, '' :: character varying
    ):: text
  ) || COALESCE(
    roles.start_date :: text, '' :: text
  ) AS id, 
  person.surname, 
  person.first_names, 
  person.title_id, 
  person.known_as, 
  person.gender_id, 
  date_part(
    'year' :: text, 
    age( person.date_of_birth )
  ) AS ro_age, 
  person.arrival_date, 
  person.leaving_date, 
  _physical_status_v2.status_id AS physical_status_id, 
  roles.start_date, 
  roles.intended_end_date, 
  roles.end_date, 
  roles.funding_end_date, 
  roles.post_category, 
  roles.status AS role_status_id, 
  roles.chem, 
  person.date_of_birth, 
  person.registration_completed, 
  person.clearance_cert_signed, 
  person.email_address, 
  person.crsid, 
  person.hide_email, 
  person.permission_to_give_out_details, 
  ARRAY(
    SELECT 
      mm_person_room.room_id 
    FROM 
      mm_person_room 
    WHERE 
      mm_person_room.person_id = person.id
  ) AS room_id, 
  person.location, 
  ARRAY(
    SELECT 
      mm_person_dept_telephone_number.dept_telephone_number_id 
    FROM 
      mm_person_dept_telephone_number 
    WHERE 
      mm_person_dept_telephone_number.person_id = person.id
  ) AS dept_telephone_number_id, 
  person.mobile_number, 
  person.cambridge_phone_number, 
  ARRAY(
    SELECT 
      mm_person_research_group.research_group_id 
    FROM 
      mm_person_research_group 
    WHERE 
      mm_person_research_group.person_id = person.id
  ) AS research_group_id, 
  ARRAY(
    SELECT 
      research_group.sector_id 
    FROM 
      mm_person_research_group 
      JOIN research_group ON research_group.id = mm_person_research_group.research_group_id 
    WHERE 
      mm_person_research_group.person_id = person.id
  ) AS sector_id, 
  roles.supervisor_id, 
  roles.manager_id, 
  roles.paid_by_university, 
  person.continuous_employment_start_date, 
  person.payroll_personal_reference_number, 
  person.paper_file_details, 
  person.ni_number, 
  roles.job_title, 
  roles.current_funding, 
  roles.research_grant_number, 
  roles.university, 
  ARRAY(
    SELECT 
      mm_person_nationality.nationality_id 
    FROM 
      mm_person_nationality 
    WHERE 
      mm_person_nationality.person_id = person.id
  ) AS nationality_id, 
  person.cambridge_college_id, 
  roles.cpgs_or_mphil_date_submission_due, 
  roles.cpgs_or_mphil_date_submitted, 
  roles.cpgs_or_mphil_date_awarded, 
  roles.mphil_date_submission_due, 
  roles.mphil_date_submitted, 
  roles.mphil_date_awarded, 
  roles.date_registered_for_phd, 
  roles.phd_date_title_approved, 
  roles.date_phd_submission_due, 
  roles.phd_four_year_submission_date, 
  roles.phd_date_submitted, 
  roles.phd_date_examiners_appointed, 
  roles.phd_date_of_viva, 
  roles.phd_date_awarded, 
  roles.co_supervisor_id, 
  roles.mentor_id, 
  roles.co_mentor_id, 
  roles.gaf_number, 
  roles.emplid_number, 
  roles.cpgs_title_html, 
  roles.mphil_title_html, 
  roles.msc_title_html, 
  roles.phd_thesis_title_html, 
  roles.next_mentor_meeting_due, 
  roles.fees_funding, 
  roles.part_iii_project_title_html, 
  roles.date_of_first_probation_meeting, 
  roles.date_of_second_probation_meeting, 
  roles.date_of_third_probation_meeting, 
  roles.date_probation_letters_sent, 
  roles.probation_period, 
  person.usual_reviewer_id AS reviewer_id, 
  roles.next_review_due, 
  roles.timing_of_reviews, 
  roles.transferable_skills_days_1st_year, 
  roles.transferable_skills_days_2nd_year, 
  roles.transferable_skills_days_3rd_year, 
  roles.transferable_skills_days_4th_year, 
  person.forwarding_address, 
  person.emergency_contact, 
  person.new_employer_address, 
  person.cambridge_address, 
  person.maiden_name AS previous_surname, 
  person.notes_and_other_information::varchar(5000) as notes, 
  person.extra_filemaker_data, 
  roles.progress_notes 
FROM 
  person 
  LEFT JOIN (
    (
      (
        (
          SELECT 
            v.person_id, 
            v.start_date, 
            v.intended_end_date, 
            v.end_date, 
            NULL :: date AS funding_end_date, 
            false AS chem, 
            _all_roles.post_category, 
            NULL :: text AS job_title, 
            NULL :: bigint AS manager_id, 
            NULL :: text AS progress_notes, 
            v.host_person_id AS supervisor_id, 
            NULL :: text AS university, 
            NULL :: text AS gaf_number, 
            NULL :: bigint AS postgraduate_studentship_type_id, 
            NULL :: text AS cpgs_title_html, 
            NULL :: text AS mphil_title_html, 
            NULL :: text AS msc_title_html, 
            NULL :: date AS cpgs_or_mphil_date_submission_due, 
            NULL :: date AS cpgs_or_mphil_date_submitted, 
            NULL :: date AS cpgs_or_mphil_date_awarded, 
            NULL :: date AS mphil_date_submission_due, 
            NULL :: date AS mphil_date_submitted, 
            NULL :: date AS mphil_date_awarded, 
            NULL :: date AS date_registered_for_phd, 
            NULL :: text AS phd_thesis_title_html, 
            NULL :: date AS phd_date_title_approved, 
            NULL :: date AS date_phd_submission_due, 
            NULL :: date AS phd_four_year_submission_date, 
            NULL :: date AS phd_date_submitted, 
            NULL :: date AS phd_date_examiners_appointed, 
            NULL :: date AS phd_date_of_viva, 
            NULL :: date AS phd_date_awarded, 
            NULL :: bigint AS co_supervisor_id, 
            NULL :: bigint AS mentor_id, 
            NULL :: bigint AS co_mentor_id, 
            NULL :: date AS next_mentor_meeting_due, 
            NULL :: text AS current_funding, 
            NULL :: text AS fees_funding, 
            NULL :: text AS research_grant_number, 
            NULL :: text AS part_iii_project_title_html, 
            NULL :: boolean AS paid_by_university, 
            NULL :: date AS date_of_first_probation_meeting, 
            NULL :: date AS date_of_second_probation_meeting, 
            NULL :: date AS date_of_third_probation_meeting, 
            NULL :: date AS date_probation_letters_sent, 
            NULL :: text AS probation_period, 
            NULL :: text AS emplid_number, 
            NULL :: date AS next_review_due, 
            NULL :: text AS timing_of_reviews, 
            NULL :: real AS transferable_skills_days_1st_year, 
            NULL :: real AS transferable_skills_days_2nd_year, 
            NULL :: real AS transferable_skills_days_3rd_year, 
            NULL :: real AS transferable_skills_days_4th_year, 
            _all_roles.status 
          FROM 
            visitorship v 
            LEFT JOIN _all_roles_v12 _all_roles ON v.id = _all_roles.role_id 
          WHERE 
            _all_roles.role_tablename = 'visitorship' :: text 
          UNION 
          SELECT 
            e.person_id, 
            e.start_date, 
            e.intended_end_date, 
            e.end_date, 
            NULL :: date AS funding_end_date, 
            false AS chem, 
            _all_roles.post_category, 
            NULL :: text AS job_title, 
            NULL :: bigint AS manager_id, 
            NULL :: text AS progress_notes, 
            e.supervisor_id, 
            e.university, 
            NULL :: text AS gaf_number, 
            NULL :: bigint AS postgraduate_studentship_type_id, 
            NULL :: text AS cpgs_title_html, 
            NULL :: text AS mphil_title_html, 
            NULL :: text AS msc_title_html, 
            NULL :: date AS cpgs_or_mphil_date_submission_due, 
            NULL :: date AS cpgs_or_mphil_date_submitted, 
            NULL :: date AS cpgs_or_mphil_date_awarded, 
            NULL :: date AS mphil_date_submission_due, 
            NULL :: date AS mphil_date_submitted, 
            NULL :: date AS mphil_date_awarded, 
            NULL :: date AS date_registered_for_phd, 
            NULL :: text AS phd_thesis_title_html, 
            NULL :: date AS phd_date_title_approved, 
            NULL :: date AS date_phd_submission_due, 
            NULL :: date AS phd_four_year_submission_date, 
            NULL :: date AS phd_date_submitted, 
            NULL :: date AS phd_date_examiners_appointed, 
            NULL :: date AS phd_date_of_viva, 
            NULL :: date AS phd_date_awarded, 
            NULL :: bigint AS co_supervisor_id, 
            NULL :: bigint AS mentor_id, 
            NULL :: bigint AS co_mentor_id, 
            NULL :: date AS next_mentor_meeting_due, 
            NULL :: text AS current_funding, 
            NULL :: text AS fees_funding, 
            NULL :: text AS research_grant_number, 
            NULL :: text AS part_iii_project_title_html, 
            NULL :: boolean AS paid_by_university, 
            NULL :: date AS date_of_first_probation_meeting, 
            NULL :: date AS date_of_second_probation_meeting, 
            NULL :: date AS date_of_third_probation_meeting, 
            NULL :: date AS date_probation_letters_sent, 
            NULL :: text AS probation_period, 
            NULL :: text AS emplid_number, 
            NULL :: date AS next_review_due, 
            NULL :: text AS timing_of_reviews, 
            NULL :: real AS transferable_skills_days_1st_year, 
            NULL :: real AS transferable_skills_days_2nd_year, 
            NULL :: real AS transferable_skills_days_3rd_year, 
            NULL :: real AS transferable_skills_days_4th_year, 
            _all_roles.status 
          FROM 
            erasmus_socrates_studentship e 
            LEFT JOIN _all_roles_v12 _all_roles ON e.id = _all_roles.role_id 
          WHERE 
            _all_roles.role_tablename = 'erasmus_socrates_studentship' :: text
        ) 
        UNION 
        SELECT 
          p.person_id, 
          p.start_date, 
          p.intended_end_date, 
          p.end_date, 
          NULL :: date AS funding_end_date, 
          false AS chem, 
          _all_roles.post_category, 
          NULL :: text AS job_title, 
          NULL :: bigint AS manager_id, 
          NULL :: text AS progress_notes, 
          p.supervisor_id, 
          NULL :: text AS university, 
          NULL :: text AS gaf_number, 
          NULL :: bigint AS postgraduate_studentship_type_id, 
          NULL :: text AS cpgs_title_html, 
          NULL :: text AS mphil_title_html, 
          NULL :: text AS msc_title_html, 
          NULL :: date AS cpgs_or_mphil_date_submission_due, 
          NULL :: date AS cpgs_or_mphil_date_submitted, 
          NULL :: date AS cpgs_or_mphil_date_awarded, 
          NULL :: date AS mphil_date_submission_due, 
          NULL :: date AS mphil_date_submitted, 
          NULL :: date AS mphil_date_awarded, 
          NULL :: date AS date_registered_for_phd, 
          NULL :: text AS phd_thesis_title_html, 
          NULL :: date AS phd_date_title_approved, 
          NULL :: date AS date_phd_submission_due, 
          NULL :: date AS phd_four_year_submission_date, 
          NULL :: date AS phd_date_submitted, 
          NULL :: date AS phd_date_examiners_appointed, 
          NULL :: date AS phd_date_of_viva, 
          NULL :: date AS phd_date_awarded, 
          NULL :: bigint AS co_supervisor_id, 
          NULL :: bigint AS mentor_id, 
          NULL :: bigint AS co_mentor_id, 
          NULL :: date AS next_mentor_meeting_due, 
          NULL :: text AS current_funding, 
          NULL :: text AS fees_funding, 
          NULL :: text AS research_grant_number, 
          p.project_title AS part_iii_project_title_html, 
          NULL :: boolean AS paid_by_university, 
          NULL :: date AS date_of_first_probation_meeting, 
          NULL :: date AS date_of_second_probation_meeting, 
          NULL :: date AS date_of_third_probation_meeting, 
          NULL :: date AS date_probation_letters_sent, 
          NULL :: text AS probation_period, 
          NULL :: text AS emplid_number, 
          NULL :: date AS next_review_due, 
          NULL :: text AS timing_of_reviews, 
          NULL :: real AS transferable_skills_days_1st_year, 
          NULL :: real AS transferable_skills_days_2nd_year, 
          NULL :: real AS transferable_skills_days_3rd_year, 
          NULL :: real AS transferable_skills_days_4th_year, 
          _all_roles.status 
        FROM 
          part_iii_studentship p 
          LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = p.id 
        WHERE 
          _all_roles.role_tablename = 'part_iii_studentship' :: text
      ) 
      UNION 
      SELECT 
        ph.person_id, 
        ph.start_date, 
        ph.intended_end_date, 
        ph.end_date, 
        ph.funding_end_date, 
        ph.chem, 
        _all_roles.post_category, 
        ph.job_title, 
        ph.supervisor_id AS manager_id, 
        NULL :: text AS progress_notes, 
        ph.supervisor_id, 
        NULL :: text AS university, 
        NULL :: text AS gaf_number, 
        NULL :: bigint AS postgraduate_studentship_type_id, 
        NULL :: text AS cpgs_title_html, 
        NULL :: text AS mphil_title_html, 
        NULL :: text AS msc_title_html, 
        NULL :: date AS cpgs_or_mphil_date_submission_due, 
        NULL :: date AS cpgs_or_mphil_date_submitted, 
        NULL :: date AS cpgs_or_mphil_date_awarded, 
        NULL :: date AS mphil_date_submission_due, 
        NULL :: date AS mphil_date_submitted, 
        NULL :: date AS mphil_date_awarded, 
        NULL :: date AS date_registered_for_phd, 
        NULL :: text AS phd_thesis_title_html, 
        NULL :: date AS phd_date_title_approved, 
        NULL :: date AS date_phd_submission_due, 
        NULL :: date AS phd_four_year_submission_date, 
        NULL :: date AS phd_date_submitted, 
        NULL :: date AS phd_date_examiners_appointed, 
        NULL :: date AS phd_date_of_viva, 
        NULL :: date AS phd_date_awarded, 
        NULL :: bigint AS co_supervisor_id, 
        ph.mentor_id, 
        NULL :: bigint AS co_mentor_id, 
        ph.next_mentor_meeting_due, 
        ph.filemaker_funding AS current_funding, 
        NULL :: text AS fees_funding, 
        ph.research_grant_number, 
        NULL :: text AS part_iii_project_title_html, 
        ph.paid_by_university, 
        ph.date_of_first_probation_meeting, 
        ph.date_of_second_probation_meeting, 
        ph.date_of_third_probation_meeting, 
        ph.date_probation_letters_sent, 
        ph.probation_period, 
        NULL :: text AS emplid_number, 
        ph.next_review_due, 
        ph.timing_of_reviews, 
        NULL :: real AS transferable_skills_days_1st_year, 
        NULL :: real AS transferable_skills_days_2nd_year, 
        NULL :: real AS transferable_skills_days_3rd_year, 
        NULL :: real AS transferable_skills_days_4th_year, 
        _all_roles.status 
      FROM 
        post_history ph 
        LEFT JOIN staff_category ON ph.staff_category_id = staff_category.id 
        LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = ph.id 
      WHERE 
        _all_roles.role_tablename = 'post_history' :: text
    ) 
    UNION 
    SELECT 
      pg.person_id, 
      pg.start_date, 
      _all_roles.intended_end_date, 
      _all_roles.end_date, 
      pg.funding_end_date, 
      true AS chem, 
      _all_roles.post_category, 
      NULL :: text AS job_title, 
      NULL :: bigint AS manager_id, 
      pg.progress_notes, 
      pg.first_supervisor_id AS supervisor_id, 
      pg.university, 
      pg.gaf_number, 
      pg.postgraduate_studentship_type_id, 
      pg.cpgs_title AS cpgs_title_html, 
      pg.mphil_title AS mphil_title_html, 
      pg.msc_title AS msc_title_html, 
      pg.cpgs_or_mphil_date_submission_due, 
      pg.cpgs_or_mphil_date_submitted, 
      pg.cpgs_or_mphil_date_awarded, 
      pg.mphil_date_submission_due, 
      pg.mphil_date_submitted, 
      pg.mphil_date_awarded, 
      pg.date_registered_for_phd, 
      pg.phd_thesis_title AS phd_title_html, 
      pg.phd_date_title_approved, 
      pg.date_phd_submission_due, 
      pg.end_of_registration_date AS phd_four_year_submission_date, 
      pg.phd_date_submitted, 
      pg.phd_date_examiners_appointed, 
      pg.phd_date_of_viva, 
      pg.phd_date_awarded, 
      pg.second_supervisor_id AS co_supervisor_id, 
      pg.first_mentor_id AS mentor_id, 
      pg.second_mentor_id AS co_mentor_id, 
      pg.next_mentor_meeting_due, 
      pg.filemaker_funding AS current_funding, 
      pg.filemaker_fees_funding AS fees_funding, 
      NULL :: text AS research_grant_number, 
      NULL :: text AS part_iii_project_title_html, 
      pg.paid_through_payroll AS paid_by_university, 
      NULL :: date AS date_of_first_probation_meeting, 
      NULL :: date AS date_of_second_probation_meeting, 
      NULL :: date AS date_of_third_probation_meeting, 
      NULL :: date AS date_probation_letters_sent, 
      NULL :: text AS probation_period, 
      pg.emplid_number, 
      NULL :: date AS next_review_due, 
      NULL :: text AS timing_of_reviews, 
      pg.transferable_skills_days_1st_year, 
      pg.transferable_skills_days_2nd_year, 
      pg.transferable_skills_days_3rd_year, 
      pg.transferable_skills_days_4th_year, 
      _all_roles.status 
    FROM 
      postgraduate_studentship pg 
      LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = pg.postgraduate_studentship_type_id 
      LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = pg.id 
    WHERE 
      _all_roles.role_tablename = 'postgraduate_studentship' :: text
  ) roles ON roles.person_id = person.id 
  LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id 
ORDER BY 
  person.surname, 
  person.first_names, 
  roles.start_date;

ALTER TABLE hotwire."30_Report/Searching" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Searching" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Searching" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Searching" TO hr;
GRANT ALL ON TABLE hotwire."30_Report/Searching" TO cen1001;

-- short searching create
CREATE VIEW hotwire."30_Report/Short_Searching" AS 
SELECT 
  a.id, 
  a.surname, 
  a.first_names, 
  a.title_id, 
  a.known_as, 
  a.gender_id, 
  a.ro_age, 
  a.start_date, 
  a.end_date, 
  a.physical_status_id, 
  a.funding_end_date, 
  a.post_category, 
  a.role_status_id, 
  a.chem, 
  a.registration_completed, 
  a.clearance_cert_signed, 
  a.email_address, 
  a.hide_email, 
  a.permission_to_give_out_details, 
  a.room_id, 
  a.location, 
  a.dept_telephone_number_id, 
  a.research_group_id, 
  a.supervisor_id, 
  a.manager_id, 
  a.paid_by_university, 
  a.continuous_employment_start_date, 
  a.payroll_personal_reference_number, 
  a.paper_file_details, 
  a.ni_number, 
  a.job_title, 
  a.current_funding, 
  a.research_grant_number, 
  a.university, 
  a.nationality_id, 
  a.cambridge_college_id, 
  a.cpgs_or_mphil_date_submission_due, 
  a.cpgs_or_mphil_date_submitted, 
  a.cpgs_or_mphil_date_awarded, 
  a.mphil_date_submission_due, 
  a.mphil_date_submitted, 
  a.mphil_date_awarded, 
  a.date_registered_for_phd, 
  a.phd_date_title_approved, 
  a.date_phd_submission_due, 
  a.phd_four_year_submission_date, 
  a.phd_date_submitted, 
  a.phd_date_examiners_appointed, 
  a.phd_date_of_viva, 
  a.phd_date_awarded, 
  a.co_supervisor_id, 
  a.mentor_id, 
  a.co_mentor_id, 
  a.next_mentor_meeting_due, 
  a.fees_funding, 
  a.part_iii_project_title_html, 
  a.emplid_number, 
  a.reviewer_id, 
  a.next_review_due, 
  a.timing_of_reviews 
FROM 
  (
    SELECT 
      "30_Report/Searching".id, 
      "30_Report/Searching".surname, 
      "30_Report/Searching".first_names, 
      "30_Report/Searching".title_id, 
      "30_Report/Searching".known_as, 
      "30_Report/Searching".gender_id, 
      date_part(
        'year' :: text, 
        age("30_Report/Searching".date_of_birth)
      ) AS ro_age, 
      "30_Report/Searching".start_date, 
      "30_Report/Searching".end_date, 
      "30_Report/Searching".physical_status_id, 
      "30_Report/Searching".funding_end_date, 
      "30_Report/Searching".post_category, 
      "30_Report/Searching".role_status_id, 
      "30_Report/Searching".chem, 
      "30_Report/Searching".registration_completed, 
      "30_Report/Searching".clearance_cert_signed, 
      "30_Report/Searching".email_address, 
      "30_Report/Searching".hide_email, 
      "30_Report/Searching".permission_to_give_out_details, 
      "30_Report/Searching".room_id, 
      "30_Report/Searching".location, 
      "30_Report/Searching".dept_telephone_number_id, 
      "30_Report/Searching".research_group_id, 
      "30_Report/Searching".supervisor_id, 
      "30_Report/Searching".manager_id, 
      "30_Report/Searching".paid_by_university, 
      "30_Report/Searching".continuous_employment_start_date, 
      "30_Report/Searching".payroll_personal_reference_number, 
      "30_Report/Searching".paper_file_details, 
      "30_Report/Searching".ni_number, 
      "30_Report/Searching".job_title, 
      "30_Report/Searching".current_funding, 
      "30_Report/Searching".research_grant_number, 
      "30_Report/Searching".university, 
      "30_Report/Searching".nationality_id, 
      "30_Report/Searching".cambridge_college_id, 
      "30_Report/Searching".cpgs_or_mphil_date_submission_due, 
      "30_Report/Searching".cpgs_or_mphil_date_submitted, 
      "30_Report/Searching".cpgs_or_mphil_date_awarded, 
      "30_Report/Searching".mphil_date_submission_due, 
      "30_Report/Searching".mphil_date_submitted, 
      "30_Report/Searching".mphil_date_awarded, 
      "30_Report/Searching".date_registered_for_phd, 
      "30_Report/Searching".phd_date_title_approved, 
      "30_Report/Searching".date_phd_submission_due, 
      "30_Report/Searching".phd_four_year_submission_date, 
      "30_Report/Searching".phd_date_submitted, 
      "30_Report/Searching".phd_date_examiners_appointed, 
      "30_Report/Searching".phd_date_of_viva, 
      "30_Report/Searching".phd_date_awarded, 
      "30_Report/Searching".co_supervisor_id, 
      "30_Report/Searching".mentor_id, 
      "30_Report/Searching".co_mentor_id, 
      "30_Report/Searching".next_mentor_meeting_due, 
      "30_Report/Searching".fees_funding, 
      "30_Report/Searching".part_iii_project_title_html, 
      "30_Report/Searching".emplid_number, 
      "30_Report/Searching".reviewer_id, 
      "30_Report/Searching".next_review_due, 
      "30_Report/Searching".timing_of_reviews 
    FROM 
      hotwire."30_Report/Searching"
  ) a 
ORDER BY 
  a.surname, 
  a.first_names, 
  a.start_date;

ALTER TABLE hotwire."30_Report/Short_Searching" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Short_Searching" TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Short_Searching" TO cen1001;
GRANT SELECT ON TABLE hotwire."30_Report/Short_Searching" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Short_Searching" TO hr;

-- source of funds create
CREATE 
OR REPLACE VIEW hotwire."30_Report/Source_of_Funds" AS 
SELECT 
  a.id, 
  a.surname, 
  a.first_names, 
  a.post_category, 
  a.start_date, 
  a.current_funding, 
  a.date_registered_for_phd, 
  a.nationality_id, 
  a.supervisor_id 
FROM 
  (
    SELECT 
      "30_Report/Searching".id, 
      "30_Report/Searching".surname, 
      "30_Report/Searching".first_names, 
      "30_Report/Searching".post_category, 
      "30_Report/Searching".start_date, 
      "30_Report/Searching".current_funding, 
      "30_Report/Searching".date_registered_for_phd, 
      "30_Report/Searching".nationality_id, 
      "30_Report/Searching".supervisor_id 
    FROM 
      hotwire."30_Report/Searching"
  ) a 
ORDER BY 
  a.surname, 
  a.first_names;

ALTER TABLE hotwire."30_Report/Source_of_Funds" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Source_of_Funds" TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Source_of_Funds" TO cen1001;
GRANT SELECT ON TABLE hotwire."30_Report/Source_of_Funds" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."30_Report/Source_of_Funds" TO student_management;

-- personnel data entry
---- drop old stuff
DROP RULE hotwire_view_personnel_data_entry_del ON hotwire."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire_view_personnel_data_entry_ins ON hotwire."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire_view_personnel_data_entry_upd ON hotwire."10_View/People/Personnel_Data_Entry";
DROP FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry");
DROP VIEW hotwire."10_View/People/Personnel_Data_Entry";

---- create view
CREATE VIEW hotwire."10_View/People/Personnel_Data_Entry" AS 
SELECT 
  a.id, 
  a.ro_person_id, 
  a.image_oid, 
  a.surname, 
  a.first_names, 
  a.title_id, 
  a.known_as :: text AS known_as, 
  a.name_suffix, 
  a.maiden_name :: text AS previous_surname, 
  a.date_of_birth, 
  a.ro_age, 
  a.gender_id, 
  a.ro_post_category_id, 
  a.ro_chem, 
  a.crsid, 
  a.email_address, 
  a.hide_email, 
  a.arrival_date, 
  a.leaving_date, 
  a.ro_physical_status_id, 
  a.left_but_no_leaving_date_given, 
  a.dept_telephone_number_id, 
  a.hide_phone_no_from_website, 
  a.hide_from_website, 
  a.room_id, 
  a.ro_supervisor_id, 
  a.ro_co_supervisor_id, 
  a.research_group_id, 
  a.home_address, 
  a.home_phone_number :: text AS home_phone_number, 
  a.cambridge_college_id, 
  a.mobile_number :: text AS mobile_number, 
  a.nationality_id, 
  a.emergency_contact, 
  a.location :: text AS location, 
  a.notes, 
  a.forwarding_address, 
  a.new_employer_address, 
  a.chem_at_cam AS "chem_@_cam", 
  a.retired_staff_mailing_list, 
  a.payroll_personal_reference_number :: text AS payroll_personal_reference_number, 
  a.continuous_employment_start_date, 
  a.paper_file_details :: text AS paper_file_status, 
  a.registration_completed, 
  a.clearance_cert_signed, 
  a._cssclass, 
  a.treat_as_academic_staff 
FROM 
  (
    SELECT 
      person.id, 
      person.id AS ro_person_id, 
      ROW(
        'image/jpeg' :: character varying, 
        person.image_oid
      ):: blobtype AS image_oid, 
      person.surname, 
      person.first_names, 
      person.title_id, 
      person.known_as, 
      person.name_suffix, 
      person.maiden_name, 
      person.date_of_birth, 
      date_part(
        'year' :: text, 
        age( person.date_of_birth)
      ) AS ro_age, 
      person.gender_id, 
      _latest_role.post_category_id AS ro_post_category_id, 
      _latest_role.chem AS ro_chem, 
      person.crsid, 
      person.email_address, 
      person.hide_email, 
      person.do_not_show_on_website AS hide_from_website, 
      person.arrival_date, 
      person.leaving_date, 
      _physical_status_v3.status_id AS ro_physical_status_id, 
      person.left_but_no_leaving_date_given, 
      ARRAY(
        SELECT 
          mm_person_dept_telephone_number.dept_telephone_number_id 
        FROM 
          mm_person_dept_telephone_number 
        WHERE 
          person.id = mm_person_dept_telephone_number.person_id
      ) AS dept_telephone_number_id, 
      person.hide_phone_no_from_website, 
      ARRAY(
        SELECT 
          mm_person_room.room_id 
        FROM 
          mm_person_room 
        WHERE 
          person.id = mm_person_room.person_id
      ) AS room_id, 
      _latest_role.supervisor_id AS ro_supervisor_id, 
      _latest_role.co_supervisor_id AS ro_co_supervisor_id, 
      _latest_role.mentor_id AS ro_mentor_id, 
      ARRAY(
        SELECT 
          mm_person_research_group.research_group_id 
        FROM 
          mm_person_research_group 
        WHERE 
          person.id = mm_person_research_group.person_id
      ) AS research_group_id, 
      person.cambridge_address AS home_address, 
      person.cambridge_phone_number AS home_phone_number, 
      person.cambridge_college_id, 
      person.mobile_number, 
      ARRAY(
        SELECT 
          mm_person_nationality.nationality_id 
        FROM 
          mm_person_nationality 
        WHERE 
          mm_person_nationality.person_id = person.id
      ) AS nationality_id, 
      person.emergency_contact, 
      person.location, 
      person.notes_and_other_information::varchar(5000) as notes, 
      person.forwarding_address, 
      person.new_employer_address, 
      person.chem_at_cam, 
      CASE WHEN retired_staff_list.include_person_id IS NOT NULL THEN true ELSE false END AS retired_staff_mailing_list, 
      person.payroll_personal_reference_number, 
      person.continuous_employment_start_date, 
      person.paper_file_details, 
      person.registration_completed, 
      person.clearance_cert_signed, 
      person.counts_as_academic AS treat_as_academic_staff, 
      CASE WHEN _physical_status_v3.status_id :: text = 'Past' :: text THEN 'orange' :: text WHEN _physical_status_v3.status_id :: text = 'Unknown' :: text 
      AND person._status :: text = 'Past' :: text THEN 'orange' :: text ELSE NULL :: text END AS _cssclass 
    FROM 
      person 
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id 
      LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id 
      LEFT JOIN (
        SELECT 
          mm_mailinglist_include_person.mailinglist_id, 
          mm_mailinglist_include_person.include_person_id 
        FROM 
          mm_mailinglist_include_person 
          JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id 
        WHERE 
          mailinglist.name :: text = 'chem-retiredstaff' :: text
      ) retired_staff_list ON person.id = retired_staff_list.include_person_id
  ) a 
ORDER BY 
  a.surname, 
  a.first_names;


ALTER TABLE hotwire."10_View/People/Personnel_Data_Entry" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO tkd25;

---- create update function
CREATE FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry") RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_oid=(select (v.image_oid).val from (select v.image_oid) as x), 
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as::varchar(80),
                                name_suffix = v.name_suffix,
                                maiden_name = v.previous_surname::varchar(80), 
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                cambridge_address = v.home_address,
                                cambridge_phone_number = v.home_phone_number::varchar(80), 
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number::varchar(80),
                                emergency_contact = v.emergency_contact, 
                                location = v.location::varchar(80),
                                notes_and_other_information = v.notes::text,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                chem_at_cam = v."chem_@_cam",
                                payroll_personal_reference_number = v.payroll_personal_reference_number::varchar(80), 
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_status::varchar(80), 
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                        image_oid, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        maiden_name, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        emergency_contact, 
                                        location,
                                        notes_and_other_information,
                                        forwarding_address,
                                        new_employer_address, 
                                        chem_at_cam,
                                        payroll_personal_reference_number, 
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        (select (v.image_oid).val from (select v.image_oid) as x),
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as::varchar(80),
                                        v.name_suffix,
                                        v.previous_surname::varchar(80), 
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.home_address,
                                        v.home_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number::varchar(80),
                                        v.emergency_contact, 
                                        v.location::varchar(80),
                                        v.notes::text,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v."chem_@_cam",
                                        v.payroll_personal_reference_number::varchar(80), 
                                        v.continuous_employment_start_date,
                                        v.paper_file_status::varchar(80), 
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry") OWNER TO dev;

---- create rules
CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_del AS
    ON DELETE TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  DELETE FROM person
      WHERE person.id = old.id;
CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_ins AS
    ON INSERT TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire.personnel_data_entry_upd(new.*) AS id;
CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire.personnel_data_entry_upd(new.*) AS id;

-- personnel history drop and create
DROP VIEW hotwire."10_View/People/Personnel_History";
CREATE VIEW hotwire."10_View/People/Personnel_History" AS WITH personnel_history AS (
  SELECT 
    person.id, 
    person_hid.person_hid AS ro_person, 
    person.surname, 
    person.first_names, 
    person.title_id, 
    date_part( 'year' :: text, age( person.date_of_birth)) AS ro_age, 
    _latest_role.post_category_id AS ro_post_category_id, 
    person.continuous_employment_start_date, 
    _latest_employment.start_date AS ro_latest_employment_start_date, 
    COALESCE(
      _latest_employment.end_date, _latest_employment.intended_end_date
    ) AS ro_employment_end_date, 
    age(
      COALESCE( _latest_employment.end_date, current_date):: timestamp with time zone, 
      person.continuous_employment_start_date :: timestamp with time zone
    ):: text AS ro_length_of_continuous_service, 
    age(
      COALESCE( _latest_employment.end_date, current_date):: timestamp with time zone, 
      _latest_employment.start_date :: timestamp with time zone
    ):: text AS ro_length_of_service_in_current_contract, 
    age(
      _latest_employment.end_date :: timestamp with time zone, 
      person.continuous_employment_start_date :: timestamp with time zone
    ) AS ro_final_length_of_continuous_service, 
    age(
      _latest_employment.end_date :: timestamp with time zone, 
      _latest_employment.start_date :: timestamp with time zone
    ) AS ro_final_length_of_service_in_last_contract, 
    _latest_role.supervisor_id AS ro_supervisor_id, 
    person.notes_and_other_information::varchar(5000) as notes, 
    CASE WHEN _physical_status_v2.status_id :: text = 'Past' :: text THEN 'orange' :: text WHEN _physical_status_v2.status_id :: text = 'Unknown' :: text 
    AND person._status :: text = 'Past' :: text THEN 'orange' :: text ELSE NULL :: text END AS _cssclass, 
    _to_hwsubviewb(
      '10_View/People/Staff_Review_History' :: character varying, 
      'person_id' :: character varying, 
      '10_View/People/Staff_Review_History' :: character varying, 
      NULL :: character varying, NULL :: character varying
    ) AS staff_review_history_subview, 
    _to_hwsubviewb(
      '10_View/Roles/_Cambridge_History_V2' :: character varying, 
      'person_id' :: character varying, 
      '_target_viewname' :: character varying, 
      '_role_xid' :: character varying, 
      NULL :: character varying
    ) AS cambridge_history_subview 
  FROM 
    person 
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id 
    LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id 
    LEFT JOIN person_hid ON person_hid.person_id = person.id 
    LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id
) 
SELECT 
  personnel_history.id, 
  personnel_history.ro_person, 
  personnel_history.surname, 
  personnel_history.first_names, 
  personnel_history.title_id, 
  personnel_history.ro_age, 
  personnel_history.ro_post_category_id, 
  personnel_history.continuous_employment_start_date, 
  personnel_history.ro_latest_employment_start_date, 
  personnel_history.ro_employment_end_date, 
  personnel_history.ro_length_of_continuous_service, 
  personnel_history.ro_length_of_service_in_current_contract, 
  personnel_history.ro_final_length_of_continuous_service, 
  personnel_history.ro_final_length_of_service_in_last_contract, 
  personnel_history.ro_supervisor_id, 
  personnel_history.notes, 
  personnel_history._cssclass, 
  personnel_history.staff_review_history_subview, 
  personnel_history.cambridge_history_subview 
FROM 
  personnel_history 
ORDER BY 
  personnel_history.surname, 
  personnel_history.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_History" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO accounts;

CREATE OR REPLACE RULE hotwire_view_personnel_history_v2_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_History" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, notes_and_other_information = new.notes::text
  WHERE person.id = old.id;
