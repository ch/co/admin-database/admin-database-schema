CREATE VIEW hotwire3."30_Report/REF_2021" AS
  WITH ref2021 AS (
  SELECT
    le.person_id as id,
    le.person_id,
    p.crsid,
    p.continuous_employment_start_date,
    le.start_date as current_contract_start_date,
    le.intended_end_date as current_contract_end_date,
    le.end_date as date_contract_ended,
    le.funding,
    le.post_category,
    ph.job_title,
    ph.percentage_of_fulltime_hours,
    p.arrival_date as date_arrived_in_department,
    p.leaving_date as date_left_department_if_known,
    ps.status_id as presence,
    le.supervisor_id,
    p.surname as _surname,
    p.first_names as _first_names
  FROM _latest_employment as le
  JOIN person p ON p.id = le.person_id
  JOIN _physical_status_v3 ps USING (person_id)
  JOIN post_history ph on ph.id = le.role_id
  WHERE le.role_tablename = 'post_history'
  )
SELECT
  id,
  person_id,
  crsid,
  continuous_employment_start_date,
  current_contract_start_date,
  current_contract_end_date,
  date_contract_ended,
  funding,
  post_category,
  job_title,
  percentage_of_fulltime_hours,
  date_arrived_in_department,
  date_left_department_if_known,
  presence,
  supervisor_id,
  _surname,
  _first_names
FROM ref2021
ORDER BY _surname, _first_names;

ALTER VIEW hotwire3."30_Report/REF_2021" OWNER TO dev;
GRANT SELECT ON hotwire3."30_Report/REF_2021" TO ref2021; 
