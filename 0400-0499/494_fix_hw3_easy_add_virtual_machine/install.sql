DROP VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";

CREATE VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" AS 
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id, ''::character varying(63) AS hostname, NULL::integer AS subnet_id, NULL::integer AS hardware_id, NULL::macaddr AS wired_mac_1, ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text ILIKE '%debian%'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id, ''::text AS system_image_comments, NULL::integer AS host_system_image_id, NULL::macaddr AS wired_mac_2, ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id, ( SELECT mm_person_research_group.research_group_id
           FROM person
      JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     WHERE person.crsid::name = "current_user"()
    LIMIT 1) AS research_group_id;

ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.upd_easy_add_virtual_machine()
  RETURNS trigger AS
$BODY$

     declare
                      ip_id BIGINT;                                                                                        
     begin                                                                                                              
	ip_id =  (SELECT ip_address.id
			FROM ip_address
			WHERE ip_address.subnet_id = NEW.subnet_id 
				AND ip_address.reserved <> true 
				AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
			LIMIT 1);

	INSERT INTO system_image 
		(wired_mac_1, wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
		VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.system_image_comments::varchar(1000), NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id);
	update ip_address set hostname = NEW.hostname where id = ip_id;
	insert into mm_system_image_ip_address ( ip_address_id, system_image_id )
		values ( ip_id, currval('system_image_id_seq'::regclass));
	return NEW;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.upd_easy_add_virtual_machine()
  OWNER TO dev;

CREATE TRIGGER hotwire3_view_easy_add_virtual_machine_view_upd
  INSTEAD OF UPDATE
  ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.upd_easy_add_virtual_machine();
