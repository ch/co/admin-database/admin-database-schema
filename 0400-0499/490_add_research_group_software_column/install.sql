-- The software licence compliance exercise has revealed that there are several
-- groups who exist for administrative purposes but don't hold software in the
-- normal sense, eg UCC, Theory (umbrella groups where the subgroups would handle the 
-- licence compliance), Part III (we no longer put students into this group), Nano DTC (all 
-- students are embedded in other groups while they're here but are part of the Nano DTC program)
-- So as not to bother people with compliance demands we introduce this flag. Use sparingly.
ALTER TABLE research_group ADD COLUMN ignore_for_software_compliance_purposes boolean default 'f' not null;
