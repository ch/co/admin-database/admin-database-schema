CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Data_Entry" AS 
 SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, a.known_as::text AS known_as, a.name_suffix, a.maiden_name::text AS previous_surname, a.date_of_birth, a.ro_age, a.gender_id, a.ro_post_category_id, a.ro_chem, a.crsid, a.email_address, a.hide_email, a.arrival_date, a.leaving_date, a.ro_physical_status_id, a.left_but_no_leaving_date_given, a.dept_telephone_number_id, a.hide_phone_no_from_website, a.hide_from_website, a.room_id, a.ro_supervisor_id, a.ro_co_supervisor_id, a.research_group_id, a.home_address, a.home_phone_number::text AS home_phone_number, a.cambridge_college_id, a.mobile_number::text AS mobile_number, a.nationality_id, a.emergency_contact, a.location::text AS location, a.other_information, a.notes, a.forwarding_address, a.new_employer_address, a.chem_at_cam AS "chem_@_cam", a.retired_staff_mailing_list, a.payroll_personal_reference_number::text AS payroll_personal_reference_number, a.continuous_employment_start_date, a.paper_file_details::text AS paper_file_status, a.registration_completed, a.clearance_cert_signed, a._cssclass, a.treat_as_academic_staff
   FROM ( SELECT person.id, person.id AS ro_person_id, ROW('image/jpeg'::character varying, person.image_oid)::blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.date_of_birth, date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, _latest_role.chem AS ro_chem, person.crsid, person.email_address, person.hide_email, person.do_not_show_on_website AS hide_from_website, person.arrival_date, person.leaving_date, _physical_status_v3.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.co_supervisor_id AS ro_co_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.cambridge_address AS home_address, person.cambridge_phone_number AS home_phone_number, person.cambridge_college_id, person.mobile_number, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE mm_person_nationality.person_id = person.id) AS nationality_id, person.emergency_contact, person.location, person.other_information::character varying(5000) AS other_information, person.notes::character varying(5000) AS notes, person.forwarding_address, person.new_employer_address, person.chem_at_cam, 
                CASE
                    WHEN retired_staff_list.include_person_id IS NOT NULL THEN true
                    ELSE false
                END AS retired_staff_mailing_list, person.payroll_personal_reference_number, person.continuous_employment_start_date, person.paper_file_details, person.registration_completed, person.clearance_cert_signed, person.counts_as_academic AS treat_as_academic_staff, 
                CASE
                    WHEN _physical_status_v3.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN _physical_status_v3.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
   LEFT JOIN ( SELECT mm_mailinglist_include_person.mailinglist_id, mm_mailinglist_include_person.include_person_id
         FROM mm_mailinglist_include_person
    JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
   WHERE mailinglist.name::text = 'chem-retiredstaff'::text) retired_staff_list ON person.id = retired_staff_list.include_person_id) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Data_Entry"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO tkd25;
