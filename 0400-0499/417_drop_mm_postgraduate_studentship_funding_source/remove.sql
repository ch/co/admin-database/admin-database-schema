CREATE TABLE funding_type
(
  id bigint NOT NULL DEFAULT nextval('funding_type_id_seq'::regclass),
  type character varying(80) NOT NULL,
  CONSTRAINT pk_funding_type PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE funding_type
  OWNER TO cen1001;

CREATE UNIQUE INDEX idx_funding_type
  ON funding_type
  USING btree
  (type COLLATE pg_catalog."default");

CREATE OR REPLACE VIEW hotwire.funding_type_hid AS 
 SELECT funding_type.id AS funding_type_id, funding_type.type AS funding_type_hid
    FROM funding_type;

    ALTER TABLE hotwire.funding_type_hid
      OWNER TO postgres;
      GRANT ALL ON TABLE hotwire.funding_type_hid TO postgres;
      GRANT ALL ON TABLE hotwire.funding_type_hid TO ro_hid;


CREATE TABLE funding_source
(
  id bigserial NOT NULL,
  source character varying(80),
  reference_number character varying(500),
  CONSTRAINT pk_funding_source PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE funding_source
  OWNER TO cen1001;
GRANT ALL ON TABLE funding_source TO cen1001;
GRANT ALL ON TABLE funding_source TO dev;

CREATE TABLE post_funded
(
  id bigserial NOT NULL,
  percentage integer,
  start_date date,
  end_date date,
  funding_source_id bigint NOT NULL,
  post_history_id bigint NOT NULL,
  CONSTRAINT pk_post_funded PRIMARY KEY (id),
  CONSTRAINT post_funded_fk_funding_source FOREIGN KEY (funding_source_id)
      REFERENCES funding_source (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT post_funded_fk_post_history FOREIGN KEY (post_history_id)
      REFERENCES post_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE post_funded
  OWNER TO cen1001;
GRANT ALL ON TABLE post_funded TO cen1001;
GRANT ALL ON TABLE post_funded TO dev;

CREATE OR REPLACE VIEW hotwire.funding_source_hid AS 
 SELECT funding_source.id AS funding_source_id, funding_source.source::text || COALESCE((' ('::text || funding_source.reference_number::text) || ')'::text, ''::text) AS funding_source_hid
    FROM funding_source;

    ALTER TABLE hotwire.funding_source_hid
      OWNER TO postgres;
      GRANT ALL ON TABLE hotwire.funding_source_hid TO postgres;
      GRANT ALL ON TABLE hotwire.funding_source_hid TO ro_hid;

CREATE TABLE mm_postgraduate_studentship_funding_source
(
  id bigint NOT NULL DEFAULT nextval('mm_postgraduate_studentship_funding_source_id_seq'::regclass),
  percentage integer DEFAULT 100,
  start_date date,
  end_date date,
  postgraduate_studentship_id bigint NOT NULL,
  funding_source_id bigint NOT NULL,
  funding_type_id bigint NOT NULL,
  CONSTRAINT pk_mm_postgraduate_studentship_funding_source PRIMARY KEY (id),
  CONSTRAINT mm_postgraduate_studentship_funding_source_fk_funding_source FOREIGN KEY (funding_source_id)
      REFERENCES funding_source (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mm_postgraduate_studentship_funding_source_fk_funding_type FOREIGN KEY (funding_type_id)
      REFERENCES funding_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mm_postgraduate_studentship_funding_source_fk_postgraduate_stud FOREIGN KEY (postgraduate_studentship_id)
      REFERENCES postgraduate_studentship (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mm_postgraduate_studentship_funding_source
  OWNER TO cen1001;
GRANT ALL ON TABLE mm_postgraduate_studentship_funding_source TO cen1001;
GRANT SELECT ON TABLE mm_postgraduate_studentship_funding_source TO mgmt_ro;
