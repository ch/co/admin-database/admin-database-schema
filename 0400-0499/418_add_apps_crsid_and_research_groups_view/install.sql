CREATE VIEW apps.username_and_research_group AS 
  SELECT 
    username,
    ARRAY(
      SELECT 
        r.name
      FROM research_group r 
      JOIN mm_person_research_group mm ON r.id = mm.research_group_id 
      JOIN person p2 ON mm.person_id = p2.id 
      WHERE p2.id = person.id
    ) AS research_groups
  FROM user_account
  LEFT JOIN person ON user_account.person_id = person.id 
  WHERE user_account.username IS NOT NULL;

ALTER VIEW apps.username_and_research_group OWNER TO dev;
GRANT SELECT ON apps.username_and_research_group TO PUBLIC;
COMMENT ON VIEW apps.username_and_research_group IS $$crsids and the research groups their owners are associated with. This is publically accessible beause there's nothing in it that's not already in the online phonebook$$;
