DROP VIEW hotwire."10_View/Roles/Post_History";
CREATE VIEW hotwire."10_View/Roles/Post_History" AS 
SELECT 
  a.id, 
  a.staff_category_id, 
  a.person_id, 
  a.ro_role_status_id, 
  a.supervisor_id, 
  a.mentor_id, 
  a.external_mentor, 
  a.start_date_for_continuous_employment_purposes, 
  a.start_date, 
  a.intended_end_date, 
  a.end_date, 
  a.funding_end_date, 
  a.chem, 
  a.paid_by_university, 
  a.hours_worked, 
  a.percentage_of_fulltime_hours, 
  a.research_grant_number, 
  a.sponsor, 
  a.probation_period, 
  a.date_of_first_probation_meeting, 
  a.date_of_second_probation_meeting, 
  a.date_of_third_probation_meeting, 
  a.date_of_fourth_probation_meeting, 
  a.first_probation_meeting_comments, 
  a.second_probation_meeting_comments, 
  a.third_probation_meeting_comments, 
  a.fourth_probation_meeting_comments, 
  a.probation_outcome, 
  a.date_probation_letters_sent, 
  a.job_title, 
  a.force_role_status_to_past, 
  a._person 
FROM 
  (
    SELECT 
      post_history.id, 
      post_history.staff_category_id, 
      post_history.person_id, 
      _all_roles.status AS ro_role_status_id, 
      post_history.supervisor_id, 
      post_history.mentor_id, 
      post_history.external_mentor, 
      person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, 
      post_history.start_date, 
      post_history.intended_end_date, 
      post_history.end_date, 
      post_history.funding_end_date, 
      post_history.chem, 
      post_history.paid_by_university, 
      post_history.hours_worked, 
      post_history.percentage_of_fulltime_hours, 
      post_history.probation_period, 
      post_history.date_of_first_probation_meeting, 
      post_history.date_of_second_probation_meeting, 
      post_history.date_of_third_probation_meeting, 
      post_history.date_of_fourth_probation_meeting, 
      post_history.first_probation_meeting_comments, 
      post_history.second_probation_meeting_comments, 
      post_history.third_probation_meeting_comments, 
      post_history.fourth_probation_meeting_comments, 
      post_history.probation_outcome, 
      post_history.date_probation_letters_sent, 
      post_history.research_grant_number, 
      post_history.filemaker_funding AS sponsor, 
      post_history.job_title, 
      post_history.force_role_status_to_past, 
      person_hid.person_hid AS _person 
    FROM 
      post_history 
      LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id 
      AND _all_roles.role_tablename = 'post_history' :: text 
      JOIN person ON post_history.person_id = person.id 
      JOIN person_hid ON post_history.person_id = person_hid.person_id
  ) a 
ORDER BY 
  a._person, 
  a.start_date DESC;

ALTER TABLE hotwire."10_View/Roles/Post_History" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Post_History" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Roles/Post_History" TO mgmt_ro;
GRANT SELECT , UPDATE , INSERT, DELETE ON TABLE hotwire."10_View/Roles/Post_History" TO hr;

CREATE RULE hotwire_view_post_history_del AS ON DELETE TO hotwire."10_View/Roles/Post_History" DO INSTEAD DELETE FROM 
  post_history 
WHERE 
  post_history.id = old.id;

CREATE RULE hotwire_view_post_history_update_rules AS ON UPDATE TO hotwire."10_View/Roles/Post_History" DO INSTEAD (
  UPDATE 
    person 
  SET 
    continuous_employment_start_date = new.start_date_for_continuous_employment_purposes 
  WHERE 
    person.id = new.person_id;
UPDATE 
  post_history 
SET 
  staff_category_id = new.staff_category_id, 
  supervisor_id = new.supervisor_id, 
  mentor_id = new.mentor_id, 
  external_mentor = new.external_mentor, 
  start_date = new.start_date, 
  intended_end_date = new.intended_end_date, 
  end_date = new.end_date, 
  funding_end_date = new.funding_end_date, 
  chem = new.chem, 
  paid_by_university = new.paid_by_university, 
  hours_worked = new.hours_worked, 
  percentage_of_fulltime_hours = new.percentage_of_fulltime_hours, 
  probation_period = new.probation_period, 
  date_of_first_probation_meeting = new.date_of_first_probation_meeting, 
  date_of_second_probation_meeting = new.date_of_second_probation_meeting, 
  date_of_third_probation_meeting = new.date_of_third_probation_meeting, 
  date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, 
  first_probation_meeting_comments = new.first_probation_meeting_comments, 
  second_probation_meeting_comments = new.second_probation_meeting_comments, 
  third_probation_meeting_comments = new.third_probation_meeting_comments, 
  fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, 
  probation_outcome = new.probation_outcome, 
  date_probation_letters_sent = new.date_probation_letters_sent, 
  research_grant_number = new.research_grant_number, 
  filemaker_funding = new.sponsor, 
  job_title = new.job_title, 
  force_role_status_to_past = new.force_role_status_to_past 
WHERE 
  post_history.id = old.id;
);

CREATE RULE post_history_ins AS ON INSERT TO hotwire."10_View/Roles/Post_History" DO INSTEAD (
  UPDATE 
    person 
  SET 
    continuous_employment_start_date = new.start_date_for_continuous_employment_purposes 
  WHERE 
    person.id = new.person_id;
INSERT INTO post_history (
  staff_category_id, person_id, supervisor_id, 
  mentor_id, external_mentor, start_date, 
  intended_end_date, end_date, funding_end_date, 
  chem, paid_by_university, hours_worked, 
  percentage_of_fulltime_hours, probation_period, 
  date_of_first_probation_meeting, 
  date_of_second_probation_meeting, 
  date_of_third_probation_meeting, 
  date_of_fourth_probation_meeting, 
  first_probation_meeting_comments, 
  second_probation_meeting_comments, 
  third_probation_meeting_comments, 
  fourth_probation_meeting_comments, 
  probation_outcome, date_probation_letters_sent, 
  research_grant_number, filemaker_funding, 
  job_title, force_role_status_to_past
) 
VALUES 
  (
    new.staff_category_id, new.person_id, 
    new.supervisor_id, new.mentor_id, 
    new.external_mentor, new.start_date, 
    new.intended_end_date, new.end_date, 
    new.funding_end_date, new.chem, 
    new.paid_by_university, new.hours_worked, 
    new.percentage_of_fulltime_hours, 
    new.probation_period, new.date_of_first_probation_meeting, 
    new.date_of_second_probation_meeting, 
    new.date_of_third_probation_meeting, 
    new.date_of_fourth_probation_meeting, 
    new.first_probation_meeting_comments, 
    new.second_probation_meeting_comments, 
    new.third_probation_meeting_comments, 
    new.fourth_probation_meeting_comments, 
    new.probation_outcome, new.date_probation_letters_sent, 
    new.research_grant_number, new.sponsor, 
    new.job_title, new.force_role_status_to_past
  ) RETURNING post_history.id, 
  post_history.staff_category_id, 
  post_history.person_id, 
  NULL :: character varying(10) AS "varchar", 
  post_history.supervisor_id, 
  post_history.mentor_id, 
  post_history.external_mentor, 
  NULL :: date AS date, 
  post_history.start_date, 
  post_history.intended_end_date, 
  post_history.end_date, 
  post_history.funding_end_date, 
  post_history.chem, 
  post_history.paid_by_university, 
  post_history.hours_worked, 
  post_history.percentage_of_fulltime_hours, 
  post_history.research_grant_number, 
  post_history.filemaker_funding, 
  post_history.probation_period, 
  post_history.date_of_first_probation_meeting, 
  post_history.date_of_second_probation_meeting, 
  post_history.date_of_third_probation_meeting, 
  post_history.date_of_fourth_probation_meeting, 
  post_history.first_probation_meeting_comments, 
  post_history.second_probation_meeting_comments, 
  post_history.third_probation_meeting_comments, 
  post_history.fourth_probation_meeting_comments, 
  post_history.probation_outcome, 
  post_history.date_probation_letters_sent, 
  post_history.job_title, 
  post_history.force_role_status_to_past, 
  NULL :: text AS text;
);

