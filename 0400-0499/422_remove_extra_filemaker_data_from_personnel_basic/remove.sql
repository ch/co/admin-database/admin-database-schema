-- drop everything in the right order
DROP RULE hotwire_view_personnel_basic_del ON hotwire."10_View/People/Personnel_Basic";
DROP RULE hotwire_view_personnel_basic_ins ON hotwire."10_View/People/Personnel_Basic";
DROP RULE hotwire_view_personnel_basic_upd ON hotwire."10_View/People/Personnel_Basic";
DROP FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic");
DROP VIEW hotwire."10_View/People/Personnel_Basic";

-- create view
CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Basic" AS 
SELECT 
	a.id,
        a.ro_person_id,
        a.image_oid,
        a.surname,
        a.first_names,
        a.title_id,
        a.name_suffix,
        a.email_address,
        a.hide_email,
        a.crsid,
        a.date_of_birth,
        a.ro_post_category_id,
        a.research_group_id,
        a.dept_telephone_number_id,
        a.room_id,
        a.location,
        a.ro_physical_status_id,
        a.ro_estimated_leaving_date,
        a.extra_filemaker_data,
        a.is_spri,
        a._hl_status
FROM ( 
	SELECT 
	person.id,
        person.id AS ro_person_id,
        ROW('image/jpeg'::character varying,
        person.image_oid)::blobtype AS image_oid,
        person.surname,
        person.first_names,
        person.title_id,
        person.name_suffix,
        person.email_address,
        person.hide_email,
        person.crsid,
        person.date_of_birth,
        person.is_spri,
        _latest_role_v10.post_category_id AS ro_post_category_id,
        ARRAY( 
		SELECT mm_person_research_group.research_group_id
		FROM mm_person_research_group
		WHERE person.id = mm_person_research_group.person_id
	) AS research_group_id,
	ARRAY(
		SELECT mm_person_dept_telephone_number.dept_telephone_number_id
		FROM mm_person_dept_telephone_number
		WHERE person.id = mm_person_dept_telephone_number.person_id
	) AS dept_telephone_number_id,
	ARRAY(
		SELECT mm_person_room.room_id
		FROM mm_person_room
                WHERE person.id = mm_person_room.person_id
	) AS room_id,
	person.location,
        _physical_status_v2.status_id AS ro_physical_status_id,
        least(person.leaving_date,futuremost_role.estimated_leaving_date) as ro_estimated_leaving_date,
        person.extra_filemaker_data,
	CASE
	    WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
	    WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
	    ELSE NULL::text
	END AS _hl_status
	FROM person
	LEFT JOIN _latest_role_v10 ON person.id = _latest_role_v10.person_id
	LEFT JOIN apps.person_futuremost_role as futuremost_role ON person.id = futuremost_role.person_id
	LEFT JOIN _physical_status_v2 USING (id)
) a
ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Basic"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Basic" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Basic" TO cos;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Basic" TO phonebook_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Basic" TO hr;


-- create function which depends on view
CREATE OR REPLACE FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    		v_person_id BIGINT;                                                                                        
      begin                                                                                                              
      IF v.id IS NOT NULL THEN                                                                                           
        v_person_id= v.id;                                                                                               
	UPDATE person SET
		surname=v.surname,
		image_oid=(select (v.image_oid).val from (select v.image_oid) as x),
	        first_names=v.first_names,
		title_id=v.title_id,                                                       
		name_suffix = v.name_suffix,
		email_address=v.email_address, 
		hide_email=v.hide_email,
		crsid=v.crsid,
		date_of_birth=v.date_of_birth,
		location=v.location,
		extra_filemaker_data=v.extra_filemaker_data,
		is_spri=v.is_spri                                                                        
        WHERE person.id = v.id;                                                                                          
      ELSE    
	INSERT INTO person (surname, image_oid, first_names, title_id, name_suffix, email_address,hide_email,crsid, date_of_birth, location, extra_filemaker_data, is_spri) VALUES (v.surname, (select (v.image_oid).val from (select v.image_oid) as x),v.first_names, v.title_id, v.name_suffix, v.email_address,v.hide_email,v.crsid, v.date_of_birth, v.location, v.extra_filemaker_data, v.is_spri) returning id into v_person_id; 
    END IF; 

 perform fn_mm_array_update(v.dept_telephone_number_id,
                            'mm_person_dept_telephone_number'::varchar,
			    'person_id'::varchar,
                            'dept_telephone_number_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.research_group_id,
                            'mm_person_research_group'::varchar,
			    'person_id'::varchar,
                            'research_group_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.room_id,
                            'mm_person_room'::varchar,
                            'person_id'::varchar,
                            'room_id'::varchar,
                            v_person_id);
                                               
      return v_person_id;                                                                                                
      end;                                                

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  OWNER TO dev;

-- create update rules for view

-- Rule: hotwire_view_personnel_basic_del ON hotwire."10_View/People/Personnel_Basic"

CREATE OR REPLACE RULE hotwire_view_personnel_basic_del AS
    ON DELETE TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

-- Rule: hotwire_view_personnel_basic_ins ON hotwire."10_View/People/Personnel_Basic"

CREATE OR REPLACE RULE hotwire_view_personnel_basic_ins AS
    ON INSERT TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  SELECT hw_fn_personnel_basic_upd(new.*) AS id;

-- Rule: hotwire_view_personnel_basic_upd ON hotwire."10_View/People/Personnel_Basic"

CREATE OR REPLACE RULE hotwire_view_personnel_basic_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  SELECT hw_fn_personnel_basic_upd(new.*) AS id;
