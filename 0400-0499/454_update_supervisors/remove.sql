DROP VIEW hotwire.old_supervisor_hid;
DROP VIEW hotwire.new_supervisor_hid;

DROP RULE update_supervisors ON hotwire."10_View/Roles/Bulk_update_supervisors";
DROP FUNCTION hotwire.update_supervisors(hotwire."10_View/Roles/Bulk_update_supervisors");
DROP VIEW hotwire."10_View/Roles/Bulk_update_supervisors";
DROP VIEW hotwire."10_View/Roles/_current_people_supervised_by";

REVOKE SELECT,UPDATE ON postgraduate_studentship, erasmus_socrates_studentship, part_iii_studentship FROM hr;

