GRANT SELECT,UPDATE ON postgraduate_studentship, erasmus_socrates_studentship, part_iii_studentship TO hr;


CREATE VIEW hotwire.old_supervisor_hid AS
SELECT person_id as old_supervisor_id, person_hid as old_supervisor_hid
FROM hotwire.person_hid;
ALTER VIEW hotwire.old_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire.old_supervisor_hid TO ro_hid;

CREATE VIEW hotwire.new_supervisor_hid AS
SELECT person_id as new_supervisor_id, person_hid as new_supervisor_hid
FROM hotwire.person_hid;
ALTER VIEW hotwire.new_supervisor_hid OWNER TO dev;
GRANT SELECT ON hotwire.new_supervisor_hid TO ro_hid;


CREATE VIEW hotwire."10_View/Roles/_current_people_supervised_by" AS
SELECT
    role_id as id,
    person_id,
    start_date,
    intended_end_date,
    end_date,
    funding_end_date,
    post_category,
    supervisor_id,
    funding,
    fees_funding,
    research_grant_number,
    paid_by_university,
    role_target_viewname as _target_viewname,
    role_id as _role_xid
FROM _latest_role_v12 lr
JOIN _physical_status_v3 ps using (person_id)
WHERE ps.status_id = 'Current' and lr.status = 'Current';
ALTER VIEW hotwire."10_View/Roles/_current_people_supervised_by" OWNER TO dev;
GRANT SELECT ON hotwire."10_View/Roles/_current_people_supervised_by" TO hr;

CREATE VIEW hotwire."10_View/Roles/Bulk_update_supervisors" AS
SELECT 
    person.id,
    person.id as old_supervisor_id,
    null::bigint as new_supervisor_id,
    _to_hwsubviewb('10_View/Roles/_current_people_supervised_by'::varchar,'supervisor_id'::varchar,'_target_viewname'::varchar, '_role_xid'::varchar,null::varchar) as current_people_supervised_by_old_supervisor_subview
FROM person;
ALTER VIEW hotwire."10_View/Roles/Bulk_update_supervisors" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire."10_View/Roles/Bulk_update_supervisors" TO hr;

CREATE FUNCTION hotwire.update_supervisors(hotwire."10_View/Roles/Bulk_update_supervisors") returns bigint AS
$$
declare 
    v alias for $1;
begin
    update post_history set supervisor_id = v.new_supervisor_id where post_history.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'post_history' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update visitorship set host_person_id = v.new_supervisor_id where visitorship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'visitorship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update postgraduate_studentship set first_supervisor_id = v.new_supervisor_id where postgraduate_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'postgraduate_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update erasmus_socrates_studentship set supervisor_id = v.new_supervisor_id where erasmus_socrates_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'erasmus_socrates_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update part_iii_studentship set supervisor_id = v.new_supervisor_id where part_iii_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'part_iii_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    return v.old_supervisor_id;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire.update_supervisors(hotwire."10_View/Roles/Bulk_update_supervisors") OWNER TO dev;

CREATE RULE update_supervisors AS ON UPDATE TO hotwire."10_View/Roles/Bulk_update_supervisors" DO INSTEAD SELECT hotwire.update_supervisors(new.*) as id;
