CREATE OR REPLACE FUNCTION person_update_image_lo()
  RETURNS trigger AS
$BODY$
BEGIN
        NEW.image_lo := NEW.image_oid::oid;
RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION person_update_image_lo()
  OWNER TO dev;
