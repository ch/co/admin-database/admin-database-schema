-- This extension comes in the postgresql-version-contrib package
CREATE EXTENSION unaccent;
update hotwire3."hw_User Preferences" set preference_value = 'TRUE' where preference_id = ( select id from hotwire3."hw_Preferences" where hw_preference_const = 'UNACCENT')
;

