CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Phone" AS 
 SELECT a.id, a.ro_person, a.image_oid, a.surname, a.first_names, a.dept_telephone_number_id, a.external_work_numbers, a.room_id, a.ro_supervisor_id, a.email_address, a.location, a.ro_post_category_id, a.ro_physical_status_id, a.is_external, a._cssclass, a._title_hid, a._known_as
   FROM ( SELECT person.id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS ro_person, person.image_oid::oid AS image_oid, person.surname, person.first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.external_work_phone_numbers AS external_work_numbers, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, person.email_address, person.location, _latest_role.post_category_id AS ro_post_category_id, _physical_status.status_id AS ro_physical_status_id, person.is_external, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN person.is_external = true THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass, title_hid.title_hid AS _title_hid, person.known_as AS _known_as
           FROM person
      LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id) a
  ORDER BY a.surname, a._title_hid, a._known_as;

ALTER TABLE hotwire."10_View/People/Personnel_Phone" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Phone" TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Phone" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO accounts;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO reception;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO phones;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Phone" TO old_cos;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO student_management_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO building_security;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO interviewtest;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO chematcam;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO phonebook_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO reception_cover;

CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_History" AS 
 WITH personnel_history AS (
         SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text AS ro_length_of_continuous_service, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text AS ro_length_of_service_in_current_contract, age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone) AS ro_final_length_of_continuous_service, age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone) AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, person.other_information::character varying(5000) AS other_information, person.notes::character varying(5000) AS notes, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass, _to_hwsubviewb('10_View/People/Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Review_History'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, _to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
        )
 SELECT personnel_history.id, personnel_history.ro_person, personnel_history.surname, personnel_history.first_names, personnel_history.title_id, personnel_history.ro_age, personnel_history.ro_post_category_id, personnel_history.continuous_employment_start_date, personnel_history.ro_latest_employment_start_date, personnel_history.ro_employment_end_date, personnel_history.ro_length_of_continuous_service, personnel_history.ro_length_of_service_in_current_contract, personnel_history.ro_final_length_of_continuous_service, personnel_history.ro_final_length_of_service_in_last_contract, personnel_history.ro_supervisor_id, personnel_history.other_information, personnel_history.notes, personnel_history._cssclass, personnel_history.staff_review_history_subview, personnel_history.cambridge_history_subview
   FROM personnel_history
  ORDER BY personnel_history.surname, personnel_history.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_History"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO accounts;

CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Accounts" AS 
 SELECT a.id, a.ro_person, a._image_oid, a.ro_surname, a.ro_first_names, a.dept_telephone_number_id, a.room_id, a.ro_crsid, a.ro_email_address, a.ni_number, a.payroll_personal_reference_number, a.ro_post_category_id, a.ro_emplid_number, a.ro_gaf_number, a.ro_date_registered_for_phd, a.ro_cpgs_or_mphil_date_submission_due, a.ro_mphil_date_submission_due, a.ro_date_phd_submission_due, a.ro_phd_four_year_submission_date, a._hl_status
   FROM ( SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname AS ro_surname, person.first_names AS ro_first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, person.crsid AS ro_crsid, person.email_address AS ro_email_address, person.ni_number, person.payroll_personal_reference_number, _latest_role.post_category_id AS ro_post_category_id, postgraduate_studentship.emplid_number AS ro_emplid_number, postgraduate_studentship.gaf_number AS ro_gaf_number, postgraduate_studentship.date_registered_for_phd AS ro_date_registered_for_phd, postgraduate_studentship.cpgs_or_mphil_date_submission_due AS ro_cpgs_or_mphil_date_submission_due, postgraduate_studentship.mphil_date_submission_due AS ro_mphil_date_submission_due, postgraduate_studentship.date_phd_submission_due AS ro_date_phd_submission_due, postgraduate_studentship.end_of_registration_date AS ro_phd_four_year_submission_date, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _hl_status
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
   LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN postgraduate_studentship ON postgraduate_studentship.person_id = person.id) a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Accounts"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Accounts" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO accounts;

CREATE OR REPLACE VIEW hotwire."10_View/People/All_Contact_Details" AS 
 WITH contact_details AS (
         SELECT person.id, ROW('image/jpeg'::character varying, person.image_lo::bigint)::blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, person.crsid, person.email_address, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.external_work_phone_numbers, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.emergency_contact, person.location, person.forwarding_address, person.new_employer_address, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE person.id = mm_person_nationality.person_id) AS nationality_id, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
        )
 SELECT contact_details.id, contact_details.id AS ro_person_id, contact_details.image_oid, contact_details.surname, contact_details.first_names, contact_details.title_id, contact_details.known_as, contact_details.name_suffix, contact_details.maiden_name, contact_details.gender_id, contact_details.ro_post_category_id, contact_details.crsid, contact_details.email_address, contact_details.arrival_date, contact_details.leaving_date, contact_details.ro_physical_status_id, contact_details.left_but_no_leaving_date_given, contact_details.dept_telephone_number_id, contact_details.room_id, contact_details.research_group_id, contact_details.external_work_phone_numbers, contact_details.cambridge_address AS home_address, contact_details.cambridge_phone_number AS home_phone_number, contact_details.cambridge_college_id, contact_details.mobile_number, contact_details.emergency_contact, contact_details.location, contact_details.forwarding_address, contact_details.new_employer_address, contact_details.nationality_id, contact_details._cssclass
   FROM contact_details
  ORDER BY contact_details.surname, contact_details.first_names;

ALTER TABLE hotwire."10_View/People/All_Contact_Details"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/All_Contact_Details" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/All_Contact_Details" TO reception;

