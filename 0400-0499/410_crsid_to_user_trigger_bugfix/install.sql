CREATE OR REPLACE FUNCTION crsid_to_user() RETURNS trigger AS
$BODY$
declare
    physical_status varchar(20);
begin
 select fn_calculate_physical_status(new.arrival_date,new.leaving_date,new.left_but_no_leaving_date_given) into physical_status;
if new.crsid is not null and physical_status <> 'Past' then
 perform fn_createuser(new.crsid);
end if;
return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION crsid_to_user() OWNER TO dev;
