CREATE OR REPLACE FUNCTION crsid_to_user() RETURNS trigger AS
$BODY$
begin
if new.crsid is not null then
 perform fn_createuser(new.crsid);
end if;
return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION crsid_to_user() OWNER TO dev;
