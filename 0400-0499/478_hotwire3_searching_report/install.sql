CREATE VIEW hotwire3."30_Report/People_With_Latest_Role" AS
SELECT
  person.id,
  -- identifying
  person.image_lo as image,
  person.surname,
  person.first_names,
  person.known_as,
  person.previous_surname,
  person.title_id,
  person.name_suffix,
  person.gender_id,
  person.date_of_birth,
  array(select nationality_id from mm_person_nationality mm where person.id = mm.person_id) as nationality_id,
  -- presence
  person.arrival_date,
  person.leaving_date,
  person.left_but_no_leaving_date_given,
  _physical_status.status_id as presence,
  -- contact details
  array(select room_id from mm_person_room mm where person.id = mm.person_id) as room_id,
  person.forwarding_address::text,
  array(select dept_telephone_number_id from mm_person_dept_telephone_number mm where person.id = mm.person_id) as dept_telephone_number_id,
  person.email_address,
  person.crsid,
  person.mobile_number,
  person.emergency_contact::text,
  person.new_employer_address::text,
  person.paper_file_details,
  person.cambridge_college_id,
  person.cambridge_address::text as home_address,
  person.cambridge_phone_number as home_phone_number,
  person.external_work_phone_numbers,
  person.location,
  person.is_external,
  person.chem_at_cam,
  person.notes::text,
  person.other_information::text,
  -- role
  _latest_role.supervisor_id,
  _latest_role.co_supervisor_id,
  array(select research_group_id from mm_person_research_group mm where person.id = mm.person_id) as research_group_id,
  _latest_role.post_category,
  -- role dates
  person.continuous_employment_start_date,
  _latest_role.start_date as role_start_date,
  _latest_role.intended_end_date as role_intended_end_date,
  _latest_role.estimated_leaving_date as role_estimated_leaving_date,
  _latest_role.funding_end_date,
  _latest_role.end_date as role_end_date,
  _latest_role.status as role_status,
  -- funding
  _latest_role.funding::text,
  _latest_role.fees_funding::text,
  _latest_role.research_grant_number::text,
  _latest_role.paid_by_university,
  _latest_role.chem
FROM person
LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id
JOIN _physical_status_v3 _physical_status on person.id = _physical_status.person_id
WHERE person.is_spri <> 't' or person.is_spri is null
ORDER BY surname, known_as, first_names
;

ALTER VIEW hotwire3."30_Report/People_With_Latest_Role" OWNER TO dev;

GRANT SELECT ON hotwire3."30_Report/People_With_Latest_Role" TO hr,mgmt_ro;

CREATE VIEW hotwire3."30_Report/People_With_All_Roles" AS
WITH people_all_roles AS (
SELECT
  person.id::varchar || '-' ||  _role.role_id::varchar || '-' ||  _role.role_tablename as id,
  -- identifying
  person.image_lo as image,
  person.surname,
  person.first_names,
  person.known_as,
  person.previous_surname,
  person.title_id,
  person.name_suffix,
  person.gender_id,
  person.date_of_birth,
  array(select nationality_id from mm_person_nationality mm where person.id = mm.person_id) as nationality_id,
  -- presence
  person.arrival_date,
  person.leaving_date,
  person.left_but_no_leaving_date_given,
  _physical_status.status_id as presence,
  -- contact details
  array(select room_id from mm_person_room mm where person.id = mm.person_id) as room_id,
  person.forwarding_address::text,
  array(select dept_telephone_number_id from mm_person_dept_telephone_number mm where person.id = mm.person_id) as dept_telephone_number_id,
  person.email_address,
  person.crsid,
  person.mobile_number,
  person.emergency_contact::text,
  person.new_employer_address::text,
  person.paper_file_details,
  person.cambridge_college_id,
  person.cambridge_address::text as home_address,
  person.cambridge_phone_number as home_phone_number,
  person.external_work_phone_numbers,
  person.location,
  person.is_external,
  person.chem_at_cam,
  person.notes::text,
  person.other_information::text,
  -- role
  _role.supervisor_id,
  _role.co_supervisor_id,
  array(select research_group_id from mm_person_research_group mm where person.id = mm.person_id) as research_group_id,
  _role.post_category,
  -- role dates
  person.continuous_employment_start_date,
  _role.start_date as role_start_date,
  _role.intended_end_date as role_intended_end_date,
  _role.estimated_leaving_date as role_estimated_leaving_date,
  _role.funding_end_date,
  _role.end_date as role_end_date,
  _role.status as role_status,
  -- funding
  _role.funding::text,
  _role.fees_funding::text,
  _role.research_grant_number::text,
  _role.paid_by_university,
  _role.chem
FROM person
LEFT JOIN _all_roles_v13 _role ON _role.person_id = person.id
JOIN _physical_status_v3 _physical_status on person.id = _physical_status.person_id
WHERE person.is_spri <> 't' or person.is_spri is null
) SELECT
  id,
  -- identifying
  image,
  surname,
  first_names,
  known_as,
  previous_surname,
  title_id,
  name_suffix,
  gender_id,
  date_of_birth,
  nationality_id,
  -- presence
  arrival_date,
  leaving_date,
  left_but_no_leaving_date_given,
  presence,
  -- contact details
  room_id,
  forwarding_address,
  dept_telephone_number_id,
  email_address,
  crsid,
  mobile_number,
  emergency_contact,
  new_employer_address,
  paper_file_details,
  cambridge_college_id,
  home_address,
  home_phone_number,
  external_work_phone_numbers,
  location,
  is_external,
  chem_at_cam,
  notes,
  other_information,
  -- role
  supervisor_id,
  co_supervisor_id,
  research_group_id,
  post_category,
  -- role dates
  continuous_employment_start_date,
  role_start_date,
  role_intended_end_date,
  role_estimated_leaving_date,
  funding_end_date,
  role_end_date,
  role_status,
  -- funding
  funding,
  fees_funding,
  research_grant_number,
  paid_by_university,
  chem
FROM people_all_roles
ORDER BY surname, known_as, first_names, role_start_date
;

ALTER VIEW hotwire3."30_Report/People_With_All_Roles" OWNER TO dev;

GRANT SELECT ON hotwire3."30_Report/People_With_All_Roles" TO hr,mgmt_ro;
