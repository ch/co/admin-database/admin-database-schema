REVOKE SELECT,UPDATE ON person, post_history, visitorship, erasmus_socrates_studentship, postgraduate_studentship, part_iii_studentship FROM gdpr_purge;

REVOKE SELECT ON _physical_status_changelog, _physical_status_v2 FROM gdpr_purge;
DROP ROLE gdpr_purge ;
