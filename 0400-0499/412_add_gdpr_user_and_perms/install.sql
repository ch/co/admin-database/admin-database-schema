CREATE ROLE gdpr_purge LOGIN;
GRANT SELECT,UPDATE ON person, post_history, visitorship, erasmus_socrates_studentship, postgraduate_studentship, part_iii_studentship to gdpr_purge;

GRANT SELECT ON _physical_status_changelog, _physical_status_v2 TO gdpr_purge;
