DROP VIEW hotwire3."10_View/Groups/Research_Groups_Computing";

CREATE VIEW hotwire3."10_View/Groups/Research_Groups_Computing" AS 
SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.deputy_head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    research_group.website_address,
    research_group.internal_use_only AS hide_from_websites,
    research_group.ignore_for_software_compliance_purposes,
    research_group.group_fileserver_id,
    research_group.active_directory_container,
    research_group.fai_class_id,
    array_to_string(
        ARRAY( SELECT person_hid.person_hid
           FROM mm_person_research_group
           JOIN _physical_status_v3 member_status USING (person_id)
           JOIN hotwire3.person_hid USING (person_id)
          WHERE 
              mm_person_research_group.research_group_id = research_group.id
          AND
              member_status.status_id = 'Current'
        ),
        E';\n'
    )::text AS ro_list_of_members_who_are_current, 
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id
    ) AS member_id, 
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id
    ) AS computer_rep_id,
    CASE
      WHEN pi_status.status_id = 'Current' THEN 'Active'::varchar
      WHEN has_current_members.has_current_members = 't' THEN 'Active'::varchar
      ELSE 'Inactive'::varchar
    END AS ro_group_status,
    CASE
      WHEN pi_status.status_id = 'Current' THEN NULL::text
      WHEN has_current_members.has_current_members = 't' THEN NULL::text
      ELSE 'orange'::text
    END AS _cssclass
FROM research_group
JOIN _physical_status_v3 pi_status on research_group.head_of_group_id = pi_status.person_id
JOIN (
    SELECT
        id,
        'Current'::varchar(20) = ANY (
            ARRAY( 
                SELECT _physical_status.status_id 
                FROM mm_person_research_group 
                JOIN _physical_status_v3 _physical_status USING (person_id) 
                WHERE mm_person_research_group.research_group_id = research_group.id 
            )
        ) AS has_current_members 
    FROM research_group
) has_current_members using (id)
ORDER BY research_group.name;

ALTER TABLE hotwire3."10_View/Groups/Research_Groups_Computing" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Groups/Research_Groups_Computing" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups_Computing" TO cos;

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_del AS ON DELETE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD  DELETE FROM research_group WHERE research_group.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD ( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, deputy_head_of_group_id = new.deputy_head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, internal_use_only = new.hide_from_websites, ignore_for_software_compliance_purposes = new.ignore_for_software_compliance_purposes, group_fileserver_id = new.group_fileserver_id, active_directory_container = new.active_directory_container, fai_class_id = new.fai_class_id
  WHERE research_group.id = old.id;
 SELECT fn_mm_array_update(new.member_id, old.member_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.computer_rep_id, old.computer_rep_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) AS fn_mm_array_update;
);

CREATE OR REPLACE FUNCTION hotwire3.groups_research_groups_computing_ins()
  RETURNS trigger AS
$BODY$
begin
    insert into research_group ( name, head_of_group_id, deputy_head_of_group_id, administrator_id, website_address, internal_use_only, ignore_for_software_compliance_purposes, group_fileserver_id, active_directory_container, fai_class_id ) VALUES ( new.name, new.head_of_group_id, new.deputy_head_of_group_id, new.alternate_admin_contact_id, new.website_address, coalesce(new.hide_from_websites,'f'), coalesce(new.ignore_for_software_compliance_purposes,'f'), new.group_fileserver_id, new.active_directory_container, new.fai_class_id ) returning id into new.id;
    perform fn_mm_array_update(new.member_id, array[]::bigint[], 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, new.id) AS fn_mm_array_update;
    perform fn_mm_array_update(new.computer_rep_id, array[]::bigint[], 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, new.id) AS fn_mm_array_update;
    return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.groups_research_groups_computing_ins() OWNER TO dev;

CREATE TRIGGER hotwire3_groups_research_groups_ins INSTEAD OF INSERT ON hotwire3."10_View/Groups/Research_Groups_Computing" FOR EACH ROW EXECUTE PROCEDURE hotwire3.groups_research_groups_computing_ins();


DROP RULE hotwire_view_research_groups_computing_upd ON hotwire."10_View/Research_Groups_Computing";
DROP RULE hotwire_view_research_groups_computing_ins ON hotwire."10_View/Research_Groups_Computing";
DROP FUNCTION hw_fn_research_groups_computing_upd(hotwire."10_View/Research_Groups_Computing");
DROP VIEW hotwire."10_View/Research_Groups_Computing";

CREATE OR REPLACE VIEW hotwire."10_View/Research_Groups_Computing" AS 
SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    array_to_string(
        ARRAY( SELECT person_hid
           FROM mm_person_research_group
           JOIN _physical_status_v3 member_status USING (person_id)
           JOIN hotwire.person_hid USING (person_id)
           WHERE 
              mm_person_research_group.research_group_id = research_group.id
           AND
              member_status.status_id = 'Current'
        ),
        E';\n'
    ) AS ro_list_of_members_who_are_current, 
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id
    ) AS member_id, 
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id
    ) AS computer_rep_id, 
    research_group.website_address,
    research_group.sector_id,
    research_group.fai_class_id,
    research_group.active_directory_container,
    research_group.internal_use_only,
    research_group.ignore_for_software_compliance_purposes,
    research_group.subnet_id,
    research_group.group_fileserver_id,
    CASE
      WHEN pi_status.status_id = 'Current' THEN 'Active'::varchar
      WHEN has_current_members.has_current_members = 't' THEN 'Active'::varchar
      ELSE 'Inactive'::varchar
    END AS ro_group_status,
    CASE
      WHEN pi_status.status_id = 'Current' THEN NULL::text
      WHEN has_current_members.has_current_members = 't' THEN NULL::text
      ELSE 'orange'::text
    END AS _cssclass
FROM research_group
JOIN _physical_status_v3 pi_status on research_group.head_of_group_id = pi_status.person_id
JOIN (
    SELECT
        id,
        'Current'::varchar(20) = ANY (
            ARRAY( 
                SELECT _physical_status.status_id 
                FROM mm_person_research_group 
                JOIN _physical_status_v3 _physical_status USING (person_id) 
                WHERE mm_person_research_group.research_group_id = research_group.id 
            )
        ) AS has_current_members 
    FROM research_group
) has_current_members using (id)
ORDER BY research_group.name;

ALTER TABLE hotwire."10_View/Research_Groups_Computing" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Research_Groups_Computing" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Groups_Computing" TO cos;

CREATE OR REPLACE FUNCTION hw_fn_research_groups_computing_upd(hotwire."10_View/Research_Groups_Computing")
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin
 IF r.id is not null then
   update research_group set name=r.name, head_of_group_id=r.head_of_group_id, administrator_id=r.alternate_admin_contact_id,  
          website_address=r.website_address, sector_id=r.sector_id, fai_class_id=r.fai_class_id,
          active_directory_container=r.active_directory_container, internal_use_only=r.internal_use_only, ignore_for_software_compliance_purposes=r.ignore_for_software_compliance_purposes,
          subnet_id=r.subnet_id, group_fileserver_id=r.group_fileserver_id where id=r.id;
   r_id=r.id;
 else
   insert into research_group (name, head_of_group_id, administrator_id, website_address, sector_id, fai_class_id, active_directory_container, internal_use_only, ignore_for_software_compliance_purposes, subnet_id, group_fileserver_id ) values 
               (r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id, r.fai_class_id, r.active_directory_container, coalesce(r.internal_use_only,'f'), coalesce(r.ignore_for_software_compliance_purposes,'f'), r.subnet_id, r.group_fileserver_id ) returning id into r_id;
 end if;

perform fn_mm_array_update(r.member_id,
                           'mm_person_research_group'::varchar,
                           'research_group_id'::varchar,
                           'person_id'::varchar,
                           r_id);
perform fn_mm_array_update(r.computer_rep_id,
                           'mm_research_group_computer_rep'::varchar,
                           'research_group_id'::varchar,
                           'computer_rep_id'::varchar,
                           r_id);
return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_research_groups_computing_upd(hotwire."10_View/Research_Groups_Computing")
  OWNER TO dev;


CREATE RULE hotwire_view_research_groups_computing_del AS
    ON DELETE TO hotwire."10_View/Research_Groups_Computing" DO INSTEAD  DELETE FROM research_group
  WHERE research_group.id = old.id;

CREATE RULE hotwire_view_research_groups_computing_ins AS
    ON INSERT TO hotwire."10_View/Research_Groups_Computing" DO INSTEAD  SELECT hw_fn_research_groups_computing_upd(new.*) AS id;

CREATE RULE hotwire_view_research_groups_computing_upd AS
    ON UPDATE TO hotwire."10_View/Research_Groups_Computing" DO INSTEAD  SELECT hw_fn_research_groups_computing_upd(new.*) AS id;
