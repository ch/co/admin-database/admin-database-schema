-- View: hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"

-- DROP VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" AS 
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id, ''::character varying(63) AS hostname, NULL::integer AS subnet_id, NULL::integer AS hardware_id, NULL::macaddr AS wired_mac_1, ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text ~* '%debian%'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id, ''::character varying AS system_image_comments, NULL::integer AS host_system_image_id, NULL::macaddr AS wired_mac_2, ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id, ( SELECT mm_person_research_group.research_group_id
           FROM person
      JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     WHERE person.crsid::name = "current_user"()
    LIMIT 1) AS research_group_id;

