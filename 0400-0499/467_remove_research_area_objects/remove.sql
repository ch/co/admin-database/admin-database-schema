--
-- Name: mm_research_area_research_group; Type: TABLE; Schema: public; Owner: cen1001
--

CREATE TABLE public.mm_research_area_research_group (
    research_area_id bigint NOT NULL,
    research_group_id bigint NOT NULL
);


ALTER TABLE public.mm_research_area_research_group OWNER TO cen1001;

--
-- Name: research_area; Type: TABLE; Schema: public; Owner: cen1001
--

CREATE TABLE public.research_area (
    id bigint NOT NULL,
    name character varying(80) NOT NULL,
    shortname character varying(10),
    description character varying(500)
);


ALTER TABLE public.research_area OWNER TO cen1001;

--
-- Name: research_area_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.research_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.research_area_id_seq OWNER TO cen1001;

--
-- Name: research_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cen1001
--

ALTER SEQUENCE public.research_area_id_seq OWNED BY public.research_area.id;


--
-- Name: research_area id; Type: DEFAULT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.research_area ALTER COLUMN id SET DEFAULT nextval('public.research_area_id_seq'::regclass);


--
-- Data for Name: mm_research_area_research_group; Type: TABLE DATA; Schema: public; Owner: cen1001
--

COPY public.mm_research_area_research_group (research_area_id, research_group_id) FROM stdin;
7	4
7	10
7	7
7	52
7	5
7	26
7	56
7	94
7	34
7	67
7	38
7	6
7	46
7	8
7	3
1	51
1	11
\.


--
-- Data for Name: research_area; Type: TABLE DATA; Schema: public; Owner: cen1001
--

COPY public.research_area (id, name, shortname, description) FROM stdin;
2	Chemical biology	\N	\N
3	Materials, Polymers and Nanoscience	\N	\N
4	Structural Chemistry and Spectroscopy	\N	\N
5	Surfaces, Interfaces and Heterogeneous Catalysis	\N	\N
6	Synthetic and Mechanistic Chemistry	\N	\N
7	Theoretical and Computational Chemistry and Informatics	\N	\N
1	Atmospheric and Gas Phase Chemistry	\N	\N
\.


--
-- Name: research_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.research_area_id_seq', 7, true);


--
-- Name: mm_research_area_research_group mm_research_area_research_group_pk; Type: CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.mm_research_area_research_group
    ADD CONSTRAINT mm_research_area_research_group_pk PRIMARY KEY (research_area_id, research_group_id);


--
-- Name: research_area research_area_name_key; Type: CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.research_area
    ADD CONSTRAINT research_area_name_key UNIQUE (name);


--
-- Name: research_area research_area_pkey; Type: CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.research_area
    ADD CONSTRAINT research_area_pkey PRIMARY KEY (id);


--
-- Name: research_area research_area_shortname_key; Type: CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.research_area
    ADD CONSTRAINT research_area_shortname_key UNIQUE (shortname);


--
-- Name: mm_research_area_research_group mm_research_area_research_group_fk_research_area; Type: FK CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.mm_research_area_research_group
    ADD CONSTRAINT mm_research_area_research_group_fk_research_area FOREIGN KEY (research_area_id) REFERENCES public.research_area(id) ON DELETE CASCADE;


--
-- Name: mm_research_area_research_group mm_research_area_research_group_fk_research_group; Type: FK CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.mm_research_area_research_group
    ADD CONSTRAINT mm_research_area_research_group_fk_research_group FOREIGN KEY (research_group_id) REFERENCES public.research_group(id) ON DELETE CASCADE;


CREATE OR REPLACE VIEW research_areas_editing_view AS 
 SELECT research_area.id, research_area.name, research_area.shortname, research_area.description, research_group_hid.research_group_hid AS mm_research_group, mm_research_area_research_group.research_group_id AS mm_research_group_id
   FROM research_area
   LEFT JOIN mm_research_area_research_group ON research_area.id = mm_research_area_research_group.research_area_id
   LEFT JOIN research_group_hid USING (research_group_id);

ALTER TABLE research_areas_editing_view
  OWNER TO cen1001;
GRANT ALL ON TABLE research_areas_editing_view TO cen1001;
GRANT ALL ON TABLE research_areas_editing_view TO dev;
GRANT SELECT ON TABLE research_areas_editing_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE research_areas_editing_view TO web_editors;

CREATE OR REPLACE RULE research_areas_editing_del AS
    ON DELETE TO research_areas_editing_view DO INSTEAD  DELETE FROM research_area
  WHERE research_area.id = old.id;

CREATE OR REPLACE RULE research_areas_editing_upd AS
    ON UPDATE TO research_areas_editing_view DO INSTEAD ( UPDATE research_area SET name = new.name, shortname = new.shortname, description = new.description
  WHERE research_area.id = old.id;
 SELECT fn_upd_many_many(old.id, new.mm_research_group::text, 'research_area'::text, 'research_group'::text) AS fn_upd_many_many;
);

CREATE OR REPLACE FUNCTION research_areas_ins(research_areas_editing_view)
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin

r_id = nextval('research_area_id_seq');

insert into research_area (id, name, shortname, description ) values (r_id, r.name, r.shortname, r.description );

perform fn_upd_many_many (r_id, r.mm_research_group, 'research_area','research_group') ;

return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION research_areas_ins(research_areas_editing_view)
  OWNER TO cen1001;


CREATE OR REPLACE RULE research_areas_editing_ins AS
    ON INSERT TO research_areas_editing_view DO INSTEAD  SELECT research_areas_ins(new.*) AS id;

CREATE OR REPLACE VIEW hotwire."10_View/Research_Areas_Editing" AS 
 SELECT research_area.id, research_area.name, research_area.shortname, research_area.description, ARRAY( SELECT mm_research_area_research_group.research_group_id
           FROM mm_research_area_research_group
          WHERE mm_research_area_research_group.research_area_id = research_area.id) AS research_group_id
   FROM research_area;

ALTER TABLE hotwire."10_View/Research_Areas_Editing"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Research_Areas_Editing" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Research_Areas_Editing" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Research_Areas_Editing" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Areas_Editing" TO web_editors;


CREATE OR REPLACE RULE hotwire_view_research_areas_editing_del AS
    ON DELETE TO hotwire."10_View/Research_Areas_Editing" DO INSTEAD  DELETE FROM research_area
  WHERE research_area.id = old.id;

CREATE OR REPLACE FUNCTION hw_fn_research_areas_ins(hotwire."10_View/Research_Areas_Editing")
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin


insert into research_area (id, name, shortname, description ) values (r_id, r.name, r.shortname, r.description ) returning id into r_id;


  select fn_mm_array_update(r.research_group_id,
                            'mm_research_area_research_group'::varchar,
                            'research_area_id'::varchar,
                            'research_group_id'::varchar,
                            r_id);
return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_research_areas_ins(hotwire."10_View/Research_Areas_Editing")
  OWNER TO cen1001;

CREATE OR REPLACE RULE hotwire_view_research_areas_editing_ins AS
    ON INSERT TO hotwire."10_View/Research_Areas_Editing" DO INSTEAD  SELECT hw_fn_research_areas_ins(new.*) AS id;

CREATE OR REPLACE RULE hotwire_view_research_areas_editing_upd AS
    ON UPDATE TO hotwire."10_View/Research_Areas_Editing" DO INSTEAD ( UPDATE research_area SET name = new.name, shortname = new.shortname, description = new.description
  WHERE research_area.id = old.id;
 SELECT fn_mm_array_update(new.research_group_id, old.research_group_id, 'mm_research_area_research_group'::character varying, 'research_area_id'::character varying, 'research_group_id'::character varying, old.id) AS fn_mm_array_update;
);


