DROP RULE research_areas_editing_ins ON research_areas_editing_view;
DROP FUNCTION research_areas_ins(research_areas_editing_view);
DROP VIEW research_areas_editing_view;

DROP RULE hotwire_view_research_areas_editing_ins ON hotwire."10_View/Research_Areas_Editing";
DROP RULE hotwire_view_research_areas_editing_upd ON hotwire."10_View/Research_Areas_Editing";
DROP FUNCTION hw_fn_research_areas_ins(hotwire."10_View/Research_Areas_Editing");
DROP VIEW hotwire."10_View/Research_Areas_Editing";

DROP TABLE mm_research_area_research_group;
DROP TABLE research_area;
