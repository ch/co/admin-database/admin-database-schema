CREATE OR REPLACE VIEW _physical_status AS 
 SELECT person.id, 
        CASE
            WHEN person.leaving_date < 'now'::text::date THEN 'Past'::character varying(20)
            WHEN person.leaving_date IS NULL AND person.left_but_no_leaving_date_given = true THEN 'Past'::character varying(20)
            WHEN person.arrival_date > person.leaving_date THEN 'Inconsistent'::character varying(20)
            WHEN person.arrival_date <= 'now'::text::date AND (person.leaving_date >= 'now'::text::date OR person.leaving_date IS NULL) THEN 'Current'::character varying(20)
            WHEN person.arrival_date > 'now'::text::date THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS physical_status, person._status
   FROM person;

ALTER TABLE _physical_status
  OWNER TO dev;
GRANT ALL ON TABLE _physical_status TO dev;
GRANT ALL ON TABLE _physical_status TO cen1001;

