DROP VIEW hotwire."10_View/Research_Groups/Software_Licence_Declaration";
CREATE VIEW hotwire."10_View/Research_Groups/Software_Licence_Declaration" AS 
with q as (
SELECT 
  research_group.id, 
  research_group.name as ro_name, 
  research_group.head_of_group_id as ro_head_of_group_id, 
  $$I have instructed my group that that all software used by members of the group must be properly licensed and the group members have certified to me that that the software they use is properly licensed$$::varchar(500) as ro_software_policy_declaration,
  CASE WHEN latest_software_declaration.declaration_date IS NULL THEN false WHEN (
    latest_software_declaration.declaration_date + '1 year' :: interval
  ) > 'now' :: text :: date THEN true ELSE false END AS declaration_made, 
  latest_software_declaration.declaration_date AS ro_date_of_last_declaration, 
  latest_software_declaration.username_of_signer AS ro_declared_by 
FROM 
  research_group 
LEFT JOIN (
  SELECT 
    group_software_licence_declarations.research_group_id, 
    max(
        group_software_licence_declarations.declaration_date
    ) AS declaration_date 
  FROM group_software_licence_declarations 
  GROUP BY group_software_licence_declarations.research_group_id
  ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id 
LEFT JOIN group_software_licence_declarations latest_software_declaration ON 
    latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date 
  AND 
    latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id 
JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id 
JOIN _physical_status_v3 ps ON head_of_group.id = ps.person_id
LEFT JOIN (
  select 
    head_of_group_id,
    count(person_id) as members 
  from ( select supervisor_id as head_of_group_id, person_id from cache._latest_role join _physical_status_v3 ps using(person_id) where ps.status_id = 'Current' 
         union 
         select co_supervisor_id as head_of_group_id, person_id from cache._latest_role join _physical_status_v3 ps using(person_id) where ps.status_id = 'Current') members 
  group by head_of_group_id
) group_members ON group_members.head_of_group_id = head_of_group.id
WHERE 
    -- show PIs their own groups only
    "current_user"() = head_of_group.crsid :: name 
  OR (
  -- show COs all the PIs who are still here or who have supervisees still here
    (pg_has_role(
    "current_user"(), 
    'cos' :: name, 
    'member' :: text)
    ) AND (
      ps.status_id = 'Current'
      or
      group_members.members > 0
    )
  ) 
) select * from q
ORDER BY ro_name;
ALTER TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" TO headsofgroup;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" TO cos;

CREATE OR REPLACE FUNCTION group_software_licence_decl_trig()
  RETURNS trigger AS
$BODY$
    BEGIN
        IF (NEW.declaration_made = 't') 
            AND
           ((current_date <> OLD.ro_date_of_last_declaration) OR OLD.ro_date_of_last_declaration IS NULL)
        THEN
            INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( NEW.id, current_date, current_user );
        END IF;
        RETURN NEW;
    END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION group_software_licence_decl_trig()
  OWNER TO dev;


CREATE TRIGGER hw_rg_software_licence_decl_trigger INSTEAD OF UPDATE ON hotwire."10_View/Research_Groups/Software_Licence_Declaration" FOR EACH ROW EXECUTE PROCEDURE group_software_licence_decl_trig();

DROP VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration";
CREATE VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration" AS 
with q as (
SELECT 
  research_group.id, 
  research_group.name as ro_name, 
  research_group.head_of_group_id as ro_head_of_group_id, 
  $$I have instructed my group that that all software used by members of the group must be properly licensed and the group members have certified to me that that the software they use is properly licensed$$::text as ro_software_policy_declaration,
  CASE WHEN latest_software_declaration.declaration_date IS NULL THEN false WHEN (
    latest_software_declaration.declaration_date + '1 year' :: interval
  ) > 'now' :: text :: date THEN true ELSE false END AS declaration_made, 
  latest_software_declaration.declaration_date AS ro_date_of_last_declaration, 
  latest_software_declaration.username_of_signer AS ro_declared_by 
FROM research_group 
LEFT JOIN (
    SELECT 
      group_software_licence_declarations.research_group_id, 
      max(
        group_software_licence_declarations.declaration_date
      ) AS declaration_date 
    FROM 
      group_software_licence_declarations 
    GROUP BY 
      group_software_licence_declarations.research_group_id
  ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id 
LEFT JOIN group_software_licence_declarations latest_software_declaration ON latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date 
  AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id 
JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id 
JOIN _physical_status_v3 ps ON head_of_group.id = ps.person_id
LEFT JOIN (
  select 
    head_of_group_id,
    count(person_id) as members 
  from ( select supervisor_id as head_of_group_id, person_id from cache._latest_role join _physical_status_v3 ps using(person_id) where ps.status_id = 'Current' 
         union 
         select co_supervisor_id as head_of_group_id, person_id from cache._latest_role join _physical_status_v3 ps using(person_id) where ps.status_id = 'Current') members 
  group by head_of_group_id
) group_members ON group_members.head_of_group_id = head_of_group.id
WHERE 
    -- show PIs their own groups only
    "current_user"() = head_of_group.crsid :: name 
  OR (
  -- show COs all the PIs who are still here or who have supervisees still here
    (pg_has_role(
    "current_user"(), 
    'cos' :: name, 
    'member' :: text)
    ) AND (
      ps.status_id = 'Current'
      or
      group_members.members > 0
    )
  )
) select * from q
ORDER BY ro_name;
ALTER TABLE hotwire3."10_View/My_Groups/Software_Licence_Declaration" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/My_Groups/Software_Licence_Declaration" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/My_Groups/Software_Licence_Declaration" TO headsofgroup;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/My_Groups/Software_Licence_Declaration" TO cos;



CREATE OR REPLACE FUNCTION hotwire3.group_software_licence_decl_trig()
  RETURNS trigger AS
$BODY$
    BEGIN
        IF (NEW.declaration_made = 't') 
            AND
           ((current_date <> OLD.ro_date_of_last_declaration) OR OLD.ro_date_of_last_declaration IS NULL)
        THEN
            INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( NEW.id, current_date, current_user );
        END IF;
        RETURN NEW;
    END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.group_software_licence_decl_trig()
  OWNER TO dev;

CREATE TRIGGER hw_rg_software_licence_decl_trigger
  INSTEAD OF UPDATE
  ON hotwire3."10_View/My_Groups/Software_Licence_Declaration"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.group_software_licence_decl_trig();

