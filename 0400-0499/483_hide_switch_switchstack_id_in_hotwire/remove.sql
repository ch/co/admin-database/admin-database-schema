
CREATE OR REPLACE VIEW hotwire."10_View/switch" AS 
 SELECT switch.id, hardware.name, switch.switchstack_id, hardware.id AS _hardware_id, system_image.id AS _system_image_id, hardware.manufacturer, hardware.model, hardware.hardware_type_id, system_image.wired_mac_1, ip_address_hid.ip_address_hid AS mm_ip_address, mm_system_image_ip_address.ip_address_id AS mm_ip_address_id, hardware.serial_number, hardware.asset_tag, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id AS physical_room_id, hardware.owner_id, system_image.research_group_id, hardware.comments, ARRAY( SELECT mm_switch_serves_room.room_id
           FROM mm_switch_serves_room
          WHERE mm_switch_serves_room.switch_id = switch.id) AS room_id
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id
   JOIN system_image ON system_image.hardware_id = hardware.id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address_hid ON ip_address_hid.ip_address_id = mm_system_image_ip_address.ip_address_id;

ALTER TABLE hotwire."10_View/switch"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/switch" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/switch" TO old_cos;
GRANT ALL ON TABLE hotwire."10_View/switch" TO cos;

-- Rule: switch_del ON hotwire."10_View/switch"

-- DROP RULE switch_del ON hotwire."10_View/switch";

CREATE OR REPLACE RULE switch_del AS
    ON DELETE TO hotwire."10_View/switch" DO INSTEAD  DELETE FROM switch
  WHERE switch.id = old.id;

CREATE OR REPLACE FUNCTION hw_fn_switch_upd(hotwire."10_View/switch")
  RETURNS bigint AS
$BODY$
         declare 
                 v alias for $1;
                 v_id bigint;
                 v_system_image_id bigint;
                 v_hardware_id bigint;
                 v_os_id bigint;
         begin
                 -- v_id := nextval('switch_id_seq');
                 v_hardware_id := nextval('hardware_id_seq');
                 insert into _debug ( debug ) values ( v_hardware_id );
                 v_system_image_id := nextval('system_image_id_seq');
                 v_os_id := (select id from operating_system where os = 'None');
                 insert into hardware 
                         ( id, name, manufacturer, model, hardware_type_id,
                         serial_number, asset_tag, date_purchased,
                         date_configured, warranty_end_date,
                         date_decommissioned, warranty_details, room_id,
                         owner_id, comments )
                 values 
                         ( v_hardware_id, v.name, v.manufacturer, v.model,
                         v.hardware_type_id, v.serial_number,
                         v.asset_tag, v.date_purchased, v.date_configured,
                         v.warranty_end_date, v.date_decommissioned,
                         v.warranty_details, v.room_id, v.owner_id,
                         v.comments);
                 insert into system_image
                         ( id, hardware_id, research_group_id, wired_mac_1,
                         operating_system_id)
                 values
                         ( v_system_image_id, v_hardware_id, v.research_group_id, v.wired_mac_1, v_os_id); 
                 insert into _debug ( debug ) values ( v_hardware_id );
                 -- insert into switch
                 --         ( id, hardware_id, switchstack_id)
                 --values
                 --        ( v_id, v_hardware_id, v.switchstack_id );
 
                 perform fn_upd_many_many(v_system_image_id, v.mm_ip_address, 'system_image', 'ip_address');
                 v_id := ( select id from switch where hardware_id = v_hardware_id );
                 return v_id;
         end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_switch_upd(hotwire."10_View/switch")
  OWNER TO cen1001;


CREATE OR REPLACE RULE switch_ins AS
    ON INSERT TO hotwire."10_View/switch" DO INSTEAD  SELECT hw_fn_switch_upd(new.*) AS id;

-- Rule: switch_upd ON hotwire."10_View/switch"

-- DROP RULE switch_upd ON hotwire."10_View/switch";

CREATE OR REPLACE RULE switch_upd AS
    ON UPDATE TO hotwire."10_View/switch" DO INSTEAD ( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.physical_room_id, owner_id = new.owner_id, comments = new.comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1
  WHERE system_image.id = old._system_image_id;
 SELECT fn_upd_many_many(new._system_image_id, new.mm_ip_address, 'system_image'::text, 'ip_address'::text) AS fn_upd_many_many;
 UPDATE switch SET switchstack_id = new.switchstack_id
  WHERE switch.id = old.id;
);


