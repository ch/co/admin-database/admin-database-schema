CREATE OR REPLACE VIEW _physical_status_v2 AS 
 SELECT person.id, person.id AS person_id, 
        CASE
            WHEN person.leaving_date < 'now'::text::date THEN 'Past'::character varying(20)
            WHEN person.leaving_date IS NULL AND person.left_but_no_leaving_date_given = true THEN 'Past'::character varying(20)
            -- We put the Inconsistent case third because we don't care about long-gone people
            -- with bad arrival dates
            WHEN person.arrival_date > person.leaving_date THEN 'Inconsistent'::character varying(20)
            WHEN person.arrival_date <= 'now'::text::date AND (person.leaving_date >= 'now'::text::date OR person.leaving_date IS NULL) THEN 'Current'::character varying(20)
            WHEN person.arrival_date > 'now'::text::date THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS status_id, person._status
   FROM person;

ALTER TABLE _physical_status_v2 OWNER TO dev;
GRANT ALL ON TABLE _physical_status_v2 TO cen1001;
GRANT ALL ON TABLE _physical_status_v2 TO dev;
GRANT SELECT ON TABLE _physical_status_v2 TO web_editors;
COMMENT ON VIEW _physical_status_v2
  IS 'Obsoleted by _physical_status_v3 table';

CREATE OR REPLACE VIEW _physical_status AS 
 SELECT person.id, 
        CASE
            WHEN person.leaving_date < 'now'::text::date THEN 'Past'::character varying(20)
            WHEN person.leaving_date IS NULL AND person.left_but_no_leaving_date_given = true THEN 'Past'::character varying(20)
            -- We put the Inconsistent case third because we don't care about long-gone people
            -- with bad arrival dates
            WHEN person.arrival_date > person.leaving_date THEN 'Inconsistent'::character varying(20)
            WHEN person.arrival_date <= 'now'::text::date AND (person.leaving_date >= 'now'::text::date OR person.leaving_date IS NULL) THEN 'Current'::character varying(20)
            WHEN person.arrival_date > 'now'::text::date THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS physical_status, person._status
   FROM person;

ALTER TABLE _physical_status OWNER TO dev;
GRANT ALL ON TABLE _physical_status TO cen1001;
GRANT ALL ON TABLE _physical_status TO dev;

CREATE OR REPLACE FUNCTION fn_calculate_physical_status(
    arrival_date date,
    leaving_date date,
    left_but_no_leaving_date_given boolean)
  RETURNS character varying AS
$BODY$
declare status varchar(20) = $$Unknown$$;
begin
select
CASE
            WHEN leaving_date < 'now'::text::date THEN 'Past'::character varying(20)
            WHEN leaving_date IS NULL AND left_but_no_leaving_date_given = true THEN 'Past'::character varying(20)
            -- We put the Inconsistent case third because if the end date is in the past we don't care
            -- if the arrival date is wrong
            WHEN arrival_date > leaving_date THEN 'Inconsistent'::character varying(20)
            WHEN arrival_date <= 'now'::text::date AND (leaving_date >= 'now'::text::date OR leaving_date IS NULL) THEN 'Current'::character varying(20)
            WHEN arrival_date > 'now'::text::date THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
end
into status;
return status;
end$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_calculate_physical_status(date, date, boolean) OWNER TO dev;
GRANT EXECUTE ON FUNCTION fn_calculate_physical_status(date, date, boolean) TO cen1001;
GRANT EXECUTE ON FUNCTION fn_calculate_physical_status(date, date, boolean) TO dev;
GRANT EXECUTE ON FUNCTION fn_calculate_physical_status(date, date, boolean) TO public;

