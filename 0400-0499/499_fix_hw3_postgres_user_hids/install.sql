DROP VIEW hotwire3."Postgres_User_hid";

DROP VIEW hotwire3."90_Action/Hotwire/User Preferences";

CREATE VIEW hotwire3.postgres_user_hid AS 
 SELECT pg_roles.oid::bigint AS postgres_user_id, pg_roles.rolname AS postgres_user_hid
   FROM pg_roles;

ALTER TABLE hotwire3.postgres_user_hid OWNER TO dev;

CREATE VIEW hotwire3."90_Action/Hotwire/User Preferences" AS 
 SELECT "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id AS postgres_user_id
    FROM hotwire3."hw_User Preferences";

ALTER TABLE hotwire3."90_Action/Hotwire/User Preferences" OWNER TO dev;

CREATE OR REPLACE RULE hw3_user_preferences_del AS ON DELETE TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  DELETE FROM hotwire3."hw_User Preferences" WHERE "hw_User Preferences".id = old.id;

CREATE OR REPLACE RULE hw3_user_preferences_ins AS
    ON INSERT TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  INSERT INTO hotwire3."hw_User Preferences" (preference_id, preference_value, user_id) 
  VALUES (new.preference_id, new.preference_value, new.postgres_user_id)
  RETURNING "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id;

CREATE OR REPLACE RULE hw3_user_preferences_upd AS
    ON UPDATE TO hotwire3."90_Action/Hotwire/User Preferences" DO INSTEAD  UPDATE hotwire3."hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new.postgres_user_id
  WHERE "hw_User Preferences".id = old.id;
