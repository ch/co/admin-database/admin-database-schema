CREATE OR REPLACE VIEW hotwire."10_View/People/All_Contact_Details" AS 
 WITH contact_details AS (
         SELECT person.id, ROW('image/jpeg'::character varying, person.image_lo::bigint)::blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, person.crsid, person.email_address, person.arrival_date, person.leaving_date, _physical_status_v3.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, person.external_work_phone_numbers, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.emergency_contact, person.location, person.forwarding_address, person.new_employer_address, person.permission_to_give_out_details, ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE person.id = mm_person_nationality.person_id) AS nationality_id, 
                CASE
                    WHEN _physical_status_v3.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN _physical_status_v3.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
        )
 SELECT contact_details.id, contact_details.id AS ro_person_id, contact_details.image_oid, contact_details.surname, contact_details.first_names, contact_details.title_id, contact_details.known_as, contact_details.name_suffix, contact_details.maiden_name, contact_details.gender_id, contact_details.ro_post_category_id, contact_details.crsid, contact_details.email_address, contact_details.arrival_date, contact_details.leaving_date, contact_details.ro_physical_status_id, contact_details.left_but_no_leaving_date_given, contact_details.dept_telephone_number_id, contact_details.room_id, contact_details.research_group_id, contact_details.external_work_phone_numbers, contact_details.cambridge_address as home_address, contact_details.cambridge_phone_number as home_phone_number, contact_details.cambridge_college_id, contact_details.mobile_number, contact_details.emergency_contact, contact_details.location, contact_details.forwarding_address, contact_details.new_employer_address, contact_details.permission_to_give_out_details, contact_details.nationality_id, contact_details._cssclass
   FROM contact_details
  ORDER BY contact_details.surname, contact_details.first_names;
