CREATE VIEW hotwire3."10_View/People/_SPRI_instances" AS
SELECT
    id,
    hardware_id,
    id as system_image_id,
    operating_system_id,
    user_id as _user_id,
    wired_mac_1
FROM system_image
;

ALTER VIEW hotwire3."10_View/People/_SPRI_instances" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/People/_SPRI_instances" TO spri_hr;

CREATE VIEW hotwire3."10_View/People/_SPRI_hardware" AS
SELECT
    id,
    id as hardware_id,
    name,
    manufacturer,
    model,
    owner_id as _owner_id,
    room_id
FROM hardware;

ALTER VIEW hotwire3."10_View/People/_SPRI_hardware" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/People/_SPRI_hardware" TO spri_hr;

CREATE VIEW hotwire3."10_View/People/SPRI_People" AS
SELECT
    id,
    id as person_id,
    surname,
    first_names,
    title_id,
    email_address,
    crsid,
    array(
        select room_id
        from mm_person_room
        where mm_person_room.person_id = person.id
    ) as room_id,
    remaining_computers.remaining AS ro_number_of_computers_associated,
    case
        when pg_has_role('cos','MEMBER') then hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware','_owner_id','10_View/Computers/Hardware','hardware_id','id')
        else
            hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware','_owner_id',null,null,null)
    end AS computers_owned,
    case
        when pg_has_role('cos','MEMBER') then hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances','_user_id','10_View/Computers/System_Instances','system_image_id','id')
        else
            hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances','_user_id',null,null,null) 
        end AS computers_used
FROM person
LEFT JOIN (
    SELECT person_id, count(id) as remaining
    FROM ( 
        SELECT owner_id AS person_id, id FROM hardware
        UNION ALL
        SELECT user_id AS person_id, id FROM system_image
    ) it_objects
    GROUP BY person_id
) remaining_computers ON remaining_computers.person_id = person.id
WHERE person.is_spri = 't';

ALTER VIEW hotwire3."10_View/People/SPRI_People" OWNER TO dev;

GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/People/SPRI_People" TO spri_hr;

CREATE RULE spri_people_del AS ON DELETE TO hotwire3."10_View/People/SPRI_People" DO INSTEAD DELETE FROM person WHERE id = OLD.id;

CREATE FUNCTION hotwire3.spri_people_trig() RETURNS TRIGGER AS
$$
begin
    if new.id is null
    then
        new.id := nextval('person_id_seq');
        insert into person ( id, first_names, surname, title_id, email_address, crsid, is_spri ) values ( new.id, new.first_names, new.surname, new.title_id, new.email_address, new.crsid, 't');
        perform fn_mm_array_update(new.room_id,array[]::bigint[],'mm_person_room','person_id','room_id',new.id);
        insert into mm_person_research_group ( person_id, research_group_id ) values ( NEW.id, (select id from research_group where name = 'SPRI'));
    else
        update person set 
            first_names = new.first_names,
            surname = new.surname,
            title_id = new.title_id,
            email_address = new.email_address,
            crsid = new.crsid
        where id = old.id;
        perform fn_mm_array_update(new.room_id,old.room_id,'mm_person_room','person_id','room_id',new.id);
    end if;
    return new;
end;
$$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

ALTER FUNCTION hotwire3.spri_people_trig() OWNER TO dev;
REVOKE EXECUTE ON FUNCTION hotwire3.spri_people_trig() FROM PUBLIC;
GRANT EXECUTE ON FUNCTION hotwire3.spri_people_trig() TO spri_hr;

CREATE TRIGGER spri_people_trig INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/People/SPRI_People" FOR EACH ROW EXECUTE PROCEDURE hotwire3.spri_people_trig();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/SPRI_People','person');
