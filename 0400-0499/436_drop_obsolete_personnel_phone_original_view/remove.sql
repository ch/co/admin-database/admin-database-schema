CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Phone_Original" AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid::oid AS image_oid, person.surname, person.first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.external_work_phone_numbers AS external_work_numbers, ARRAY( SELECT mm_person_room.room_id
           FROM mm_person_room
          WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, person.email_address, person.location, _latest_role_v8.post_category_id AS ro_post_category_id, _physical_status_v3.status_id AS ro_physical_status_id, person.is_external, 
        CASE
            WHEN _physical_status_v3.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v3.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            WHEN person.is_external = true THEN 'blue'::text
            ELSE NULL::text
        END AS _cssclass
   FROM person
   LEFT JOIN _latest_role_v8 ON _latest_role_v8.person_id = person.id
   LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
  GROUP BY person.surname, person.first_names, person.id, person_hid.person_hid, person.image_oid, _latest_role_v8.supervisor_id, person.email_address, person.location, person.hide_email, _latest_role_v8.post_category_id, _physical_status_v3.status_id, person._status, person.external_work_phone_numbers, person.is_external
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Phone_Original"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Phone_Original" TO dev;

-- Rule: hotwire_view_personnel_phone_upd ON hotwire."10_View/People/Personnel_Phone_Original"

-- DROP RULE hotwire_view_personnel_phone_upd ON hotwire."10_View/People/Personnel_Phone_Original";

CREATE OR REPLACE RULE hotwire_view_personnel_phone_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Phone_Original" DO INSTEAD ( UPDATE person SET surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address, is_external = new.is_external
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
);


