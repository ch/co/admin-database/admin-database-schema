GRANT USAGE ON SEQUENCE dns_anames_id_seq TO old_cos ;
REVOKE ALL ON SEQUENCE dns_anames_id_seq FROM cos;
GRANT USAGE ON SEQUENCE dns_cnames_seq TO old_cos;
GRANT ALL ON SEQUENCE dns_cnames_seq TO cos;
GRANT USAGE ON SEQUENCE dns_mxrecords_id_seq TO old_cos;
REVOKE ALL ON SEQUENCE dns_mxrecords_id_seq FROM cos;
