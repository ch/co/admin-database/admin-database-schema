GRANT USAGE ON SEQUENCE dns_anames_id_seq TO cos;
REVOKE USAGE ON SEQUENCE dns_anames_id_seq FROM old_cos;
GRANT USAGE ON SEQUENCE dns_cnames_seq TO cos;
REVOKE USAGE ON SEQUENCE dns_cnames_seq FROM old_cos;
GRANT USAGE ON SEQUENCE dns_mxrecords_id_seq TO cos;
REVOKE USAGE ON SEQUENCE dns_mxrecords_id_seq FROM old_cos;
