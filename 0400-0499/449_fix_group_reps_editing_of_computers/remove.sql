REVOKE SELECT ON subnet, ip_address, mm_system_image_ip_address, system_image, vlan FROM shadow_mac_to_vlan_maintenance;
REVOKE SELECT,UPDATE,DELETE,INSERT ON shadow_mac_to_vlan FROM shadow_mac_to_vlan_maintenance;
REVOKE USAGE ON shadow_mac_to_vlan_id_seq FROM shadow_mac_to_vlan_maintenance;

CREATE OR REPLACE FUNCTION shadow_mac_to_vlan_update_from_system_image_change() RETURNS TRIGGER AS
$$
begin

if (TG_OP='DELETE') then
 -- Remove a system image? Nuke its MAC addresses
 delete from shadow_mac_to_vlan where mac in (old.wired_mac_1, old.wired_mac_2, old.wired_mac_4, old.wireless_mac);
elsif (TG_OP='INSERT') then
 -- Adding a system image? Find appropriate MAC addresses and VLANs
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_1, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_1 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_2, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_2 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_3, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_3 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_4, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_4 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wireless_mac, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wireless_mac is not null;
else --TG_OP='UPDATE'
  -- We might change MAC address:
 if (new.wired_mac_1 !=  old.wired_mac_1)  then update shadow_mac_to_vlan set mac=new.wired_mac_1  where mac=old.wired_mac_1; end if;
 if (new.wired_mac_2 !=  old.wired_mac_2)  then update shadow_mac_to_vlan set mac=new.wired_mac_2  where mac=old.wired_mac_2; end if;
 if (new.wired_mac_3 !=  old.wired_mac_3)  then update shadow_mac_to_vlan set mac=new.wired_mac_3  where mac=old.wired_mac_3; end if;
 if (new.wired_mac_4 !=  old.wired_mac_4)  then update shadow_mac_to_vlan set mac=new.wired_mac_4  where mac=old.wired_mac_4; end if;
 if (new.wireless_mac != old.wireless_mac) then update shadow_mac_to_vlan set mac=new.wireless_mac where mac=old.wireless_mac; end if;
 if (old.wired_mac_1 is null and new.wired_mac_1 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_1, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_1 is not null limit 1;
 end if;
 if (old.wired_mac_2 is null and new.wired_mac_2 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_2, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_2 is not null limit 1;
 end if;
 if (old.wired_mac_3 is null and new.wired_mac_3 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_3, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_3 is not null limit 1;
 end if;
 if (old.wired_mac_4 is null and new.wired_mac_4 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_4, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_4 is not null limit 1;
 end if;
 if (old.wireless_mac is null and new.wireless_mac is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wireless_mac, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wireless_mac is not null limit 1;
 end if;
 if (new.wireless_mac is null and old.wireless_mac is not null) then
  delete from shadow_mac_to_vlan where mac=old.wireless_mac;
 end if;
 if (new.wired_mac_1 is null and old.wired_mac_1 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_1;
 end if;
 if (new.wired_mac_2 is null and old.wired_mac_2 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_2;
 end if;
 if (new.wired_mac_3 is null and old.wired_mac_3 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_3;
 end if;
 if (new.wired_mac_4 is null and old.wired_mac_4 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_4;
 end if;
 

 
end if;
return new;

end;

$$ LANGUAGE plpgsql;

COMMENT ON FUNCTION shadow_mac_to_vlan_update_from_system_image_change() IS 'Updates the shadow_mac_to_vlan table when the system_image table changes';

ALTER FUNCTION shadow_mac_to_vlan_update_from_system_image_change() OWNER TO dev;
REVOKE shadow_mac_to_vlan_maintenance FROM dev;
-- DROP ROLE shadow_mac_to_vlan_maintenance;
