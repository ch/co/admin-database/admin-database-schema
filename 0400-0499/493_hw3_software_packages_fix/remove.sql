DROP TRIGGER wpkg_software ON hotwire3."10_View/Software/Packages";
DROP FUNCTION hotwire3.software_package_trig();

CREATE FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_id bigint;

        begin
                -- Detect update
	if v.id is not null then
		v_id = v.id;
		update software_package set
			reporting_string=v.reporting_string,
			name=v.name,
			revision=v.revision,
			priority=v.priority,
			reporting_version=v.reporting_version,
			wpkg_package_name=v.wpkg_package_name,
			munki_package_name=v.munki_package_name,
			min_os_ver=v.min_os_ver,
			max_os_ver=v.max_os_ver,
			reporting_path=v.reporting_path,
			reboot_id=v.reboot_id,
			sitelicence=v.sitelicence,
			on_all_macs=v.on_all_macs
		where id=v.id;
	else
		v_id=nextval('software_package_id_seq');
		insert into software_package (
			id,
			reporting_string,
			name,
			revision,
			priority,
			reporting_version,
			wpkg_package_name,
			munki_package_name,
			min_os_ver,
			max_os_ver,
			reporting_path,
			reboot_id,
			sitelicence,
			on_all_macs
		) values (
			v_id,
			v.reporting_string,
			v.name,
			v.revision,
			v.priority,
			v.reporting_version,
			v.wpkg_package_name,
			v.munki_package_name,
			v.min_os_ver,
			v.max_os_ver,
			v.reporting_path,
			v.reboot_id,
			v.sitelicence,
			v.on_all_macs
		);
	end if;
	perform fn_mm_update('mm_software_package_can_install_on_os'::varchar,
			     'operating_system_id'::varchar,v.operating_system_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_depends_on'::varchar,
	                     'depended_software_package_id'::varchar, v.depended_software_package_id,
	                     'depending_software_package_id'::varchar, v_id);
	perform fn_mm_update('mm_software_package_installs_on_architecture'::varchar,
			     'architecture_id'::varchar,v.architecture_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_updates_software_package'::varchar,
			     'updated_software_package_id'::varchar,v.updated_software_package_id,
			     'updating_software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_is_licensed_by_licence'::varchar,
			     'licence_id'::varchar,v.licence_id,
			     'software_package_id'::varchar,v_id);
-- 	perform fn_mm_update('mm_system_image_has_installed_software_package'::varchar,
-- 			     'system_image_id'::varchar,v.system_image_id,
-- 			     'software_package_id'::varchar,v_id);
        return v_id;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages")
  OWNER TO dev;


CREATE OR REPLACE RULE wpkg_software_ins AS
    ON INSERT TO hotwire3."10_View/Software/Packages" DO INSTEAD  SELECT hotwire3.software_package_insupd(new.*) AS id;

CREATE OR REPLACE RULE wpkg_software_upd AS
    ON UPDATE TO hotwire3."10_View/Software/Packages" DO INSTEAD  SELECT hotwire3.software_package_insupd(new.*) AS id;

