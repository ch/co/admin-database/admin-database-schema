CREATE FUNCTION hotwire3.software_package_trig() RETURNS TRIGGER AS
$$
begin
  if NEW.id is not null then
    update software_package set
      reporting_string=NEW.reporting_string,
      name=NEW.name,
      revision=NEW.revision,
      priority=NEW.priority,
      reporting_version=NEW.reporting_version,
      wpkg_package_name=NEW.wpkg_package_name,
      munki_package_name=NEW.munki_package_name,
      min_os_ver=NEW.min_os_ver,
      max_os_ver=NEW.max_os_ver,
      reporting_path=NEW.reporting_path,
      reboot_id=NEW.reboot_id,
      sitelicence=NEW.sitelicence,
      on_all_macs=NEW.on_all_macs
    where id = new.id; 
  else
    NEW.id = nextval('software_package_id_seq');
    INSERT INTO software_package (
      id,
      reporting_string,
      name,
      revision,
      priority,
      reporting_version,
      wpkg_package_name,
      munki_package_name,
      min_os_ver,
      max_os_ver,
      reporting_path,
      reboot_id,
      sitelicence,
      on_all_macs
    ) VALUES (
      NEW.id,
      NEW.reporting_string,
      NEW.name,
      NEW.revision,
      NEW.priority,
      NEW.reporting_version,
      NEW.wpkg_package_name,
      NEW.munki_package_name,
      NEW.min_os_ver,
      NEW.max_os_ver,
      NEW.reporting_path,
      NEW.reboot_id,
      NEW.sitelicence,
      NEW.on_all_macs
    );
  end if;
  perform fn_mm_update('mm_software_package_can_install_on_os'::varchar,
                       'operating_system_id'::varchar,NEW.operating_system_id,
		       'software_package_id'::varchar,NEW.id);
  perform fn_mm_update('mm_software_package_depends_on'::varchar,
                       'depended_software_package_id'::varchar, NEW.depended_software_package_id,
                       'depending_software_package_id'::varchar, NEW.id);
  perform fn_mm_update('mm_software_package_installs_on_architecture'::varchar,
                       'architecture_id'::varchar,NEW.architecture_id,
                       'software_package_id'::varchar,NEW.id);
  perform fn_mm_update('mm_software_package_updates_software_package'::varchar,
                       'updated_software_package_id'::varchar,NEW.updated_software_package_id,
                       'updating_software_package_id'::varchar,NEW.id);
  perform fn_mm_update('mm_software_package_is_licensed_by_licence'::varchar,
                       'licence_id'::varchar,NEW.licence_id,
                       'software_package_id'::varchar,NEW.id);
  RETURN NEW;
end;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.software_package_trig() OWNER TO dev;

DROP RULE wpkg_software_ins ON hotwire3."10_View/Software/Packages";
DROP RULE wpkg_software_upd ON hotwire3."10_View/Software/Packages";

CREATE TRIGGER wpkg_software INSTEAD OF INSERT OR UPDATE ON hotwire3."10_View/Software/Packages" FOR EACH ROW EXECUTE PROCEDURE hotwire3.software_package_trig();

DROP FUNCTION hotwire3.software_package_insupd(hotwire3."10_View/Software/Packages");
