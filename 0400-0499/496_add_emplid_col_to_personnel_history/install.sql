DROP VIEW hotwire."10_View/People/Personnel_History";

CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_History" AS 
WITH personnel_history AS (
    SELECT person.id,
    person_hid.person_hid AS ro_person,
    person.surname,
    person.first_names,
    person.title_id,
    date_part('year'::text,age(person.date_of_birth::timestamp with time zone)) AS ro_age,
    _latest_role.post_category_id AS ro_post_category_id,
    person.continuous_employment_start_date,
    _latest_employment.start_date AS ro_latest_employment_start_date,
    COALESCE(_latest_employment.end_date,_latest_employment.intended_end_date) AS ro_employment_end_date,
    age(COALESCE(_latest_employment.end_date,'now'::text::date)::timestamp with time zone,person.continuous_employment_start_date::timestamp with time zone)::text AS ro_length_of_continuous_service,
    age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text AS ro_length_of_service_in_current_contract,
    age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone) AS ro_final_length_of_continuous_service,
    age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone) AS ro_final_length_of_service_in_last_contract,
    _latest_role.supervisor_id AS ro_supervisor_id,
    person.other_information::character varying(5000) AS other_information,
    person.notes::character varying(5000) AS notes,
    emplids.emplid_number as ro_emplid_number,
    CASE
        WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
        ELSE NULL::text
    END AS _cssclass, 
    _to_hwsubviewb('10_View/People/Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Review_History'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, 
    _to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
    FROM person
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
    LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
    LEFT JOIN (select person_id, string_agg(emplid_number,'/')::varchar as emplid_number from ( SELECT DISTINCT person_id, emplid_number from postgraduate_studentship WHERE emplid_number is not null ) all_emplids group by person_id ) emplids ON person.id = emplids.person_id
)
 SELECT personnel_history.id, personnel_history.ro_person, personnel_history.surname, personnel_history.first_names, personnel_history.title_id, personnel_history.ro_age, personnel_history.ro_post_category_id, personnel_history.continuous_employment_start_date, personnel_history.ro_latest_employment_start_date, personnel_history.ro_employment_end_date, personnel_history.ro_length_of_continuous_service, personnel_history.ro_length_of_service_in_current_contract, personnel_history.ro_final_length_of_continuous_service, personnel_history.ro_final_length_of_service_in_last_contract, personnel_history.ro_supervisor_id, personnel_history.other_information, personnel_history.notes, personnel_history.ro_emplid_number, personnel_history._cssclass, personnel_history.staff_review_history_subview, personnel_history.cambridge_history_subview
   FROM personnel_history
  ORDER BY personnel_history.surname, personnel_history.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_History"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO accounts;

CREATE OR REPLACE RULE hotwire_view_personnel_history_v2_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_History" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, other_information = new.other_information::text, notes = new.notes::text
  WHERE person.id = old.id;


DROP VIEW hotwire3."10_View/People/Personnel_History";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_History" AS 
 WITH personnel_history AS (
         SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text AS ro_length_of_continuous_service, age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text AS ro_length_of_service_in_current_contract, age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone) AS ro_final_length_of_continuous_service, age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone) AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, person.other_information, person.notes, emplids.emplid_number as ro_emplid_number,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass, _to_hwsubviewb('10_View/People/_Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Reviews'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, _to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
           FROM person
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
    LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
    LEFT JOIN (select person_id, string_agg(emplid_number,'/')::varchar as emplid_number from ( SELECT DISTINCT person_id, emplid_number from postgraduate_studentship WHERE emplid_number is not null ) all_emplids group by person_id ) emplids ON person.id = emplids.person_id
        )
 SELECT personnel_history.id, personnel_history.ro_person, personnel_history.surname, personnel_history.first_names, personnel_history.title_id, personnel_history.ro_age, personnel_history.ro_post_category_id, personnel_history.continuous_employment_start_date, personnel_history.ro_latest_employment_start_date, personnel_history.ro_employment_end_date, personnel_history.ro_length_of_continuous_service, personnel_history.ro_length_of_service_in_current_contract, personnel_history.ro_final_length_of_continuous_service, personnel_history.ro_final_length_of_service_in_last_contract, personnel_history.ro_supervisor_id, personnel_history.other_information, personnel_history.notes, personnel_history.ro_emplid_number, personnel_history._cssclass, personnel_history.staff_review_history_subview, personnel_history.cambridge_history_subview
   FROM personnel_history
  ORDER BY personnel_history.surname, personnel_history.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_History"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_History" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_History" TO accounts;

CREATE OR REPLACE RULE hotwire3_view_personnel_history_v2_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_History" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, other_information = new.other_information, notes = new.notes
  WHERE person.id = old.id;


