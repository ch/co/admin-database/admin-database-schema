ALTER TABLE extract_device_type_hid OWNER TO cen1001;
REVOKE ALL ON extract_device_type_hid FROM dev;

ALTER TABLE extract_arm_canopy_type_hid OWNER TO cen1001;
REVOKE ALL ON extract_arm_canopy_type_hid FROM dev;

ALTER TABLE extract_arm_canopy OWNER TO cen1001;
REVOKE ALL ON extract_arm_canopy FROM dev;

ALTER TABLE glove_box OWNER TO cen1001;
REVOKE ALL ON glove_box FROM dev;

ALTER TABLE vented_enclosure OWNER TO cen1001;
REVOKE ALL ON vented_enclosure FROM dev;

ALTER TABLE vented_enclosure_device_type_hid OWNER TO cen1001;
REVOKE ALL ON vented_enclosure_device_type_hid FROM dev;
