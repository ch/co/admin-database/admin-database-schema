ALTER TABLE extract_device_type_hid OWNER TO dev;
GRANT ALL ON extract_device_type_hid TO cen1001;

ALTER TABLE extract_arm_canopy_type_hid OWNER TO dev;
GRANT ALL ON extract_arm_canopy_type_hid TO cen1001;

ALTER TABLE extract_arm_canopy OWNER TO dev;
GRANT ALL ON extract_arm_canopy TO cen1001;

ALTER TABLE glove_box OWNER TO dev;
GRANT ALL ON glove_box TO cen1001;

ALTER TABLE vented_enclosure OWNER TO dev;
GRANT ALL ON vented_enclosure TO cen1001;

ALTER TABLE vented_enclosure_device_type_hid OWNER TO dev;
GRANT ALL ON vented_enclosure_device_type_hid TO cen1001;
