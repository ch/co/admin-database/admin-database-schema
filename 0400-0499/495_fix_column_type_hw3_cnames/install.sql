DROP VIEW hotwire3."10_View/DNS/CNames";

CREATE OR REPLACE VIEW hotwire3."10_View/DNS/CNames" AS 
 SELECT dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date::date, dns_cnames.research_group_id
   FROM dns_cnames;

ALTER TABLE hotwire3."10_View/DNS/CNames"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/DNS/CNames" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/DNS/CNames" TO cos;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_del AS
    ON DELETE TO hotwire3."10_View/DNS/CNames" DO INSTEAD  DELETE FROM dns_cnames
  WHERE dns_cnames.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_ins AS
    ON INSERT TO hotwire3."10_View/DNS/CNames" DO INSTEAD  INSERT INTO dns_cnames (name, dns_domain_id, toname, fromname, expiry_date, research_group_id) 
  VALUES (new.name, new.dns_domain_id, new.toname, (((new.name::text || '.'::text) || ((( SELECT dns_domains.name
           FROM dns_domains
          WHERE dns_domains.id = new.dns_domain_id))::text))), new.expiry_date::timestamp, new.research_group_id)
  RETURNING dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date::date, dns_cnames.research_group_id;

CREATE OR REPLACE RULE hotwire3_view_dns_cnames_upd AS
    ON UPDATE TO hotwire3."10_View/DNS/CNames" DO INSTEAD  UPDATE dns_cnames SET name = new.name, dns_domain_id = new.dns_domain_id, research_group_id = new.research_group_id, toname = new.toname, expiry_date = new.expiry_date::timestamp, fromname = (((new.name::text || '.'::text) || ((( SELECT dns_domains.name
           FROM dns_domains
          WHERE dns_domains.id = new.dns_domain_id))::text)))
  WHERE dns_cnames.id = old.id;
