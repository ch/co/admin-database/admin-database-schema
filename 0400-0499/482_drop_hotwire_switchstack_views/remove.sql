CREATE OR REPLACE VIEW hotwire."10_View/Switchstack" AS 
 SELECT switchstack.id, switchstack.cabinet_id, switchstack.name, switchstack.description
   FROM switchstack;

ALTER TABLE hotwire."10_View/Switchstack"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Switchstack" TO cen1001;

-- Rule: hotwire_view_switchstack_del ON hotwire."10_View/Switchstack"

-- DROP RULE hotwire_view_switchstack_del ON hotwire."10_View/Switchstack";

CREATE OR REPLACE RULE hotwire_view_switchstack_del AS
    ON DELETE TO hotwire."10_View/Switchstack" DO INSTEAD  DELETE FROM switchstack
  WHERE switchstack.id = old.id;

-- Rule: hotwire_view_switchstack_ins ON hotwire."10_View/Switchstack"

-- DROP RULE hotwire_view_switchstack_ins ON hotwire."10_View/Switchstack";

CREATE OR REPLACE RULE hotwire_view_switchstack_ins AS
    ON INSERT TO hotwire."10_View/Switchstack" DO INSTEAD  INSERT INTO switchstack (cabinet_id, name, description) 
  VALUES (new.cabinet_id, new.name, new.description)
  RETURNING switchstack.id, switchstack.cabinet_id, switchstack.name, switchstack.description;

-- Rule: hotwire_view_switchstack_upd ON hotwire."10_View/Switchstack"

-- DROP RULE hotwire_view_switchstack_upd ON hotwire."10_View/Switchstack";

CREATE OR REPLACE RULE hotwire_view_switchstack_upd AS
    ON UPDATE TO hotwire."10_View/Switchstack" DO INSTEAD  UPDATE switchstack SET cabinet_id = new.cabinet_id, name = new.name, description = new.description
  WHERE switchstack.id = old.id;

CREATE OR REPLACE VIEW switchstack_view AS 
 SELECT switchstack.id, switchstack.cabinet_id, switchstack.name, switchstack.description
   FROM switchstack;

ALTER TABLE switchstack_view
  OWNER TO cen1001;
GRANT ALL ON TABLE switchstack_view TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE switchstack_view TO old_cos;

-- Rule: switchstack_del ON switchstack_view

-- DROP RULE switchstack_del ON switchstack_view;

CREATE OR REPLACE RULE switchstack_del AS
    ON DELETE TO switchstack_view DO INSTEAD  DELETE FROM switchstack
  WHERE switchstack.id = old.id;

-- Rule: switchstack_ins ON switchstack_view

-- DROP RULE switchstack_ins ON switchstack_view;

CREATE OR REPLACE RULE switchstack_ins AS
    ON INSERT TO switchstack_view DO INSTEAD  INSERT INTO switchstack (cabinet_id, name, description) 
  VALUES (new.cabinet_id, new.name, new.description)
  RETURNING switchstack.id, switchstack.cabinet_id, switchstack.name, switchstack.description;

-- Rule: switchstack_upd ON switchstack_view

-- DROP RULE switchstack_upd ON switchstack_view;

CREATE OR REPLACE RULE switchstack_upd AS
    ON UPDATE TO switchstack_view DO INSTEAD  UPDATE switchstack SET cabinet_id = new.cabinet_id, name = new.name, description = new.description
  WHERE switchstack.id = old.id;

CREATE OR REPLACE VIEW cabinet_switchstack_subview AS 
 SELECT switchstack.id, cabinet.id AS cabinet_id, switchstack.name, 'switchstack_view'::text AS _targetview
   FROM cabinet
   LEFT JOIN switchstack ON switchstack.cabinet_id = cabinet.id;

ALTER TABLE cabinet_switchstack_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE cabinet_switchstack_subview TO cen1001;
GRANT SELECT ON TABLE cabinet_switchstack_subview TO old_cos;


CREATE OR REPLACE VIEW switch_switchstack_subview AS 
 SELECT switchstack.id, switch.id AS switch_id, switchstack.name, 'switchstack_view'::text AS _targetview
   FROM switch
   LEFT JOIN switchstack ON switch.switchstack_id = switchstack.id;

ALTER TABLE switch_switchstack_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE switch_switchstack_subview TO cen1001;
GRANT SELECT ON TABLE switch_switchstack_subview TO old_cos;

CREATE OR REPLACE VIEW switchstack_cabinet_subview AS 
 SELECT switchstack.cabinet_id AS id, switchstack.id AS switchstack_id, cabinet.name, cabinet.location, 'cabinet_view'::text AS _targetview
   FROM switchstack
   JOIN cabinet ON switchstack.cabinet_id = cabinet.id;

ALTER TABLE switchstack_cabinet_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE switchstack_cabinet_subview TO cen1001;
GRANT SELECT ON TABLE switchstack_cabinet_subview TO old_cos;

CREATE OR REPLACE VIEW switchstack_switches_subview AS 
 SELECT switch.id, switchstack.id AS switchstack_id, hardware.name, hardware.room_id, 'switch_view'::text AS _targetview
   FROM switchstack
   JOIN switch ON switchstack.id = switch.switchstack_id
   JOIN hardware ON switch.hardware_id = hardware.id;

ALTER TABLE switchstack_switches_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE switchstack_switches_subview TO cen1001;
GRANT SELECT ON TABLE switchstack_switches_subview TO old_cos;

