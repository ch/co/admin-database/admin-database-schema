ALTER TABLE person DROP CONSTRAINT crsid_is_valid;

CREATE OR REPLACE FUNCTION set_crsid()
  RETURNS trigger AS
$BODY$

    # if the email address has been updated then we attempt to set the
    # crsid
    # ...only we can't because of the way the front end does updates, you
    # can get multiple identical updates for a given record which leads to
    # careful tests clobbering the update to the crsid
    my $new_email_address = $_TD->{new}->{email_address};
    #my $old_email_address = $_TD->{old}->{email_address};
    #my $new_crsid = $_TD->{new}->{crsid};
    #my $old_crsid = $_TD->{old}->{crsid};

    #if ( $old_email_address ne $new_email_address )
    {
        # work out the new crsid from the email address if we can
        if ( $new_email_address =~ /[^a-z]*([a-z][a-z0-9]{1,7})\@cam\.ac\.uk.*/ )
        {
            $calc_crsid=$1;
            #die $calc_crsid;
            # see if it is different than the old one
            # and the user didn't excplitly want to change it
            #if (($calc_crsid ne $old_crsid) and ( $old_crsid eq $new_crsid ))
            #if (($calc_crsid ne $old_crsid))
            {
                $_TD->{new}->{crsid}=$calc_crsid;
                #die $calc_crsid;
                #die 'modify';
                return 'MODIFY';
            } #else { die 'no change';}
        } #else { die 'no match'; }

        
    } #else { die 'there'; }

    return;
    
$BODY$
  LANGUAGE plperl VOLATILE
  COST 100;
ALTER FUNCTION set_crsid()
  OWNER TO cen1001;

