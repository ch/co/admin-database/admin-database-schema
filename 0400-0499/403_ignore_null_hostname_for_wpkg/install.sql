CREATE OR REPLACE VIEW apps.wpkg_hosts AS 
 SELECT mm_system_image_ip_address.system_image_id, ip_address.hostname, ip_address.short_hostname, fai_class_hid.fai_class_hid AS extra_profiles
   FROM ip_address
   JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
   LEFT JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
   LEFT JOIN research_group ON system_image.research_group_id = research_group.id
   LEFT JOIN fai_class_hid ON research_group.fai_class_id = fai_class_hid.fai_class_id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN os_class_hid ON os_class_hid.os_class_id = operating_system.os_class_id
  WHERE COALESCE(os_class_hid.os_class_hid::text, ''::text) = 'Windows'::text
  AND hostname is not null;

ALTER TABLE apps.wpkg_hosts
  OWNER TO dev;
GRANT ALL ON TABLE apps.wpkg_hosts TO dev;
GRANT SELECT ON TABLE apps.wpkg_hosts TO wpkguser;
COMMENT ON VIEW apps.wpkg_hosts
  IS 'Used by hosts_xml.php on wpkg-xml';

