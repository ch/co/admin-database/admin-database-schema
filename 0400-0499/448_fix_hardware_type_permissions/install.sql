ALTER TABLE hardware_type OWNER TO dev;
-- this is just insurance in case there are views using this table that are still owned by me
GRANT ALL ON hardware_type TO cen1001;

ALTER SEQUENCE hardwaretype_id_seq OWNER TO dev;
GRANT USAGE ON hardwaretype_id_seq TO cen1001;
GRANT USAGE ON hardwaretype_id_seq TO cos;
