ALTER TABLE hardware_type OWNER TO cen1001;
GRANT ALL ON TABLE hardware_type TO dev;

ALTER SEQUENCE hardwaretype_id_seq OWNER TO cen1001;
REVOKE USAGE ON hardwaretype_id_seq FROM cos;
GRANT ALL ON SEQUENCE hardwaretype_id_seq TO dev;
