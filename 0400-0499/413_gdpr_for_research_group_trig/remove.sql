CREATE OR REPLACE FUNCTION research_group_trig()
  RETURNS trigger AS
$BODY$


    my $table = $_TD->{table_name};
    my $person_id = $_TD->{new}{person_id};
    my $debug='Doing nothing';

    my $field = 'supervisor_id';
    if ($table eq 'visitorship') { $field = 'host_person_id'; }
    if ($table eq 'postgraduate_studentship') { $field = 'first_supervisor_id'; }

    my $supervisor_id = $_TD->{new}{$field};

    # what if the supervisor id isn't given? in which case we can bail
    if (defined $supervisor_id) {
    
        # find the research group from the supervisor_id
        my $rv = spi_exec_query('select id from research_group where head_of_group_id = ' . $supervisor_id . ' and internal_use_only = $$f$$');
        # how many groups did we find?
        my $number_of_groups = $rv->{processed};
    
        # if the group for this PI is unique it is safe to try to add the person to it
        # Some people head more than one group.
        if ($number_of_groups == 1) {
            my $research_group_id = $rv->{rows}[0]->{id};
            $debug = 'found rg id ' . $research_group_id;
            # check if they're in the group already
            $rv = spi_exec_query('select * from mm_person_research_group where person_id = ' . $person_id . ' and research_group_id = ' .  $research_group_id);
            # if not then insert them
            if ($rv->{processed} == 0) {
                $rv = spi_exec_query('insert into mm_person_research_group ( person_id, research_group_id ) values ( ' . $person_id . ', ' .  $research_group_id . ')');
                $debug = $debug . ' putting person ' . $person_id . ' group ' . $research_group_id; 
            } else { $debug = 'person ' . $person_id . ' is already in group ' . $research_group_id; }
        } 
    
        #spi_exec_query('insert into _debug ( stamp, output ) values ( now(), $$' . $debug . '$$ )');
    }
    return; 
    
$BODY$
  LANGUAGE plperl VOLATILE
  COST 100;
ALTER FUNCTION research_group_trig()
  OWNER TO cen1001;

