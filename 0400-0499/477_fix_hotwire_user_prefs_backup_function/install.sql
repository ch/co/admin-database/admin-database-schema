CREATE OR REPLACE FUNCTION hotwire_user_migration_script()
  RETURNS SETOF character varying AS
$BODY$ 
select s from (
select $$delete from hotwire."hw_User Preferences";$$ as s, 0 as weight
union
select 
    $$insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$',$$||
    $$( select pg_roles.oid from pg_roles where pg_roles.rolname = '$$ ||
    pg_roles.rolname ||
    $$') ) ;$$ as s,
    1 as weight

from hotwire."hw_User Preferences" prefs
join pg_roles on prefs.user_id = pg_roles.oid 
union
select
    $$insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$');$$ as s,
    1 as weight
from hotwire."hw_User Preferences" where user_id is null
) restores order by weight;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION hotwire_user_migration_script()
  OWNER TO dev;

CREATE OR REPLACE FUNCTION hotwire3_user_migration_script()
  RETURNS SETOF character varying AS
$BODY$ 
select s from (
select $$delete from hotwire3."hw_User Preferences";$$ as s, 0 as weight
union
select 
    $$insert into hotwire3."hw_User Preferences" (preference_id, preference_value, user_id ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$',$$||
    $$( select pg_roles.oid from pg_roles where pg_roles.rolname = '$$ ||
    pg_roles.rolname ||
    $$') ) ;$$ as s,
    1 as weight

from hotwire3."hw_User Preferences" prefs
join pg_roles on prefs.user_id = pg_roles.oid 
union
select
    $$insert into hotwire3."hw_User Preferences" (preference_id, preference_value ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$');$$ as s,
    1 as weight
from hotwire3."hw_User Preferences" where user_id is null
) restores order by weight;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION hotwire3_user_migration_script()
  OWNER TO dev;

