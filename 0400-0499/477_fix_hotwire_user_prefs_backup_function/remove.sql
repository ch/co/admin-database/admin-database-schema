CREATE OR REPLACE FUNCTION hotwire_user_migration_script()
  RETURNS SETOF character varying AS
$BODY$ 
select s from (
select $$delete from hotwire."hw_User Preferences";$$ as s, 0 as weight
union
select 
    $$insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$',$$||
    $$( select pg_user.usesysid from pg_user where pg_user.usename = '$$ ||
    pg_user.usename ||
    $$') ) ;$$ as s,
    1 as weight

from hotwire."hw_User Preferences" prefs
join pg_user on prefs.user_id = pg_user.usesysid 
union
select
    $$insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values ($$ ||
    preference_id ||
    $$,'$$ ||
    preference_value ||
    $$');$$ as s,
    1 as weight
from hotwire."hw_User Preferences" where user_id is null
) restores order by weight;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION hotwire_user_migration_script()
  OWNER TO dev;

DROP FUNCTION hotwire3_user_migration_script();
