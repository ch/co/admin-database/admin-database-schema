DROP VIEW hotwire3."10_View/People/Personnel_Data_Entry";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Data_Entry"
 AS
 SELECT a.id,
    a.ro_person_id,
    a.image_oid,
    a.surname,
    a.first_names,
    a.title_id,
    a.known_as,
    a.name_suffix,
    a.previous_surname,
    a.date_of_birth,
    a.ro_age,
    a.gender_id,
    a.ro_post_category_id,
    a.ro_chem,
    a.crsid,
    a.email_address,
    a.hide_email,
    a.arrival_date,
    a.leaving_date,
    a.ro_physical_status_id,
    a.left_but_no_leaving_date_given,
    a.dept_telephone_number_id,
    a.hide_phone_no_from_website,
    a.hide_from_website,
    a.room_id,
    a.ro_supervisor_id,
    a.ro_co_supervisor_id,
    a.research_group_id,
    a.home_address,
    a.home_phone_number,
    a.cambridge_college_id,
    a.mobile_number,
    a.nationality_id,
    a.emergency_contact,
    a.location,
    a.other_information,
    a.notes,
    a.forwarding_address,
    a.new_employer_address,
    a.chem_at_cam AS "chem_@_cam",
    a.retired_staff_mailing_list,
    a.continuous_employment_start_date,
    a.paper_file_details AS paper_file_status,
    a.registration_completed,
    a.clearance_cert_signed,
    a._cssclass,
    a.treat_as_academic_staff
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            person.image_lo AS image_oid,
            person.surname,
            person.first_names,
            person.title_id,
            person.known_as,
            person.name_suffix,
            person.previous_surname,
            person.date_of_birth,
            date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age,
            person.gender_id,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.chem AS ro_chem,
            person.crsid,
            person.email_address,
            person.hide_email,
            person.do_not_show_on_website AS hide_from_website,
            person.arrival_date,
            person.leaving_date,
            _physical_status.status_id AS ro_physical_status_id,
            person.left_but_no_leaving_date_given,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            person.hide_phone_no_from_website,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            _latest_role.co_supervisor_id AS ro_co_supervisor_id,
            _latest_role.mentor_id AS ro_mentor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            person.cambridge_address::text AS home_address,
            person.cambridge_phone_number AS home_phone_number,
            person.cambridge_college_id,
            person.mobile_number,
            ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE mm_person_nationality.person_id = person.id) AS nationality_id,
            person.emergency_contact::text AS emergency_contact,
            person.location,
            person.other_information,
            person.notes,
            person.forwarding_address::text AS forwarding_address,
            person.new_employer_address::text AS new_employer_address,
            person.chem_at_cam,
                CASE
                    WHEN retired_staff_list.include_person_id IS NOT NULL THEN true
                    ELSE false
                END AS retired_staff_mailing_list,
            person.continuous_employment_start_date,
            person.paper_file_details,
            person.registration_completed,
            person.clearance_cert_signed,
            person.counts_as_academic AS treat_as_academic_staff,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
             LEFT JOIN ( SELECT mm_mailinglist_include_person.mailinglist_id,
                    mm_mailinglist_include_person.include_person_id
                   FROM mm_mailinglist_include_person
                     JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
                  WHERE mailinglist.name::text = 'chem-retiredstaff'::text) retired_staff_list ON person.id = retired_staff_list.include_person_id) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_Data_Entry"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO tkd25;

CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare 
	d_do_not_show_on_website boolean := 'f';
	d_hide_phone_no_from_website boolean := 'f';
	on_retired_staff_list bigint;
	retired_staff_list_id bigint;

begin
	if NEW.id is not null 
	then
		update person set
			surname = NEW.surname,
			image_lo = NEW.image_oid,
			first_names = NEW.first_names, 
			title_id = NEW.title_id, 
			known_as = NEW.known_as,
			name_suffix = NEW.name_suffix,
			previous_surname = NEW.previous_surname,
			date_of_birth = NEW.date_of_birth, 
			gender_id = NEW.gender_id, 
			crsid = NEW.crsid,
			email_address = NEW.email_address, 
			hide_email = NEW.hide_email, 
			do_not_show_on_website = COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
			hide_phone_no_from_website = COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
			arrival_date = NEW.arrival_date,
			leaving_date = NEW.leaving_date,
			left_but_no_leaving_date_given = NEW.left_but_no_leaving_date_given,
			cambridge_address = NEW.home_address::varchar(500),
			cambridge_phone_number = NEW.home_phone_number,
			cambridge_college_id = NEW.cambridge_college_id,
			mobile_number = NEW.mobile_number,
			emergency_contact = NEW.emergency_contact::varchar(500), 
			location = NEW.location,
			other_information = NEW.other_information,
			notes = NEW.notes,
			forwarding_address = NEW.forwarding_address,
			new_employer_address = NEW.new_employer_address, 
			chem_at_cam = NEW."chem_@_cam",
			continuous_employment_start_date = NEW.continuous_employment_start_date,
			paper_file_details = NEW.paper_file_status, 
			registration_completed = NEW.registration_completed,
			clearance_cert_signed = NEW.clearance_cert_signed,
			counts_as_academic=NEW.treat_as_academic_staff
		where person.id = OLD.id;
	else
                NEW.id := nextval('person_id_seq');
		insert into person (
                                id,
				surname,
				image_lo, 
				first_names, 
				title_id, 
				known_as,
				name_suffix,
				previous_surname, 
				date_of_birth, 
				gender_id, 
				crsid,
				email_address, 
				hide_email, 
				do_not_show_on_website,
				arrival_date,
				leaving_date,
				cambridge_address,
				cambridge_phone_number, 
				cambridge_college_id,
				mobile_number,
				emergency_contact, 
				location,
				other_information,
				notes,
				forwarding_address,
				new_employer_address, 
				chem_at_cam,
				continuous_employment_start_date,
				paper_file_details, 
				registration_completed,
				clearance_cert_signed,
				counts_as_academic,
				hide_phone_no_from_website

			) values (
                                NEW.id,
				NEW.surname, 
				NEW.image_oid,
				NEW.first_names, 
				NEW.title_id, 
				NEW.known_as,
				NEW.name_suffix,
				NEW.previous_surname,
				NEW.date_of_birth, 
				NEW.gender_id, 
				NEW.crsid,
				NEW.email_address, 
				NEW.hide_email, 
				COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
				NEW.arrival_date,
				NEW.leaving_date,
				NEW.home_address::varchar(500),
				NEW.home_phone_number, 
				NEW.cambridge_college_id,
				NEW.mobile_number,
				NEW.emergency_contact::varchar(500), 
				NEW.location,
				NEW.other_information,
				NEW.notes,
				NEW.forwarding_address,
				NEW.new_employer_address, 
				NEW."chem_@_cam",
				NEW.continuous_employment_start_date,
				NEW.paper_file_status, 
				NEW.registration_completed,
				NEW.clearance_cert_signed,
				NEW.treat_as_academic_staff,
				COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website)

			) ;
	end if;

	-- do retired staff list update
	select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
	select include_person_id from mm_mailinglist_include_person where include_person_id = NEW.id and mailinglist_id = retired_staff_list_id into on_retired_staff_list;
	if NEW.retired_staff_mailing_list = 't' and on_retired_staff_list is null
	then
	   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, NEW.id); 
	end if;
	if NEW.retired_staff_mailing_list = 'f' and on_retired_staff_list is not null
	then
	   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = NEW.id; 
	end if;

	-- many-many updates here
	perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, NEW.id);

	
	return NEW;
end;
$BODY$;

ALTER FUNCTION hotwire3.personnel_data_entry()
    OWNER TO dev;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_del AS
    ON DELETE TO hotwire3."10_View/People/Personnel_Data_Entry"
    DO INSTEAD
(DELETE FROM person
  WHERE (person.id = old.id));

CREATE TRIGGER hotwire3_view_personnel_data_entry
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/People/Personnel_Data_Entry"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.personnel_data_entry();


