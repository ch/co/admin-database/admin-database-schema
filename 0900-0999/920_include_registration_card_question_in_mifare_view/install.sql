DROP VIEW hotwire3."10_View/People/MiFare_Access";

CREATE OR REPLACE VIEW hotwire3."10_View/People/MiFare_Access"
 AS
 SELECT form.form_id AS id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.crsid AS ro_crsid,
    form.mifare_access_level_id AS ro_mifare_access_level_id,
    form.mifare_areas AS ro_group_areas,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    university_card.legacy_card_holder_id AS ro_card,
    university_card.mifare_number AS ro_mifarenum,
    university_card.mifare_id AS ro_mifare_identifier,
    university_card.barcode AS ro_barcode,
    form.already_has_university_card AS had_card_before_registering,
    form.mifare_access_processed
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN university_card ON lower(university_card.crsid) = person.crsid::text
  WHERE form._imported_on <= 'now'::text::date AND form.mifare_access_processed = false;

ALTER TABLE hotwire3."10_View/People/MiFare_Access"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/MiFare_Access" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO reception;


CREATE TRIGGER mifare_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/MiFare_Access"
    FOR EACH ROW
    EXECUTE FUNCTION registration.update_mifare();


