CREATE ROLE nonacad_tokens;

GRANT SELECT ON TABLE public.non_chemnet_user_account TO nonacad_tokens;
