DROP VIEW hotwire3."10_View/People/Incomplete_Registration_Forms";

CREATE VIEW hotwire3."10_View/People/Incomplete_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form._form_started AS form_started_on,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signer_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_separate_safety_form,
    registration.state_of_form(form.uuid) AS reason,
    form.uuid AS form_uuid,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE registration.state_of_form(form.uuid) <> 'Complete'::text;

ALTER TABLE hotwire3."10_View/People/Incomplete_Registration_Forms"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO dev;
GRANT SELECT, DELETE ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO student_management;


DROP FUNCTION hotwire3.incomplete_registration_form_upd();
CREATE RULE hw3_incomplete_reg_del AS
    ON DELETE TO hotwire3."10_View/People/Incomplete_Registration_Forms"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.uuid = old.form_uuid));

