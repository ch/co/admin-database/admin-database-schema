SET ROLE dev;
SET ROLE slony;

-- View: public.all_group_computers_report

-- DROP VIEW public.all_group_computers_report;

CREATE OR REPLACE VIEW public.all_group_computers_report
 AS
 SELECT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ip_address_hid.ip_address_hid AS ro_mm_ip_address,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid USING (ip_address_id)
  WHERE ip_address_hid.ip_address_hid !~~ '%personal.private%'::text;

ALTER TABLE public.all_group_computers_report
    OWNER TO postgres;

GRANT ALL ON TABLE public.all_group_computers_report TO groupitreps;
GRANT ALL ON TABLE public.all_group_computers_report TO postgres;

-- View: public.building_floors_view

-- DROP VIEW public.building_floors_view;

CREATE OR REPLACE VIEW public.building_floors_view
 AS
 SELECT building_floor_hid.building_floor_id AS id,
    building_floor_hid.building_floor_hid AS building_floor
   FROM building_floor_hid;

ALTER TABLE public.building_floors_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.building_floors_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.building_floors_view TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE public.building_floors_view TO space_management;


-- Rule: building_floors_ins ON public.building_floors_view

-- DROP Rule building_floors_ins ON public.building_floors_view;

CREATE OR REPLACE RULE building_floors_ins AS
    ON INSERT TO public.building_floors_view
    DO INSTEAD
(INSERT INTO building_floor_hid (building_floor_hid)
  VALUES (new.building_floor)
  RETURNING building_floor_hid.building_floor_id,
    building_floor_hid.building_floor_hid);

-- Rule: building_floors_upd ON public.building_floors_view

-- DROP Rule building_floors_upd ON public.building_floors_view;

CREATE OR REPLACE RULE building_floors_upd AS
    ON UPDATE TO public.building_floors_view
    DO INSTEAD
(UPDATE building_floor_hid SET building_floor_hid = new.building_floor
  WHERE (building_floor_hid.building_floor_id = old.id));

-- View: public.building_regions_view

-- DROP VIEW public.building_regions_view;

CREATE OR REPLACE VIEW public.building_regions_view
 AS
 SELECT building_region_hid.building_region_id AS id,
    building_region_hid.building_region_hid AS building_region
   FROM building_region_hid;

ALTER TABLE public.building_regions_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.building_regions_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.building_regions_view TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE public.building_regions_view TO space_management;


-- Rule: building_regions_ins ON public.building_regions_view

-- DROP Rule building_regions_ins ON public.building_regions_view;

CREATE OR REPLACE RULE building_regions_ins AS
    ON INSERT TO public.building_regions_view
    DO INSTEAD
(INSERT INTO building_region_hid (building_region_hid)
  VALUES (new.building_region)
  RETURNING building_region_hid.building_region_id,
    building_region_hid.building_region_hid);

-- Rule: building_regions_upd ON public.building_regions_view

-- DROP Rule building_regions_upd ON public.building_regions_view;

CREATE OR REPLACE RULE building_regions_upd AS
    ON UPDATE TO public.building_regions_view
    DO INSTEAD
(UPDATE building_region_hid SET building_region_hid = new.building_region
  WHERE (building_region_hid.building_region_id = old.id));


-- View: public.buildings_view

-- DROP VIEW public.buildings_view;

CREATE OR REPLACE VIEW public.buildings_view
 AS
 SELECT building_hid.building_id AS id,
    building_hid.building_hid AS building
   FROM building_hid;

ALTER TABLE public.buildings_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.buildings_view TO cen1001;
GRANT INSERT, SELECT, UPDATE ON TABLE public.buildings_view TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE public.buildings_view TO space_management;


-- Rule: buildings_ins ON public.buildings_view

-- DROP Rule buildings_ins ON public.buildings_view;

CREATE OR REPLACE RULE buildings_ins AS
    ON INSERT TO public.buildings_view
    DO INSTEAD
(INSERT INTO building_hid (building_hid)
  VALUES (new.building));

-- Rule: buildings_upd ON public.buildings_view

-- DROP Rule buildings_upd ON public.buildings_view;

CREATE OR REPLACE RULE buildings_upd AS
    ON UPDATE TO public.buildings_view
    DO INSTEAD
(UPDATE building_hid SET building_hid = new.building
  WHERE (building_hid.building_id = old.id));

-- View: public.cabinet_patch_panel_subview

-- DROP VIEW public.cabinet_patch_panel_subview;

CREATE OR REPLACE VIEW public.cabinet_patch_panel_subview
 AS
 SELECT patch_panel.id,
    patch_panel.cabinet_id,
    patch_panel.name,
    patch_panel.patch_panel_type_id,
    'patch_panel_view'::text AS _targetview
   FROM patch_panel
  ORDER BY patch_panel.name;

ALTER TABLE public.cabinet_patch_panel_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.cabinet_patch_panel_subview TO cen1001;

-- View: public.cabinet_view

-- DROP VIEW public.cabinet_view;

CREATE OR REPLACE VIEW public.cabinet_view
 AS
 SELECT cabinet.id,
    cabinet.name,
    cabinet.location,
    cabinet.notes AS access_details
   FROM cabinet
  ORDER BY cabinet.location;

ALTER TABLE public.cabinet_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.cabinet_view TO cen1001;


-- Rule: cabinet_del ON public.cabinet_view

-- DROP Rule cabinet_del ON public.cabinet_view;

CREATE OR REPLACE RULE cabinet_del AS
    ON DELETE TO public.cabinet_view
    DO INSTEAD
(DELETE FROM cabinet
  WHERE (cabinet.id = old.id));

-- Rule: cabinet_ins ON public.cabinet_view

-- DROP Rule cabinet_ins ON public.cabinet_view;

CREATE OR REPLACE RULE cabinet_ins AS
    ON INSERT TO public.cabinet_view
    DO INSTEAD
(INSERT INTO cabinet (id, name, location, notes)
  VALUES (nextval('cabinet_id_seq'::regclass), new.name, new.location, new.access_details)
  RETURNING cabinet.id,
    cabinet.name,
    cabinet.location,
    cabinet.notes AS access_details);

-- Rule: cabinet_upd ON public.cabinet_view

-- DROP Rule cabinet_upd ON public.cabinet_view;

CREATE OR REPLACE RULE cabinet_upd AS
    ON UPDATE TO public.cabinet_view
    DO INSTEAD
(UPDATE cabinet SET name = new.name, location = new.location, notes = new.access_details
  WHERE (cabinet.id = old.id));

-- View: public.dns_anames_view

-- DROP VIEW public.dns_anames_view;

CREATE OR REPLACE VIEW public.dns_anames_view
 AS
 SELECT dns_anames.id,
    dns_anames.hostname,
    dns_anames.ip_address_id
   FROM dns_anames;

ALTER TABLE public.dns_anames_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.dns_anames_view TO cen1001;
GRANT ALL ON TABLE public.dns_anames_view TO dev;


-- Rule: dns_anames_del ON public.dns_anames_view

-- DROP Rule dns_anames_del ON public.dns_anames_view;

CREATE OR REPLACE RULE dns_anames_del AS
    ON DELETE TO public.dns_anames_view
    DO INSTEAD
(DELETE FROM dns_anames
  WHERE (dns_anames.id = old.id));

-- Rule: dns_anames_ins ON public.dns_anames_view

-- DROP Rule dns_anames_ins ON public.dns_anames_view;

CREATE OR REPLACE RULE dns_anames_ins AS
    ON INSERT TO public.dns_anames_view
    DO INSTEAD
(INSERT INTO dns_anames (id, hostname, ip_address_id)
  VALUES (nextval('dns_anames_id_seq'::regclass), new.hostname, new.ip_address_id)
  RETURNING dns_anames.id,
    dns_anames.hostname,
    dns_anames.ip_address_id);

-- Rule: dns_anames_upd ON public.dns_anames_view

-- DROP Rule dns_anames_upd ON public.dns_anames_view;

CREATE OR REPLACE RULE dns_anames_upd AS
    ON UPDATE TO public.dns_anames_view
    DO INSTEAD
(UPDATE dns_anames SET hostname = new.hostname, ip_address_id = new.ip_address_id
  WHERE (dns_anames.id = old.id));

-- View: public.dns_cnames_view

-- DROP VIEW public.dns_cnames_view;

CREATE OR REPLACE VIEW public.dns_cnames_view
 AS
 SELECT dns_cnames.id,
    dns_cnames.fromname,
    dns_cnames.toname
   FROM dns_cnames;

ALTER TABLE public.dns_cnames_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.dns_cnames_view TO cen1001;
GRANT ALL ON TABLE public.dns_cnames_view TO dev;


-- Rule: dns_cnames_del ON public.dns_cnames_view

-- DROP Rule dns_cnames_del ON public.dns_cnames_view;

CREATE OR REPLACE RULE dns_cnames_del AS
    ON DELETE TO public.dns_cnames_view
    DO INSTEAD
(DELETE FROM dns_cnames
  WHERE (dns_cnames.id = old.id));

-- Rule: dns_cnames_ins ON public.dns_cnames_view

-- DROP Rule dns_cnames_ins ON public.dns_cnames_view;

CREATE OR REPLACE RULE dns_cnames_ins AS
    ON INSERT TO public.dns_cnames_view
    DO INSTEAD
(INSERT INTO dns_cnames (id, fromname, toname)
  VALUES (nextval('dns_cnames_seq'::regclass), new.fromname, new.toname)
  RETURNING dns_cnames.id,
    dns_cnames.fromname,
    dns_cnames.toname);

-- Rule: dns_cnames_upd ON public.dns_cnames_view

-- DROP Rule dns_cnames_upd ON public.dns_cnames_view;

CREATE OR REPLACE RULE dns_cnames_upd AS
    ON UPDATE TO public.dns_cnames_view
    DO INSTEAD
(UPDATE dns_cnames SET fromname = new.fromname, toname = new.toname
  WHERE (dns_cnames.id = old.id));

-- View: public.dns_mxrecords_view

-- DROP VIEW public.dns_mxrecords_view;

CREATE OR REPLACE VIEW public.dns_mxrecords_view
 AS
 SELECT dns_mxrecords.id,
    dns_mxrecords.domainname,
    dns_mxrecords.mailhost
   FROM dns_mxrecords;

ALTER TABLE public.dns_mxrecords_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.dns_mxrecords_view TO cen1001;
GRANT ALL ON TABLE public.dns_mxrecords_view TO dev;


-- Rule: dns_mxrecords_del ON public.dns_mxrecords_view

-- DROP Rule dns_mxrecords_del ON public.dns_mxrecords_view;

CREATE OR REPLACE RULE dns_mxrecords_del AS
    ON DELETE TO public.dns_mxrecords_view
    DO INSTEAD
(DELETE FROM dns_mxrecords
  WHERE (dns_mxrecords.id = old.id));

-- Rule: dns_mxrecords_ins ON public.dns_mxrecords_view

-- DROP Rule dns_mxrecords_ins ON public.dns_mxrecords_view;

CREATE OR REPLACE RULE dns_mxrecords_ins AS
    ON INSERT TO public.dns_mxrecords_view
    DO INSTEAD
(INSERT INTO dns_mxrecords (id, domainname, mailhost)
  VALUES (nextval('dns_mxrecords_id_seq'::regclass), new.domainname, new.mailhost)
  RETURNING dns_mxrecords.id,
    dns_mxrecords.domainname,
    dns_mxrecords.mailhost);

-- Rule: dns_mxrecords_upd ON public.dns_mxrecords_view

-- DROP Rule dns_mxrecords_upd ON public.dns_mxrecords_view;

CREATE OR REPLACE RULE dns_mxrecords_upd AS
    ON UPDATE TO public.dns_mxrecords_view
    DO INSTEAD
(UPDATE dns_mxrecords SET domainname = new.domainname, mailhost = new.mailhost
  WHERE (dns_mxrecords.id = old.id));


-- View: public.edit_hardware_types_view

-- DROP VIEW public.edit_hardware_types_view;

CREATE OR REPLACE VIEW public.edit_hardware_types_view
 AS
 SELECT hardware_type.id,
    hardware_type.name,
    hardware_type.subnet_id
   FROM hardware_type;

ALTER TABLE public.edit_hardware_types_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.edit_hardware_types_view TO cen1001;


-- Rule: edit_hardware_types_del ON public.edit_hardware_types_view

-- DROP Rule edit_hardware_types_del ON public.edit_hardware_types_view;

CREATE OR REPLACE RULE edit_hardware_types_del AS
    ON DELETE TO public.edit_hardware_types_view
    DO INSTEAD
(DELETE FROM hardware_type
  WHERE (hardware_type.id = old.id));

-- Rule: edit_hardware_types_ins ON public.edit_hardware_types_view

-- DROP Rule edit_hardware_types_ins ON public.edit_hardware_types_view;

CREATE OR REPLACE RULE edit_hardware_types_ins AS
    ON INSERT TO public.edit_hardware_types_view
    DO INSTEAD
(INSERT INTO hardware_type (name, subnet_id)
  VALUES (new.name, new.subnet_id)
  RETURNING hardware_type.id,
    hardware_type.name,
    hardware_type.subnet_id);

-- Rule: edit_hardware_types_upd ON public.edit_hardware_types_view

-- DROP Rule edit_hardware_types_upd ON public.edit_hardware_types_view;

CREATE OR REPLACE RULE edit_hardware_types_upd AS
    ON UPDATE TO public.edit_hardware_types_view
    DO INSTEAD
(UPDATE hardware_type SET name = new.name, subnet_id = new.subnet_id
  WHERE (hardware_type.id = old.id));

-- View: public.edit_operating_systems_view

-- DROP VIEW public.edit_operating_systems_view;

CREATE OR REPLACE VIEW public.edit_operating_systems_view
 AS
 SELECT operating_system.id,
    operating_system.os,
    operating_system.nmap_identifies_as,
    operating_system.path_to_autoinstaller,
    operating_system.default_key
   FROM operating_system;

ALTER TABLE public.edit_operating_systems_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.edit_operating_systems_view TO cen1001;


-- Rule: edit_operating_systems_del ON public.edit_operating_systems_view

-- DROP Rule edit_operating_systems_del ON public.edit_operating_systems_view;

CREATE OR REPLACE RULE edit_operating_systems_del AS
    ON DELETE TO public.edit_operating_systems_view
    DO INSTEAD
(DELETE FROM operating_system
  WHERE (operating_system.id = old.id));

-- Rule: edit_operating_systems_ins ON public.edit_operating_systems_view

-- DROP Rule edit_operating_systems_ins ON public.edit_operating_systems_view;

CREATE OR REPLACE RULE edit_operating_systems_ins AS
    ON INSERT TO public.edit_operating_systems_view
    DO INSTEAD
(INSERT INTO operating_system (os, nmap_identifies_as, path_to_autoinstaller, default_key)
  VALUES (new.os, new.nmap_identifies_as, new.path_to_autoinstaller, new.default_key)
  RETURNING operating_system.id,
    operating_system.os,
    operating_system.nmap_identifies_as,
    operating_system.path_to_autoinstaller,
    operating_system.default_key);

-- Rule: edit_operating_systems_upd ON public.edit_operating_systems_view

-- DROP Rule edit_operating_systems_upd ON public.edit_operating_systems_view;

CREATE OR REPLACE RULE edit_operating_systems_upd AS
    ON UPDATE TO public.edit_operating_systems_view
    DO INSTEAD
(UPDATE operating_system SET os = new.os, nmap_identifies_as = new.nmap_identifies_as, path_to_autoinstaller = new.path_to_autoinstaller, default_key = new.default_key
  WHERE (operating_system.id = old.id));

-- View: public.group_computers_view

-- DROP VIEW public.group_computers_view;

CREATE OR REPLACE VIEW public.group_computers_view
 AS
 SELECT system_image.id,
    hardware.id AS _hardware_id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ip_address_hid.ip_address_hid AS ro_mm_ip_address,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid USING (ip_address_id)
     JOIN mm_research_group_computer_rep USING (research_group_id)
     JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
     LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE public.group_computers_view
    OWNER TO postgres;

GRANT SELECT, UPDATE, DELETE, REFERENCES, TRIGGER ON TABLE public.group_computers_view TO groupitreps;
GRANT SELECT ON TABLE public.group_computers_view TO headsofgroup;
GRANT ALL ON TABLE public.group_computers_view TO postgres;


-- Rule: group_computers_view_del ON public.group_computers_view

-- DROP Rule group_computers_view_del ON public.group_computers_view;

CREATE OR REPLACE RULE group_computers_view_del AS
    ON DELETE TO public.group_computers_view
    DO INSTEAD
(DELETE FROM system_image
  WHERE (system_image.id = old.id));

-- Rule: group_computers_view_upd ON public.group_computers_view

-- DROP Rule group_computers_view_upd ON public.group_computers_view;

CREATE OR REPLACE RULE group_computers_view_upd AS
    ON UPDATE TO public.group_computers_view
    DO INSTEAD
( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET user_id = new.user_id, research_group_id = new.research_group_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id
  WHERE (system_image.id = old.id);
);


-- View: public.group_fileservers_report

-- DROP VIEW public.group_fileservers_report;

CREATE OR REPLACE VIEW public.group_fileservers_report
 AS
 SELECT (group_fileserver.id || '-'::text) || COALESCE(research_group.id::text, ''::text) AS id,
    research_group.name,
    group_fileserver.hostname_for_users,
    group_fileserver.system_image_id,
    group_fileserver.localhomepath,
    group_fileserver.homepath
   FROM group_fileserver
     LEFT JOIN research_group ON research_group.group_fileserver_id = group_fileserver.id;

ALTER TABLE public.group_fileservers_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.group_fileservers_report TO cen1001;
GRANT SELECT ON TABLE public.group_fileservers_report TO cos;
GRANT SELECT ON TABLE public.group_fileservers_report TO mgmt_ro;

-- View: public.group_fileservers_view

-- DROP VIEW public.group_fileservers_view;

CREATE OR REPLACE VIEW public.group_fileservers_view
 AS
 SELECT group_fileserver.id,
    group_fileserver.hostname_for_users,
    group_fileserver.system_image_id,
    group_fileserver.homepath,
    group_fileserver.localhomepath,
    group_fileserver.profilepath,
    group_fileserver.localprofilepath,
    operating_system.os_class_id AS ro_os_class_id
   FROM group_fileserver
     JOIN system_image ON group_fileserver.system_image_id = system_image.id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN os_class_hid USING (os_class_id);

ALTER TABLE public.group_fileservers_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.group_fileservers_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.group_fileservers_view TO cos;


-- Rule: group_fileservers_del ON public.group_fileservers_view

-- DROP Rule group_fileservers_del ON public.group_fileservers_view;

CREATE OR REPLACE RULE group_fileservers_del AS
    ON DELETE TO public.group_fileservers_view
    DO INSTEAD
(DELETE FROM group_fileserver
  WHERE (group_fileserver.id = old.id));

-- Rule: group_fileservers_ins ON public.group_fileservers_view

-- DROP Rule group_fileservers_ins ON public.group_fileservers_view;

CREATE OR REPLACE RULE group_fileservers_ins AS
    ON INSERT TO public.group_fileservers_view
    DO INSTEAD
(INSERT INTO group_fileserver (hostname_for_users, system_image_id, homepath, localhomepath, profilepath, localprofilepath)
  VALUES (new.hostname_for_users, new.system_image_id, new.homepath, new.localhomepath, new.profilepath, new.localprofilepath)
  RETURNING group_fileserver.id,
    group_fileserver.hostname_for_users,
    group_fileserver.system_image_id,
    group_fileserver.homepath,
    group_fileserver.localhomepath,
    group_fileserver.profilepath,
    group_fileserver.localprofilepath,
    NULL::bigint AS int8);

-- Rule: group_fileservers_upd ON public.group_fileservers_view

-- DROP Rule group_fileservers_upd ON public.group_fileservers_view;

CREATE OR REPLACE RULE group_fileservers_upd AS
    ON UPDATE TO public.group_fileservers_view
    DO INSTEAD
(UPDATE group_fileserver SET hostname_for_users = new.hostname_for_users, system_image_id = new.system_image_id, homepath = new.homepath, localhomepath = new.localhomepath, profilepath = new.profilepath, localprofilepath = new.localprofilepath
  WHERE (group_fileserver.id = old.id));

-- View: public.hardware_basic_view

-- DROP VIEW public.hardware_basic_view;

CREATE OR REPLACE VIEW public.hardware_basic_view
 AS
 SELECT hardware.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name,
    hardware.room_id,
    hardware.hardware_type_id,
    hardware.owner_id AS person_id
   FROM hardware;

ALTER TABLE public.hardware_basic_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.hardware_basic_view TO cen1001;


-- Rule: hardware_basic_del ON public.hardware_basic_view

-- DROP Rule hardware_basic_del ON public.hardware_basic_view;

CREATE OR REPLACE RULE hardware_basic_del AS
    ON DELETE TO public.hardware_basic_view
    DO INSTEAD
(DELETE FROM hardware
  WHERE (hardware.id = old.id));

-- Rule: hardware_basic_ins ON public.hardware_basic_view

-- DROP Rule hardware_basic_ins ON public.hardware_basic_view;

CREATE OR REPLACE RULE hardware_basic_ins AS
    ON INSERT TO public.hardware_basic_view
    DO INSTEAD
(INSERT INTO hardware (manufacturer, model, name, room_id, hardware_type_id, owner_id)
  VALUES (new.manufacturer, new.model, new.name, new.room_id, new.hardware_type_id, new.person_id)
  RETURNING hardware.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name,
    hardware.room_id,
    hardware.hardware_type_id,
    hardware.owner_id);

-- Rule: hardware_basic_upd ON public.hardware_basic_view

-- DROP Rule hardware_basic_upd ON public.hardware_basic_view;

CREATE OR REPLACE RULE hardware_basic_upd AS
    ON UPDATE TO public.hardware_basic_view
    DO INSTEAD
(UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, room_id = new.room_id, hardware_type_id = new.hardware_type_id, owner_id = new.person_id
  WHERE (hardware.id = old.id));

-- View: public.hardware_full_view

-- DROP VIEW public.hardware_full_view;

CREATE OR REPLACE VIEW public.hardware_full_view
 AS
 SELECT hardware.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.asset_tag,
    hardware.processor_type,
    hardware.processor_speed,
    hardware.number_of_disks,
    hardware.value_when_new,
    hardware.date_purchased,
    hardware.date_configured,
    hardware.date_decommissioned,
    hardware.delete_after_date,
    hardware.warranty_end_date,
    hardware.warranty_details,
    hardware.room_id,
    hardware.hardware_type_id,
    hardware.owner_id AS person_id
   FROM hardware;

ALTER TABLE public.hardware_full_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.hardware_full_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.hardware_full_view TO cos;


-- Rule: hardware_full_del ON public.hardware_full_view

-- DROP Rule hardware_full_del ON public.hardware_full_view;

CREATE OR REPLACE RULE hardware_full_del AS
    ON DELETE TO public.hardware_full_view
    DO INSTEAD
(DELETE FROM hardware
  WHERE (hardware.id = old.id));

-- Rule: hardware_full_ins ON public.hardware_full_view

-- DROP Rule hardware_full_ins ON public.hardware_full_view;

CREATE OR REPLACE RULE hardware_full_ins AS
    ON INSERT TO public.hardware_full_view
    DO INSTEAD
(INSERT INTO hardware (manufacturer, model, name, serial_number, monitor_serial_number, asset_tag, processor_type, processor_speed, number_of_disks, value_when_new, date_purchased, date_configured, date_decommissioned, delete_after_date, warranty_end_date, warranty_details, room_id, hardware_type_id, owner_id)
  VALUES (new.manufacturer, new.model, new.name, new.serial_number, new.monitor_serial_number, new.asset_tag, new.processor_type, new.processor_speed, new.number_of_disks, new.value_when_new, new.date_purchased, new.date_configured, new.date_decommissioned, new.delete_after_date, new.warranty_end_date, new.warranty_details, new.room_id, new.hardware_type_id, new.person_id)
  RETURNING hardware.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.asset_tag,
    hardware.processor_type,
    hardware.processor_speed,
    hardware.number_of_disks,
    hardware.value_when_new,
    hardware.date_purchased,
    hardware.date_configured,
    hardware.date_decommissioned,
    hardware.delete_after_date,
    hardware.warranty_end_date,
    hardware.warranty_details,
    hardware.room_id,
    hardware.hardware_type_id,
    hardware.owner_id);

-- Rule: hardware_full_upd ON public.hardware_full_view

-- DROP Rule hardware_full_upd ON public.hardware_full_view;

CREATE OR REPLACE RULE hardware_full_upd AS
    ON UPDATE TO public.hardware_full_view
    DO INSTEAD
(UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, processor_type = new.processor_type, processor_speed = new.processor_speed, number_of_disks = new.number_of_disks, value_when_new = new.value_when_new, date_purchased = new.date_purchased, date_configured = new.date_configured, date_decommissioned = new.date_decommissioned, delete_after_date = new.delete_after_date, warranty_end_date = new.warranty_end_date, warranty_details = new.warranty_details, room_id = new.room_id, hardware_type_id = new.hardware_type_id, owner_id = new.person_id, asset_tag = new.asset_tag
  WHERE (hardware.id = old.id));

-- View: public.hardware_support_report

-- DROP VIEW public.hardware_support_report;

CREATE OR REPLACE VIEW public.hardware_support_report
 AS
 SELECT hardware.id,
    hardware.name,
    hardware.hardware_type_id,
    hardware.manufacturer,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.model,
    hardware.owner_id,
    person.crsid,
    ARRAY( SELECT DISTINCT research_group.name
           FROM research_group
             JOIN system_image ON research_group.id = system_image.research_group_id
             JOIN hardware h2 ON h2.id = system_image.hardware_id
          WHERE h2.id = hardware.id
          ORDER BY research_group.name) AS rgs,
    hardware.date_purchased,
    age(hardware.date_purchased::timestamp with time zone) AS age,
    ARRAY( SELECT DISTINCT operating_system.os
           FROM hardware h3
             JOIN system_image ON system_image.hardware_id = h3.id
             JOIN operating_system ON operating_system.id = system_image.operating_system_id
          WHERE h3.id = hardware.id
          ORDER BY operating_system.os) AS oses,
    hardware.warranty_end_date
   FROM hardware
     JOIN person ON hardware.owner_id = person.id;

ALTER TABLE public.hardware_support_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.hardware_support_report TO cen1001;
GRANT SELECT ON TABLE public.hardware_support_report TO cos;


-- View: public.hardware_system_image_subview

-- DROP VIEW public.hardware_system_image_subview;

CREATE OR REPLACE VIEW public.hardware_system_image_subview
 AS
 SELECT system_image.id,
    system_image.hardware_id,
    system_image.operating_system_id,
    mm_system_image_ip_address.ip_address_id AS mm_ip_address_id,
    ip_address_hid.ip_address_hid AS mm_ip_address,
    system_image.wired_mac_1,
    system_image.wireless_mac,
    'system_image_all_view'::text AS _targetview
   FROM system_image
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid ON ip_address_hid.ip_address_id = mm_system_image_ip_address.ip_address_id;

ALTER TABLE public.hardware_system_image_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.hardware_system_image_subview TO cen1001;
GRANT SELECT ON TABLE public.hardware_system_image_subview TO cos;

-- View: public.ip_address_pick

-- DROP VIEW public.ip_address_pick;

CREATE OR REPLACE VIEW public.ip_address_pick
 AS
 SELECT ip_address.id AS ip_address_id,
    COALESCE(((ip_address.hostname::text || ' ('::text) || ip_address.ip) || ')'::text, ip_address.ip::text) AS ip_address_hid
   FROM ip_address
  WHERE NOT ip_address.reserved;

ALTER TABLE public.ip_address_pick
    OWNER TO cen1001;

GRANT ALL ON TABLE public.ip_address_pick TO cen1001;
GRANT SELECT ON TABLE public.ip_address_pick TO ro_hid;


-- View: public.ips_without_hardware_report

-- DROP VIEW public.ips_without_hardware_report;

CREATE OR REPLACE VIEW public.ips_without_hardware_report
 AS
 SELECT ip_address.id,
    ip_address.ip,
    ip_address.hostname
   FROM ip_address
     LEFT JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
  WHERE mm_system_image_ip_address.system_image_id IS NULL
  ORDER BY ip_address.ip;

ALTER TABLE public.ips_without_hardware_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.ips_without_hardware_report TO cen1001;


-- Rule: ips_without_hardware_upd ON public.ips_without_hardware_report

-- DROP Rule ips_without_hardware_upd ON public.ips_without_hardware_report;

CREATE OR REPLACE RULE ips_without_hardware_upd AS
    ON UPDATE TO public.ips_without_hardware_report
    DO INSTEAD
(UPDATE ip_address SET ip = new.ip, hostname = new.hostname
  WHERE (ip_address.id = old.id)
  RETURNING ip_address.id,
    ip_address.ip,
    ip_address.hostname);

-- View: public.ips_without_hostnames_view

-- DROP VIEW public.ips_without_hostnames_view;

CREATE OR REPLACE VIEW public.ips_without_hostnames_view
 AS
 SELECT ip_address.id,
    ip_address.ip,
    ip_address.hostname
   FROM ip_address
  WHERE (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text) AND NOT ip_address.reserved
  ORDER BY ip_address.ip;

ALTER TABLE public.ips_without_hostnames_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.ips_without_hostnames_view TO cen1001;


-- Rule: ips_without_hostnames_upd ON public.ips_without_hostnames_view

-- DROP Rule ips_without_hostnames_upd ON public.ips_without_hostnames_view;

CREATE OR REPLACE RULE ips_without_hostnames_upd AS
    ON UPDATE TO public.ips_without_hostnames_view
    DO INSTEAD
(UPDATE ip_address SET ip = new.ip, hostname = new.hostname
  WHERE (ip_address.id = old.id)
  RETURNING ip_address.id,
    ip_address.ip,
    ip_address.hostname);

-- View: public.patch_panel_cabinet_subview

-- DROP VIEW public.patch_panel_cabinet_subview;

CREATE OR REPLACE VIEW public.patch_panel_cabinet_subview
 AS
 SELECT cabinet.id,
    patch_panel.id AS patch_panel_id,
    cabinet.name,
    cabinet.location,
    'cabinet_view'::text AS _targetview
   FROM cabinet
     LEFT JOIN patch_panel ON patch_panel.cabinet_id = cabinet.id;

ALTER TABLE public.patch_panel_cabinet_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.patch_panel_cabinet_subview TO cen1001;


-- View: public.patch_panel_socket_subview

-- DROP VIEW public.patch_panel_socket_subview;

CREATE OR REPLACE VIEW public.patch_panel_socket_subview
 AS
 SELECT socket.id,
    socket.patch_panel_id,
    socket.panel_label,
    socket.room_label,
    socket.room_id,
    socket.cable_type_id,
    socket.connector_type_id,
    'socket_view'::text AS _targetview
   FROM socket
  ORDER BY socket.panel_label;

ALTER TABLE public.patch_panel_socket_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.patch_panel_socket_subview TO cen1001;


-- View: public.patch_panel_view

-- DROP VIEW public.patch_panel_view;

CREATE OR REPLACE VIEW public.patch_panel_view
 AS
 SELECT patch_panel.id,
    patch_panel.name,
    patch_panel.cabinet_id,
    patch_panel.patch_panel_type_id
   FROM patch_panel;

ALTER TABLE public.patch_panel_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.patch_panel_view TO cen1001;


-- Rule: patch_panel_del ON public.patch_panel_view

-- DROP Rule patch_panel_del ON public.patch_panel_view;

CREATE OR REPLACE RULE patch_panel_del AS
    ON DELETE TO public.patch_panel_view
    DO INSTEAD
(DELETE FROM patch_panel
  WHERE (patch_panel.id = old.id));

-- Rule: patch_panel_ins ON public.patch_panel_view

-- DROP Rule patch_panel_ins ON public.patch_panel_view;

CREATE OR REPLACE RULE patch_panel_ins AS
    ON INSERT TO public.patch_panel_view
    DO INSTEAD
(INSERT INTO patch_panel (id, name, cabinet_id, patch_panel_type_id)
  VALUES (nextval('patch_panel_id_seq'::regclass), new.name, new.cabinet_id, new.patch_panel_type_id)
  RETURNING patch_panel.id,
    patch_panel.name,
    patch_panel.cabinet_id,
    patch_panel.patch_panel_type_id);

-- Rule: patch_panel_upd ON public.patch_panel_view

-- DROP Rule patch_panel_upd ON public.patch_panel_view;

CREATE OR REPLACE RULE patch_panel_upd AS
    ON UPDATE TO public.patch_panel_view
    DO INSTEAD
(UPDATE patch_panel SET name = new.name, cabinet_id = new.cabinet_id, patch_panel_type_id = new.patch_panel_type_id
  WHERE (patch_panel.id = old.id));

-- View: public.people_without_staff_reviews_report

-- DROP VIEW public.people_without_staff_reviews_report;

CREATE OR REPLACE VIEW public.people_without_staff_reviews_report
 AS
 SELECT person.id,
    person.id AS person_id,
    lr.supervisor_id,
    person.usual_reviewer_id,
    lr.post_category
   FROM person
     LEFT JOIN staff_review_meeting ON staff_review_meeting.person_id = person.id
     JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN cache._latest_role lr ON lr.person_id = person.id
  WHERE staff_review_meeting.id IS NULL AND lr.role_tablename = 'post_history'::text AND ps.status_id::text = 'Current'::text;

ALTER TABLE public.people_without_staff_reviews_report
    OWNER TO dev;

GRANT ALL ON TABLE public.people_without_staff_reviews_report TO cen1001;
GRANT ALL ON TABLE public.people_without_staff_reviews_report TO dev;
GRANT SELECT ON TABLE public.people_without_staff_reviews_report TO hr;
GRANT SELECT ON TABLE public.people_without_staff_reviews_report TO mgmt_ro;


-- View: public.personnel_subview

-- DROP VIEW public.personnel_subview;

CREATE OR REPLACE VIEW public.personnel_subview
 AS
 SELECT person.id,
    person.id AS person_id,
    'personnel_phone_view'::text AS _target_view
   FROM person;

ALTER TABLE public.personnel_subview
    OWNER TO cen1001;

GRANT SELECT ON TABLE public.personnel_subview TO accounts;
GRANT ALL ON TABLE public.personnel_subview TO cen1001;
GRANT SELECT ON TABLE public.personnel_subview TO hr;
GRANT SELECT ON TABLE public.personnel_subview TO mgmt_ro;
GRANT SELECT ON TABLE public.personnel_subview TO reception;
GRANT SELECT ON TABLE public.personnel_subview TO student_management;

-- View: public.printer_system_image_subview

-- DROP VIEW public.printer_system_image_subview;

CREATE OR REPLACE VIEW public.printer_system_image_subview
 AS
 SELECT system_image.id,
    printer.id AS printer_id,
    system_image.operating_system_id,
    mm_system_image_ip_address.ip_address_id AS mm_ip_address_id,
    ip_address_hid.ip_address_hid AS mm_ip_address,
    system_image.wired_mac_1,
    system_image.wireless_mac,
    'system_image_all_view'::text AS _targetview
   FROM system_image
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid ON ip_address_hid.ip_address_id = mm_system_image_ip_address.ip_address_id
     LEFT JOIN hardware ON system_image.hardware_id = hardware.id
     JOIN printer ON printer.hardware_id = hardware.id;

ALTER TABLE public.printer_system_image_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.printer_system_image_subview TO cen1001;

-- View: public.printers_view

-- DROP VIEW public.printers_view;

CREATE OR REPLACE VIEW public.printers_view
 AS
 SELECT printer.id,
    hardware.id AS _hardware_id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.date_purchased,
    hardware.serial_number,
    hardware.asset_tag,
    hardware.log_usage,
    hardware.usage_url,
    hardware.date_configured,
    hardware.warranty_end_date,
    hardware.date_decommissioned,
    hardware.warranty_details,
    hardware.room_id,
    hardware.owner_id,
    printer.printer_class_id,
    printer.cups_options
   FROM printer
     JOIN hardware ON printer.hardware_id = hardware.id;

ALTER TABLE public.printers_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.printers_view TO cen1001;
GRANT ALL ON TABLE public.printers_view TO dev;


-- Rule: printers_del ON public.printers_view

-- DROP Rule printers_del ON public.printers_view;

CREATE OR REPLACE RULE printers_del AS
    ON DELETE TO public.printers_view
    DO INSTEAD
( DELETE FROM printer
  WHERE (printer.id = old.id);
 DELETE FROM hardware
  WHERE (hardware.id = old._hardware_id);
);

-- Rule: printers_upd ON public.printers_view

-- DROP Rule printers_upd ON public.printers_view;

CREATE OR REPLACE RULE printers_upd AS
    ON UPDATE TO public.printers_view
    DO INSTEAD
( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, date_purchased = new.date_purchased, serial_number = new.serial_number, asset_tag = new.asset_tag, log_usage = new.log_usage, usage_url = new.usage_url, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id
  WHERE (hardware.id = old._hardware_id);
 UPDATE printer SET printer_class_id = new.printer_class_id, cups_options = new.cups_options
  WHERE (printer.id = old.id);
);

-- View: public.registration_and_leaving_view

-- DROP VIEW public.registration_and_leaving_view;

CREATE OR REPLACE VIEW public.registration_and_leaving_view
 AS
 SELECT person.id,
    person_hid.person_hid AS ro_person,
    person.arrival_date AS date_starting,
    person.registration_completed,
    person.leaving_date AS date_left,
    person.clearance_cert_signed,
    person.surname AS _surname,
    person.first_names AS _first_names
   FROM person
     LEFT JOIN person_hid ON person.id = person_hid.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.registration_and_leaving_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.registration_and_leaving_view TO cen1001;
GRANT ALL ON TABLE public.registration_and_leaving_view TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.registration_and_leaving_view TO hr;
GRANT SELECT ON TABLE public.registration_and_leaving_view TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.registration_and_leaving_view TO reception;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.registration_and_leaving_view TO student_management;


-- Rule: registration_and_leaving_upd ON public.registration_and_leaving_view

-- DROP Rule registration_and_leaving_upd ON public.registration_and_leaving_view;

CREATE OR REPLACE RULE registration_and_leaving_upd AS
    ON UPDATE TO public.registration_and_leaving_view
    DO INSTEAD
(UPDATE person SET arrival_date = new.date_starting, leaving_date = new.date_left, registration_completed = new.registration_completed, clearance_cert_signed = new.clearance_cert_signed
  WHERE (person.id = old.id));

-- View: public.research_group_editing_view

-- DROP VIEW public.research_group_editing_view;

CREATE OR REPLACE VIEW public.research_group_editing_view
 AS
 SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    person_hid.person_hid AS mm_person,
    mm_person_research_group.person_id AS mm_person_id,
    computer_rep_hid.computer_rep_hid AS mm_computer_rep,
    mm_research_group_computer_rep.computer_rep_id AS mm_computer_rep_id,
    research_group.website_address,
    research_group.sector_id
   FROM research_group
     LEFT JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     LEFT JOIN person_hid USING (person_id)
     LEFT JOIN mm_research_group_computer_rep ON research_group.id = mm_research_group_computer_rep.research_group_id
     LEFT JOIN computer_rep_hid ON computer_rep_hid.computer_rep_id = mm_research_group_computer_rep.computer_rep_id
  ORDER BY research_group.name;

ALTER TABLE public.research_group_editing_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.research_group_editing_view TO cen1001;
GRANT ALL ON TABLE public.research_group_editing_view TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.research_group_editing_view TO hr;
GRANT SELECT ON TABLE public.research_group_editing_view TO mgmt_ro;
GRANT SELECT ON TABLE public.research_group_editing_view TO safety_management;

-- FUNCTION: public.research_group_ins(research_group_editing_view)

-- DROP FUNCTION public.research_group_ins(research_group_editing_view);

CREATE OR REPLACE FUNCTION public.research_group_ins(
	research_group_editing_view)
    RETURNS bigint
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

declare 

r alias for $1;
r_id bigint;

begin

r_id = nextval('research_group_id_seq');

insert into research_group (id, name, head_of_group_id, administrator_id, website_address, sector_id ) values (r_id, r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id );

perform fn_upd_many_many (r_id, r.mm_person, 'research_group','person') ;
perform fn_upd_many_many (r_id, r.mm_computer_rep, 'research_group','computer_rep') ;

return r_id;

end;

$BODY$;

ALTER FUNCTION public.research_group_ins(research_group_editing_view)
    OWNER TO cen1001;


-- Rule: research_group_editing_del ON public.research_group_editing_view

-- DROP Rule research_group_editing_del ON public.research_group_editing_view;

CREATE OR REPLACE RULE research_group_editing_del AS
    ON DELETE TO public.research_group_editing_view
    DO INSTEAD
(DELETE FROM research_group
  WHERE (research_group.id = old.id));

-- Rule: research_group_editing_ins ON public.research_group_editing_view

-- DROP Rule research_group_editing_ins ON public.research_group_editing_view;

CREATE OR REPLACE RULE research_group_editing_ins AS
    ON INSERT TO public.research_group_editing_view
    DO INSTEAD
(SELECT research_group_ins(new.*) AS id);

-- Rule: research_group_editing_upd ON public.research_group_editing_view

-- DROP Rule research_group_editing_upd ON public.research_group_editing_view;

CREATE OR REPLACE RULE research_group_editing_upd AS
    ON UPDATE TO public.research_group_editing_view
    DO INSTEAD
( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, sector_id = new.sector_id
  WHERE (research_group.id = old.id);
 SELECT fn_upd_many_many(old.id, new.mm_person, 'research_group'::text, 'person'::text) AS fn_upd_many_many;
 SELECT fn_upd_many_many(old.id, new.mm_computer_rep, 'research_group'::text, 'computer_rep'::text) AS fn_upd_many_many;
);

-- View: public.research_groups_computing_computers_subview

-- DROP VIEW public.research_groups_computing_computers_subview;

CREATE OR REPLACE VIEW public.research_groups_computing_computers_subview
 AS
 SELECT system_image.id,
    research_group.id AS research_group_id,
    system_image.id AS system_image_id,
    hardware.manufacturer,
    hardware.model,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    hardware.asset_tag,
    hardware.serial_number,
    operating_system.os,
    ARRAY( SELECT ((COALESCE(ip_address.hostname, ''::character varying)::text || ' ('::text) || ip_address.ip) || ')'::text
           FROM ip_address
             JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
             JOIN system_image s2 ON mm_system_image_ip_address.system_image_id = s2.id
          WHERE s2.id = system_image.id) AS ip_addresses,
    'system_image_all_view'::text AS _targetview
   FROM system_image
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id;

ALTER TABLE public.research_groups_computing_computers_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.research_groups_computing_computers_subview TO cen1001;
GRANT SELECT ON TABLE public.research_groups_computing_computers_subview TO cos;

-- View: public.research_groups_computing_view

-- DROP VIEW public.research_groups_computing_view;

CREATE OR REPLACE VIEW public.research_groups_computing_view
 AS
 SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    person_hid.person_hid AS mm_person,
    mm_person_research_group.person_id AS mm_person_id,
    computer_rep_hid.computer_rep_hid AS mm_computer_rep,
    mm_research_group_computer_rep.computer_rep_id AS mm_computer_rep_id,
    research_group.website_address,
    research_group.sector_id,
    research_group.fai_class_id,
    research_group.active_directory_container,
    research_group.internal_use_only,
    research_group.subnet_id,
    research_group.group_fileserver_id
   FROM research_group
     LEFT JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     LEFT JOIN person_hid USING (person_id)
     LEFT JOIN mm_research_group_computer_rep ON research_group.id = mm_research_group_computer_rep.research_group_id
     LEFT JOIN computer_rep_hid ON computer_rep_hid.computer_rep_id = mm_research_group_computer_rep.computer_rep_id
  ORDER BY research_group.name;

ALTER TABLE public.research_groups_computing_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.research_groups_computing_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.research_groups_computing_view TO cos;
GRANT ALL ON TABLE public.research_groups_computing_view TO dev;


-- Rule: research_groups__computing_upd ON public.research_groups_computing_view

-- DROP Rule research_groups__computing_upd ON public.research_groups_computing_view;

CREATE OR REPLACE RULE research_groups__computing_upd AS
    ON UPDATE TO public.research_groups_computing_view
    DO INSTEAD
( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, fai_class_id = new.fai_class_id, active_directory_container = new.active_directory_container, website_address = new.website_address, internal_use_only = new.internal_use_only, sector_id = new.sector_id, subnet_id = new.subnet_id, group_fileserver_id = new.group_fileserver_id
  WHERE (research_group.id = old.id);
 SELECT fn_upd_many_many(old.id, new.mm_person, 'research_group'::text, 'person'::text) AS fn_upd_many_many;
 SELECT fn_upd_many_many(old.id, new.mm_computer_rep, 'research_group'::text, 'computer_rep'::text) AS fn_upd_many_many;
);

CREATE OR REPLACE FUNCTION public.research_groups_computing_upd(
	research_groups_computing_view)
    RETURNS bigint
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

declare 

r alias for $1;
r_id bigint;

begin

r_id = nextval('research_group_id_seq');

insert into research_group (id, name, head_of_group_id, administrator_id, website_address, sector_id, fai_class_id, active_directory_container, internal_use_only, subnet_id, group_fileserver_id ) values (r_id, r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id, r.fai_class_id, r.active_directory_container, r.internal_use_only, r.subnet_id, r.group_fileserver_id );

perform fn_upd_many_many (r_id, r.mm_person, 'research_group','person') ;
perform fn_upd_many_many (r_id, r.mm_computer_rep, 'research_group','computer_rep') ;

return r_id;

end;

$BODY$;

ALTER FUNCTION public.research_groups_computing_upd(research_groups_computing_view)
    OWNER TO cen1001;


-- Rule: research_groups_computing_del ON public.research_groups_computing_view

-- DROP Rule research_groups_computing_del ON public.research_groups_computing_view;

CREATE OR REPLACE RULE research_groups_computing_del AS
    ON DELETE TO public.research_groups_computing_view
    DO INSTEAD
(DELETE FROM research_group
  WHERE (research_group.id = old.id));

-- Rule: research_groups_computing_ins ON public.research_groups_computing_view

-- DROP Rule research_groups_computing_ins ON public.research_groups_computing_view;

CREATE OR REPLACE RULE research_groups_computing_ins AS
    ON INSERT TO public.research_groups_computing_view
    DO INSTEAD
(SELECT research_groups_computing_upd(new.*) AS id);


-- View: public.room_attributes_view

-- DROP VIEW public.room_attributes_view;

CREATE OR REPLACE VIEW public.room_attributes_view
 AS
 SELECT room.id,
    room.name,
    room.building_id,
    room.building_region_id,
    room.building_floor_id,
    room.area,
    room.room_type_id,
    room.number_of_desks,
    room.number_of_benches,
    room.embs_name,
    room.suggested_new_name,
    room.survey_date,
    room.survey_comments,
    room.survey_door_label,
    room.maximum_occupancy,
    room.last_refurbished_date,
    room.responsible_group_id,
    research_group.head_of_group_id AS ro_responsible_person_id,
    room.comments
   FROM room
     LEFT JOIN research_group ON room.responsible_group_id = research_group.id
  ORDER BY room.name;

ALTER TABLE public.room_attributes_view
    OWNER TO postgres;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.room_attributes_view TO dev;
GRANT SELECT ON TABLE public.room_attributes_view TO mgmt_ro;
GRANT ALL ON TABLE public.room_attributes_view TO postgres;
GRANT SELECT ON TABLE public.room_attributes_view TO safety_management;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.room_attributes_view TO space_management;


-- Rule: room_attributes_del ON public.room_attributes_view

-- DROP Rule room_attributes_del ON public.room_attributes_view;

CREATE OR REPLACE RULE room_attributes_del AS
    ON DELETE TO public.room_attributes_view
    DO INSTEAD
(DELETE FROM room
  WHERE (room.id = old.id));

-- Rule: room_attributes_ins ON public.room_attributes_view

-- DROP Rule room_attributes_ins ON public.room_attributes_view;

CREATE OR REPLACE RULE room_attributes_ins AS
    ON INSERT TO public.room_attributes_view
    DO INSTEAD
(INSERT INTO room (name, building_id, building_region_id, building_floor_id, area, room_type_id, number_of_desks, number_of_benches, maximum_occupancy, last_refurbished_date, responsible_group_id, comments, embs_name, suggested_new_name, survey_date, survey_comments, survey_door_label)
  VALUES (new.name, new.building_id, new.building_region_id, new.building_floor_id, new.area, new.room_type_id, new.number_of_desks, new.number_of_benches, new.maximum_occupancy, new.last_refurbished_date, new.responsible_group_id, new.comments, new.embs_name, new.suggested_new_name, new.survey_date, new.survey_comments, new.survey_door_label)
  RETURNING room.id,
    room.name,
    room.building_id,
    room.building_region_id,
    room.building_floor_id,
    room.area,
    room.room_type_id,
    room.number_of_desks,
    room.number_of_benches,
    room.embs_name,
    room.suggested_new_name,
    room.survey_date,
    room.survey_comments,
    room.survey_door_label,
    room.maximum_occupancy,
    room.last_refurbished_date,
    room.responsible_group_id,
    NULL::bigint AS int8,
    room.comments);

-- Rule: room_attributes_upd ON public.room_attributes_view

-- DROP Rule room_attributes_upd ON public.room_attributes_view;

CREATE OR REPLACE RULE room_attributes_upd AS
    ON UPDATE TO public.room_attributes_view
    DO INSTEAD
(UPDATE room SET name = new.name, building_id = new.building_id, building_region_id = new.building_region_id, building_floor_id = new.building_floor_id, number_of_desks = new.number_of_desks, number_of_benches = new.number_of_benches, area = new.area, room_type_id = new.room_type_id, maximum_occupancy = new.maximum_occupancy, last_refurbished_date = new.last_refurbished_date, responsible_group_id = new.responsible_group_id, comments = new.comments, embs_name = new.embs_name, suggested_new_name = new.suggested_new_name, survey_date = new.survey_date, survey_comments = new.survey_comments, survey_door_label = new.survey_door_label
  WHERE (room.id = old.id));

-- View: public.rooms_with_embs_chnages_report

-- DROP VIEW public.rooms_with_embs_chnages_report;

CREATE OR REPLACE VIEW public.rooms_with_embs_chnages_report
 AS
 SELECT room.id,
    room.name,
    room.building_id,
    room.building_region_id,
    room.building_floor_id,
    room.area,
    room.room_type_id,
    room.number_of_desks,
    room.number_of_benches,
    room.embs_name,
    room.suggested_new_name,
    room.survey_date,
    room.survey_comments,
    room.survey_door_label,
    room.maximum_occupancy,
    room.last_refurbished_date,
    room.responsible_group_id,
    research_group.head_of_group_id AS ro_responsible_person_id,
    room.comments
   FROM room
     LEFT JOIN research_group ON room.responsible_group_id = research_group.id
  WHERE room.name::text <> room.embs_name::text
  ORDER BY room.name;

ALTER TABLE public.rooms_with_embs_chnages_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.rooms_with_embs_chnages_report TO cen1001;
GRANT SELECT ON TABLE public.rooms_with_embs_chnages_report TO dev;
GRANT SELECT ON TABLE public.rooms_with_embs_chnages_report TO space_management;

-- View: public.rooms_with_local_name_changes_report

-- DROP VIEW public.rooms_with_local_name_changes_report;

CREATE OR REPLACE VIEW public.rooms_with_local_name_changes_report
 AS
 SELECT room.id,
    room.name,
    room.building_id,
    room.building_region_id,
    room.building_floor_id,
    room.area,
    room.room_type_id,
    room.number_of_desks,
    room.number_of_benches,
    room.embs_name,
    room.suggested_new_name,
    room.survey_date,
    room.survey_comments,
    room.survey_door_label,
    room.maximum_occupancy,
    room.last_refurbished_date,
    room.responsible_group_id,
    research_group.head_of_group_id AS ro_responsible_person_id,
    room.comments
   FROM room
     LEFT JOIN research_group ON room.responsible_group_id = research_group.id
  WHERE room.name::text <> room.suggested_new_name::text
  ORDER BY room.name;

ALTER TABLE public.rooms_with_local_name_changes_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.rooms_with_local_name_changes_report TO cen1001;
GRANT SELECT ON TABLE public.rooms_with_local_name_changes_report TO cos;
GRANT SELECT ON TABLE public.rooms_with_local_name_changes_report TO dev;
GRANT SELECT ON TABLE public.rooms_with_local_name_changes_report TO space_management;

-- View: public.socket_patch_panel_subview

-- DROP VIEW public.socket_patch_panel_subview;

CREATE OR REPLACE VIEW public.socket_patch_panel_subview
 AS
 SELECT patch_panel.id,
    socket.id AS socket_id,
    patch_panel.name,
    'patch_panel_view'::text AS _targetview
   FROM patch_panel
     LEFT JOIN socket ON socket.patch_panel_id = patch_panel.id;

ALTER TABLE public.socket_patch_panel_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.socket_patch_panel_subview TO cen1001;

-- View: public.socket_view

-- DROP VIEW public.socket_view;

CREATE OR REPLACE VIEW public.socket_view
 AS
 SELECT socket.id,
    socket.room_label,
    socket.panel_label,
    socket.room_id,
    socket.patch_panel_id,
    socket.cable_type_id,
    socket.connector_type_id,
    socket.linked_socket_id,
    switchport.id AS switchport_id
   FROM socket
     LEFT JOIN switchport ON switchport.socket_id = socket.id
  ORDER BY socket.patch_panel_id, socket.panel_label;

ALTER TABLE public.socket_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.socket_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.socket_view TO cos;

-- FUNCTION: public.fn_socket_insert_or_update(socket_view)

-- DROP FUNCTION public.fn_socket_insert_or_update(socket_view);

CREATE OR REPLACE FUNCTION public.fn_socket_insert_or_update(
	socket_view)
    RETURNS bigint
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
        declare
                s alias for $1;
                s_id bigint;
        begin
                -- updating
                if s.id is not null then
                    s_id = s.id;
                    update socket set
                        room_label= s.room_label,
                        panel_label= s.panel_label,
                        room_id= s.room_id,
                        patch_panel_id= s.patch_panel_id,
                        connector_type_id= s.connector_type_id,
                        linked_socket_id= s.linked_socket_id
                    where socket.id = s_id;

                else
                        s_id := nextval('socket_id_seq');
                -- inserting
                        insert into socket (
                                id, 
                                room_label,
                                panel_label,
                                room_id,
                                patch_panel_id,
                                cable_type_id,
                                connector_type_id,
                                linked_socket_id
                        ) values (
                                s_id, 
                                s.room_label,
                                s.panel_label,
                                s.room_id,
                                s.patch_panel_id,
                                s.cable_type_id,
                                s.connector_type_id,
                                s.linked_socket_id
                        );
                end if;
                -- now do linked sockets and switchports
                if s.switchport_id is not null then
                            update switchport set
                            socket_id = s_id where switchport.id =
                            s.switchport_id;
                end if;
                if s.linked_socket_id is not null then
                        insert into _debug ( debug ) values ( 'update socket set linked_socket_id = ' || s_id );
                        update socket set linked_socket_id = s_id where
                        id = s.linked_socket_id;
                else
                        update socket set linked_socket_id = null where
                        linked_socket_id = s_id;
                end if;
        
                return s_id;
                
        end;

$BODY$;

ALTER FUNCTION public.fn_socket_insert_or_update(socket_view)
    OWNER TO cen1001;


-- Rule: socket_del ON public.socket_view

-- DROP Rule socket_del ON public.socket_view;

CREATE OR REPLACE RULE socket_del AS
    ON DELETE TO public.socket_view
    DO INSTEAD
(DELETE FROM socket
  WHERE (socket.id = old.id));

-- Rule: socket_ins ON public.socket_view

-- DROP Rule socket_ins ON public.socket_view;

CREATE OR REPLACE RULE socket_ins AS
    ON INSERT TO public.socket_view
    DO INSTEAD
(SELECT fn_socket_insert_or_update(new.*) AS id);

-- Rule: socket_upd ON public.socket_view

-- DROP Rule socket_upd ON public.socket_view;

CREATE OR REPLACE RULE socket_upd AS
    ON UPDATE TO public.socket_view
    DO INSTEAD
(SELECT fn_socket_insert_or_update(new.*) AS id);

-- View: public.staff_review_history_subview

-- DROP VIEW public.staff_review_history_subview;

CREATE OR REPLACE VIEW public.staff_review_history_subview
 AS
 SELECT staff_review_meeting.id,
    person.id AS person_id,
    staff_review_meeting.date_of_meeting,
    person.next_review_due,
    staff_review_meeting.reviewer_id,
    staff_review_meeting.notes,
    'staff_reviews_view'::text AS _targetview
   FROM staff_review_meeting
     JOIN person ON staff_review_meeting.person_id = person.id;

ALTER TABLE public.staff_review_history_subview
    OWNER TO dev;

GRANT ALL ON TABLE public.staff_review_history_subview TO cen1001;
GRANT ALL ON TABLE public.staff_review_history_subview TO dev;
GRANT SELECT ON TABLE public.staff_review_history_subview TO hr;
GRANT SELECT ON TABLE public.staff_review_history_subview TO reception;


-- View: public.staff_reviews_view

-- DROP VIEW public.staff_reviews_view;

CREATE OR REPLACE VIEW public.staff_reviews_view
 AS
 SELECT staff_review_meeting.id,
    staff_review_meeting.person_id,
    person.usual_reviewer_id,
    staff_review_meeting.date_of_meeting,
    staff_review_meeting.reviewer_id,
    person.next_review_due,
    staff_review_meeting.notes
   FROM staff_review_meeting
     JOIN person ON person.id = staff_review_meeting.person_id;

ALTER TABLE public.staff_reviews_view
    OWNER TO dev;

GRANT ALL ON TABLE public.staff_reviews_view TO cen1001;
GRANT ALL ON TABLE public.staff_reviews_view TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.staff_reviews_view TO hr;
GRANT SELECT ON TABLE public.staff_reviews_view TO mgmt_ro;


-- Rule: staff_reviews_del ON public.staff_reviews_view

-- DROP Rule staff_reviews_del ON public.staff_reviews_view;

CREATE OR REPLACE RULE staff_reviews_del AS
    ON DELETE TO public.staff_reviews_view
    DO INSTEAD
(DELETE FROM staff_review_meeting
  WHERE (staff_review_meeting.id = old.id));

-- Rule: staff_reviews_ins ON public.staff_reviews_view

-- DROP Rule staff_reviews_ins ON public.staff_reviews_view;

CREATE OR REPLACE RULE staff_reviews_ins AS
    ON INSERT TO public.staff_reviews_view
    DO INSTEAD
( UPDATE person SET next_review_due = new.next_review_due
  WHERE (person.id = new.person_id);
 INSERT INTO staff_review_meeting (person_id, date_of_meeting, reviewer_id, notes)
  VALUES (new.person_id, new.date_of_meeting, COALESCE(new.reviewer_id, ( SELECT person.usual_reviewer_id
           FROM person
          WHERE (person.id = new.person_id))), new.notes)
  RETURNING staff_review_meeting.id,
    NULL::bigint AS int8,
    NULL::bigint AS int8,
    staff_review_meeting.date_of_meeting,
    staff_review_meeting.reviewer_id,
    NULL::date AS date,
    staff_review_meeting.notes;
);

-- Rule: staff_reviews_upd ON public.staff_reviews_view

-- DROP Rule staff_reviews_upd ON public.staff_reviews_view;

CREATE OR REPLACE RULE staff_reviews_upd AS
    ON UPDATE TO public.staff_reviews_view
    DO INSTEAD
( UPDATE staff_review_meeting SET date_of_meeting = new.date_of_meeting, reviewer_id = COALESCE(new.reviewer_id, new.usual_reviewer_id), notes = new.notes
  WHERE (staff_review_meeting.id = old.id);
 UPDATE person SET next_review_due = new.next_review_due, usual_reviewer_id = new.usual_reviewer_id
  WHERE (person.id = old.person_id);
);

-- View: public.switch_switchport_subview

-- DROP VIEW public.switch_switchport_subview;

CREATE OR REPLACE VIEW public.switch_switchport_subview
 AS
 SELECT switchport.id,
    switch.id AS switch_id,
    switchport.name,
    switchport.socket_id,
    'switchport_view'::text AS _targetview
   FROM switchport
     JOIN switch ON switchport.switch_id = switch.id;

ALTER TABLE public.switch_switchport_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.switch_switchport_subview TO cen1001;
GRANT SELECT ON TABLE public.switch_switchport_subview TO cos;


-- View: public.switchport_switch_subview

-- DROP VIEW public.switchport_switch_subview;

CREATE OR REPLACE VIEW public.switchport_switch_subview
 AS
 SELECT switch.id,
    switchport.id AS switchport_id,
    hardware.name,
    'switch_view'::text AS _targetview
   FROM switch
     JOIN switchport ON switchport.switch_id = switch.id
     JOIN hardware ON switch.hardware_id = hardware.id;

ALTER TABLE public.switchport_switch_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.switchport_switch_subview TO cen1001;
GRANT SELECT ON TABLE public.switchport_switch_subview TO cos;


-- View: public.switchport_view

-- DROP VIEW public.switchport_view;

CREATE OR REPLACE VIEW public.switchport_view
 AS
 SELECT switchport.id,
    switchport.name,
    switchport.switch_id,
    switchport.socket_id
   FROM switchport;

ALTER TABLE public.switchport_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.switchport_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.switchport_view TO cos;


-- Rule: switchport_del ON public.switchport_view

-- DROP Rule switchport_del ON public.switchport_view;

CREATE OR REPLACE RULE switchport_del AS
    ON DELETE TO public.switchport_view
    DO INSTEAD
(DELETE FROM switchport
  WHERE (switchport.id = old.id));

-- Rule: switchport_ins ON public.switchport_view

-- DROP Rule switchport_ins ON public.switchport_view;

CREATE OR REPLACE RULE switchport_ins AS
    ON INSERT TO public.switchport_view
    DO INSTEAD
(INSERT INTO switchport (name, socket_id, switch_id)
  VALUES (new.name, new.socket_id, new.switch_id)
  RETURNING switchport.id,
    switchport.name,
    switchport.socket_id,
    switchport.switch_id);

-- Rule: switchport_upd ON public.switchport_view

-- DROP Rule switchport_upd ON public.switchport_view;

CREATE OR REPLACE RULE switchport_upd AS
    ON UPDATE TO public.switchport_view
    DO INSTEAD
(UPDATE switchport SET name = new.name, socket_id = new.socket_id, switch_id = new.switch_id
  WHERE (switchport.id = old.id));


-- View: public.system_image_all_view

-- DROP VIEW public.system_image_all_view;

CREATE OR REPLACE VIEW public.system_image_all_view
 AS
 SELECT system_image.id,
    system_image.hardware_id AS _hardware_id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    rtrim((((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text)) AS ro_all_mac_addresses,
    ip_address_hid.ip_address_hid AS mm_ip_address,
    mm_system_image_ip_address.ip_address_id AS mm_ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    system_image.hobbit_tier,
    system_image.hobbit_flags,
    system_image.is_development_system
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid USING (ip_address_id);

ALTER TABLE public.system_image_all_view
    OWNER TO dev;

GRANT ALL ON TABLE public.system_image_all_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.system_image_all_view TO cos;
GRANT ALL ON TABLE public.system_image_all_view TO dev;


-- FUNCTION: public.fn_system_image_all_upd(system_image_all_view)

-- DROP FUNCTION public.fn_system_image_all_upd(system_image_all_view);

CREATE OR REPLACE FUNCTION public.fn_system_image_all_upd(
	system_image_all_view)
    RETURNS bigint
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
 
  declare
          v alias for $1;
          v_system_image_id BIGINT;
          v_hardware_id BIGINT;
  begin
 
  IF v.id IS NOT NULL THEN
 
    v_system_image_id := v.id;
 
    UPDATE hardware SET
         manufacturer=v.manufacturer, 
         model=v.model, 
         name=v.hardware_name,
         hardware_type_id=v.hardware_type_id, 
         asset_tag = v.asset_tag,
         serial_number = v.serial_number,
         monitor_serial_number = v.monitor_serial_number,
         room_id=v.room_id,
         owner_id=v.owner_id,
         comments=v.hardware_comments
    WHERE hardware.id = v._hardware_id;
 
    UPDATE system_image SET
          user_id=v.user_id,
          research_group_id=v.research_group_id,
          operating_system_id = v.operating_system_id,
          wired_mac_1 = v.wired_mac_1, 
          wired_mac_2 = v.wired_mac_2,
          wireless_mac = v.wireless_mac,
          host_system_image_id = v.host_system_image_id,
          comments = v.system_image_comments,
          hobbit_tier = v.hobbit_tier,
          hobbit_flags = v.hobbit_flags,
          is_development_system = v.is_development_system
    WHERE system_image.id = v.id;
 
  ELSE
 
    v_system_image_id := nextval('system_image_id_seq');
    v_hardware_id := nextval('hardware_id_seq');
 
    INSERT INTO hardware
          (id, manufacturer, model, name, hardware_type_id, asset_tag, serial_number, monitor_serial_number, room_id, owner_id, comments )
    VALUES
          (v_hardware_id, v.manufacturer, v.model, v.hardware_name, v.hardware_type_id, v.asset_tag, v.serial_number, v.monitor_serial_number,
           v.room_id, v.owner_id, v.hardware_comments);
 
    INSERT INTO system_image
          (id, hardware_id, operating_system_id, wired_mac_1, wired_mac_2,
  wireless_mac, host_system_image_id,
  comments,user_id,research_group_id,hobbit_tier,hobbit_flags,is_development_system)
    VALUES
          (v_system_image_id, v_hardware_id, v.operating_system_id,
           v.wired_mac_1, v.wired_mac_2, v.wireless_mac,
           v.host_system_image_id,
           v.system_image_comments,v.user_id,v.research_group_id,v.hobbit_tier,v.hobbit_flags,v.is_development_system);
 
  END IF;
 
  PERFORM fn_upd_many_many (v_system_image_id, v.mm_ip_address, 'system_image',
                          'ip_address');
 
  return v_system_image_id;
 
  end;
 
$BODY$;

ALTER FUNCTION public.fn_system_image_all_upd(system_image_all_view)
    OWNER TO dev;



-- Rule: system_image_all_del ON public.system_image_all_view

-- DROP Rule system_image_all_del ON public.system_image_all_view;

CREATE OR REPLACE RULE system_image_all_del AS
    ON DELETE TO public.system_image_all_view
    DO INSTEAD
(DELETE FROM system_image
  WHERE (system_image.id = old.id));

-- Rule: system_image_all_ins ON public.system_image_all_view

-- DROP Rule system_image_all_ins ON public.system_image_all_view;

CREATE OR REPLACE RULE system_image_all_ins AS
    ON INSERT TO public.system_image_all_view
    DO INSTEAD
(SELECT fn_system_image_all_upd(new.*) AS id);

-- Rule: system_image_all_upd ON public.system_image_all_view

-- DROP Rule system_image_all_upd ON public.system_image_all_view;

CREATE OR REPLACE RULE system_image_all_upd AS
    ON UPDATE TO public.system_image_all_view
    DO INSTEAD
(SELECT fn_system_image_all_upd(new.*) AS id);

-- View: public.system_image_autoinstaller_view

-- DROP VIEW public.system_image_autoinstaller_view;

CREATE OR REPLACE VIEW public.system_image_autoinstaller_view
 AS
 SELECT system_image.id,
    system_image.hardware_id AS _hardware_id,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    system_image.architecture_id,
    ip_address_hid.ip_address_hid AS mm_ip_address,
    mm_system_image_ip_address.ip_address_id AS mm_ip_address_id,
    installer_tag_hid.installer_tag_hid AS mm_installer_tag,
    mm_system_image_installer_tag.installer_tag_id AS mm_installer_tag_id,
    system_image.reinstall_on_next_boot,
    reinstall_results.reinstall_results AS ro_reinstall_results,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments
   FROM system_image
     LEFT JOIN reinstall_results USING (id)
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN research_group ON system_image.research_group_id = research_group.id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN mm_system_image_installer_tag ON system_image.id = mm_system_image_installer_tag.system_image_id
     LEFT JOIN installer_tag_hid ON mm_system_image_installer_tag.installer_tag_id = installer_tag_hid.installer_tag_id
     LEFT JOIN ip_address_hid USING (ip_address_id);

ALTER TABLE public.system_image_autoinstaller_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.system_image_autoinstaller_view TO cen1001;
GRANT SELECT, UPDATE ON TABLE public.system_image_autoinstaller_view TO cos;
GRANT ALL ON TABLE public.system_image_autoinstaller_view TO dev;


-- Rule: system_image_all_upd ON public.system_image_autoinstaller_view

-- DROP Rule system_image_all_upd ON public.system_image_autoinstaller_view;

CREATE OR REPLACE RULE system_image_all_upd AS
    ON UPDATE TO public.system_image_autoinstaller_view
    DO INSTEAD
( SELECT fn_upd_many_many_perl(old.id, new.mm_ip_address, 'system_image'::text, 'ip_address'::text) AS fn_upd_many_many_perl;
 SELECT fn_upd_many_many_perl(old.id, (new.mm_installer_tag)::text, 'system_image'::text, 'installer_tag'::text) AS fn_upd_many_many_perl;
 UPDATE hardware SET name = new.hardware_name, hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments, research_group_id = new.research_group_id, user_id = new.user_id
  WHERE (system_image.id = old.id);
);

-- View: public.system_image_contact_details_subview

-- DROP VIEW public.system_image_contact_details_subview;

CREATE OR REPLACE VIEW public.system_image_contact_details_subview
 AS
 SELECT all_people.person_id || all_people.role::text AS id,
    system_image.id AS system_image_id,
    all_people.person_id,
    all_people.role,
    person.email_address,
    dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone,
    room_hid.room_hid AS mm_room,
    'personnel_basic_view'::text AS _targetview
   FROM system_image
     JOIN ( SELECT system_image_1.id,
            hardware.owner_id AS person_id,
            'Hardware owner'::character varying(20) AS role
           FROM system_image system_image_1
             JOIN hardware ON system_image_1.hardware_id = hardware.id
        UNION
         SELECT system_image_1.id,
            system_image_1.user_id AS person_id,
            'System user'::character varying(20) AS role
           FROM system_image system_image_1
          WHERE system_image_1.user_id IS NOT NULL
        UNION
         SELECT system_image_1.id,
            mm_research_group_computer_rep.computer_rep_id AS person_id,
            'Group computer rep'::character varying(20) AS role
           FROM system_image system_image_1
             JOIN research_group ON system_image_1.research_group_id = research_group.id
             JOIN mm_research_group_computer_rep ON mm_research_group_computer_rep.research_group_id = research_group.id) all_people USING (id)
     JOIN person ON all_people.person_id = person.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
     LEFT JOIN room_hid USING (room_id);

ALTER TABLE public.system_image_contact_details_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.system_image_contact_details_subview TO cen1001;
GRANT ALL ON TABLE public.system_image_contact_details_subview TO dev;


-- View: public.system_image_hardware_subview

-- DROP VIEW public.system_image_hardware_subview;

CREATE OR REPLACE VIEW public.system_image_hardware_subview
 AS
 SELECT hardware.id,
    system_image.id AS system_image_id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.room_id,
    'hardware_full_view'::text AS _targetview
   FROM hardware
     LEFT JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE public.system_image_hardware_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.system_image_hardware_subview TO cen1001;
GRANT SELECT ON TABLE public.system_image_hardware_subview TO cos;


-- View: public.telephone_management_view

-- DROP VIEW public.telephone_management_view;

CREATE OR REPLACE VIEW public.telephone_management_view
 AS
 SELECT dept_telephone_number.id,
    dept_telephone_number.extension_number,
    dept_telephone_number.room_id,
    dept_telephone_number.fax,
    dept_telephone_number.personal_line,
    person_hid.person_hid AS mm_person,
    mm_person_dept_telephone_number.person_id AS mm_person_id
   FROM dept_telephone_number
     LEFT JOIN mm_person_dept_telephone_number ON dept_telephone_number.id = mm_person_dept_telephone_number.dept_telephone_number_id
     LEFT JOIN person_hid USING (person_id);

ALTER TABLE public.telephone_management_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.telephone_management_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.telephone_management_view TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.telephone_management_view TO phones_management;

-- FUNCTION: public.fn_telephone_mgmt_upd(telephone_management_view)

-- DROP FUNCTION public.fn_telephone_mgmt_upd(telephone_management_view);

CREATE OR REPLACE FUNCTION public.fn_telephone_mgmt_upd(
	telephone_management_view)
    RETURNS bigint
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
 declare
         v alias for $1;
         v_dept_telephone_number_id BIGINT;
 begin
 
 IF v.id IS NOT NULL THEN
 
   v_dept_telephone_number_id := v.id;
 
   UPDATE dept_telephone_number 
   SET
       extension_number= v.extension_number,
       room_id = v.room_id,
       fax = v.fax,
       personal_line = v.personal_line
   WHERE dept_telephone_number.id = v.id;
 
 ELSE
 
   v_dept_telephone_number_id := nextval('dept_telephone_number_id_seq');
 
   INSERT INTO dept_telephone_number
         (id, extension_number, room_id, fax, personal_line)
   VALUES
         (v_dept_telephone_number_id, v.extension_number, v.room_id,
          v.fax, v.personal_line);
 
 END IF;
 
 PERFORM fn_upd_many_many (v_dept_telephone_number_id, v.mm_person, 'dept_telephone_number', 'person');
 
 return v_dept_telephone_number_id;
 
 end;
$BODY$;

ALTER FUNCTION public.fn_telephone_mgmt_upd(telephone_management_view)
    OWNER TO cen1001;


-- Rule: telephone_management_del ON public.telephone_management_view

-- DROP Rule telephone_management_del ON public.telephone_management_view;

CREATE OR REPLACE RULE telephone_management_del AS
    ON DELETE TO public.telephone_management_view
    DO INSTEAD
( DELETE FROM mm_person_dept_telephone_number
  WHERE (mm_person_dept_telephone_number.dept_telephone_number_id = old.id);
 DELETE FROM dept_telephone_number
  WHERE (dept_telephone_number.id = old.id);
);

-- Rule: telephone_management_ins ON public.telephone_management_view

-- DROP Rule telephone_management_ins ON public.telephone_management_view;

CREATE OR REPLACE RULE telephone_management_ins AS
    ON INSERT TO public.telephone_management_view
    DO INSTEAD
(SELECT fn_telephone_mgmt_upd(new.*) AS id);

-- Rule: telephone_management_upd ON public.telephone_management_view

-- DROP Rule telephone_management_upd ON public.telephone_management_view;

CREATE OR REPLACE RULE telephone_management_upd AS
    ON UPDATE TO public.telephone_management_view
    DO INSTEAD
(SELECT fn_telephone_mgmt_upd(new.*) AS id);


-- View: public.unknown_group_computers_view

-- DROP VIEW public.unknown_group_computers_view;

CREATE OR REPLACE VIEW public.unknown_group_computers_view
 AS
 SELECT system_image.id,
    hardware.id AS _hardware_id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ip_address_hid.ip_address_hid AS ro_mm_ip_address,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address_hid USING (ip_address_id)
  WHERE system_image.research_group_id = 2 AND ip_address_hid.ip_address_hid !~~ '%personal.private%'::text;

ALTER TABLE public.unknown_group_computers_view
    OWNER TO postgres;

GRANT ALL ON TABLE public.unknown_group_computers_view TO groupitreps;
GRANT ALL ON TABLE public.unknown_group_computers_view TO postgres;


-- Rule: hotwire_view_unknown_group_computers_view ON public.unknown_group_computers_view

-- DROP Rule hotwire_view_unknown_group_computers_view ON public.unknown_group_computers_view;

CREATE OR REPLACE RULE hotwire_view_unknown_group_computers_view AS
    ON UPDATE TO public.unknown_group_computers_view
    DO INSTEAD
( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET user_id = new.user_id, research_group_id = new.research_group_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id
  WHERE (system_image.id = old.id);
);

-- Rule: unknown_group_computers_upd ON public.unknown_group_computers_view

-- DROP Rule unknown_group_computers_upd ON public.unknown_group_computers_view;

CREATE OR REPLACE RULE unknown_group_computers_upd AS
    ON UPDATE TO public.unknown_group_computers_view
    DO INSTEAD
( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET user_id = new.user_id, research_group_id = new.research_group_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id
  WHERE (system_image.id = old.id);
);

-- View: public.user_account_view

-- DROP VIEW public.user_account_view;

CREATE OR REPLACE VIEW public.user_account_view
 AS
 SELECT user_account.id,
    user_account.person_id AS x_person_id,
    user_account.username,
    user_account.uid,
    user_account.description,
    user_account.gecos,
    user_account.expiry_date,
    user_account.is_disabled
   FROM user_account;

ALTER TABLE public.user_account_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.user_account_view TO cen1001;
GRANT ALL ON TABLE public.user_account_view TO dev;
GRANT SELECT ON TABLE public.user_account_view TO mgmt_ro;


-- Rule: user_account_del ON public.user_account_view

-- DROP Rule user_account_del ON public.user_account_view;

CREATE OR REPLACE RULE user_account_del AS
    ON DELETE TO public.user_account_view
    DO INSTEAD
(DELETE FROM user_account
  WHERE (user_account.id = old.id));

-- Rule: user_account_ins ON public.user_account_view

-- DROP Rule user_account_ins ON public.user_account_view;

CREATE OR REPLACE RULE user_account_ins AS
    ON INSERT TO public.user_account_view
    DO INSTEAD
(INSERT INTO user_account (person_id, username, uid, description, gecos, expiry_date, is_disabled)
  VALUES (new.x_person_id, new.username, new.uid, new.description, new.gecos, new.expiry_date, new.is_disabled)
  RETURNING user_account.id,
    user_account.person_id AS x_person_id,
    user_account.username,
    user_account.uid,
    user_account.description,
    user_account.gecos,
    user_account.expiry_date,
    user_account.is_disabled);

-- Rule: user_account_upd ON public.user_account_view

-- DROP Rule user_account_upd ON public.user_account_view;

CREATE OR REPLACE RULE user_account_upd AS
    ON UPDATE TO public.user_account_view
    DO INSTEAD
(UPDATE user_account SET username = new.username, uid = new.uid, description = new.description, gecos = new.gecos, expiry_date = new.expiry_date, is_disabled = new.is_disabled
  WHERE (user_account.id = old.id));

-- FUNCTION: public.research_groups_computing_upd(research_groups_computing_view)

-- DROP FUNCTION public.research_groups_computing_upd(research_groups_computing_view);

