DROP VIEW leavers_form.role;

CREATE OR REPLACE VIEW leavers_form.role AS
  SELECT
    all_roles.role_id as role_id,
    all_roles.role_tablename as role_type,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    person.person_surname,
    all_roles.start_date,
    all_roles.end_date,
    all_roles.intended_end_date,
    all_roles.estimated_leaving_date,
    TRIM(BOTH FROM all_roles.job_title) AS job_title,
    all_roles.post_category_id AS post_category_id,
    all_roles.post_category AS post_category,
    all_roles.status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name
 FROM public._all_roles_with_predicted_status all_roles
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = all_roles.supervisor_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname
  FROM
    person) person ON person.person_id = all_roles.person_id
  ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leavers_form.role
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.role TO dev;
GRANT SELECT ON TABLE leavers_form.role TO onlineleaversform;
