-- View: hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"

DROP VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
 AS
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id,
    ''::character varying(63) AS hostname,
    NULL::integer AS subnet_id,
    NULL::integer AS hardware_id,
    NULL::macaddr AS wired_mac_1,
    ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text ~~* 'ubuntu%'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id,
    ''::text AS system_image_comments,
    NULL::integer AS host_system_image_id,
    NULL::macaddr AS wired_mac_2,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id,
    ( SELECT mm_person_research_group.research_group_id
           FROM person
             JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS research_group_id;

ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    OWNER TO dev;

GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO dev;

-- FUNCTION: hotwire3.easy_add_virtual_machine()

CREATE OR REPLACE FUNCTION hotwire3.easy_add_virtual_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE
    ip_id BIGINT;                                                                                        
    new_system_image_id BIGINT;
BEGIN                                                                                                              
    ip_id =  (SELECT ip_address.id
              FROM ip_address
              WHERE ip_address.subnet_id = NEW.subnet_id 
	          AND ip_address.reserved <> true 
	          AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
              LIMIT 1);

    INSERT INTO system_image 
        (wired_mac_1,
         wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
    VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.system_image_comments::varchar(1000), NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id)
    RETURNING id INTO new_system_image_id;
    UPDATE ip_address SET hostname = NEW.hostname WHERE id = ip_id;
    INSERT INTO mm_system_image_ip_address
        ( ip_address_id, system_image_id )
    VALUES
        ( ip_id, new_system_image_id);
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.easy_add_virtual_machine()
    OWNER TO dev;

CREATE TRIGGER hw3_easy_add_virtual_machine
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    FOR EACH ROW
    EXECUTE PROCEDURE hotwire3.easy_add_virtual_machine();

-- View: hotwire3."10_View/Computers/Easy_Add_Machine"

DROP VIEW hotwire3."10_View/Computers/Easy_Add_Machine";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Easy_Add_Machine"
 AS
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id,
    ''::character varying(63) AS hostname,
    ( SELECT easy_addable_subnet_hid.easy_addable_subnet_id
           FROM hotwire3.easy_addable_subnet_hid
         LIMIT 1) AS easy_addable_subnet_id,
    NULL::macaddr AS wired_mac_1,
    ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text = 'Windows 10'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id,
    'Unknown'::character varying(80) AS manufacturer,
    'Unknown'::character varying(80) AS model,
    ''::character varying(80) AS hardware_name,
    ( SELECT hardware_type_hid.hardware_type_id
           FROM hotwire3.hardware_type_hid
          WHERE hardware_type_hid.hardware_type_hid::text = 'PC'::text
         LIMIT 1) AS hardware_type_id,
    CURRENT_DATE AS date_purchased,
    ''::text AS system_image_comments,
    NULL::integer AS host_system_image_id,
    ( SELECT mm_person_room.room_id
           FROM person
             JOIN mm_person_room ON person.id = mm_person_room.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS room_id,
    NULL::macaddr AS wired_mac_2,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS owner_id,
    ( SELECT mm_person_research_group.research_group_id
           FROM person
             JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS research_group_id;

ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Machine"
    OWNER TO dev;

GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Machine" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Machine" TO dev;

CREATE TRIGGER hw3_easy_add_machine
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Computers/Easy_Add_Machine"
    FOR EACH ROW
    EXECUTE PROCEDURE hotwire3.easy_add_machine();

-- FUNCTION: hotwire3.easy_add_machine()

CREATE OR REPLACE FUNCTION hotwire3.easy_add_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    new_system_image_id bigint;
    new_hardware_id bigint;
BEGIN
    INSERT INTO hardware
        (manufacturer, model, room_id, hardware_type_id, owner_id, name, date_purchased)
    VALUES
        (NEW.manufacturer, NEW.model, NEW.room_id, NEW.hardware_type_id, NEW.owner_id, NEW.hardware_name, NEW.date_purchased)
    RETURNING id INTO new_hardware_id;

    INSERT INTO system_image
        (wired_mac_1, wired_mac_2, hardware_id, operating_system_id, comments, host_system_image_id, user_id, research_group_id)
    VALUES
        (NEW.wired_mac_1, NEW.wired_mac_2, new_hardware_id, NEW.operating_system_id, (NEW.system_image_comments)::character varying(1000), NEW.host_system_image_id, NEW.user_id, NEW.research_group_id) RETURNING id INTO new_system_image_id;

    INSERT INTO mm_system_image_ip_address
        (ip_address_id, system_image_id)
    VALUES (
        (
            SELECT ip_address.id
            FROM ip_address
            WHERE (
                (ip_address.subnet_id = NEW.easy_addable_subnet_id)
                AND
                (ip_address.reserved <> true)
                AND
                ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text)))
            LIMIT 1
        ),
        new_system_image_id)
        ;

    UPDATE ip_address SET hostname = NEW.hostname
    WHERE 
        (ip_address.id IN
            (
                SELECT mm_system_image_ip_address.ip_address_id
                FROM mm_system_image_ip_address
                WHERE (mm_system_image_ip_address.system_image_id = new_system_image_id)
            )
        );
RETURN NEW;
END;

$BODY$;

ALTER FUNCTION hotwire3.easy_add_machine()
    OWNER TO dev;

