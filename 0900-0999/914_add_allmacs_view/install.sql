-- View: public._allmacs

-- DROP VIEW public._allmacs;

CREATE OR REPLACE VIEW public._allmacs
 AS
 SELECT system_image.id,
    system_image.wired_mac_1 AS mac
   FROM system_image
UNION ALL
 SELECT system_image.id,
    system_image.wired_mac_2 AS mac
   FROM system_image
UNION ALL
 SELECT system_image.id,
    system_image.wired_mac_3 AS mac
   FROM system_image
UNION ALL
 SELECT system_image.id,
    system_image.wired_mac_4 AS mac
   FROM system_image
UNION ALL
 SELECT system_image.id,
    system_image.wireless_mac AS mac
   FROM system_image;

ALTER TABLE public._allmacs
    OWNER TO dev;

GRANT SELECT on public._allmacs to osbuilder;
