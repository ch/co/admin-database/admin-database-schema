ALTER TABLE public.postgraduate_studentship ADD COLUMN next_mentor_meeting_due DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN college_history VARCHAR(500);
ALTER TABLE public.postgraduate_studentship ADD COLUMN cpgs_title VARCHAR(500);
ALTER TABLE public.postgraduate_studentship ADD COLUMN msc_title VARCHAR(300);
ALTER TABLE public.postgraduate_studentship ADD COLUMN mphil_title VARCHAR(300);
ALTER TABLE public.postgraduate_studentship ADD COLUMN phd_thesis_title VARCHAR(500);

ALTER TABLE public.postgraduate_studentship ADD COLUMN first_year_probationary_report_title VARCHAR(500);
ALTER TABLE public.postgraduate_studentship ADD COLUMN first_year_probationary_report_due DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN first_year_probationary_report_submitted DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN first_year_probationary_report_approved DATE;

ALTER TABLE public.postgraduate_studentship ADD COLUMN mphil_date_submitted DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN msc_date_submitted DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN cpgs_or_mphil_date_submitted DATE;

ALTER TABLE public.postgraduate_studentship ADD COLUMN phd_date_title_approved DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN thesis_submission_due_date DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN date_phd_submission_due DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN phd_date_examiners_appointed DATE;
ALTER TABLE public.postgraduate_studentship ADD COLUMN phd_date_of_viva DATE;

ALTER TABLE public.postgraduate_studentship ADD COLUMN transferable_skills_days_1st_year REAL;
ALTER TABLE public.postgraduate_studentship ADD COLUMN transferable_skills_days_2nd_year REAL;
ALTER TABLE public.postgraduate_studentship ADD COLUMN transferable_skills_days_3rd_year REAL;
ALTER TABLE public.postgraduate_studentship ADD COLUMN transferable_skills_days_4th_year REAL;

ALTER TABLE public.postgraduate_studentship ADD COLUMN gaf_number VARCHAR(80);
