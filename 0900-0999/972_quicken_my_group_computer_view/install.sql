CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Computers"
 AS
 SELECT DISTINCT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    array_to_string(ARRAY( SELECT ip_address_unsorted_hid.ip_address_hid
           FROM mm_system_image_ip_address
             JOIN hotwire3.ip_address_unsorted_hid USING (ip_address_id)
          WHERE mm_system_image_ip_address.system_image_id = system_image.id), ','::text) AS ro_ip_address,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id AS ro_research_group_id,
    system_image.host_system_image_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban AS ro_override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    system_image.was_managed_machine_on AS ro_was_managed_machine_on,
    system_image.hardware_id AS hardware_pk
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_research_group_computer_rep USING (research_group_id)
     LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
     LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

