ALTER TABLE registration.form ADD COLUMN notify_person_id BIGINT CONSTRAINT form_notify_person_id_fkey REFERENCES public.person(id);
