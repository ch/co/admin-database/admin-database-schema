CREATE OR REPLACE FUNCTION public.fn_update_group_heads(
	)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
 declare
   headofgp varchar;

 begin
  -- people without DB roles: create them
    for headofgp in 
        select distinct crsid 
        from person 
        inner join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        left join pg_roles on person.crsid=rolname 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        where crsid is not null and rolname is null and _physical_status_v2.status_id <> 'Past'
    loop
        execute 'create role '||headofgp||' with login in role headsofgroup';
   end loop;

  -- people who don't have head role
    for headofgp in 
        select distinct crsid 
        from person 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        inner join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        where (
                select count(*) 
                from pg_auth_members 
                inner join pg_roles u on member=u.oid 
                inner join pg_roles g on roleid=g.oid 
                where u.rolname=crsid and g.rolname='groupheadofgps'
            )=0 
        and 
            crsid is not null
        and 
            _physical_status_v2.status_id <> 'Past'
    loop
        execute 'grant headsofgroup to '||headofgp;
    end loop;

  -- people with the head of group role who shouldn't have
    for headofgp in 
        select u.rolname 
        from pg_auth_members 
        inner join pg_authid u on u.oid=pg_auth_members.member 
        inner join pg_authid g on pg_auth_members.roleid=g.oid 
        left join person on person.crsid=u.rolname 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        left join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        where g.rolname='headsofgroup' and ( research_group.id is null or _physical_status_v2.status_id = 'Past' )
    loop
        execute 'revoke headsofgroup from '||headofgp;
    end loop;

 end;
$BODY$;

ALTER FUNCTION public.fn_update_group_heads()
    OWNER TO dev;



CREATE OR REPLACE FUNCTION public.fn_createuser(
	_crsid character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE SECURITY DEFINER PARALLEL UNSAFE
    SET search_path=public, pg_temp
AS $BODY$
declare
 user_account_id bigint;
begin
 -- create postgres user account
  perform usename from pg_catalog.pg_user where usename=_crsid;
  if not found then
   execute format('create user %I with in role _autoadded, selfservice_access',_crsid); 
  end if;
 -- Create or update entry in user_account table
  if _crsid is not null then
   select id from public.user_account where username=_crsid into user_account_id;
   if not found then
    -- we set an expiry date, as if the person never becomes Current the normal expiry apparatus won't take effect and the account will never be closed
    insert into public.user_account (person_id,username,chemnet_token,expiry_date) values ((select id from public.person where crsid=_crsid),_crsid,public.random_string_lower(16),(current_date + interval '1 month'));
   else
    update public.user_account set person_id = (select id from public.person where crsid=_crsid) where id = user_account_id;
   end if;
  end if; 
 end
$BODY$;

ALTER FUNCTION public.fn_createuser(character varying)
    OWNER TO useradder;

COMMENT ON FUNCTION public.fn_createuser(character varying)
    IS 'A Privileged function which ensures that a postgres account exists for the user.';

REVOKE SELECT ON apps.user_accounts_to_create_in_ad_v2 FROM useradder;


CREATE OR REPLACE FUNCTION public.fn_update_itreps(
	)
    RETURNS SETOF text 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

declare
  itrep varchar;
  itrepid bigint;

begin
 -- IT reps without DB roles: create them
  for itrep in 
    select 
        distinct crsid 
    from person 
    inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id 
    left join pg_roles on person.crsid=rolname 
    left join _physical_status_v2 on person.id = _physical_status_v2.person_id
    where crsid is not null and rolname is null and _physical_status_v2.status_id <> 'Past'
  loop
    execute 'create role '||itrep||' with login in role groupitreps';
  end loop;

 -- IT reps who don't have groupitreps role
  for itrep in 
    select 
        distinct crsid 
    from person 
    left join _physical_status_v2 on person.id = _physical_status_v2.person_id
    inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id 
    where (
        select count(*) 
        from pg_auth_members 
        inner join pg_roles u on member=u.oid 
        inner join pg_roles g on roleid=g.oid 
        where u.rolname=crsid and g.rolname='groupitreps'
    )=0 and crsid is not null and _physical_status_v2.status_id <> 'Past'
  loop
   execute 'grant groupitreps to '||itrep;
  end loop;
 -- people with the IT rep role who shouldn't have
 for itrep in select u.rolname from pg_auth_members inner join pg_authid u on u.oid=pg_auth_members.member inner join pg_authid g on pg_auth_members.roleid=g.oid left join person on person.crsid=u.rolname left join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id where g.rolname='groupitreps' and research_group_id is null loop
   execute 'revoke groupitreps from '||itrep;
 end loop;
 -- set homepage in hotwire for people who have no other homepage, but for those with valid database roles only
 for itrep in select distinct rolname from pg_catalog.pg_roles join person on person.crsid = pg_roles.rolname join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id left join ( select rolename, preference_id from hotwire3."hw_User Preferences" where hotwire3."hw_User Preferences".preference_id = 9 ) homepages on homepages.rolename = person.crsid where homepages.preference_id is null loop
   execute 'insert into hotwire3."hw_User Preferences" ( preference_id, preference_value, rolename ) values ( 9, $$10_View/My_Groups/Computers$$, $1)' using itrep;
 end loop;

end;

$BODY$;

ALTER FUNCTION public.fn_update_itreps()
    OWNER TO dev;
