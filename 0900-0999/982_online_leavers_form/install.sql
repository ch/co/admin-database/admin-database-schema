CREATE SCHEMA leavers_form AUTHORIZATION dev;
GRANT ALL ON SCHEMA leavers_form TO dev;
GRANT USAGE ON SCHEMA leavers_form TO onlineleaversform;
GRANT USAGE ON SCHEMA leavers_form TO _pgbackup;


CREATE OR REPLACE VIEW leavers_form.person_basic AS
 SELECT
    person.id AS person_id,
    person.crsid AS crsid,
    person.email_address AS email,
    (((((COALESCE(title_hid.long_title, ''::character varying))::text || ' '::text) || (TRIM(BOTH FROM person.first_names))::text) || ' '::text) || TRIM(BOTH FROM person.surname)::text) AS full_name,
    TRIM(BOTH FROM COALESCE(person.known_as, person.first_names)::text) AS friendly_name
  FROM
    person
  LEFT JOIN public.title_hid USING (title_id)
  WHERE person.crsid IS NOT NULL
  ;

ALTER TABLE leavers_form.person_basic
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.person_basic TO dev;
GRANT SELECT ON TABLE leavers_form.person_basic TO onlineleaversform;


CREATE OR REPLACE VIEW leavers_form.role AS
  SELECT
    all_roles.role_id as role_id,
    all_roles.role_tablename as role_type,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    person.person_surname,
    all_roles.start_date,
    all_roles.end_date,
    all_roles.intended_end_date,
    all_roles.estimated_leaving_date,
    TRIM(BOTH FROM all_roles.job_title) AS job_title,
    all_roles.post_category_id AS post_category_id,
    all_roles.post_category AS post_category,
    all_roles.status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name
 FROM public._all_roles_with_predicted_status all_roles
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = all_roles.supervisor_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname
  FROM
    person) person ON person.person_id = all_roles.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leavers_form.role
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.role TO dev;
GRANT SELECT ON TABLE leavers_form.role TO onlineleaversform;


CREATE OR REPLACE VIEW leavers_form.unified_post_category AS
  SELECT
    'v-'::text || visitor_type_hid.visitor_type_id::text AS post_category_id,
    visitor_type_hid.visitor_type_hid AS post_category,
    'visitorship'::text AS role_tablename,
    visitor_type_hid.visitor_type_id AS raw_id,
    5 AS weight
  FROM
    visitor_type_hid
  UNION
  SELECT
    'p3-1'::text AS post_category_id,
    'Part III'::character varying(80) AS post_category,
    'part_iii_studentship'::text AS role_tablename,
    1 AS raw_id,
    4 as weight
  UNION
  SELECT
    'e-'::text || erasmus_type_hid.erasmus_type_id AS post_category_id,
    erasmus_type_hid.erasmus_type_hid AS post_category,
    'erasmus_socrates_studentship'::text AS role_tablename,
    erasmus_type_hid.erasmus_type_id AS raw_id,
    3 as weight
  FROM
    erasmus_type_hid
  UNION
  SELECT
    'sc-'::text || staff_category.id::text AS post_category_id,
    staff_category.category AS post_category,
    'post_history'::text AS role_tablename,
    staff_category.id AS raw_id,
    1 as weight
  FROM
    staff_category
  UNION
  SELECT
    'pg-'::text || postgraduate_studentship_type.id::text AS post_category_id,
    postgraduate_studentship_type.name AS post_category,
    'postgraduate_studentship'::text AS role_tablename,
    postgraduate_studentship_type.id AS raw_id,
    2 as weight
  FROM
    postgraduate_studentship_type
  ORDER BY weight ASC, raw_id ASC;

ALTER TABLE leavers_form.unified_post_category
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.unified_post_category TO dev;
GRANT SELECT ON TABLE leavers_form.unified_post_category TO onlineleaversform;


CREATE TABLE leavers_form.audit_log
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  actor_apparent BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself. If su1 is masquerading as spqr1 then this will be spqr1.
  actor_real BIGINT NULL REFERENCES public.person(id),  -- allow NULL in case the action is taken by the system itself. If su1 is masquerading as spqr1 then this will be su1.
  action varchar(80) NOT NULL,  -- e.g. login, create form, view, entered date, update
  person bigint NULL REFERENCES public.person(id),  -- where it relates to someone's leaving form
  action_notes TEXT NULL, -- details of changes here where applicable
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.audit_log
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.audit_log TO dev;
GRANT SELECT, INSERT ON TABLE leavers_form.audit_log TO onlineleaversform;

CREATE INDEX audit_log_actor_index ON leavers_form.audit_log (actor_real);
CREATE INDEX audit_log_person_index ON leavers_form.audit_log (person);


-- for provisional leaving dates that need to be actioned/approved by the HR team before becoming live
CREATE TABLE leavers_form.pending_leaving_date
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  person bigint NOT NULL REFERENCES public.person(id),
  leaving_date DATE NOT NULL,
  comments TEXT NULL,
  actor BIGINT NOT NULL REFERENCES public.person(id),
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  related_form BIGINT NOT NULL,
  resolution TEXT NULL,  -- when merged, becomes not null: e.g. "rejected", "accepted", "changed to X", "superseded by id X" ...
  resolution_comments TEXT NULL,
  resolution_actor BIGINT NULL REFERENCES public.person(id),
  resolved_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.pending_leaving_date
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.pending_leaving_date TO dev;
GRANT SELECT, INSERT, UPDATE(resolution, resolution_comments, resolution_actor, resolved_when) ON TABLE leavers_form.pending_leaving_date TO onlineleaversform;

CREATE INDEX pending_leaving_date_person_index ON leavers_form.pending_leaving_date (person);


CREATE TABLE leavers_form.question
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  form_stage int NOT NULL,
  question_key VARCHAR(80) NOT NULL UNIQUE -- how responses are keyed in json and how form elements are named etc
  --use_text_revision bigint NULL -- can be NULL if the question has since been revoked
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.question
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.question TO dev;
GRANT SELECT ON TABLE leavers_form.question TO onlineleaversform;
-- N.B. questions cannot currently be added or edited through the onlineleaversform so permissions not granted to do so

CREATE INDEX leavers_form_question_order_index ON leavers_form.question (form_stage);


CREATE TABLE leavers_form.question_text
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  question_id bigint NOT NULL REFERENCES leavers_form.question(id),
  question_ordering int NOT NULL, -- how to order questions for display within the form_stage that it applies to
  question_summary TEXT NOT NULL, -- not necessarily shown in the main interface but easier for a human to see a summary in a list than the full question
  question_text TEXT NOT NULL, -- the full question text, assumed to be html safe (i.e. can include links to surveys etc). May be overridden at the front-end by a special component.
  allow_notes BOOLEAN NOT NULL DEFAULT TRUE,
  response_type TEXT NOT NULL, -- e.g. 'Text', 'Multiline', 'YesNoNA'
  response_placeholder TEXT NULL,
  actor BIGINT NOT NULL REFERENCES public.person(id), -- who created/updated this edition of the question (either the text, summary or to whom it applies)
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  expired_when TIMESTAMP WITH TIME ZONE NULL  -- when this version of the question was rescinded, or NULL if it is still active
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.question_text
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.question_text TO dev;
GRANT SELECT ON TABLE leavers_form.question_text TO onlineleaversform;
-- N.B. question text cannot currently be edited through the onlineleaversform so permissions not granted to do so

CREATE INDEX leavers_form_question_text_order_index ON leavers_form.question_text (question_ordering);

--ALTER TABLE leavers_form.question ADD FOREIGN KEY (use_text_revision)
--  REFERENCES leavers_form.question_text(id);


CREATE TABLE leavers_form.question_applies_to
(
  question_text_id bigint NOT NULL REFERENCES leavers_form.question_text(id),
  post_category_id TEXT NOT NULL,
  applies BOOLEAN NOT NULL DEFAULT True,
  constraint pk_question_applies_to primary key (question_text_id, post_category_id)
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.question_applies_to
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.question_applies_to TO dev;
GRANT SELECT ON TABLE leavers_form.question_applies_to TO onlineleaversform;
-- N.B. question applications cannot currently be edited through the onlineleaversform so permissions not granted to do so


CREATE TABLE leavers_form.leavers_form_overview
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  person bigint NOT NULL REFERENCES public.person(id),
  edition int NOT NULL, -- start with edition 1 and increment each time the form is reset
  form_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(), -- when this edition of the form was first setup
  last_updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(), -- when did anyone last update any aspect of the form (except submission or deactivation of the form)
  deactivated TIMESTAMP WITH TIME ZONE NULL DEFAULT(Null) -- mark as inactive after a certain period of time by entering a date+time here. at most 1 edition can be active at a time.
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.leavers_form_overview
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leavers_form_overview TO dev;
GRANT SELECT, INSERT, UPDATE(last_updated, deactivated) ON TABLE leavers_form.leavers_form_overview TO onlineleaversform;

CREATE INDEX leavers_form_overview_index ON leavers_form.leavers_form_overview (person, edition);

ALTER TABLE leavers_form.pending_leaving_date ADD FOREIGN KEY(related_form) REFERENCES leavers_form.leavers_form_overview(id);  -- if there is a pending leaving date to be actioned by the HR team


CREATE TABLE leavers_form.leavers_form_submission
(
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  leavers_form_id bigint NOT NULL REFERENCES leavers_form.leavers_form_overview(id),
  submitted_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL, -- did the user actually submit the form or is this just a work-in-progress?
  submitted_by BIGINT NOT NULL REFERENCES public.person(id) -- who submitted the form
  -- note that the users might submit the same form multiple times, possibly with adjustments in between
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.leavers_form_submission
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leavers_form_submission TO dev;
GRANT SELECT, INSERT ON TABLE leavers_form.leavers_form_submission TO onlineleaversform;


-- keep track of which staff categories applied to a person when a form was created
CREATE TABLE leavers_form.mm_leavers_form_post_category
(
  --id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  leavers_form_id bigint NOT NULL REFERENCES leavers_form.leavers_form_overview(id),
  post_category_id TEXT NOT NULL,
  constraint pk_mm_leavers_form_post_category primary key (leavers_form_id, post_category_id)
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.mm_leavers_form_post_category
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.mm_leavers_form_post_category TO dev;
GRANT SELECT, INSERT ON TABLE leavers_form.mm_leavers_form_post_category TO onlineleaversform;

CREATE INDEX mm_leavers_form_post_category_index ON leavers_form.mm_leavers_form_post_category (leavers_form_id);


-- updated answers get new rows in the table. the most recently updated row with a given leavers_form_id and question_id is the correct one to use.
CREATE TABLE leavers_form.leavers_form_detail
(
  id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
  leavers_form_id bigint NOT NULL REFERENCES leavers_form.leavers_form_overview(id),
  question_id bigint REFERENCES leavers_form.question(id) NOT NULL,
  --answer BOOLEAN NULL, -- if the record exists but is null, treat as NA. otherwise treat as not answered yet.
  answer_detail TEXT NULL,  -- null if the question is a Yes/No/NA question. the answer if the question is a text or multiline question. otherwise the detail should appear in a semi-structured way here.
  answer_notes TEXT NULL, -- can be set whether or not the question is a Yes/No/NA question or otherwise. usually a free-form comment box below the question.
  actor BIGINT NOT NULL REFERENCES public.person(id), -- who answered the question
  answered_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(), -- when was this record created
  saved BOOLEAN NULL DEFAULT NULL, -- Null if auto saved, True if explicitly saved, False if explicitly reset this field (not the whole form)
  status_changed_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL -- if the user has explicitly saved or reset this value, this column records when they did so
  -- if the record was saved/cancelled by someone other than the user who entered it, then create a new row instead
) WITH (
  OIDS=FALSE
);
ALTER TABLE leavers_form.leavers_form_detail
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leavers_form_detail TO dev;
GRANT SELECT, INSERT, UPDATE(saved, status_changed_when) ON TABLE leavers_form.leavers_form_detail TO onlineleaversform;


CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent AS
  SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.leaving_date,
    leavers_form_overview_.id AS leavers_form_id,
    leavers_form_overview_.form_exists AS leavers_form_exists,
    leavers_form_overview_.last_updated AS leavers_form_last_updated
  FROM public.person
  LEFT JOIN
  (SELECT id, TRUE as form_exists, last_updated, person FROM leavers_form.leavers_form_overview WHERE deactivated IS NULL) leavers_form_overview_
  ON
  leavers_form_overview_.person = person.id
  WHERE person.leaving_date > (current_date - concat(31, ' DAYS')::INTERVAL) AND
        person.leaving_date < (current_date + concat(31, ' DAYS')::INTERVAL)
  ORDER BY person_surname, person_name ASC;

ALTER TABLE leavers_form.leaver_soon_or_recent
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent TO dev;
GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent TO onlineleaversform;


CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent_submitted AS
  SELECT
    leaver_soon_or_recent.person_id,
    leaver_soon_or_recent.person_crsid,
    leaver_soon_or_recent.person_email,
    leaver_soon_or_recent.person_name,
    leaver_soon_or_recent.person_surname,
    leaver_soon_or_recent.leaving_date,
    leaver_soon_or_recent.leavers_form_id,
    COALESCE(leaver_soon_or_recent.leavers_form_exists, FALSE) AS leavers_form_exists,
    leaver_soon_or_recent.leavers_form_last_updated,
    EXISTS (SELECT 1 FROM leavers_form.leavers_form_submission WHERE leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id AND leavers_form_submission.submitted_when >= leaver_soon_or_recent.leavers_form_last_updated) AS leavers_form_submitted
  FROM leavers_form.leaver_soon_or_recent
  ORDER BY person_surname, person_name ASC;

ALTER TABLE leavers_form.leaver_soon_or_recent_submitted
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent_submitted TO dev;
GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;
