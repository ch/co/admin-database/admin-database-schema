DROP VIEW leavers_form.leaver_soon_or_recent_submitted;
DROP VIEW leavers_form.leaver_soon_or_recent;
DROP TABLE leavers_form.leavers_form_detail;
ALTER TABLE leavers_form.pending_leaving_date DROP CONSTRAINT pending_leaving_date_related_form_fkey;
DROP TABLE leavers_form.mm_leavers_form_post_category;
DROP TABLE leavers_form.leavers_form_submission;
DROP TABLE leavers_form.leavers_form_overview;
DROP TABLE leavers_form.question_applies_to;
DROP TABLE leavers_form.question_text;
DROP TABLE leavers_form.question;
DROP TABLE leavers_form.pending_leaving_date;
DROP TABLE leavers_form.audit_log;
DROP VIEW leavers_form.unified_post_category;
DROP VIEW leavers_form.role;
DROP VIEW leavers_form.person_basic;
DROP SCHEMA leavers_form;
