alter table dns_domains add constraint dns_domain_master_non_null_updated check (type != 'MASTER' OR dns_lastchange IS NOT NULL);
