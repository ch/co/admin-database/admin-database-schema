CREATE OR REPLACE VIEW leavers_form.research_group_admin AS
  SELECT
    research_group.id AS research_group_id,
    research_group.name AS research_group_name,
    research_group.head_of_group_id AS research_group_head_person_id,
    research_group.administrator_id AS administrator_person_id,
    research_group_head_person.crsid AS research_group_head_person_crsid,
    research_group_head_person.email_address AS research_group_head_person_email_address,
    research_group_head_person.person_name AS research_group_head_person_name,
    administrator_person.crsid AS administrator_person_crsid,
    administrator_person.email_address AS administrator_person_email_address,
    administrator_person.person_name AS administrator_person_name
 FROM public.research_group
 LEFT JOIN (SELECT
    person.id,
    person.crsid,
    person.email_address,
    TRIM(BOTH FROM (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text) AS person_name
  FROM
    person) research_group_head_person
  ON
    research_group_head_person.id = research_group.head_of_group_id
  LEFT JOIN (SELECT
    person.id,
    person.crsid,
    person.email_address,
    TRIM(BOTH FROM (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text) AS person_name,
    person.surname AS surname
  FROM
    person) administrator_person
  ON
    administrator_person.id = research_group.administrator_id
  ORDER BY research_group_name ASC;

ALTER TABLE leavers_form.research_group_admin
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.research_group_admin TO dev;
GRANT SELECT ON TABLE leavers_form.research_group_admin TO onlineleaversform;
