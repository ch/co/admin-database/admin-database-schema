-- add reverse_subnet records for each forward subnet for which we provide local DNS

INSERT INTO reverse_subnet.reverse_subnet (domain_name, network_address, dns_lastchange)
    SELECT arpa AS domain_name, arpa_as_network_address AS network_address, MAX(dns_lastchange) AS dns_lastchange
    FROM reverse_subnet.arpa_subnet GROUP BY arpa_as_network_address, arpa ORDER BY network_address;


-- update reverse_subnet references for existing ip_address{es}
-- note that this step is fairly slow, which also demonstrates why we need this reference in the first place:
-- trying to do lookups on the fly from powerdns without this information yields intolerable performance
UPDATE public.ip_address SET reverse_subnet = (SELECT id FROM reverse_subnet.reverse_subnet WHERE ip_address.ip <<= reverse_subnet.network_address) FROM public.subnet WHERE subnet.id = ip_address.subnet_id AND subnet.use_local_records_for_dns;
