CREATE SCHEMA reverse_subnet;
ALTER SCHEMA reverse_subnet OWNER TO dev;
GRANT ALL ON SCHEMA reverse_subnet TO dev;
GRANT USAGE ON SCHEMA reverse_subnet TO public;
GRANT USAGE ON SCHEMA reverse_subnet TO _pgbackup;


CREATE OR REPLACE FUNCTION reverse_subnet.array_reverse(anyarray)
RETURNS anyarray AS
$BODY$
SELECT ARRAY(
    SELECT $1[i]
    FROM generate_subscripts($1,1) AS s(i)
    ORDER BY i DESC
);
$BODY$
LANGUAGE sql STRICT IMMUTABLE;
ALTER FUNCTION reverse_subnet.array_reverse(anyarray) OWNER TO dev;
COMMENT ON FUNCTION reverse_subnet.array_reverse(anyarray) IS 'To reverse an array';
GRANT EXECUTE ON FUNCTION reverse_subnet.array_reverse(anyarray) to public;


-- We (currently as of 2022-07-01) use subnets with masklens of
-- 20,23,24,25,26,27,28,29
-- We are unlikely to use anything larger than /16, so aren't implementing that
--
-- To avoid having to define reverse_subnet.subnet_to_octets_boundaries below
-- for a longer list of masklens, let's constrain ourselves as follows...
-- This check will mean we get a meaningful error which we can then lookup
-- should we ever try to create an unsupported subnet.
ALTER TABLE public.subnet ADD CONSTRAINT subnet_size CHECK (masklen(subnet.network_address) BETWEEN 16 AND 30);
-- note that /31 and /32 masks have different interpretations for the otherwise reserved addresses, which we don't want to get into here

-- To convert a subnet to ARPA
-- if the subnet is a.b.c.d/32 then the ARPA is b.c.d.a.in-addr.arpa, and it does not need to be shared
-- if the subnet is a.b.c.d/x with 25 <= x <= 31 then let's use an ARPA of c.b.a.in-addr.arpa, but share it across all relevant subnets
-- if the subnet is a.b.c.0/24 then the ARPA is c.b.a.in-addr.arpa, but it does not need to be shared
-- if the subnet is a.b.c.0/x with 17 <= x <= 23 then let's use ARPAs of Y.b.a.in-addr.arpa, where Y has the relevant range. it does not need to be shared with other subnets.
-- if the subnet is a.b.0.0/16 then the ARPA is b.a.in-addr.arpa, but it does not need to be shared
-- we do not need to worry about larger subnets due to our constraint subnet_size
CREATE OR REPLACE FUNCTION reverse_subnet.subnet_to_octets_boundaries(cidr)
RETURNS SETOF cidr AS
$BODY$
begin
  IF masklen($1) = 32 THEN
    RETURN QUERY 
      select network($1);
  ELSIF masklen($1) >= 24 THEN
    RETURN QUERY 
      select network(set_masklen($1, 24));
  ELSIF masklen($1) >= 17 THEN
    RETURN QUERY
      WITH RECURSIVE subnet_to_octets_boundaries_recursive(_cidr_first3) AS
      (
          SELECT network(set_masklen($1, 24))
          UNION
          SELECT network(_cidr_first3 + 256) FROM subnet_to_octets_boundaries_recursive WHERE _cidr_first3 + 256 <<= $1
      )
      SELECT reverse_subnet.subnet_to_octets_boundaries(_cidr_first3) FROM subnet_to_octets_boundaries_recursive;
  ELSIF masklen($1) = 16 THEN
    RETURN QUERY
      select network($1);
 END IF;
end
$BODY$
LANGUAGE plpgsql IMMUTABLE STRICT COST 100;

ALTER FUNCTION reverse_subnet.subnet_to_octets_boundaries(cidr) OWNER TO dev;
COMMENT ON FUNCTION reverse_subnet.subnet_to_octets_boundaries(cidr) IS 'To convert a subnet to (possibly multiple) /32, /24 or /16 cidr value(s) for use in ARPA';
GRANT EXECUTE ON FUNCTION reverse_subnet.subnet_to_octets_boundaries(cidr) to public;


CREATE OR REPLACE FUNCTION reverse_subnet.ip_to_full_arpa(inet)
RETURNS text AS
$BODY$
begin
    RETURN array_to_string(reverse_subnet.array_reverse(string_to_array(host($1),'.')),'.')||'.in-addr.arpa';
end
$BODY$
LANGUAGE plpgsql IMMUTABLE STRICT COST 100;

ALTER FUNCTION reverse_subnet.ip_to_full_arpa(inet) OWNER TO dev;
COMMENT ON FUNCTION reverse_subnet.ip_to_full_arpa(inet) IS 'To convert a 4-segment full-IPv4 address value to a full 4-octet in-addr.arpa name. Netmask is ignored.';
GRANT EXECUTE ON FUNCTION reverse_subnet.ip_to_full_arpa(inet) to public;


CREATE OR REPLACE FUNCTION reverse_subnet.octets_boundary_to_arpa(cidr)
RETURNS SETOF text AS
$BODY$
declare _reversed_ip varchar array;
begin
  select reverse_subnet.array_reverse(string_to_array(host($1),'.')) into _reversed_ip;
  IF masklen($1) = 32 THEN
    RETURN QUERY 
      select array_to_string(_reversed_ip,'.')||'.in-addr.arpa';
  ELSIF masklen($1) = 24 THEN
    RETURN QUERY 
      select array_to_string(_reversed_ip[array_lower(_reversed_ip,1)+1:array_upper(_reversed_ip,1)],'.')||'.in-addr.arpa';
  ELSIF masklen($1) = 16 THEN
    RETURN QUERY
      select array_to_string(_reversed_ip[array_lower(_reversed_ip,1)+2:array_upper(_reversed_ip,1)],'.')||'.in-addr.arpa';
 END IF;
end
$BODY$
LANGUAGE plpgsql IMMUTABLE STRICT COST 100;

ALTER FUNCTION reverse_subnet.octets_boundary_to_arpa(cidr) OWNER TO dev;
COMMENT ON FUNCTION reverse_subnet.octets_boundary_to_arpa(cidr) IS 'To convert a /32, /24 or /16 cidr value to a 4-octet, 3-octet or 2-octet in-addr.arpa name, respectively.';
GRANT EXECUTE ON FUNCTION reverse_subnet.octets_boundary_to_arpa(cidr) to public;


CREATE TABLE reverse_subnet.reverse_subnet (
    id bigint DEFAULT nextval('public.subnet_id_seq'::regclass) NOT NULL,
    network_address inet NOT NULL,
    domain_name character varying(255),
    dns_lastchange timestamp with time zone DEFAULT now()
);

COMMENT ON TABLE reverse_subnet.reverse_subnet IS 'For storing reverse subnets which will facilitate making powerdns more performant. Only subnets for which use_local_records_for_dns is true should be present here. The relationship between subnets and reverse subnets is many-to-many. If a reverse subnet is present then all corresponding forward subnets must have use_local_records_for_dns set to true.';

ALTER TABLE reverse_subnet.reverse_subnet OWNER TO dev;

ALTER TABLE ONLY reverse_subnet.reverse_subnet
    ADD CONSTRAINT pk_reverse_subnet PRIMARY KEY (id);

CREATE UNIQUE INDEX idx_reverse_subnet_networkaddress ON reverse_subnet.reverse_subnet USING btree (network_address);

GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO dns;
GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO PUBLIC;


CREATE OR REPLACE FUNCTION reverse_subnet.update_reverse_subnet_on_dns_aname_change()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare _reverse_subnet_id bigint;
begin
if TG_OP in ('INSERT','UPDATE') then
  if new.ip_address_id is not null then
    select reverse_subnet_id FROM ip_address WHERE ip_address.id=new.ip_address_id into _reverse_subnet_id;
    if _reverse_subnet_id is not null then
      update reverse_subnet set dns_lastchange=now() where id=_reverse_subnet_id;
    end if;
  end if;
end if;

if TG_OP in ('UPDATE', 'DELETE') then
  if old.ip_address_id is not null then
    select reverse_subnet_id FROM ip_address WHERE ip_address.id=old.ip_address_id into _reverse_subnet_id;
    if _reverse_subnet_id is not null then
      update reverse_subnet set dns_lastchange=now() where id=_reverse_subnet_id;
    end if;
  end if;
end if;

if TG_OP in ('UPDATE', 'INSERT') then
 return new;
else -- we are DELETEing
  return old;
end if;

end;
$function$;

ALTER FUNCTION reverse_subnet.update_reverse_subnet_on_dns_aname_change() OWNER TO dev;
CREATE TRIGGER dns_aname_set_reverse_subnet_change_time BEFORE INSERT OR DELETE OR UPDATE ON public.dns_anames FOR EACH ROW EXECUTE FUNCTION reverse_subnet.update_reverse_subnet_on_dns_aname_change();


CREATE OR REPLACE FUNCTION reverse_subnet.update_reverse_subnet_on_ip_address_change()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
begin
if TG_OP in ('INSERT','UPDATE') then
 if new.reverse_subnet is not null then
   update reverse_subnet.reverse_subnet set dns_lastchange=now() where id = new.reverse_subnet;
 end if;
end if;

if TG_OP in ('UPDATE', 'DELETE') then
 if old.reverse_subnet is not null then
   update reverse_subnet.reverse_subnet set dns_lastchange=now() where id = old.reverse_subnet;
 end if;
end if;

if TG_OP in ('UPDATE', 'INSERT') then
 return new;
else -- we are DELETEing
  return old;
end if;

end;
$function$
;

ALTER FUNCTION reverse_subnet.update_reverse_subnet_on_ip_address_change() OWNER TO dev;
CREATE TRIGGER ip_address_set_reverse_subnet_change_time BEFORE INSERT OR DELETE OR UPDATE ON public.ip_address FOR EACH ROW EXECUTE FUNCTION reverse_subnet.update_reverse_subnet_on_ip_address_change();

-- view for populating/comparing against reverse_subnet.reverse_subnet
CREATE OR REPLACE VIEW reverse_subnet.arpa_subnet AS
    select id AS subnet_id, network_address, arpa, arpa_as_network_address, dns_lastchange FROM subnet CROSS JOIN LATERAL (SELECT subnet_to_octets_boundaries AS arpa_as_network_address, reverse_subnet.octets_boundary_to_arpa(subnet_to_octets_boundaries) AS arpa FROM reverse_subnet.subnet_to_octets_boundaries(subnet.network_address::cidr)) s WHERE subnet.use_local_records_for_dns;
;
ALTER TABLE reverse_subnet.arpa_subnet OWNER TO dev;

-- allow ip_address to reference reverse_subnet
ALTER TABLE public.ip_address ADD COLUMN reverse_subnet bigint NULL REFERENCES reverse_subnet.reverse_subnet(id) ON DELETE SET NULL; -- can be null in the event that we don't provide local DNS for the subnet

-- distinct rows in the reverse_subnet table should not overlap
ALTER TABLE reverse_subnet.reverse_subnet ADD CONSTRAINT reverse_subnet_network_address_no_overlap EXCLUDE USING gist (network_address inet_ops WITH &&);

-- when new subnets are added, add/update corresponding reverse_subnet entries and ip_address entries
CREATE OR REPLACE FUNCTION public.subnet_trig()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    declare
        new_network_address inet;
        new_router inet;
        next_ip inet;
        new_subnet_id bigint;
        new_broadcast inet;
        reverse_subnet_id bigint;
        reverse_subnet_network_address cidr;
        _reserved boolean;
    begin
        new_subnet_id = NEW.id;
        new_network_address = NEW.network_address;
        new_router = NEW.router;
        new_broadcast = set_masklen(broadcast(new_network_address), 32); -- remove the netmask

        -- first ensure that all the reverse subnets exist (or do not exist, as necessary)
        IF new.use_local_records_for_dns THEN
            -- ensure they do exist
            INSERT INTO reverse_subnet.reverse_subnet (network_address, domain_name, dns_lastchange)
                SELECT new_reverse_subnets.new_reverse_subnets AS network_address, reverse_subnet.octets_boundary_to_arpa(new_reverse_subnets.new_reverse_subnets) AS domain_name, now() AS dns_lastchange FROM reverse_subnet.subnet_to_octets_boundaries(new_network_address::cidr) new_reverse_subnets
                WHERE NOT EXISTS (SELECT reverse_subnet.network_address FROM reverse_subnet.reverse_subnet WHERE reverse_subnet.network_address = new_reverse_subnets.new_reverse_subnets)
            ;
        ELSE
            -- ensure they don't exist
            -- N.B. we don't use the value retrieved into reverse_subnet_network_address here (except in the exception message)
            -- we only check whether there were any results through the magic FOUND variable
            SELECT new_reverse_subnets.new_reverse_subnets AS network_address FROM reverse_subnet.subnet_to_octets_boundaries(new_network_address::cidr) new_reverse_subnets
                WHERE EXISTS (SELECT reverse_subnet.network_address FROM reverse_subnet.reverse_subnet WHERE reverse_subnet.network_address && new_reverse_subnets.new_reverse_subnets) INTO reverse_subnet_network_address;
            IF FOUND THEN
                RAISE EXCEPTION 'There are already reverse subnet(s) that overlap (e.g. %). This could be due to incompatible values for use_local_records_for_dns on the forward subnets', reverse_subnet_network_address;
            END IF;
        END IF;

        -- now add the ip addresses
        -- there is a constraint that the router must belong to the subnet
        -- and we handle that address specially by setting its reserved flag
        -- use the all ones netmask (/32 cidr) to denote a single IP address
        -- (same as converting to text using host(inet) and then converting back to inet)
        next_ip = set_masklen(network(new_network_address),32);

        -- we will want to set the reverse_subnet fields of the newly added IP addresses
        IF NOT new.use_local_records_for_dns THEN
            reverse_subnet_id = NULL;
            reverse_subnet_network_address = '0.0.0.0/0'::cidr;
        ELSE
            -- so lookup the correct reverse subnet for the first next_ip
            SELECT reverse_subnet.id, reverse_subnet.network_address FROM reverse_subnet.reverse_subnet WHERE reverse_subnet.network_address >>= next_ip INTO STRICT reverse_subnet_id, reverse_subnet_network_address;
        END IF;

        <<insert_ip>>
        loop
            -- the subnet address itself is considered reserved; do not add it (i.e. we skip the first value in the loop)
            -- TODO: we would need to special case if we ever added a trivial /31 or /32 subnet
            next_ip = next_ip + 1;
            begin
                -- check for falling off the end of the network
                -- TODO: again, special casing needed for /31 or /32 subnets
                if next_ip = new_broadcast then
                    exit insert_ip;
                end if;
                -- most often the next ip will be in the same reverse subnet as the previous one
                -- but if we've crossed octet boundaries, then we may need to recompute
                IF NOT next_ip <<= reverse_subnet_network_address THEN
                    -- need to determine correct reverse subnet
                    SELECT reverse_subnet.id, reverse_subnet.network_address FROM reverse_subnet.reverse_subnet WHERE reverse_subnet.network_address >>= next_ip INTO STRICT reverse_subnet_id, reverse_subnet_network_address;
                END IF;
                _reserved = COALESCE(NEW.router = next_ip, FALSE);
                -- create a new ip_address record if one doesn't already exist...
                insert into ip_address ( ip, subnet_id, reverse_subnet, reserved ) values ( next_ip, new_subnet_id, reverse_subnet_id, _reserved ) ;
            exception when unique_violation then
                -- ...or update the existing one otherwise
                update ip_address set subnet_id = new_subnet_id, reverse_subnet = reverse_subnet_id, reserved = (_reserved OR reserved) where ip = next_ip;
            end;
        end loop insert_ip;

        return null;
    end;
$function$;

ALTER FUNCTION public.subnet_trig() OWNER TO dev;
