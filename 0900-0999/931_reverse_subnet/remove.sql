DROP SCHEMA reverse_subnet CASCADE;

ALTER TABLE public.ip_address DROP COLUMN reverse_subnet;

ALTER TABLE public.subnet DROP CONSTRAINT subnet_size;

CREATE OR REPLACE FUNCTION public.subnet_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

        declare
                new_network_address inet;
                new_router inet;
                next_ip inet;
                new_subnet_id bigint;
                new_broadcast inet;

        begin
                new_subnet_id = NEW.id;
                new_network_address = NEW.network_address;
                new_router = NEW.router;
                new_broadcast=host(broadcast(new_network_address));

                -- first we create the router address
                if new_router is not null then
                        insert into ip_address ( ip, subnet_id, reserved ) values ( new_router, new_subnet_id, 't' );
                end if;

                -- now add all the rest
                next_ip =
                host(set_masklen(network(new_network_address),32));

                <<insert_ip>>
                loop
                        next_ip = next_ip + 1;
                        begin
                                -- check for falling off the end of the network
                                if  next_ip = new_broadcast then
                                        exit insert_ip;
                                end if;
                                insert into ip_address ( ip, subnet_id ) values ( next_ip, new_subnet_id ) ;
                        exception when unique_violation then
                                        -- do nothing
                        end;
                end loop insert_ip;

        return null;

        end;

$BODY$;

ALTER FUNCTION public.subnet_trig()
    OWNER TO cen1001;
