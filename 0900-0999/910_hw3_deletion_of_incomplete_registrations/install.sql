GRANT DELETE on hotwire3."10_View/People/Incomplete_Registration_Forms" to hr;

CREATE RULE hw3_incomplete_reg_del AS ON DELETE TO hotwire3."10_View/People/Incomplete_Registration_Forms" DO INSTEAD DELETE FROM registration.form WHERE uuid = old.form_uuid;
