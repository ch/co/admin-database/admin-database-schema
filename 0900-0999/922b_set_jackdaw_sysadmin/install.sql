-- View: public._jackdaw_data

-- DROP VIEW public._jackdaw_data;

CREATE OR REPLACE VIEW public._jackdaw_data
 AS
 SELECT DISTINCT ON (ip_address_view.ip) ip_address_view.subnet_id,
    ip_address_view.ip,
    lower(ip_address_view.hostname::text)::character varying(63) AS hostname,
    _jackdaw_cleantext((hardware.manufacturer::text || ' '::text) || hardware.model::text) AS equipment,
    _jackdaw_cleantext(room.name::text) AS location,
    _jackdaw_cleantext(COALESCE(((((person.first_names::text || ' '::text) || person.surname::text) || ' ('::text) || person.crsid::text) || ')'::text, 'support@ch.cam.ac.uk'::text)) AS end_user,
    substr(_jackdaw_cleantext(COALESCE(((((person_owner.first_names::text || ' '::text) || person_owner.surname::text) || ' ('::text) || person_owner.crsid::text) || ')'::text, 'support@ch.cam.ac.uk'::text)), 1, 48) AS owner,
    'support@ch.cam.ac.uk' AS sysadmin,
    _jackdaw_oses_for_ip(ip_address_view.id::integer) AS remarks,
    ''::text AS review_date
   FROM ip_address_view
     LEFT JOIN subnet_hid ON ip_address_view.subnet_id = subnet_hid.subnet_id
     LEFT JOIN mm_system_image_ip_address ON ip_address_view.id = mm_system_image_ip_address.ip_address_id
     LEFT JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
     LEFT JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN person ON person.id = system_image.user_id
     LEFT JOIN room ON room.id = hardware.room_id
     LEFT JOIN person person_owner ON person_owner.id = hardware.owner_id
  ORDER BY ip_address_view.ip;

ALTER TABLE public._jackdaw_data
    OWNER TO dev;

GRANT SELECT ON TABLE public._jackdaw_data TO jackdaw;
GRANT ALL ON TABLE public._jackdaw_data TO dev;
