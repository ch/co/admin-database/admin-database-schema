CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Firstaiders"
 AS
 SELECT firstaider.id,
    person.image_lo AS ro_image,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.hf_cn_qualification_date,
    firstaider.hf_cn_requalify_date,
    firstaider.mental_health_awareness_trained,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_hid USING (building_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
          WHERE p2.id = firstaider.person_id
          ORDER BY room.name), '/'::text) AS ro_room,
    array_to_string(ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON mm_person_dept_telephone_number.person_id = p2.id
             JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
          WHERE p2.id = firstaider.person_id), ' / '::text) AS ro_dept_telephone_numbers,
    _physical_status.status_id AS status,
    CASE
        WHEN firstaider.requalify_date >= current_date THEN 't'::boolean
        ELSE 'f'::boolean
    END AS ro_qualification_up_to_date,
    CASE
        WHEN firstaider.hf_cn_trained AND firstaider.hf_cn_requalify_date >= current_date THEN TRUE
        WHEN firstaider.hf_cn_trained AND (firstaider.hf_cn_requalify_date < current_date OR firstaider.hf_cn_requalify_date IS NULL) THEN FALSE
        ELSE NULL
    END AS ro_hf_cn_qualification_up_to_date,
    _latest_role.post_category AS ro_post_category,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
        CASE
            WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN firstaider.requalify_date < current_date THEN 'red'::text
            ELSE NULL::text
        END AS _cssclass
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     LEFT JOIN cache._latest_role_v12 _latest_role ON firstaider.person_id = _latest_role.person_id
     LEFT JOIN cache.person_futuremost_role futuremost_role ON firstaider.person_id = futuremost_role.person_id
     JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = firstaider.person_id;
