CREATE TABLE public.immigration_status (
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    status VARCHAR UNIQUE NOT NULL
);
ALTER TABLE public.immigration_status OWNER TO dev;

INSERT INTO public.immigration_status
    ( status )
    VALUES
    ( 'Unknown'),
    ( 'Global Talent/Tier 1 Visa' ),
    ( 'Skilled Worker/Tier 2 Visa' ),
    ( 'Student/Tier 4 Visa' ),
    ( 'GAE/Tier 5 Visa' ),
    ( 'Indefinite leave to remain'),
    ( 'Pre-settled status' ),
    ( 'Visitor (non-UK)' ),
    ( 'UK National' )
;

CREATE VIEW hotwire3.immigration_status_hid AS
SELECT id AS immigration_status_id, status AS immigration_status_hid
FROM public.immigration_status;
ALTER VIEW hotwire3.immigration_status_hid OWNER TO dev;

-- nationality has a country_name column but it's nullable and not unique
-- because there are cases where people coming from the same country prefer
-- different descriptions for their nationality.
CREATE VIEW hotwire3.passport_issuing_country_hid AS
SELECT min(id) AS passport_issuing_country_id, country_name AS passport_issuing_country_hid
FROM public.nationality 
WHERE country_name IS NOT NULL
GROUP BY country_name
ORDER BY country_name;
ALTER VIEW hotwire3.immigration_status_hid OWNER TO dev;
