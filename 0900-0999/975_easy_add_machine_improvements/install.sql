DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Easy_Add_Machine';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Computers/Easy_Add_Virtual_Machine';


CREATE FUNCTION hotwire3.easy_add_machine() RETURNS TRIGGER AS
$body$
DECLARE
    new_system_image_id bigint;
    new_hardware_id bigint;
BEGIN
    INSERT INTO hardware
        (manufacturer, model, room_id, hardware_type_id, owner_id, name, date_purchased)
    VALUES
        (NEW.manufacturer, NEW.model, NEW.room_id, NEW.hardware_type_id, NEW.owner_id, NEW.hardware_name, NEW.date_purchased)
    RETURNING id INTO new_hardware_id;

    INSERT INTO system_image
        (wired_mac_1, wired_mac_2, hardware_id, operating_system_id, comments, host_system_image_id, user_id, research_group_id)
    VALUES
        (NEW.wired_mac_1, NEW.wired_mac_2, new_hardware_id, NEW.operating_system_id, (NEW.system_image_comments)::character varying(1000), NEW.host_system_image_id, NEW.user_id, NEW.research_group_id) RETURNING id INTO new_system_image_id;

    INSERT INTO mm_system_image_ip_address
        (ip_address_id, system_image_id)
    VALUES (
        (
            SELECT ip_address.id
            FROM ip_address
            WHERE (
                (ip_address.subnet_id = NEW.easy_addable_subnet_id)
                AND
                (ip_address.reserved <> true)
                AND
                ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text)))
            LIMIT 1
        ),
        new_system_image_id)
        ;

    UPDATE ip_address SET hostname = NEW.hostname
    WHERE 
        (ip_address.id IN
            (
                SELECT mm_system_image_ip_address.ip_address_id
                FROM mm_system_image_ip_address
                WHERE (mm_system_image_ip_address.system_image_id = new_system_image_id)
            )
        );
RETURN NEW;
END;

$body$ LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.easy_add_machine() OWNER TO dev;

CREATE FUNCTION hotwire3.easy_add_virtual_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE
    ip_id BIGINT;                                                                                        
    new_system_image_id BIGINT;
BEGIN                                                                                                              
    ip_id =  (SELECT ip_address.id
              FROM ip_address
              WHERE ip_address.subnet_id = NEW.subnet_id 
	          AND ip_address.reserved <> true 
	          AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
              LIMIT 1);

    INSERT INTO system_image 
        (wired_mac_1,
         wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
    VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.system_image_comments::varchar(1000), NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id)
    RETURNING id INTO new_system_image_id;
    UPDATE ip_address SET hostname = NEW.hostname WHERE id = ip_id;
    INSERT INTO mm_system_image_ip_address
        ( ip_address_id, system_image_id )
    VALUES
        ( ip_id, new_system_image_id);
    RETURN NEW;
END;
$BODY$;
ALTER FUNCTION hotwire3.easy_add_virtual_machine() OWNER TO dev;

DROP RULE hotwire3_view_easy_add_machine_view_upd ON hotwire3."10_View/Computers/Easy_Add_Machine";
DROP TRIGGER hotwire3_view_easy_add_virtual_machine_view_upd ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";
DROP FUNCTION hotwire3.upd_easy_add_virtual_machine();

CREATE TRIGGER hw3_easy_add_machine INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/Computers/Easy_Add_Machine" FOR EACH ROW EXECUTE PROCEDURE hotwire3.easy_add_machine();
CREATE TRIGGER hw3_easy_add_virtual_machine
    INSTEAD OF UPDATE OR INSERT
    ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.easy_add_virtual_machine();
