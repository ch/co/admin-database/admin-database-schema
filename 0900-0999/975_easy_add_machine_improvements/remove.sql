DROP TRIGGER hw3_easy_add_machine ON hotwire3."10_View/Computers/Easy_Add_Machine";
DROP TRIGGER hw3_easy_add_virtual_machine ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";

CREATE FUNCTION hotwire3.upd_easy_add_virtual_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

     declare
                      ip_id BIGINT;                                                                                        
     begin                                                                                                              
	ip_id =  (SELECT ip_address.id
			FROM ip_address
			WHERE ip_address.subnet_id = NEW.subnet_id 
				AND ip_address.reserved <> true 
				AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
			LIMIT 1);

	INSERT INTO system_image 
		(wired_mac_1, wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
		VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.system_image_comments::varchar(1000), NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id);
	update ip_address set hostname = NEW.hostname where id = ip_id;
	insert into mm_system_image_ip_address ( ip_address_id, system_image_id )
		values ( ip_id, currval('system_image_id_seq'::regclass));
	return NEW;
	end
$BODY$;
ALTER FUNCTION hotwire3.upd_easy_add_virtual_machine() OWNER TO dev;

CREATE TRIGGER hotwire3_view_easy_add_virtual_machine_view_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.upd_easy_add_virtual_machine();

CREATE RULE hotwire3_view_easy_add_machine_view_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Easy_Add_Machine"
    DO INSTEAD
( INSERT INTO hardware (manufacturer, model, room_id, hardware_type_id, owner_id, name, date_purchased)
  VALUES (new.manufacturer, new.model, new.room_id, new.hardware_type_id, new.owner_id, new.hardware_name, new.date_purchased);
 INSERT INTO system_image (wired_mac_1, wired_mac_2, hardware_id, operating_system_id, comments, host_system_image_id, user_id, research_group_id)
  VALUES (new.wired_mac_1, new.wired_mac_2, currval('hardware_id_seq'::regclass), new.operating_system_id, (new.system_image_comments)::character varying(1000), new.host_system_image_id, new.user_id, new.research_group_id);
 INSERT INTO mm_system_image_ip_address (ip_address_id, system_image_id)
  VALUES (( SELECT ip_address.id
           FROM ip_address
          WHERE ((ip_address.subnet_id = new.easy_addable_subnet_id) AND (ip_address.reserved <> true) AND ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text)))
         LIMIT 1), currval('system_image_id_seq'::regclass));
 UPDATE ip_address SET hostname = new.hostname
  WHERE (ip_address.id IN ( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE (mm_system_image_ip_address.system_image_id = currval('system_image_id_seq'::regclass))));
);

DROP FUNCTION hotwire3.easy_add_machine();
DROP FUNCTION hotwire3.easy_add_virtual_machine();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Easy_Add_Machine', 'system_image' ), ( '10_View/Computers/Easy_Add_Virtual_Machine', 'system_image');
