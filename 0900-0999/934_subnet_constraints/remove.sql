ALTER TABLE public.subnet DROP CONSTRAINT subnet_network_address_no_overlap;
ALTER TABLE public.subnet DROP CONSTRAINT subnet_router_agreement;
ALTER TABLE public.subnet DROP CONSTRAINT subnet_aligned;
