-- distinct rows in the subnet table should not overlap
ALTER TABLE public.subnet ADD CONSTRAINT subnet_network_address_no_overlap EXCLUDE USING gist (network_address inet_ops WITH &&);

-- the router for a subnet should belong to that subnet!
ALTER TABLE public.subnet ADD CONSTRAINT subnet_router_agreement CHECK (subnet.router <<= subnet.network_address);

-- for some reason, subnet.network_address is an inet value rather than a cidr, so we need to manually check that it has a null host part
ALTER TABLE public.subnet ADD CONSTRAINT subnet_aligned CHECK (network(subnet.network_address) = subnet.network_address);
