CREATE OR REPLACE FUNCTION leave_recording.void_stale_trigger() RETURNS trigger AS $$
BEGIN
INSERT INTO leave_recording.leave_audit (post_id, start_date, end_date, leave_type, half_day_start_date, half_day_end_date, action)
SELECT post_history.id AS post_id,
       standing_leave.leave_date AS start_date,
       standing_leave.leave_date AS end_date,
       standing_leave.leave_type,
       standing_leave.am_off AND NOT standing_leave.pm_off AS half_day_end_date,
       standing_leave.pm_off AND NOT standing_leave.am_off AS half_day_start_date,
       'void' AS action
    FROM public.post_history
    INNER JOIN leave_recording.standing_leave
    ON standing_leave.post_id = post_history.id
    WHERE post_history.end_date IS NOT NULL AND standing_leave.leave_date > post_history.end_date
    ORDER BY post_history.id ASC, standing_leave.leave_date ASC
;
DELETE FROM leave_recording.standing_leave WHERE id IN
   (SELECT standing_leave.id
    FROM public.post_history
    INNER JOIN leave_recording.standing_leave
    ON standing_leave.post_id = post_history.id
    WHERE post_history.end_date IS NOT NULL AND standing_leave.leave_date > post_history.end_date
    );
RETURN NULL;
END;
$$
LANGUAGE plpgsql;
GRANT ALL ON FUNCTION leave_recording.void_stale_trigger() TO PUBLIC;
ALTER FUNCTION leave_recording.void_stale_trigger() OWNER TO dev;
