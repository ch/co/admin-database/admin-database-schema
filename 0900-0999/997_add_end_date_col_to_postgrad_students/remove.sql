DROP TRIGGER pg_end_date ON public.postgraduate_studentship;
DROP FUNCTION public.postgrad_end_dates_trig();
ALTER TABLE public.postgraduate_studentship DROP COLUMN end_date;
