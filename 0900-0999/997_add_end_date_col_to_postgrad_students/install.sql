ALTER TABLE public.postgraduate_studentship ADD COLUMN end_date DATE;

UPDATE public.postgraduate_studentship SET end_date = 
    CASE
        WHEN postgraduate_studentship.force_role_status_to_past = true THEN COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.date_withdrawn_from_register)
        WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
        WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
        WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN COALESCE(postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
        WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN COALESCE(postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
        ELSE NULL::date
    END
FROM public.postgraduate_studentship_type WHERE postgraduate_studentship_type.id = postgraduate_studentship.postgraduate_studentship_type_id;

-- the only differences between the end date in _postgrad_end_dates_v5 and the new end_date are the cases where force_role_status_to_past is set. I think
-- it's more incorrect to set a specific end date for those cases than leave it null, so that difference is OK.

-- now we need a trigger to update end_date when the old date columns change

CREATE FUNCTION public.postgrad_end_dates_trig() RETURNS TRIGGER AS
$b$
DECLARE
    my_end_date DATE := NULL;
    my_type TEXT;
BEGIN
    SELECT t.name INTO my_type FROM public.postgraduate_studentship_type t WHERE NEW.postgraduate_studentship_type_id = t.id;
    IF NEW.force_role_status_to_past THEN
        my_end_date := COALESCE(NEW.phd_date_awarded, NEW.phd_date_submitted, NEW.date_withdrawn_from_register);
    ELSE
        CASE my_type
            WHEN 'PhD' THEN
                my_end_date := COALESCE(NEW.phd_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'MPhil' THEN
                my_end_date := COALESCE(NEW.mphil_date_awarded, NEW.cpgs_or_mphil_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'CPGS' THEN
                my_end_date := COALESCE(NEW.cpgs_or_mphil_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'MSc' THEN
                my_end_date := COALESCE(NEW.msc_date_awarded, NEW.date_withdrawn_from_register);
        END CASE;
    END IF;
    NEW.end_date := my_end_date;
    RETURN NEW;
END;
$b$ LANGUAGE plpgsql;

ALTER FUNCTION public.postgrad_end_dates_trig() OWNER TO dev;

CREATE TRIGGER pg_end_date BEFORE INSERT OR UPDATE ON public.postgraduate_studentship FOR EACH ROW EXECUTE FUNCTION public.postgrad_end_dates_trig();
