CREATE OR REPLACE VIEW apps.user_accounts_to_create_in_ad
 AS
 SELECT person.id,
    person.crsid,
    btrim(COALESCE(person.known_as, person.first_names)::text)::character varying(32) AS first_names,
    btrim(person.surname::text)::character varying(32) AS surname,
    btrim(person.email_address::text)::character varying(48) AS email_address,
    max(COALESCE(research_group.active_directory_container, research_group.name)::text) AS rg
   FROM person
     LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     JOIN _physical_status_v3 physical_status ON person.id = physical_status.person_id
     LEFT JOIN _latest_role_v12 AS latest_role ON person.id = latest_role.person_id
  WHERE physical_status.status_id::text = 'Current'::text AND latest_role.post_category <> 'Visitor (Commercial)' AND person.crsid IS NOT NULL AND person.email_address::text ~~ '%@%'::text
  GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address;
