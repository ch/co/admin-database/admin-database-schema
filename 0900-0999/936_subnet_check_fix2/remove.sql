CREATE OR REPLACE PROCEDURE reverse_subnet.reverse_subnet_check()
 LANGUAGE plpgsql
AS $function$
    declare
        ip_id bigint;
    begin
        SELECT ip_address.id FROM ip_address LEFT JOIN reverse_subnet.reverse_subnet ON ip_address.reverse_subnet_id = reverse_subnet.id WHERE NOT (ip_address.ip <<= reverse_subnet.network_address) INTO ip_id;
        IF FOUND THEN
            RAISE EXCEPTION 'There are ip addresses that do not belong to their linked reverse subnets, e.g. ip with id %', ip_id;
        END IF;
    end;
$function$;
