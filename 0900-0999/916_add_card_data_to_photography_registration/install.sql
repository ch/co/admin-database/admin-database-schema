DROP VIEW hotwire3."10_View/People/Photography_Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Photography_Registration"
 AS
 WITH futuremost_role_estimated_leaving_date AS (
         SELECT DISTINCT ON (_all_roles_v13.person_id) _all_roles_v13.person_id,
            _all_roles_v13.estimated_leaving_date,
            _all_roles_v13.post_category
           FROM _all_roles_v13
          ORDER BY _all_roles_v13.person_id, _all_roles_v13.start_date DESC, _all_roles_v13.estimated_leaving_date DESC
        )
 SELECT person.id,
    person.crsid AS ro_crsid,
    person.gender_id,
    person.first_names,
    person.known_as,
    person.surname,
    person.date_of_birth,
    person.arrival_date AS ro_arrival_date,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
    futuremost_role.post_category AS ro_post_category,
    ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
    person.image_lo AS image,
    registration_form.already_has_university_card AS ro_had_card_when_registering,
    university_card.issued_at AS ro_university_card_issued_at,
    university_card.expires_at AS ro_university_card_expires_at,
    university_card.issue_number AS ro_university_card_issue_number,
    university_card.legacy_card_holder_id AS ro_university_card_identifier,
    university_card.mifare_number AS ro_university_card_mifare_number,
    university_card.mifare_id AS ro_university_card_mifare_identifier,
    university_card.barcode AS ro_university_card_barcode
   FROM person
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     LEFT JOIN futuremost_role_estimated_leaving_date futuremost_role ON person.id = futuremost_role.person_id
     LEFT JOIN registration.form AS registration_form ON registration_form._match_to_person_id = person.id
     LEFT JOIN public.university_card ON person.crsid = university_card.crsid
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Photography_Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Photography_Registration" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/People/Photography_Registration" TO photography;

CREATE TRIGGER hotwire3_photography_registration_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


CREATE TRIGGER hotwire3_photography_registration_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


