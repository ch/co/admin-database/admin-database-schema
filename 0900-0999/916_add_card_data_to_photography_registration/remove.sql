DROP VIEW hotwire3."10_View/People/Photography_Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Photography_Registration"
 AS
 WITH futuremost_role_estimated_leaving_date AS (
         SELECT DISTINCT ON (_all_roles_v13.person_id) _all_roles_v13.person_id,
            _all_roles_v13.estimated_leaving_date,
            _all_roles_v13.post_category
           FROM _all_roles_v13
          ORDER BY _all_roles_v13.person_id, _all_roles_v13.start_date DESC, _all_roles_v13.estimated_leaving_date DESC
        )
 SELECT person.id,
    person.crsid AS ro_crsid,
    person.gender_id,
    person.first_names,
    person.known_as,
    person.surname,
    person.date_of_birth,
    person.arrival_date AS ro_arrival_date,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
    futuremost_role.post_category AS ro_post_category,
    ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
    person.image_lo AS image
   FROM person
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     LEFT JOIN futuremost_role_estimated_leaving_date futuremost_role ON person.id = futuremost_role.person_id
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Photography_Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Photography_Registration" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/People/Photography_Registration" TO photography;

CREATE OR REPLACE FUNCTION hotwire3.photography_registration_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	begin
		if NEW.id is not null
		then
			update person set
				gender_id = NEW.gender_id,
				first_names = NEW.first_names,
				known_as = NEW.known_as,
				surname = NEW.surname,
				date_of_birth = NEW.date_of_birth,
                                image_lo = NEW.image
			where person.id = NEW.id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
                                image_lo
			) values (
				NEW.gender_id,
				NEW.first_names,
				NEW.known_as,
				NEW.surname,
				NEW.date_of_birth,
                                NEW.image
			) returning id into NEW.id;
		end if;
		perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);	
		return NEW;
	end
$BODY$;

ALTER FUNCTION hotwire3.photography_registration_update()
    OWNER TO dev;


CREATE TRIGGER hotwire3_photography_registration_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


CREATE TRIGGER hotwire3_photography_registration_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


