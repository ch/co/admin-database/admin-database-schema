-- this index dramatically speeds up the new apps.swipe_card_expiry query
CREATE INDEX university_card_crsid ON university_card(crsid);

DROP VIEW apps.swipe_card_expiry;

CREATE VIEW apps.swipe_card_expiry AS
-- CTE for the latest card for each person
WITH latest_cards AS (
    -- where we can identify a card holder, take only the card with the newest expiry date
    (
        SELECT
            newest_card.*
        FROM
            (
                SELECT DISTINCT
                    crsid
                FROM
                    university_card
                WHERE
                    crsid IS NOT NULL
            ) AS card_crsids
            JOIN LATERAL (
                SELECT
                    *
                FROM
                    university_card AS all_cards
                WHERE
                    card_crsids.crsid = all_cards.crsid
                ORDER BY
                    all_cards.expires_at DESC
                LIMIT 1
            ) AS newest_card ON true
    )
    UNION
    -- where we can't identify a card holder include the card unconditionally
    (
        SELECT
            *
        FROM
            university_card
        WHERE
            crsid IS NULL
    )
)
SELECT
    COALESCE( latest_cards.surname, person.surname::text )::pg_catalog.varchar AS surname,
    COALESCE( latest_cards.forename, person.first_names::text )::pg_catalog.varchar AS forenames,
    latest_cards.full_name::pg_catalog.varchar AS full_name,
    latest_cards.expires_at::date AS expires,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date,
    person.crsid,
    latest_cards.mifare_number
FROM
    latest_cards
    LEFT JOIN person ON lower( latest_cards.crsid ) = person.crsid::text
    LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
WHERE
    latest_cards.from_chemistry_card_feed = 't';


ALTER TABLE apps.swipe_card_expiry
    OWNER TO dev;

GRANT SELECT ON TABLE apps.swipe_card_expiry TO cos;
GRANT ALL ON TABLE apps.swipe_card_expiry TO dev;
GRANT SELECT ON TABLE apps.swipe_card_expiry TO leavers_management;
