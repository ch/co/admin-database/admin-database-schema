CREATE OR REPLACE VIEW apps.reboot_policy AS
  SELECT ip_address.hostname, reboot_policy.policy FROM ip_address
         JOIN mm_system_image_ip_address ON ip_address_id=ip_address.id 
	 JOIN system_image ON system_image.id=system_image_id 
	 JOIN reboot_policy ON reboot_policy.id=reboot_policy_id;

ALTER VIEW apps.reboot_policy OWNER TO dev;

GRANT SELECT ON apps.reboot_policy TO osbuilder;
