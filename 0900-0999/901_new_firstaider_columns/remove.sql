-- View: hotwire3.10_View/Safety/Firstaiders

DROP VIEW hotwire3."10_View/Safety/Firstaiders";

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Firstaiders"
 AS
 SELECT firstaider.id,
    person.image_lo AS ro_image,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.mental_health_awareness_trained,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_hid USING (building_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
          WHERE p2.id = firstaider.person_id
          ORDER BY room.name), '/'::text) AS ro_room,
    array_to_string(ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON mm_person_dept_telephone_number.person_id = p2.id
             JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
          WHERE p2.id = firstaider.person_id), ' / '::text) AS ro_dept_telephone_numbers,
    _physical_status.status_id AS status,
    firstaider.qualification_up_to_date AS ro_qualification_up_to_date,
    _latest_role.post_category AS ro_post_category,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
        CASE
            WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN firstaider.qualification_up_to_date = false THEN 'red'::text
            ELSE NULL::text
        END AS _cssclass
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     LEFT JOIN _latest_role_v12 _latest_role ON firstaider.person_id = _latest_role.person_id
     LEFT JOIN apps.person_futuremost_role futuremost_role ON firstaider.person_id = futuremost_role.person_id
     JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = firstaider.person_id;

ALTER TABLE hotwire3."10_View/Safety/Firstaiders"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Firstaiders" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO firstaiders;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO hr;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Firstaiders" TO www_sites;


CREATE OR REPLACE RULE firstaiders_del AS
    ON DELETE TO hotwire3."10_View/Safety/Firstaiders"
    DO INSTEAD
(DELETE FROM firstaider
  WHERE (firstaider.id = old.id));

CREATE OR REPLACE RULE firstaiders_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Firstaiders"
    DO INSTEAD
(INSERT INTO firstaider (person_id, firstaider_funding_id, qualification_date, requalify_date, hf_cn_trained, mental_health_awareness_trained)
  VALUES (new.person_id, new.firstaider_funding_id, new.qualification_date, new.requalify_date, new.hf_cn_trained, new.mental_health_awareness_trained)
  RETURNING firstaider.id,
    NULL::oid AS oid,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.mental_health_awareness_trained,
    NULL::text AS text,
    NULL::text AS text,
    NULL::text AS text,
    NULL::text AS text,
    NULL::character varying(20) AS "varchar",
    NULL::boolean AS bool,
    NULL::character varying(80) AS "varchar",
    NULL::date AS date,
    NULL::text AS text);

CREATE OR REPLACE RULE firstaiders_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Firstaiders"
    DO INSTEAD
(UPDATE firstaider SET person_id = new.person_id, firstaider_funding_id = new.firstaider_funding_id, qualification_date = new.qualification_date, requalify_date = new.requalify_date, hf_cn_trained = new.hf_cn_trained, mental_health_awareness_trained = new.mental_health_awareness_trained
  WHERE (firstaider.id = old.id));

-- View: www.first_aiders_v3

CREATE OR REPLACE VIEW www.first_aiders_v3
 AS
 SELECT DISTINCT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid AS person,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained,
        CASE
            WHEN firstaider.mental_health_awareness_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS mental_health_awareness_trained,
    building_hid.building_hid AS building,
    building_floor_hid.building_floor_hid AS building_floor,
    array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms,
    ps.status_id AS status,
    (building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END AS floor_sort,
        CASE
            WHEN firstaider.requalify_date > ('now'::text::date - '1 mon'::interval) THEN true
            ELSE false
        END AS qualification_up_to_date
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     LEFT JOIN mm_person_room USING (person_id)
     LEFT JOIN room ON mm_person_room.room_id = room.id
     LEFT JOIN building_hid ON room.building_id = building_hid.building_id
     LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
     JOIN _physical_status_v3 ps USING (person_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END), firstaider.id, (person.image_lo::bigint), person_hid.person_hid, firstaider.firstaider_funding_id, firstaider.requalify_date, (
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END), building_hid.building_hid, building_floor_hid.building_floor_hid, (array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text)), ps.status_id;


ALTER TABLE public.firstaider DROP COLUMN hf_cn_qualification_date ;
ALTER TABLE public.firstaider DROP COLUMN hf_cn_requalify_date ;

