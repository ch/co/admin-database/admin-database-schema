-- Need to reproduce this table
--     Column     |         Type          | Collation | Nullable | Default | Storage  | Description
---------------+-----------------------+-----------+----------+---------+----------+-------------
-- id            | bigint                |           |          |         | plain    |
-- crsid         | character varying(8)  |           |          |         | extended |
-- first_names   | character varying(32) |           |          |         | extended |
-- surname       | character varying(32) |           |          |         | extended |
-- email_address | character varying(48) |           |          |         | extended |
-- rg            | text                  |           |          |         | extended |
--
-- but change the rows included to be Current people with active roles that aren't Visitor (Commercial).
-- SPRI are done by hand by setting end dates.


CREATE VIEW apps.user_accounts_to_create_in_ad_v2 AS
 SELECT person.id,
    person.crsid,
    btrim(COALESCE(person.known_as, person.first_names)::text)::character varying(32) AS first_names,
    btrim(person.surname::text)::character varying(32) AS surname,
    btrim(person.email_address::text)::character varying(48) AS email_address,
    max(COALESCE(research_group.active_directory_container, research_group.name)::text) AS rg
   FROM person
     LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     JOIN _physical_status_v3 physical_status ON person.id = physical_status.person_id
     LEFT JOIN  _all_roles_with_predicted_status all_roles ON person.id = all_roles.person_id
  WHERE physical_status.status_id::text = 'Current'::text AND all_roles.post_category::text <> 'Visitor (Commercial)'::text AND person.crsid IS NOT NULL AND person.email_address::text ~~ '%@%'::text AND all_roles.status = 'Current'
  GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address;

ALTER VIEW apps.user_accounts_to_create_in_ad_v2 OWNER TO dev;

GRANT SELECT ON apps.user_accounts_to_create_in_ad_v2 TO ad_accounts;
