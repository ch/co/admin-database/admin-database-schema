DROP VIEW hotwire3."10_View/Network/Switches/Switches";

CREATE VIEW hotwire3."10_View/Network/Switches/Switches"
 AS
 SELECT switch.id,
    hardware.name::character varying(40) AS name,
    hardware.id AS _hardware_id,
    system_image.id AS _system_image_id,
    hardware.manufacturer::character varying(60) AS manufacturer,
    hardware.hardware_type_id,
    system_image.wired_mac_1,
    switch.switch_model_id,
    switch.ignore_config,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.serial_number::character varying(40) AS serial_number,
    hardware.asset_tag::character varying(40) AS asset_tag,
    hardware.date_purchased,
    hardware.date_configured,
    hardware.warranty_end_date,
    hardware.date_decommissioned,
    hardware.warranty_details,
    hardware.room_id,
    switch.cabinet_id,
    hardware.owner_id,
    system_image.research_group_id,
    hardware.comments::text AS comments,
    ARRAY( SELECT mm_switch_goal_applies_to_switch.switch_config_goal_id
           FROM mm_switch_goal_applies_to_switch
          WHERE mm_switch_goal_applies_to_switch.switch_id = switch.id) AS switch_config_goal_id,
    switch.extraconfig::text AS extraconfig,
    hotwire3.to_hwsubviewb('10_View/Network/_Cabinet_for_switch'::character varying, 'switch_id'::character varying, '10_View/Network/Cabinets'::character varying, 'cabinet_id'::character varying, 'id'::character varying) AS "Cabinet_details",
    hotwire3.to_hwsubviewb('10_View/Network/Switches/_Switchport_ro'::character varying, 'switch_id'::character varying, '10_View/Network/Switches/Switch_Ports'::character varying, NULL::character varying, NULL::character varying) AS "Ports",
    hotwire3.to_hwsubviewb('10_View/Network/Switches/_switch_config_subview'::character varying, 'id'::character varying, '10_View/Network/Switches/Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config"
   FROM switch
     JOIN hardware ON switch.hardware_id = hardware.id
     JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Network/Switches/Switches"
    OWNER TO dev;

GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switches" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switches" TO dev;


CREATE OR REPLACE RULE hotwire3_view_network_switch_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switches"
    DO INSTEAD
(DELETE FROM switch
  WHERE (switch.id = old.id));


CREATE OR REPLACE RULE hotwire3_view_network_switch_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switches"
    DO INSTEAD
( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id, comments = new.comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1
  WHERE (system_image.id = old._system_image_id);
 SELECT fn_mm_array_update((new.switch_config_goal_id)::bigint[], (old.switch_config_goal_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update;
 UPDATE switch SET ignore_config = new.ignore_config, switch_model_id = new.switch_model_id, extraconfig = (new.extraconfig)::character varying, cabinet_id = new.cabinet_id
  WHERE (switch.id = old.id);
);

