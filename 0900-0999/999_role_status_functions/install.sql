CREATE FUNCTION public.role_status(
    start_date DATE,
    end_date DATE,
    funding_end_date DATE,
    force_role_status_to_past BOOLEAN,
    role_type TEXT) RETURNS VARCHAR(10)
AS
$BODY$
DECLARE
    status_id VARCHAR(10);
BEGIN
    -- calculates the current status for a role. The rules vary depending on type of role!
    -- does not take intended end date into account
    SELECT
        CASE
            WHEN force_role_status_to_past = true THEN 'Past'::character varying(10)
            WHEN end_date < 'now'::text::date THEN 'Past'::character varying(10)
            WHEN funding_end_date < 'now'::text::date AND role_type <> 'postgraduate_studentship'::text THEN 'Past'::character varying(10)
            WHEN start_date <= 'now'::text::date THEN 'Current'::character varying(10)
            WHEN start_date > 'now'::text::date THEN 'Future'::character varying(10)
            ELSE 'Unknown'::character varying(10)
        END AS status_id INTO status_id;
    RETURN status_id;
END;
$BODY$ LANGUAGE plpgsql;

ALTER FUNCTION public.role_status OWNER TO dev;

CREATE FUNCTION public.role_predicted_status(
    start_date DATE,
    end_date DATE,
    intended_end_date DATE,
    funding_end_date DATE,
    force_role_status_to_past BOOLEAN,
    role_type TEXT) RETURNS VARCHAR(10)
AS
$BODY$
DECLARE
    status_id VARCHAR(10);
BEGIN
    -- calculates the predicted status for a role. The rules vary depending on type of role!
    -- the difference between this and role_status() is that this one uses intended end date
    -- and assumes the person is Past if that has passed
    SELECT
        CASE
            WHEN force_role_status_to_past = true THEN 'Past'::character varying(10)
            WHEN end_date < 'now'::text::date THEN 'Past'::character varying(10)
            WHEN intended_end_date < 'now'::text::date THEN 'Past'::character varying(10)
            WHEN funding_end_date < 'now'::text::date AND role_type <> 'postgraduate_studentship'::text THEN 'Past'::character varying(10)
            WHEN start_date <= 'now'::text::date THEN 'Current'::character varying(10)
            WHEN start_date > 'now'::text::date THEN 'Future'::character varying(10)
            ELSE 'Unknown'::character varying(10)
        END AS status_id INTO status_id;
    RETURN status_id;
END;
$BODY$ LANGUAGE plpgsql;

ALTER FUNCTION public.role_status OWNER TO dev;
