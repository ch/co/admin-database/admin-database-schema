ALTER TABLE subnet 
  ADD COLUMN preferred_dns1 inet DEFAULT '131.111.112.9'::inet,
  ADD COLUMN preferred_dns2 inet DEFAULT '131.111.112.138'::inet,
  ADD COLUMN preferred_dns3 inet DEFAULT '172.26.81.157'::inet;

