CREATE FUNCTION public.update_selfservice_access_role(
	)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    username varchar;
BEGIN
    -- people without database roles
    FOR username IN
        SELECT DISTINCT crsid 
        FROM person
        LEFT JOIN pg_roles ON person.crsid=rolname
        LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
        WHERE crsid IS NOT NULL AND rolname IS NULL AND _physical_status_v2.status_id <> 'Past'
    LOOP
        -- create role
        RAISE NOTICE 'Creating role for %', username;
        EXECUTE format('CREATE ROLE %I LOGIN IN ROLE selfservice_access',username);
    END LOOP;
    -- people with roles but not in the group
    FOR username IN
        SELECT DISTINCT crsid
        FROM person
        LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
        WHERE (
            SELECT count(*)
            FROM pg_auth_members
            JOIN pg_roles u ON member=u.oid
            JOIN pg_roles g ON roleid=g.oid
            WHERE u.rolname=crsid AND g.rolname='selfservice_access'
        )=0 AND crsid IS NOT NULL AND _physical_status_v2.status_id <> 'Past'
    LOOP
        -- put into group
        RAISE NOTICE 'Putting % in group', username;
        EXECUTE format('GRANT selfservice_access TO %I',username);
    END LOOP;

END;
$BODY$;

ALTER FUNCTION public.update_selfservice_access_role()
    OWNER TO dev;
