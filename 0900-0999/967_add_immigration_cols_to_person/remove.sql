ALTER TABLE public.person DROP COLUMN passport_issuing_country_id;
ALTER TABLE public.person DROP COLUMN passport_number;
ALTER TABLE public.person DROP COLUMN passport_expiry_date;
ALTER TABLE public.person DROP COLUMN immigration_status_id;
ALTER TABLE public.person DROP COLUMN immigration_status_expiry_date;
ALTER TABLE public.person DROP COLUMN brp_number;
