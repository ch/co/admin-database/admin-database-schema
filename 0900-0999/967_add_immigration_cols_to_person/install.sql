ALTER TABLE public.person ADD COLUMN passport_issuing_country_id BIGINT;
ALTER TABLE public.person ADD COLUMN passport_number VARCHAR;
ALTER TABLE public.person ADD COLUMN passport_expiry_date DATE;
ALTER TABLE public.person ADD COLUMN immigration_status_id BIGINT NOT NULL DEFAULT 1;
ALTER TABLE public.person ADD COLUMN immigration_status_expiry_date DATE;
ALTER TABLE public.person ADD COLUMN brp_number VARCHAR;

ALTER TABLE public.person ADD CONSTRAINT person_fk_immigration_status FOREIGN KEY (immigration_status_id) REFERENCES public.immigration_status(id);
ALTER TABLE public.person ADD CONSTRAINT person_fk_passport_issuing_country FOREIGN KEY (passport_issuing_country_id) REFERENCES public.nationality(id);
