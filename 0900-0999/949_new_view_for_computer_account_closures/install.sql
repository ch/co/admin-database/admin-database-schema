-- Want to reproduce this table but change the definition of which rows are
-- included.

--                      View "apps.user_accounts_to_disable"
--        Column        |         Type          | Collation | Nullable | Default
-- ---------------------+-----------------------+-----------+----------+---------
-- crsid                | character varying(8)  |           |          |
-- surname              | character varying(32) |           |          |
-- first_names          | character varying(32) |           |          |
-- post_category        | character varying(80) |           |          |
-- leaving_date         | date                  |           |          |
-- next_role_start_date | date                  |           |          |


-- The rows should include all people who were given Chemistry Admitto accounts based on
-- being current (ie physically present, which due to our processes implies
-- they also had a current role at the time they were first marked physically
-- present as we don't add people to the database without roles) but whose
-- reason for being here (employment contract, visitor arrangement, course of study etc)
-- has ended. We are therefore looking for those who have no current roles, or
-- have been marked as physically past. We should not have to do the latter,
-- but it's common for role end dates not to be updated when people leave so as
-- a matter of practicality this gives better results than relying on roles
-- alone. Accounts for 'remote' users (ie those not physically present)
-- continue to be handled outside the database for now by setting the account
-- expiry date in AD, which in the code that closes accounts overrides the
-- account closure logic in this view.

-- We don't allow 'Visitor (Commercial)' roles (v-17) to count towards current
-- roles. 

-- SPRI people are explicitly excluded as they are never physically current or have roles so
-- their accounts would always be closed, which we do not want. 

CREATE OR REPLACE VIEW apps.user_accounts_to_disable_v2 AS
SELECT
    person.crsid,
    person.surname,
    person.first_names,
    latest_post_category.post_category,

    -- sometimes have no idea of end date so we assume at least six weeks ago if no data
    -- which has the effect of closing the account right away as we only allow 30 days grace
    coalesce(person.leaving_date, past_roles.leaving_date, CURRENT_DATE - 42) AS leaving_date,

    -- check for qualifying roles starting in the future as we don't want to
    -- close accounts if the person is returning soon. 'soon' is defined in the
    -- account closure code, not here, this view just gives the estimated date
    -- of return.
    --
    -- We include dates up to one month in the past as 'future'
    -- start dates, the reason being that people who have been with us before
    -- and whose latest arrival is known about in advance (mainly new
    -- academics, new postgrads) can get their roles entered into the
    -- database in advance of arrival but do not do their registrations and so
    -- become 'Current' and eligible for accounts to be opened until some days
    -- after their new role starts, which can lead to their accounts being
    -- temporarily closed as they're still 'Past' despite the current
    -- role. We try to avoid that with the October intake of postgrads by
    -- granting them all what's effectively a short term remote user account
    -- arrangement expiring at the end of November, but this extra month guards
    -- against the problem arising for any others.
    (
        SELECT
            min(all_future_start_dates.start_date) AS min_start FROM (
            SELECT
                all_roles.start_date FROM _all_roles_with_predicted_status all_roles
            WHERE
                all_roles.person_id = person.id
                AND all_roles.post_category_id <> 'v-17'
                AND all_roles.start_date > ('now'::text::date - '1 mon'::interval)) all_future_start_dates) AS next_role_start_date
FROM
    person
    JOIN _physical_status_v3 ps ON person.id = ps.person_id
    -- outer join against the current roles, so we can find rows with no current roles
    LEFT JOIN (
        SELECT
            person_id,
            role_id,
            status
        FROM
            _all_roles_with_predicted_status role
        WHERE
            role.status = 'Current'
            AND role.post_category_id <> 'v-17') current_roles ON person.id = current_roles.person_id
    -- to find the most recent role end date for past roles that qualified for accounts, allowing us to
    -- give a grace period when closing accounts
    LEFT JOIN (
        SELECT
            person_id,
            max(least (end_date, estimated_leaving_date)) AS leaving_date
        FROM
            _all_roles_with_predicted_status role
        WHERE
            role.status = 'Past' AND role.post_category_id <> 'v-17'
        GROUP BY
            person_id) past_roles ON person.id = past_roles.person_id
    -- to find the most accurate description of the person's post category
    -- We include all roles, not just those which qualify for accounts, as
    -- this is not used in account closure decisions, but it's useful for
    -- investigating problems
    LEFT JOIN LATERAL (
        SELECT post_category FROM _all_roles_with_predicted_status all_roles
        WHERE all_roles.person_id = person.id
        ORDER BY start_date
        LIMIT 1
    ) latest_post_category ON TRUE
WHERE
    person.is_spri IS NOT TRUE
    AND (current_roles.role_id IS NULL
        OR ps.status_id = 'Past')
;

ALTER VIEW apps.user_accounts_to_disable_v2 OWNER TO dev;
GRANT SELECT ON TABLE apps.user_accounts_to_disable_v2 TO ad_accounts;
