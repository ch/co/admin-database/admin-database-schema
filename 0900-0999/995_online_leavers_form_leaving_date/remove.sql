DROP VIEW leavers_form.role_end_date;

CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent AS
  SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.leaving_date,
    leavers_form_overview_.id AS leavers_form_id,
    leavers_form_overview_.form_exists AS leavers_form_exists,
    leavers_form_overview_.last_updated AS leavers_form_last_updated
  FROM public.person
  LEFT JOIN
  (SELECT id, TRUE as form_exists, last_updated, person FROM leavers_form.leavers_form_overview WHERE deactivated IS NULL) leavers_form_overview_
  ON
  leavers_form_overview_.person = person.id
  WHERE person.leaving_date > (current_date - concat(31, ' DAYS')::INTERVAL) AND
        person.leaving_date < (current_date + concat(31, ' DAYS')::INTERVAL)
  ORDER BY person_surname, person_name ASC;

ALTER TABLE leavers_form.leaver_soon_or_recent
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent TO dev;
GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent TO onlineleaversform;


DROP VIEW leavers_form.leaving_date_estimate;
DROP VIEW leavers_form.leaving_date;
