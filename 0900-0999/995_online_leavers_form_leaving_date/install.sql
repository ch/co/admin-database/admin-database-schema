CREATE OR REPLACE VIEW leavers_form.leaving_date AS
  SELECT
    person.id AS person_id,
    person.leaving_date
  FROM
    public.person
;

ALTER TABLE leavers_form.leaving_date
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaving_date TO dev;
GRANT SELECT, UPDATE(leaving_date) ON TABLE leavers_form.leaving_date TO onlineleaversform;


-- N.B. the following does not cope with people who gaps between roles
CREATE OR REPLACE VIEW leavers_form.leaving_date_estimate AS
  SELECT
    person.id AS person_id,
    COALESCE(person.leaving_date, (SELECT MAX(COALESCE(role.end_date, role.intended_end_date)) FROM leavers_form.role WHERE role.person_id = person.id)) AS leaving_date
  FROM
    public.person
;

ALTER TABLE leavers_form.leaving_date_estimate
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaving_date_estimate TO dev;
GRANT SELECT ON TABLE leavers_form.leaving_date_estimate TO onlineleaversform;


CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent AS
  SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    leaving_date_estimate.leaving_date,
    leavers_form_overview_.id AS leavers_form_id,
    leavers_form_overview_.form_exists AS leavers_form_exists,
    leavers_form_overview_.last_updated AS leavers_form_last_updated
  FROM public.person
  LEFT JOIN
  (SELECT id, TRUE as form_exists, last_updated, person FROM leavers_form.leavers_form_overview WHERE deactivated IS NULL) leavers_form_overview_
  ON
  leavers_form_overview_.person = person.id
  JOIN
  leavers_form.leaving_date_estimate
  ON
  leaving_date_estimate.person_id = person.id
  WHERE person.leaving_date > (current_date - concat(31, ' DAYS')::INTERVAL) AND
        person.leaving_date < (current_date + concat(31, ' DAYS')::INTERVAL)
  ORDER BY person_surname, person_name ASC;


CREATE OR REPLACE VIEW leavers_form.role_end_date AS
  SELECT
    role.role_id,
    role.role_type,
    role.end_date
  FROM
    leavers_form.role
;

ALTER TABLE leavers_form.role_end_date
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.role_end_date TO dev;
GRANT SELECT, UPDATE(end_date) ON TABLE leavers_form.role_end_date TO onlineleaversform;


CREATE OR REPLACE FUNCTION leavers_form.set_role_end_date_do()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
   _sql text;
  begin
    if NEW.role_id != OLD.role_id OR NEW.role_type != OLD.role_type
    then
      RAISE 'Not possible to change role id or type';
    end if;

    if NEW.role_id is not null AND NEW.role_type is not null
    then
      if NEW.role_type != 'postgraduate_studentship' then
        _sql = format('UPDATE public.%1$I SET end_date = $1 WHERE id = $2'
                   , NEW.role_type);
        EXECUTE _sql USING NEW.end_date, NEW.role_id;
        return NEW;
      else
        RAISE 'Cannot set end date for postgraduate_studentship';
      end if;
    end if;
    return NULL;
  end;
$BODY$;

ALTER FUNCTION leavers_form.set_role_end_date_do()
    OWNER TO dev;


CREATE TRIGGER leavers_form_set_role_end_date INSTEAD OF UPDATE ON leavers_form.role_end_date
    FOR EACH ROW
    EXECUTE FUNCTION leavers_form.set_role_end_date_do();
