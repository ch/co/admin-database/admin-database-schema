DROP RULE check_subnet_on_insert ON public.subnet;
DROP RULE check_subnet_on_update ON public.subnet;

DROP FUNCTION public.subnet_check_helper();
DROP PROCEDURE public.subnet_check();

DROP RULE check_reverse_subnet_on_insert ON reverse_subnet.reverse_subnet;
DROP RULE check_reverse_subnet_on_update ON reverse_subnet.reverse_subnet;

DROP FUNCTION reverse_subnet.reverse_subnet_check_helper();
DROP PROCEDURE reverse_subnet.reverse_subnet_check();

