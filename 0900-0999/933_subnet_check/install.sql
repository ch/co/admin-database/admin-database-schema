CREATE OR REPLACE PROCEDURE public.subnet_check()
 LANGUAGE plpgsql
AS $function$
    declare
        ip_id bigint;
    begin
        SELECT ip_address.id FROM ip_address LEFT JOIN subnet ON ip_address.subnet_id = subnet.id WHERE NOT (ip_address.ip <<= subnet.network_address) INTO ip_id;
        IF FOUND THEN
            RAISE EXCEPTION 'There are ip addresses that do not belong to their linked subnets, e.g. ip with id %', ip_id;
        END IF;
    end;
$function$;

ALTER PROCEDURE public.subnet_check() OWNER TO dev;


-- can only CALL a procedure from a plpgsql function...
CREATE OR REPLACE FUNCTION public.subnet_check_helper() RETURNS VOID AS
$BODY$
BEGIN
CALL public.subnet_check();
END;
$BODY$
LANGUAGE plpgsql;
;

CREATE RULE check_subnet_on_insert AS ON INSERT TO public.subnet DO ALSO SELECT public.subnet_check_helper();
CREATE RULE check_subnet_on_update AS ON UPDATE TO public.subnet DO ALSO SELECT public.subnet_check_helper();


CREATE OR REPLACE PROCEDURE reverse_subnet.reverse_subnet_check()
 LANGUAGE plpgsql
AS $function$
    declare
        ip_id bigint;
    begin
        SELECT ip_address.id FROM ip_address LEFT JOIN reverse_subnet ON ip_address.reverse_subnet_id = reverse_subnet.id WHERE NOT (ip_address.ip <<= reverse_subnet.network_address) INTO ip_id;
        IF FOUND THEN
            RAISE EXCEPTION 'There are ip addresses that do not belong to their linked reverse subnets, e.g. ip with id %', ip_id;
        END IF;
    end;
$function$;
ALTER PROCEDURE reverse_subnet.reverse_subnet_check() OWNER TO dev;

CREATE OR REPLACE FUNCTION reverse_subnet.reverse_subnet_check_helper() RETURNS VOID AS
$BODY$
BEGIN
CALL reverse_subnet.reverse_subnet_check();
END;
$BODY$
LANGUAGE plpgsql;
;
CREATE RULE check_reverse_subnet_on_insert AS ON INSERT TO reverse_subnet.reverse_subnet DO ALSO SELECT reverse_subnet.reverse_subnet_check_helper();
CREATE RULE check_reverse_subnet_on_update AS ON UPDATE TO reverse_subnet.reverse_subnet DO ALSO SELECT reverse_subnet.reverse_subnet_check_helper();


-- note that we do not have a check that all subnets have their corresponding reverse subnets defined
-- nor that subnets are not missing IP addresses

