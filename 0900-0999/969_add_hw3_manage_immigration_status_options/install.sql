CREATE VIEW hotwire3."10_View/People/Immigration_Statuses" AS
SELECT id, status FROM public.immigration_status;

ALTER VIEW hotwire3."10_View/People/Immigration_Statuses" OWNER TO dev;

GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/People/Immigration_Statuses" TO hr;
