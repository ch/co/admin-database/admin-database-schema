CREATE TABLE reboot_policy (
  id SERIAL NOT NULL PRIMARY KEY,
  policy TEXT, -- the string we expect to template in ansible and refernce in scripts
  description TEXT -- human readable version as e.g. displayed in hotwire
);

ALTER TABLE reboot_policy OWNER TO dev;
GRANT ALL ON TABLE reboot_policy TO dev;

GRANT SELECT ON TABLE reboot_policy TO osbuilder;

-- Seed an initial value, mainly so we can set the default reboot_policy_id next.
INSERT INTO reboot_policy (policy, description) VALUES ('any_time', 'Any time');

CREATE VIEW hotwire3.reboot_policy_hid AS
  SELECT
    reboot_policy.id AS reboot_policy_id,
    reboot_policy.description AS reboot_policy_hid
  FROM reboot_policy;

-- default to the 'Any time' policy we inserted above.
ALTER TABLE system_image 
  ADD COLUMN reboot_policy_id integer NOT NULL DEFAULT 1 REFERENCES reboot_policy(id);


-- Rule: hotwire3_view_system_image_all_del ON hotwire3."10_View/Computers/System_Instances"
-- DROP Rule hotwire3_view_system_image_all_del ON hotwire3."10_View/Computers/System_Instances";

-- View: hotwire3."10_View/Computers/System_Instances"

DROP VIEW hotwire3."10_View/Computers/System_Instances";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Instances"
 AS
 SELECT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    ARRAY( SELECT mm_dom0_domu.dom0_id
           FROM mm_dom0_domu
          WHERE mm_dom0_domu.domu_id = system_image.id) AS host_system_image_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    hardware.date_purchased AS date_hardware_purchased,
    hardware.warranty_end_date AS hardware_warranty_end_date,
    system_image.hobbit_tier,
    system_image.hobbit_flags,
    system_image.is_development_system,
    system_image.reboot_policy_id,
    system_image.expiry_date AS system_image_expiry_date,
    hardware.delete_after_date AS hardware_delete_after_date,
    system_image.is_managed_mac AS is_managed_macintosh,
    system_image.hardware_id AS hardware_pk,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/System_Instances"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/System_Instances" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO dev;


CREATE OR REPLACE RULE hotwire3_view_system_image_all_del AS
    ON DELETE TO hotwire3."10_View/Computers/System_Instances"
    DO INSTEAD
(DELETE FROM system_image
  WHERE (system_image.id = old.id));

CREATE TRIGGER hotwire3_view_system_image_all_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/Computers/System_Instances"
    FOR EACH ROW
    EXECUTE PROCEDURE hotwire3.system_image_all_upd();


CREATE TRIGGER hotwire3_view_system_image_all_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/Computers/System_Instances"
    FOR EACH ROW
    EXECUTE PROCEDURE hotwire3.system_image_all_upd();

-- FUNCTION: hotwire3.system_image_all_upd()

CREATE OR REPLACE FUNCTION hotwire3.system_image_all_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 
  declare
          v_system_image_id BIGINT;
          v_hardware_id BIGINT;
  begin
 
  IF NEW.id IS NOT NULL THEN
 
    v_system_image_id := NEW.id;
    v_hardware_id := NEW.hardware_pk;
 
    UPDATE hardware SET
         manufacturer=NEW.manufacturer, 
         model=NEW.model, 
         name=NEW.hardware_name,
         hardware_type_id=NEW.hardware_type_id, 
         asset_tag = NEW.asset_tag,
         serial_number = NEW.serial_number,
         monitor_serial_number = NEW.monitor_serial_number,
         room_id=NEW.room_id,
         owner_id=NEW.owner_id,
         comments=NEW.hardware_comments::varchar(1000),
         date_purchased = NEW.date_hardware_purchased,
         warranty_end_date = NEW.hardware_warranty_end_date,
         delete_after_date = NEW.hardware_delete_after_date
    WHERE hardware.id = v_hardware_id;
 
    UPDATE system_image SET
          user_id=NEW.user_id,
          research_group_id=NEW.research_group_id,
          operating_system_id = NEW.operating_system_id,
          wired_mac_1 = NEW.wired_mac_1, 
          wired_mac_2 = NEW.wired_mac_2,
          wireless_mac = NEW.wireless_mac,
          comments = NEW.system_image_comments::varchar(1000),
          hobbit_tier = NEW.hobbit_tier,
          hobbit_flags = NEW.hobbit_flags,
          is_development_system = NEW.is_development_system,
	  reboot_policy_id = NEW.reboot_policy_id,
          expiry_date = NEW.system_image_expiry_date,
          is_managed_mac = NEW.is_managed_macintosh
    WHERE system_image.id = NEW.id;
 
  ELSE
 
    v_system_image_id := nextval('system_image_id_seq');
    v_hardware_id := nextval('hardware_id_seq');
 
    INSERT INTO hardware
          (manufacturer, model, name, hardware_type_id, asset_tag, serial_number, monitor_serial_number, room_id, owner_id, comments, date_purchased, warranty_end_date, delete_after_date )
    VALUES
          (NEW.manufacturer, NEW.model, NEW.hardware_name, NEW.hardware_type_id, NEW.asset_tag, NEW.serial_number, NEW.monitor_serial_number,
           NEW.room_id, NEW.owner_id, NEW.hardware_comments::varchar(1000), NEW.date_hardware_purchased, NEW.hardware_warranty_end_date, NEW.hardware_delete_after_date) returning id INTO v_hardware_id;
 
    INSERT INTO system_image
          (hardware_id, operating_system_id, wired_mac_1, wired_mac_2,
  wireless_mac,
  comments,user_id,research_group_id,hobbit_tier,hobbit_flags,is_development_system,reboot_policy_id,expiry_date,is_managed_mac)
    VALUES
          (v_hardware_id, NEW.operating_system_id,
           NEW.wired_mac_1, NEW.wired_mac_2, NEW.wireless_mac,
           NEW.system_image_comments::varchar(1000),NEW.user_id,NEW.research_group_id,NEW.hobbit_tier,NEW.hobbit_flags,NEW.is_development_system,NEW.reboot_policy_id,NEW.system_image_expiry_date,NEW.is_managed_macintosh) RETURNING id into v_system_image_id;
 
  END IF;
  PERFORM fn_mm_array_update(NEW.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,v_system_image_id);
 
  return NEW;
 
  end;
 
$BODY$;

ALTER FUNCTION hotwire3.system_image_all_upd()
    OWNER TO dev;

