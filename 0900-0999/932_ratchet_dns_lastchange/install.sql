CREATE TRIGGER reverse_subnet_dns_lastchange BEFORE UPDATE ON reverse_subnet.reverse_subnet FOR EACH ROW EXECUTE FUNCTION public.dns_lastchange_ratchet_on_update();
