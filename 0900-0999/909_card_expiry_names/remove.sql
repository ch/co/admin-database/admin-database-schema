DROP VIEW apps.swipe_card_expiry;
CREATE VIEW apps.swipe_card_expiry
 AS
 SELECT university_card.surname::varchar,
    university_card.forename::varchar as forenames,
    university_card.expires_at::date as expires,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date
   FROM university_card
     LEFT JOIN person ON lower(university_card.crsid::text) = person.crsid::text
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id;

ALTER TABLE apps.swipe_card_expiry OWNER TO dev;

GRANT SELECT ON TABLE apps.swipe_card_expiry TO cos;
GRANT ALL ON TABLE apps.swipe_card_expiry TO dev;
GRANT SELECT ON TABLE apps.swipe_card_expiry TO leavers_management;
