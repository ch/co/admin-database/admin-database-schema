CREATE OR REPLACE VIEW apps.process_leavers_v3
 AS
 WITH emails_of_current_people AS (
     SELECT ps.person_id, person.email_address
     FROM _physical_status_v3 ps
     JOIN person ON person.id = ps.person_id
     WHERE ps.status_id = 'Current'
 )
 SELECT DISTINCT person.surname,
    person.first_names,
    title_hid.long_title AS title,
    person.email_address AS email,
    person_hid.person_hid AS full_name,
    supervisor_hid.supervisor_hid AS supervisor,
    supervisor.email_address AS supervisor_email,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date,
    person_futuremost_role.post_category,
    (COALESCE(title_hid.salutation_title, person.first_names)::text || ' '::text) || person.surname::text AS salutation,
    (COALESCE(pi_title.salutation_title, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS pi_salutation,
    (((COALESCE(title_hid.long_title, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS person_title_surname,
        CASE
            WHEN firstaider.id IS NOT NULL THEN 't'::text
            ELSE 'f'::text
        END AS is_firstaider,
    string_agg(DISTINCT admin_email.email_address::text, ','::text) AS additional_contact_emails
   FROM person
     LEFT JOIN title_hid USING (title_id)
     JOIN person_hid ON person.id = person_hid.person_id
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
     LEFT JOIN person supervisor ON person_futuremost_role.supervisor_id = supervisor.id
     LEFT JOIN supervisor_hid USING (supervisor_id)
     LEFT JOIN title_hid pi_title ON supervisor.title_id = pi_title.title_id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN firstaider ON person.id = firstaider.person_id
     LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     LEFT JOIN person administrator ON research_group.administrator_id = administrator.id
     LEFT JOIN emails_of_current_people admin_email ON research_group.administrator_id = admin_email.person_id
  WHERE ps.status_id::text = 'Current'::text AND person_futuremost_role.post_category::text <> 'Assistant staff'::text AND person_futuremost_role.post_category::text <> 'Part III'::text
  GROUP BY person.surname, person.first_names, title_hid.long_title, person.email_address, person_hid.person_hid, supervisor_hid.supervisor_hid, supervisor.email_address, person_futuremost_role.estimated_leaving_date, person_futuremost_role.post_category, ((COALESCE(title_hid.salutation_title, person.first_names)::text || ' '::text) || person.surname::text), ((COALESCE(pi_title.salutation_title, supervisor.first_names)::text || ' '::text) || supervisor.surname::text), ((((COALESCE(title_hid.long_title, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text), (
        CASE
            WHEN firstaider.id IS NOT NULL THEN 't'::text
            ELSE 'f'::text
        END)
  ORDER BY person.surname, person.first_names;
