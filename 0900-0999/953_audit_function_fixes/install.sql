SET ROLE dev;

CREATE FUNCTION registration.audit_form()
    RETURNS trigger
    LANGUAGE 'plperl'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

    my $rv;
    my @olddata;
    my @newdata;
    #my %data_types;
    my $old;
    my $new;
    my $sql = <<EOSQL;

      insert into _audit ( operation, stamp, username, schemaname, tablename, oldcols, newcols, id ) values ( 
        '$_TD->{event}',
                now(),
        current_user,
        '$_TD->{table_schema}',
        '$_TD->{table_name}',

EOSQL

    # find out who we are running as
    my $whoami = 'select current_user';
    $rv = spi_exec_query($whoami);
    my $username = $rv->{rows}[0]->{current_user};

    ## get column data types
    # This approach does not work for detecting columns needing special treatment as the user may not have enough rights to see the information schema
    #$rv = spi_exec_query('select data_type, column_name
    #                             from information_schema.columns 
    #                             where table_name = '. quote_literal($_TD->{table_name}) . 'and table_schema = '.quote_literal($_TD->{table_schema}));
    #foreach (@{$rv->{rows}}) {
    #    $data_types{$_->{column_name}} = $_->{data_type};
    #}

    # fill in oldcols
    if ($_TD->{event} eq 'INSERT')  {
          $sql .= 'NULL, ';
    } elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $old = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{old}})) {
            # safety_form_pdf is a bytea column so don't log contents
            if ($col eq 'safety_form_pdf') {
                if ($val ne $_TD->{new}{$col}) {
                    push @olddata, $col . ': binary data changed' ;
                }
            # this is an enormously long html document
            } elsif ($col eq 'safety_form_html') {
                if ($val ne $_TD->{new}{$col}) {
                    push @olddata, $col . ': data changed' ;
                }
            } else {
            # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{new}{$col}) {
                        push @olddata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @olddata, $col . ':' . $val ;
                }
            }
        }
	unless (!@olddata) {
	    $old = join("\n",sort @olddata);
	}
        $sql .= quote_nullable($old) . ', ';
    }

    # fill in newcols
    if ($_TD->{event} eq 'DELETE')  {
        $sql .= 'NULL, ';
    } elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $new = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{new}})) {
            # safety_form_pdf is a bytea column so don't log contents
            if ($col eq 'safety_form_pdf') {
                if ($val ne $_TD->{old}{$col}) {
                    push @newdata, $col . ': binary data changed' ;
                }
            # this is an enormously long html document
            } elsif ($col eq 'safety_form_html') {
                if ($val ne $_TD->{old}{$col}) {
                    push @newdata, $col . ': data changed' ;
                }
           } else {
                # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{old}{$col}) {
                        push @newdata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @newdata, $col . ':' . $val;
                }
            }
        }
	unless (!@newdata) {
		$new = join("\n",sort @newdata);
	}
        $sql .= quote_nullable($new) . ', ';
    }


    # fill in primary key
    my $long_pk = $_TD->{table_name} . '_id';
    my $pk_col;
    # look for a primary key field in either of our standard forms
    # Have to do it like this because when we are updating views the user may not have
    # sufficient rights to look at the information schema on the underlying table. 
    foreach (keys(%{$_TD->{new}}), keys(%{$_TD->{old}})) {
        if ($_ eq $long_pk ) {
            $pk_col = $long_pk;
	} elsif ($_ eq 'id')  {
		$pk_col='id';
	}

    }    
    if (defined $pk_col )  {
        if ($_TD->{event} eq 'DELETE') {
            $sql .= quote_literal($_TD->{old}{$pk_col}) . ' )';
        } else {
            $sql .= quote_literal($_TD->{new}{$pk_col}) . ' )';
        }
    } else {
        $sql .= '0' . ')';
    }

    # for debugging
    #elog(NOTICE,$sql);

    # if anything changed, write to the audit log
    if (defined $old or defined $new) {
        $rv = spi_exec_query($sql);
        unless ($rv->{status} eq 'SPI_OK_INSERT') {
            elog(NOTICE,"trigger status: $rv->{status}");
            return 'SKIP'
        }
    }
    return undef; 
    
$BODY$;

ALTER FUNCTION registration.audit_form()
    OWNER TO dev;

CREATE TRIGGER audit AFTER INSERT OR UPDATE OR DELETE ON registration.form FOR EACH ROW EXECUTE PROCEDURE registration.audit_form();
