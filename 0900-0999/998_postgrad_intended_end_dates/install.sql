CREATE FUNCTION public.postgrad_intended_end_dates_trig() RETURNS TRIGGER AS
$b$
DECLARE
    my_intended_end_date DATE := NULL;
    my_type TEXT;
    my_start_date DATE;
BEGIN
    IF TG_OP = 'INSERT' THEN
      my_start_date = COALESCE(NEW.start_date,current_date);
    ELSE
      my_start_date = NEW.start_date;
    END IF;
    IF NEW.intended_end_date IS NULL THEN
        SELECT t.name INTO my_type FROM public.postgraduate_studentship_type t WHERE NEW.postgraduate_studentship_type_id = t.id;
        CASE my_type
            -- Full time PhDs do min of 9 terms, max of 12, so 4 years is about correct
	    WHEN 'PhD' THEN my_intended_end_date := my_start_date + interval '4 years';
            -- MPhil does 3 terms but there's no maximum. The ones starting October 1st are expected to finish by 31st August
            -- so 11 months seems about right; to make it come out at 31/8 for the 1/10 starters we take off one day from that
            WHEN 'MPhil' THEN my_intended_end_date := my_start_date + interval '11 months' - interval '1 day';
            -- we no longer admit people to do CPGS or MSc so no clauses for those
        END CASE;
        NEW.intended_end_date := my_intended_end_date;
    END IF;
    RETURN NEW;
END;
$b$ LANGUAGE plpgsql;

ALTER FUNCTION public.postgrad_intended_end_dates_trig() OWNER TO dev;

CREATE TRIGGER pg_intended_end_date BEFORE INSERT OR UPDATE ON public.postgraduate_studentship FOR EACH ROW EXECUTE FUNCTION public.postgrad_intended_end_dates_trig();

