DROP VIEW apps.swipe_card_expiry;

CREATE OR REPLACE VIEW apps.swipe_card_expiry
 AS
 SELECT COALESCE(university_card.surname, person.surname::text)::character varying AS surname,
    COALESCE(university_card.forename, person.first_names::text)::character varying AS forenames,
    university_card.full_name::character varying AS full_name,
    university_card.expires_at::date AS expires,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date
   FROM university_card
     LEFT JOIN person ON lower(university_card.crsid) = person.crsid::text
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
  WHERE university_card.from_chemistry_card_feed = true;

ALTER TABLE apps.swipe_card_expiry
    OWNER TO dev;

GRANT SELECT ON TABLE apps.swipe_card_expiry TO cos;
GRANT ALL ON TABLE apps.swipe_card_expiry TO dev;
GRANT SELECT ON TABLE apps.swipe_card_expiry TO leavers_management;

