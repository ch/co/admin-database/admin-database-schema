CREATE OR REPLACE VIEW hotwire3."10_View/People/Swipe_Cards"
 AS
WITH combined_cards_and_people AS ( SELECT university_card.id,
    COALESCE(person.surname,university_card.surname)::text AS surname,
    COALESCE(person.first_names,university_card.forename)::text AS forename,
    COALESCE((COALESCE(person.known_as,person.first_names) || ' ' || person.surname), university_card.full_name) AS full_name,
    university_card.crsid,
    university_card.issued_at,
    university_card.expires_at,
    university_card.issue_number,
    university_card.legacy_card_holder_id AS card_identifier,
    university_card.mifare_id AS mifare_identifier,
    university_card.mifare_number,
    university_card.barcode
   FROM university_card
   LEFT JOIN person ON university_card.crsid = person.crsid )
SELECT
  id,
  surname,
  forename,
  full_name,
  crsid,
  issued_at,
  expires_at,
  issue_number,
  card_identifier,
  mifare_identifier,
  mifare_number,
  barcode
FROM combined_cards_and_people
  ORDER BY surname, forename;
