CREATE OR REPLACE VIEW hotwire3."10_View/People/Swipe_Cards"
 AS
 SELECT university_card.id,
    university_card.surname,
    university_card.forename,
    university_card.full_name,
    university_card.crsid,
    university_card.issued_at,
    university_card.expires_at,
    university_card.issue_number,
    university_card.legacy_card_holder_id AS card_identifier,
    university_card.mifare_id AS mifare_identifier,
    university_card.mifare_number,
    university_card.barcode
   FROM university_card
  ORDER BY university_card.surname, university_card.forename;
