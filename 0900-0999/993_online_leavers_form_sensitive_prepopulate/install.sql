CREATE OR REPLACE VIEW leavers_form.person_sensitive AS
 SELECT
    leavers_form.person_basic.*,
    COALESCE(public.person.forwarding_address, public.person.cambridge_address) AS forwarding_address
  FROM
    leavers_form.person_basic
  JOIN public.person ON leavers_form.person_basic.person_id = public.person.id
  ;

ALTER TABLE leavers_form.person_sensitive
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.person_sensitive TO dev;
GRANT SELECT ON TABLE leavers_form.person_sensitive TO onlineleaversform;
