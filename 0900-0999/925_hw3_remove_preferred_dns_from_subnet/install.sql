-- View: hotwire3."10_View/Network/Subnets"

DROP Rule hotwire3_view_subnet_ins ON hotwire3."10_View/Network/Subnets";

DROP Rule hotwire3_view_subnet_upd ON hotwire3."10_View/Network/Subnets";

DROP VIEW hotwire3."10_View/Network/Subnets";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Subnets"
 AS
 SELECT subnet.id,
    subnet.network_address,
    subnet.router,
    subnet.domain_name,
    subnet.dns_domain_id AS domain_for_dns_server_id,
    subnet.dns1,
    subnet.dns2,
    subnet.dns3,
    subnet.provide_dhcp_service,
    subnet.vlan_id,
    subnet.upload_records_to_central_dns,
    subnet.use_local_records_for_dns,
    subnet.notes
   FROM subnet;

ALTER TABLE hotwire3."10_View/Network/Subnets"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/Network/Subnets" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Subnets" TO dev;

-- Rule: hotwire3_view_subnet_ins ON hotwire3."10_View/Network/Subnets"


CREATE OR REPLACE RULE hotwire3_view_subnet_ins AS
    ON INSERT TO hotwire3."10_View/Network/Subnets"
    DO INSTEAD
(INSERT INTO subnet (network_address, router, domain_name, dns_domain_id, dns1, dns2, dns3, provide_dhcp_service, vlan_id, upload_records_to_central_dns, use_local_records_for_dns, notes)
  VALUES (new.network_address, new.router, new.domain_name, new.domain_for_dns_server_id, new.dns1, new.dns2, new.dns3, new.provide_dhcp_service, new.vlan_id, new.upload_records_to_central_dns, new.use_local_records_for_dns, new.notes)
  RETURNING subnet.id,
    subnet.network_address,
    subnet.router,
    subnet.domain_name,
    subnet.dns_domain_id,
    subnet.dns1,
    subnet.dns2,
    subnet.dns3,
    subnet.provide_dhcp_service,
    subnet.vlan_id,
    subnet.upload_records_to_central_dns,
    subnet.use_local_records_for_dns,
    subnet.notes);

-- Rule: hotwire3_view_subnet_upd ON hotwire3."10_View/Network/Subnets"


CREATE OR REPLACE RULE hotwire3_view_subnet_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Subnets"
    DO INSTEAD
(UPDATE subnet SET network_address = new.network_address, router = new.router, domain_name = new.domain_name, dns_domain_id = new.domain_for_dns_server_id, dns1 = new.dns1, dns2 = new.dns2, dns3 = new.dns3, provide_dhcp_service = new.provide_dhcp_service, vlan_id = new.vlan_id, upload_records_to_central_dns = new.upload_records_to_central_dns, use_local_records_for_dns = new.use_local_records_for_dns, notes = new.notes
  WHERE (subnet.id = old.id)
  RETURNING subnet.id,
    subnet.network_address,
    subnet.router,
    subnet.domain_name,
    subnet.dns_domain_id,
    subnet.dns1,
    subnet.dns2,
    subnet.dns3,
    subnet.provide_dhcp_service,
    subnet.vlan_id,
    subnet.upload_records_to_central_dns,
    subnet.use_local_records_for_dns,
    subnet.notes);

