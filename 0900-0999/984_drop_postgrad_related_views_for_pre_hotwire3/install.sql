DROP VIEW public.cpgs_submission_view;
DROP VIEW public.current_postgrad_summary_report;
DROP VIEW public.phd_submission_report;
DROP VIEW public.submission_time_taken_report;
DROP VIEW public.postgrad_training_view;
DROP VIEW public.postgrad_personnel_report;
DROP VIEW public.postgrad_mentor_meetings_view;
DROP VIEW public.postgrad_student_mentor_meetings_subview;
