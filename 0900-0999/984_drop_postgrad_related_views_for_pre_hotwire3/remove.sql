-- View: public.postgrad_student_mentor_meetings_subview

-- DROP VIEW public.postgrad_student_mentor_meetings_subview;

CREATE OR REPLACE VIEW public.postgrad_student_mentor_meetings_subview
 AS
 SELECT mentor_meeting.id,
    mentor_meeting.postgraduate_studentship_id,
    mentor_meeting.date_of_meeting,
    mentor_meeting.mentor_id,
    'postgrad_mentor_meetings_view'::text AS _targetview
   FROM mentor_meeting
  WHERE mentor_meeting.postgraduate_studentship_id IS NOT NULL;

ALTER TABLE public.postgrad_student_mentor_meetings_subview
    OWNER TO cen1001;

GRANT ALL ON TABLE public.postgrad_student_mentor_meetings_subview TO cen1001;
GRANT SELECT ON TABLE public.postgrad_student_mentor_meetings_subview TO dev;
GRANT SELECT ON TABLE public.postgrad_student_mentor_meetings_subview TO student_management;


-- View: public.postgrad_mentor_meetings_view

-- DROP VIEW public.postgrad_mentor_meetings_view;

CREATE OR REPLACE VIEW public.postgrad_mentor_meetings_view
 AS
 SELECT mentor_meeting.id,
    postgraduate_studentship.id AS student_name_id,
    postgraduate_studentship.first_mentor_id AS usual_mentor_id,
    mentor_meeting.date_of_meeting,
    mentor_meeting.mentor_id,
    postgraduate_studentship.next_mentor_meeting_due,
    mentor_meeting.notes
   FROM mentor_meeting
     JOIN postgraduate_studentship ON postgraduate_studentship.id = mentor_meeting.postgraduate_studentship_id
  WHERE mentor_meeting.postgraduate_studentship_id IS NOT NULL;

ALTER TABLE public.postgrad_mentor_meetings_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.postgrad_mentor_meetings_view TO cen1001;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.postgrad_mentor_meetings_view TO dev;
GRANT SELECT ON TABLE public.postgrad_mentor_meetings_view TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.postgrad_mentor_meetings_view TO student_management;


-- Rule: postgrad_mentor_meeting_upd ON public.postgrad_mentor_meetings_view

-- DROP Rule postgrad_mentor_meeting_upd ON public.postgrad_mentor_meetings_view;

CREATE OR REPLACE RULE postgrad_mentor_meeting_upd AS
    ON UPDATE TO public.postgrad_mentor_meetings_view
    DO INSTEAD
( UPDATE mentor_meeting SET postgraduate_studentship_id = new.student_name_id, date_of_meeting = new.date_of_meeting, mentor_id = COALESCE(new.mentor_id, new.usual_mentor_id), notes = new.notes
  WHERE (mentor_meeting.id = old.id);
 UPDATE postgraduate_studentship SET next_mentor_meeting_due = new.next_mentor_meeting_due, first_mentor_id = new.usual_mentor_id
  WHERE (postgraduate_studentship.id = old.student_name_id);
);

-- Rule: postgrad_mentor_meetings_del ON public.postgrad_mentor_meetings_view

-- DROP Rule postgrad_mentor_meetings_del ON public.postgrad_mentor_meetings_view;

CREATE OR REPLACE RULE postgrad_mentor_meetings_del AS
    ON DELETE TO public.postgrad_mentor_meetings_view
    DO INSTEAD
(DELETE FROM mentor_meeting
  WHERE (mentor_meeting.id = old.id));

-- Rule: postgrad_mentor_meetings_ins ON public.postgrad_mentor_meetings_view

-- DROP Rule postgrad_mentor_meetings_ins ON public.postgrad_mentor_meetings_view;

CREATE OR REPLACE RULE postgrad_mentor_meetings_ins AS
    ON INSERT TO public.postgrad_mentor_meetings_view
    DO INSTEAD
( UPDATE postgraduate_studentship SET next_mentor_meeting_due = new.next_mentor_meeting_due
  WHERE (postgraduate_studentship.id = new.student_name_id);
 INSERT INTO mentor_meeting (postgraduate_studentship_id, date_of_meeting, mentor_id, notes)
  VALUES (new.student_name_id, new.date_of_meeting, COALESCE(new.mentor_id, ( SELECT postgraduate_studentship.first_mentor_id
           FROM postgraduate_studentship
          WHERE (postgraduate_studentship.id = new.student_name_id))), new.notes)
  RETURNING mentor_meeting.id,
    NULL::bigint AS int8,
    NULL::bigint AS int8,
    mentor_meeting.date_of_meeting,
    mentor_meeting.mentor_id,
    NULL::date AS date,
    mentor_meeting.notes;
);


-- View: public.postgrad_personnel_report

-- DROP VIEW public.postgrad_personnel_report;

CREATE OR REPLACE VIEW public.postgrad_personnel_report
 AS
 SELECT postgraduate_studentship.id,
    supervisor_hid.supervisor_hid AS supervisor,
    person_hid.person_hid AS person,
    postgraduate_studentship_type.name AS studentship_type,
    second_supervisor_hid.supervisor_hid AS second_supervisor,
    postgraduate_studentship.start_date,
    postgraduate_studentship.end_of_registration_date,
    postgraduate_studentship.filemaker_funding AS source_of_funds,
    first_mentor_hid.mentor_hid AS first_mentor,
    second_mentor_hid.mentor_hid AS second_mentor
   FROM postgraduate_studentship
     JOIN supervisor_hid ON postgraduate_studentship.first_supervisor_id = supervisor_hid.supervisor_id
     LEFT JOIN supervisor_hid second_supervisor_hid ON postgraduate_studentship.second_supervisor_id = second_supervisor_hid.supervisor_id
     LEFT JOIN mentor_hid first_mentor_hid ON postgraduate_studentship.first_mentor_id = first_mentor_hid.mentor_id
     LEFT JOIN mentor_hid second_mentor_hid ON postgraduate_studentship.second_mentor_id = second_mentor_hid.mentor_id
     JOIN person_hid USING (person_id)
     JOIN _postgrad_end_dates_v5 USING (id)
     JOIN postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
  WHERE (postgraduate_studentship_type.name::text = 'PhD'::text OR postgraduate_studentship_type.name::text = 'MPhil'::text) AND _postgrad_end_dates_v5.status_id::text = 'Current'::text
  ORDER BY supervisor_hid.supervisor_hid, person_hid.person_hid;

ALTER TABLE public.postgrad_personnel_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.postgrad_personnel_report TO cen1001;
GRANT SELECT ON TABLE public.postgrad_personnel_report TO dev;
GRANT SELECT ON TABLE public.postgrad_personnel_report TO student_management;

-- View: public.postgrad_training_view

-- DROP VIEW public.postgrad_training_view;

CREATE OR REPLACE VIEW public.postgrad_training_view
 AS
 SELECT postgraduate_studentship.id,
    postgraduate_studentship.person_id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    postgraduate_studentship.postgraduate_studentship_type_id,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    postgraduate_studentship.start_date,
    _postgrad_end_dates_v5.status_id AS ro_status_id,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.filemaker_fees_funding AS fees_funding,
    postgraduate_studentship.transferable_skills_days_1st_year,
    postgraduate_studentship.transferable_skills_days_2nd_year,
    postgraduate_studentship.transferable_skills_days_3rd_year,
    postgraduate_studentship.transferable_skills_days_4th_year,
    COALESCE(postgraduate_studentship.transferable_skills_days_1st_year, 0::real) + COALESCE(postgraduate_studentship.transferable_skills_days_2nd_year, 0::real) + COALESCE(postgraduate_studentship.transferable_skills_days_3rd_year, 0::real) + COALESCE(postgraduate_studentship.transferable_skills_days_4th_year, 0::real) AS total_days_ro
   FROM postgraduate_studentship
     LEFT JOIN _postgrad_end_dates_v5 USING (id)
     LEFT JOIN person ON person.id = postgraduate_studentship.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.postgrad_training_view
    OWNER TO cen1001;

GRANT ALL ON TABLE public.postgrad_training_view TO cen1001;
GRANT ALL ON TABLE public.postgrad_training_view TO dev;
GRANT SELECT ON TABLE public.postgrad_training_view TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.postgrad_training_view TO student_management;
GRANT SELECT ON TABLE public.postgrad_training_view TO student_management_ro;


-- Rule: postgrad_training_del ON public.postgrad_training_view

-- DROP Rule postgrad_training_del ON public.postgrad_training_view;

CREATE OR REPLACE RULE postgrad_training_del AS
    ON DELETE TO public.postgrad_training_view
    DO INSTEAD
(DELETE FROM postgraduate_studentship
  WHERE (postgraduate_studentship.id = old.id));

-- Rule: postgrad_training_ins ON public.postgrad_training_view

-- DROP Rule postgrad_training_ins ON public.postgrad_training_view;

CREATE OR REPLACE RULE postgrad_training_ins AS
    ON INSERT TO public.postgrad_training_view
    DO INSTEAD
(INSERT INTO postgraduate_studentship (person_id, postgraduate_studentship_type_id, first_supervisor_id, start_date, filemaker_funding, filemaker_fees_funding, transferable_skills_days_1st_year, transferable_skills_days_2nd_year, transferable_skills_days_3rd_year, transferable_skills_days_4th_year)
  VALUES (new.person_id, new.postgraduate_studentship_type_id, new.supervisor_id, new.start_date, new.funding, new.fees_funding, new.transferable_skills_days_1st_year, new.transferable_skills_days_2nd_year, new.transferable_skills_days_3rd_year, new.transferable_skills_days_4th_year)
  RETURNING postgraduate_studentship.id,
    postgraduate_studentship.person_id,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar",
    postgraduate_studentship.postgraduate_studentship_type_id,
    postgraduate_studentship.first_supervisor_id,
    postgraduate_studentship.start_date,
    NULL::character varying(20) AS "varchar",
    postgraduate_studentship.filemaker_funding,
    postgraduate_studentship.filemaker_fees_funding,
    postgraduate_studentship.transferable_skills_days_1st_year,
    postgraduate_studentship.transferable_skills_days_2nd_year,
    postgraduate_studentship.transferable_skills_days_3rd_year,
    postgraduate_studentship.transferable_skills_days_4th_year,
    NULL::real AS float4);

-- Rule: postgrad_training_upd ON public.postgrad_training_view

-- DROP Rule postgrad_training_upd ON public.postgrad_training_view;

CREATE OR REPLACE RULE postgrad_training_upd AS
    ON UPDATE TO public.postgrad_training_view
    DO INSTEAD
(UPDATE postgraduate_studentship SET person_id = new.person_id, postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, first_supervisor_id = new.supervisor_id, start_date = new.start_date, filemaker_funding = new.funding, filemaker_fees_funding = new.fees_funding, transferable_skills_days_1st_year = new.transferable_skills_days_1st_year, transferable_skills_days_2nd_year = new.transferable_skills_days_2nd_year, transferable_skills_days_3rd_year = new.transferable_skills_days_3rd_year, transferable_skills_days_4th_year = new.transferable_skills_days_4th_year
  WHERE (postgraduate_studentship.id = old.id));

-- View: public.submission_time_taken_report

-- DROP VIEW public.submission_time_taken_report;

CREATE OR REPLACE VIEW public.submission_time_taken_report
 AS
 SELECT postgraduate_studentship.id,
    postgraduate_studentship.person_id,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.date_registered_for_phd,
    postgraduate_studentship.phd_date_submitted,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    _postgrad_end_dates_v5.phd_duration,
    _postgrad_end_dates_v5.phd_duration_days,
    _postgrad_end_dates_v5.status_id,
    _postgrad_end_dates_v5.end_date,
    person.surname AS _surname,
    person.first_names AS _first_names
   FROM postgraduate_studentship
     JOIN _postgrad_end_dates_v5 USING (id)
     LEFT JOIN person ON person.id = postgraduate_studentship.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.submission_time_taken_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.submission_time_taken_report TO cen1001;
GRANT ALL ON TABLE public.submission_time_taken_report TO dev;
GRANT SELECT ON TABLE public.submission_time_taken_report TO mgmt_ro;
GRANT SELECT ON TABLE public.submission_time_taken_report TO student_management;

-- View: public.phd_submission_report

-- DROP VIEW public.phd_submission_report;

CREATE OR REPLACE VIEW public.phd_submission_report
 AS
 SELECT postgraduate_studentship.id,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    postgraduate_studentship.person_id,
    _postgrad_end_dates_v5.phd_duration AS ro_duration,
    postgraduate_studentship.date_registered_for_phd,
    postgraduate_studentship.date_phd_submission_due,
    postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
    postgraduate_studentship.phd_date_submitted,
    postgraduate_studentship.cpgs_or_mphil_date_submitted,
    postgraduate_studentship.start_date,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.postgraduate_studentship_type_id,
    _postgrad_end_dates_v5.phd_duration_days AS ro_duration_days,
    _postgrad_end_dates_v5.status_id AS ro_status_id,
    person.surname AS _surname,
    person.first_names AS _first_names
   FROM postgraduate_studentship
     LEFT JOIN _postgrad_end_dates_v5 USING (id)
     LEFT JOIN person ON person.id = postgraduate_studentship.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.phd_submission_report
    OWNER TO cen1001;

GRANT SELECT ON TABLE public.phd_submission_report TO accounts;
GRANT ALL ON TABLE public.phd_submission_report TO cen1001;
GRANT ALL ON TABLE public.phd_submission_report TO dev;
GRANT SELECT ON TABLE public.phd_submission_report TO mgmt_ro;
GRANT SELECT ON TABLE public.phd_submission_report TO student_management;

-- View: public.current_postgrad_summary_report

-- DROP VIEW public.current_postgrad_summary_report;

CREATE OR REPLACE VIEW public.current_postgrad_summary_report
 AS
 SELECT postgraduate_studentship.id,
    postgraduate_studentship.person_id,
    postgraduate_studentship.phd_thesis_title AS phd_thesis_title_html,
    postgraduate_studentship.filemaker_funding,
    postgraduate_studentship.filemaker_fees_funding,
    postgraduate_studentship.first_supervisor_id,
    postgraduate_studentship.postgraduate_studentship_type_id,
    _postgrad_end_dates_v5.phd_duration,
    _postgrad_end_dates_v5.phd_duration_days,
    person.surname AS _surname,
    person.first_names AS _first_names
   FROM postgraduate_studentship
     LEFT JOIN _postgrad_end_dates_v5 USING (id)
     LEFT JOIN person ON postgraduate_studentship.person_id = person.id
  WHERE _postgrad_end_dates_v5.status_id::text = 'Current'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.current_postgrad_summary_report
    OWNER TO cen1001;

GRANT ALL ON TABLE public.current_postgrad_summary_report TO cen1001;
GRANT SELECT ON TABLE public.current_postgrad_summary_report TO dev;
GRANT SELECT ON TABLE public.current_postgrad_summary_report TO mgmt_ro;
GRANT SELECT ON TABLE public.current_postgrad_summary_report TO student_management;

-- View: public.cpgs_submission_view

-- DROP VIEW public.cpgs_submission_view;

CREATE OR REPLACE VIEW public.cpgs_submission_view
 AS
 SELECT postgraduate_studentship.id,
    postgraduate_studentship.person_id,
    person.surname AS _surname,
    person.first_names AS _first_names,
    postgraduate_studentship.postgraduate_studentship_type_id,
    postgraduate_studentship.cpgs_or_mphil_date_submission_due,
    postgraduate_studentship.cpgs_or_mphil_date_submitted,
    postgraduate_studentship.cpgs_or_mphil_date_awarded,
    postgraduate_studentship.mphil_date_submission_due,
    postgraduate_studentship.mphil_date_submitted,
    postgraduate_studentship.mphil_date_awarded,
    postgraduate_studentship.first_supervisor_id AS supervisor_id
   FROM postgraduate_studentship
     LEFT JOIN _postgrad_end_dates_v5 USING (id)
     LEFT JOIN person ON person.id = postgraduate_studentship.person_id
  WHERE _postgrad_end_dates_v5.status_id::text = 'Current'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE public.cpgs_submission_view
    OWNER TO cen1001;

GRANT SELECT ON TABLE public.cpgs_submission_view TO accounts;
GRANT ALL ON TABLE public.cpgs_submission_view TO cen1001;
GRANT ALL ON TABLE public.cpgs_submission_view TO dev;
GRANT SELECT ON TABLE public.cpgs_submission_view TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE public.cpgs_submission_view TO student_management;


-- Rule: cpgs_submission_upd ON public.cpgs_submission_view

-- DROP Rule cpgs_submission_upd ON public.cpgs_submission_view;

CREATE OR REPLACE RULE cpgs_submission_upd AS
    ON UPDATE TO public.cpgs_submission_view
    DO INSTEAD
(UPDATE postgraduate_studentship SET person_id = new.person_id, postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_submitted = new.cpgs_or_mphil_date_submitted, cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded, mphil_date_submission_due = new.mphil_date_submission_due, mphil_date_submitted = new.mphil_date_submitted, mphil_date_awarded = new.mphil_date_awarded, first_supervisor_id = new.supervisor_id
  WHERE (postgraduate_studentship.id = old.id));

