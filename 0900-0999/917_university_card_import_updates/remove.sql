REVOKE SELECT,UPDATE ON university_card FROM _misd_import;
REVOKE SELECT (id,crsid) ON person FROM _misd_import;
REVOKE SELECT ON _physical_status_v3 FROM _misd_import;

DROP INDEX idx_card_uuid;
DROP INDEX idx_card_mifare_id;
