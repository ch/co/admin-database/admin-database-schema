-- so it can copy the table structure and update records
GRANT SELECT,UPDATE ON university_card TO _misd_import;
GRANT SELECT (id,crsid) ON person TO _misd_import;
GRANT SELECT ON _physical_status_v3 TO _misd_import;

CREATE UNIQUE INDEX idx_card_uuid ON university_card(id);

-- stash the original table
CREATE TEMPORARY TABLE duplicated_university_card ( LIKE university_card );
INSERT INTO duplicated_university_card SELECT * FROM university_card;
-- remove data from uni card
TRUNCATE university_card;
-- add index
CREATE UNIQUE INDEX idx_card_mifare_id ON university_card(mifare_id);
-- copy back data, skipping duplicated rows
INSERT INTO university_card SELECT * FROM duplicated_university_card ON CONFLICT DO NOTHING;
