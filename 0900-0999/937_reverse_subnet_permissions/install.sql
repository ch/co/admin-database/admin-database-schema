GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO jackdaw;
GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO dhcp;
GRANT SELECT,UPDATE ON TABLE reverse_subnet.reverse_subnet TO ipreg;
GRANT ALL ON TABLE reverse_subnet.reverse_subnet TO cos;
GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO rancid;
GRANT ALL ON TABLE reverse_subnet.reverse_subnet TO hobbit;
GRANT SELECT,UPDATE ON TABLE reverse_subnet.reverse_subnet TO ipadjust;
GRANT SELECT ON TABLE reverse_subnet.reverse_subnet TO shadow_mac_to_vlan_maintenance;
