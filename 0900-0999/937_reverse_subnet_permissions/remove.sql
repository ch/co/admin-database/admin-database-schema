REVOKE SELECT ON TABLE reverse_subnet.reverse_subnet FROM jackdaw;
REVOKE SELECT ON TABLE reverse_subnet.reverse_subnet FROM dhcp;
REVOKE SELECT,UPDATE ON TABLE reverse_subnet.reverse_subnet FROM ipreg;
REVOKE ALL ON TABLE reverse_subnet.reverse_subnet FROM cos;
REVOKE SELECT ON TABLE reverse_subnet.reverse_subnet FROM rancid;
REVOKE ALL ON TABLE reverse_subnet.reverse_subnet FROM hobbit;
REVOKE SELECT,UPDATE ON TABLE reverse_subnet.reverse_subnet FROM ipadjust;
REVOKE SELECT ON TABLE reverse_subnet.reverse_subnet FROM shadow_mac_to_vlan_maintenance;
