CREATE VIEW leavers_form.forwarding_address AS
  SELECT
    id AS person_id,
    forwarding_address
  FROM
    public.person
;

ALTER TABLE leavers_form.forwarding_address
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.forwarding_address TO dev;
GRANT SELECT, UPDATE ON TABLE leavers_form.forwarding_address TO onlineleaversform;


CREATE VIEW leavers_form.email_address AS
  SELECT
    id AS person_id,
    email_address AS email
  FROM
    public.person
;

ALTER TABLE leavers_form.email_address
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.email_address TO dev;
GRANT SELECT, UPDATE ON TABLE leavers_form.email_address TO onlineleaversform;
