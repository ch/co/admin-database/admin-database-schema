CREATE VIEW registration.incomplete_form_reminders AS
SELECT
    form.email AS starter_email,
    notify_person.email_address AS admin_email,
    uuid,
    form_role,
    _form_started,
    registration.state_of_form(uuid) AS reason
FROM
    registration.form
    LEFT JOIN person AS notify_person ON form.notify_person_id = notify_person.id
WHERE
    registration.state_of_form( uuid ) <> 'Complete' AND
    form.email IS NOT NULL AND
    form.email <> ''
    ;

ALTER VIEW registration.incomplete_form_reminders OWNER TO dev;
GRANT SELECT ON registration.incomplete_form_reminders TO starters_registration;
