-- FUNCTION: public.correct_hostname()

-- DROP FUNCTION public.correct_hostname();

CREATE OR REPLACE FUNCTION public.correct_hostname()
    RETURNS trigger
    LANGUAGE 'plperl'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

    # check the hostname field for sanity. Correct some obvious mistakes. Update short_hostname.
    my $hostname = $_TD->{new}->{hostname};
    my $subnet_id = $_TD->{old}->{subnet_id};
    my $allow_free_choice = $_TD->{new}->{allow_free_choice_of_hostname};

    # We use this boolean as a proxy indication of whether or not we should clear allow_free_choice_of_hostname.
    # If this causes a problem we should instead add an extra boolean to subnet, probably.
    my $sync_to_jackdaw = spi_exec_query('SELECT upload_records_to_central_dns FROM subnet where id=' . $subnet_id);

    if ((! defined $hostname) or ($hostname eq '')){
        $_TD->{new}->{short_hostname} = undef;
        $_TD->{new}->{hostname} = undef;
        if($sync_to_jackdaw->{rows}[0]->{'upload_records_to_central_dns'} eq 't') {
          $_TD->{new}->{allow_free_choice_of_hostname} = 'f';
        }
        return 'MODIFY';
        
    }

    # Let's be kind to the users and lowercase their hostnames automagically
    $hostname=lc($hostname);

    @components = split(/\./,$hostname);
    foreach $label (@components) {
        if ($label !~ /^[a-z0-9][-a-z0-9]*$/) 
        {
            elog(ERROR, "\"$label\" is not permitted as a DNS component");
        }
    }

    # we must update the short hostname
    $short_hostname=$components[0];
    $_TD->{new}->{short_hostname}=$short_hostname;
    
    # need to find the appropriate subnet domain name here
    my $sql = 'select domain_name from subnet where id = ' .  $subnet_id ;
    $rv = spi_exec_query($sql); 
    # this will be an empty string in some cases
    my $domain_name = $rv->{rows}[0]->{domain_name};

    my $matchstring = $domain_name . '$';

    if ($hostname !~ /$matchstring/) 
    {
        # something not right here - now do we just splat the domain name
        # on the end or what? If there are no dots in it, splat the
        # domain name. 
        # Else raise an error
        if ( $hostname =~ /\./ ) 
        {
            elog(ERROR, "bad hostname $hostname (contains '.' but does not end $domain_name)");
            return 'SKIP';
        } else {
            $hostname = $short_hostname . '.' . $domain_name ;
        }
    } 
    $_TD->{new}->{hostname}=$hostname;
    return 'MODIFY';
    
# vim:syntax=perl:sw=4
$BODY$;

ALTER FUNCTION public.correct_hostname()
    OWNER TO dev;
