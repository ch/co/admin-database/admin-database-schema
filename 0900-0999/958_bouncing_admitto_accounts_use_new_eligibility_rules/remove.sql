DROP VIEW apps.bouncing_admitto_accounts;

CREATE VIEW apps.bouncing_admitto_accounts
 AS
 SELECT pt.long_title AS ptitle,
    p.surname AS psurname,
    COALESCE(p.known_as, p.first_names) AS pfirst,
    p.email_address AS pemail,
    p.crsid,
    ps.status_id AS pstatus,
    ts.salutation_title AS stitle,
    s.email_address AS semail,
    s.surname AS ssurname
   FROM person p
     LEFT JOIN title_hid pt ON p.title_id = pt.title_id
     JOIN _latest_role_v12 r ON r.person_id = p.id
     JOIN _physical_status_v3 ps ON p.id = ps.person_id
     JOIN person s ON r.supervisor_id = s.id
     LEFT JOIN title_hid ts ON ts.title_id = s.title_id;

ALTER TABLE apps.bouncing_admitto_accounts
    OWNER TO dev;

GRANT SELECT ON TABLE apps.bouncing_admitto_accounts TO cos;
GRANT ALL ON TABLE apps.bouncing_admitto_accounts TO dev;
