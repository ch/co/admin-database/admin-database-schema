CREATE OR REPLACE VIEW hotwire3.ip_address_unsorted_hid
 AS
 SELECT ip_address.id AS ip_address_id,
    COALESCE(((ip_address.hostname::text || ' ('::text) || ip_address.ip) || ')'::text, ip_address.ip::text) AS ip_address_hid
   FROM ip_address;

ALTER TABLE hotwire3.ip_address_unsorted_hid
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3.ip_address_unsorted_hid TO dev;
GRANT SELECT ON TABLE hotwire3.ip_address_unsorted_hid TO groupitreps;
GRANT SELECT ON TABLE hotwire3.ip_address_unsorted_hid TO headsofgroup;
GRANT SELECT ON TABLE hotwire3.ip_address_unsorted_hid TO ro_hid;
