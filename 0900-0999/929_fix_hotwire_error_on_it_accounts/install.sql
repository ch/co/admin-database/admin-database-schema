DROP VIEW hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts";

CREATE VIEW hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" WITH (security_barrier)
 AS
 SELECT accounts_allowed_to_see.id,
    accounts_allowed_to_see.username,
    owner_hid.owner_hid::varchar AS owner,
    accounts_allowed_to_see.description,
    accounts_allowed_to_see.is_disabled,
    accounts_allowed_to_see.expiry_date,
    accounts_allowed_to_see.ro_owner_banned
   FROM ( SELECT user_account.id,
            user_account.username,
            user_account.person_id AS owner_id,
            user_account.description,
            user_account.is_disabled,
            user_account.expiry_date,
            user_account.owner_banned AS ro_owner_banned
           FROM user_account
             LEFT JOIN person ON user_account.person_id = person.id
          WHERE "current_user"() = person.crsid::name OR pg_has_role('cos'::name, 'MEMBER'::text)) accounts_allowed_to_see
   LEFT JOIN hotwire3.owner_hid USING (owner_id)
;

ALTER TABLE hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" TO PUBLIC;


DROP VIEW hotwire3."10_View/IT_Accounts/Admitto_Accounts";

CREATE VIEW hotwire3."10_View/IT_Accounts/Admitto_Accounts" WITH (security_barrier)
 AS
 SELECT accounts_allowed_to_see.id,
    accounts_allowed_to_see.username,
    accounts_allowed_to_see.uid,
    accounts_allowed_to_see.gid,
    accounts_allowed_to_see.gecos,
    owner_hid.owner_hid::varchar AS owner,
    accounts_allowed_to_see.description,
    accounts_allowed_to_see.is_disabled,
    accounts_allowed_to_see.expiry_date
   FROM ( SELECT admitto_account.id,
            admitto_account.username,
            admitto_account.uid,
            admitto_account.gecos,
            admitto_account.gid,
            admitto_account.person_id AS owner_id,
            admitto_account.description,
            admitto_account.is_disabled,
            admitto_account.expiry_date
           FROM admitto_account
             LEFT JOIN person ON admitto_account.person_id = person.id
          WHERE "current_user"() = person.crsid::name OR pg_has_role('cos'::name, 'MEMBER'::text)) accounts_allowed_to_see
   LEFT JOIN hotwire3.owner_hid USING (owner_id)
;

ALTER TABLE hotwire3."10_View/IT_Accounts/Admitto_Accounts"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/IT_Accounts/Admitto_Accounts" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/IT_Accounts/Admitto_Accounts" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/IT_Accounts/Admitto_Accounts" TO PUBLIC;
