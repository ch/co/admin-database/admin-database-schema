CREATE VIEW apps.hostname_and_date_purchased AS
SELECT
    hostname,
    date_purchased
FROM
    hardware
    JOIN system_image ON hardware.id = system_image.hardware_id
    JOIN mm_system_image_ip_address mm ON system_image.id = mm.system_image_id
    JOIN ip_address ON ip_address.id = mm.ip_address_id
WHERE
    hostname IS NOT NULL AND date_purchased IS NOT NULL;

ALTER TABLE apps.hostname_and_date_purchased OWNER TO dev;
GRANT SELECT ON apps.hostname_and_date_purchased TO cos,osbuilder;
