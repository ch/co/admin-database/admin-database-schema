--
-- PostgreSQL database dump
--


--
-- Name: non_chemnet_user_account; Type: TABLE; Schema: public; Owner: dev
--

CREATE TABLE public.non_chemnet_user_account (
    id integer NOT NULL,
    username character varying(32) NOT NULL,
    realm character varying(72) NOT NULL,
    token character varying(20) NOT NULL,
    email_address character varying(48) NOT NULL,
    description character varying(500),
    expiry_date date,
    is_disabled boolean DEFAULT false NOT NULL
);


ALTER TABLE public.non_chemnet_user_account OWNER TO dev;

--
-- Name: non_chemnet_user_account_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE public.non_chemnet_user_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.non_chemnet_user_account_id_seq OWNER TO dev;

--
-- Name: non_chemnet_user_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE public.non_chemnet_user_account_id_seq OWNED BY public.non_chemnet_user_account.id;


--
-- Name: non_chemnet_user_account id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY public.non_chemnet_user_account ALTER COLUMN id SET DEFAULT nextval('public.non_chemnet_user_account_id_seq'::regclass);


--
-- Name: non_chemnet_user_account non_chemnet_user_account_pkey; Type: CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY public.non_chemnet_user_account
    ADD CONSTRAINT non_chemnet_user_account_pkey PRIMARY KEY (username, realm);



