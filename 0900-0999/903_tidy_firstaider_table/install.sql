CREATE OR REPLACE VIEW public._chemfirstaid_mailinglist
 AS
 SELECT person.email_address
   FROM firstaider
     JOIN _physical_status_v3 ON _physical_status_v3.person_id = firstaider.person_id
     JOIN person ON firstaider.person_id = person.id
  WHERE _physical_status_v3.status_id::text = 'Current'::text AND firstaider.requalify_date >= current_date
UNION
 SELECT person.email_address
   FROM mm_mailinglist_include_person
     JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
  WHERE mailinglist.name::text = 'chem-firstaid'::text;

DROP VIEW apps.first_aider_status_changes;

DROP TRIGGER firstaid_status_trig ON public.firstaider;

DROP TABLE public._firstaid_status_changelog;

DROP FUNCTION public.firstaid_qualification_trig();

ALTER TABLE public.firstaider DROP COLUMN qualification_up_to_date;

