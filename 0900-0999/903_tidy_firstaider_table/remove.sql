ALTER TABLE public.firstaider
    ADD COLUMN qualification_up_to_date boolean;


CREATE TABLE IF NOT EXISTS public._firstaid_status_changelog
(
    firstaider_id bigint NOT NULL,
    old_qualification_up_to_date boolean,
    new_qualification_up_to_date boolean,
    stamp timestamp without time zone NOT NULL,
    username character varying COLLATE pg_catalog."default" NOT NULL,
    person_id bigint NOT NULL
)

TABLESPACE pg_default;

ALTER TABLE public._firstaid_status_changelog
    OWNER to cen1001;

REVOKE ALL ON TABLE public._firstaid_status_changelog FROM safety_management;

GRANT ALL ON TABLE public._firstaid_status_changelog TO cen1001;

GRANT ALL ON TABLE public._firstaid_status_changelog TO dev;

GRANT SELECT ON TABLE public._firstaid_status_changelog TO firstaider_apps;

GRANT INSERT ON TABLE public._firstaid_status_changelog TO hr;

GRANT INSERT ON TABLE public._firstaid_status_changelog TO safety_management;

CREATE OR REPLACE FUNCTION public.firstaid_qualification_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
begin
	if (TG_OP = 'UPDATE') then
		select NEW.requalify_date > now() into NEW.qualification_up_to_date;		
		if (NEW.qualification_up_to_date is distinct from OLD.qualification_up_to_date) then
			insert into _firstaid_status_changelog ( firstaider_id, person_id, old_qualification_up_to_date, new_qualification_up_to_date, stamp, username ) values (NEW.id,NEW.person_id,OLD.qualification_up_to_date, NEW.qualification_up_to_date,current_timestamp,current_user);
		end if;
		return NEW;
	elsif (TG_OP = 'INSERT') then
		select NEW.requalify_date > now() into NEW.qualification_up_to_date;	
		insert into _firstaid_status_changelog ( firstaider_id, person_id, old_qualification_up_to_date, new_qualification_up_to_date, stamp, username ) values (NEW.id,NEW.person_id,NULL, NEW.qualification_up_to_date,current_timestamp,current_user);
		return NEW;
	elsif (TG_OP = 'DELETE') then
		insert into _firstaid_status_changelog ( firstaider_id, person_id, old_qualification_up_to_date, new_qualification_up_to_date, stamp, username ) values (OLD.id,OLD.person_id,OLD.qualification_up_to_date, NULL,current_timestamp,current_user);
		return OLD;
	end if;
end;

$BODY$;

ALTER FUNCTION public.firstaid_qualification_trig()
    OWNER TO dev;

CREATE TRIGGER firstaid_status_trig
    BEFORE INSERT OR DELETE OR UPDATE 
    ON public.firstaider
    FOR EACH ROW
    EXECUTE FUNCTION public.firstaid_qualification_trig();

CREATE OR REPLACE VIEW apps.first_aider_status_changes
 AS
 SELECT person.surname,
    person.first_names,
    person.crsid,
    _firstaid_status_changelog.stamp,
    _firstaid_status_changelog.old_qualification_up_to_date,
    _firstaid_status_changelog.new_qualification_up_to_date,
    firstaider.qualification_up_to_date
   FROM _firstaid_status_changelog
     LEFT JOIN firstaider ON _firstaid_status_changelog.firstaider_id = firstaider.id
     LEFT JOIN person ON _firstaid_status_changelog.person_id = person.id;

ALTER TABLE apps.first_aider_status_changes
    OWNER TO cen1001;

GRANT ALL ON TABLE apps.first_aider_status_changes TO cen1001;
GRANT SELECT ON TABLE apps.first_aider_status_changes TO firstaider_apps;

CREATE OR REPLACE VIEW public._chemfirstaid_mailinglist
 AS
 SELECT person.email_address
   FROM firstaider
     JOIN _physical_status_v3 ON _physical_status_v3.person_id = firstaider.person_id
     JOIN person ON firstaider.person_id = person.id
  WHERE _physical_status_v3.status_id::text = 'Current'::text AND firstaider.qualification_up_to_date
UNION
 SELECT person.email_address
   FROM mm_mailinglist_include_person
     JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
  WHERE mailinglist.name::text = 'chem-firstaid'::text;
