DROP VIEW apps.leavers_crsid_email_role;

CREATE VIEW apps.leavers_crsid_email_role
 AS
 SELECT person.crsid,
    person.image_lo::bigint AS image_oid,
    _latest_role.post_category,
    (COALESCE(person.known_as, person.first_names, ''::character varying)::text || ' '::text) || person.surname::text AS name,
    person.email_address,
    person.hide_email
   FROM person
     LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
  WHERE person.crsid IS NOT NULL;

ALTER TABLE apps.leavers_crsid_email_role
    OWNER TO dev;

GRANT ALL ON TABLE apps.leavers_crsid_email_role TO dev;
GRANT SELECT, UPDATE ON TABLE apps.leavers_crsid_email_role TO leavers_trigger;


CREATE OR REPLACE RULE leaver_update_email AS
    ON UPDATE TO apps.leavers_crsid_email_role
    DO INSTEAD
(UPDATE person SET email_address = new.email_address, hide_email = new.hide_email
  WHERE ((person.crsid)::text = (old.crsid)::text));

