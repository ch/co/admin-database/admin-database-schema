DROP VIEW apps.leavers_crsid_email_role;

CREATE VIEW apps.leavers_crsid_email_role
 AS
 SELECT person.crsid,
    person.email_address,
    person.hide_email
   FROM person
  WHERE person.crsid IS NOT NULL;

ALTER TABLE apps.leavers_crsid_email_role
    OWNER TO dev;

GRANT ALL ON TABLE apps.leavers_crsid_email_role TO dev;
GRANT SELECT ON TABLE apps.leavers_crsid_email_role TO leavers_trigger;
