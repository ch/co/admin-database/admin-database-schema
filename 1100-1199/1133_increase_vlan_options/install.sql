-- To record whether a VLAN participates in proxy-arp or WOL packet forwarding
alter table vlan add column if not exists proxy_arp boolean default 'f';
alter table vlan add column if not exists forward_wol boolean default 'f';

-- Record the initial set of VLANs which have these flags set
update vlan set forward_wol='t' where vid in (1102,1239,1201,1202,1203,1205,1206,1207,1209,1210,1211,1212,1215,1216,1232,1107,1100,1112,1104,1217,1110,1106,1238,1111,1113,1116,1108,1118,618,1103);
update vlan set proxy_arp='t' where vid in (1,1103);
