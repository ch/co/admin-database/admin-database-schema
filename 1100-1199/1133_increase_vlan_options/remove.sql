-- To record whether a VLAN participates in proxy-arp or WOL packet forwarding
alter table vlan drop column if exists proxy_arp;
alter table vlan drop column if exists forward_wol;

