ALTER TABLE public.system_image
    ADD COLUMN host_system_image_id bigint;

ALTER TABLE public.system_image
    ADD CONSTRAINT system_image_fk_system_image FOREIGN KEY (host_system_image_id)
    REFERENCES public.system_image (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
