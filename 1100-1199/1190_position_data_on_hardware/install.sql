-- To add positional data to hardware. Somewhat unstructured for the time being.
ALTER TABLE public.hardware ADD COLUMN position JSONB;
