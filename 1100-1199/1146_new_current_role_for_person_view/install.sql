CREATE VIEW public._current_hr_role_for_person AS
SELECT DISTINCT ON (person_id)
    _hr_role.*
FROM _hr_role
ORDER BY person_id,
    CASE
        WHEN predicted_role_status_id = 'Current' THEN 10
        WHEN predicted_role_status_id = 'Past' THEN 5
        WHEN predicted_role_status_id = 'Future' THEN 1
        ELSE 0
    END DESC,
    -- For current roles go for the role with the newest start. This isn't perfect if people hold multiple current roles but should be good enough as newer roles are generally the more senior.
    CASE
        WHEN predicted_role_status_id = 'Current' THEN start_date 
        ELSE NULL
    END DESC NULLS LAST,
    -- If past, picking the role with the most recent start date is usually appropriate (because is start date data generally much more reliable than end date)
    CASE
        WHEN predicted_role_status_id = 'Past' THEN start_date
        ELSE NULL
    END DESC NULLS LAST,
    -- For past roles with equal start dates, go by the earliest end date (and for non-postgrads check the funding end date too). People might leave sooner than their original contract or funding end date, but later makes no sense.
    CASE
        WHEN predicted_role_status_id = 'Past' AND role_tablename = 'postgraduate_studentship' THEN LEAST(end_date,intended_end_date)
        WHEN predicted_role_status_id = 'Past' THEN LEAST(end_date,intended_end_date,funding_end_date) 
        ELSE NULL
    END DESC NULLS LAST,
    -- For future roles, the one starting soonest
    CASE
        WHEN predicted_role_status_id = 'Future' THEN start_date 
        ELSE NULL
    END ASC NULLS LAST
;

ALTER VIEW public._current_hr_role_for_person OWNER TO dev;
