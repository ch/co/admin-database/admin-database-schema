-- They need this to run public.system_image_is_managed_machine
GRANT SELECT (id, reinstall_on_next_boot, was_managed_machine_on, is_managed_mac) ON public.system_image TO headsofgroup;


CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs"
 AS
 SELECT system_image.id,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    system_image.architecture_id,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id,
    system_image.reinstall_on_next_boot,
    COALESCE(system_image.ansible_branch, 'master'::character varying) AS ansible_branch,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    system_image.netboot_id,
    public.system_image_is_managed_machine(system_image.id) AS ro_is_managed_machine,
    system_image.hardware_id AS hardware_pk,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN research_group ON system_image.research_group_id = research_group.id;

-- View: hotwire3.10_View/My_Groups/Computer_Autoinstallation

DROP VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation";

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation"
 AS
 SELECT DISTINCT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.architecture_id,
    system_image.reinstall_on_next_boot,
    array_to_string(ARRAY( SELECT ip_address_unsorted_hid.ip_address_hid
           FROM mm_system_image_ip_address
             JOIN hotwire3.ip_address_unsorted_hid USING (ip_address_id)
          WHERE mm_system_image_ip_address.system_image_id = system_image.id), ','::text) AS ro_ip_address,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id AS ro_research_group_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban AS ro_override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    public.system_image_is_managed_machine(system_image.id) AS ro_is_managed_machine,
    hardware.id AS hardware_pk
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_research_group_computer_rep USING (research_group_id)
     LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
     LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO groupitreps;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO headsofgroup;


-- Rule: group_computers_view_del ON hotwire3."10_View/My_Groups/Computer_Autoinstallation"

-- DROP Rule IF EXISTS group_computers_view_del ON hotwire3."10_View/My_Groups/Computer_Autoinstallation";

CREATE RULE group_computers_view_del AS
    ON DELETE TO hotwire3."10_View/My_Groups/Computer_Autoinstallation"
    DO INSTEAD
(DELETE FROM system_image
  WHERE (system_image.id = old.id));

CREATE TRIGGER hotwire3_mygroups_autoinstall_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/My_Groups/Computer_Autoinstallation"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.mygroups_autoinstallation_upd();


-- View: hotwire3.10_View/My_Groups/Computers

DROP VIEW hotwire3."10_View/My_Groups/Computers";

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Computers"
 AS
 SELECT DISTINCT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    array_to_string(ARRAY( SELECT ip_address_unsorted_hid.ip_address_hid
           FROM mm_system_image_ip_address
             JOIN hotwire3.ip_address_unsorted_hid USING (ip_address_id)
          WHERE mm_system_image_ip_address.system_image_id = system_image.id), ','::text) AS ro_ip_address,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id AS ro_research_group_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban AS ro_override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    public.system_image_is_managed_machine(system_image.id) AS ro_is_managed_machine,
    system_image.hardware_id AS hardware_pk
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_research_group_computer_rep USING (research_group_id)
     LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
     LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE hotwire3."10_View/My_Groups/Computers"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/My_Groups/Computers" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computers" TO groupitreps;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computers" TO headsofgroup;


-- Rule: group_computers_view_del ON hotwire3."10_View/My_Groups/Computers"

-- DROP Rule IF EXISTS group_computers_view_del ON hotwire3."10_View/My_Groups/Computers";

CREATE RULE group_computers_view_del AS
    ON DELETE TO hotwire3."10_View/My_Groups/Computers"
    DO INSTEAD
(DELETE FROM system_image
  WHERE (system_image.id = old.id));

CREATE TRIGGER hotwire3_mygroups_computers_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/My_Groups/Computers"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.mygroups_computers_upd();


-- View: hotwire3.10_View/Computers/Network_Bans

DROP VIEW hotwire3."10_View/Computers/Network_Bans";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Network_Bans"
 AS
 SELECT system_image.id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    system_image.is_managed_mac,
    public.system_image_is_managed_machine(system_image.id) AS ro_is_managed_machine,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    system_image.hardware_id AS hardware_pk,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/Network_Bans"
    OWNER TO dev;

GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Network_Bans" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Network_Bans" TO dev;


CREATE TRIGGER computers_network_bans_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/Computers/Network_Bans"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.system_image_bans_upd();


