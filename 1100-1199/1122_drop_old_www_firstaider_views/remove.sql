CREATE VIEW www.firstaiders
 AS
 SELECT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN building_hid USING (building_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN building_floor_hid USING (building_floor_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
          WHERE p2.id = firstaider.person_id
          ORDER BY room.name), '/'::text) AS ro_room,
    ps.status_id AS status
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id);

ALTER TABLE www.firstaiders
    OWNER TO dev;

GRANT ALL ON TABLE www.firstaiders TO dev;
GRANT SELECT ON TABLE www.firstaiders TO www_sites;

CREATE VIEW www.firstaiders_v2
 AS
 SELECT DISTINCT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid AS person,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained,
    building_hid.building_hid AS building,
    building_floor_hid.building_floor_hid AS building_floor,
    array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms,
    ps.status_id AS status
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     LEFT JOIN mm_person_room USING (person_id)
     LEFT JOIN room ON mm_person_room.room_id = room.id
     LEFT JOIN building_hid ON room.building_id = building_hid.building_id
     LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
     JOIN _physical_status_v3 ps USING (person_id)
  ORDER BY building_hid.building_hid, building_floor_hid.building_floor_hid, person_hid.person_hid, firstaider.id, (person.image_lo::bigint), firstaider.firstaider_funding_id, firstaider.requalify_date, (
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END), (array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text)), ps.status_id;

ALTER TABLE www.firstaiders_v2
    OWNER TO dev;

GRANT ALL ON TABLE www.firstaiders_v2 TO dev;
GRANT SELECT ON TABLE www.firstaiders_v2 TO www_sites;


