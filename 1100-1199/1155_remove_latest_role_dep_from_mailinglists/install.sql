-- view _chem_faculty_mailinglist depends on view _latest_role_v12
CREATE OR REPLACE VIEW public._chem_faculty_mailinglist
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _current_hr_role_for_person lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE (lr.post_category::text = 'Academic staff'::text OR lr.post_category::text = 'Teaching Fellow'::text OR person.counts_as_academic = true) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-faculty'::text))));

-- view _chemacademic_mailinglist depends on view _latest_role_v12
CREATE OR REPLACE VIEW public._chemacademic_mailinglist
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _current_hr_role_for_person lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE (lr.post_category::text = 'Academic staff'::text OR lr.post_category::text = 'Academic-related staff'::text OR person.counts_as_academic = true) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-academic'::text)))) AND person.email_address IS NOT NULL
UNION
 SELECT person.email_address,
    person.crsid,
    NULL::character varying(80) AS person_hid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-academic'::text)) AND ps.status_id::text = 'Current'::text AND person.email_address IS NOT NULL;

-- view _chemacademicrelated_mailinglist depends on view _latest_role_v12

CREATE OR REPLACE VIEW public._chemacademicrelated_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM person
     LEFT JOIN _current_hr_role_for_person lr ON person.id = lr.person_id
     LEFT JOIN _physical_status ps ON ps.id = person.id
  WHERE lr.post_category::text = 'Academic-related staff'::text AND ps.physical_status::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-academic-related'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status ps ON ps.id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-academic-related'::text)) AND ps.physical_status::text = 'Current'::text;


-- view _chememeritus_mailinglist depends on view _latest_role_v12

CREATE OR REPLACE VIEW public._chememeritus_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM person
     LEFT JOIN _current_hr_role_for_person lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE lr.post_category::text = 'Retired'::text AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-emeritus'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-emeritus'::text)) AND ps.status_id::text = 'Current'::text;

-- view _chemgeneral_mailinglist_v2 depends on view _latest_role_v12
DROP VIEW public._chemgeneral_mailinglist_v2;
