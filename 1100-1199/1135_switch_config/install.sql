-- Alter ownership of switch table
Alter table switch owner to dev;

-- To put some config into JSON structures
alter table switch_config_fragment add column config_json jsonb;
alter table switch_port_config_fragment add column config_json jsonb;
alter table vlan add column forward_dhcp boolean default 'f';
alter table switch add column config_json jsonb ;

-- A view to list IP addresses per VLAN, per switch.
CREATE OR REPLACE VIEW public._switch_cfg_vlan_ips_json AS
 SELECT _switch_cfg_vlan_ips.switch_id,
    jsonb_agg(jsonb_build_object('path', ('{"vlans",'::text || _switch_cfg_vlan_ips.vid) || ',"ips %","-0"}'::text, 'sense', 'add', 
     'newval', jsonb_build_object('ip', _switch_cfg_vlan_ips.ip, 'netmask', _switch_cfg_vlan_ips.netmask)) 
    ORDER BY _switch_cfg_vlan_ips.vid) AS config_json
   FROM _switch_cfg_vlan_ips
  GROUP BY _switch_cfg_vlan_ips.switch_id;
alter view public._switch_cfg_vlan_ips_json owner to dev;

-- Initial data for these extra columns
update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","2682","%","0"}','sense','add','newval','voice'),
 jsonb_build_object('path','{"vlans","2682","%","0"}','sense','add','newval','qos priority 6')
) where id=21;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","%","0"}','sense','add','newval','qos priority 4')
) where id=36;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('SQLEXEC',$$ select json_agg(i) from (select 
  jsonb_build_object('path','{"vlans","'||vid||'","name %"}','newval',name,'sense','set') i from vlan) a$$)
) where id=46;

update switch_config_fragment set config_json=jsonb_build_array(                                                                       
 jsonb_build_object('path','{"vlans","1","untagged %",-0}','sense','add','newval',jsonb_build_array('1-48'))
) where id=60;

update switch_config_fragment set config_json=jsonb_build_array(
 --jsonb_build_object('path','{"vlans","191","ip igmp blocked %","0"}','sense','add','newval','%ALLPORTS%'),
 --jsonb_build_object('path','{"vlans","1106","ip igmp blocked %","0"}','sense','add','newval','%ALLPORTS%')
 json_build_object('SQLEXEC',$$
select json_agg(config_json) as config_json from (
 select 
  json_build_object('path','{"vlans","'||vid||'","ip igmp blocked %","-0"}','sense','add','newval',array_agg(name order by name)) config_json 
 from switchport, (select unnest('{191,1106}'::text[]) vid) b where switch_id=%SWITCHID% group by vid)a
 $$)
) where id=67;

update switch_config_fragment set config_json=json_build_array(      
 json_build_object('path','{"vlans","1100","name %"}','sense','set','newval','chswitches'),                  
 json_build_object('path','{"vlans","2682","name %"}','sense','set','newval','VOIP'),
 json_build_object('path','{"vlans","1115","name %"}','sense','set','newval','wren'),
 json_build_object('path','{"vlans","1117","name %"}','sense','set','newval','wren-ext'),
 json_build_object('path','{"vlans","1","name %"}','sense','set','newval','DEFAULT_VLAN'),
 json_build_object('path','{"vlans","3116","name %"}','sense','set','newval','ch-ap'),
 json_build_object('path','{"vlans","618","name %"}','sense','set','newval','chemnet'),
 json_build_object('path','{"%","-0"}','sense','add','newval','gvrp'),
 json_build_object('path','{"%","-0"}','sense','add','newval','aaa port-access gvrp-vlans')
) where id=71;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('SQLEXEC',$$ 
  select json_agg(i) from (
    select jsonb_build_object('path','{"vlans","'||vid||'","%",0}','newval','ip forward-protocol udp 192.168.66.255 9','sense','add') i 
    from vlan where forward_wol 
  )a
 $$)
) where id=75;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('SQLEXEC',$$
  select jsonb_agg(i) from (
    select jsonb_array_elements(jsonb_build_array(
      jsonb_build_object('path','{"vlans","'||vid||'","vrrp","'||vrrpid||'","%",0}','newval','priority '||vrrp_priority_for_switch_id(%SWITCHID%),'sense','add')
                               )||jsonb_agg(
      jsonb_build_object('path','{"vlans","'||vid||'","vrrp","'||vrrpid||'","%",0}','newval','virtual-ip-address '||host(router),'sense','add')
                               )||jsonb_build_array(
      jsonb_build_object('path','{"vlans","'||vid||'","vrrp","'||vrrpid||'","%",99}','newval','enable', 'sense','add')
      )) i 
    from vlan inner join subnet on vlan.id=vlan_id 
              inner join ip_address on ip_address.ip=router 
              inner join mm_system_image_ip_address on ip_address_id=ip_address.id 
              inner join system_image on system_image_id=system_image.id 
              inner join switch using (hardware_id) where vrrpid is not null and switch.id=%SWITCHID%
    group by vid,vrrpid, vrrp_priority_for_switch_id(%SWITCHID%)) a
 $$))
 where id=76;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('SQLEXEC',$$ select json_agg(i) from (select jsonb_build_object('path','{"vlans","'||vid||'","%",0}','newval','ip proxy-arp','sense','add') i from vlan where proxy_arp )a$$)
) where id=86;

update switch_config_fragment set config_json= json_build_array(
 json_build_object('SQLEXEC',$$select json_agg(i) from (select jsonb_build_object('path','{"vlans",'||vid||',"%",0}','newval','ip helper-address '||ip) i from vlan , (select unnest('{131.111.112.9,131.111.112.138,131.111.115.208}'::text[]) ip) a where forward_dhcp) b$$)
) where id=88;

update switch_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('SQLEXEC','select config_json from _switch_cfg_vlan_ips_json where switch_id=%SWITCHID%')
) where id=92;
update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","%EVERYVLAN%","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=1;

update switch_port_config_fragment set config_json=jsonb_build_array(                                                                                         
 jsonb_build_object('path','{"vlans","1","untagged %",-0}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access authenticator % client-limit 1",-0}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access authenticator %",-0}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access mac-based %",-0}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access % controlled-direction in",-0}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %",-0}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=2;


update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=13;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","untagged %",-0}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=19;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","191","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1106","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1101","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1290","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=22;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","191","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1106","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1101","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1290","ip igmp blocked %"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=28;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","2682","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1115","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1117","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","3116","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","618","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect % receiver-action no-disable","-0"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=29;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access authenticator %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"aaa port-access % controlled-direction in","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"interfaces","%PORT%","%","-0"}','sense','add','newval','unknown-vlans block')
) where id=30;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1100","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","2682","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","1115","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","3116","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"vlans","618","tagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect % receiver-action no-disable","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"no snmp-server enable traps link-change %","-0"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=32;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1102","untagged %",-0}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=33;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%'))
) where id=38;

update switch_port_config_fragment set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1115","untagged %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"spanning-tree % bpdu-protection","-0"}','sense','add','newval',jsonb_build_array('%PORT%')),
 jsonb_build_object('path','{"interfaces","%PORT%","%","-0"}','sense','add','newval','unknown-vlans block')
) where id=39;


update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add','newval',jsonb_build_array('5','37')),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2'))
) where id=592;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1111","name %"}','sense','set','newval','chitc'),
 jsonb_build_object('path','{"vlans","1111","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1111","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=624;

update switch set config_json=jsonb_build_array(
jsonb_build_object('path','{"vlans",2682,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3510,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3460,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3116,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",638,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"untagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','A1'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','F5'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','F6'),
jsonb_build_object('path','{"vlans",615,"untagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"name %"}','sense','set','newval','CUDN-Ice'),
jsonb_build_object('path','{"vlans",606,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",1100,"%",0}','sense','add','newval','ip access-group "switch-vlan" vlan-in'),
jsonb_build_object('path','{"vlans",1110,"%",0}','sense','add','newval','ip igmp'),
jsonb_build_object('path','{"vlans",3310,"%",0}','sense','add','newval','ip igmp'),
jsonb_build_object('path','{"vlans",1199,"tagged %"}','sense','set','newval',json_build_array())
) where id=704;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1103","untagged %","-0"}','sense','add','newval','6'),
 jsonb_build_object('path','{"vlans","1103","name %"}','sense','set','newval','chphoto'),
 jsonb_build_object('path','{"vlans","1103","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2','B1','B2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('6')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('6'))
) where id=632;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1111","name %"}','sense','set','newval','chitc'),
 jsonb_build_object('path','{"vlans","1111","untagged %","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"vlans","1111","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('35','38'))
) where id=646;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1201","name %"}','sense','set','newval','chnet1'),
 jsonb_build_object('path','{"vlans","1201","untagged %","-0"}','sense','add','newval',jsonb_build_array('6')),
 jsonb_build_object('path','{"vlans","1201","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('4')),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('4','6')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('4','6'))
) where id=654;

-- cab26-4
-- TODO: Something is setting vlan1 untagged into 1-48.
update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('16')),
 jsonb_build_object('path','{"vlans","1207","name %"}','sense','set','newval','VLAN1207'),
 jsonb_build_object('path','{"vlans","1207","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2','18')),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','VLAN1102'),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2','18')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('16','18')),
 --jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('18'))
) where id=657;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add','newval',jsonb_build_array('41','43','45')),
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2'))
) where id=670;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1113","untagged %","-0"}','sense','add','newval',jsonb_build_array('3','13')),
 jsonb_build_object('path','{"vlans","1113","name %"}','sense','set','newval','VLAN1113'),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('3','8','13')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('3','8','13')),
 jsonb_build_object('path','{"aaa port-access % controlled-direction in","-0"}','sense','add','newval',jsonb_build_array('3','13'))
) where id=673;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1112","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1112","name %"}','sense','set','newval','VLAN1112'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=675;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1112","untagged %","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"vlans","1112","name %"}','sense','set','newval','VLAN1112'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('32'))
) where id=685;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1119","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1119","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1119","name %"}','sense','set','newval','chubb'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=687;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('20')),
 jsonb_build_object('path','{"aaa port-access authenticator % client-limit 1","-0"}','sense','add','newval',jsonb_build_array('20'))
) where id=694;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('45','47'))
) where id=700;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','IPMI'),
 jsonb_build_object('path','{"vlans","3310","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2','10')),
 jsonb_build_object('path','{"vlans","3310","name %"}','sense','set','newval','cctv'),
 jsonb_build_object('path','{"vlans","1280","untagged %","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"vlans","1280","name %"}','sense','set','newval','stub'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('10'))
) where id=702;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi'),
 jsonb_build_object('path','{"vlans","1102","untagged %","-0"}','sense','add','newval',jsonb_build_array('28','29')),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-27','30-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','21','23','27','29')
   -- TODO: Something is untagging VLAN 1 into ports 1-48, overriding this
 ),
 jsonb_build_object('path','{"vlans","1201","name %"}','sense','set','newval','chnet1'),
 jsonb_build_object('path','{"vlans","1201","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1202","name %"}','sense','set','newval','chnet2'),
 jsonb_build_object('path','{"vlans","1202","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('8','16','23','31-32')
 ),
 jsonb_build_object('path','{"vlans","1202","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-7','9-12','13','14-15','17-22','24-26','28','30','33-48','A1-A4')
 ),
 jsonb_build_object('path','{"vlans","1203","name %"}','sense','set','newval','chnet3'),
 jsonb_build_object('path','{"vlans","1203","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1205","name %"}','sense','set','newval','chnet5'),
 jsonb_build_object('path','{"vlans","1205","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1206","name %"}','sense','set','newval','chnet6'),
 jsonb_build_object('path','{"vlans","1206","untagged %","-0"}','sense','add','newval',jsonb_build_array('7','9')),
 jsonb_build_object('path','{"vlans","1206","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-6','8','10-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1207","name %"}','sense','set','newval','chnet7'),
 jsonb_build_object('path','{"vlans","1207","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1209","name %"}','sense','set','newval','chnet9'),
 jsonb_build_object('path','{"vlans","1209","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1210","name %"}','sense','set','newval','chnet10'),
 jsonb_build_object('path','{"vlans","1210","untagged %","-0"}','sense','add','newval',jsonb_build_array('2','21')),
 jsonb_build_object('path','{"vlans","1210","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','3-20','22-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('1')),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('2-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1212","name %"}','sense','set','newval','chnet12'),
 jsonb_build_object('path','{"vlans","1212","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1217","name %"}','sense','set','newval','chnet113'),
 jsonb_build_object('path','{"vlans","1217","untagged %","-0"}','sense','add','newval',jsonb_build_array('27')),
 jsonb_build_object('path','{"vlans","1217","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-26','28-48','A1','A2','A3','A4')
 )
) where id=717;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1199","%","-0"}','sense','add','newval',jsonb_build_array('jumbo')),
 jsonb_build_object('path','{"vlans","1199","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-7','9-48','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1199","name %"}','sense','set','newval','chrepnet')
) where id=721;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1199","%","-0"}','sense','add','newval',jsonb_build_array('jumbo')),
 jsonb_build_object('path','{"vlans","1199","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-47','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1199","name %"}','sense','set','newval','chrepnet')
) where id=728;


update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3510","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","3510","untagged %","-0"}','sense','add','newval',jsonb_build_array('41')),
 jsonb_build_object('path','{"vlans","3510","name %"}','sense','set','newval','VLAN3510')
) where id=738;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 -- TODO: something is setting 1-48 to be /untagged/ into VLAN 1 which overrides this.
 jsonb_build_object('path','{"vlans","1209","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-48','A1','A2')
 ),
 jsonb_build_object('path','{"vlans","1209","name %"}','sense','set','newval','chnet9'),
 jsonb_build_object('path','{"vlans","1212","untagged %","-0"}','sense','add','newval',jsonb_build_array('44')),
 jsonb_build_object('path','{"vlans","1212","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-43','45-48','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1212","name %"}','sense','set','newval','chnet12'),
 jsonb_build_object('path','{"vlans","1102","untagged %","-0"}','sense','add','newval',jsonb_build_array('40')),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-39','41-48','A1','A2')
 ),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi')
) where id=742;

-- Cab57-26
update switch set config_json=jsonb_build_array(
 --jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi'),
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"vlans","1117","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1117","untagged %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1117","name %"}','sense','set','newval','wren-ext'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"interfaces","48","%","-0"}','sense','add','newval','unknown-vlans block')
) where id=802;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","untagged %","_0"}','sense','add',
   'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 ),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 ),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 )
) where id=810;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"interfaces","42","%","-0"}','sense','add','newval','unknown-vlans block'),
 jsonb_build_object('path','{"interfaces","44","%","-0"}','sense','add','newval','unknown-vlans block'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"vlans","1117","untagged %","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"vlans","1117","name %"}','sense','set','newval','wren-ext')
) where id=816;



-- Helper functions

-- Expanding string tokens
CREATE OR REPLACE FUNCTION public._switch_expand_tokens2b(_config text, _switch_id bigint)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
 _stacka text[];
 _stackb text[];
 _configa text;
begin
-- Valid tokens:
-- %LOCATION%
-- %HOSTNAME%
-- %ALLPORTS%
-- %EVERYVLAN%

 -- Start with simple strings
 _configa=replace(_config,E'\r','');
 _configa=replace(_configa,
                 '%LOCATION%',
                 (select '"'||room.name||'"' from switch inner join hardware on hardware_id=hardware.id inner join room on room_id=room.id where switch.id=_switch_id)
                 );
 _configa=replace(_configa,
                  '%HOSTNAME%',
                  (select '"'||hardware.name||'"' from switch inner join hardware on hardware.id=hardware_id where switch.id=_switch_id)
                 );
 _configa=replace(_configa,
                  '%ALLPORTS%',
                  (select procurve_collapse_array(array_agg(name)) from switchport where switch_id=_switch_id)
                 );

 return _configa;
end
$function$
ALTER FUNCTION public._switch_expand_tokens2b(text,bigint) owner to dev;

-- Expanding aribtrary SQL
CREATE OR REPLACE FUNCTION public._switch_expand_sql(_src jsonb, _switch_id bigint)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
 DECLARE
  _sql varchar;
  _rowi record;
  _result jsonb;
  BEGIN
    case jsonb_typeof(_src)
        when 'array' then
                    -- Un-loop an array, applying ourselves to each element
                return jsonb_agg(i) from (select jsonb_array_elements(_switch_expand_sql(jsonb_array_elements(_src),_switch_id))i)a;
                else
            -- if _src has an SQLEXEC entity, expand ig
                if jsonb_path_exists(_src,'$.SQLEXEC') then
                            execute replace(_src->>'SQLEXEC','%SWITCHID%',_switch_id::varchar) into _result;
                                return _result;
                        else
                            -- otherwise pass unchanged
                            return jsonb_agg(_src);
                        end if;
    end case;
 END;
$function$
alter function public._switch_expand_sql(jsonb,bigint) owner to dev;

-- Joining JSONB arrays 
create or replace function _switch_cfg_array_join(_txt text,_arr jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _newarr text[];
BEGIN
 select array_agg(x) from (select repeat(' ',_depth)||jsonb_array_elements_text(_arr) x) a into _newarr;
 return query select replace(_txt,'%',unnest(_newarr));
END
$function$;
alter function _switch_cfg_array_join(text,jsonb,int) owner to dev;

-- Converting lists of IP addresses into 'ip address IP NETMASK' lines
create or replace function _switch_cfg_formatter_iplist(_txt text,_arr jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
    if _arr ='[]'::jsonb then
       return next repeat(' ',_depth)||'no ip address';
    else
       return query select repeat(' ',_depth)||'ip address '||(x#>>'{ip}')||' '||(x#>>'{netmask}') from jsonb_array_elements(_arr) as x(t);
    end if;
END
$function$;a
alter function _switch_cfg_formatter_iplist(text,jsonb,int) owner to dev;

-- Converting a JSONB string into a quoted string
create or replace function _switch_cfg_formatter_string(_txt text,_ident jsonb,_depth int) 
 RETURNS text
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
    return repeat(' ',_depth)||replace(_txt,'%','"'||(_ident#>>'{}')||'"');
END
$function$;
alter function _switch_cfg_formatter_string(text,jsonb,int) owner to dev;

-- convert a JSON representation of an interface to text
create or replace function _switch_interface_formatter(_txt text,_vlans jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','_switch_cfg_array_join',
   'name %','_switch_cfg_formatter_string'
  );
BEGIN
    return next repeat(' ',_depth)||'interface '||_txt;
    for _rowi in select key,value from jsonb_each(_vlans) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$function$;
alter function _switch_interface_formatter(text,jsonb,int) owner to dev;

-- Convert a list of interfaces to text
create or replace function _switch_interfaces_formatter(_txt text,_arr jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _newarr text[];
BEGIN
 return query select _switch_interface_formatter(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$function$;
alter function _switch_interfaces_formatter(text,jsonb,int) owner to dev;

-- Convert a list of switchports to text
create or replace function _switch_portlist(_txt text,_arr jsonb, _depth int) 
 RETURNS text
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
    return repeat(' ',_depth)||replace(_txt,'%',procurve_collapse_array(_arr));
END
$function$;
alter function _switch_portlist(text,jsonb,int) owner to dev;

-- Convert a JSON representation of a VLAN to text
create or replace function _switch_vlan_formatter(_txt text,_vlans jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','_switch_cfg_array_join',
   'ips %','_switch_cfg_formatter_iplist',
   'untagged %','_switch_portlist',
   'tagged %','_switch_portlist',
   'name %','_switch_cfg_formatter_string',
   'ip igmp blocked %','_switch_portlist',
   'vrrp','_switch_vrrps_formatter'
  );
BEGIN
    return next repeat(' ',_depth)||'vlan '||_txt;
    -- key='vrrp' places this last
    for _rowi in select key,value from jsonb_each(_vlans) order by key='vrrp' loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$function$;
alter function _switch_vlan_formatter(text,jsonb,int) owner to dev;

-- Convert a set of VLANs to text
create or replace function _switch_vlans_formatter(_txt text,_vlans jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
 return query select _switch_vlan_formatter(key,value,_depth) from jsonb_each(_vlans) where key !='_template' and key !='%EVERYVLAN%';
END
$function$;
alter function _switch_vlans_formatter(text,jsonb,int) owner to dev;

-- Convert a JSON representation of a single VRRP instance to text
create or replace function _switch_vrrp_formatter(_txt text,_vlans jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','_switch_cfg_array_join'
 );
BEGIN
    return next repeat(' ',_depth)||'vrrp vrid '||_txt;
    for _rowi in select key,value from jsonb_each(_vlans) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,'||(_depth+2)||') a ) 
b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$function$;
alter function _switch_vrrp_formatter(text,jsonb,int) owner to dev;

-- Convert a list of VRRP instances to text
create or replace function _switch_vrrps_formatter(_txt text,_vrrps jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
 return query select _switch_vrrp_formatter(key,value,_depth) from jsonb_each(_vrrps) where key!='_template';
END
$function$;
alter function _switch_vrrps_formatter(text,jsonb,int) owner to dev;

-- Set part of a JSON object to the specified value.
CREATE OR REPLACE FUNCTION public._switch_cfg_set(_oldcfg jsonb, _path text[], _value jsonb, _sense character varying)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
 -- {"path": "{\"vlans\",\"1106\",\"ip igmp blocked\",\"0\"}",
 DECLARE
    _newval jsonb;
    _collectionname text;
    _itemname text;
  BEGIN
    -- raise notice 'Path %',_path;
    -- raise notice 'Cfg %',_oldcfg;
    if ARRAY_LENGTH(_path,1) > 2 then
	  _collectionname=_path[1];
	  _itemname=_path[2];
	  -- Does the top-level object already exist? 
          _oldcfg=_switch_cfg_ensure_object(_oldcfg,_path[1:2]);
	  -- Recurse
	  _newval=_switch_cfg_set(_oldcfg->(_collectionname)->(_itemname),_path[3:],_value,_sense);
	  return jsonb_set(_oldcfg,_path[1:2],_newval,'t'::boolean);
    elsif ARRAY_LENGTH(_path,1) >1 then
	    -- Two keys
	    case jsonb_typeof(_oldcfg->(_path[1]))
	        when 'object' then
                -- Create from _template if missing
		        _oldcfg=_switch_cfg_ensure_object(_oldcfg,_path[1:2]);
                        return jsonb_set(_oldcfg,_path[1:2],_newval,'t'::boolean);
		when 'array' then
		        case coalesce(_sense,'add')
		            when 'set' then
			            -- "Set" an array to _newval, independent of _path[2]
			            return jsonb_set(_oldcfg,_path[1:1],_value,'t'::boolean);
                            when 'append' then
				    -- Treat the position '-0' as append to the end (i.e. -1,true)
                                    return jsonb_insert(_oldcfg,_path,_value,(_path[2]='-0'));
			    when 'add' then
                                                if (jsonb_typeof(_value)='array') then
                                                    -- Concatenate two arrays and set the result
                                                    if _path[0]='-0' then
                                                        return jsonb_set(_oldcfg,_path[1:1],(_oldcfg->(_path[1]))||_value,'t');
                                                    else 
                                                        return jsonb_set(_oldcfg,_path[1:1],_value||(_oldcfg->(_path[1])),'t');
                                                    end if;
                                                else
					            -- Treat the position '-0' as append to the end (i.e. -1,true)
                                                    return jsonb_insert(_oldcfg,_path,_value,(_path[2]='-0'));
                                                end if;
			     else
			         raise EXCEPTION 'Unknown Sense "%"',_sense;
			end case;
            else
		if _oldcfg->(_path[1]) is null then
                        raise exception 'No object element named "%"',_path[1];
                else
		    raise exception 'Unknown type of object "%"',jsonb_typeof(_oldcfg->(_path[1]));
                end if;
      end case;
	else
	  if jsonb_typeof(_oldcfg)='array' then
	      case coalesce(_sense,'add')
		      when 'set' then
			       return jsonb_insert(_oldcfg,_path,_oldcfg||_value,(_path[1]='-0'));
			  when 'add' then
			      return jsonb_insert(_oldcfg,_path,_oldcfg||_value,(_path[1]='-0'));
	          when 'append' then
			   return jsonb_insert(_oldcfg,_path,_oldcfg||_value,(_path[1]='-0'));
			   else
			   raise exception 'Unknown sense "%"',_sense;
		  end case;
	  else
	    -- add / append makes no sense for non-array options.
	    return jsonb_set(_oldcfg,_path,_value,'t');
	  end if;
	end if;
  END;
$function$;
alter function _switch_cfg_set(jsonb,text[],jsonb,varchar) owner to dev;

-- A function to return the configuration for a switch.
CREATE OR REPLACE FUNCTION public._switch_config_b3(v_switch_id bigint)
 RETURNS SETOF text
 LANGUAGE plpgsql
AS $function$
declare
 r record;
 s record;
 v record;
 i integer;
 sql varchar;
 conf varchar;
 pconf varchar[];
 defvlan integer :=1;
 earr jsonb:=jsonb_build_array();
 formatters jsonb:=jsonb_build_object(
   '%','_switch_cfg_array_join',
   'aaa port-access authenticator %','_switch_portlist',
   'aaa port-access % controlled-direction in','_switch_portlist',
   'aaa port-access authenticator % client-limit 1','_switch_portlist',
   'aaa port-access mac-based %','_switch_portlist',
   'spanning-tree % admin-edge-port','_switch_portlist',
   'spanning-tree % bpdu-filter','_switch_portlist',
   'spanning-tree % bpdu-protection','_switch_portlist',
   'spanning-tree % root-guard','_switch_portlist',
   'spanning-tree % tcn-guard','_switch_portlist',
   'loop-protect %','_switch_portlist',
   'loop-protect % receiver-action no-disable','_switch_portlist',
   'no snmp-server enable traps link-change %','_switch_portlist',
   'vlans','_switch_vlans_formatter',
   'interfaces','_switch_interfaces_formatter');
 cfg jsonb:=jsonb_build_object(
    '%',jsonb_build_array(),
    'aaa port-access authenticator %',earr,
    'aaa port-access % controlled-direction in',earr,
    'aaa port-access authenticator % client-limit 1',earr,
    'aaa port-access mac-based %',earr,
    'spanning-tree % admin-edge-port',earr,
    'spanning-tree % bpdu-protection',earr,
    'spanning-tree % bpdu-filter',earr,
    'spanning-tree % root-guard',earr,
    'spanning-tree % tcn-guard',earr,
    'loop-protect %',earr,
    'loop-protect % receiver-action no-disable',earr,
    'no snmp-server enable traps link-change %',earr,
    'vlans',jsonb_build_object(
        '_template',jsonb_build_object(
            'ips %',earr,
            'untagged %',earr,
            'tagged %',earr,
            'name %',null,
            'ip igmp blocked %',earr,
            '%',jsonb_build_array(),
            --'interfaces',jsonb_build_object(
                --'_template',jsonb_build_object(
                    --'%',earr
                --)
            --),
            'vrrp',jsonb_build_object(
                '_template',jsonb_build_object(
                    '%',earr
                )
            )
        )
    ),
    'interfaces',json_build_object(
        '_template',jsonb_build_object(
            'name %',null,
            '%',earr
        )
    )
 );
 vlanrow record;
 vidrow record;
 _rowi record; -- For loops
 trow record; -- To handle loops
 vlans jsonb;
 emptyvrrp jsonb;
 emptyvlan jsonb;
 _everyvid integer[];
begin
 -- Facts, immutable
 select array_agg(vid order by vid) from vlan where vid!=1 into _everyvid;
 -- Basic stuff about the switch
 
 select switch.id, switch_model.model, switch_model.id as model_id, hardware.name as shortname, 
 array(select name from switchport where switch_id=switch.id order by id asc) as allports, 
 is_repnet,
hardware.name as hostname, wired_mac_1, system_image.id, switch.id, serial_number,  room.name as room, 
  array(
      select config from (
          select distinct config,priority from (
              select E'\n; swf '||switch_config_fragment.id||E'\n'||replace(config,E'\r','') config, priority 
			   from mm_switch_config_fragment_switch_model 
			   inner join switch_config_fragment on switch_config_fragment_id=switch_config_fragment.id 
			   inner join mm_switch_goal_applies_to_switch using (switch_config_goal_id) 
			   where switch_id=switch.id and switch_model_id=switch_model.id and not switch_config_fragment.ignore 
			   and switch_config_fragment.config_json is null
			   order by priority asc nulls last) b order by priority) c) as allconf,
case when config_json is not null then '' else extraconfig end as extraconfig
from switch inner join switch_model on switch_model.id=switch_model_id inner join 
hardware on hardware_id=hardware.id inner join system_image on system_image.hardware_id=hardware.id 
left join room on room.id=room_id where switch.id=v_switch_id into r;
 return next '; '||r.model||' Configuration Editor; Created on release X.XX' ;
 return next '; Switch config generated by _switch_config_C for switch ID '||r.id;
 -- return next '; Generated at '||now();
 return next '; serial number '||r.serial_number;
 return next '; model number '||r.model;
 -- return next array_to_string(r.allconf,E'\n!!');
 
    return next ';Section 1';
    if (array_lower(r.allconf,1) is not null) then
        for i in array_lower(r.allconf,1)..array_upper(r.allconf,1) 
		loop
            conf=r.allconf[i];
            conf=replace(
                  replace(
                    conf,
                    '%LOCATION%',r.room),
                    '%HOSTNAME%',r.hostname);
            case 
            when (conf LIKE E'%\\%EVERYVLAN\\%%') THEN
                return query select replace(conf,'%EVERYVLAN%',vid::varchar) from vlan;
            when (conf LIKE E'%\\%ALLPORTS\\%%') THEN
                return next replace(conf,'%ALLPORTS%',procurve_collapse_array(r.allports));
            when (conf like E'%\\%EACHVLANNAME\\%%') THEN
                return query select 
                    replace(
                      replace(
                        replace(
                          replace(conf,'%EACHVLANNAME%',name),
                        '%EACHVLANVID%',vid::varchar),
                      '%EACHVLANDESC%',description),
                    E'\r','') from vlan;
            when (conf like E'%!SQLEXEC%') THEN
                sql=replace(split_part(conf,'!SQLEXEC',2),'%SWITCHID%',r.id::varchar);
				return next '; SQL '||conf;
                return query execute sql;
            ELSE
                return next conf;
            end case;
        end loop;
    end if;


 
 return next ';Section 2';

return next ';Section 3';
return query select case when cconfig LIKE E'%\\%EVERYVLAN\\%%' then
  array_to_string(array(select replace(cconfig,'%EVERYVLAN%',vid::varchar) from vlan where vid!=1),E'\r') 
 else 
  --';'||b.id||' '||b.switch_port_config_fragment_id||E'\r'||
  cconfig 
end
from 

(
 select 
  a.*,
  switch_port_config_fragment_id,
  replace(replace(replace(switch_port_config_fragment.config,'%PORT%',a.name),'%PORTNAME%',coalesce(a.ifacename,a.name)),E'\r','') cconfig 
 from 
(
 select 
  switchport.*, 
  switch_model_id, 
  array(select switch_port_config_goal_id from mm_switch_config_switch_port where switch_port_id=switchport.id) ||
  case when ifacename is not null and ifacename != '' then 12::bigint end||
  --switch_auth_id::bigint || 
  speed_id::bigint 
goals from switchport inner join switch on switch_id=switch.id where switch_id=r.id ) a inner join switch_port_config_goal on switch_port_config_goal.id=any(a.goals) inner join mm_switch_port_config_fragment_switch_model 
	using (switch_model_id) inner join switch_port_config_fragment on switch_port_config_fragment_id=switch_port_config_fragment.id where switch_port_config_fragment.config_json is null and switch_port_config_goal.id=switch_port_config_goal_id order by a.name
) b
;

return next ';Section 4';
--return next cfg->'vlans';
    -- Look for json configuration applying to this switch
	for _rowi in
	select id,jsonb_array_elements(_switch_expand_sql(config_json,v_switch_id)) config_json from _applied_switch_config_fragments where config_json is not null and switch_id=v_switch_id
    loop
        -- for debug return next vlanrow.config_json;
  	    case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    end loop;


 return next '; Section 4b - per-port, non-json';
 --return next cfg->'vlans';
return query select '; spf'||switch_port_config_fragment.id||E'\n'||_switch_expand_tokens(replace(replace(config,E'\r',''),'%PORT%',portlist),v_switch_id) as config from ( 

select
  procurve_collapse_array(array_agg(switchport.name)) as portlist,
  switch_auth_id,
  switch_model_id
from
  switchport
  inner join switch on switch_id = switch.id
where
  switch_id = v_switch_id
group by
  switch_auth_id,
  switch_model_id
) a
inner join switch_port_config_fragment on switch_auth_id = switch_port_config_goal_id
inner join mm_switch_port_config_fragment_switch_model on switch_port_config_fragment_id = switch_port_config_fragment.id
where
  mm_switch_port_config_fragment_switch_model.switch_model_id = a.switch_model_id
  and config_json is null;
  
  return next ';Section 5';
--   return query select _switch_expand_tokens(replace(replace(config,E'\r',''),'%PORT%',portlist),v_switch_id) as config from ( select procurve_collapse_array(array_agg(name)) as portlist,switch_auth_id from switchport where switch_id=v_switch_id group by switch_auth_id) a inner join switch_port_config_fragment on switch_auth_id=switch_port_config_goal_id;
  return next '; Extra config';
  return next replace(r.extraconfig,E'\r','');
  return next ';Section 6';
  
   -- Loop over per-port JSON configuration as required
    for _rowi in 
        select jsonb_array_elements(_switch_expand_sql(config_json,switch_id)) config_json from _switch_port_cfg_json where config_json is not null and switch_id=v_switch_id
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    END LOOP;
return next cfg->'loop-protect%';
    return next ';Section 7';
    --return next cfg->'vlans';
	
	-- Expand %EVERYVLAN% configuration into other VLANs
    if cfg->'vlans'?'%EVERYVLAN%' then
      foreach i in array _everyvid loop
        -- return next (vlans)::text;
	if cfg->'vlans'->'%EVERYVLAN%'->'tagged %' != cfg->'vlans'->'_template'->'tagged %' then
            -- Tagged is initially an empty array, so we concatenate it with the new one
            cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>('{"vlans","'||i||'","tagged %"}')::text[],_value=>(cfg->'vlans'->(i::text)->'tagged %')||(cfg->'vlans'->'%EVERYVLAN%'->'tagged %'),_sense=>'set');
        end if;
        if cfg->'vlans'->'%EVERYVLAN%'->'untagged %' != cfg->'vlans'->'_template'->'untagged %' then
            -- Unagged is initially an empty array, so we concatenate it with the new one
            cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>('{"vlans","'||i||'","untagged %"}')::text[],_value=>(cfg->'vlans'->(i::text)->'untagged %')||(cfg->'vlans'->'%EVERYVLAN%'->'untagged %'),_sense=>'set');
        end if;
      end loop;
    end if;
    -- Switch configuration overrides everything else
    for _rowi in 
        select id,jsonb_array_elements(_switch_expand_sql(config_json,v_switch_id)) config_json from switch where id=v_switch_id and config_json is not null
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    end loop;
        -- Start returning information. Flags first
        for _rowi in select key,value from jsonb_each(cfg) loop
            if formatters?(_rowi.key) then
               return query execute 'select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,0)';
            else
               raise exception 'No formatter specified for %',(_rowi.key);
            end if;
        end loop;
end

$function$;
alter function public._switch_config_b3(bigint) owner to dev;