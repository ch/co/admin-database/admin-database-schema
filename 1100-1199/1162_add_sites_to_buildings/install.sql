CREATE TABLE public.site (
    site_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name TEXT UNIQUE NOT NULL
);
ALTER TABLE public.site OWNER TO dev;
GRANT SELECT ON public.site TO PUBLIC;

INSERT INTO public.site ( name ) VALUES ( 'Lensfield Road' ), ( 'West Cambridge' ), ( 'Engineering' );

ALTER TABLE public.building_hid ADD COLUMN site_id BIGINT REFERENCES public.site (site_id);

UPDATE public.building_hid SET site_id = (SELECT site_id FROM public.site WHERE name = 'West Cambridge') WHERE building_hid = 'West Cambridge Data Centre';
UPDATE public.building_hid SET site_id = (SELECT site_id FROM public.site WHERE name = 'Engineering') WHERE building_hid = 'Engineering';
UPDATE public.building_hid SET site_id = (SELECT site_id FROM public.site WHERE name = 'Lensfield Road') WHERE site_id IS NULL;

ALTER TABLE public.building_hid ALTER COLUMN site_id SET NOT NULL;

CREATE VIEW hotwire3.site_hid AS
    SELECT
        site_id,
        name AS site_hid
    FROM public.site;

ALTER VIEW hotwire3.site_hid OWNER TO dev;
GRANT SELECT ON hotwire3.site_hid TO ro_hid;

DROP VIEW hotwire3."10_View/Buildings/Buildings";

CREATE VIEW hotwire3."10_View/Buildings/Buildings"
 AS
 SELECT building_hid.building_id AS id,
    building_hid.building_hid AS building,
    site_id AS site_id,
    building_hid.embs_building_code,
    building_hid.email_abbreviation AS ro_email_abbreviation
   FROM building_hid;

ALTER TABLE hotwire3."10_View/Buildings/Buildings"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Buildings/Buildings" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/Buildings/Buildings" TO space_management;

CREATE VIEW hotwire3."10_View/Buildings/Sites"
 AS
 SELECT
    site_id AS id,
    name
 FROM public.site;

ALTER TABLE hotwire3."10_View/Buildings/Sites"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/Buildings/Sites" TO space_management;

