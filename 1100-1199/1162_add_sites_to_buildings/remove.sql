DROP VIEW hotwire3."10_View/Buildings/Buildings";

CREATE VIEW hotwire3."10_View/Buildings/Buildings"
 AS
 SELECT building_hid.building_id AS id,
    building_hid.building_hid AS building,
    building_hid.embs_building_code,
    building_hid.email_abbreviation AS ro_email_abbreviation
   FROM building_hid;

ALTER TABLE hotwire3."10_View/Buildings/Buildings"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Buildings/Buildings" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/Buildings/Buildings" TO space_management;


CREATE OR REPLACE RULE hotwire3_view_buildings_ins AS
    ON INSERT TO hotwire3."10_View/Buildings/Buildings"
    DO INSTEAD
(INSERT INTO building_hid (building_hid, embs_building_code)
  VALUES (new.building, new.embs_building_code)
  RETURNING building_hid.building_id,
    building_hid.building_hid,
    building_hid.embs_building_code,
    building_hid.email_abbreviation);

CREATE OR REPLACE RULE hotwire3_view_buildings_upd AS
    ON UPDATE TO hotwire3."10_View/Buildings/Buildings"
    DO INSTEAD
(UPDATE building_hid SET building_hid = new.building, embs_building_code = new.embs_building_code
  WHERE (building_hid.building_id = old.id));


DROP VIEW hotwire3.site_hid;
DROP VIEW hotwire3."10_View/Buildings/Sites";

ALTER TABLE public.building_hid DROP COLUMN site_id;
DROP TABLE public.site;
