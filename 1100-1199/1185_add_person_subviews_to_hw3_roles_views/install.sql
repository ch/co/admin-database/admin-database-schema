-- View: hotwire3.10_View/Roles/Part_III_Students

DROP VIEW hotwire3."10_View/Roles/Part_III_Students";

CREATE VIEW hotwire3."10_View/Roles/Part_III_Students"
 AS
 SELECT a.id,
    a.person_id,
    a.project_title_html,
    a.supervisor_id,
    a.intended_end_date,
    a.end_date,
    a.start_date,
    a.ro_role_status_id,
    a.notes,
    a.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_part_iii_studentship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    a._surname,
    a._first_names
   FROM ( SELECT part_iii_studentship.id,
            part_iii_studentship.person_id,
            part_iii_studentship.project_title AS project_title_html,
            part_iii_studentship.supervisor_id,
            part_iii_studentship.intended_end_date,
            part_iii_studentship.end_date,
            part_iii_studentship.start_date,
            public.role_predicted_status(part_iii_studentship.start_date,part_iii_studentship.end_date,part_iii_studentship.intended_end_date,NULL::date,part_iii_studentship.force_role_status_to_past,'part_iii_studentship'::text) AS ro_role_status_id,
            part_iii_studentship.notes::text AS notes,
            part_iii_studentship.force_role_status_to_past,
            person.surname AS _surname,
            person.first_names AS _first_names
            FROM part_iii_studentship
            JOIN person ON person.id = part_iii_studentship.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire3."10_View/Roles/Part_III_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Part_III_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Part_III_Students" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Part_III_Students" TO student_management;


-- Rule: hotwire3_view_part_iii_del ON hotwire3."10_View/Roles/Part_III_Students"

-- DROP Rule IF EXISTS hotwire3_view_part_iii_del ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_del AS
    ON DELETE TO hotwire3."10_View/Roles/Part_III_Students"
    DO INSTEAD
(DELETE FROM part_iii_studentship
  WHERE (part_iii_studentship.id = old.id));

-- Rule: hotwire3_view_part_iii_ins ON hotwire3."10_View/Roles/Part_III_Students"

-- DROP Rule IF EXISTS hotwire3_view_part_iii_ins ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Part_III_Students"
    DO INSTEAD
(INSERT INTO part_iii_studentship (person_id, project_title, supervisor_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past)
  VALUES (new.person_id, new.project_title_html, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, (new.notes)::character varying(80), new.force_role_status_to_past)
  RETURNING part_iii_studentship.id,
    part_iii_studentship.person_id,
    part_iii_studentship.project_title,
    part_iii_studentship.supervisor_id,
    part_iii_studentship.intended_end_date,
    part_iii_studentship.end_date,
    part_iii_studentship.start_date,
    NULL::character varying(10),
    (part_iii_studentship.notes)::text AS notes,
    part_iii_studentship.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_part_iii_studentship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    NULL::character varying(32),
    NULL::character varying(32)
    );

-- Rule: hotwire3_view_part_iii_upd ON hotwire3."10_View/Roles/Part_III_Students"

-- DROP Rule IF EXISTS hotwire3_view_part_iii_upd ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Part_III_Students"
    DO INSTEAD
(UPDATE part_iii_studentship SET project_title = new.project_title_html, person_id = new.person_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = (new.notes)::character varying(80), force_role_status_to_past = new.force_role_status_to_past
  WHERE (part_iii_studentship.id = old.id));

-- View: hotwire3.10_View/Roles/Erasmus_Students

DROP VIEW hotwire3."10_View/Roles/Erasmus_Students";

CREATE VIEW hotwire3."10_View/Roles/Erasmus_Students"
 AS
 SELECT a.id,
    a.person_id,
    a.erasmus_type_id,
    a.supervisor_id,
    a.email_address,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.cambridge_college_id,
    a.university,
    a.ro_role_status_id,
    a.notes,
    a.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_erasmus_socrates_studentship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    a._surname,
    a._first_names
   FROM ( SELECT erasmus_socrates_studentship.id,
            erasmus_socrates_studentship.person_id,
            erasmus_socrates_studentship.erasmus_type_id,
            erasmus_socrates_studentship.supervisor_id,
            person.email_address,
            erasmus_socrates_studentship.start_date,
            erasmus_socrates_studentship.intended_end_date,
            erasmus_socrates_studentship.end_date,
            person.cambridge_college_id,
            erasmus_socrates_studentship.university,
            public.role_predicted_status(erasmus_socrates_studentship.start_date,erasmus_socrates_studentship.end_date,erasmus_socrates_studentship.intended_end_date,NULL::date,erasmus_socrates_studentship.force_role_status_to_past,'erasmus_socrates_studentship'::text) AS ro_role_status_id,
            erasmus_socrates_studentship.notes::text AS notes,
            erasmus_socrates_studentship.force_role_status_to_past,
            person.surname AS _surname,
            person.first_names AS _first_names
           FROM erasmus_socrates_studentship
           JOIN person ON person.id = erasmus_socrates_studentship.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire3."10_View/Roles/Erasmus_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Erasmus_Students" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Erasmus_Students" TO student_management;


-- Rule: hotwire3_view_erasmus_students_del ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP Rule IF EXISTS hotwire3_view_erasmus_students_del ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE RULE hotwire3_view_erasmus_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Erasmus_Students"
    DO INSTEAD
(DELETE FROM erasmus_socrates_studentship
  WHERE (erasmus_socrates_studentship.id = old.id));

-- Rule: hotwire3_view_erasmus_students_ins ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP Rule IF EXISTS hotwire3_view_erasmus_students_ins ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE RULE hotwire3_view_erasmus_students_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Erasmus_Students"
    DO INSTEAD
(INSERT INTO erasmus_socrates_studentship (person_id, erasmus_type_id, supervisor_id, intended_end_date, end_date, start_date, university, notes, force_role_status_to_past)
  VALUES (new.person_id, new.erasmus_type_id, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.university, (new.notes)::character varying(160), new.force_role_status_to_past)
  RETURNING erasmus_socrates_studentship.id,
    erasmus_socrates_studentship.erasmus_type_id,
    erasmus_socrates_studentship.person_id,
    erasmus_socrates_studentship.supervisor_id,
    NULL::character varying(48) AS "varchar",
    erasmus_socrates_studentship.start_date,
    erasmus_socrates_studentship.intended_end_date,
    erasmus_socrates_studentship.end_date,
    NULL::bigint AS int8,
    erasmus_socrates_studentship.university,
    NULL::character varying(10) AS "varchar",
    (erasmus_socrates_studentship.notes)::text AS notes,
    erasmus_socrates_studentship.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_erasmus_socrates_studentship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar");

-- Rule: hotwire3_view_erasmus_students_upd ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP Rule IF EXISTS hotwire3_view_erasmus_students_upd ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE RULE hotwire3_view_erasmus_students_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Erasmus_Students"
    DO INSTEAD
( UPDATE erasmus_socrates_studentship SET person_id = new.person_id, erasmus_type_id = new.erasmus_type_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, university = new.university, notes = (new.notes)::character varying(160), force_role_status_to_past = new.force_role_status_to_past
  WHERE (erasmus_socrates_studentship.id = old.id);
 UPDATE person SET email_address = new.email_address, cambridge_college_id = new.cambridge_college_id
  WHERE (person.id = old.person_id);
);

-- View: hotwire3.10_View/Roles/Postgrad_Students

DROP VIEW hotwire3."10_View/Roles/Postgrad_Students";

CREATE VIEW hotwire3."10_View/Roles/Postgrad_Students"
 AS
 WITH a AS (
         SELECT postgraduate_studentship.id,
            postgraduate_studentship.person_id,
            person.surname AS ro_surname,
            person.first_names AS ro_first_names,
            role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10) AS ro_role_status,
            person.email_address AS ro_email_address,
            postgraduate_studentship.postgraduate_studentship_type_id,
            postgraduate_studentship.part_time,
            postgraduate_studentship.first_supervisor_id AS supervisor_id,
            postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
            postgraduate_studentship.external_co_supervisor,
            postgraduate_studentship.substitute_supervisor_id,
            postgraduate_studentship.first_mentor_id AS mentor_id,
            postgraduate_studentship.second_mentor_id AS co_mentor_id,
            postgraduate_studentship.external_mentor,
            postgraduate_studentship.university,
            person.cambridge_college_id,
            postgraduate_studentship.paid_through_payroll,
            postgraduate_studentship.emplid_number,
            postgraduate_studentship.start_date,
            person.leaving_date AS dept_leaving_date,
            postgraduate_studentship.intended_end_date AS role_intended_end_date,
            postgraduate_studentship.end_date AS role_end_date,
            postgraduate_studentship.force_role_status_to_past,
            postgraduate_studentship.filemaker_funding::text AS funding,
            postgraduate_studentship.filemaker_fees_funding::text AS fees_funding,
            postgraduate_studentship.funding_end_date,
            postgraduate_studentship.progress_notes::text AS progress_notes,
            ARRAY( SELECT mm2.nationality_id
                   FROM person p3
                     JOIN mm_person_nationality mm2 ON p3.id = mm2.person_id
                  WHERE p3.id = postgraduate_studentship.person_id) AS nationality_id,
                CASE
                    WHEN role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10)::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM postgraduate_studentship
             JOIN person ON postgraduate_studentship.person_id = person.id
        )
 SELECT a.id,
    a.person_id,
    a.ro_surname,
    a.ro_first_names,
    a.ro_email_address,
    a.postgraduate_studentship_type_id,
    a.part_time,
    a.supervisor_id,
    a.co_supervisor_id,
    a.external_co_supervisor,
    a.substitute_supervisor_id,
    a.mentor_id,
    a.co_mentor_id,
    a.external_mentor,
    a.university,
    a.cambridge_college_id,
    a.paid_through_payroll,
    a.emplid_number,
    a.nationality_id,
    a.start_date,
    a.dept_leaving_date,
    a.role_intended_end_date,
    a.role_end_date,
    a.force_role_status_to_past,
    a.ro_role_status,
    a.funding,
    a.fees_funding,
    a.funding_end_date,
    a.progress_notes,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_postgraduate_studentship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    a._cssclass
   FROM a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire3."10_View/Roles/Postgrad_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management_ro;


-- Rule: hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule IF EXISTS hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE RULE hotwire3_view_postgrad_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
(DELETE FROM postgraduate_studentship
  WHERE (postgraduate_studentship.id = old.id));

CREATE TRIGGER hotwire3_view_postgrad_students
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Roles/Postgrad_Students"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.postgrad_studentship_upd();

-- View: hotwire3.10_View/Roles/Visitors

DROP VIEW hotwire3."10_View/Roles/Visitors";

CREATE VIEW hotwire3."10_View/Roles/Visitors"
 AS
 WITH person_visitor AS (
         SELECT visitorship.id,
            visitorship.person_id,
            public.role_predicted_status(visitorship.start_date,visitorship.end_date,visitorship.intended_end_date,NULL::date,visitorship.force_role_status_to_past,'visitorship'::text) AS ro_role_status_id,
            visitorship.home_institution,
            visitorship.visitor_type_id,
            visitorship.host_person_id AS host_id,
            visitorship.start_date,
            visitorship.intended_end_date,
            visitorship.end_date,
            visitorship.notes::text AS notes,
            visitorship.force_role_status_to_past,
            person.surname AS _surname,
            person.first_names AS _first_names
           FROM visitorship
             JOIN person ON person.id = visitorship.person_id
        )
 SELECT person_visitor.id,
    person_visitor.person_id,
    person_visitor.ro_role_status_id,
    person_visitor.home_institution,
    person_visitor.visitor_type_id,
    person_visitor.host_id,
    person_visitor.start_date,
    person_visitor.intended_end_date,
    person_visitor.end_date,
    person_visitor.notes,
    person_visitor.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_visitorship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    person_visitor._surname,
    person_visitor._first_names
   FROM person_visitor
  ORDER BY person_visitor._surname, person_visitor._first_names;

ALTER TABLE hotwire3."10_View/Roles/Visitors"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Visitors" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Visitors" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Visitors" TO mgmt_ro;


-- Rule: hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors"

-- DROP Rule IF EXISTS hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors";

CREATE RULE hotwire3_view_visitors_del AS
    ON DELETE TO hotwire3."10_View/Roles/Visitors"
    DO INSTEAD
(DELETE FROM visitorship
  WHERE (visitorship.id = old.id));

-- Rule: hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors"

-- DROP Rule IF EXISTS hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors";

CREATE RULE hotwire3_view_visitors_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Visitors"
    DO INSTEAD
(INSERT INTO visitorship (home_institution, visitor_type_id, person_id, host_person_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past)
  VALUES (new.home_institution, new.visitor_type_id, new.person_id, new.host_id, new.intended_end_date, new.end_date, new.start_date, (new.notes)::character varying(80), new.force_role_status_to_past)
  RETURNING visitorship.id,
    visitorship.person_id,
    NULL::character varying(10) AS "varchar",
    visitorship.home_institution,
    visitorship.visitor_type_id,
    visitorship.host_person_id AS host_id,
    visitorship.start_date,
    visitorship.intended_end_date,
    visitorship.end_date,
    (visitorship.notes)::text AS notes,
    visitorship.force_role_status_to_past,
    hotwire3.to_hwsubviewb('10_View/Roles/_person_for_role'::character varying, '_visitorship_id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, 'id'::character varying, 'id'::character varying) AS person_data,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar");

-- Rule: hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors"

-- DROP Rule IF EXISTS hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors";

CREATE RULE hotwire3_view_visitors_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Visitors"
    DO INSTEAD
(UPDATE visitorship SET home_institution = new.home_institution, visitor_type_id = new.visitor_type_id, person_id = new.person_id, host_person_id = new.host_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = (new.notes)::character varying(80), force_role_status_to_past = new.force_role_status_to_past
  WHERE (visitorship.id = old.id));

