DROP VIEW hotwire3."30_Report/People_With_All_Roles";

CREATE OR REPLACE VIEW hotwire3."30_Report/People_With_All_Roles"
 AS
 WITH people_all_roles AS (
         SELECT (((person.id::character varying::text || '-'::text) || _role.role_id::character varying::text) || '-'::text) || _role.role_tablename AS id,
            convert_to(E'Content-type: image/jpeg\n\n','UTF8') || person_photo.photo AS ro_image,
            person.surname,
            person.first_names,
            person.known_as,
            person.previous_surname,
            person.title_id,
            person.name_suffix,
            person.gender_id,
            person.date_of_birth,
            ARRAY( SELECT mm.nationality_id
                   FROM mm_person_nationality mm
                  WHERE person.id = mm.person_id) AS nationality_id,
            person.arrival_date,
            person.leaving_date,
            person.left_but_no_leaving_date_given,
            _physical_status.status_id AS presence,
            ARRAY( SELECT mm.room_id
                   FROM mm_person_room mm
                  WHERE person.id = mm.person_id) AS room_id,
            person.forwarding_address::text AS forwarding_address,
            ARRAY( SELECT mm.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number mm
                  WHERE person.id = mm.person_id) AS dept_telephone_number_id,
            person.email_address,
            person.crsid,
            person.mobile_number,
            person.emergency_contact::text AS emergency_contact,
            person.new_employer_address::text AS new_employer_address,
            person.paper_file_details,
            person.cambridge_college_id,
            person.cambridge_address::text AS home_address,
            person.cambridge_phone_number AS home_phone_number,
            person.external_work_phone_numbers,
            person.location,
            person.is_external,
            person.chem_at_cam,
            person.notes,
            person.other_information,
            _role.supervisor_id,
            _role.co_supervisor_id,
            ARRAY( SELECT mm.research_group_id
                   FROM mm_person_research_group mm
                  WHERE person.id = mm.person_id) AS research_group_id,
            _role.post_category,
            person.continuous_employment_start_date,
            _role.start_date AS role_start_date,
            _role.intended_end_date AS role_intended_end_date,
            _role.estimated_role_end_date AS role_estimated_leaving_date,
            _role.funding_end_date,
            _role.end_date AS role_end_date,
            _role.predicted_role_status_id AS role_status,
            _role.funding::text AS funding,
            _role.fees_funding::text AS fees_funding,
            _role.research_grant_number::text AS research_grant_number,
            _role.paid_by_university,
            _role.chem
           FROM person
             LEFT JOIN _hr_role _role ON _role.person_id = person.id
             LEFT JOIN public.person_photo ON person.id = person_photo.person_id
             JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
          WHERE person.is_spri <> true OR person.is_spri IS NULL
        )
 SELECT people_all_roles.id,
    people_all_roles.ro_image,
    people_all_roles.surname,
    people_all_roles.first_names,
    people_all_roles.known_as,
    people_all_roles.previous_surname,
    people_all_roles.title_id,
    people_all_roles.name_suffix,
    people_all_roles.gender_id,
    people_all_roles.date_of_birth,
    people_all_roles.nationality_id,
    people_all_roles.arrival_date,
    people_all_roles.leaving_date,
    people_all_roles.left_but_no_leaving_date_given,
    people_all_roles.presence,
    people_all_roles.room_id,
    people_all_roles.forwarding_address,
    people_all_roles.dept_telephone_number_id,
    people_all_roles.email_address,
    people_all_roles.crsid,
    people_all_roles.mobile_number,
    people_all_roles.emergency_contact,
    people_all_roles.new_employer_address,
    people_all_roles.paper_file_details,
    people_all_roles.cambridge_college_id,
    people_all_roles.home_address,
    people_all_roles.home_phone_number,
    people_all_roles.external_work_phone_numbers,
    people_all_roles.location,
    people_all_roles.is_external,
    people_all_roles.chem_at_cam,
    people_all_roles.notes,
    people_all_roles.other_information,
    people_all_roles.supervisor_id,
    people_all_roles.co_supervisor_id,
    people_all_roles.research_group_id,
    people_all_roles.post_category,
    people_all_roles.continuous_employment_start_date,
    people_all_roles.role_start_date,
    people_all_roles.role_intended_end_date,
    people_all_roles.role_estimated_leaving_date,
    people_all_roles.funding_end_date,
    people_all_roles.role_end_date,
    people_all_roles.role_status,
    people_all_roles.funding,
    people_all_roles.fees_funding,
    people_all_roles.research_grant_number,
    people_all_roles.paid_by_university,
    people_all_roles.chem
   FROM people_all_roles
  ORDER BY people_all_roles.surname, people_all_roles.known_as, people_all_roles.first_names, people_all_roles.role_start_date;

ALTER TABLE hotwire3."30_Report/People_With_All_Roles"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."30_Report/People_With_All_Roles" TO dev;
GRANT SELECT ON TABLE hotwire3."30_Report/People_With_All_Roles" TO hr;
GRANT SELECT ON TABLE hotwire3."30_Report/People_With_All_Roles" TO mgmt_ro;


DROP VIEW hotwire3."30_Report/People_With_Latest_Role";

CREATE OR REPLACE VIEW hotwire3."30_Report/People_With_Latest_Role"
 AS
 SELECT person.id,
    convert_to(E'Content-type: image/jpeg\n\n','UTF8') || person_photo.photo AS ro_image,
    person.surname,
    person.first_names,
    person.known_as,
    person.previous_surname,
    person.title_id,
    person.name_suffix,
    person.gender_id,
    person.date_of_birth,
    ARRAY( SELECT mm.nationality_id
           FROM mm_person_nationality mm
          WHERE person.id = mm.person_id) AS nationality_id,
    person.arrival_date,
    person.leaving_date,
    person.left_but_no_leaving_date_given,
    _physical_status.status_id AS presence,
    ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE person.id = mm.person_id) AS room_id,
    person.forwarding_address::text AS forwarding_address,
    ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE person.id = mm.person_id) AS dept_telephone_number_id,
    person.email_address,
    person.crsid,
    person.mobile_number,
    person.emergency_contact::text AS emergency_contact,
    person.new_employer_address::text AS new_employer_address,
    person.paper_file_details,
    person.cambridge_college_id,
    person.cambridge_address::text AS home_address,
    person.cambridge_phone_number AS home_phone_number,
    person.external_work_phone_numbers,
    person.location,
    person.is_external,
    person.chem_at_cam,
    person.notes,
    person.other_information,
    _latest_role.supervisor_id,
    _latest_role.co_supervisor_id,
    ARRAY( SELECT mm.research_group_id
           FROM mm_person_research_group mm
          WHERE person.id = mm.person_id) AS research_group_id,
    _latest_role.post_category,
    person.continuous_employment_start_date,
    _latest_role.start_date AS role_start_date,
    _latest_role.intended_end_date AS role_intended_end_date,
    _latest_role.estimated_leaving_date AS role_estimated_leaving_date,
    _latest_role.funding_end_date,
    _latest_role.end_date AS role_end_date,
    _latest_role.status AS role_status,
    _latest_role.funding::text AS funding,
    _latest_role.fees_funding::text AS fees_funding,
    _latest_role.research_grant_number::text AS research_grant_number,
    _latest_role.paid_by_university,
    _latest_role.chem
   FROM person
     LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id
     LEFT JOIN public.person_photo ON person.id = person_photo.person_id
     JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
  WHERE person.is_spri <> true OR person.is_spri IS NULL
  ORDER BY person.surname, person.known_as, person.first_names;

ALTER TABLE hotwire3."30_Report/People_With_Latest_Role"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."30_Report/People_With_Latest_Role" TO dev;
GRANT SELECT ON TABLE hotwire3."30_Report/People_With_Latest_Role" TO hr;
GRANT SELECT ON TABLE hotwire3."30_Report/People_With_Latest_Role" TO mgmt_ro;


GRANT SELECT ON TABLE hotwire3."30_Report/People_With_All_Roles" TO mgmt_ro;


