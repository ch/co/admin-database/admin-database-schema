DROP VIEW apps.crsid_and_role;

CREATE OR REPLACE VIEW apps.crsid_and_role
 AS
 SELECT person.crsid,
    person.image_lo::bigint AS image_oid,
    _latest_role.post_category,
    (COALESCE(person.known_as, person.first_names, ''::character varying)::text || ' '::text) || person.surname::text AS name
   FROM person
     LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
  WHERE person.crsid IS NOT NULL AND _latest_role.post_category IS NOT NULL;

ALTER TABLE apps.crsid_and_role
    OWNER TO dev;

GRANT SELECT ON TABLE apps.crsid_and_role TO ad_accounts;
GRANT ALL ON TABLE apps.crsid_and_role TO dev;
GRANT SELECT ON TABLE apps.crsid_and_role TO leavers_trigger;
