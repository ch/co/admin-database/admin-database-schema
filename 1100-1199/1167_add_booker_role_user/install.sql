-- Adding the roles below does not get reversed by the remove.sql script; it edits the
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'booker_user_maintenance') THEN
      CREATE ROLE booker_user_maintenance LOGIN;
   END IF;
END
$do$;

GRANT SELECT ON apps.booker_users TO booker_user_maintenance;
