-- apps._space_people_using_other_groups_space: rename and remove _latest_role_v12
DROP VIEW apps.space_people_using_other_groups_space;

CREATE VIEW apps._space_people_using_other_groups_space
 AS
 SELECT research_group.name AS research_group,
    person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    room.name AS room,
    room_group.name AS room_group
   FROM research_group
     JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id)
     JOIN _latest_role_v12 lr USING (person_id)
     JOIN mm_person_room USING (person_id)
     JOIN room ON mm_person_room.room_id = room.id
     JOIN research_group room_group ON room.responsible_group_id = room_group.id
  WHERE ps.status_id::text = 'Current'::text AND NOT (room_group.name::text IN ( SELECT rg.name
           FROM mm_person_research_group mm1
             JOIN research_group rg ON mm1.research_group_id = rg.id
          WHERE mm1.person_id = person_hid.person_id));

ALTER TABLE apps._space_people_using_other_groups_space
    OWNER TO dev;

GRANT ALL ON TABLE apps._space_people_using_other_groups_space TO dev;
GRANT SELECT ON TABLE apps._space_people_using_other_groups_space TO space_report;


-- _space_hosted_people_by_group: rename and remove _latest_role_v12
DROP VIEW apps.space_hosted_people_by_group;

CREATE VIEW public._space_hosted_people_by_group
 AS
 SELECT DISTINCT person_hid.person_hid AS name,
    room.name AS room,
    lr.post_category,
    lr.estimated_leaving_date,
    supervisor_hid.supervisor_hid,
    research_group.name AS research_group,
    supervisor.crsid AS supervisor_crsid
   FROM room
     JOIN mm_person_room ON room.id = mm_person_room.room_id
     JOIN person ON mm_person_room.person_id = person.id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id)
     JOIN _latest_role_v12 lr USING (person_id)
     JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
     JOIN person supervisor ON lr.supervisor_id = supervisor.id
     JOIN research_group ON room.responsible_group_id = research_group.id
     JOIN person hog ON research_group.head_of_group_id = hog.id
  WHERE ps.status_id::text = 'Current'::text AND hog.crsid::text <> supervisor.crsid::text
  ORDER BY person_hid.person_hid, room.name, lr.post_category, lr.estimated_leaving_date, supervisor_hid.supervisor_hid, research_group.name, supervisor.crsid;

ALTER TABLE public._space_hosted_people_by_group
    OWNER TO dev;

GRANT ALL ON TABLE public._space_hosted_people_by_group TO dev;
GRANT SELECT ON TABLE public._space_hosted_people_by_group TO space_report;



-- _space_people_by_research_group: rename and on view _latest_role_v12
DROP VIEW apps.space_people_by_research_group;

CREATE VIEW public._space_people_by_research_group
 AS
( SELECT DISTINCT person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id) AS rooms,
    person.email_address,
    ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id) AS phones,
    research_group.name AS research_group
   FROM person
     JOIN person_hid ON person.id = person_hid.person_id
     JOIN _physical_status_v3 ps USING (person_id)
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     LEFT JOIN research_group ON research_group.id = mm_person_research_group.research_group_id
  WHERE ps.status_id::text = 'Current'::text
  ORDER BY person_hid.person_hid, lr.post_category, lr.estimated_leaving_date, (ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id)), person.email_address, (ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id)), research_group.name)
UNION
 SELECT person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id) AS rooms,
    person.email_address,
    ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id) AS phones,
    research_group.name AS research_group
   FROM research_group
     JOIN person ON person.id = research_group.head_of_group_id
     JOIN person_hid ON person.id = person_hid.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id;

ALTER TABLE public._space_people_by_research_group
    OWNER TO dev;

GRANT ALL ON TABLE public._space_people_by_research_group TO dev;
GRANT SELECT ON TABLE public._space_people_by_research_group TO space_report;



-- apps._space_report_total_fumehoods_per_group: rename, fix ownership
DROP VIEW apps.space_report_total_fumehoods_per_group;

CREATE VIEW apps._space_report_total_fumehoods_per_group
 AS
 SELECT research_group.name,
    COALESCE(sum(_room_fume_hoods.number_of_fumehoods), 0::numeric) AS number
   FROM research_group
     LEFT JOIN room ON room.responsible_group_id = research_group.id
     LEFT JOIN _room_fume_hoods ON _room_fume_hoods.id = room.id
  GROUP BY research_group.name;

ALTER TABLE apps._space_report_total_fumehoods_per_group
    OWNER TO cen1001;

GRANT ALL ON TABLE apps._space_report_total_fumehoods_per_group TO cen1001;
GRANT SELECT ON TABLE apps._space_report_total_fumehoods_per_group TO space_report;

-- public._space_research_groups_by_crsid: drop
CREATE VIEW public._space_research_groups_by_crsid
 AS
 SELECT person.crsid,
    research_group.name
   FROM person
     JOIN research_group ON person.id = research_group.head_of_group_id
  WHERE person.crsid IS NOT NULL
UNION
 SELECT person.crsid,
    research_group.name
   FROM person
     JOIN research_group ON person.id = research_group.administrator_id;

ALTER TABLE public._space_research_groups_by_crsid
    OWNER TO cen1001;

GRANT ALL ON TABLE public._space_research_groups_by_crsid TO cen1001;
GRANT SELECT ON TABLE public._space_research_groups_by_crsid TO space_report;

-- _space_room_by_research_group: rename
DROP VIEW apps.space_room_by_research_group;

CREATE VIEW public._space_room_by_research_group
 AS
 SELECT DISTINCT room.name,
    room_type_hid.room_type_hid AS type,
    room.maximum_occupancy,
    _room_current_occupancy.current_occupancy,
    room.area,
    _room_fume_hoods.number_of_fumehoods,
    research_group.name AS research_group
   FROM room
     LEFT JOIN room_type_hid USING (room_type_id)
     LEFT JOIN _room_current_occupancy USING (id)
     LEFT JOIN _room_fume_hoods USING (id)
     JOIN research_group ON research_group.id = room.responsible_group_id
  ORDER BY room.name, room_type_hid.room_type_hid, room.maximum_occupancy, _room_current_occupancy.current_occupancy, room.area, _room_fume_hoods.number_of_fumehoods, research_group.name;

ALTER TABLE public._space_room_by_research_group
    OWNER TO cen1001;

GRANT ALL ON TABLE public._space_room_by_research_group TO cen1001;
GRANT SELECT ON TABLE public._space_room_by_research_group TO space_report;
