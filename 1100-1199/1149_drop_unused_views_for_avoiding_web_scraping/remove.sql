-- DROP VIEW apps.jmg11_ticket89275_people;

CREATE VIEW apps.jmg11_ticket89275_people
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.surname,
    person.first_names,
    person.name_suffix,
    title_hid.abbrev_title AS title,
    college.name,
    college.website AS college_website,
    _latest_role.post_category,
    s.status_id AS physical_status,
    ( SELECT research_interest_group.name
           FROM research_interest_group
             LEFT JOIN mm_member_research_interest_group mmpr ON research_interest_group.id = mmpr.research_interest_group_id
          WHERE mmpr.is_primary = true AND mmpr.member_id = person.id) AS primary_rig,
    ARRAY( SELECT research_interest_group.name
           FROM research_interest_group
             LEFT JOIN mm_member_research_interest_group mm ON mm.research_interest_group_id = research_interest_group.id
          WHERE (mm.is_primary = false OR mm.is_primary = NULL::boolean) AND mm.member_id = person.id) AS secondary_rigs,
    ARRAY( SELECT research_group.name
           FROM research_group
             LEFT JOIN mm_person_research_group mmr ON research_group.id = mmr.research_group_id
          WHERE mmr.person_id = person.id) AS in_research_groups,
    ARRAY( SELECT research_group.name
           FROM research_group
          WHERE research_group.head_of_group_id = person.id) AS heads_research_groups,
    ARRAY( SELECT dept_telephone_number.extension_number
           FROM dept_telephone_number
             LEFT JOIN mm_person_dept_telephone_number mmt ON mmt.dept_telephone_number_id = dept_telephone_number.id
          WHERE mmt.person_id = person.id) AS telephone_numbers
   FROM person
     LEFT JOIN cambridge_college college ON person.cambridge_college_id = college.id
     LEFT JOIN title_hid ON person.title_id = title_hid.title_id
     JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
     JOIN _physical_status_v3 s ON s.person_id = person.id
  WHERE person.do_not_show_on_website IS FALSE AND person.crsid IS NOT NULL AND (_latest_role.post_category::text = 'Academic staff'::text OR _latest_role.post_category::text = 'Academic-related staff'::text OR _latest_role.post_category::text = 'Teaching Fellow'::text OR _latest_role.post_category::text = 'Retired'::text OR person.counts_as_academic = true);

ALTER TABLE apps.jmg11_ticket89275_people
    OWNER TO dev;

GRANT ALL ON TABLE apps.jmg11_ticket89275_people TO dev;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_people TO jmg11;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_people TO jmg11_89275;

-- View: apps.jmg11_ticket89275_research_groups

-- DROP VIEW apps.jmg11_ticket89275_research_groups;

CREATE VIEW apps.jmg11_ticket89275_research_groups
 AS
 SELECT research_group.name,
    research_group.website_address
   FROM research_group
  WHERE research_group.internal_use_only <> true;

ALTER TABLE apps.jmg11_ticket89275_research_groups
    OWNER TO dev;

GRANT ALL ON TABLE apps.jmg11_ticket89275_research_groups TO dev;
GRANT ALL ON TABLE apps.jmg11_ticket89275_research_groups TO jmg11;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_research_groups TO jmg11_89275;

-- View: apps.jmg11_ticket89275_rigs

-- DROP VIEW apps.jmg11_ticket89275_rigs;

CREATE VIEW apps.jmg11_ticket89275_rigs
 AS
 SELECT research_interest_group.name,
    research_interest_group.website
   FROM research_interest_group;

ALTER TABLE apps.jmg11_ticket89275_rigs
    OWNER TO dev;

GRANT ALL ON TABLE apps.jmg11_ticket89275_rigs TO dev;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_rigs TO jmg11;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_rigs TO jmg11_89275;
