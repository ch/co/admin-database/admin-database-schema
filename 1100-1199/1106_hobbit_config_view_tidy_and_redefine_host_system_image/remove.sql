CREATE VIEW apps.hobbit_config_system_images
 AS
 SELECT system_image.id AS system_image_id,
    ip_address.ip,
    ip_address.hostname,
    (((COALESCE(system_image.hobbit_flags::text || ' '::text, ''::text) || COALESCE(ip_address.hobbit_flags::text, ''::text)) || COALESCE('NET:'::text || subnet.hobbit_net_name::text, ''::text)))::character varying(500) AS hobbit_flags,
    COALESCE(( SELECT min(child_si.hobbit_tier) AS min
           FROM mm_dom0_domu
             JOIN system_image child_si ON mm_dom0_domu.domu_id = child_si.id
          WHERE mm_dom0_domu.dom0_id = system_image.id), system_image.hobbit_tier)::character varying(1) AS hobbit_tier,
    system_image.is_development_system,
    research_group.name,
    operating_system.os,
    hardware_type.name AS type,
    room.name AS room,
    ((((COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text) || ' ('::text) || person.email_address::text) || ')'::text AS machine_user,
    system_image.comments,
    system_image.host_system_image_id,
    subnet.domain_name,
    subnet.monitor_with_hobbit,
    subnet.is_pool_subnet
   FROM system_image
     JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
     JOIN subnet ON ip_address.subnet_id = subnet.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN person ON system_image.user_id = person.id
     LEFT JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN hardware_type ON hardware.hardware_type_id = hardware_type.id
     JOIN room ON hardware.room_id = room.id;

ALTER TABLE apps.hobbit_config_system_images
    OWNER TO cen1001;

GRANT ALL ON TABLE apps.hobbit_config_system_images TO cen1001;
GRANT SELECT ON TABLE apps.hobbit_config_system_images TO cos;
GRANT SELECT ON TABLE apps.hobbit_config_system_images TO hobbit;

DROP VIEW apps.hobbit_config_system_images_v2;

CREATE OR REPLACE VIEW apps.hobbit_config_system_images_v2
 AS
 SELECT system_image.id AS system_image_id,
    ip_address.ip,
    ip_address.hostname,
    system_image.friendly_name AS known_to_xymon_as,
    (((COALESCE(system_image.hobbit_flags::text || ' '::text, ''::text) || COALESCE(ip_address.hobbit_flags::text || ' '::text, ''::text)) || COALESCE('NET:'::text || subnet.hobbit_net_name::text, ''::text)))::character varying(500) AS hobbit_flags,
    ((( SELECT min(all_tiers.hobbit_tier) AS min
           FROM ( SELECT child_si.hobbit_tier
                   FROM mm_dom0_domu
                     JOIN system_image child_si ON mm_dom0_domu.domu_id = child_si.id
                  WHERE mm_dom0_domu.dom0_id = system_image.id
                UNION
                 SELECT this_si.hobbit_tier
                   FROM system_image this_si
                  WHERE this_si.id = system_image.id) all_tiers)))::character varying(1) AS hobbit_tier,
    system_image.is_development_system,
    research_group.name,
    operating_system.os,
    hardware_type.name AS type,
    room.name AS room,
    ((((COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text) || ' ('::text) || person.email_address::text) || ')'::text AS machine_user,
    system_image.comments,
    system_image.host_system_image_id,
    subnet.domain_name,
    subnet.monitor_with_hobbit,
    subnet.is_pool_subnet,
    hobbit_clients_config_stanza.settings AS clients_config_settings
   FROM system_image
     JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
     JOIN subnet ON ip_address.subnet_id = subnet.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN person ON system_image.user_id = person.id
     LEFT JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN hardware_type ON hardware.hardware_type_id = hardware_type.id
     JOIN room ON hardware.room_id = room.id
     LEFT JOIN hobbit_clients_config_stanza ON system_image.id = hobbit_clients_config_stanza.system_image_id;

ALTER TABLE apps.hobbit_config_system_images_v2
    OWNER TO dev;

GRANT SELECT ON TABLE apps.hobbit_config_system_images_v2 TO cos;
GRANT ALL ON TABLE apps.hobbit_config_system_images_v2 TO dev;
GRANT SELECT ON TABLE apps.hobbit_config_system_images_v2 TO hobbit;


