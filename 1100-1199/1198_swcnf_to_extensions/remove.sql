drop extension "hotwire-swcnf";
drop extension swcnf;

CREATE FUNCTION swcnf.apply_switch(_cfg jsonb,_switchid bigint)
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
DECLARE
    _rowi record;
    _everyvid integer[];
    --_path text[];
BEGIN
    -- Switch configuration overrides everything else
    for _rowi in 
        select id,jsonb_array_elements(swcnf.expand_sql(config_json,_switchid)) config_json from switch where id=_switchid and config_json is not null
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'),_switchid=>_switchid);
            when '' then
                if jsonb_typeof(_rowi.config_json->'path')!='array' then
                    raise exception 'Path found not to be an array in %',_rowi.config_json;
                end if;
                --select array_agg(a)::text[] from (select jsonb_array_elements_text(_rowi.config_json->'path') a) b into _path;
                if _rowi.config_json->'newval' is null then
                    raise exception 'Null value found in _switch_cfg_apply_switch';
                end if; 
			    _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>_rowi.config_json->'path',_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'),_switchid=>_switchid);
	        else
	    end case;
    end loop;
    return _cfg;
END

$_$;
CREATE FUNCTION swcnf.apply_switch_fragments(_cfg jsonb, _switchid bigint)
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
DECLARE
    _rowi record;
    -- _path text[];
BEGIN
    -- Look for json configuration applying to this switch
    -- raise notice 'Called with CFG=%',_cfg::text;
	for _rowi in
	select 
        id,
        jsonb_array_elements(swcnf.expand_sql(config_json,_switchid)) config_json 
    from swcnf.applied_switch_config_fragments where config_json is not null and switch_id=_switchid
    loop
        -- raise notice 'Setting config according to %',_rowi;
        -- raise notice 'Setting via path %',_rowi.config_json->'path';
        if jsonb_typeof(_rowi.config_json->'path')!='array' then
            raise exception 'Path found not to be an array in %',_rowi.config_json;
        end if;
        if _rowi.config_json->'newval' is null then
            raise exception 'Null value found in apply_switch_fragments';
        end if;
        _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>_rowi.config_json->'path',_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense')::varchar,_switchid=>_switchid);
    end loop;
    return _cfg;
END

$_$;

CREATE FUNCTION swcnf.apply_switch_port_fragments(_cfg jsonb, _switchid bigint)
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
DECLARE
    _rowi record;
   --  _path text[];
BEGIN
   -- Loop over per-port JSON configuration as required
    for _rowi in 
        select id,jsonb_array_elements(swcnf.expand_sql(config_json,switch_id)) config_json from swcnf.applied_switch_port_cfg_json where config_json is not null and switch_id=_switchid
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'),_switchid=>_switchid);
            when '' then
                if jsonb_typeof(_rowi.config_json->'path')!='array' then
                    raise exception 'Path found not to be an array in %',_rowi.config_json;
                end if; 
                -- select array_agg(a)::text[] from (select jsonb_array_elements_text(_rowi.config_json->'path') a) b into _path;
                if _rowi.config_json->'newval' is null then
                    raise exception 'Null value found in _switch_cfg_apply_switch_port_fragments';
                end if;
                -- raise notice 'Applying switch port config fragment %',_rowi.id;
			    _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>_rowi.config_json->'path',_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'),_switchid=>_switchid);
	        else
	    end case;
    END LOOP;
    return _cfg;
END

$_$;

CREATE FUNCTION swcnf.def_formatterlist()
    -- Return JSON from this function to retain key ordering.
    RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE
    formatters json:= json_build_object(
        'module %','swcnf.formatter_array_join',
        '%','swcnf.formatter_array_join',
        'ip access-list extended','swcnf.formatter_ipacls_ext',
        'ip access-list standard','swcnf.formatter_ipacls_std',
        'aaa port-access authenticator %','swcnf.formatter_portlist',
        'aaa port-access % controlled-direction in','swcnf.formatter_portlist',
        'aaa port-access authenticator % client-limit 1','swcnf.formatter_portlist',
        'aaa port-access mac-based %','swcnf.formatter_portlist',
        'spanning-tree % admin-edge-port','swcnf.formatter_portlist',
        'spanning-tree % bpdu-filter','swcnf.formatter_portlist',
        'spanning-tree % bpdu-protection','swcnf.formatter_portlist',
        'spanning-tree % root-guard','swcnf.formatter_portlist',
        'spanning-tree % tcn-guard','swcnf.formatter_portlist',
        'no snmp-server enable traps link-change %','swcnf.formatter_portlist',
        'vlan','swcnf.formatter_vlans',
        'oobm','swcnf.formatter_oobm',
        'loop-protect %','swcnf.formatter_portlist',
        'device-identity','swcnf.formatter_device_identities',
        'device-profile','swcnf.formatter_device_profiles',
        'interface','swcnf.formatter_interfaces',
        'loop-protect % receiver-action no-disable','swcnf.formatter_portlist'   
    );
BEGIN
    RETURN formatters;
END
$_$;
CREATE FUNCTION swcnf.empty_cfg()
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
DECLARE
    earr jsonb:=jsonb_build_array();
    cfg jsonb:=jsonb_build_object(
        'module %',jsonb_build_array(),
        '%',jsonb_build_array(),
        'aaa port-access authenticator %',earr,
        'aaa port-access % controlled-direction in',earr,
        'aaa port-access authenticator % client-limit 1',earr,
        'aaa port-access mac-based %',earr,
        'spanning-tree % admin-edge-port',earr,
        'spanning-tree % bpdu-protection',earr,
        'spanning-tree % bpdu-filter',earr,
        'spanning-tree % root-guard',earr,
        'spanning-tree % tcn-guard',earr,
        'loop-protect %',earr,
        'loop-protect % receiver-action no-disable',earr,
        'no snmp-server enable traps link-change %',earr,
        'ip access-list standard',jsonb_build_object(
            '_template',json_build_object(
                '%',earr
            )
        ),
        'ip access-list extended',jsonb_build_object(
            '_template',json_build_object(
                '%',earr
            )
        ),
        'device-identity',jsonb_build_object(
            '_template',jsonb_build_object(
                'name %',earr,
                'lldp oui',earr,
                'sub-type',earr
            )
        ),
        'device-profile',jsonb_build_object(
            '_template',jsonb_build_object(
                'cos %',earr,
                '%',earr
            )
        ),
        'oobm',jsonb_build_object(
            '_template',jsonb_build_object(
                'ips %',earr,
                '%',earr
            )
        ),
        'vlan',jsonb_build_object(
            '_template',jsonb_build_object(
                'ips %',earr,
                'untagged %',earr,
                'no untagged %',earr,
                'tagged %',earr,
                'name %',earr,
                'ip igmp blocked %',earr,
                '%',jsonb_build_array(),
                --'interfaces',jsonb_build_object(
                    --'_template',jsonb_build_object(
                        --'%',earr
                    --)
                --),
                'vrrp',jsonb_build_object(
                    '_template',jsonb_build_object(
                        '%',earr
                    )
                )
            )
        ),
        'interface',json_build_object(
            '_template',jsonb_build_object(
                'name %',earr,
                '%',earr
            )
        )
    );
    BEGIN
        RETURN cfg;
    END
$_$;

CREATE FUNCTION swcnf.expand_everyvlan(_cfg jsonb,_switchid bigint)
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
DECLARE
    _rowi record;
    _everyvid integer[];
    i integer;
    _path text[];
    _curval jsonb;
    _class text;
BEGIN
    --raise notice 'Expanding EVERYVLAN in %',_cfg;
    select array_agg(vid order by vid) from vlan where vid!=1 into _everyvid;
	-- Expand %EVERYVLAN% configuration into other VLANs
    if _cfg->'vlan'?'%EVERYVLAN%' then
        foreach i in array _everyvid loop
            foreach _class in array array['tagged %','untagged %']::text[] loop
                -- return next (vlans)::text;
                if _cfg->'vlan'->'%EVERYVLAN%'->_class != _cfg->'vlan'->'_template'->_class then
                    _curval='[]'::jsonb;
                    -- If we have some existing data, include it
                    if _cfg->'vlan'?(i::text) 
                    and _cfg->'vlan'->(i::text)?_class
                    and jsonb_typeof(_cfg->'vlan'->(i::text)->_class)!='null' 
                    and (_cfg->'vlan'->(i::text)->_class)!='[]'::jsonb then
                        _curval=_cfg->'vlan'->(i::text)->_class;
                    end if;
                    _cfg=swcnf.apply_config(_oldcfg=>_cfg,_path=>jsonb_build_array(jsonb_build_object('vlan',i::text),_class),_value=>_curval||(_cfg->'vlan'->'%EVERYVLAN%'->_class),_sense=>'set',_switchid=>_switchid);
                end if;
            end loop;
        end loop;
    end if;
    return _cfg;
END
$_$;

CREATE FUNCTION swcnf.output(_cfg jsonb, _formatters json) 
    RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    _rowi record;
    _buffer text[];
    _formattersb jsonb;
BEGIN
    _formattersb=_formatters;
    -- raise notice 'JSON %',_cfg::text;
    -- raise notice 'FORMATTERS %',_formatters::text;
    -- Start returning information. Flags first
    for _rowi in select c.key, c.value from jsonb_each(_cfg) c left join (select row_number() over (), * from json_each(_formatters))a on a.key=c.key order by row_number loop
        if _formattersb?(_rowi.key) then
            execute 'select array_agg(x) from (select '||(_formattersb->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,0) x ) a' into _buffer;
            if _buffer is not null then
                return query select unnest(array_remove(_buffer,NULL));
            end if;
        else
            raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
END
$_$;
CREATE OR REPLACE FUNCTION swcnf.vrrp_priority_for_switch_id(_swid integer)
    RETURNS integer
    LANGUAGE plpgsql
    AS $function$
BEGIN
    RETURN CASE WHEN _swid = 704 THEN
        255
    ELSE
        200
    END;
END;
$function$;

--
-- Name: swcnf.apply_config(jsonb, text[], jsonb, character varying, bigint); Type: FUNCTION; Schema: public; Owner: dev
--
CREATE FUNCTION swcnf.apply_config(_oldcfg jsonb, _path jsonb, _value jsonb, _sense character varying, _switchid bigint)
    RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    -- {"path": "{\"vlans\",\"1106\",\"ip igmp blocked\",\"0\"}",
DECLARE
    _newval jsonb;
    _collectionname text;
    _itemname text;
    _stack text;
    _ident text;
    _p0 jsonb;
BEGIN
    IF _value IS NULL THEN
        RAISE EXCEPTION 'May not set to a NULL value: called with path % and config % with sense %', _path, _oldcfg, _sense;
    END IF;
    -- raise notice 'swcnf.apply_config Path % to %',_path,_oldcfg;
    -- If (and only if) our value is a string, expand any tokens therein.
    IF jsonb_typeof(_value) = 'string' THEN
        _value = to_jsonb(swcnf.expand_tokens(_value #>> '{}', _switchid));
    END IF;
    -- Get the first value of our path.
    _p0 = _path -> 0;
    -- Differentiate between different types of object in the path:
    CASE jsonb_typeof(_p0)
    WHEN 'object' THEN
        SELECT key FROM jsonb_each_text(_p0) LIMIT 1 INTO _collectionname;
        SELECT value FROM jsonb_each_text(_p0) LIMIT 1 INTO _itemname;
        -- raise notice 'Before ensure_object, oldcfg = %',_oldcfg::text;
        _oldcfg = swcnf.ensure_object(_oldcfg, ARRAY[_collectionname, _itemname]);
        -- raise notice 'After ensure_object, oldcfg = %',_oldcfg::text;
        -- _path-0 returns the array without the first (0-indexed) item.
        _newval = swcnf.apply_config(_oldcfg ->(_collectionname) ->(_itemname),(_path - 0), _value, _sense, _switchid); 
        -- raise notice '_newval = %',_newval::text;
        RETURN jsonb_set(_oldcfg, ARRAY[_collectionname, _itemname] , _newval, 't'::boolean);
    WHEN 'string' THEN
        -- It's a string.
        CASE jsonb_typeof(_oldcfg ->(_path ->> 0))
        WHEN 'object' THEN
            -- Create from _template if missing
            -- _oldcfg = swcnf.ensure_object(_oldcfg, _path[1:2]);
            -- RETURN jsonb_set(_oldcfg, _path[1:2], _newval, 't'::boolean);
            RAISE NOTICE 'Object specified without an identifier.';
            RAISE EXCEPTION 'Path is %',_path::text;
        WHEN 'array' THEN
            -- Is this ever reached?
            IF 'set' = coalesce(_sense, 'add') THEN
                -- "Set" an array to _newval
                RETURN jsonb_set(_oldcfg, ARRAY[_path ->> 0], '[]'::jsonb||_value, 't'::boolean);
            ELSE
                -- || is overloaded as concat(array, array) or concat(array, element). Ace.
                RETURN jsonb_set(_oldcfg, ARRAY[_path ->> 0],(_oldcfg ->(_path ->> 0)) || _value, 't');
            END IF;
        ELSE
            IF _oldcfg ->(_path ->> 0) IS NULL THEN
                RAISE NOTICE '_oldcfg = %', _oldcfg::text; 
                GET DIAGNOSTICS _stack = PG_CONTEXT; 
                RAISE NOTICE 'stack = %', _stack; 
                RAISE EXCEPTION 'No object element named "%"', _path ->> 0;
            ELSE
                RAISE NOTICE '_oldcfg = %', _oldcfg::text;
                RAISE NOTICE '_path = %', _path::text;
                RAISE EXCEPTION 'Type of config object "%" is neither object nor array', jsonb_typeof(_oldcfg ->(_path ->> 0));
            END IF;
        END CASE;
    ELSE
        RAISE EXCEPTION 'Element of path is neither object nor string: %', _path;
    END CASE;
    RAISE EXCEPTION 'End of swcnf.apply_config reached without return.';
END;

$$;

-- ALTER FUNCTION apply_config(_oldcfg jsonb, _path text[], _value jsonb, _sense character varying, _switchid bigint) OWNER TO dev;
--
-- Name: swcnf.ensure_object(jsonb, text[]); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.ensure_object(_oldcfg jsonb, _path text[]) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
declare
    _collectionname text;
    _itemname text;
    _templatename text:='_template';
begin
    if array_length(_path,1)!=2 then
        raise EXCEPTION 'Require exactly one collection and one item. Got %',_path;
    end if;
	_collectionname=_path[1];
	_itemname=_path[2];
	-- Does the top-level object already exist? 
	if _oldcfg?_collectionname then
	    -- Well, the collection exists....
		if not _oldcfg->(_collectionname)?(_itemname) then
		    if not _oldcfg->(_collectionname)?(_templatename) then
		        raise EXCEPTION '_template not found in collection "%" in config %',_collectionname,_oldcfg;
		    end if;
		    -- Create the key from _template
		    _oldcfg=jsonb_set(_oldcfg,_path[1:2],_oldcfg->(_collectionname)->(_templatename),'t'::boolean);
		end if;
	else
            -- raise NOTICE '%',_oldcfg;
	    raise EXCEPTION 'Collection not found "%", in config %', _path[1],_oldcfg;
	end if;
	return _oldcfg;
end;
$$;


-- ALTER FUNCTION ensure_object(_oldcfg jsonb, _path text[]) OWNER TO dev;
--
-- Name: swcnf.expand_sql(jsonb, bigint); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.expand_sql(_src jsonb, _switch_id bigint) RETURNS jsonb
    LANGUAGE plpgsql
    AS $_$
 DECLARE
  _sql varchar;
  _rowi record;
  _result jsonb;
  BEGIN
    case jsonb_typeof(_src) 
        when 'array' then
		    -- Un-loop an array, applying ourselves to each element
	        return jsonb_agg(i) from (select jsonb_array_elements(swcnf.expand_sql(jsonb_array_elements(_src),_switch_id))i)a;
		else
            -- if _src has an SQLEXEC entity, expand ig
	        if jsonb_path_exists(_src,'$.SQLEXEC') then
--raise notice 'Executing %',replace(_src->>'SQLEXEC','%SWITCHID%',_switch_id::varchar);
			    execute replace(_src->>'SQLEXEC','%SWITCHID%',_switch_id::varchar) into _result;
				return _result;
			else
			    -- otherwise pass unchanged
			    return jsonb_agg(_src);
			end if;
    end case;
 END;
$_$;


-- ALTER FUNCTION expand_sql(_src jsonb, _switch_id bigint) OWNER TO dev;
--
-- Name: swcnf.expand_tokens(text, bigint); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.expand_tokens(_config text, _switch_id bigint) RETURNS SETOF text
    LANGUAGE plpgsql ROWS 10
    AS $$ 
declare 
 _stacka text[];
 _stackb text[];
 _replacement text;
 _configa text;
begin
    -- Valid tokens:
    -- %LOCATION%
    -- %HOSTNAME%
    -- %ALLPORTS%
    -- %EVERYVLAN%

    -- %EACHVLANNAME%
    -- %EACHVLANVID%
    -- %EACHVLANDESC%

    -- Start with simple strings
    _configa=replace(_config,E'\r','');
    -- raise notice 'Expanding tokens in %',_configa;
    if _configa LIKE '%\%LOCATION\%%' THEN
        select '"'||room.name||'"' from switch inner join hardware on hardware_id=hardware.id inner join room on room_id=room.id where switch.id=_switch_id into _replacement;
        _configa=replace(_configa,'%LOCATION%',coalesce(_replacement,'"unknown"'));
    end if;
    if _configa LIKE '%\%HOSTNAME\%%' THEN
        select '"'||hardware.name||'"' from switch inner join hardware on hardware.id=hardware_id where switch.id=_switch_id into _replacement;
        _configa=replace(_configa,'%HOSTNAME%',coalesce(_replacement,'unknown'));
    end if;
    if _configa LIKE '%\%ALLPORTS\%%' THEN
        select swcnf.procurve_collapse_array(array_agg(name)) from switchport where switch_id=_switch_id into _replacement;
        _configa=replace(_configa, '%ALLPORTS%',coalesce(_replacement,''));
    end if;
    
    -- EVERYVLAN is VID of all vlans VID != 1
    -- EACHVLANVID is VID of all vlans
    -- These are thus orthogonal
    if (_configa LIKE E'%\\%EVERYVLAN%\\%%') then
        _configa=(select array_to_string(array_agg(replace(_configa,'%EVERYVLAN%',vid::varchar)),E'\r') from vlan where vid !=1);
    elsif (_configa LIKE E'%\\%EACHVLAN%') then
        _configa=(select array_to_string(array_agg(replace(
				   replace(
                                    replace(
				            _configa,
				            '%EACHVLANVID%',
				            vid::varchar
				           ),
				    '%EACHVLANNAME%',
				    name
                                   ),'%EACHVLANDESC%',description)
                                   ),E'\r') from vlan);
    end if;
    -- raise notice 'Tokens expanded: %',_configa;
    return next _configa;
end
$$;


-- ALTER FUNCTION expand_tokens(_config text, _switch_id bigint) OWNER TO dev;
--
-- Name: swcnf.formatter_array_join(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_array_join(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
 _newarr text[];
BEGIN
 select array_agg(x order by x) from (select repeat(' ',_depth)||jsonb_array_elements_text(_arr) x) a into _newarr;
 return query select replace(_txt,'%',unnest(_newarr));
END
$$;


-- ALTER FUNCTION formatter_array_join(_txt text, _arr jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_device_identities(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_identities(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    return query select swcnf.formatter_device_identity(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$$;


-- ALTER FUNCTION formatter_device_identities(_txt text, _arr jsonb, _depth integer) OWNER TO dev;

--
-- Name: _switch_device_identity__formatter(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_identity(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
BEGIN
    return next repeat(' ',_depth)||'device-identity name "'||_key||'"';
    if _jobj->>'lldp oui %' is not null then
        if _jobj?'sub-type %' and _jobj->>'sub-type %' is not null then
            return next swcnf.formatter_unquotedstring('lldp oui %',jsonb_build_array()||(_jobj->'lldp oui %'),2+_depth)||swcnf.formatter_unquotedstring('sub-type %',jsonb_build_array()||(_jobj->'sub-type %'),1);
        else
            return next swcnf.formatter_unquotedstring('lldp oui %',jsonb_build_array()||(_jobj->'lldp oui %'),2+_depth);
        end if;
    end if;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;


-- ALTER FUNCTION formatter_device_identity(_txt text, _jobj jsonb, _depth integer) OWNER TO dev;


--
-- Name: swcnf.formatter_interface(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_profile(_txt text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 _class text;
 _ident text;
BEGIN
    _class=split_part(_txt,' ',1);
    _ident=split_part(_txt,' ',2);
    case _class
        when 'device-type' then
            return query select swcnf.formatter_device_profile_devicetype(_ident, _jobj, _depth);
        when 'name' then
            return query select swcnf.formatter_device_profile_name(_ident,_jobj,_depth);
        when 'type' then
            return query select swcnf.formatter_device_profile_type(_ident,_jobj,_depth);
        else
            raise exception 'Unknown type of device-profile "%"',_class;
    end case;
END
$_$;


-- ALTER FUNCTION formatter_interface(_txt text, _vlans jsonb, _depth integer) OWNER TO dev;


--
-- Name: swcnf.formatter_device_profile(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_profile_devicetype(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'associate %','swcnf.formatter_quotedstring'
  );
BEGIN
    return next swcnf.formatter_quotedstring('device-profile device-type %',(jsonb_build_array()||to_jsonb(_key)),_depth);
    -- No particular ordering required here
    for _rowi in select key,value from jsonb_each(_jobj) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',2+_depth)||'exit';
END
$_$;




--
-- Name: swcnf.formatter_device_profile(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_profile_name(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'name %','swcnf.formatter_quotedstring',
   'cos %','swcnf.formatter_unquotedstring',
   'egress-bandwidth %','swcnf.formatter_unquotedstring',
   'ingress-bandwidth %','_switch_cfg_formatter_unquoted_string',
   'poe-max-power %','_switch_cfg_formatter_unquoted_string',
   'poe-priority %','_switch_cfg_formatter_unquoted_string',
   'speed-duplex %','_switch_cfg_formatter_unquoted_string',
   'tagged-vlan %','swcnf.formatter_iplist',
   'untagged-vlan %','swcnf.formatter_iplist'
  );
BEGIN
    return next swcnf.formatter_quotedstring('device-profile name %',(jsonb_build_array()||to_jsonb(_key)),_depth);
    -- No particular ordering required here
    for _rowi in select key,value from jsonb_each(_jobj) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',2+_depth)||'exit';
END
$_$;




--
-- Name: swcnf.formatter_device_profile(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_profile_type(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'associate %','swcnf.formatter_quotedstring'
  );
BEGIN
    return next swcnf.formatter_unquotedstring('device-profile type %',(jsonb_build_array()||to_jsonb(_key)),_depth);
    -- No particular ordering required here
    for _rowi in select key,value from jsonb_each(_jobj) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',2+_depth)||'exit';
END
$_$;



--
-- Name: swcnf.formatter_device_profiles(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_device_profiles(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    return query select swcnf.formatter_device_profile(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$$;


-- ALTER FUNCTION formatter_device_profiles(_txt text, _arr jsonb, _depth integer) OWNER TO dev;

--
-- Name: swcnf.formatter_interface(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_interface(_txt text, _vlans jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'name %','swcnf.formatter_quotedstring'
  );
BEGIN
    return next repeat(' ',_depth)||'interface '||_txt;
    for _rowi in select key,value from jsonb_each(_vlans) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;


-- ALTER FUNCTION formatter_interface(_txt text, _vlans jsonb, _depth integer) OWNER TO dev;

--
-- Name: swcnf.formatter_interfaces(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_interfaces(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
 _newarr text[];
BEGIN
 return query select swcnf.formatter_interface(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$$;


-- ALTER FUNCTION formatter_interfaces(_txt text, _arr jsonb, _depth integer) OWNER TO dev;

--
-- Name:formatter_ipacl(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_ipacl_ext(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join'
  );
BEGIN
    return next swcnf.formatter_quotedstring('ip access-list extended %',(jsonb_build_array()||to_jsonb(_key)),_depth);
    for _rowi in select key,value from jsonb_each(_jobj) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;

CREATE FUNCTION swcnf.formatter_ipacl_std(_key text, _jobj jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join'
  );
BEGIN
    return next swcnf.formatter_quotedstring('ip access-list standard %',(jsonb_build_array()||to_jsonb(_key)),_depth);
    for _rowi in select key,value from jsonb_each(_jobj) loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;--
-- Name: swcnf.formatter_ipacls(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_ipacls_ext(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    return query select swcnf.formatter_ipacl_ext(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$$;

CREATE FUNCTION swcnf.formatter_ipacls_std(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    return query select swcnf.formatter_ipacl_std(key,value,_depth) from jsonb_each(_arr) where key !='_template';
END
$$;


-- ALTER FUNCTION formatter_ipacls(_txt text, _arr jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_iplist(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_iplist(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    if _arr ='[]'::jsonb then
       return next repeat(' ',_depth)||'no ip address';
    else
       return query select repeat(' ',_depth)||'ip address '||(x#>>'{ip}')||' '||(x#>>'{netmask}') from jsonb_array_elements(_arr) as x(t);
    end if;
END
$$;


-- ALTER FUNCTION formatter_iplist(_txt text, _arr jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_oobm(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_oobm(_txt text, _oobm jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
_rowi record;
formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'ips %','swcnf.formatter_iplist'
);
BEGIN
 -- TODO: raise notice if _template missing from _oobm
 if _oobm->'' != _oobm->'_template' then
   return next repeat(' ',_depth)||'oobm';
   for _rowi in select key,value from jsonb_each(_oobm->'') order by key='%' loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
   end loop;
   return next repeat(' ',_depth+2)||'exit';
 end if;
END
$_$;


-- ALTER FUNCTION formatter_oobm(_txt text, _oobm jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_portlist(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_portlist(_txt text, _arr jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    if _arr !='[]'::jsonb then
       return next repeat(' ',_depth)||replace(_txt,'%',swcnf.procurve_collapse_array(ARRAY(select jsonb_array_elements_text(_arr))));
    end if;
END
$$;


-- ALTER FUNCTION formatter_portlist(_txt text, _arr jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_quotedstring(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_quotedstring(_txt text, _ident jsonb, _depth integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    _t text;
BEGIN
    if jsonb_typeof(_ident)='array' then
      select array_to_string(array_agg(value),',') from jsonb_array_elements_text(_ident) into _t;
    else
      _t=_ident::text;
    end if;
    return repeat(' ',_depth)||replace(_txt,'%','"'||_t||'"');
END

$$;


-- ALTER FUNCTION formatter_quotedstring(_txt text, _ident jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_unquotedstring(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_unquotedstring(_txt text, _ident jsonb, _depth integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    _t text;
BEGIN
    if jsonb_typeof(_ident)='array' then
      select array_to_string(array_agg(value),',') from jsonb_array_elements_text(_ident) into _t;
    else
      _t=_ident::text;
    end if;
    return repeat(' ',_depth)||replace(_txt,'%',_t);
END

$$;


-- ALTER FUNCTION formatter_unquotedstring(_txt text, _ident jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_vlan(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_vlan(_txt text, _vlans jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join',
   'ips %','swcnf.formatter_iplist',
   'untagged %','swcnf.formatter_portlist',
   'no untagged %','swcnf.formatter_portlist',
   'tagged %','swcnf.formatter_portlist',
   'name %','swcnf.formatter_quotedstring',
   'ip igmp blocked %','swcnf.formatter_portlist',
   'vrrp','swcnf.formatter_vrrps'
  );
BEGIN
    return next repeat(' ',_depth)||'vlan '||_txt;
    -- key='vrrp' places this last
    for _rowi in select key,value from jsonb_each(_vlans) order by key='vrrp' loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(jsonb_build_array()||_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;


-- ALTER FUNCTION formatter_vlan(_txt text, _vlans jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_vlans(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_vlans(_txt text, _vlans jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
 return query select swcnf.formatter_vlan(key,value,_depth) from jsonb_each(_vlans) where key !='_template' and key !='%EVERYVLAN%';
END
$$;


-- ALTER FUNCTION formatter_vlans(_txt text, _vlans jsonb, _depth integer) OWNER TO dev;
--
-- Name: swcnf.formatter_vrrp(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_vrrp(_txt text, _vlans jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
 _rowi record;
 _newval jsonb;
 _enable jsonb='"enable"';
 formatters jsonb=jsonb_build_object(
   '%','swcnf.formatter_array_join'
 );
BEGIN
    return next repeat(' ',_depth)||'vrrp vrid '||_txt;
    for _rowi in select key,value from jsonb_each(_vlans) loop
        select jsonb_agg(value order by value=_enable, value) from jsonb_array_elements(jsonb_build_array()||_rowi.value) into _newval;
        return query select repeat(' ',_depth+2)||value from jsonb_array_elements_text(_newval);
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$_$;


-- ALTER FUNCTION formatter_vrrp(_txt text, _vlans jsonb, _depth integer) OWNER TO dev;

--
-- Name: swcnf.formatter_vrrps(text, jsonb, integer); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.formatter_vrrps(_txt text, _vrrps jsonb, _depth integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    return query select swcnf.formatter_vrrp(key,value,_depth) from (select (jsonb_each(a)).key,(jsonb_each(a)).value from (select jsonb_array_elements(jsonb_build_array()||_vrrps)a)a)b where key!='_template';
END
$$;


-- ALTER FUNCTION formatter_vrrps(_txt text, _vrrps jsonb, _depth integer) OWNER TO dev;


--
-- Name: swcnf.procurve_collapse_array(character varying[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION swcnf.procurve_collapse_array(_orig character varying[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
declare
 _after varchar[];
 _prefix varchar;
 _prefices varchar[];
 _pending integer[];
 _i integer;
 _j integer;
 _lfirst integer;
 _llast integer;
  _result varchar;
begin
 -- Empty list? Bale out
 if _orig is null then
  return  null;
 end if;
 -- Handles a list of integers, optionally with a textual prefix
 select array(select distinct regexp_replace(unnest,E'\\d+$','') from unnest(_orig) order by 1) into _prefices;
 -- Get the prefix
 if array_lower(_prefices,1) is not null and array_upper(_prefices,1) is not null then
 FOR _i IN array_lower(_prefices, 1) .. array_upper(_prefices, 1) loop
  _prefix=_prefices[_i];
  -- Get the matching entities
  select array(
   select b from (
    select distinct regexp_replace(unnest,'^'||_prefix,'') b from (
     select * from unnest(_orig)
    ) a where unnest ~ ('^'||_prefix||E'\\d+$') 
   ) c order by b::integer
  ) into _pending;
  -- Loop through, forming a textual X1-X2 representation of the array
  _lfirst=null;
  _llast=null;
  -- Seek out single-item lists, for which the loop won't fire:
 -- _result='LoXwer'||array_lower(_pending,1)||', '||array_upper(_pending,1);
 -- return _result;
-- IF FALSE THEN
  if (array_lower(_pending,1) = array_upper(_pending,1)) then
   _after[coalesce(array_upper(_after,1),0)+1]=_prefix||_pending[array_lower(_pending,1)];
  else
   FOR _i IN array_lower(_pending,1) .. array_upper(_pending,1)-1 loop
    _result=_result||' Pending '||array_to_string(_pending,',');
    if _lfirst is null then _lfirst=_pending[_i] ; _result=_result||'Set lfirst to '||_pending[_i]; end if;
    -- Do we have a break in the sequence?
    if _pending[_i+1]!=_pending[_i]+1 then
     _llast=_pending[_i];
     if (_lfirst=_llast)  then
     _after[coalesce(array_upper(_after,1),0)+1]=_prefix||_lfirst;
     else
       _after[coalesce(array_upper(_after,1),0)+1]=_prefix||_lfirst||'-'||_prefix||_llast;
     end if;
     _lfirst=_pending[_i+1];
     _llast=null;
    end if;
   end loop;
   if _lfirst is not null and _llast is null then
    _llast=_pending[array_upper(_pending,1)];
    if (_lfirst=_llast)  then
     _after[coalesce(array_upper(_after,1),0)+1]=_prefix||_lfirst;
    else
     _after[coalesce(array_upper(_after,1),0)+1]=_prefix||_lfirst||'-'||_prefix||_llast;
    end if;
   end if;
  end if;
--  END IF;
 end loop;
 end if; -- Checking for NULL prefices.
-- return _result;
  return  array_to_string(_after,',');
end
$_$;

--
-- Name: swcnf.procurve_collapse_array(jsonb); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.procurve_collapse_array(_orig jsonb) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
declare
 _new varchar[];
begin
 select array_agg(a) from (select jsonb_array_elements_text(_orig) a)a into _new;
 return swcnf.procurve_collapse_array(_new); 
end
$$;
CREATE OR REPLACE FUNCTION swcnf.vrrp_priority_for_switch_id(
	_swid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
begin
 return case
  when _swid=704 then
    255
  else
    200
  end;
 end;
$BODY$;


--
-- Name: swcnf.switch_config_b(inet); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.switch_config_b(v_switch_ip inet) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
declare
 v_switch_id bigint;
begin
 select switch.id from switch inner join system_image using (hardware_id) inner join mm_system_image_ip_address on system_image_id=system_image.id inner join ip_address on ip_address.id=ip_address_id where ip=v_switch_ip into v_switch_id;
 if v_switch_id is null then
  return next '; No known switch with IP address '||host(v_switch_ip);
 else
--  return next '; ID is '||v_switch_id;
  return query select swcnf.switch_config_b3(v_switch_id);
 end if;
end

$$;


-- ALTER FUNCTION public.swcnf.switch_config_b(v_switch_ip inet) OWNER TO dev;

--
-- Name: swcnf.switch_config_b(bigint); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.switch_config_b(v_switch_id bigint) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
declare
begin
return query select swcnf.switch_config_b3(v_switch_id);
end

$$;


-- ALTER FUNCTION public.swcnf.switch_config_b(v_switch_id bigint) OWNER TO dev;

--
-- Name: swcnf.switch_config_b3(bigint); Type: FUNCTION; Schema: public; Owner: dev
--
CREATE FUNCTION swcnf.switch_config_b3(v_switch_id bigint)
    RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    r record;
    formatters json:= swcnf.def_formatterlist();
    cfg jsonb:= swcnf.empty_cfg();
    stack text;
    fcesig text;
BEGIN
    -- Get our function name
    GET DIAGNOSTICS stack = PG_CONTEXT;
    fcesig := substring(stack from 'function (.*?) line');
    -- Basic stuff about the switch
    SELECT
        switch.id,
        switch_model.model,
        serial_number
    FROM
        switch
        INNER JOIN switch_model ON switch_model.id = switch_model_id
        INNER JOIN hardware ON hardware_id = hardware.id
    WHERE
        switch.id = v_switch_id INTO r;

    RETURN NEXT '; ' || r.model || ' Configuration Editor; Created on release X.XX';
    RETURN NEXT '; Switch config generated by '||(fcesig::regprocedure::text)||' for switch ID ' || r.id;
    RETURN NEXT '; serial number ' || r.serial_number;
    RETURN NEXT '; model number ' || r.model;

    cfg = swcnf.apply_switch_fragments(_cfg => cfg, _switchid => v_switch_id);
    cfg = swcnf.apply_switch_port_fragments(cfg, v_switch_id);
    cfg = swcnf.expand_everyvlan(cfg, _switchid => v_switch_id);
    cfg = swcnf.apply_switch(cfg, _switchid => v_switch_id);
    RETURN query SELECT swcnf.output(cfg, formatters);
END
$_$;

-- ALTER FUNCTION public.swcnf.switch_config_b3(v_switch_id bigint) OWNER TO dev;
--
-- Name: swcnf.switch_config_d(inet); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.switch_config_d(v_switch_ip inet) RETURNS text
    LANGUAGE sql
    AS $_$



select array_to_string(array_agg(a),E'\n') from (

select regexp_replace(c,E'\nloop-protect [^\\s]+\n',E'\n','g') a from (select array_to_string(array_agg(switch_config_b),E'\n')||E'\n' c from 
swcnf.switch_config_b(v_switch_ip))a

union all

select regexp_replace('loop-protect '||array_to_string(ARRAY(select array_to_string(regexp_matches(c,E'\nloop-protect ([^\\s]+)\n','g'),'')e from (select array_to_string(array_agg(switch_config_b),E'\n')||E'\n' c from swcnf.switch_config_b(v_switch_ip))a),',') ,'^loop-protect $','') b) c;


$_$;


-- ALTER FUNCTION switch_config_d(v_switch_ip inet) OWNER TO dev;

--
-- Name: swcnf.switch_config_d(bigint); Type: FUNCTION; Schema: public; Owner: dev
--

CREATE FUNCTION swcnf.switch_config_d(v_switch_id bigint) RETURNS text
    LANGUAGE sql
    AS $_$



select array_to_string(array_agg(a),E'\n') from (

select regexp_replace(c,E'\nloop-protect [^\\s]+\n',E'\n','g') a from (select array_to_string(array_agg(switch_config_b),E'\n')||E'\n' c from 
swcnf.switch_config_b(v_switch_id))a

union all

select regexp_replace('loop-protect '||array_to_string(ARRAY(select array_to_string(regexp_matches(c,E'\nloop-protect ([^\\s]+)\n','g'),'')e from (select array_to_string(array_agg(switch_config_b),E'\n')||E'\n' c from swcnf.switch_config_b(v_switch_id))a),',') ,'^loop-protect $','') b) c;


$_$;


-- ALTER FUNCTION switch_config_d(v_switch_id bigint) OWNER TO dev;
 
CREATE OR REPLACE VIEW swcnf.applied_switch_config_fragments as 
SELECT switch_config_fragment.id,
    switch_config_fragment.config,
    switch_config_fragment.config_json,
    switch_config_fragment.priority,
    mm_switch_goal_applies_to_switch.switch_id,
    mm_switch_config_fragment_switch_model.switch_model_id
   FROM public.switch
     JOIN public.mm_switch_goal_applies_to_switch ON switch.id = mm_switch_goal_applies_to_switch.switch_id
     JOIN public.mm_switch_config_fragment_switch_model USING (switch_model_id)
     JOIN public.switch_config_fragment USING (switch_config_goal_id)
   WHERE switch_config_fragment.id = mm_switch_config_fragment_switch_model.switch_config_fragment_id;
 create view swcnf.applied_switch_cfg_vlan_ips as 
 SELECT d.vid,
    unnest(d.ips) AS ip,
    unnest(d.netmasks) AS netmask,
    d.switch_id
   FROM ( SELECT c.vid,
            COALESCE(c.non_router_ips, c.router_ips) AS ips,
            COALESCE(c.non_router_netmasks, c.router_netmasks) AS netmasks,
            c.switch_id
           FROM ( SELECT b.vid,
                    array_agg(host(a.ip)) FILTER (WHERE a.ip IS DISTINCT FROM b.router) AS non_router_ips,
                    array_agg(host(a.ip)) FILTER (WHERE a.ip = b.router) AS router_ips,
                    array_agg(host(netmask(b.network_address))) FILTER (WHERE a.ip IS DISTINCT FROM b.router) AS non_router_netmasks,
                    array_agg(host(netmask(b.network_address))) FILTER (WHERE a.ip = b.router) AS router_netmasks,
                    a.switch_id
                   FROM ( SELECT ip_address.ip,
                            ip_address.subnet_id,
                            switch.id AS switch_id
                           FROM public.switch
                             JOIN public.system_image USING (hardware_id)
                             JOIN public.mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = system_image.id
                             JOIN public.ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
                             JOIN public.subnet ON ip_address.subnet_id = subnet.id) a
                     RIGHT JOIN ( SELECT vlan.vid,
                            subnet.id AS subnet_id,
                            subnet.network_address,
                            subnet.router
                           FROM public.vlan
                             JOIN public.subnet ON subnet.vlan_id = vlan.id) b USING (subnet_id)
                  GROUP BY b.vid, a.switch_id) c
          ORDER BY c.vid) d
  ORDER BY d.vid;

CREATE VIEW swcnf.applied_switch_port_cfg_json AS
 SELECT c.switch_id,
    jsonb_agg(replace(c.config_json::text, '%PORT%'::text, c.portlist::text)::jsonb) AS config_json,
    array_to_string(array_agg(DISTINCT c.id), ','::text) AS id
   FROM ( SELECT a.portlist,
            jsonb_array_elements(switch_port_config_fragment.config_json) AS config_json,
            switch_port_config_fragment.id,
            a.switch_id
           FROM ( SELECT switchport.name AS portlist,
                    array_agg(mm_switch_config_switch_port.switch_port_config_goal_id) FILTER (WHERE mm_switch_config_switch_port.switch_port_config_goal_id IS NOT NULL) || switchport.switch_auth_id::bigint AS switch_port_config_goal_id,
                    switch.switch_model_id,
                    switchport.switch_id
                   FROM public.switchport
                     JOIN public.switch ON switchport.switch_id = switch.id
                     LEFT JOIN public.mm_switch_config_switch_port ON switchport.id = mm_switch_config_switch_port.switch_port_id
                  GROUP BY switch.switch_model_id, switchport.id) a
             JOIN public.switch_port_config_fragment ON switch_port_config_fragment.switch_port_config_goal_id = ANY (a.switch_port_config_goal_id)
             JOIN public.mm_switch_port_config_fragment_switch_model ON mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id
          WHERE mm_switch_port_config_fragment_switch_model.switch_model_id = a.switch_model_id AND switch_port_config_fragment.config_json IS NOT NULL) c
  GROUP BY c.switch_id;

 
CREATE OR REPLACE VIEW swcnf.applied_vlan_ips_json AS
 SELECT applied_switch_cfg_vlan_ips.switch_id,
    jsonb_agg(jsonb_build_object('path', jsonb_build_array(jsonb_build_object('vlan',applied_switch_cfg_vlan_ips.vid::text),'ips %'),
                                 'sense','add',
                                 'trace','Here123',
                                 'newval',jsonb_build_object('ip', swcnf.applied_switch_cfg_vlan_ips.ip, 
                                                            'netmask', applied_switch_cfg_vlan_ips.netmask))) AS config_json
   FROM swcnf.applied_switch_cfg_vlan_ips
  GROUP BY applied_switch_cfg_vlan_ips.switch_id;
-- View: hotwire3.10_View/Network/Switches/_switch_config_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_switch_config_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switch_config_sv" AS
SELECT
    switch.id,
    'Config for switch - click here to show'::character varying AS "Link to config"
FROM
    switch;

ALTER TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/_switches_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_switches_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switches_sv" AS
SELECT
  switch.id,
  switchport.id AS switch_port_id,
  hardware.name AS switch_name,
  hardware.manufacturer,
  switch_model_hid.switch_model_hid,
  room_hid.room_hid
FROM
  switchport
  LEFT JOIN switch ON switchport.switch_id = switch.id
  JOIN hardware ON switch.hardware_id = hardware.id
  LEFT JOIN hotwire3.room_hid USING (room_id)
  LEFT JOIN hotwire3.switch_model_hid USING (switch_model_id);

ALTER TABLE hotwire3."10_View/Network/Switches/_switches_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switches_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switches_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/_Switchport_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_Switchport_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_Switchport_sv" AS
SELECT
    switchport.id,
    switchport.switch_id,
    switchport.name,
    socket_hid.socket_hid AS socket,
    switchport.ifacename,
    switch_auth_hid.switch_auth_hid,
    speed_hid.speed_hid
FROM
    switchport
    LEFT JOIN hotwire3.switch_auth_hid USING (switch_auth_id)
    LEFT JOIN hotwire3.speed_hid USING (speed_id)
    LEFT JOIN hotwire3.socket_hid USING (socket_id)
ORDER BY
    (
        CASE WHEN switchport.name::text ~ '^[A-Za-z]+'::text THEN
            10 * ascii(substr(switchport.name::text, 1, 1)) + regexp_replace(switchport.name::text, '[A-Za-z]+'::text, ''::text)::integer
        ELSE
            switchport.name::integer
        END);

ALTER TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/Switches
-- DROP VIEW hotwire3."10_View/Network/Switches/05_Switches";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/05_Switches" AS
SELECT
  switch.id,
  hardware.name,
  hardware.id AS _hardware_id,
  system_image.id AS _system_image_id,
  hardware.manufacturer,
  hardware.hardware_type_id,
  system_image.wired_mac_1,
  switch.switch_model_id,
  switch.ignore_config,
  ARRAY (
    SELECT
      mm_system_image_ip_address.ip_address_id
    FROM
      mm_system_image_ip_address
    WHERE
      mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
  hardware.serial_number,
  hardware.asset_tag,
  hardware.date_purchased,
  hardware.date_configured,
  hardware.warranty_end_date,
  hardware.date_decommissioned,
  hardware.warranty_details,
  hardware.room_id,
  switch.cabinet_id,
  hardware.owner_id,
  system_image.research_group_id,
  hardware.comments,
  ARRAY (
    SELECT
      mm_switch_goal_applies_to_switch.switch_config_goal_id
    FROM
      mm_switch_goal_applies_to_switch
    WHERE
      mm_switch_goal_applies_to_switch.switch_id = switch.id) AS switch_config_goal_id,
  switch.config_json,
  hotwire3.to_hwsubviewb('10_View/Network/_Cabinet_for_switch'::character varying, 'switch_id'::character varying, '10_View/Network/Cabinets'::character varying, 'cabinet_id'::character varying, 'id'::character varying) AS "Cabinet_details",
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_Switchport_sv'::character varying, 'switch_id'::character varying, '10_View/Network/Switches/20_Switch_Ports'::character varying, NULL::character varying, NULL::character varying) AS "Ports",
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_switch_config_sv'::character varying, 'id'::character varying, '10_View/Network/Switches/20_Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config"
FROM
  switch
  JOIN hardware ON switch.hardware_id = hardware.id
  JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Network/Switches/05_Switches" OWNER TO dev;

GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/05_Switches" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/05_Switches" TO dev;

-- Rule: hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/05_Switches"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/05_Switches";
CREATE RULE hotwire3_view_network_switch_del AS ON DELETE TO hotwire3."10_View/Network/Switches/05_Switches"
  DO INSTEAD
  ( DELETE FROM switch
    WHERE (switch.id = OLD.id));

-- Rule: hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/05_Switches"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/05_Switches";
CREATE RULE hotwire3_view_network_switch_upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/05_Switches"
    DO INSTEAD
    ( UPDATE
        hardware SET
        name = NEW.name,
        manufacturer = NEW.manufacturer,
        hardware_type_id = NEW.hardware_type_id,
        serial_number = NEW.serial_number,
        asset_tag = NEW.asset_tag,
        date_purchased = NEW.date_purchased,
        date_configured = NEW.date_configured,
        warranty_end_date = NEW.warranty_end_date,
        date_decommissioned = NEW.date_decommissioned,
        warranty_details = NEW.warranty_details,
        room_id = NEW.room_id,
        owner_id = NEW.owner_id,
        comments = NEW.comments WHERE (hardware.id = OLD._hardware_id);

UPDATE
  system_image
SET
  research_group_id = new.research_group_id,
  wired_mac_1 = new.wired_mac_1
WHERE (system_image.id = old._system_image_id);

SELECT
  fn_mm_array_update((new.switch_config_goal_id)::bigint[],(old.switch_config_goal_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;

SELECT
  fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update;

UPDATE
  switch
SET
  ignore_config = new.ignore_config,
  switch_model_id = new.switch_model_id,
  config_json = new.config_json,
  cabinet_id = new.cabinet_id
WHERE (switch.id = old.id);

);

-- View: hotwire3.10_View/Network/Switches/Switch_Configs_ro
-- DROP VIEW hotwire3."10_View/Network/Switches/20_Switch_Configs_ro";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" AS
SELECT
    switch.id,
    switch.id AS switch_id,
    array_to_string(ARRAY (
            SELECT
                swcnf.switch_config_d(switch.id) AS _switch_config), '
'::text) AS ro_config
FROM
    switch;

ALTER TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" TO dev;

-- View: hotwire3.10_View/Network/Switches/20_Switch_Ports
-- DROP VIEW hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/20_Switch_Ports" AS
SELECT
  switchport.id,
  switchport.name,
  switchport.socket_id,
  switchport.switch_id,
  switchport.ifacename,
  switchport.disabled,
  switchport.speed_id,
  switchport.switch_auth_id,
  ARRAY (
    SELECT
      mm_switch_config_switch_port.switch_port_config_goal_id
    FROM
      mm_switch_config_switch_port
    WHERE
      mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id,
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_switches_sv'::character varying, 'switch_port_id'::character varying, '10_View/Network/Switches/05_Switches'::character varying, NULL::character varying, NULL::character varying) AS switch
FROM
  switchport;

ALTER TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" TO dev;

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" AS ON DELETE TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
  DO INSTEAD
  ( DELETE FROM switchport
    WHERE (switchport.id = OLD.id));

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" AS ON INSERT TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
  DO INSTEAD
  (INSERT INTO switchport(name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id)
    VALUES (NEW.name, NEW.socket_id, NEW.switch_id, NEW.ifacename, NEW.disabled, NEW.speed_id, NEW.switch_auth_id)
  RETURNING
    switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY (
      SELECT
        mm_switch_config_switch_port.switch_port_config_goal_id FROM
        mm_switch_config_switch_port WHERE (mm_switch_config_switch_port.switch_port_id = switchport.id)) AS switch_port_goal_id,
    NULL::_hwsubviewb AS _hwsubviewb);

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
    DO INSTEAD
    ( UPDATE
        switchport SET
        id = NEW.id,
        name = NEW.name,
        socket_id = NEW.socket_id,
        switch_id = NEW.switch_id,
        ifacename = NEW.ifacename,
        disabled = NEW.disabled,
        speed_id = NEW.speed_id,
        switch_auth_id = NEW.switch_auth_id WHERE (switchport.id = OLD.id);

SELECT
  fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;

);

-- View: hotwire3.10_View/Network/Switches/Switch_Config_Goals
-- DROP VIEW hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" AS
SELECT
    switch_config_goal.id,
    switch_config_goal.name,
    ARRAY (
        SELECT
            mm_switch_goal_applies_to_switch.switch_id
        FROM
            mm_switch_goal_applies_to_switch
        WHERE
            mm_switch_goal_applies_to_switch.switch_config_goal_id = switch_config_goal.id) AS switch_id
FROM
    switch_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" TO dev;

-- Rule: hotwire3_view_network_switch_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE RULE hotwire3_view_network_switch_config_goal_del AS ON DELETE TO hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
    DO INSTEAD
    ( DELETE FROM switch_config_goal
        WHERE (switch_config_goal.id = OLD.id));

-- Rule: hotwire3_view_network_switch_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE RULE hotwire3_view_network_switch_config_goal_upd AS ON UPDATE
    TO hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
        DO INSTEAD
        ( UPDATE
                switch_config_goal SET
                name = NEW.name WHERE (switch_config_goal.id = OLD.id);

SELECT
    fn_mm_array_update((new.switch_id)::bigint[],(old.switch_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_config_goal_id'::character varying, 'switch_id'::character varying,(old.id)::bigint) AS fn_mm_array_update2;

);

CREATE TRIGGER hotwire3_view_network_switch_config_goal_ins
    INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.switch_config_goals_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Port_Config_Goals
-- DROP VIEW hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" AS
SELECT
  switch_port_config_goal.id,
  switch_port_config_goal.name,
  switch_port_config_goal.goal_class AS goal_class_id
FROM
  switch_port_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" TO dev;

-- Rule: hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_del AS ON DELETE TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
  DO INSTEAD
  ( DELETE FROM switch_port_config_goal
    WHERE (switch_port_config_goal.id = OLD.id));

-- Rule: hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_ins AS ON INSERT TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
  DO INSTEAD
  (INSERT INTO switch_port_config_goal(name, goal_class)
    VALUES (NEW.name, NEW.goal_class_id)
  RETURNING
    switch_port_config_goal.id, switch_port_config_goal.name, switch_port_config_goal.goal_class);

-- Rule: hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
    DO INSTEAD
    ( UPDATE
        switch_port_config_goal SET
        name = NEW.name,
        goal_class = NEW.goal_class_id WHERE (switch_port_config_goal.id = OLD.id));

-- View: hotwire3.10_View/Network/Switches/Switch_Config_Fragments
-- DROP VIEW hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" AS
SELECT
  switch_config_fragment.id,
  switch_config_fragment.name,
  switch_config_fragment.config_json,
  switch_config_fragment.switch_config_goal_id,
  ARRAY (
    SELECT
      mm_switch_config_fragment_switch_model.switch_model_id
    FROM
      mm_switch_config_fragment_switch_model
    WHERE
      mm_switch_config_fragment_switch_model.switch_config_fragment_id = switch_config_fragment.id) AS switch_model_id
FROM
  switch_config_fragment
ORDER BY
  switch_config_fragment.name;

ALTER TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" TO dev;

-- Rule: del ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
-- DROP Rule IF EXISTS del ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE RULE del AS ON DELETE TO hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
  DO INSTEAD
  ( DELETE FROM switch_config_fragment
    WHERE (switch_config_fragment.id = OLD.id));

-- Rule: upd ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
-- DROP Rule IF EXISTS upd ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE RULE upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
    DO INSTEAD
    ( UPDATE
        switch_config_fragment SET
        name = NEW.name,
        config_json = NEW.config_json,
        switch_config_goal_id = NEW.switch_config_goal_id WHERE (switch_config_fragment.id = OLD.id);

SELECT
  fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id) AS fn_mm_array_update;

UPDATE
  switch_config_fragment
SET
  id = switch_config_fragment.id
WHERE
  FALSE
RETURNING
  new.id,
  new.name,
  new.config_json,
  new.switch_config_goal_id,
  new.switch_model_id;

);

CREATE TRIGGER switch_config_fragments_ins
  INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
  FOR EACH ROW
  EXECUTE FUNCTION hotwire3.switch_config_fragments_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Port_Config_Fragments
-- DROP VIEW hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" AS
SELECT
    switch_port_config_fragment.id,
    switch_port_config_fragment.switch_port_config_goal_id,
    switch_port_config_fragment.config_json,
    ARRAY (
        SELECT
            mm_switch_port_config_fragment_switch_model.switch_model_id
        FROM
            mm_switch_port_config_fragment_switch_model
        WHERE
            mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id) AS switch_model_id
FROM
    switch_port_config_fragment;

ALTER TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" TO dev;

-- Rule: hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE RULE hotwire3_view_network_switch_port_config_fragment_del AS ON DELETE TO hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
    DO INSTEAD
    ( DELETE FROM switch_port_config_fragment
        WHERE (switch_port_config_fragment.id = OLD.id));

-- Rule: hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE RULE hotwire3_view_network_switch_port_config_fragment_upd AS ON UPDATE
    TO hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
        DO INSTEAD
        ( UPDATE
                switch_port_config_fragment SET
                switch_port_config_goal_id = NEW.switch_port_config_goal_id,
                config_json = NEW.config_json WHERE (switch_port_config_fragment.id = OLD.id);

SELECT
    fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying,(old.id)::bigint) AS fn_mm_array_update2;

);

CREATE TRIGGER hotwire3_view_network_switch_port_config_fragment_ins
    INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.network_switch_port_config_fragment_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Models
-- DROP VIEW hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/80_Switch_Models" AS
SELECT
  switch_model.id,
  switch_model.model,
  switch_model.description,
  switch_model.ports,
  switch_model.uplink_ports
FROM
  switch_model;

ALTER TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" TO dev;

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_del" AS ON DELETE TO hotwire3."10_View/Network/Switches/80_Switch_Models"
  DO INSTEAD
  ( DELETE FROM switch_model
    WHERE (switch_model.id = OLD.id));

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_ins" AS ON INSERT TO hotwire3."10_View/Network/Switches/80_Switch_Models"
  DO INSTEAD
  (INSERT INTO switch_model(model, description, ports, uplink_ports)
    VALUES (NEW.model, NEW.description, NEW.ports, NEW.uplink_ports)
  RETURNING
    switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports);

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_upd" AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/80_Switch_Models"
    DO INSTEAD
    ( UPDATE
        switch_model SET
        id = NEW.id,
        model = NEW.model,
        description = NEW.description,
        ports = NEW.ports,
        uplink_ports = NEW.uplink_ports WHERE (switch_model.id = OLD.id));

