--
-- Name: academic_staff_crsids; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.academic_staff_crsids AS
 SELECT person.crsid
   FROM ((public.person
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._current_hr_role_for_person lr ON ((person.id = lr.person_id)))
  WHERE (((ps.status_id)::text = 'Current'::text) AND ((lr.post_category_id = 'sc-1'::text) OR (lr.post_category_id = 'sc-9'::text) OR (person.counts_as_academic = true)) AND (person.crsid IS NOT NULL));


--
-- Name: account_extension_details; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.account_extension_details AS
 SELECT person.crsid AS ext_crsid,
    person.email_address AS ext_email,
    ph.person_hid AS ext_name,
    s.crsid AS pi_crsid,
    s.email_address AS pi_email,
    pi_h.person_hid AS pi_name,
    aes."timestamp",
    aes.reason,
    aes.ext_date,
    us.crsid AS user_crsid
   FROM ((((((public.person
     LEFT JOIN public._current_hr_role_for_person lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.person s ON ((lr.supervisor_id = s.id)))
     LEFT JOIN public.person_hid ph ON ((person.id = ph.person_id)))
     LEFT JOIN public.person_hid pi_h ON ((s.id = pi_h.person_id)))
     LEFT JOIN public.account_extension_log aes ON ((person.id = aes.ext_id)))
     LEFT JOIN public.person us ON ((aes.user_id = us.id)))
  WHERE ((person.crsid IS NOT NULL) AND (s.email_address IS NOT NULL));


--
-- Name: account_extension_pi_delegate; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.account_extension_pi_delegate AS
 SELECT pi.crsid AS pi_crsid,
    usr.crsid AS delegate_crsid,
    ph.person_hid AS delegate_name
   FROM ((((public.account_extension_delegates
     RIGHT JOIN public.person pi ON ((account_extension_delegates.pi_id = pi.id)))
     LEFT JOIN public.person usr ON ((account_extension_delegates.delegate_id = usr.id)))
     LEFT JOIN public._current_hr_role_for_person lr ON ((pi.id = lr.person_id)))
     LEFT JOIN public.person_hid ph ON ((account_extension_delegates.delegate_id = ph.person_id)))
  WHERE (((lr.predicted_role_status_id)::text = 'Current'::text) AND ((lr.post_category_id = 'sc-1'::text) OR (lr.post_category_id = 'sc-9'::text) OR (pi.counts_as_academic = true)) AND (pi.crsid IS NOT NULL));


--
-- Name: bouncing_admitto_accounts; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.bouncing_admitto_accounts AS
 SELECT pt.long_title AS ptitle,
    p.surname AS psurname,
    COALESCE(p.known_as, p.first_names) AS pfirst,
    p.email_address AS pemail,
    p.crsid,
    ps.status_id AS pstatus,
        CASE
            WHEN (eligible_for_account.crsid IS NOT NULL) THEN true
            ELSE false
        END AS automatically_eligible_for_account,
    ts.salutation_title AS stitle,
    s.email_address AS semail,
    s.surname AS ssurname
   FROM ((((((public.person p
     LEFT JOIN LATERAL ( SELECT user_accounts_to_create_in_ad_v2.id,
            user_accounts_to_create_in_ad_v2.crsid,
            user_accounts_to_create_in_ad_v2.first_names,
            user_accounts_to_create_in_ad_v2.surname,
            user_accounts_to_create_in_ad_v2.email_address,
            user_accounts_to_create_in_ad_v2.rg
           FROM apps.user_accounts_to_create_in_ad_v2
          WHERE ((p.crsid)::text = (user_accounts_to_create_in_ad_v2.crsid)::text)) eligible_for_account ON (true))
     LEFT JOIN public.title_hid pt ON ((p.title_id = pt.title_id)))
     JOIN public._current_hr_role_for_person r ON ((r.person_id = p.id)))
     JOIN public._physical_status_v3 ps ON ((p.id = ps.person_id)))
     JOIN public.person s ON ((r.supervisor_id = s.id)))
     LEFT JOIN public.title_hid ts ON ((ts.title_id = s.title_id)));


--
-- Name: crsid_and_role; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.crsid_and_role AS
 SELECT person.crsid,
    _latest_role.post_category,
    (((COALESCE(person.known_as, person.first_names, ''::character varying))::text || ' '::text) || (person.surname)::text) AS name
   FROM (public.person
     LEFT JOIN public._current_hr_role_for_person _latest_role ON ((person.id = _latest_role.person_id)))
  WHERE ((person.crsid IS NOT NULL) AND (_latest_role.post_category IS NOT NULL));


--
-- Name: firstaiders_to_pay; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.firstaiders_to_pay AS
 SELECT person.surname,
    person.first_names,
    person.email_address,
    lr.post_category,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider_funding.funding
   FROM ((((public.firstaider
     JOIN public.person ON ((firstaider.person_id = person.id)))
     LEFT JOIN public.firstaider_funding ON ((firstaider_funding.id = firstaider.firstaider_funding_id)))
     LEFT JOIN public._current_hr_role_for_person lr ON ((firstaider.person_id = lr.person_id)))
     JOIN public._physical_status_v3 ps ON ((ps.person_id = firstaider.person_id)))
  WHERE (((ps.status_id)::text = 'Current'::text) AND (firstaider.requalify_date >= ('now'::text)::date))
  ORDER BY person.surname;


--
-- Name: leavers_pi_details; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.leavers_pi_details AS
 SELECT person.crsid AS leaver_crsid,
    person_hid.person_hid AS leaver_name,
    s.id AS pi_id,
    s.crsid AS pi_crsid,
    s.email_address AS pi_email,
    supervisor_hid.supervisor_hid AS pi_name,
    it_leaving_form.pi_date,
    last_signer_hid.person_hid AS last_signature
   FROM (((((((public.person
     LEFT JOIN public._current_hr_role_for_person lr ON ((person.id = lr.person_id)))
     JOIN public.person s ON ((lr.supervisor_id = s.id)))
     LEFT JOIN public.supervisor_hid ON ((s.id = supervisor_hid.supervisor_id)))
     JOIN public.person_hid ON ((person.id = person_hid.person_id)))
     LEFT JOIN public.it_leaving_form ON (((it_leaving_form.crsid)::text = (person.crsid)::text)))
     LEFT JOIN public.person_hid last_signer_hid ON ((it_leaving_form.pi_id = last_signer_hid.person_id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
  WHERE ((person.crsid IS NOT NULL) AND (s.email_address IS NOT NULL) AND ((ps.status_id)::text <> 'Past'::text));


--
-- Name: people_crsid_and_supervisor_emails; Type: VIEW; Schema: apps; Owner: cen1001
--

DROP VIEW apps.people_crsid_and_supervisor_emails;
CREATE VIEW apps.people_crsid_and_supervisor_emails AS
 SELECT person.crsid,
    s.email_address
   FROM ((public.person
     LEFT JOIN public._current_hr_role_for_person lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.person s ON ((lr.supervisor_id = s.id)))
  WHERE ((person.crsid IS NOT NULL) AND (s.email_address IS NOT NULL));


ALTER VIEW apps.people_crsid_and_supervisor_emails OWNER TO dev;

--
-- Name: TABLE people_crsid_and_supervisor_emails; Type: ACL; Schema: apps; Owner: cen1001
--

GRANT SELECT ON TABLE apps.people_crsid_and_supervisor_emails TO ad_accounts;
GRANT SELECT ON TABLE apps.people_crsid_and_supervisor_emails TO leavers_trigger;


--
-- Name: remote_user_account_metrics; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.remote_user_account_metrics AS
 SELECT person.crsid,
    name.person_hid AS name,
    person.arrival_date,
    person.leaving_date,
    person.left_but_no_leaving_date_given,
    sup.person_hid AS supervisor,
    lr.post_category,
    ps.status_id,
    person.is_spri
   FROM ((((public.person
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._current_hr_role_for_person lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.person_hid name ON ((person.id = name.person_id)))
     LEFT JOIN public.person_hid sup ON ((lr.supervisor_id = sup.person_id)))
  WHERE (person.crsid IS NOT NULL);


--
-- Name: researcher_staff_review_reminders; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.researcher_staff_review_reminders AS
 SELECT person.id,
    person.surname,
    person.first_names,
    person_title.abbrev_title,
    (((COALESCE(((person_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(person.known_as, person.first_names))::text) || ' '::text) || (person.surname)::text) AS reviewee,
    person.email_address AS reviewee_email_address,
    _latest_role.post_category_id,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN (((COALESCE(reviewer_title.salutation_title, COALESCE(usual_reviewer.known_as, usual_reviewer.first_names)))::text || ' '::text) || (usual_reviewer.surname)::text)
            ELSE (((COALESCE(supervisor_title.salutation_title, COALESCE(supervisor.known_as, supervisor.first_names)))::text || ' '::text) || (supervisor.surname)::text)
        END AS reviewer,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN usual_reviewer.email_address
            ELSE supervisor.email_address
        END AS reviewer_email_address,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN (((COALESCE(((reviewer_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(usual_reviewer.known_as, usual_reviewer.first_names))::text) || ' '::text) || (usual_reviewer.surname)::text)
            ELSE (((COALESCE(((supervisor_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(supervisor.known_as, supervisor.first_names))::text) || ' '::text) || (supervisor.surname)::text)
        END AS reviewer_full_name,
    string_agg((administrator.email_address)::text, ','::text) AS additional_contact_emails,
    person.next_review_due,
    last_review.date_of_meeting AS date_of_last_review,
    person.silence_staff_review_reminders,
    COALESCE(usual_reviewer.silence_staff_review_reminders, supervisor.silence_staff_review_reminders) AS silence_staff_review_reminders_for_reviewer
   FROM (((((((((((public.person
     JOIN public._current_hr_role_for_person _latest_role ON ((_latest_role.person_id = person.id)))
     JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = person.id)))
     JOIN public.person supervisor ON ((_latest_role.supervisor_id = supervisor.id)))
     LEFT JOIN public.person usual_reviewer ON ((person.usual_reviewer_id = usual_reviewer.id)))
     LEFT JOIN ( SELECT staff_review_meeting.person_id,
            max(staff_review_meeting.date_of_meeting) AS date_of_meeting
           FROM public.staff_review_meeting
          GROUP BY staff_review_meeting.person_id) last_review ON ((last_review.person_id = person.id)))
     LEFT JOIN public.title_hid person_title ON ((person.title_id = person_title.title_id)))
     LEFT JOIN public.title_hid reviewer_title ON ((usual_reviewer.title_id = reviewer_title.title_id)))
     LEFT JOIN public.title_hid supervisor_title ON ((supervisor.title_id = supervisor_title.title_id)))
     LEFT JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id)))
     LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id)))
     LEFT JOIN public.person administrator ON ((research_group.administrator_id = administrator.id)))
  WHERE (((_physical_status.status_id)::text = 'Current'::text) AND ((_latest_role.post_category_id = 'sc-7'::text) OR (_latest_role.post_category_id = 'sc-4'::text) OR (_latest_role.post_category_id = 'sc-10'::text) OR (_latest_role.post_category_id = 'sc-6'::text) OR (_latest_role.post_category_id = 'sc-5'::text)) AND (_latest_role.paid_by_university <> false))
  GROUP BY person.id, person.surname, person.first_names, person_title.abbrev_title, (((COALESCE(((person_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(person.known_as, person.first_names))::text) || ' '::text) || (person.surname)::text), person.email_address, _latest_role.post_category_id,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN (((COALESCE(reviewer_title.salutation_title, COALESCE(usual_reviewer.known_as, usual_reviewer.first_names)))::text || ' '::text) || (usual_reviewer.surname)::text)
            ELSE (((COALESCE(supervisor_title.salutation_title, COALESCE(supervisor.known_as, supervisor.first_names)))::text || ' '::text) || (supervisor.surname)::text)
        END,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN usual_reviewer.email_address
            ELSE supervisor.email_address
        END,
        CASE
            WHEN (person.usual_reviewer_id IS NOT NULL) THEN (((COALESCE(((reviewer_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(usual_reviewer.known_as, usual_reviewer.first_names))::text) || ' '::text) || (usual_reviewer.surname)::text)
            ELSE (((COALESCE(((supervisor_title.salutation_title)::text || ' '::text), ''::text) || (COALESCE(supervisor.known_as, supervisor.first_names))::text) || ' '::text) || (supervisor.surname)::text)
        END, person.next_review_due, last_review.date_of_meeting, person.silence_staff_review_reminders, COALESCE(usual_reviewer.silence_staff_review_reminders, supervisor.silence_staff_review_reminders);


--
-- Name: user_accounts_to_create_in_ad; Type: VIEW; Schema: apps; Owner: dev
--

DROP VIEW apps.user_accounts_to_create_in_ad;

--
-- Name: user_accounts_to_disable; Type: VIEW; Schema: apps; Owner: dev
--

DROP VIEW apps.user_accounts_to_disable;


--
-- Name: www_academic_staff_v2; Type: VIEW; Schema: apps; Owner: dev
--

DROP VIEW apps.www_academic_staff_v2;


--
-- Name: www_academic_staff_v3; Type: VIEW; Schema: apps; Owner: dev
--

DROP VIEW apps.www_academic_staff_v3;


--
-- Name: staff_reviews; Type: VIEW; Schema: apps; Owner: dev
--

CREATE OR REPLACE VIEW apps.staff_reviews AS
 SELECT DISTINCT lr.person_id,
    person.surname,
    person.first_names,
    person.email_address,
    lr.start_date,
    lr.estimated_role_end_date AS estimated_leaving_date,
    newest_funding.funding_end_date,
    lr.supervisor_id,
    supervisor.email_address AS supervisor_email,
    lr.post_category,
    person.next_review_due,
    person.usual_reviewer_id,
    usual_reviewer.email_address AS usual_reviewer_email,
    last_staff_review_meeting.date_of_meeting AS last_meeting,
    staff_review_meeting.reviewer_id,
    last_reviewer.email_address AS last_reviewer_email
   FROM ((((((((public._current_hr_role_for_person lr
     LEFT JOIN ( SELECT max(staff_review_meeting_1.date_of_meeting) AS date_of_meeting,
            staff_review_meeting_1.person_id
           FROM public.staff_review_meeting staff_review_meeting_1
          WHERE (staff_review_meeting_1.date_of_meeting IS NOT NULL)
          GROUP BY staff_review_meeting_1.person_id) last_staff_review_meeting ON ((last_staff_review_meeting.person_id = lr.person_id)))
     LEFT JOIN public.staff_review_meeting ON (((last_staff_review_meeting.date_of_meeting = staff_review_meeting.date_of_meeting) AND (last_staff_review_meeting.person_id = staff_review_meeting.person_id))))
     JOIN public._physical_status_v3 ON ((_physical_status_v3.person_id = lr.person_id)))
     JOIN public.person ON ((lr.person_id = person.id)))
     JOIN ( SELECT ph.funding_end_date,
            ph.person_id
           FROM (( SELECT max(p1.start_date) AS max_start,
                    p1.person_id
                   FROM public.post_history p1
                  WHERE (p1.start_date IS NOT NULL)
                  GROUP BY p1.person_id) max_start
             JOIN public.post_history ph ON (((max_start.max_start = ph.start_date) AND (max_start.person_id = ph.person_id))))
          ORDER BY ph.person_id) newest_funding ON ((newest_funding.person_id = lr.person_id)))
     LEFT JOIN public.person supervisor ON ((lr.supervisor_id = supervisor.id)))
     LEFT JOIN public.person usual_reviewer ON ((person.usual_reviewer_id = usual_reviewer.id)))
     LEFT JOIN public.person last_reviewer ON ((staff_review_meeting.reviewer_id = last_reviewer.id)))
  WHERE ((lr.role_tablename = 'post_history'::text) AND ((_physical_status_v3.status_id)::text = 'Current'::text))
  ORDER BY person.surname;
