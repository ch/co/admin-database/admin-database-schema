CREATE VIEW public.www_person_info_v1
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v1.www_person_hid,
    research_group.name AS research_group,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number
   FROM person
     LEFT JOIN www_person_hid_v1 ON www_person_hid_v1.person_id = person.id
     LEFT JOIN research_group ON research_group.head_of_group_id = person.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id;

ALTER TABLE public.www_person_info_v1
    OWNER TO dev;

GRANT ALL ON TABLE public.www_person_info_v1 TO dev;
GRANT SELECT ON TABLE public.www_person_info_v1 TO www_sites;

CREATE VIEW public.www_person_info_v2
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v1.www_person_hid,
    research_group.name AS research_group,
    research_group.website_address,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number
   FROM person
     LEFT JOIN www_person_hid_v1 ON www_person_hid_v1.person_id = person.id
     LEFT JOIN research_group ON research_group.head_of_group_id = person.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id;

ALTER TABLE public.www_person_info_v2
    OWNER TO dev;

GRANT ALL ON TABLE public.www_person_info_v2 TO cen1001;
GRANT SELECT ON TABLE public.www_person_info_v2 TO cos;
GRANT ALL ON TABLE public.www_person_info_v2 TO dev;
GRANT SELECT ON TABLE public.www_person_info_v2 TO www_sites;

CREATE VIEW public.www_person_info_v3
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    research_group.name AS research_group,
    research_group.website_address,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN research_group ON research_group.head_of_group_id = person.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  WHERE research_group.internal_use_only IS FALSE OR research_group.internal_use_only IS NULL;

ALTER TABLE public.www_person_info_v3
    OWNER TO dev;

GRANT ALL ON TABLE public.www_person_info_v3 TO cen1001;
GRANT SELECT ON TABLE public.www_person_info_v3 TO cos;
GRANT ALL ON TABLE public.www_person_info_v3 TO dev;
GRANT SELECT ON TABLE public.www_person_info_v3 TO www_sites;

CREATE VIEW public.www_person_info_v4
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.name
        END AS research_group,
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.website_address
        END AS website_address,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN research_group ON research_group.head_of_group_id = person.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  ORDER BY person.crsid, (
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.name
        END);

ALTER TABLE public.www_person_info_v4
    OWNER TO dev;

GRANT ALL ON TABLE public.www_person_info_v4 TO cen1001;
GRANT SELECT ON TABLE public.www_person_info_v4 TO cos;
GRANT ALL ON TABLE public.www_person_info_v4 TO dev;
GRANT SELECT ON TABLE public.www_person_info_v4 TO www_sites;

CREATE VIEW public.www_person_info_v5
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.name
        END AS research_group,
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.website_address
        END AS website_address,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  ORDER BY person.crsid, (
        CASE
            WHEN research_group.internal_use_only IS TRUE THEN NULL::character varying
            WHEN research_group.internal_use_only IS NULL THEN NULL::character varying
            ELSE research_group.name
        END);

ALTER TABLE public.www_person_info_v5
    OWNER TO dev;

GRANT ALL ON TABLE public.www_person_info_v5 TO cen1001;
GRANT SELECT ON TABLE public.www_person_info_v5 TO cos;
GRANT ALL ON TABLE public.www_person_info_v5 TO dev;
GRANT SELECT ON TABLE public.www_person_info_v5 TO www_sites;


