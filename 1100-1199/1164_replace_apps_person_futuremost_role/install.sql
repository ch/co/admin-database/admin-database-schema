CREATE OR REPLACE VIEW apps.person_futuremost_role_new
 AS
 SELECT DISTINCT ON (r1.person_id) 
    r1.person_id,
    r1.start_date,
    r1.intended_end_date,
    r1.estimated_role_end_date AS estimated_leaving_date,
    r1.funding_end_date,
    r1.end_date,
    r1.supervisor_id,
    r1.co_supervisor_id,
    r1.mentor_id,
    r1.post_category_id,
    r1.post_category,
    r1.predicted_role_status_id AS status,
    r1.funding,
    r1.fees_funding,
    r1.research_grant_number,
    r1.paid_by_university,
    -- this is not used, but I'd have to redefine all the views that rely on this view to remove it
    NULL::varchar(2) AS hesa_leaving_code,
    -- this is not used, but I'd have to redefine all the views that rely on this view to remove it
    NULL::boolean AS chem,
    r1.role_id,
    r1.role_tablename,
    -- this is not used, but I'd have to redefine all the views that rely on this view to remove it
    NULL::text AS role_target_viewname
   FROM _hr_role r1
   ORDER BY person_id, start_date DESC, estimated_role_end_date DESC;
