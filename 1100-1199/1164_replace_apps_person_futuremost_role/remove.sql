CREATE OR REPLACE VIEW apps.person_futuremost_role
 AS
 SELECT r1.person_id,
    r1.start_date,
    r1.intended_end_date,
    r1.estimated_leaving_date,
    r1.funding_end_date,
    r1.end_date,
    r1.supervisor_id,
    r1.co_supervisor_id,
    r1.mentor_id,
    r1.post_category_id,
    r1.post_category,
    r1.status,
    r1.funding,
    r1.fees_funding,
    r1.research_grant_number,
    r1.paid_by_university,
    r1.hesa_leaving_code,
    r1.chem,
    r1.role_id,
    r1.role_tablename,
    r1.role_target_viewname
   FROM _all_roles_v12 r1
  WHERE r1.weight = (( SELECT max(r2.weight) AS max
           FROM _all_roles_v12 r2
          WHERE r1.person_id = r2.person_id));
