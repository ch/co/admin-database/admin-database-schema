--
-- Name: 10_View/Groups/Software_Licence_Banning; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Software_Licence_Banning" AS
 WITH q AS (
         SELECT research_group.id,
            research_group.name AS ro_name,
            research_group.head_of_group_id AS ro_head_of_group_id,
                CASE
                    WHEN (latest_software_declaration.declaration_date IS NULL) THEN false
                    WHEN ((latest_software_declaration.declaration_date + '1 year'::interval) > ('now'::text)::date) THEN true
                    ELSE false
                END AS ro_declaration_made,
            latest_software_declaration.declaration_date AS ro_date_of_last_declaration,
            latest_software_declaration.username_of_signer AS ro_declared_by,
            research_group.banned_for_lack_of_software_compliance
           FROM (((((public.research_group
             LEFT JOIN ( SELECT group_software_licence_declarations.research_group_id,
                    max(group_software_licence_declarations.declaration_date) AS declaration_date
                   FROM public.group_software_licence_declarations
                  GROUP BY group_software_licence_declarations.research_group_id) latest_software_declaration_date ON ((latest_software_declaration_date.research_group_id = research_group.id)))
             LEFT JOIN public.group_software_licence_declarations latest_software_declaration ON (((latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date) AND (latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id))))
             JOIN public.person head_of_group ON ((research_group.head_of_group_id = head_of_group.id)))
             JOIN public._physical_status_v3 ps ON ((head_of_group.id = ps.person_id)))
             LEFT JOIN ( SELECT members.head_of_group_id,
                    count(members.person_id) AS members
                   FROM ( SELECT _latest_role.supervisor_id AS head_of_group_id,
                            _latest_role.person_id
                           FROM (cache._latest_role
                             JOIN public._physical_status_v3 ps_1 USING (person_id))
                          WHERE ((ps_1.status_id)::text = 'Current'::text)
                        UNION
                         SELECT _latest_role.co_supervisor_id AS head_of_group_id,
                            _latest_role.person_id
                           FROM (cache._latest_role
                             JOIN public._physical_status_v3 ps_1 USING (person_id))
                          WHERE ((ps_1.status_id)::text = 'Current'::text)) members
                  GROUP BY members.head_of_group_id) group_members ON ((group_members.head_of_group_id = head_of_group.id)))
          WHERE ((((ps.status_id)::text = 'Current'::text) OR (group_members.members > 0)) AND (research_group.ignore_for_software_compliance_purposes IS NOT TRUE))
        )
 SELECT q.id,
    q.ro_name,
    q.ro_head_of_group_id,
    q.ro_declaration_made,
    q.ro_date_of_last_declaration,
    q.ro_declared_by,
    q.banned_for_lack_of_software_compliance
   FROM q
  ORDER BY q.ro_name;


ALTER VIEW hotwire3."10_View/Groups/Software_Licence_Banning" OWNER TO dev;

--
-- Name: 10_View/My_Groups/Software_Licence_Declaration; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration" AS
 WITH q AS (
         SELECT research_group.id,
            research_group.name AS ro_name,
            research_group.head_of_group_id AS ro_head_of_group_id,
            'I have instructed my group that that all software used by members of the group must be properly licensed and the group members have certified to me that that the software they use is properly licensed'::text AS ro_software_policy_declaration,
                CASE
                    WHEN (latest_software_declaration.declaration_date IS NULL) THEN false
                    WHEN ((latest_software_declaration.declaration_date + '1 year'::interval) > ('now'::text)::date) THEN true
                    ELSE false
                END AS declaration_made,
            latest_software_declaration.declaration_date AS ro_date_of_last_declaration,
            latest_software_declaration.username_of_signer AS ro_declared_by
           FROM (((((public.research_group
             LEFT JOIN ( SELECT group_software_licence_declarations.research_group_id,
                    max(group_software_licence_declarations.declaration_date) AS declaration_date
                   FROM public.group_software_licence_declarations
                  GROUP BY group_software_licence_declarations.research_group_id) latest_software_declaration_date ON ((latest_software_declaration_date.research_group_id = research_group.id)))
             LEFT JOIN public.group_software_licence_declarations latest_software_declaration ON (((latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date) AND (latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id))))
             JOIN public.person head_of_group ON ((research_group.head_of_group_id = head_of_group.id)))
             JOIN public._physical_status_v3 ps ON ((head_of_group.id = ps.person_id)))
             LEFT JOIN ( SELECT members.head_of_group_id,
                    count(members.person_id) AS members
                   FROM ( SELECT _latest_role.supervisor_id AS head_of_group_id,
                            _latest_role.person_id
                           FROM (cache._latest_role
                             JOIN public._physical_status_v3 ps_1 USING (person_id))
                          WHERE ((ps_1.status_id)::text = 'Current'::text)
                        UNION
                         SELECT _latest_role.co_supervisor_id AS head_of_group_id,
                            _latest_role.person_id
                           FROM (cache._latest_role
                             JOIN public._physical_status_v3 ps_1 USING (person_id))
                          WHERE ((ps_1.status_id)::text = 'Current'::text)) members
                  GROUP BY members.head_of_group_id) group_members ON ((group_members.head_of_group_id = head_of_group.id)))
          WHERE (("current_user"() = (head_of_group.crsid)::name) OR (pg_has_role("current_user"(), 'cos'::name, 'member'::text) AND (((ps.status_id)::text = 'Current'::text) OR (group_members.members > 0)) AND (research_group.ignore_for_software_compliance_purposes IS NOT TRUE)))
        )
 SELECT q.id,
    q.ro_name,
    q.ro_head_of_group_id,
    q.ro_software_policy_declaration,
    q.declaration_made,
    q.ro_date_of_last_declaration,
    q.ro_declared_by
   FROM q
  ORDER BY q.ro_name;


ALTER VIEW hotwire3."10_View/My_Groups/Software_Licence_Declaration" OWNER TO dev;

--
-- Name: 10_View/People/All_Contact_Details; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/All_Contact_Details" AS
 WITH contact_details AS (
         SELECT person.id,
            (convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo) AS ro_image,
            person.surname,
            person.first_names,
            person.title_id,
            person.known_as,
            person.name_suffix,
            person.previous_surname,
            person.gender_id,
            _latest_role.post_category_id AS ro_post_category_id,
            person.crsid,
            person.email_address,
            person.arrival_date,
            person.leaving_date,
            _physical_status.status_id AS ro_physical_status_id,
            person.left_but_no_leaving_date_given,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id,
            person.hide_phone_no_from_website,
            ARRAY( SELECT mm_person_room.room_id
                   FROM public.mm_person_room
                  WHERE (person.id = mm_person_room.person_id)) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM public.mm_person_research_group
                  WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id,
            person.external_work_phone_numbers,
            person.cambridge_address,
            person.cambridge_phone_number,
            person.cambridge_college_id,
            person.mobile_number,
            person.emergency_contact,
            person.location,
            person.forwarding_address,
            person.new_employer_address,
            ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM public.mm_person_nationality
                  WHERE (person.id = mm_person_nationality.person_id)) AS nationality_id,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM (((public.person
             LEFT JOIN public.person_photo ON ((person.id = person_photo.person_id)))
             LEFT JOIN cache._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
             LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))
        )
 SELECT contact_details.id,
    contact_details.id AS ro_person_id,
    contact_details.ro_image,
    contact_details.surname,
    contact_details.first_names,
    contact_details.title_id,
    contact_details.known_as,
    contact_details.name_suffix,
    contact_details.previous_surname,
    contact_details.gender_id,
    contact_details.ro_post_category_id,
    contact_details.crsid,
    contact_details.email_address,
    contact_details.arrival_date,
    contact_details.leaving_date,
    contact_details.ro_physical_status_id,
    contact_details.left_but_no_leaving_date_given,
    contact_details.dept_telephone_number_id,
    contact_details.room_id,
    contact_details.research_group_id,
    contact_details.external_work_phone_numbers,
    (contact_details.cambridge_address)::text AS home_address,
    contact_details.cambridge_phone_number AS home_phone_number,
    contact_details.cambridge_college_id,
    contact_details.mobile_number,
    (contact_details.emergency_contact)::text AS emergency_contact,
    contact_details.location,
    (contact_details.forwarding_address)::text AS forwarding_address,
    (contact_details.new_employer_address)::text AS new_employer_address,
    contact_details.nationality_id,
    contact_details._cssclass
   FROM contact_details
  ORDER BY contact_details.surname, contact_details.first_names;


ALTER VIEW hotwire3."10_View/People/All_Contact_Details" OWNER TO dev;

--
-- Name: 10_View/People/COs_View; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/COs_View" AS
 SELECT a.id,
    a.ro_person_id,
    a.image,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.crsid,
    a.date_of_birth,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.location,
    a.ro_physical_status_id,
    a.ro_estimated_leaving_date,
    a.do_not_show_on_website AS hide_person_from_website,
    a.hide_photo_from_website,
    a.hide_email AS hide_email_from_website,
    a.is_spri,
    a.managed_mail_domain_target,
    a.managed_mail_domain_optout,
    a.ro_final_managed_mail_domain_destination,
    a.ro_auto_banned_from_network,
    a.override_auto_ban,
    a.ro_network_ban_log,
    a._cssclass
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            (convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo) AS image,
            person.surname,
            person.first_names,
            person.title_id,
            person.name_suffix,
            person.email_address,
            person.crsid,
            person.date_of_birth,
            person.do_not_show_on_website,
            person.hide_photo_from_website,
            person.hide_email,
            person.is_spri,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM public.mm_person_research_group
                  WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id,
            ARRAY( SELECT mm_person_room.room_id
                   FROM public.mm_person_room
                  WHERE (person.id = mm_person_room.person_id)) AS room_id,
            person.location,
            _physical_status.status_id AS ro_physical_status_id,
            LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
            managed_mail_domain_v3.target AS ro_final_managed_mail_domain_destination,
            (person.managed_mail_domain_target)::character varying AS managed_mail_domain_target,
            person.managed_mail_domain_optout,
            person.auto_banned_from_network AS ro_auto_banned_from_network,
            person.override_auto_ban,
            (person.network_ban_log)::text AS ro_network_ban_log,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM (((((public.person
             LEFT JOIN public.person_photo ON ((person.id = person_photo.person_id)))
             LEFT JOIN cache._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
             LEFT JOIN cache.person_futuremost_role futuremost_role ON ((person.id = futuremost_role.person_id)))
             LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))
             LEFT JOIN apps.managed_mail_domain_v3 ON (((person.crsid)::text = (managed_mail_domain_v3.alias)::text)))) a
  ORDER BY a.surname, a.first_names;


ALTER VIEW hotwire3."10_View/People/COs_View" OWNER TO dev;

--
-- Name: 10_View/People/Chem_at_Cam; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Chem_at_Cam" AS
 WITH optins AS (
         SELECT person.id,
            person.surname,
            person.first_names,
            title_hid.abbrev_title,
            _latest_role.post_category,
            person.email_address,
            (person.forwarding_address)::text AS forwarding_address,
            person.leaving_date,
            _physical_status_v3.status_id AS presence
           FROM (((public.person
             LEFT JOIN cache._latest_role_v12 _latest_role ON ((_latest_role.person_id = person.id)))
             LEFT JOIN public.title_hid USING (title_id))
             LEFT JOIN public._physical_status_v3 USING (person_id))
          WHERE (person.chem_at_cam = true)
          ORDER BY person.leaving_date DESC
        )
 SELECT optins.id,
    optins.surname,
    optins.first_names,
    optins.abbrev_title AS title,
    optins.post_category,
    optins.email_address,
    optins.forwarding_address,
    optins.leaving_date,
    optins.presence
   FROM optins;


ALTER VIEW hotwire3."10_View/People/Chem_at_Cam" OWNER TO dev;

--
-- Name: 10_View/People/Contact_Details_By_Group; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Contact_Details_By_Group" AS
 WITH group_contact_details AS (
         SELECT person.id,
            research_group.name AS research_group,
            person_hid.person_hid AS person,
            _latest_role.post_category_id,
            person.arrival_date,
            COALESCE(_latest_role.end_date, _latest_role.intended_end_date) AS end_date,
            ARRAY( SELECT mm_person_room.room_id
                   FROM public.mm_person_room
                  WHERE (mm_person_room.person_id = person.id)) AS room_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (mm_person_dept_telephone_number.person_id = person.id)) AS dept_telephone_number_id,
            person.email_address,
            person.surname AS _surname,
            COALESCE(person.known_as, person.first_names) AS _first_names
           FROM (((((public.person
             JOIN public.person_hid ON ((person.id = person_hid.person_id)))
             LEFT JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id)))
             LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id)))
             JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))
             LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
          WHERE ((_physical_status.status_id)::text <> 'Past'::text)
        )
 SELECT group_contact_details.id,
    group_contact_details.research_group,
    group_contact_details.person,
    group_contact_details.post_category_id,
    group_contact_details.arrival_date,
    group_contact_details.end_date,
    group_contact_details.room_id,
    group_contact_details.dept_telephone_number_id,
    group_contact_details.email_address,
    group_contact_details._surname,
    group_contact_details._first_names
   FROM group_contact_details
  ORDER BY group_contact_details.research_group, group_contact_details._surname, group_contact_details._first_names;


ALTER VIEW hotwire3."10_View/People/Contact_Details_By_Group" OWNER TO dev;

--
-- Name: 10_View/People/Group_Admins_view; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Group_Admins_view" AS
 SELECT a.id,
    a.ro_person_id,
    a.ro_image,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.ro_physical_status_id,
    a.arrival_date,
    a.ro_estimated_leaving_date,
    a._cssclass
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            (convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo) AS ro_image,
            person.surname,
            person.first_names,
            person.title_id,
            person.name_suffix,
            person.email_address,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM public.mm_person_research_group
                  WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id,
            ARRAY( SELECT mm_person_room.room_id
                   FROM public.mm_person_room
                  WHERE (person.id = mm_person_room.person_id)) AS room_id,
            person.location,
            _physical_status.status_id AS ro_physical_status_id,
            person.arrival_date,
            LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM ((((public.person
             LEFT JOIN public.person_photo ON ((person.id = person_photo.person_id)))
             LEFT JOIN cache._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
             LEFT JOIN cache.person_futuremost_role futuremost_role ON ((person.id = futuremost_role.person_id)))
             LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))) a
  ORDER BY a.surname, a.first_names;


ALTER VIEW hotwire3."10_View/People/Group_Admins_view" OWNER TO dev;

--
-- Name: 10_View/People/Personnel_History; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_History" AS
 WITH personnel_history AS (
         SELECT person.id,
            person_hid.person_hid AS ro_person,
            person.surname,
            person.first_names,
            person.title_id,
            date_part('year'::text, age((person.date_of_birth)::timestamp with time zone)) AS ro_age,
            _latest_role.post_category_id AS ro_post_category_id,
            person.continuous_employment_start_date,
            _latest_employment.start_date AS ro_latest_employment_start_date,
            COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date,
            (age((COALESCE(_latest_employment.end_date, ('now'::text)::date))::timestamp with time zone, (person.continuous_employment_start_date)::timestamp with time zone))::text AS ro_length_of_continuous_service,
            (age((COALESCE(_latest_employment.end_date, ('now'::text)::date))::timestamp with time zone, (_latest_employment.start_date)::timestamp with time zone))::text AS ro_length_of_service_in_current_contract,
            age((_latest_employment.end_date)::timestamp with time zone, (person.continuous_employment_start_date)::timestamp with time zone) AS ro_final_length_of_continuous_service,
            age((_latest_employment.end_date)::timestamp with time zone, (_latest_employment.start_date)::timestamp with time zone) AS ro_final_length_of_service_in_last_contract,
            _latest_role.supervisor_id AS ro_supervisor_id,
            person.other_information,
            person.notes,
            emplids.emplid_number AS ro_emplid_number,
            person.next_review_due AS staff_review_due,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass,
            public._to_hwsubviewb('10_View/People/_Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Reviews'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview,
            public._to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
           FROM (((((public.person
             LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
             LEFT JOIN public._latest_employment ON ((person.id = _latest_employment.person_id)))
             LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
             LEFT JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = person.id)))
             LEFT JOIN ( SELECT all_emplids.person_id,
                    (string_agg((all_emplids.emplid_number)::text, '/'::text))::character varying AS emplid_number
                   FROM ( SELECT DISTINCT postgraduate_studentship.person_id,
                            postgraduate_studentship.emplid_number
                           FROM public.postgraduate_studentship
                          WHERE (postgraduate_studentship.emplid_number IS NOT NULL)) all_emplids
                  GROUP BY all_emplids.person_id) emplids ON ((person.id = emplids.person_id)))
        )
 SELECT personnel_history.id,
    personnel_history.ro_person,
    personnel_history.surname,
    personnel_history.first_names,
    personnel_history.title_id,
    personnel_history.ro_age,
    personnel_history.ro_post_category_id,
    personnel_history.continuous_employment_start_date,
    personnel_history.ro_latest_employment_start_date,
    personnel_history.ro_employment_end_date,
    personnel_history.ro_length_of_continuous_service,
    personnel_history.ro_length_of_service_in_current_contract,
    personnel_history.ro_final_length_of_continuous_service,
    personnel_history.ro_final_length_of_service_in_last_contract,
    personnel_history.ro_supervisor_id,
    personnel_history.other_information,
    personnel_history.notes,
    personnel_history.ro_emplid_number,
    personnel_history.staff_review_due,
    personnel_history._cssclass,
    personnel_history.staff_review_history_subview,
    personnel_history.cambridge_history_subview
   FROM personnel_history
  ORDER BY personnel_history.surname, personnel_history.first_names;


ALTER VIEW hotwire3."10_View/People/Personnel_History" OWNER TO dev;

--
-- Name: 10_View/People/Personnel_Phone; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Phone" AS
 SELECT a.id,
    (a.ro_person)::character varying(80) AS ro_person,
    a.ro_image,
    a.surname,
    a.first_names,
    a.contact_preference_id,
    a.dept_telephone_number_id,
    a.external_work_numbers,
    (a.ro_teams_chat)::character varying(80) AS ro_teams_chat,
    a.other_contact_details,
    a.room_id,
    a.ro_supervisor_id,
    a.email_address,
    a.location,
    a.ro_post_category_id,
    a.ro_physical_status_id,
    a.is_external,
    a._cssclass,
    a._title_hid,
    a._known_as
   FROM ( SELECT person.id,
            ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.abbrev_title)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text) AS ro_person,
            (convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo) AS ro_image,
            person.surname,
            person.first_names,
            mm_person_contact_preference.contact_preference_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id,
            person.external_work_phone_numbers AS external_work_numbers,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Current'::text) THEN ((person.crsid)::text || '@cam.ac.uk'::text)
                    ELSE NULL::text
                END AS ro_teams_chat,
            mm_person_contact_preference.further_details AS other_contact_details,
            ARRAY( SELECT mm_person_room.room_id
                   FROM public.mm_person_room
                  WHERE (person.id = mm_person_room.person_id)) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            person.email_address,
            person.location,
            _latest_role.post_category_id AS ro_post_category_id,
            _physical_status.status_id AS ro_physical_status_id,
            person.is_external,
                CASE
                    WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
                    WHEN (person.is_external = true) THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass,
            title_hid.abbrev_title AS _title_hid,
            person.known_as AS _known_as
           FROM (((((public.person
             LEFT JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = person.id)))
             LEFT JOIN public.person_photo ON ((person.id = person_photo.person_id)))
             LEFT JOIN public.title_hid USING (title_id))
             LEFT JOIN public.mm_person_contact_preference ON ((mm_person_contact_preference.person_id = person.id)))
             LEFT JOIN cache._latest_role _latest_role ON ((_latest_role.person_id = person.id)))) a
  ORDER BY a.surname, a._title_hid, a._known_as;


ALTER VIEW hotwire3."10_View/People/Personnel_Phone" OWNER TO dev;

--
-- Name: 10_View/People/Researcher_Staff_Reviews; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Researcher_Staff_Reviews" AS
 SELECT person.id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.title_id AS ro_title_id,
    person.email_address AS ro_email_address,
    _latest_role.post_category_id AS ro_post_category_id,
    COALESCE(person.usual_reviewer_id, _latest_role.supervisor_id) AS ro_reviewer_id,
    COALESCE(usual_reviewer.email_address, supervisor.email_address) AS ro_reviewer_email_address,
    _latest_role.supervisor_id AS ro_supervisor_id,
    person.next_review_due,
    last_review.date_of_meeting AS ro_date_of_last_review,
    NULL::date AS record_a_new_review,
    COALESCE(person.usual_reviewer_id, _latest_role.supervisor_id) AS new_reviewer_id,
    NULL::text AS new_review_notes,
    NULL::bigint AS update_usual_reviewer_id,
    person.silence_staff_review_reminders AS silence_reminders_for_this_reviewee,
    COALESCE(usual_reviewer.silence_staff_review_reminders, supervisor.silence_staff_review_reminders) AS silence_all_reminders_for_this_reviewer,
    person.staff_review_notes AS reminder_notes,
        CASE
            WHEN (person.next_review_due < ('now'::text)::date) THEN 'red'::text
            WHEN (person.next_review_due < (('now'::text)::date + '1 mon'::interval)) THEN 'orange'::text
            ELSE NULL::text
        END AS _cssclass
   FROM (((((public.person
     JOIN public._latest_role_v12 _latest_role ON ((_latest_role.person_id = person.id)))
     JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = person.id)))
     JOIN public.person supervisor ON ((_latest_role.supervisor_id = supervisor.id)))
     LEFT JOIN public.person usual_reviewer ON ((person.usual_reviewer_id = usual_reviewer.id)))
     LEFT JOIN ( SELECT staff_review_meeting.person_id,
            max(staff_review_meeting.date_of_meeting) AS date_of_meeting
           FROM public.staff_review_meeting
          WHERE (staff_review_meeting.date_of_meeting IS NOT NULL)
          GROUP BY staff_review_meeting.person_id) last_review ON ((last_review.person_id = person.id)))
  WHERE (((_physical_status.status_id)::text = 'Current'::text) AND ((_latest_role.post_category_id = 'sc-7'::text) OR (_latest_role.post_category_id = 'sc-4'::text) OR (_latest_role.post_category_id = 'sc-10'::text) OR (_latest_role.post_category_id = 'sc-6'::text) OR (_latest_role.post_category_id = 'sc-5'::text)) AND (_latest_role.paid_by_university <> false))
  ORDER BY person.next_review_due;


ALTER VIEW hotwire3."10_View/People/Researcher_Staff_Reviews" OWNER TO dev;

--
-- Name: 10_View/People/SPRI_People; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/SPRI_People" AS
 SELECT person.id,
    person.id AS person_id,
    person.surname,
    person.first_names,
    person.title_id,
    person.email_address,
    person.crsid,
    ARRAY( SELECT mm_person_room.room_id
           FROM public.mm_person_room
          WHERE (mm_person_room.person_id = person.id)) AS room_id,
    remaining_computers.remaining AS ro_number_of_computers_associated,
        CASE
            WHEN ((_physical_status.status_id IS NOT NULL) AND ((_physical_status.status_id)::text <> 'Unknown'::text)) THEN true
            WHEN (_latest_role.status IS NOT NULL) THEN true
            ELSE false
        END AS ro_also_chemistry_person,
        CASE
            WHEN pg_has_role('cos'::name, 'MEMBER'::text) THEN hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware'::character varying, '_owner_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying)
            ELSE hotwire3.to_hwsubviewb('10_View/People/_SPRI_hardware'::character varying, '_owner_id'::character varying, NULL::character varying, NULL::character varying, NULL::character varying)
        END AS computers_owned,
        CASE
            WHEN pg_has_role('cos'::name, 'MEMBER'::text) THEN hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances'::character varying, '_user_id'::character varying, '10_View/Computers/System_Instances'::character varying, 'system_image_id'::character varying, 'id'::character varying)
            ELSE hotwire3.to_hwsubviewb('10_View/People/_SPRI_instances'::character varying, '_user_id'::character varying, NULL::character varying, NULL::character varying, NULL::character varying)
        END AS computers_used
   FROM (((public.person
     LEFT JOIN ( SELECT it_objects.person_id,
            count(it_objects.id) AS remaining
           FROM ( SELECT hardware.owner_id AS person_id,
                    hardware.id
                   FROM public.hardware
                UNION ALL
                 SELECT system_image.user_id AS person_id,
                    system_image.id
                   FROM public.system_image) it_objects
          GROUP BY it_objects.person_id) remaining_computers ON ((remaining_computers.person_id = person.id)))
     LEFT JOIN cache._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
     LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))
  WHERE (person.is_spri = true)
  ORDER BY person.surname, person.first_names;


ALTER VIEW hotwire3."10_View/People/SPRI_People" OWNER TO dev;

--
-- Name: 10_View/People/Staff_Reviews; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/People/Staff_Reviews" AS
 WITH a AS (
         SELECT staff_review_meeting.id,
            staff_review_meeting.person_id,
            person.usual_reviewer_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            staff_review_meeting.date_of_meeting,
            staff_review_meeting.reviewer_id,
            person.next_review_due,
            (staff_review_meeting.notes)::text AS notes,
            (staff_review_meeting.last_updated)::character varying AS ro_last_updated,
            (staff_review_meeting.last_updated)::timestamp(0) without time zone AS _last_updated
           FROM ((public.staff_review_meeting
             JOIN public.person ON ((person.id = staff_review_meeting.person_id)))
             JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id)))
        )
 SELECT a.id,
    a.person_id,
    a.usual_reviewer_id,
    a.ro_supervisor_id,
    a.date_of_meeting,
    a.reviewer_id,
    a.next_review_due,
    a.notes,
    a.ro_last_updated,
    a._last_updated
   FROM a
  ORDER BY a._last_updated DESC NULLS LAST;


ALTER VIEW hotwire3."10_View/People/Staff_Reviews" OWNER TO dev;

--
-- Name: 10_View/Roles/_current_people_supervised_by; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/_current_people_supervised_by" AS
 SELECT lr.role_id AS id,
    lr.person_id,
    lr.start_date,
    lr.intended_end_date,
    lr.end_date,
    lr.funding_end_date,
    lr.post_category,
    lr.supervisor_id,
    lr.funding,
    lr.fees_funding,
    lr.research_grant_number,
    lr.paid_by_university,
    lr.role_target_viewname AS _target_viewname,
    lr.role_id AS _role_xid
   FROM (public._latest_role_v12 lr
     JOIN public._physical_status_v3 ps USING (person_id))
  WHERE (((ps.status_id)::text = 'Current'::text) AND ((lr.status)::text = 'Current'::text));


ALTER VIEW hotwire3."10_View/Roles/_current_people_supervised_by" OWNER TO dev;

--
-- Name: 10_View/Rooms/_contact_details; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Rooms/_contact_details" AS
 SELECT person.id,
    mm_person_room.room_id,
    person.id AS person_id,
    person.email_address,
    lr.supervisor_id
   FROM (((public.person
     JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id)))
     JOIN public._physical_status_v3 ps USING (person_id))
     LEFT JOIN public._latest_role_v12 lr USING (person_id))
  WHERE ((ps.status_id)::text = 'Current'::text);


ALTER VIEW hotwire3."10_View/Rooms/_contact_details" OWNER TO dev;

--
-- Name: 10_View/Safety/Fire_wardens; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Fire_wardens" AS
 WITH hotwire_fire_wardens AS (
         SELECT mm_person_fire_warden_area.mm_person_fire_warden_area_id AS id,
            mm_person_fire_warden_area.person_id,
            person.fire_warden_qualification_date AS qualification_date,
            mm_person_fire_warden_area.fire_warden_area_id,
            building_hid.building_hid AS ro_building,
            building_region_hid.building_region_hid AS ro_building_region,
            building_floor_hid.building_floor_hid AS ro_building_floor,
            mm_person_fire_warden_area.is_primary AS main_firewarden_for_area,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM public.mm_person_dept_telephone_number
                  WHERE (mm_person_fire_warden_area.person_id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id,
            person.email_address AS ro_email_address,
            _physical_status.status_id AS ro_physical_status,
            (_fire_warden_status.requalification_date)::date AS ro_requalification_date,
            _fire_warden_status.in_date AS ro_qualification_in_date,
            _latest_role.post_category AS ro_post_category,
            _latest_role.estimated_leaving_date AS ro_estimated_leaving_date,
                CASE
                    WHEN ((_physical_status.status_id)::text <> 'Current'::text) THEN 'orange'::text
                    WHEN (_fire_warden_status.in_date = false) THEN 'red'::text
                    WHEN (_fire_warden_status.in_date IS NULL) THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass,
            (((building_hid.building_id * 100))::numeric +
                CASE
                    WHEN ((building_floor_hid.building_floor_hid)::text = 'Basement'::text) THEN (1)::numeric
                    WHEN (((building_floor_hid.building_floor_hid)::text = 'Mezzanine'::text) AND ((building_hid.building_hid)::text = 'Main building'::text)) THEN '-1.5'::numeric
                    WHEN (((building_floor_hid.building_floor_hid)::text = 'Mezzanine'::text) AND ((building_hid.building_hid)::text = 'UCC'::text)) THEN '-2.5'::numeric
                    ELSE (('-1'::integer * building_floor_hid.building_floor_id))::numeric
                END) AS _sort_order
           FROM ((((((((public.mm_person_fire_warden_area
             LEFT JOIN public._latest_role_v12 _latest_role USING (person_id))
             LEFT JOIN public.fire_warden_area USING (fire_warden_area_id))
             LEFT JOIN hotwire3.building_hid USING (building_id))
             LEFT JOIN hotwire3.building_region_hid USING (building_region_id))
             LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id))
             LEFT JOIN public._physical_status_v3 _physical_status USING (person_id))
             LEFT JOIN public._fire_warden_status USING (person_id))
             LEFT JOIN public.person ON ((person.id = mm_person_fire_warden_area.person_id)))
        )
 SELECT hotwire_fire_wardens.id,
    hotwire_fire_wardens.person_id,
    hotwire_fire_wardens.qualification_date,
    hotwire_fire_wardens.fire_warden_area_id,
    hotwire_fire_wardens.ro_building,
    hotwire_fire_wardens.ro_building_region,
    hotwire_fire_wardens.ro_building_floor,
    hotwire_fire_wardens.main_firewarden_for_area,
    hotwire_fire_wardens.dept_telephone_number_id,
    hotwire_fire_wardens.ro_email_address,
    hotwire_fire_wardens.ro_physical_status,
    hotwire_fire_wardens.ro_requalification_date,
    hotwire_fire_wardens.ro_qualification_in_date,
    hotwire_fire_wardens.ro_post_category,
    hotwire_fire_wardens.ro_estimated_leaving_date,
    hotwire_fire_wardens._cssclass,
    hotwire_fire_wardens._sort_order
   FROM hotwire_fire_wardens
  ORDER BY hotwire_fire_wardens._sort_order;


ALTER VIEW hotwire3."10_View/Safety/Fire_wardens" OWNER TO dev;

--
-- Name: 10_View/Safety/Firstaiders; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Firstaiders" AS
 SELECT firstaider.id,
    (convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo) AS ro_image,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.hf_cn_qualification_date,
    firstaider.hf_cn_requalify_date,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM (((public.person p2
             LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id)))
             LEFT JOIN public.room ON ((room.id = mm_person_room.room_id)))
             LEFT JOIN hotwire3.building_hid USING (building_id))
          WHERE (p2.id = firstaider.person_id)
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM (((public.person p2
             LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id)))
             LEFT JOIN public.room ON ((room.id = mm_person_room.room_id)))
             LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id))
          WHERE (p2.id = firstaider.person_id)
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM ((public.person p2
             LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id)))
             LEFT JOIN public.room ON ((room.id = mm_person_room.room_id)))
          WHERE (p2.id = firstaider.person_id)
          ORDER BY room.name), '/'::text) AS ro_room,
    array_to_string(ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM ((public.person p2
             JOIN public.mm_person_dept_telephone_number ON ((mm_person_dept_telephone_number.person_id = p2.id)))
             JOIN public.dept_telephone_number_hid ON ((dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id)))
          WHERE (p2.id = firstaider.person_id)), ' / '::text) AS ro_dept_telephone_numbers,
    person.email_address AS ro_email_address,
    _physical_status.status_id AS status,
        CASE
            WHEN (firstaider.requalify_date >= CURRENT_DATE) THEN true
            ELSE false
        END AS ro_qualification_up_to_date,
        CASE
            WHEN (firstaider.hf_cn_trained AND (firstaider.requalify_date >= CURRENT_DATE) AND (firstaider.hf_cn_requalify_date >= CURRENT_DATE)) THEN true
            WHEN (firstaider.hf_cn_trained AND ((firstaider.requalify_date < CURRENT_DATE) OR (firstaider.hf_cn_requalify_date < CURRENT_DATE) OR (firstaider.hf_cn_requalify_date IS NULL))) THEN false
            ELSE NULL::boolean
        END AS ro_hf_cn_qualification_up_to_date,
    _latest_role.post_category AS ro_post_category,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
        CASE
            WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text
            WHEN (firstaider.requalify_date < CURRENT_DATE) THEN 'red'::text
            ELSE NULL::text
        END AS _cssclass
   FROM (((((public.firstaider
     JOIN public.person ON ((firstaider.person_id = person.id)))
     LEFT JOIN public.person_photo ON ((firstaider.person_id = person_photo.person_id)))
     LEFT JOIN cache._latest_role_v12 _latest_role ON ((firstaider.person_id = _latest_role.person_id)))
     LEFT JOIN cache.person_futuremost_role futuremost_role ON ((firstaider.person_id = futuremost_role.person_id)))
     JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = firstaider.person_id)));


ALTER VIEW hotwire3."10_View/Safety/Firstaiders" OWNER TO dev;

--
-- Name: 10_View/Safety/Registration; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Registration" AS
 SELECT form.form_id AS id,
    COALESCE(person.first_names, (form.first_names)::character varying) AS first_names,
    COALESCE(person.surname, (form.surname)::character varying) AS surname,
    COALESCE(person.known_as, (form.known_as)::character varying) AS known_as,
    COALESCE(person.title_id, form.title_id) AS title_id,
    COALESCE(_latest_role.post_category_id, form.post_category_id) AS post_category_id,
    COALESCE(ARRAY( SELECT mm_person_room.room_id
           FROM public.mm_person_room
          WHERE (person.id = mm_person_room.person_id)), form.dept_room_id) AS room_id,
    COALESCE(ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM public.mm_person_dept_telephone_number
          WHERE (person.id = mm_person_dept_telephone_number.person_id)), form.dept_telephone_id) AS dept_telephone_number_id,
    COALESCE(person.email_address, (form.email)::character varying) AS email,
    COALESCE(person.crsid, (form.crsid)::character varying) AS crsid,
    COALESCE(person.arrival_date, form.start_date) AS arrival_date,
    COALESCE(_latest_role.intended_end_date, form.intended_end_date) AS intended_end_date,
    COALESCE(_latest_role.supervisor_id, form.department_host_id) AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.separate_safety_form AS ro_paper_safety_checklist,
    ('\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf) AS "safety_checklist.pdf",
    safety_induction_carried_out_by.person_hid AS ro_safety_induction_carried_out_by,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_off_on,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date
   FROM (((registration.form
     LEFT JOIN public.person ON ((form._match_to_person_id = person.id)))
     LEFT JOIN hotwire3.person_hid safety_induction_carried_out_by ON ((form.safety_induction_person_signing_off_id = safety_induction_carried_out_by.person_id)))
     LEFT JOIN public._latest_role_v12 _latest_role ON ((_latest_role.person_id = person.id)))
  WHERE (form.submitted = true);


ALTER VIEW hotwire3."10_View/Safety/Registration" OWNER TO dev;

--
-- Name: 30_Report/Inconsistent_Status; Type: VIEW; Schema: hotwire3; Owner: dev
--

CREATE OR REPLACE VIEW hotwire3."30_Report/Inconsistent_Status" AS
 SELECT _physical_status.person_id AS id,
    _physical_status.person_id,
    _latest_role.post_category_id,
    _physical_status.status_id AS physical_status,
    person.arrival_date AS physical_arrival_date,
    person.leaving_date AS physical_leaving_date,
    _latest_role.status AS newest_role_status,
    _latest_role.start_date AS newest_role_start_date,
    _latest_role.intended_end_date AS newest_role_intended_end_date,
    _latest_role.end_date AS newest_role_end_date
   FROM ((public._physical_status_v3 _physical_status
     LEFT JOIN public._latest_role_v12 _latest_role USING (person_id))
     LEFT JOIN public.person ON ((person.id = _physical_status.person_id)))
  WHERE ((_physical_status.status_id)::text <> (_latest_role.status)::text);


ALTER VIEW hotwire3."30_Report/Inconsistent_Status" OWNER TO dev;

--
-- Name: 40_Selfservice/Database_Selfservice; Type: VIEW; Schema: selfservice; Owner: dev
--

CREATE OR REPLACE VIEW selfservice."40_Selfservice/Database_Selfservice" WITH (security_barrier='true') AS
 SELECT DISTINCT person.id,
    title_hid.abbrev_title AS ro_title,
    person.first_names AS ro_first_names,
    person.surname AS ro_surname,
    person.known_as,
    person.name_suffix AS name_suffix_eg_frs,
    gender_hid.gender_hid AS ro_gender,
    person.previous_surname AS ro_previous_name,
    nationality_string.ro_nationality,
    person.email_address,
    person.hide_email AS hide_email_from_dept_websites,
    ARRAY( SELECT mm.room_id
           FROM public.mm_person_room mm
          WHERE (mm.person_id = person.id)) AS room_id,
    ARRAY( SELECT mm.dept_telephone_number_id
           FROM public.mm_person_dept_telephone_number mm
          WHERE (mm.person_id = person.id)) AS dept_telephone_number_id,
    mm_person_contact_preference.contact_preference_id,
    mm_person_contact_preference.further_details AS if_contact_preference_is_other_give_details,
    cambridge_college_hid.cambridge_college_hid AS ro_cambridge_college,
    (person.cambridge_address)::text AS home_address,
    person.cambridge_phone_number AS home_phone_number,
    (person.emergency_contact)::text AS emergency_contact,
    research_groups_string.ro_research_group,
    (post_category_hid.post_category_hid)::character varying AS ro_post_category,
    (supervisor_hid.supervisor_hid)::character varying AS ro_supervisor,
    person.arrival_date AS ro_arrival_date,
    _latest_role_v12.funding_end_date AS ro_funding_end_date,
    COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date,
    person.crsid AS ro_crsid,
    (person.managed_mail_domain_target)::character varying AS alternative_email_target_for_chemistry_mail_domain,
    person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain,
    person.do_not_show_on_website AS hide_from_website,
    person.hide_photo_from_website,
    user_account.chemnet_token AS ro_chemnet_token,
    false AS create_new_token
   FROM ((((((((((((public.person
     LEFT JOIN public._latest_role_v12 ON ((person.id = _latest_role_v12.person_id)))
     LEFT JOIN public.user_account ON (((user_account.username)::text = (person.crsid)::text)))
     LEFT JOIN public.cambridge_college_hid USING (cambridge_college_id))
     LEFT JOIN public.supervisor_hid USING (supervisor_id))
     LEFT JOIN public.gender_hid USING (gender_id))
     LEFT JOIN public.title_hid USING (title_id))
     LEFT JOIN public.post_category_hid USING (post_category_id))
     LEFT JOIN public.mm_person_research_group ON ((mm_person_research_group.person_id = person.id)))
     LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id)))
     LEFT JOIN ( SELECT person_1.id AS person_id,
            (string_agg((nationality.nationality)::text, ', '::text))::character varying AS ro_nationality
           FROM ((public.person person_1
             LEFT JOIN public.mm_person_nationality ON ((mm_person_nationality.person_id = person_1.id)))
             LEFT JOIN public.nationality ON ((mm_person_nationality.nationality_id = nationality.id)))
          GROUP BY person_1.id) nationality_string ON ((nationality_string.person_id = person.id)))
     LEFT JOIN ( SELECT person_1.id AS person_id,
            (string_agg((research_group_1.name)::text, ', '::text))::character varying AS ro_research_group
           FROM ((public.person person_1
             LEFT JOIN public.mm_person_research_group mm_person_research_group_1 ON ((mm_person_research_group_1.person_id = person_1.id)))
             LEFT JOIN public.research_group research_group_1 ON ((mm_person_research_group_1.research_group_id = research_group_1.id)))
          GROUP BY person_1.id) research_groups_string ON ((research_groups_string.person_id = person.id)))
     LEFT JOIN public.mm_person_contact_preference ON ((person.id = mm_person_contact_preference.person_id)))
  WHERE (((person.crsid)::name = "current_user"()) AND (person.ban_from_self_service <> true))
  GROUP BY person.id, title_hid.abbrev_title, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, _latest_role_v12.intended_end_date, _latest_role_v12.end_date, user_account.chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group, mm_person_contact_preference.contact_preference_id, mm_person_contact_preference.further_details;


ALTER VIEW selfservice."40_Selfservice/Database_Selfservice" OWNER TO dev;
