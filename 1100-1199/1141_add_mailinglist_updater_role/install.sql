-- Adding the roles below does not get reversed by the remove.sql script; it edits the
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'mailinglist_updater') THEN
      CREATE ROLE mailinglist_updater LOGIN;
   END IF;
END
$do$;

GRANT SELECT,INSERT,DELETE ON public.mm_mailinglist_include_person TO mailinglist_updater;
GRANT SELECT,INSERT,DELETE ON public.mm_mailinglist_exclude_person TO mailinglist_updater;
GRANT SELECT,UPDATE ON public.mailinglist TO mailinglist_updater;
GRANT SELECT (id,email_address) ON public.person TO mailinglist_updater;
