REVOKE SELECT,INSERT,DELETE ON public.mm_mailinglist_include_person FROM mailinglist_updater;
REVOKE SELECT,INSERT,DELETE ON public.mm_mailinglist_exclude_person FROM mailinglist_updater;
REVOKE SELECT,UPDATE ON public.mailinglist FROM mailinglist_updater;
REVOKE SELECT (id,email_address) ON public.person FROM mailinglist_updater;
