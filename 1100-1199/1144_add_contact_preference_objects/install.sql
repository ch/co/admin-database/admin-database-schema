-- These are synchronous contact preferences so email is not going to be
-- an option
CREATE TABLE public.contact_preference (
    contact_preference_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    preference TEXT UNIQUE NOT NULL
    )
;

ALTER TABLE public.contact_preference OWNER TO dev;

GRANT SELECT ON public.contact_preference TO PUBLIC;

INSERT INTO public.contact_preference ( preference ) VALUES ( 'Teams' ), ('University phone'), ('Other');

CREATE TABLE public.mm_person_contact_preference (
    person_id BIGINT UNIQUE NOT NULL REFERENCES public.person(id),
    contact_preference_id BIGINT NOT NULL REFERENCES public.contact_preference(contact_preference_id),
    further_details TEXT
);

ALTER TABLE public.mm_person_contact_preference OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_contact_preference TO hr, reception; -- selfservice? websites?
GRANT SELECT,INSERT,UPDATE ON public.mm_person_contact_preference TO selfservice_updater;

-- make person_id unique on public.mm_person_contact_preference for now, we will only allow recording one preference per person
CREATE UNIQUE INDEX person_contact_pref ON public.mm_person_contact_preference(person_id);
-- add constraint that details is not null if contact_preference_id is 3
ALTER TABLE public.mm_person_contact_preference ADD CONSTRAINT if_contact_preference_is_other_must_have_further_details CHECK ( contact_preference_id <> 3 OR ( (further_details IS NOT NULL) AND (further_details <> '') ));
