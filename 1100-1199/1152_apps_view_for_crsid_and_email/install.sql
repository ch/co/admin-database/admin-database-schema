CREATE VIEW apps.crsid_and_email AS
    SELECT
        crsid,
        email_address
    FROM public.person
    WHERE crsid IS NOT NULL;

ALTER VIEW apps.crsid_and_email OWNER TO dev;
GRANT SELECT ON apps.crsid_and_email TO ad_accounts;
