-- View: www.group_computer_reps_v2

-- DROP VIEW www.group_computer_reps_v2;

CREATE OR REPLACE VIEW www.group_computer_reps_v2
 AS
 SELECT person_info.www_person_hid,
    person_info.email_address,
    mm_research_group_computer_rep.research_group_id
   FROM mm_research_group_computer_rep
     LEFT JOIN person ON person.id = mm_research_group_computer_rep.computer_rep_id
     LEFT JOIN www.person_info ON person_info.id = mm_research_group_computer_rep.computer_rep_id
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
  WHERE _physical_status_v3.status_id::text = 'Current'::text OR person.is_spri = true;

ALTER TABLE www.group_computer_reps_v2
    OWNER TO dev;

GRANT ALL ON TABLE www.group_computer_reps_v2 TO dev;
GRANT SELECT ON TABLE www.group_computer_reps_v2 TO PUBLIC;

