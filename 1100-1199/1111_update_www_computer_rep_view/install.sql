-- View: www.group_computer_reps_v2

-- DROP VIEW www.group_computer_reps_v2;

CREATE OR REPLACE VIEW www.group_computer_reps_v2
 AS
 SELECT www_person_hid_v2.www_person_hid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
        mm_research_group_computer_rep.research_group_id
   FROM mm_research_group_computer_rep
     LEFT JOIN person ON person.id = mm_research_group_computer_rep.computer_rep_id
     LEFT JOIN www_person_hid_v2 on person_id=computer_rep_id
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
  WHERE _physical_status_v3.status_id::text = 'Current'::text OR person.is_spri = true;

ALTER TABLE www.group_computer_reps_v2
    OWNER TO dev;

GRANT SELECT ON TABLE www.group_computer_reps_v2 TO www_sites;

