CREATE OR REPLACE FUNCTION public.update_rg_admins(
	)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    admin text;
BEGIN
    -- Create missing database roles
    FOR admin IN
        SELECT DISTINCT person.crsid
        FROM public.research_group
        JOIN person ON person.id = research_group.administrator_id
        -- Joining to this view ensures we only include people who are eligible for IT accounts
        JOIN apps.user_accounts_to_create_in_ad_v2 ON user_accounts_to_create_in_ad_v2.crsid = person.crsid
        LEFT JOIN pg_catalog.pg_roles ON person.crsid = pg_roles.rolname
        WHERE person.crsid IS NOT NULL AND pg_roles.rolname IS NULL
    LOOP
        EXECUTE FORMAT("CREATE ROLE %I WITH LOGIN IN ROLE rg_admins,_autoadded", admin);
    END LOOP;
    -- Remove membership in rg_admins of roles belonging to an individual who is no longer a research_group admin.
    -- They could still be getting rights via the lab_managers role which is manually maintained, hence we don't check
    -- pg_has_role() here, but look for a direct membership.
    FOR admin IN
        WITH direct_rg_admins_members AS (
            SELECT member.rolname
            FROM pg_catalog.pg_auth_members
            JOIN pg_catalog.pg_roles member ON pg_auth_members.member = member.oid
            JOIN pg_catalog.pg_roles role ON pg_auth_members.roleid = role.oid
            WHERE role.rolname = 'rg_admins'
        )
        SELECT DISTINCT person.crsid
        FROM pg_catalog.pg_roles
        JOIN public.person ON person.crsid = pg_roles.rolname
        JOIN direct_rg_admins_members ON direct_rg_admins_members.rolname = person.crsid
        LEFT JOIN public.research_group ON person.id = research_group.administrator_id
        WHERE person.crsid IS NOT NULL AND research_group.id IS NULL
    LOOP
        EXECUTE FORMAT('REVOKE rg_admins FROM %I', admin);
    END LOOP;
    -- Grant direct membership to any group admin who doesn't already have it
    FOR admin IN
        WITH direct_rg_admins_members AS (
            SELECT member.rolname
            FROM pg_catalog.pg_auth_members
            JOIN pg_catalog.pg_roles member ON pg_auth_members.member = member.oid
            JOIN pg_catalog.pg_roles role ON pg_auth_members.roleid = role.oid
            WHERE role.rolname = 'rg_admins'
        )
        SELECT DISTINCT person.crsid
        FROM public.research_group
        JOIN person ON person.id = research_group.administrator_id
        -- Joining to this view ensures we only include people who are eligible for IT accounts
        JOIN apps.user_accounts_to_create_in_ad_v2 ON user_accounts_to_create_in_ad_v2.crsid = person.crsid
        LEFT JOIN direct_rg_admins_members ON direct_rg_admins_members.rolname = person.crsid
        WHERE person.crsid IS NOT NULL AND direct_rg_admins_members.rolname IS NULL
    LOOP
        EXECUTE FORMAT('GRANT rg_admins TO %I', admin);
    END LOOP;
END;
$BODY$;
