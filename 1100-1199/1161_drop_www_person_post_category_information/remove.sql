CREATE VIEW www.person_post_category_information
 AS
 SELECT p.id,
    p.counts_as_academic,
    p.counts_as_postdoc,
    l.post_category_id,
    l.post_category
   FROM person p
     LEFT JOIN cache._latest_role l ON p.id = l.person_id;

ALTER TABLE www.person_post_category_information
    OWNER TO cen1001;
COMMENT ON VIEW www.person_post_category_information
    IS 'see ticket #120135, this was set up for Carl so he can see the columns that contribute to post_category in www.person_info_v6';

GRANT ALL ON TABLE www.person_post_category_information TO cen1001;
GRANT SELECT ON TABLE www.person_post_category_information TO web_editors;

