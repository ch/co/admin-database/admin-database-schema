-- This makes phones of type 2 (Teams) have either TRUE or NULL in the personal_line column
ALTER TABLE public.dept_telephone_number ADD CONSTRAINT teams_phones_are_not_shared CHECK ( NOT ( phone_type_id = 2 AND personal_line = FALSE ));
