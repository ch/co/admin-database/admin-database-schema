ALTER TABLE public.mailinglist OWNER TO cen1001;
GRANT ALL ON public.mailinglist TO dev;
ALTER TABLE public.mm_mailinglist_include_person OWNER TO cen1001;
GRANT ALL ON public.mm_mailinglist_include_person TO dev;
ALTER TABLE public.mm_mailinglist_exclude_person OWNER TO cen1001;
GRANT ALL ON public.mm_mailinglist_exclude_person TO dev;
