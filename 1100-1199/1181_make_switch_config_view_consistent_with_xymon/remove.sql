-- View: hotwire3."10_View/Network/Switches/Switch_Configs_ro"

-- DROP VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro"
 AS
 SELECT switch.id,
    switch.id AS switch_id,
    array_to_string(ARRAY( SELECT _switch_config_b(switch.id) AS _switch_config), '
'::text) AS ro_config
   FROM switch;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" TO dev;
