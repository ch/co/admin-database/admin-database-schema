CREATE VIEW public._virtualised_hosts_updates
 AS
 SELECT system_image.id AS system_image_id,
    system_image.host_system_image_id,
    ip_address.hostname
   FROM system_image
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id;

ALTER TABLE public._virtualised_hosts_updates
    OWNER TO cen1001;

GRANT ALL ON TABLE public._virtualised_hosts_updates TO cen1001;
GRANT ALL ON TABLE public._virtualised_hosts_updates TO dev;
GRANT SELECT, UPDATE ON TABLE public._virtualised_hosts_updates TO virtualisation;


-- Rule: _virtualised_hosts_updates_upd ON public._virtualised_hosts_updates

-- DROP Rule _virtualised_hosts_updates_upd ON public._virtualised_hosts_updates;

CREATE RULE _virtualised_hosts_updates_upd AS
    ON UPDATE TO public._virtualised_hosts_updates
    DO INSTEAD
(UPDATE system_image SET host_system_image_id = new.host_system_image_id
  WHERE (system_image.id = old.system_image_id));

