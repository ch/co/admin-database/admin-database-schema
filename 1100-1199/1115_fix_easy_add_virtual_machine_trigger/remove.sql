CREATE OR REPLACE FUNCTION hotwire3.easy_add_virtual_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE
    ip_id BIGINT;                                                                                        
    new_system_image_id BIGINT;
BEGIN                                                                                                              
    ip_id =  (SELECT ip_address.id
              FROM ip_address
              WHERE ip_address.subnet_id = NEW.subnet_id 
	          AND ip_address.reserved <> true 
	          AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
              LIMIT 1);

    INSERT INTO system_image 
        (wired_mac_1,
         wired_mac_2, hardware_id, 
		operating_system_id, reboot_policy_id, comments, host_system_image_id, 
		user_id, research_group_id) 
    VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.reboot_policy_id, NEW.system_image_comments::varchar(1000), NEW.host_system_image_id, 
		NEW.user_id, NEW.research_group_id)
    RETURNING id INTO new_system_image_id;
    UPDATE ip_address SET hostname = NEW.hostname WHERE id = ip_id;
    INSERT INTO mm_system_image_ip_address
        ( ip_address_id, system_image_id )
    VALUES
        ( ip_id, new_system_image_id);
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.easy_add_virtual_machine()
    OWNER TO dev;

