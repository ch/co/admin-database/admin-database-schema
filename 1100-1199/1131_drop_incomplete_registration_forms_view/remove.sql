CREATE VIEW hotwire3."10_View/People/Incomplete_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form._form_started AS ro_form_started_on,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    post_category_hid.post_category_hid::character varying AS ro_post_category,
    form.nationality_id,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.email::character varying AS email,
    form.crsid::character varying AS crsid,
    form.start_date,
    form.intended_end_date,
    form.department_host_id AS supervisor_id,
    form.submitted AS _personal_details_submitted,
    NULL::boolean AS reopen_personal_details_form,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signer_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_paper_safety_checklist,
    form.safety_submitted AS _safety_submitted,
    NULL::boolean AS reopen_safety_checklist,
    registration.state_of_form(form.uuid) AS ro_reason,
    form.uuid AS form_uuid,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
     LEFT JOIN hotwire3.post_category_hid ON post_category_hid.post_category_id = form.post_category_id
  WHERE registration.state_of_form(form.uuid) <> 'Complete'::text;

ALTER TABLE hotwire3."10_View/People/Incomplete_Registration_Forms"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO student_management;


CREATE FUNCTION hotwire3.incomplete_registration_form_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    -- reopen forms for editing in the registration app
    IF NEW.reopen_personal_details_form THEN
        NEW._personal_details_submitted = FALSE::boolean;
    END IF;
    IF NEW.reopen_safety_checklist THEN
        NEW._safety_submitted = FALSE::boolean;
    END IF;
    UPDATE registration.form SET
    first_names = NEW.first_names,
    surname = NEW.surname,
    known_as = NEW.known_as,
    title_id = NEW.title_id,
    date_of_birth = NEW.date_of_birth,
    gender_id = NEW.gender_id,
    nationality_id = NEW.nationality_id,
    dept_room_id = NEW.room_id,
    dept_telephone_id = NEW.dept_telephone_number_id,
    email = NEW.email::text,
    crsid = NEW.crsid,
    start_date = NEW.start_date,
    intended_end_date = NEW.intended_end_date,
    department_host_id = NEW.supervisor_id,
    submitted = NEW._personal_details_submitted,
    safety_submitted = NEW._safety_submitted
     WHERE form_id = OLD.id;
RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.incomplete_registration_form_upd()
    OWNER TO dev;


CREATE OR REPLACE RULE hw3_incomplete_reg_del AS
    ON DELETE TO hotwire3."10_View/People/Incomplete_Registration_Forms"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.uuid = old.form_uuid));

CREATE TRIGGER incomplete_form_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Incomplete_Registration_Forms"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.incomplete_registration_form_upd();


