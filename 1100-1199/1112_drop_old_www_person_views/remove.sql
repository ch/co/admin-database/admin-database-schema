-- View: www.person_info

-- DROP VIEW www.person_info;

CREATE OR REPLACE VIEW www.person_info
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    research_groups.names AS research_groups,
    research_groups.website_addresses,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  ORDER BY person.crsid;

ALTER TABLE www.person_info
    OWNER TO dev;

GRANT ALL ON TABLE www.person_info TO alt36;
GRANT ALL ON TABLE www.person_info TO www_sites;


-- View: www.academic_staff_v1

-- DROP VIEW www.academic_staff_v1;

CREATE OR REPLACE VIEW www.academic_staff_v1
 AS
 SELECT p.id,
    p.crsid,
    p.email_address,
    p.image_oid,
    p.www_person_hid,
    p.research_groups,
    p.website_addresses,
    p.college_name,
    p.college_website,
    p.telephone_numbers
   FROM www.person_info p
     JOIN apps.www_academic_staff_v2 w USING (id)
  WHERE w.post_category_id = 'sc-1'::text OR w.post_category_id = 'sc-9'::text OR w.counts_as_academic = true;

ALTER TABLE www.academic_staff_v1
    OWNER TO alt36;

GRANT ALL ON TABLE www.academic_staff_v1 TO alt36;
GRANT ALL ON TABLE www.academic_staff_v1 TO dev;
GRANT ALL ON TABLE www.academic_staff_v1 TO www_sites;




-- View: www.person_info_test

-- DROP VIEW www.person_info_test;

CREATE OR REPLACE VIEW www.person_info_test
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS post_category,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig,
    rgs.research_groups,
    rgs.website_addresses,
    post_history.job_title
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN post_history ON post_history.id = _latest_role.role_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
     LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_test
    OWNER TO dev;

GRANT SELECT ON TABLE www.person_info_test TO www_sites;


-- View: www.person_info_v2

-- DROP VIEW www.person_info_v2;

CREATE OR REPLACE VIEW www.person_info_v2
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    research_groups.names AS research_groups,
    research_groups.website_addresses,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  WHERE person.do_not_show_on_website = true
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v2
    OWNER TO dev;

GRANT ALL ON TABLE www.person_info_v2 TO alt36;
GRANT ALL ON TABLE www.person_info_v2 TO www_sites;


-- View: www.person_info_v3

-- DROP VIEW www.person_info_v3;

CREATE OR REPLACE VIEW www.person_info_v3
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    research_groups.names AS research_groups,
    research_groups.website_addresses,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v3
    OWNER TO dev;

GRANT ALL ON TABLE www.person_info_v3 TO alt36;
GRANT ALL ON TABLE www.person_info_v3 TO www_sites;


-- View: www.person_info_v4

-- DROP VIEW www.person_info_v4;

CREATE OR REPLACE VIEW www.person_info_v4
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    research_groups.names AS research_groups,
    research_groups.website_addresses,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v4
    OWNER TO dev;

GRANT ALL ON TABLE www.person_info_v4 TO alt36;
GRANT ALL ON TABLE www.person_info_v4 TO dev;
GRANT ALL ON TABLE www.person_info_v4 TO postgres;
GRANT ALL ON TABLE www.person_info_v4 TO www_sites;

-- View: www.person_info_v5

-- DROP VIEW www.person_info_v5;

CREATE OR REPLACE VIEW www.person_info_v5
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig,
    rgs.research_groups,
    rgs.website_addresses
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v5
    OWNER TO dev;

GRANT ALL ON TABLE www.person_info_v5 TO alt36;
GRANT ALL ON TABLE www.person_info_v5 TO www_sites;


-- View: www.person_info_v6

-- DROP VIEW www.person_info_v6;

CREATE OR REPLACE VIEW www.person_info_v6
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
        CASE
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS post_category,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig,
    rgs.research_groups,
    rgs.website_addresses,
    post_history.job_title,
    website_staff_category_hid.website_staff_category_hid AS website_staff_category
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN post_history ON post_history.id = _latest_role.role_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
     LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v6
    OWNER TO dev;

GRANT SELECT ON TABLE www.person_info_v6 TO www_sites;



-- View: www.person_info_v7

-- DROP VIEW www.person_info_v7;

CREATE OR REPLACE VIEW www.person_info_v7
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
    person.image_lo::bigint AS image_oid,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
    telephone_numbers.telephone_numbers,
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE post_category_hid.post_category_hid
        END AS post_category,
    _latest_role.post_category_id,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig,
    rgs.research_groups,
    rgs.website_addresses,
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN post_history ON post_history.id = _latest_role.role_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
     LEFT JOIN www.post_category_hid ON post_category_hid.post_category_id = _latest_role.post_category_id
     LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE www.person_info_v7
    OWNER TO dev;

GRANT SELECT ON TABLE www.person_info_v7 TO www_sites;


-- View: www.research_group_members

-- DROP VIEW www.research_group_members;

CREATE OR REPLACE VIEW www.research_group_members
 AS
 SELECT research_group.name AS research_group_name,
    p.id AS person_id,
    p.email_address,
    p.www_person_hid,
    p.sortable_name,
    p.telephone_numbers
   FROM research_group
     JOIN mm_person_research_group m ON research_group.id = m.research_group_id
     JOIN www.person_info_v2 p ON p.id = m.person_id
     JOIN _physical_status_v3 physical_status ON p.id = physical_status.person_id
  WHERE research_group.internal_use_only IS FALSE AND physical_status.status_id::text = 'Current'::text
  ORDER BY research_group.name, p.sortable_name;

ALTER TABLE www.research_group_members
    OWNER TO alt36;

GRANT ALL ON TABLE www.research_group_members TO alt36;
GRANT ALL ON TABLE www.research_group_members TO dev;
GRANT ALL ON TABLE www.research_group_members TO postgres;
GRANT ALL ON TABLE www.research_group_members TO www_sites;
