update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add','newval',jsonb_build_array('5','37')),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2'))
) where id=592;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1111","name %"}','sense','set','newval','chitc'),
 jsonb_build_object('path','{"vlans","1111","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1111","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=624;

update switch set config_json=jsonb_build_array(
jsonb_build_object('path','{"vlans",2682,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3510,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3460,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",3116,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",638,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"untagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','A1'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','F5'),
jsonb_build_object('path','{"vlans",615,"tagged %",0}','sense','add','newval','F6'),
jsonb_build_object('path','{"vlans",615,"untagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",615,"name %"}','sense','set','newval','CUDN-Ice'),
jsonb_build_object('path','{"vlans",606,"tagged %",0}','sense','add','newval','F8'),
jsonb_build_object('path','{"vlans",1100,"%",0}','sense','add','newval','ip access-group "switch-vlan" vlan-in'),
jsonb_build_object('path','{"vlans",1110,"%",0}','sense','add','newval','ip igmp'),
jsonb_build_object('path','{"vlans",3310,"%",0}','sense','add','newval','ip igmp'),
jsonb_build_object('path','{"vlans",1199,"tagged %"}','sense','set','newval',json_build_array())
) where id=704;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1103","untagged %","-0"}','sense','add','newval','6'),
 jsonb_build_object('path','{"vlans","1103","name %"}','sense','set','newval','chphoto'),
 jsonb_build_object('path','{"vlans","1103","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2','B1','B2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('6')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('6'))
) where id=632;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1111","name %"}','sense','set','newval','chitc'),
 jsonb_build_object('path','{"vlans","1111","untagged %","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"vlans","1111","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('35','38')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('35','38'))
) where id=646;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1201","name %"}','sense','set','newval','chnet1'),
 jsonb_build_object('path','{"vlans","1201","untagged %","-0"}','sense','add','newval',jsonb_build_array('6')),
 jsonb_build_object('path','{"vlans","1201","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('4')),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('4','6')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('4','6'))
) where id=654;

-- cab26-4
-- TODO: Something is setting vlan1 untagged into 1-48.
update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","no untagged %","-0"}','sense','add','newval',jsonb_build_array('16','18')),
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('16')),
 jsonb_build_object('path','{"vlans","1207","name %"}','sense','set','newval','VLAN1207'),
 jsonb_build_object('path','{"vlans","1207","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2','18')),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','VLAN1102'),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2','18')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('16','18')),
 --jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('18')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('18'))
) where id=657;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add','newval',jsonb_build_array('41','43','45')),
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2'))
) where id=670;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','set','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1113","untagged %","-0"}','sense','add','newval',jsonb_build_array('3','13')),
 jsonb_build_object('path','{"vlans","1113","name %"}','sense','set','newval','VLAN1113'),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('3','8','13')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('3','8','13')),
 jsonb_build_object('path','{"aaa port-access % controlled-direction in","-0"}','sense','add','newval',jsonb_build_array('3','13'))
) where id=673;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1112","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1112","name %"}','sense','set','newval','VLAN1112'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=675;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1112","untagged %","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"vlans","1112","name %"}','sense','set','newval','VLAN1112'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('32')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('32'))
) where id=685;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1119","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1119","untagged %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"vlans","1119","name %"}','sense','set','newval','chubb'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('36')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('36'))
) where id=687;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('20')),
 jsonb_build_object('path','{"aaa port-access authenticator % client-limit 1","-0"}','sense','add','newval',jsonb_build_array('20'))
) where id=694;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('45','47'))
) where id=700;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','IPMI'),
 jsonb_build_object('path','{"vlans","3310","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2','10')),
 jsonb_build_object('path','{"vlans","3310","name %"}','sense','set','newval','cctv'),
 jsonb_build_object('path','{"vlans","1280","untagged %","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"vlans","1280","name %"}','sense','set','newval','stub'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('10')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('10'))
) where id=702;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","no untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('7','8','9','16','28','31','32')
 ),
 jsonb_build_object('path','{"vlans","1","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','21','23','27','29')
 ),
 jsonb_build_object('path','{"vlans","618","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','7','8','9','16','21','23','27','28','29','31','32')
 ),
 jsonb_build_object('path','{"vlans","1100","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','7','8','9','16','21','23','27','28','29','31','32')
 ),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi'),
 jsonb_build_object('path','{"vlans","1102","untagged %","-0"}','sense','add','newval',jsonb_build_array('28','29')),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-27','30-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1115","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','7','8','9','16','21','23','27','28','29','31','32')
 ),
 jsonb_build_object('path','{"vlans","1201","name %"}','sense','set','newval','chnet1'),
 jsonb_build_object('path','{"vlans","1201","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1202","name %"}','sense','set','newval','chnet2'),
 jsonb_build_object('path','{"vlans","1202","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('8','16','23','31-32')
 ),
 jsonb_build_object('path','{"vlans","1202","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-7','9-12','13','14-15','17-22','24-26','28','30','33-48','A1-A4')
 ),
 jsonb_build_object('path','{"vlans","1203","name %"}','sense','set','newval','chnet3'),
 jsonb_build_object('path','{"vlans","1203","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1205","name %"}','sense','set','newval','chnet5'),
 jsonb_build_object('path','{"vlans","1205","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1206","name %"}','sense','set','newval','chnet6'),
 jsonb_build_object('path','{"vlans","1206","untagged %","-0"}','sense','add','newval',jsonb_build_array('7','9')),
 jsonb_build_object('path','{"vlans","1206","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-6','8','10-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1207","name %"}','sense','set','newval','chnet7'),
 jsonb_build_object('path','{"vlans","1207","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1209","name %"}','sense','set','newval','chnet9'),
 jsonb_build_object('path','{"vlans","1209","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1210","name %"}','sense','set','newval','chnet10'),
 jsonb_build_object('path','{"vlans","1210","untagged %","-0"}','sense','add','newval',jsonb_build_array('2','21')),
 jsonb_build_object('path','{"vlans","1210","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','3-20','22-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1211","name %"}','sense','set','newval','chnet11'),
 jsonb_build_object('path','{"vlans","1211","untagged %","-0"}','sense','add','newval',jsonb_build_array('1')),
 jsonb_build_object('path','{"vlans","1211","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('2-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1212","name %"}','sense','set','newval','chnet12'),
 jsonb_build_object('path','{"vlans","1212","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","1217","name %"}','sense','set','newval','chnet113'),
 jsonb_build_object('path','{"vlans","1217","untagged %","-0"}','sense','add','newval',jsonb_build_array('27')),
 jsonb_build_object('path','{"vlans","1217","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-26','28-48','A1','A2','A3','A4')
 ),
 jsonb_build_object('path','{"vlans","2682","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','7','8','9','16','21','23','27','28','29','31','32')
 ),
 jsonb_build_object('path','{"vlans","3116","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1','2','7','8','9','16','21','23','27','28','29','31','32')
 )
) where id=717;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('8')),
 jsonb_build_object('path','{"vlans","1199","%","-0"}','sense','add','newval',jsonb_build_array('jumbo')),
 jsonb_build_object('path','{"vlans","1199","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-7','9-48','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1199","name %"}','sense','set','newval','chrepnet')
) where id=721;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1100","untagged %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1199","%","-0"}','sense','add','newval',jsonb_build_array('jumbo')),
 jsonb_build_object('path','{"vlans","1199","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('1-47','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1199","name %"}','sense','set','newval','chrepnet')
) where id=728;


update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3510","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","3510","untagged %","-0"}','sense','add','newval',jsonb_build_array('41')),
 jsonb_build_object('path','{"vlans","3510","name %"}','sense','set','newval','VLAN3510')
) where id=738;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","1","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 jsonb_build_object('path','{"vlans","618","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 jsonb_build_object('path','{"vlans","1100","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 jsonb_build_object('path','{"vlans","1102","untagged %","-0"}','sense','add','newval',jsonb_build_array('40')),
 jsonb_build_object('path','{"vlans","1102","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-39','41-48','A1','A2')
 ),
 jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi'),
 jsonb_build_object('path','{"vlans","1115","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 jsonb_build_object('path','{"vlans","1209","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-48','A1','A2')
 ),
 jsonb_build_object('path','{"vlans","1209","name %"}','sense','set','newval','chnet9'),
 jsonb_build_object('path','{"vlans","1212","untagged %","-0"}','sense','add','newval',jsonb_build_array('44')),
 jsonb_build_object('path','{"vlans","1212","tagged %","-0"}','sense','add','newval',
  jsonb_build_array('1-43','45-48','A1-A2')
 ),
 jsonb_build_object('path','{"vlans","1212","name %"}','sense','set','newval','chnet12'),
 jsonb_build_object('path','{"vlans","2682","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 ),
 jsonb_build_object('path','{"vlans","3116","tagged %","-0"}','sense','add',
  'newval',jsonb_build_array('20','40','44')
 )
) where id=742;

-- Cab57-26
update switch set config_json=jsonb_build_array(
 --jsonb_build_object('path','{"vlans","1102","name %"}','sense','set','newval','chipmi'),
 jsonb_build_object('path','{"vlans","3460","untagged %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24')
 ),
 jsonb_build_object('path','{"vlans","1117","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"vlans","1117","untagged %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"vlans","1117","name %"}','sense','set','newval','wren-ext'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('48')),
 jsonb_build_object('path','{"interfaces","48","%","-0"}','sense','add','newval','unknown-vlans block')
) where id=802;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"vlans","3460","name %"}','sense','set','newval','VLAN3460'),
 jsonb_build_object('path','{"vlans","3460","untagged %","_0"}','sense','add',
   'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 ),
 jsonb_build_object('path','{"vlans","3460","tagged %","-0"}','sense','add','newval',jsonb_build_array('A1','A2')),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 ),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add',
  'newval',jsonb_build_array('2','4','6','8','10','12','14','16','18','20','22','24','26','28','30','32')
 )
) where id=810;

update switch set config_json=jsonb_build_array(
 jsonb_build_object('path','{"interfaces","42","%","-0"}','sense','add','newval','unknown-vlans block'),
 jsonb_build_object('path','{"interfaces","44","%","-0"}','sense','add','newval','unknown-vlans block'),
 jsonb_build_object('path','{"loop-protect %","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % admin-edge-port","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % bpdu-filter","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % root-guard","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"spanning-tree % tcn-guard","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"vlans","1117","untagged %","-0"}','sense','add','newval',jsonb_build_array('42','44')),
 jsonb_build_object('path','{"vlans","1117","name %"}','sense','set','newval','wren-ext')
) where id=816;


CREATE OR REPLACE VIEW public._switch_port_cfg_json AS
SELECT c.switch_id,
    jsonb_agg(replace(c.config_json::text, '%PORT%'::text, c.portlist::text)::jsonb) AS config_json,
    array_to_string(array_agg(distinct c.id), ','::text) AS id
   FROM (
SELECT a.portlist,
            jsonb_array_elements(switch_port_config_fragment.config_json) AS config_json,
            switch_port_config_fragment.id,
            a.switch_id
           FROM ( 

SELECT switchport.name AS portlist,
                    array_agg(switch_port_config_goal_id) FILTER (where switch_port_config_goal_id is not
 null)||switch_auth_id::bigint as switch_port_config_goal_id,
                    switch.switch_model_id,
                    switchport.switch_id
                   FROM switchport
                     JOIN switch ON switchport.switch_id = switch.id left join mm_switch_config_switch_port on switchport.id=switch_port_id group by switch_model_id, switchport.id

) a
             JOIN switch_port_config_fragment ON switch_port_config_fragment.switch_port_config_goal_id=any(a.switch_port_config_goal_id)
             JOIN mm_switch_port_config_fragment_switch_model ON mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id
          WHERE mm_switch_port_config_fragment_switch_model.switch_model_id = a.switch_model_id AND switch_port_config_fragment.config_json IS NOT NULL) c
  GROUP BY c.switch_id;
create or replace function _switch_vlan_formatter(_txt text,_vlans jsonb,_depth int) 
 RETURNS setof text
 LANGUAGE plpgsql
AS $function$
DECLARE
 _rowi record;
 formatters jsonb=jsonb_build_object(
   '%','_switch_cfg_array_join',
   'ips %','_switch_cfg_formatter_iplist',
   'untagged %','_switch_portlist',
   'no untagged %','_switch_portlist',
   'tagged %','_switch_portlist',
   'name %','_switch_cfg_formatter_string',
   'ip igmp blocked %','_switch_portlist',
   'vrrp','_switch_vrrps_formatter'
  );
BEGIN
    return next repeat(' ',_depth)||'vlan '||_txt;
    -- key='vrrp' places this last
    for _rowi in select key,value from jsonb_each(_vlans) order by key='vrrp' loop
        if formatters?(_rowi.key) then
           return query execute 'select a from (select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,'||(_depth+2)||') a ) b where a is not null';
        else
           raise exception 'No formatter specified for %',(_rowi.key);
        end if;
    end loop;
    return next repeat(' ',_depth+2)||'exit';
END
$function$;
alter function _switch_vlan_formatter(text,jsonb,int) owner to dev;
CREATE OR REPLACE FUNCTION public._switch_config_b3(v_switch_id bigint)
 RETURNS SETOF text
 LANGUAGE plpgsql
AS $function$
declare
 r record;
 s record;
 v record;
 i integer;
 sql varchar;
 conf varchar;
 pconf varchar[];
 defvlan integer :=1;
 earr jsonb:=jsonb_build_array();
 formatters jsonb:=jsonb_build_object(
   '%','_switch_cfg_array_join',
   'aaa port-access authenticator %','_switch_portlist',
   'aaa port-access % controlled-direction in','_switch_portlist',
   'aaa port-access authenticator % client-limit 1','_switch_portlist',
   'aaa port-access mac-based %','_switch_portlist',
   'spanning-tree % admin-edge-port','_switch_portlist',
   'spanning-tree % bpdu-filter','_switch_portlist',
   'spanning-tree % bpdu-protection','_switch_portlist',
   'spanning-tree % root-guard','_switch_portlist',
   'spanning-tree % tcn-guard','_switch_portlist',
   'loop-protect %','_switch_portlist',
   'loop-protect % receiver-action no-disable','_switch_portlist',
   'no snmp-server enable traps link-change %','_switch_portlist',
   'vlans','_switch_vlans_formatter',
   'interfaces','_switch_interfaces_formatter');
 cfg jsonb:=jsonb_build_object(
    '%',jsonb_build_array(),
    'aaa port-access authenticator %',earr,
    'aaa port-access % controlled-direction in',earr,
    'aaa port-access authenticator % client-limit 1',earr,
    'aaa port-access mac-based %',earr,
    'spanning-tree % admin-edge-port',earr,
    'spanning-tree % bpdu-protection',earr,
    'spanning-tree % bpdu-filter',earr,
    'spanning-tree % root-guard',earr,
    'spanning-tree % tcn-guard',earr,
    'loop-protect %',earr,
    'loop-protect % receiver-action no-disable',earr,
    'no snmp-server enable traps link-change %',earr,
    'vlans',jsonb_build_object(
        '_template',jsonb_build_object(
            'ips %',earr,
            'untagged %',earr,
            'no untagged %',earr,
            'tagged %',earr,
            'name %',null,
            'ip igmp blocked %',earr,
            '%',jsonb_build_array(),
            --'interfaces',jsonb_build_object(
                --'_template',jsonb_build_object(
                    --'%',earr
                --)
            --),
            'vrrp',jsonb_build_object(
                '_template',jsonb_build_object(
                    '%',earr
                )
            )
        )
    ),
    'interfaces',json_build_object(
        '_template',jsonb_build_object(
            'name %',null,
            '%',earr
        )
    )
 );
 vlanrow record;
 vidrow record;
 _rowi record; -- For loops
 trow record; -- To handle loops
 vlans jsonb;
 emptyvrrp jsonb;
 emptyvlan jsonb;
 _everyvid integer[];
begin
 -- Facts, immutable
 select array_agg(vid order by vid) from vlan where vid!=1 into _everyvid;
 -- Basic stuff about the switch
 
 select switch.id, switch_model.model, switch_model.id as model_id, hardware.name as shortname, 
 array(select name from switchport where switch_id=switch.id order by id asc) as allports, 
 is_repnet,
hardware.name as hostname, wired_mac_1, system_image.id, switch.id, serial_number,  room.name as room, 
  array(
      select config from (
          select distinct config,priority from (
              select E'\n; swf '||switch_config_fragment.id||E'\n'||replace(config,E'\r','') config, priority 
			   from mm_switch_config_fragment_switch_model 
			   inner join switch_config_fragment on switch_config_fragment_id=switch_config_fragment.id 
			   inner join mm_switch_goal_applies_to_switch using (switch_config_goal_id) 
			   where switch_id=switch.id and switch_model_id=switch_model.id and not switch_config_fragment.ignore 
			   and switch_config_fragment.config_json is null
			   order by priority asc nulls last) b order by priority) c) as allconf,
case when config_json is not null then '' else extraconfig end as extraconfig
from switch inner join switch_model on switch_model.id=switch_model_id inner join 
hardware on hardware_id=hardware.id inner join system_image on system_image.hardware_id=hardware.id 
left join room on room.id=room_id where switch.id=v_switch_id into r;
 return next '; '||r.model||' Configuration Editor; Created on release X.XX' ;
 return next '; Switch config generated by _switch_config_C for switch ID '||r.id;
 -- return next '; Generated at '||now();
 return next '; serial number '||r.serial_number;
 return next '; model number '||r.model;
 -- return next array_to_string(r.allconf,E'\n!!');
 
    return next ';Section 1';
    if (array_lower(r.allconf,1) is not null) then
        for i in array_lower(r.allconf,1)..array_upper(r.allconf,1) 
		loop
            conf=r.allconf[i];
            conf=replace(
                  replace(
                    conf,
                    '%LOCATION%',r.room),
                    '%HOSTNAME%',r.hostname);
            case 
            when (conf LIKE E'%\\%EVERYVLAN\\%%') THEN
                return query select replace(conf,'%EVERYVLAN%',vid::varchar) from vlan;
            when (conf LIKE E'%\\%ALLPORTS\\%%') THEN
                return next replace(conf,'%ALLPORTS%',procurve_collapse_array(r.allports));
            when (conf like E'%\\%EACHVLANNAME\\%%') THEN
                return query select 
                    replace(
                      replace(
                        replace(
                          replace(conf,'%EACHVLANNAME%',name),
                        '%EACHVLANVID%',vid::varchar),
                      '%EACHVLANDESC%',description),
                    E'\r','') from vlan;
            when (conf like E'%!SQLEXEC%') THEN
                sql=replace(split_part(conf,'!SQLEXEC',2),'%SWITCHID%',r.id::varchar);
				return next '; SQL '||conf;
                return query execute sql;
            ELSE
                return next conf;
            end case;
        end loop;
    end if;


 
 return next ';Section 2';

return next ';Section 3';
return query select case when cconfig LIKE E'%\\%EVERYVLAN\\%%' then
  array_to_string(array(select replace(cconfig,'%EVERYVLAN%',vid::varchar) from vlan where vid!=1),E'\r') 
 else 
  --';'||b.id||' '||b.switch_port_config_fragment_id||E'\r'||
  cconfig 
end
from 

(
 select 
  a.*,
  switch_port_config_fragment_id,
  replace(replace(replace(switch_port_config_fragment.config,'%PORT%',a.name),'%PORTNAME%',coalesce(a.ifacename,a.name)),E'\r','') cconfig 
 from 
(
 select 
  switchport.*, 
  switch_model_id, 
  array(select switch_port_config_goal_id from mm_switch_config_switch_port where switch_port_id=switchport.id) ||
  case when ifacename is not null and ifacename != '' then 12::bigint end||
  --switch_auth_id::bigint || 
  speed_id::bigint 
goals from switchport inner join switch on switch_id=switch.id where switch_id=r.id ) a inner join switch_port_config_goal on switch_port_config_goal.id=any(a.goals) inner join mm_switch_port_config_fragment_switch_model 
	using (switch_model_id) inner join switch_port_config_fragment on switch_port_config_fragment_id=switch_port_config_fragment.id where switch_port_config_fragment.config_json is null and switch_port_config_goal.id=switch_port_config_goal_id order by a.name
) b
;

return next ';Section 4';
--return next cfg->'vlans';
    -- Look for json configuration applying to this switch
	for _rowi in
	select id,jsonb_array_elements(_switch_expand_sql(config_json,v_switch_id)) config_json from _applied_switch_config_fragments where config_json is not null and switch_id=v_switch_id
    loop
        -- for debug return next vlanrow.config_json;
  	    case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    end loop;


 return next '; Section 4b - per-port, non-json';
 --return next cfg->'vlans';
return query select '; spf'||switch_port_config_fragment.id||E'\n'||_switch_expand_tokens(replace(replace(config,E'\r',''),'%PORT%',portlist),v_switch_id) as config from ( 

select
  procurve_collapse_array(array_agg(switchport.name)) as portlist,
  switch_auth_id,
  switch_model_id
from
  switchport
  inner join switch on switch_id = switch.id
where
  switch_id = v_switch_id
group by
  switch_auth_id,
  switch_model_id
) a
inner join switch_port_config_fragment on switch_auth_id = switch_port_config_goal_id
inner join mm_switch_port_config_fragment_switch_model on switch_port_config_fragment_id = switch_port_config_fragment.id
where
  mm_switch_port_config_fragment_switch_model.switch_model_id = a.switch_model_id
  and config_json is null;
  
  return next ';Section 5';
--   return query select _switch_expand_tokens(replace(replace(config,E'\r',''),'%PORT%',portlist),v_switch_id) as config from ( select procurve_collapse_array(array_agg(name)) as portlist,switch_auth_id from switchport where switch_id=v_switch_id group by switch_auth_id) a inner join switch_port_config_fragment on switch_auth_id=switch_port_config_goal_id;
  return next '; Extra config';
  return next replace(r.extraconfig,E'\r','');
  return next ';Section 6';
  
   -- Loop over per-port JSON configuration as required
    for _rowi in 
        select jsonb_array_elements(_switch_expand_sql(config_json,switch_id)) config_json from _switch_port_cfg_json where config_json is not null and switch_id=v_switch_id
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    END LOOP;
return next cfg->'loop-protect%';
    return next ';Section 7';
    --return next cfg->'vlans';
	
	-- Expand %EVERYVLAN% configuration into other VLANs
    if cfg->'vlans'?'%EVERYVLAN%' then
      foreach i in array _everyvid loop
        -- return next (vlans)::text;
	if cfg->'vlans'->'%EVERYVLAN%'->'tagged %' != cfg->'vlans'->'_template'->'tagged %' then
            -- Tagged is initially an empty array, so we concatenate it with the new one
            cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>('{"vlans","'||i||'","tagged %"}')::text[],_value=>(cfg->'vlans'->(i::text)->'tagged %')||(cfg->'vlans'->'%EVERYVLAN%'->'tagged %'),_sense=>'set');
        end if;
        if cfg->'vlans'->'%EVERYVLAN%'->'untagged %' != cfg->'vlans'->'_template'->'untagged %' then
            -- Unagged is initially an empty array, so we concatenate it with the new one
            cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>('{"vlans","'||i||'","untagged %"}')::text[],_value=>(cfg->'vlans'->(i::text)->'untagged %')||(cfg->'vlans'->'%EVERYVLAN%'->'untagged %'),_sense=>'set');
        end if;
      end loop;
    end if;
    -- Switch configuration overrides everything else
    for _rowi in 
        select id,jsonb_array_elements(_switch_expand_sql(config_json,v_switch_id)) config_json from switch where id=v_switch_id and config_json is not null
    loop
        case coalesce((_rowi.config_json ->> 'type'),'')
            when 'vlan' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>array_prepend('vlans',(_rowi.config_json->>'path')::text[]),_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
            when '' then
			    cfg=_switch_cfg_set(_oldcfg=>cfg,_path=>(_rowi.config_json->>'path')::text[],_value=>(_rowi.config_json->'newval'),_sense=>(_rowi.config_json->>'sense'));
	        else
	    end case;
    end loop;
        -- Start returning information. Flags first
        for _rowi in select key,value from jsonb_each(cfg) loop
            if formatters?(_rowi.key) then
               return query execute 'select '||(formatters->>(_rowi.key))||'($TXT$'||(_rowi.key)||'$TXT$,$JSON$'||(_rowi.value)||'$JSON$,0)';
            else
               raise exception 'No formatter specified for %',(_rowi.key);
            end if;
        end loop;
end

$function$
