-- View: hotwire3.10_View/Network/Switches/_switch_config_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_switch_config_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switch_config_sv" AS
SELECT
    switch.id,
    'Config for switch - click here to show'::character varying AS "Link to config"
FROM
    switch;

ALTER TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switch_config_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/_switches_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_switches_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switches_sv" AS
SELECT
  switch.id,
  switchport.id AS switch_port_id,
  hardware.name AS switch_name,
  hardware.manufacturer,
  switch_model_hid.switch_model_hid,
  room_hid.room_hid
FROM
  switchport
  LEFT JOIN switch ON switchport.switch_id = switch.id
  JOIN hardware ON switch.hardware_id = hardware.id
  LEFT JOIN hotwire3.room_hid USING (room_id)
  LEFT JOIN hotwire3.switch_model_hid USING (switch_model_id);

ALTER TABLE hotwire3."10_View/Network/Switches/_switches_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switches_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switches_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/_Switchport_sv
-- DROP VIEW hotwire3."10_View/Network/Switches/_Switchport_sv";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_Switchport_sv" AS
SELECT
    switchport.id,
    switchport.switch_id,
    switchport.name,
    socket_hid.socket_hid AS socket,
    switchport.ifacename,
    switch_auth_hid.switch_auth_hid,
    speed_hid.speed_hid
FROM
    switchport
    LEFT JOIN hotwire3.switch_auth_hid USING (switch_auth_id)
    LEFT JOIN hotwire3.speed_hid USING (speed_id)
    LEFT JOIN hotwire3.socket_hid USING (socket_id)
ORDER BY
    (
        CASE WHEN switchport.name::text ~ '^[A-Za-z]+'::text THEN
            10 * ascii(substr(switchport.name::text, 1, 1)) + regexp_replace(switchport.name::text, '[A-Za-z]+'::text, ''::text)::integer
        ELSE
            switchport.name::integer
        END);

ALTER TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_Switchport_sv" TO dev;

-- View: hotwire3.10_View/Network/Switches/Switches
-- DROP VIEW hotwire3."10_View/Network/Switches/05_Switches";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/05_Switches" AS
SELECT
  switch.id,
  hardware.name,
  hardware.id AS _hardware_id,
  system_image.id AS _system_image_id,
  hardware.manufacturer,
  hardware.hardware_type_id,
  system_image.wired_mac_1,
  switch.switch_model_id,
  switch.ignore_config,
  ARRAY (
    SELECT
      mm_system_image_ip_address.ip_address_id
    FROM
      mm_system_image_ip_address
    WHERE
      mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
  hardware.serial_number,
  hardware.asset_tag,
  hardware.date_purchased,
  hardware.date_configured,
  hardware.warranty_end_date,
  hardware.date_decommissioned,
  hardware.warranty_details,
  hardware.room_id,
  switch.cabinet_id,
  hardware.owner_id,
  system_image.research_group_id,
  hardware.comments,
  ARRAY (
    SELECT
      mm_switch_goal_applies_to_switch.switch_config_goal_id
    FROM
      mm_switch_goal_applies_to_switch
    WHERE
      mm_switch_goal_applies_to_switch.switch_id = switch.id) AS switch_config_goal_id,
  switch.config_json,
  hotwire3.to_hwsubviewb('10_View/Network/_Cabinet_for_switch'::character varying, 'switch_id'::character varying, '10_View/Network/Cabinets'::character varying, 'cabinet_id'::character varying, 'id'::character varying) AS "Cabinet_details",
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_Switchport_sv'::character varying, 'switch_id'::character varying, '10_View/Network/Switches/20_Switch_Ports'::character varying, NULL::character varying, NULL::character varying) AS "Ports",
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_switch_config_sv'::character varying, 'id'::character varying, '10_View/Network/Switches/20_Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config"
FROM
  switch
  JOIN hardware ON switch.hardware_id = hardware.id
  JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Network/Switches/05_Switches" OWNER TO dev;

GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/05_Switches" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/05_Switches" TO dev;

-- Rule: hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/05_Switches"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/05_Switches";
CREATE RULE hotwire3_view_network_switch_del AS ON DELETE TO hotwire3."10_View/Network/Switches/05_Switches"
  DO INSTEAD
  ( DELETE FROM switch
    WHERE (switch.id = OLD.id));

-- Rule: hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/05_Switches"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/05_Switches";
CREATE RULE hotwire3_view_network_switch_upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/05_Switches"
    DO INSTEAD
    ( UPDATE
        hardware SET
        name = NEW.name,
        manufacturer = NEW.manufacturer,
        hardware_type_id = NEW.hardware_type_id,
        serial_number = NEW.serial_number,
        asset_tag = NEW.asset_tag,
        date_purchased = NEW.date_purchased,
        date_configured = NEW.date_configured,
        warranty_end_date = NEW.warranty_end_date,
        date_decommissioned = NEW.date_decommissioned,
        warranty_details = NEW.warranty_details,
        room_id = NEW.room_id,
        owner_id = NEW.owner_id,
        comments = NEW.comments WHERE (hardware.id = OLD._hardware_id);

UPDATE
  system_image
SET
  research_group_id = new.research_group_id,
  wired_mac_1 = new.wired_mac_1
WHERE (system_image.id = old._system_image_id);

SELECT
  fn_mm_array_update((new.switch_config_goal_id)::bigint[],(old.switch_config_goal_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;

SELECT
  fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update;

UPDATE
  switch
SET
  ignore_config = new.ignore_config,
  switch_model_id = new.switch_model_id,
  config_json = new.config_json,
  cabinet_id = new.cabinet_id
WHERE (switch.id = old.id);

);

-- View: hotwire3.10_View/Network/Switches/Switch_Configs_ro
-- DROP VIEW hotwire3."10_View/Network/Switches/20_Switch_Configs_ro";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" AS
SELECT
    switch.id,
    switch.id AS switch_id,
    array_to_string(ARRAY (
            SELECT
                swcnf.switch_config_d(switch.id) AS _switch_config), '
'::text) AS ro_config
FROM
    switch;

ALTER TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Configs_ro" TO dev;

-- View: hotwire3.10_View/Network/Switches/20_Switch_Ports
-- DROP VIEW hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/20_Switch_Ports" AS
SELECT
  switchport.id,
  switchport.name,
  switchport.socket_id,
  switchport.switch_id,
  switchport.ifacename,
  switchport.disabled,
  switchport.speed_id,
  switchport.switch_auth_id,
  ARRAY (
    SELECT
      mm_switch_config_switch_port.switch_port_config_goal_id
    FROM
      mm_switch_config_switch_port
    WHERE
      mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id,
  hotwire3.to_hwsubviewb('10_View/Network/Switches/_switches_sv'::character varying, 'switch_port_id'::character varying, '10_View/Network/Switches/05_Switches'::character varying, NULL::character varying, NULL::character varying) AS switch
FROM
  switchport;

ALTER TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/20_Switch_Ports" TO dev;

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_del" AS ON DELETE TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
  DO INSTEAD
  ( DELETE FROM switchport
    WHERE (switchport.id = OLD.id));

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_ins" AS ON INSERT TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
  DO INSTEAD
  (INSERT INTO switchport(name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id)
    VALUES (NEW.name, NEW.socket_id, NEW.switch_id, NEW.ifacename, NEW.disabled, NEW.speed_id, NEW.switch_auth_id)
  RETURNING
    switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY (
      SELECT
        mm_switch_config_switch_port.switch_port_config_goal_id FROM
        mm_switch_config_switch_port WHERE (mm_switch_config_switch_port.switch_port_id = switchport.id)) AS switch_port_goal_id,
    NULL::_hwsubviewb AS _hwsubviewb);

-- Rule: "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/20_Switch_Ports"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/20_Switch_Ports";
CREATE RULE "hotwire3_10_View/Network/Switches/20_Switch_Ports_upd" AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/20_Switch_Ports"
    DO INSTEAD
    ( UPDATE
        switchport SET
        id = NEW.id,
        name = NEW.name,
        socket_id = NEW.socket_id,
        switch_id = NEW.switch_id,
        ifacename = NEW.ifacename,
        disabled = NEW.disabled,
        speed_id = NEW.speed_id,
        switch_auth_id = NEW.switch_auth_id WHERE (switchport.id = OLD.id);

SELECT
  fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;

);

-- View: hotwire3.10_View/Network/Switches/Switch_Config_Goals
-- DROP VIEW hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" AS
SELECT
    switch_config_goal.id,
    switch_config_goal.name,
    ARRAY (
        SELECT
            mm_switch_goal_applies_to_switch.switch_id
        FROM
            mm_switch_goal_applies_to_switch
        WHERE
            mm_switch_goal_applies_to_switch.switch_config_goal_id = switch_config_goal.id) AS switch_id
FROM
    switch_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Config_Goals" TO dev;

-- Rule: hotwire3_view_network_switch_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE RULE hotwire3_view_network_switch_config_goal_del AS ON DELETE TO hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
    DO INSTEAD
    ( DELETE FROM switch_config_goal
        WHERE (switch_config_goal.id = OLD.id));

-- Rule: hotwire3_view_network_switch_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals";
CREATE RULE hotwire3_view_network_switch_config_goal_upd AS ON UPDATE
    TO hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
        DO INSTEAD
        ( UPDATE
                switch_config_goal SET
                name = NEW.name WHERE (switch_config_goal.id = OLD.id);

SELECT
    fn_mm_array_update((new.switch_id)::bigint[],(old.switch_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_config_goal_id'::character varying, 'switch_id'::character varying,(old.id)::bigint) AS fn_mm_array_update2;

);

CREATE TRIGGER hotwire3_view_network_switch_config_goal_ins
    INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/30_Switch_Config_Goals"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.switch_config_goals_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Port_Config_Goals
-- DROP VIEW hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" AS
SELECT
  switch_port_config_goal.id,
  switch_port_config_goal.name,
  switch_port_config_goal.goal_class AS goal_class_id
FROM
  switch_port_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals" TO dev;

-- Rule: hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_del AS ON DELETE TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
  DO INSTEAD
  ( DELETE FROM switch_port_config_goal
    WHERE (switch_port_config_goal.id = OLD.id));

-- Rule: hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_ins AS ON INSERT TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
  DO INSTEAD
  (INSERT INTO switch_port_config_goal(name, goal_class)
    VALUES (NEW.name, NEW.goal_class_id)
  RETURNING
    switch_port_config_goal.id, switch_port_config_goal.name, switch_port_config_goal.goal_class);

-- Rule: hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals";
CREATE RULE hotwire3_view_network_switch_port_config_goal_upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/30_Switch_Port_Config_Goals"
    DO INSTEAD
    ( UPDATE
        switch_port_config_goal SET
        name = NEW.name,
        goal_class = NEW.goal_class_id WHERE (switch_port_config_goal.id = OLD.id));

-- View: hotwire3.10_View/Network/Switches/Switch_Config_Fragments
-- DROP VIEW hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" AS
SELECT
  switch_config_fragment.id,
  switch_config_fragment.name,
  switch_config_fragment.config_json,
  switch_config_fragment.switch_config_goal_id,
  ARRAY (
    SELECT
      mm_switch_config_fragment_switch_model.switch_model_id
    FROM
      mm_switch_config_fragment_switch_model
    WHERE
      mm_switch_config_fragment_switch_model.switch_config_fragment_id = switch_config_fragment.id) AS switch_model_id
FROM
  switch_config_fragment
ORDER BY
  switch_config_fragment.name;

ALTER TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments" TO dev;

-- Rule: del ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
-- DROP Rule IF EXISTS del ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE RULE del AS ON DELETE TO hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
  DO INSTEAD
  ( DELETE FROM switch_config_fragment
    WHERE (switch_config_fragment.id = OLD.id));

-- Rule: upd ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
-- DROP Rule IF EXISTS upd ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments";
CREATE RULE upd AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
    DO INSTEAD
    ( UPDATE
        switch_config_fragment SET
        name = NEW.name,
        config_json = NEW.config_json,
        switch_config_goal_id = NEW.switch_config_goal_id WHERE (switch_config_fragment.id = OLD.id);

SELECT
  fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id) AS fn_mm_array_update;

UPDATE
  switch_config_fragment
SET
  id = switch_config_fragment.id
WHERE
  FALSE
RETURNING
  new.id,
  new.name,
  new.config_json,
  new.switch_config_goal_id,
  new.switch_model_id;

);

CREATE TRIGGER switch_config_fragments_ins
  INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/70_Switch_Config_Fragments"
  FOR EACH ROW
  EXECUTE FUNCTION hotwire3.switch_config_fragments_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Port_Config_Fragments
-- DROP VIEW hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" AS
SELECT
    switch_port_config_fragment.id,
    switch_port_config_fragment.switch_port_config_goal_id,
    switch_port_config_fragment.config_json,
    ARRAY (
        SELECT
            mm_switch_port_config_fragment_switch_model.switch_model_id
        FROM
            mm_switch_port_config_fragment_switch_model
        WHERE
            mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id) AS switch_model_id
FROM
    switch_port_config_fragment;

ALTER TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments" TO dev;

-- Rule: hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE RULE hotwire3_view_network_switch_port_config_fragment_del AS ON DELETE TO hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
    DO INSTEAD
    ( DELETE FROM switch_port_config_fragment
        WHERE (switch_port_config_fragment.id = OLD.id));

-- Rule: hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments";
CREATE RULE hotwire3_view_network_switch_port_config_fragment_upd AS ON UPDATE
    TO hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
        DO INSTEAD
        ( UPDATE
                switch_port_config_fragment SET
                switch_port_config_goal_id = NEW.switch_port_config_goal_id,
                config_json = NEW.config_json WHERE (switch_port_config_fragment.id = OLD.id);

SELECT
    fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying,(old.id)::bigint) AS fn_mm_array_update2;

);

CREATE TRIGGER hotwire3_view_network_switch_port_config_fragment_ins
    INSTEAD OF INSERT ON hotwire3."10_View/Network/Switches/70_Switch_Port_Config_Fragments"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.network_switch_port_config_fragment_ins();

-- View: hotwire3.10_View/Network/Switches/Switch_Models
-- DROP VIEW hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/80_Switch_Models" AS
SELECT
  switch_model.id,
  switch_model.model,
  switch_model.description,
  switch_model.ports,
  switch_model.uplink_ports
FROM
  switch_model;

ALTER TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/80_Switch_Models" TO dev;

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_del" AS ON DELETE TO hotwire3."10_View/Network/Switches/80_Switch_Models"
  DO INSTEAD
  ( DELETE FROM switch_model
    WHERE (switch_model.id = OLD.id));

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_ins" AS ON INSERT TO hotwire3."10_View/Network/Switches/80_Switch_Models"
  DO INSTEAD
  (INSERT INTO switch_model(model, description, ports, uplink_ports)
    VALUES (NEW.model, NEW.description, NEW.ports, NEW.uplink_ports)
  RETURNING
    switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports);

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/80_Switch_Models"
-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/80_Switch_Models";
CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_upd" AS ON UPDATE
  TO hotwire3."10_View/Network/Switches/80_Switch_Models"
    DO INSTEAD
    ( UPDATE
        switch_model SET
        id = NEW.id,
        model = NEW.model,
        description = NEW.description,
        ports = NEW.ports,
        uplink_ports = NEW.uplink_ports WHERE (switch_model.id = OLD.id));

