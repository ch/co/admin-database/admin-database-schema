SET ROLE dev;
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = 'lab_managers') THEN
      CREATE ROLE lab_managers NOLOGIN;
      GRANT ro_hid,rg_admins TO lab_managers;
   END IF;
END
$do$;

