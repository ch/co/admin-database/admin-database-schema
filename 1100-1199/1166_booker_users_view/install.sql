CREATE VIEW apps.booker_users AS
    SELECT
        crsid,
        email_address,
        COALESCE(known_as,first_names) AS first_names,
        surname,
        CASE
            WHEN hr_role.role_tablename = 'postgraduate_studentship' THEN 'Student'
            WHEN hr_role.role_tablename = 'part_iii_studentship' THEN 'Student'
            WHEN hr_role.role_tablename = 'erasmus_socrates_studentship' THEN 'Student'
            -- Checks both the name and primary key for student visitor roles so we can rename the roles, but retain the readability here
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Summer Student' OR hr_role.post_category_id = 'v-5' ) THEN 'Student'
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Summer Student (paid)' OR hr_role.post_category_id = 'v-16' ) THEN 'Student'
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Visitor (student)' OR hr_role.post_category_id = 'v-13' ) THEN 'Student'
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Visitor (PhD Postgraduate)' OR hr_role.post_category_id = 'v-2' ) THEN 'Student'
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Visitor (MPhil Postgraduate)' OR hr_role.post_category_id = 'v-3' ) THEN 'Student'
            WHEN hr_role.role_tablename = 'visitorship' AND (hr_role.post_category = 'Visitor (Undergraduate)' OR hr_role.post_category_id = 'v-6' ) THEN 'Student'
            ELSE 'Staff'
        END AS booker_type
    FROM person
    JOIN _current_hr_role_for_person hr_role ON person.id = hr_role.person_id
    WHERE person.crsid IS NOT NULL
    AND person.email_address IS NOT NULL
    AND hr_role.predicted_role_status_id = 'Current'
    AND hr_role.post_category_id <> 'v-17'
    ;

ALTER VIEW apps.booker_users OWNER TO dev;
-- GRANT SELECT ON apps.booker TO booker_user_maintenance;
