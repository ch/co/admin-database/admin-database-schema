DROP VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads";
CREATE VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads" AS
    SELECT 
        row_number() over() AS id,
        COALESCE((COALESCE(p.known_as,p.first_names) || ' '),'')|| p.surname AS name,
        p.email_address AS email,
        upper(to_hex(c.mifare_number)) AS "RFID"
    FROM public.person p
    JOIN public.university_card c ON lower(p.crsid) = lower(c.crsid)
    JOIN public._hr_role r on r.person_id = p.id
    WHERE r.predicted_role_status_id = 'Current'
    AND c.expires_at > current_timestamp
    AND 
        (
        r.role_tablename = 'postgraduate_studentship'
        OR
        r.role_tablename = 'erasmus_socrates_studentship'
        OR
        (
            r.role_tablename = 'visitorship'
            AND 
            (
                ( r.post_category_id = 'v-3' OR r.post_category = 'Visitor (MPhil Postgraduate)')
                OR
                ( r.post_category_id = 'v-2' OR r.post_category = 'Visitor (PhD Postgraduate)')
            )
        )
    )
;

ALTER VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/Sign_In_System/Current_Postgrads" TO cos, student_management;

DROP VIEW hotwire3."10_View/Sign_In_System/Current_University_Cards";
CREATE VIEW hotwire3."10_View/Sign_In_System/Current_University_Cards" AS
    SELECT
        row_number() over() AS id,
        COALESCE(
            COALESCE((COALESCE(p.known_as,p.first_names) || ' '),'')|| p.surname,
            n.lookup_visible_name,
            c.full_name,
            c.crsid
        ) AS name,
        COALESCE(p.email_address, n.lookup_email) AS email,
        upper(to_hex(c.mifare_number)) AS "RFID"
    FROM public.university_card c
    LEFT JOIN public.person p ON lower(p.crsid) = lower(c.crsid)
    LEFT JOIN public.university_cardholder_detail n ON n.crsid = lower(c.crsid)  -- n.crsid is constrained to be lowercase
    WHERE c.expires_at > current_timestamp
    AND (
        c.crsid IS NOT NULL
        OR
        c.full_name IS NOT NULL
    )
    ;

ALTER VIEW  hotwire3."10_View/Sign_In_System/Current_University_Cards" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/Sign_In_System/Current_University_Cards" TO cos, reception;
