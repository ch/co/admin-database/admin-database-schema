CREATE VIEW registration.contact_preference_hid AS
SELECT
    contact_preference.contact_preference_id AS id,
    CASE
        WHEN contact_preference.preference = 'University phone' THEN 'Department Phone'
        WHEN contact_preference.preference = 'Teams' THEN 'Microsoft Teams'
        ELSE contact_preference.preference
    END AS hid
FROM public.contact_preference;

ALTER VIEW registration.contact_preference_hid OWNER TO dev;
GRANT SELECT ON registration.contact_preference_hid TO starters_registration;

-- This is nullable because we don't want to have to update all previous forms to have a value. But the registration app
-- should force people to set a value for new ones.
ALTER TABLE registration.form ADD COLUMN contact_preference_id BIGINT REFERENCES public.contact_preference(contact_preference_id);

ALTER TABLE registration.form ADD COLUMN contact_preference_details TEXT;

DROP VIEW hotwire3."10_View/People/Registration";

CREATE VIEW hotwire3."10_View/People/Registration"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.contact_preference_id,
    form.contact_preference_details,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_paper_safety_checklist,
    form.hide_from_website,
    form.already_has_university_card,
    form.previously_registered,
        CASE
            WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
            ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid::character varying, form.date_of_birth)
        END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    false AS import_this_record,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE registration.state_of_form(form.uuid) = 'Complete'::text AND form._imported_on IS NULL;

ALTER TABLE hotwire3."10_View/People/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Registration" TO hr;


CREATE OR REPLACE RULE hotwire3_view_registration_del AS
    ON DELETE TO hotwire3."10_View/People/Registration"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.form_id = old.id));

-- FUNCTION: registration.people_registration_upd()

-- DROP FUNCTION registration.people_registration_upd();

CREATE OR REPLACE FUNCTION registration.people_registration_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    new_person_id BIGINT;
    new_emergency_contact VARCHAR(500);
    existing_pg_id BIGINT;
BEGIN
-- first and foremost update the data
     UPDATE registration.form SET
         first_names = NEW.first_names,
         surname = NEW.surname,
         known_as = NEW.known_as,
         title_id = NEW.title_id,
         date_of_birth = NEW.date_of_birth,
         gender_id = NEW.gender_id,
         post_category_id = NEW.post_category_id,
         nationality_id = NEW.nationality_id,
         job_title = NEW.job_title,
         dept_room_id = NEW.room_id,
         dept_telephone_id = NEW.dept_telephone_number_id,
         contact_preference_id = NEW.contact_preference_id,
         contact_preference_details = NEW.contact_preference_details,
         college_id = NEW.cambridge_college_id,
         home_address = NEW.home_address,
         home_phone_number = NEW.home_phone_number,
         mobile_number = NEW.mobile_number,
         email = NEW.email,
         crsid = NEW.crsid,
         start_date = NEW.start_date,
         intended_end_date = NEW.intended_end_date,
         home_institution = NEW.home_institution,
         mifare_areas = NEW.group_areas,
         mifare_access_level_id = NEW.mifare_access_level_id,
         department_host_id = NEW.supervisor_id,
         deposit_receipt = NEW.deposit_receipt,
         emergency_contact_name_1 = NEW.emergency_contact_name_1,
         emergency_contact_number_1 = NEW.emergency_contact_number_1,
         emergency_contact_name_2 = NEW.emergency_contact_name_2,
         emergency_contact_number_2 = NEW.emergency_contact_number_2,
         previously_registered = NEW.previously_registered,
         hide_from_website = NEW.hide_from_website,
         already_has_university_card = NEW.already_has_university_card,
         _match_to_person_id = NEW.match_to_person_id
     WHERE form_id = OLD.id;

     IF NEW.import_this_record = 't'::boolean THEN
         -- Build a composite field
         new_emergency_contact := NEW.emergency_contact_name_1
                     || ' '
                     || NEW.emergency_contact_number_1
                     || (   COALESCE(E'\n'||NEW.emergency_contact_name_2||' ','')
                            || COALESCE(NEW.emergency_contact_number_2,'')
                        );
         -- Is there a match with an existing person?
         -- If match manually set, use that. 
         IF NEW.match_to_person_id IS NOT NULL THEN
             new_person_id = NEW.match_to_person_id;
         -- Or if the system has worked it out for itself use that
         -- Using OLD here because going for NEW would seem potentially fatal as an edit could change the automatic match
         ELSIF OLD.previously_registered = 't' AND OLD.ro_automatically_matched_person_id IS NOT NULL THEN
             new_person_id = OLD.ro_automatically_matched_person_id;
         END IF;
         -- If we found a match, update that person
         -- First the non-nullable fields; these can be updated unconditionally
         IF new_person_id IS NOT NULL THEN
             UPDATE person SET
                 surname = NEW.surname,
                 title_id = NEW.title_id,
                 date_of_birth = NEW.date_of_birth,
                 cambridge_address = NEW.home_address,
                 cambridge_phone_number = NEW.home_phone_number,
                 mobile_number = NEW.mobile_number,
                 email_address = NEW.email,
                 crsid = NEW.crsid,
                 emergency_contact = new_emergency_contact,
                 do_not_show_on_website = NEW.hide_from_website,
                 arrival_date = NEW.start_date,
                 left_but_no_leaving_date_given = null::boolean, -- they are definitely here now, so remove leaving dates
                 leaving_date = null::date
             WHERE person.id = new_person_id;
             -- These are nullable on the registration form, so if we had data in them
             -- we don't want to overwrite it with blanks
             IF NEW.first_names IS NOT NULL THEN
                 UPDATE person SET first_names = NEW.first_names WHERE person.id = new_person_id;
             END IF;
             IF NEW.known_as IS NOT NULL THEN
                 UPDATE person SET known_as = NEW.known_as WHERE person.id = new_person_id;
             END IF;
             IF NEW.cambridge_college_id IS NOT NULL THEN
                 UPDATE person SET cambridge_college_id = NEW.cambridge_college_id WHERE person.id = new_person_id;
             END IF;
             IF NEW.gender_id IS NOT NULL THEN
                 UPDATE person SET gender_id = NEW.gender_id WHERE person.id = new_person_id;
             END IF;
         ELSE -- create new
             INSERT INTO person (
                 first_names,
                 surname,
                 known_as,
                 title_id,
                 date_of_birth,
                 gender_id,
                 cambridge_college_id,
                 cambridge_address,
                 cambridge_phone_number,
                 mobile_number,
                 email_address,
                 crsid,
                 emergency_contact,
                 do_not_show_on_website,
                 arrival_date
             ) VALUES (
                 NEW.first_names,
                 NEW.surname,
                 NEW.known_as,
                 NEW.title_id,
                 NEW.date_of_birth,
                 NEW.gender_id,
                 NEW.cambridge_college_id,
                 NEW.home_address,
                 NEW.home_phone_number,
                 NEW.mobile_number,
                 NEW.email,
                 NEW.crsid,
                 new_emergency_contact,
                 NEW.hide_from_website,
                 NEW.start_date
             ) RETURNING id INTO new_person_id;
         END IF;
         -- Upsert any contact preferences
         IF NEW.contact_preference_id IS NOT NULL THEN
             INSERT INTO public.mm_person_contact_preference (
                person_id,
                contact_preference_id,
                further_details
            ) VALUES (
                new_person_id,
                NEW.contact_preference_id,
                NEW.contact_preference_details
            ) ON CONFLICT (person_id) DO UPDATE SET contact_preference_id = EXCLUDED.contact_preference_id, further_details = EXCLUDED.further_details;
        END IF;
         -- Do mm-updates here on nationality, room, phone
         PERFORM public.fn_mm_array_update(NEW.room_id,'mm_person_room'::varchar,'person_id'::varchar,'room_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.dept_telephone_number_id,'mm_person_dept_telephone_number'::varchar,'person_id'::varchar,'dept_telephone_number_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.nationality_id,'mm_person_nationality'::varchar,'person_id'::varchar,'nationality_id'::varchar,new_person_id);
         -- create the role
         CASE rtrim(NEW.post_category_id,'-1234567890')
             WHEN 'sc' THEN
                 INSERT INTO post_history (
                     person_id,
                     start_date,
                     supervisor_id,
                     job_title,
                     intended_end_date,
                     staff_category_id
                 ) VALUES (
                     new_person_id,
                     NEW.start_date,
                     NEW.supervisor_id,
                     NEW.job_title,
                     NEW.intended_end_date,
                     ltrim(NEW.post_category_id,'sc-')::bigint
                 );
             WHEN 'v' THEN
                 INSERT INTO visitorship (
                     person_id,
                     host_person_id,
                     start_date,
                     intended_end_date,
                     home_institution,
                     visitor_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'v-')::bigint
                 );
             WHEN 'pg' THEN
               -- Need to check for an existing postgraduate_studentship of the same type here and not import if there is one
               -- because these sometimes get created in bulk at the start of the academic year
                 SELECT id INTO existing_pg_id
                 FROM postgraduate_studentship
                 WHERE person_id = new_person_id AND postgraduate_studentship_type_id = ltrim(NEW.post_category_id,'pg-')::bigint;
                 IF existing_pg_id IS NULL THEN
                     INSERT INTO postgraduate_studentship (
                         person_id,
                         first_supervisor_id,
                         start_date,
                         postgraduate_studentship_type_id
                     ) VALUES (
                         new_person_id,
                         NEW.supervisor_id,
                         NEW.start_date,
                         ltrim(NEW.post_category_id,'pg-')::bigint
                     );
                 END IF;
             WHEN 'e' THEN
                 INSERT INTO erasmus_socrates_studentship (
                     person_id,
                     supervisor_id,
                     start_date,
                     intended_end_date,
                     university,
                     erasmus_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'e-')::bigint
                 );
         END CASE;
         RAISE INFO 'Importing a person';
         -- Record this one was imported and who it matches to
         UPDATE registration.form SET _imported_on = current_date, _match_to_person_id = new_person_id WHERE form_id = OLD.id; 
     END IF;
     RETURN NEW;
END;
$BODY$;

ALTER FUNCTION registration.people_registration_upd()
    OWNER TO dev;


CREATE TRIGGER registration_form_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Registration"
    FOR EACH ROW
    EXECUTE FUNCTION registration.people_registration_upd();


