DROP VIEW hotwire3."10_View/Network/MAC_to_VLAN";

CREATE VIEW hotwire3."10_View/Network/MAC_to_VLAN"
 AS
 SELECT a.id,
    a.mac,
    a.vlan_vid_id,
    a.infraction_type_id,
    a.ticket,
    a."Infraction notes",
    a.last_change,
    a."Logged change"
   FROM ( SELECT shadow_mac_to_vlan.id,
            shadow_mac_to_vlan.mac,
            shadow_mac_to_vlan.vid AS vlan_vid_id,
            NULL::integer AS infraction_type_id,
            NULL::integer AS ticket,
            NULL::character varying AS "Infraction notes",
            ( SELECT (((((to_char(network_vlan_change."timestamp", 'YYYY-MM-DD HH24:MI:SS'::text) || ' : '::text) || infraction_type_hid.infraction_type_hid::text) || ' ['::text) || network_vlan_change.actor::text) || ']'::text)::character varying AS history
                   FROM network_vlan_change
                     JOIN infraction_type_hid USING (infraction_type_id)
                  WHERE network_vlan_change.mac = shadow_mac_to_vlan.mac
                  ORDER BY network_vlan_change."timestamp" DESC
                 LIMIT 1) AS last_change,
            hotwire3.to_hwsubviewb('10_View/Network/_vlanchange_ro'::character varying, '_shmacid'::character varying, NULL::character varying, NULL::character varying, NULL::character varying) AS "Logged change"
           FROM shadow_mac_to_vlan) a
  ORDER BY a.last_change DESC NULLS LAST;

ALTER TABLE hotwire3."10_View/Network/MAC_to_VLAN"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/MAC_to_VLAN" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/MAC_to_VLAN" TO dev;

-- FUNCTION: hotwire3.mac_to_vlan_trig()

CREATE OR REPLACE FUNCTION hotwire3.mac_to_vlan_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    v_id bigint;
    old_vid bigint := null;
BEGIN
    IF NEW.id IS NULL THEN
        INSERT INTO shadow_mac_to_vlan ( mac, vid ) VALUES ( NEW.mac, NEW.vlan_vid_id ) RETURNING id INTO v_id;
        NEW.id := v_id;
    ELSE
        old_vid := OLD.vlan_vid_id;
        v_id := NEW.id;
        UPDATE shadow_mac_to_vlan SET mac = NEW.mac, vid = NEW.vlan_vid_id WHERE id = NEW.id;
    END IF;
    INSERT INTO network_vlan_change (shadow_mac_to_vlan_id, mac, infraction_type_id, ticket, notes, old_vid, new_vid) VALUES (v_id, NEW.mac, NEW.infraction_type_id, NEW.ticket, NEW."Infraction notes", old_vid, NEW.vlan_vid_id);
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.mac_to_vlan_trig()
    OWNER TO dev;


-- Rule: "hotwire3_10_View/Network/MAC_to_VLAN2_del" ON hotwire3."10_View/Network/MAC_to_VLAN"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/MAC_to_VLAN2_del" ON hotwire3."10_View/Network/MAC_to_VLAN";

CREATE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_del" AS
    ON DELETE TO hotwire3."10_View/Network/MAC_to_VLAN"
    DO INSTEAD
(DELETE FROM shadow_mac_to_vlan
  WHERE (shadow_mac_to_vlan.id = old.id));

CREATE TRIGGER mac_to_vlan_infractions
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Network/MAC_to_VLAN"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.mac_to_vlan_trig();
