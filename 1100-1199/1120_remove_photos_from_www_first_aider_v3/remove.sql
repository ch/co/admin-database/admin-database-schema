DROP VIEW www.first_aiders_v3;

CREATE VIEW www.first_aiders_v3
 AS
 SELECT DISTINCT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid AS person,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
        CASE
            WHEN firstaider.hf_cn_trained = true AND firstaider.requalify_date >= CURRENT_DATE AND firstaider.hf_cn_requalify_date >= CURRENT_DATE THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained,
        CASE
            WHEN firstaider.mental_health_awareness_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS mental_health_awareness_trained,
    building_hid.building_hid AS building,
    building_floor_hid.building_floor_hid AS building_floor,
    array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms,
    ps.status_id AS status,
    (building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END AS floor_sort,
        CASE
            WHEN firstaider.requalify_date >= CURRENT_DATE THEN true
            ELSE false
        END AS qualification_up_to_date
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     LEFT JOIN mm_person_room USING (person_id)
     LEFT JOIN room ON mm_person_room.room_id = room.id
     LEFT JOIN building_hid ON room.building_id = building_hid.building_id
     LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
     JOIN _physical_status_v3 ps USING (person_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END), firstaider.id, (person.image_lo::bigint), person_hid.person_hid, firstaider.firstaider_funding_id, firstaider.requalify_date, (
        CASE
            WHEN firstaider.hf_cn_trained = true AND firstaider.requalify_date >= CURRENT_DATE AND firstaider.hf_cn_requalify_date >= CURRENT_DATE THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END), building_hid.building_hid, building_floor_hid.building_floor_hid, (array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text)), ps.status_id;

ALTER TABLE www.first_aiders_v3
    OWNER TO dev;

GRANT ALL ON TABLE www.first_aiders_v3 TO dev;
GRANT SELECT ON TABLE www.first_aiders_v3 TO www_sites;


