ALTER TABLE public.person ADD CONSTRAINT email_address_has_domain CHECK ( email_address LIKE '%@%' );
