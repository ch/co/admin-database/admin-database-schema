CREATE OR REPLACE FUNCTION public.update_person_photo()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    IF TG_OP = 'INSERT' AND NEW.image_lo IS NOT NULL THEN
        -- inserts with a photo
        INSERT INTO public.person_photo (person_id, photo) VALUES (NEW.id, lo_get(NEW.image_lo));
    ELSE
        -- updates to person may add a photo where there was none, change an existing photo, or make no change to photo
        IF NEW.image_lo IS NOT NULL AND NEW.image_lo IS DISTINCT FROM OLD.image_lo THEN
            INSERT INTO public.person_photo
                (person_id, photo)
                VALUES
                (NEW.id, lo_get(NEW.image_lo))
                ON CONFLICT (person_id) DO UPDATE SET photo = lo_get(NEW.image_lo);
        ELSIF NEW.image_lo IS NULL AND OLD.image_lo IS NOT NULL THEN
            DELETE FROM public.person_photo WHERE person_id = NEW.id;
        END IF;
    END IF;
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.update_person_photo()
    OWNER TO dev;


CREATE TRIGGER update_photo_table
    AFTER INSERT OR UPDATE 
    ON public.person
    FOR EACH ROW
    EXECUTE FUNCTION public.update_person_photo();
