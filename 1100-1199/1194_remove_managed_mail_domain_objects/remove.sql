CREATE TABLE public.mail_alias (
    mail_alias_id bigint NOT NULL,
    comment character varying,
    alias character varying(64) NOT NULL,
    addresses character varying
);


ALTER TABLE public.mail_alias OWNER TO dev;

COMMENT ON TABLE public.mail_alias IS 'Mail aliases for the managed mail domain';


COMMENT ON COLUMN public.mail_alias.addresses IS 'Email addresses to send to, must be comma-separated';


CREATE SEQUENCE public.mail_alias_mail_alias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.mail_alias_mail_alias_id_seq OWNER TO dev;

ALTER SEQUENCE public.mail_alias_mail_alias_id_seq OWNED BY public.mail_alias.mail_alias_id;


ALTER TABLE ONLY public.mail_alias ALTER COLUMN mail_alias_id SET DEFAULT nextval('public.mail_alias_mail_alias_id_seq'::regclass);


ALTER TABLE ONLY public.mail_alias
    ADD CONSTRAINT alias_unique UNIQUE (alias);


ALTER TABLE ONLY public.mail_alias
    ADD CONSTRAINT mail_alias_pk PRIMARY KEY (mail_alias_id);


GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.mail_alias TO cos;


GRANT USAGE ON SEQUENCE public.mail_alias_mail_alias_id_seq TO cos;



CREATE OR REPLACE VIEW apps.managed_mail_domain
 AS
 SELECT COALESCE(mail_alias.alias, autogen.alias, room_alias.alias) AS alias,
    COALESCE(mail_alias.addresses, autogen.addresses, room_alias.addresses::character varying) AS target,
        CASE
            WHEN mail_alias.alias IS NOT NULL THEN mail_alias.comment
            WHEN room_alias.alias IS NOT NULL THEN room_alias.comment
            ELSE 'autogenerated from db'::character varying
        END AS comment
   FROM mail_alias
     FULL JOIN ( SELECT person.crsid AS alias,
            person.email_address AS addresses
           FROM person
             JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
          WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Past'::text) AND person.crsid IS NOT NULL AND person.email_address IS NOT NULL AND NOT person.managed_mail_domain_optout) autogen ON mail_alias.alias::text = autogen.alias::text
     FULL JOIN ( SELECT a.email_local_part AS alias,
            array_to_string(a.array_addresses, ','::text) AS addresses,
            'room alias from db'::character varying AS comment
           FROM ( SELECT room.email_local_part,
                    ARRAY( SELECT btrim(p.email_address::text) AS btrim
                           FROM person p
                             JOIN mm_person_room mm ON p.id = mm.person_id
                             JOIN room r2 ON mm.room_id = r2.id
                             LEFT JOIN _physical_status_v3 USING (person_id)
                          WHERE r2.id = room.id AND p.email_address IS NOT NULL AND p.email_address::text ~~ '%@%'::text AND _physical_status_v3.status_id::text = 'Current'::text) AS array_addresses
                   FROM room) a
          WHERE a.array_addresses <> '{}'::text[]) room_alias ON room_alias.alias::text = autogen.alias::text;

ALTER TABLE apps.managed_mail_domain
    OWNER TO cen1001;

GRANT SELECT ON TABLE apps.managed_mail_domain TO ad_accounts;
GRANT ALL ON TABLE apps.managed_mail_domain TO cen1001;
GRANT SELECT ON TABLE apps.managed_mail_domain TO mailinglists;

CREATE OR REPLACE VIEW apps.managed_mail_domain_v2
 AS
 SELECT COALESCE(mail_alias.alias, autogen.alias, room_alias.alias) AS alias,
    COALESCE(mail_alias.addresses, autogen.addresses, room_alias.addresses::character varying) AS target,
        CASE
            WHEN mail_alias.alias IS NOT NULL THEN mail_alias.comment
            ELSE 'autogenerated from db'::character varying
        END AS comment
   FROM mail_alias
     FULL JOIN ( SELECT person.crsid AS alias,
            person.email_address AS addresses
           FROM person
             JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
          WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Past'::text) AND person.crsid IS NOT NULL AND person.email_address IS NOT NULL AND NOT person.managed_mail_domain_optout) autogen ON mail_alias.alias::text = autogen.alias::text
     FULL JOIN ( SELECT a.email_local_part AS alias,
            array_to_string(a.array_addresses, ','::text) AS addresses,
            'autogenerated from db'::text AS comment
           FROM ( SELECT room.email_local_part,
                    ARRAY( SELECT btrim(p.email_address::text) AS btrim
                           FROM person p
                             JOIN mm_person_room mm ON p.id = mm.person_id
                             JOIN room r2 ON mm.room_id = r2.id
                             LEFT JOIN _physical_status_v3 USING (person_id)
                          WHERE r2.id = room.id AND p.email_address IS NOT NULL AND _physical_status_v3.status_id::text = 'Current'::text) AS array_addresses
                   FROM room) a
          WHERE a.array_addresses <> '{}'::text[]) room_alias ON room_alias.alias::text = autogen.alias::text;

ALTER TABLE apps.managed_mail_domain_v2
    OWNER TO cen1001;


CREATE OR REPLACE VIEW apps.managed_mail_domain_v3
 AS
 WITH all_aliases AS (
         SELECT mail_alias.alias,
            mail_alias.addresses AS target,
            mail_alias.comment,
            1 AS priority
           FROM mail_alias
        UNION
         SELECT person.crsid AS alias,
            COALESCE(person.managed_mail_domain_target, person.email_address::text) AS addresses,
            'autogenerated from db'::character varying AS comment,
            2 AS priority
           FROM person
             JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
          WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Past'::text) AND person.crsid IS NOT NULL AND person.email_address::text ~~ '%@%'::text AND btrim(COALESCE(person.managed_mail_domain_target, person.email_address::text)) <> (person.crsid::text || '@ch.cam.ac.uk'::text) AND NOT person.managed_mail_domain_optout
        )
 SELECT DISTINCT ON (all_aliases.alias) all_aliases.alias,
    all_aliases.target,
    all_aliases.comment
   FROM all_aliases
  ORDER BY all_aliases.alias, all_aliases.priority;

ALTER TABLE apps.managed_mail_domain_v3
    OWNER TO dev;

GRANT SELECT ON TABLE apps.managed_mail_domain_v3 TO ad_accounts;
GRANT ALL ON TABLE apps.managed_mail_domain_v3 TO cen1001;
GRANT ALL ON TABLE apps.managed_mail_domain_v3 TO dev;
GRANT SELECT ON TABLE apps.managed_mail_domain_v3 TO mailinglists;

