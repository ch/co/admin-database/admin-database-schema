DROP VIEW hotwire3."10_View/People/COs_View";

CREATE VIEW hotwire3."10_View/People/COs_View"
 AS
 SELECT a.id,
    a.ro_person_id,
    a.image,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.crsid,
    a.date_of_birth,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.location,
    a.ro_physical_status_id,
    a.ro_estimated_leaving_date,
    a.do_not_show_on_website AS hide_person_from_website,
    a.hide_photo_from_website,
    a.hide_email AS hide_email_from_website,
    a.is_spri,
    a.managed_mail_domain_target,
    a.managed_mail_domain_optout,
    a.ro_final_managed_mail_domain_destination,
    a.ro_auto_banned_from_network,
    a.override_auto_ban,
    a.ro_network_ban_log,
    a._cssclass
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            convert_to(E'Content-type: image/jpeg\n\n','UTF8') || person_photo.photo AS image,
            person.surname,
            person.first_names,
            person.title_id,
            person.name_suffix,
            person.email_address,
            person.crsid,
            person.date_of_birth,
            person.do_not_show_on_website,
            person.hide_photo_from_website,
            person.hide_email,
            person.is_spri,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            person.location,
            _physical_status.status_id AS ro_physical_status_id,
            LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
            managed_mail_domain_v3.target AS ro_final_managed_mail_domain_destination,
            person.managed_mail_domain_target::character varying AS managed_mail_domain_target,
            person.managed_mail_domain_optout,
            person.auto_banned_from_network AS ro_auto_banned_from_network,
            person.override_auto_ban,
            person.network_ban_log::text AS ro_network_ban_log,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN person_photo ON person.id = person_photo.person_id
             LEFT JOIN cache._latest_role_v12 _latest_role ON person.id = _latest_role.person_id
             LEFT JOIN cache.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
             LEFT JOIN apps.managed_mail_domain_v3 ON person.crsid::text = managed_mail_domain_v3.alias::text) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/COs_View"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/COs_View" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO hr;


CREATE OR REPLACE RULE hotwire3_people_cos_view_del AS
    ON DELETE TO hotwire3."10_View/People/COs_View"
    DO INSTEAD
(DELETE FROM person
  WHERE (person.id = old.id));

CREATE OR REPLACE FUNCTION hotwire3.people_cos_view_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    IF NEW.id IS NOT NULL THEN
        UPDATE
            person
        SET
            surname = NEW.surname,
            first_names = NEW.first_names,
            title_id = NEW.title_id,
            name_suffix = NEW.name_suffix,
            email_address = NEW.email_address,
            crsid = NEW.crsid,
            date_of_birth = NEW.date_of_birth,
            location = NEW.location,
            do_not_show_on_website = NEW.hide_person_from_website,
            hide_photo_from_website = NEW.hide_photo_from_website,
            hide_email = NEW.hide_email_from_website,
            is_spri = NEW.is_spri,
            managed_mail_domain_target = NEW.managed_mail_domain_target,
            managed_mail_domain_optout = NEW.managed_mail_domain_optout,
            override_auto_ban = NEW.override_auto_ban
        WHERE
            person.id = NEW.id;
    ELSE
        NEW.id := nextval('person_id_seq');
        INSERT INTO person (id, surname, first_names, title_id, name_suffix, email_address, crsid, date_of_birth, location, do_not_show_on_website, hide_photo_from_website, hide_email, is_spri, managed_mail_domain_target, managed_mail_domain_optout, override_auto_ban)
            VALUES (NEW.id, NEW.surname, NEW.first_names, NEW.title_id, NEW.name_suffix, NEW.email_address, NEW.crsid, NEW.date_of_birth, NEW.location, coalesce(NEW.hide_person_from_website, 'f'), coalesce(NEW.hide_photo_from_website, 'f'), NEW.hide_email_from_website, NEW.is_spri, NEW.managed_mail_domain_target, NEW.managed_mail_domain_optout, NEW.override_auto_ban);
    END IF;

    IF NEW.image IS NOT NULL AND NEW.image IS DISTINCT FROM OLD.image
    -- We have a picture and it's a new one. Has to be 'is distinct from' not
    -- <> for case where OLD.image is null. We do need to check the image is
    -- different between OLD and NEW because the image data in the OLD column
    -- has metadata added by the view definition, which we don't want to save
    -- in the bytea column by mistake. But if the image has changed we can
    -- assume what we've got is a new photo which should have been uploaded
    -- without any metadata being prepended, and so we can safely save that
    THEN
        INSERT INTO person_photo ( person_id, photo ) VALUES ( NEW.id, NEW.image )
        ON CONFLICT (person_id) DO UPDATE SET photo = NEW.image;
    ELSIF NEW.image IS NULL
    -- Hotwire doesn't let us set an existing bytea to null yet, but this is for when it does
    THEN
        DELETE FROM person_photo WHERE person_id = NEW.id;
    END IF;

    PERFORM
        fn_mm_array_update (NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar, 'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.research_group_id, 'mm_person_research_group'::varchar, 'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.room_id, 'mm_person_room'::varchar, 'person_id'::varchar, 'room_id'::varchar, NEW.id);
    RETURN NEW;
END;
$BODY$;

CREATE TRIGGER hotwire3_people_cos_view_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig();


CREATE TRIGGER hotwire3_people_cos_view_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig();


DROP VIEW hotwire3."10_View/People/Photography_Registration";

CREATE VIEW hotwire3."10_View/People/Photography_Registration"
 AS
 WITH futuremost_role_estimated_leaving_date AS (
         SELECT DISTINCT ON (_all_roles_v13.person_id) _all_roles_v13.person_id,
            _all_roles_v13.estimated_leaving_date,
            _all_roles_v13.post_category
           FROM _all_roles_v13
          ORDER BY _all_roles_v13.person_id, _all_roles_v13.start_date DESC, _all_roles_v13.estimated_leaving_date DESC
        )
 SELECT person.id,
    person.crsid AS ro_crsid,
    person.gender_id,
    person.first_names,
    person.known_as,
    person.surname,
    person.date_of_birth,
    person.arrival_date AS ro_arrival_date,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
    futuremost_role.post_category AS ro_post_category,
    ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
    convert_to(E'Content-type: image/jpeg\n\n','UTF8') || person_photo.photo AS image,
    registration_form.already_has_university_card AS ro_had_card_when_registering,
    university_card.issued_at AS ro_university_card_issued_at,
    university_card.expires_at AS ro_university_card_expires_at,
    university_card.issue_number AS ro_university_card_issue_number,
    university_card.legacy_card_holder_id AS ro_university_card_identifier,
    university_card.mifare_number AS ro_university_card_mifare_number,
    university_card.mifare_id AS ro_university_card_mifare_identifier,
    university_card.barcode AS ro_university_card_barcode,
    FALSE::BOOLEAN AS delete_photo
   FROM person
     LEFT JOIN person_photo ON person.id = person_photo.person_id
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     LEFT JOIN futuremost_role_estimated_leaving_date futuremost_role ON person.id = futuremost_role.person_id
     LEFT JOIN registration.form registration_form ON registration_form._match_to_person_id = person.id
     LEFT JOIN university_card ON person.crsid::text = university_card.crsid
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Photography_Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Photography_Registration" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/People/Photography_Registration" TO photography;

CREATE OR REPLACE FUNCTION hotwire3.photography_registration_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	BEGIN
		IF NEW.id IS NOT NULL
		THEN
			UPDATE person SET
				gender_id = NEW.gender_id,
				first_names = NEW.first_names,
				known_as = NEW.known_as,
				surname = NEW.surname,
				date_of_birth = NEW.date_of_birth
			WHERE person.id = NEW.id;
		ELSE
			INSERT INTO person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth
			) VALUES (
				NEW.gender_id,
				NEW.first_names,
				NEW.known_as,
				NEW.surname,
				NEW.date_of_birth
			) RETURNING id INTO NEW.id;
		END IF;

                IF NEW.image IS NOT NULL AND NEW.image IS DISTINCT FROM OLD.image
                -- We have a picture and it's a new one. Has to be 'is distinct
                -- from' not <> for case where OLD.image is null. We do need to
                -- check the image is different between OLD and NEW because the
                -- image data in the OLD column has metadata added by the view
                -- definition, which we don't want to save in the bytea column by
                -- mistake. But if the image has changed we can assume what
                -- we've got is a new photo which should have been uploaded
                -- without any metadata being prepended, and so we can safely
                -- save that
                THEN
                    INSERT INTO person_photo ( person_id, photo ) VALUES ( NEW.id, NEW.image )
                    ON CONFLICT (person_id) DO UPDATE SET photo = NEW.image;
                ELSIF NEW.image IS NULL
                -- Hotwire doesn't yet provide a way to set a bytea to null but
                -- this clause is included for correctness
                THEN
                    DELETE FROM person_photo WHERE person_id = NEW.id;
                END IF;

                -- To allow removing a photo
                IF NEW.delete_photo = TRUE
                THEN
                    DELETE FROM person_photo WHERE person_id = NEW.id;
                END IF;

		PERFORM fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);	

		RETURN NEW;
	END;
$BODY$;


CREATE TRIGGER hotwire3_photography_registration_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


CREATE TRIGGER hotwire3_photography_registration_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Photography_Registration"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.photography_registration_update();


