REVOKE UPDATE,SELECT,DELETE ON public.printer FROM cos;

CREATE OR REPLACE RULE hotwire3_view_printers_del AS
    ON DELETE TO hotwire3."10_View/Computers/Printers"
    DO INSTEAD
( DELETE FROM printer
  WHERE (printer.id = old.printer_pk);
 DELETE FROM hardware
  WHERE (hardware.id = old.id);
);

ALTER TABLE public.printer OWNER TO postgres;
GRANT ALL ON public.printer TO dev;
