ALTER TABLE public.printer OWNER TO dev;
GRANT UPDATE,SELECT,DELETE ON public.printer TO cos;

CREATE OR REPLACE RULE hotwire3_view_printers_del AS
    ON DELETE TO hotwire3."10_View/Computers/Printers"
    DO INSTEAD
 DELETE FROM hardware
  WHERE hardware.id = OLD.id;
;
