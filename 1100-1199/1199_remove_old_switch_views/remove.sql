-- Actually, this puts the views back rather than removing them.


-- Views to create:
-- hotwire3."10_View/Network/Switches/Switch_Configs_ro";
-- hotwire3."10_View/Network/Switches/Switches";
-- hotwire3."10_View/Network/Switches/Switch_Models";
-- hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";
-- hotwire3."10_View/Network/Switches/Switch_Ports";
-- hotwire3."10_View/Network/Switches/_switch_config_subview";
-- hotwire3."10_View/Network/Switches/_switches";
-- hotwire3."10_View/Network/Switches/_Switchport_ro";

-- View: hotwire3.10_View/Network/Switches/Switch_Configs_ro

-- DROP VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Configs_ro"
 AS
 SELECT switch.id,
    switch.id AS switch_id,
    array_to_string(ARRAY( SELECT _switch_config_d(switch.id) AS _switch_config), '
'::text) AS ro_config
   FROM switch;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Configs_ro" TO dev;

-- View: hotwire3.10_View/Network/Switches/Switches

-- DROP VIEW hotwire3."10_View/Network/Switches/Switches";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switches"
 AS
 SELECT switch.id,
    hardware.name::character varying(40) AS name,
    hardware.id AS _hardware_id,
    system_image.id AS _system_image_id,
    hardware.manufacturer::character varying(60) AS manufacturer,
    hardware.hardware_type_id,
    system_image.wired_mac_1,
    switch.switch_model_id,
    switch.ignore_config,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.serial_number::character varying(40) AS serial_number,
    hardware.asset_tag::character varying(40) AS asset_tag,
    hardware.date_purchased,
    hardware.date_configured,
    hardware.warranty_end_date,
    hardware.date_decommissioned,
    hardware.warranty_details,
    hardware.room_id,
    switch.cabinet_id,
    hardware.owner_id,
    system_image.research_group_id,
    hardware.comments::text AS comments,
    ARRAY( SELECT mm_switch_goal_applies_to_switch.switch_config_goal_id
           FROM mm_switch_goal_applies_to_switch
          WHERE mm_switch_goal_applies_to_switch.switch_id = switch.id) AS switch_config_goal_id,
    switch.extraconfig::text AS extraconfig,
    hotwire3.to_hwsubviewb('10_View/Network/_Cabinet_for_switch'::character varying, 'switch_id'::character varying, '10_View/Network/Cabinets'::character varying, 'cabinet_id'::character varying, 'id'::character varying) AS "Cabinet_details",
    hotwire3.to_hwsubviewb('10_View/Network/Switches/_Switchport_ro'::character varying, 'switch_id'::character varying, '10_View/Network/Switches/Switch_Ports'::character varying, NULL::character varying, NULL::character varying) AS "Ports",
    hotwire3.to_hwsubviewb('10_View/Network/Switches/_switch_config_subview'::character varying, 'id'::character varying, '10_View/Network/Switches/Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config"
   FROM switch
     JOIN hardware ON switch.hardware_id = hardware.id
     JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Network/Switches/Switches"
    OWNER TO dev;

GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switches" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switches" TO dev;


-- Rule: hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/Switches"

-- DROP Rule IF EXISTS hotwire3_view_network_switch_del ON hotwire3."10_View/Network/Switches/Switches";

CREATE RULE hotwire3_view_network_switch_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switches"
    DO INSTEAD
(DELETE FROM switch
  WHERE (switch.id = old.id));

-- Rule: hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/Switches"

-- DROP Rule IF EXISTS hotwire3_view_network_switch_upd ON hotwire3."10_View/Network/Switches/Switches";

CREATE RULE hotwire3_view_network_switch_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switches"
    DO INSTEAD
( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id, comments = new.comments
  WHERE (hardware.id = old._hardware_id);
 UPDATE system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1
  WHERE (system_image.id = old._system_image_id);
 SELECT fn_mm_array_update((new.switch_config_goal_id)::bigint[], (old.switch_config_goal_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update;
 UPDATE switch SET ignore_config = new.ignore_config, switch_model_id = new.switch_model_id, extraconfig = (new.extraconfig)::character varying, cabinet_id = new.cabinet_id
  WHERE (switch.id = old.id);
);

-- View: hotwire3.10_View/Network/Switches/Switch_Models

-- DROP VIEW hotwire3."10_View/Network/Switches/Switch_Models";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Models"
 AS
 SELECT switch_model.id,
    switch_model.model,
    switch_model.description,
    switch_model.ports,
    switch_model.uplink_ports
   FROM switch_model;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Models"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Models" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Models" TO dev;


-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/Switch_Models"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_del" ON hotwire3."10_View/Network/Switches/Switch_Models";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Models"
    DO INSTEAD
(DELETE FROM switch_model
  WHERE (switch_model.id = old.id));

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/Switch_Models"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_ins" ON hotwire3."10_View/Network/Switches/Switch_Models";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Models"
    DO INSTEAD
(INSERT INTO switch_model (model, description, ports, uplink_ports)
  VALUES (new.model, new.description, new.ports, new.uplink_ports)
  RETURNING switch_model.id,
    switch_model.model,
    switch_model.description,
    switch_model.ports,
    switch_model.uplink_ports);

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/Switch_Models"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Models_upd" ON hotwire3."10_View/Network/Switches/Switch_Models";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Models_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Models"
    DO INSTEAD
(UPDATE switch_model SET id = new.id, model = new.model, description = new.description, ports = new.ports, uplink_ports = new.uplink_ports
  WHERE (switch_model.id = old.id));

-- View: hotwire3.10_View/Network/Switches/Switch_Port_Config_Goals

-- DROP VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"
 AS
 SELECT switch_port_config_goal.id,
    switch_port_config_goal.name,
    switch_port_config_goal.goal_class AS goal_class_id
   FROM switch_port_config_goal;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals" TO dev;


-- Rule: hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"

-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_del ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";

CREATE RULE hotwire3_view_network_switch_port_config_goal_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"
    DO INSTEAD
(DELETE FROM switch_port_config_goal
  WHERE (switch_port_config_goal.id = old.id));

-- Rule: hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"

-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_ins ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";

CREATE RULE hotwire3_view_network_switch_port_config_goal_ins AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"
    DO INSTEAD
(INSERT INTO switch_port_config_goal (name, goal_class)
  VALUES (new.name, new.goal_class_id)
  RETURNING switch_port_config_goal.id,
    switch_port_config_goal.name,
    switch_port_config_goal.goal_class);

-- Rule: hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"

-- DROP Rule IF EXISTS hotwire3_view_network_switch_port_config_goal_upd ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals";

CREATE RULE hotwire3_view_network_switch_port_config_goal_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Goals"
    DO INSTEAD
(UPDATE switch_port_config_goal SET name = new.name, goal_class = new.goal_class_id
  WHERE (switch_port_config_goal.id = old.id));

-- View: hotwire3.10_View/Network/Switches/Switch_Ports

-- DROP VIEW hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Ports"
 AS
 SELECT switchport.id,
    switchport.name,
    switchport.socket_id,
    switchport.switch_id,
    switchport.ifacename,
    switchport.disabled,
    switchport.speed_id,
    switchport.switch_auth_id,
    ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id,
    hotwire3.to_hwsubviewb('10_View/Network/Switches/_switches'::character varying, 'switch_port_id'::character varying, '10_View/Network/Switches/Switches'::character varying, NULL::character varying, NULL::character varying) AS switch
   FROM switchport;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Ports"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO dev;


-- Rule: "hotwire3_10_View/Network/Switches/Switch_Ports_del" ON hotwire3."10_View/Network/Switches/Switch_Ports"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Ports_del" ON hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Ports"
    DO INSTEAD
(DELETE FROM switchport
  WHERE (switchport.id = old.id));

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/Switch_Ports"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Ports_ins" ON hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Ports"
    DO INSTEAD
(INSERT INTO switchport (name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id)
  VALUES (new.name, new.socket_id, new.switch_id, new.ifacename, new.disabled, new.speed_id, new.switch_auth_id)
  RETURNING switchport.id,
    switchport.name,
    switchport.socket_id,
    switchport.switch_id,
    switchport.ifacename,
    switchport.disabled,
    switchport.speed_id,
    switchport.switch_auth_id,
    ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE (mm_switch_config_switch_port.switch_port_id = switchport.id)) AS switch_port_goal_id,
    NULL::_hwsubviewb AS _hwsubviewb);

-- Rule: "hotwire3_10_View/Network/Switches/Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/Switch_Ports"

-- DROP Rule IF EXISTS "hotwire3_10_View/Network/Switches/Switch_Ports_upd" ON hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Ports"
    DO INSTEAD
( UPDATE switchport SET id = new.id, name = new.name, socket_id = new.socket_id, switch_id = new.switch_id, ifacename = new.ifacename, disabled = new.disabled, speed_id = new.speed_id, switch_auth_id = new.switch_auth_id
  WHERE (switchport.id = old.id);
 SELECT fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
);

-- View: hotwire3.10_View/Network/Switches/_switch_config_subview

-- DROP VIEW hotwire3."10_View/Network/Switches/_switch_config_subview";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switch_config_subview"
 AS
 SELECT switch.id,
    'Config for switch - click here to show'::character varying AS "Link to config"
   FROM switch;

ALTER TABLE hotwire3."10_View/Network/Switches/_switch_config_subview"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switch_config_subview" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switch_config_subview" TO dev;


-- View: hotwire3.10_View/Network/Switches/_switches

-- DROP VIEW hotwire3."10_View/Network/Switches/_switches";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_switches"
 AS
 SELECT switch.id,
    switchport.id AS switch_port_id,
    hardware.name AS switch_name,
    hardware.manufacturer,
    switch_model_hid.switch_model_hid,
    room_hid.room_hid
   FROM switchport
     LEFT JOIN switch ON switchport.switch_id = switch.id
     JOIN hardware ON switch.hardware_id = hardware.id
     LEFT JOIN hotwire3.room_hid USING (room_id)
     LEFT JOIN hotwire3.switch_model_hid USING (switch_model_id);

ALTER TABLE hotwire3."10_View/Network/Switches/_switches"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_switches" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_switches" TO dev;

-- View: hotwire3.10_View/Network/Switches/_Switchport_ro

-- DROP VIEW hotwire3."10_View/Network/Switches/_Switchport_ro";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_Switchport_ro"
 AS
 SELECT switchport.id,
    switchport.switch_id,
    switchport.name,
    socket_hid.socket_hid AS socket,
    switchport.ifacename,
    switch_auth_hid.switch_auth_hid,
    speed_hid.speed_hid
   FROM switchport
     LEFT JOIN hotwire3.switch_auth_hid USING (switch_auth_id)
     LEFT JOIN hotwire3.speed_hid USING (speed_id)
     LEFT JOIN hotwire3.socket_hid USING (socket_id)
  ORDER BY (
        CASE
            WHEN switchport.name::text ~ '^[A-Za-z]+'::text THEN 10 * ascii(substr(switchport.name::text, 1, 1)) + regexp_replace(switchport.name::text, '[A-Za-z]+'::text, ''::text)::integer
            ELSE switchport.name::integer
        END);

ALTER TABLE hotwire3."10_View/Network/Switches/_Switchport_ro"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_Switchport_ro" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_Switchport_ro" TO dev;


