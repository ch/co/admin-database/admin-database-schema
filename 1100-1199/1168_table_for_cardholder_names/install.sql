CREATE TABLE public.university_cardholder_detail (
    crsid text PRIMARY KEY,
    lookup_visible_name text,
    lookup_email text
);

ALTER TABLE public.university_cardholder_detail ADD CONSTRAINT crsids_are_lowercase CHECK (crsid = lower(crsid));

ALTER TABLE public.university_cardholder_detail OWNER TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.university_cardholder_detail TO _misd_import;

CREATE INDEX card_lower_case_crsid ON public.university_card (lower(crsid));
