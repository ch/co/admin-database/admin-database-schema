CREATE OR REPLACE VIEW hotwire3."10_View/Email/Chemistry_Mail_Domain"
 AS
 SELECT mail_alias.mail_alias_id AS id,
    mail_alias.alias,
    mail_alias.addresses,
    mail_alias.comment::text AS comment
   FROM mail_alias
  ORDER BY mail_alias.alias;

ALTER TABLE hotwire3."10_View/Email/Chemistry_Mail_Domain"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Email/Chemistry_Mail_Domain" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Email/Chemistry_Mail_Domain" TO dev;

CREATE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_del" AS
    ON DELETE TO hotwire3."10_View/Email/Chemistry_Mail_Domain"
    DO INSTEAD
(DELETE FROM mail_alias
  WHERE (mail_alias.mail_alias_id = old.id));

CREATE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_ins" AS
    ON INSERT TO hotwire3."10_View/Email/Chemistry_Mail_Domain"
    DO INSTEAD
(INSERT INTO mail_alias (comment, alias, addresses)
  VALUES ((new.comment)::character varying, new.alias, new.addresses)
  RETURNING mail_alias.mail_alias_id,
    mail_alias.alias,
    mail_alias.addresses,
    (mail_alias.comment)::text AS comment);

CREATE RULE "hotwire3_10_View/Email/Chemistry_Mail_Domain_upd" AS
    ON UPDATE TO hotwire3."10_View/Email/Chemistry_Mail_Domain"
    DO INSTEAD
(UPDATE mail_alias SET mail_alias_id = new.id, comment = (new.comment)::character varying, alias = new.alias, addresses = new.addresses
  WHERE (mail_alias.mail_alias_id = old.id));
