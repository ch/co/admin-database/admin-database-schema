CREATE VIEW hotwire3."10_View/People/_mifare_cards_by_person" AS
    SELECT
        card.id,
        person.id as person_id,
        card.issued_at,
        card.expires_at,
        card.issue_number,
        card.legacy_card_holder_id AS card_identifier,
        card.mifare_id AS mifare_identifier,
        card.mifare_number,
        card.barcode
    FROM university_card card
    JOIN public.person ON card.crsid = person.crsid::text
    ;

ALTER VIEW  hotwire3."10_View/People/_mifare_cards_by_person" OWNER TO dev;
GRANT SELECT ON  hotwire3."10_View/People/_mifare_cards_by_person" TO reception;


CREATE VIEW hotwire3."10_View/People/Personnel_Mifare"
 AS
 WITH futuremost_role_estimated_leaving_date AS (
         SELECT DISTINCT ON (_hr_role.person_id) _hr_role.person_id,
            _hr_role.estimated_role_end_date,
            _hr_role.post_category
           FROM _hr_role
          ORDER BY _hr_role.person_id, _hr_role.start_date DESC, _hr_role.estimated_role_end_date DESC
        ),
      newest_mifare_card AS (
          SELECT DISTINCT ON (crsid) crsid,
            issued_at,
            expires_at,
            issue_number,
            legacy_card_holder_id,
            mifare_number,
            mifare_id,
            barcode
          FROM university_card
          ORDER BY crsid, issue_number DESC
      )
 SELECT person.id,
    person.surname,
    person.first_names,
    person.known_as,
    person.crsid AS crsid,
    person.gender_id,
    person.date_of_birth,
    person.arrival_date AS arrival_date,
    LEAST(person.leaving_date, futuremost_role.estimated_role_end_date) AS estimated_leaving_date,
    futuremost_role.post_category AS post_category,
    university_card.issued_at AS university_card_issued_at,
    university_card.expires_at AS university_card_expires_at,
    university_card.issue_number AS university_card_issue_number,
    university_card.legacy_card_holder_id AS university_card_identifier,
    university_card.mifare_number AS university_card_mifare_number,
    university_card.mifare_id AS university_card_mifare_identifier,
    university_card.barcode AS university_card_barcode,
    hotwire3.to_hwsubviewb('10_View/People/_mifare_cards_by_person','person_id',null,null,null) AS all_cards
   FROM person
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     LEFT JOIN futuremost_role_estimated_leaving_date futuremost_role ON person.id = futuremost_role.person_id
     LEFT JOIN registration.form registration_form ON registration_form._match_to_person_id = person.id
     LEFT JOIN newest_mifare_card university_card ON person.crsid::text = university_card.crsid
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_Mifare"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Mifare" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Mifare" TO reception;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Personnel_Mifare', 'person' );
