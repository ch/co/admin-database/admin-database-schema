--
-- Name: www_academic_staff_v1; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_academic_staff_v1 AS
 SELECT person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category
   FROM (((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
  WHERE ((ps.status_id)::text = 'Current'::text);


ALTER VIEW public.www_academic_staff_v1 OWNER TO dev;

--
-- Name: www_committee_members; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_committee_members AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id
   FROM ((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
  WHERE ((lr.post_category_id = 'sc-1'::text) OR (lr.post_category_id = 'sc-2'::text) OR (lr.post_category_id = 'sc-3'::text) OR (lr.post_category_id = 'sc-9'::text))
  ORDER BY person_hid.person_hid;


ALTER VIEW public.www_committee_members OWNER TO dev;

--
-- Name: www_committee_members_v2; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_committee_members_v2 AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id
   FROM ((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
  WHERE ((lr.post_category_id = 'sc-1'::text) OR (lr.post_category_id = 'sc-2'::text) OR (lr.post_category_id = 'sc-3'::text) OR (lr.post_category_id = 'sc-9'::text) OR (lr.post_category_id = 'v-8'::text) OR (lr.post_category_id = 'sc-4'::text) OR (lr.post_category_id = 'sc-7'::text))
  ORDER BY person_hid.person_hid;


ALTER VIEW public.www_committee_members_v2 OWNER TO dev;

--
-- Name: www_committee_members_v3; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_committee_members_v3 AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
   FROM (((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
  WHERE (((lr.post_category_id = 'sc-1'::text) OR (lr.post_category_id = 'sc-2'::text) OR (lr.post_category_id = 'sc-3'::text) OR (lr.post_category_id = 'sc-9'::text) OR (lr.post_category_id = 'v-8'::text) OR (lr.post_category_id = 'sc-4'::text) OR (lr.post_category_id = 'sc-7'::text) OR (lr.post_category_id = 'sc-6'::text)) AND ((ps.status_id)::text = 'Current'::text))
  ORDER BY person_hid.person_hid;


ALTER VIEW public.www_committee_members_v3 OWNER TO dev;

--
-- Name: www_committee_student_members; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_committee_student_members AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
   FROM (((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
  WHERE (((lr.post_category_id = 'pg-1'::text) OR (lr.post_category_id = 'pg-2'::text) OR (lr.post_category_id = 'pg-3'::text) OR (lr.post_category_id = 'pg-4'::text) OR (lr.post_category_id = 'e-1'::text) OR (lr.post_category_id = 'e-2'::text) OR (lr.post_category_id = 'e-3'::text)) AND ((ps.status_id)::text = 'Current'::text))
  ORDER BY person_hid.person_hid;


ALTER VIEW public.www_committee_student_members OWNER TO dev;

--
-- Name: www_mailinglist_academic; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_mailinglist_academic AS
 SELECT person.crsid,
    person_hid.person_hid
   FROM (((public.person
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
     LEFT JOIN public._physical_status_v3 ps ON ((ps.person_id = person.id)))
     JOIN public.person_hid ON ((person.id = person_hid.person_id)))
  WHERE ((((lr.post_category)::text = 'Academic staff'::text) OR ((lr.post_category)::text = 'Academic-related staff'::text)) AND ((ps.status_id)::text = 'Current'::text) AND (NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM public.mm_mailinglist_exclude_person
          WHERE (mm_mailinglist_exclude_person.mailinglist_id = ( SELECT mailinglist.id
                   FROM public.mailinglist
                  WHERE ((mailinglist.name)::text = 'chem-academic'::text)))))))
UNION
 SELECT person.crsid,
    NULL::character varying(80) AS person_hid
   FROM ((public.mm_mailinglist_include_person
     LEFT JOIN public.person ON ((mm_mailinglist_include_person.include_person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((ps.person_id = person.id)))
  WHERE ((mm_mailinglist_include_person.mailinglist_id = ( SELECT mailinglist.id
           FROM public.mailinglist
          WHERE ((mailinglist.name)::text = 'chem-academic'::text))) AND ((ps.status_id)::text = 'Current'::text));


ALTER VIEW public.www_mailinglist_academic OWNER TO dev;

--
-- Name: www_telephone_directory_v1; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_telephone_directory_v1 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM ((((((((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id)))
     LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id))
     LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id)))
     LEFT JOIN public.room_hid USING (room_id))
     LEFT JOIN public.supervisor_hid ON ((supervisor_hid.supervisor_id = lr.supervisor_id)))
  WHERE ((ps.status_id)::text = 'Current'::text);


ALTER VIEW public.www_telephone_directory_v1 OWNER TO dev;

--
-- Name: www_telephone_directory_v2; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_telephone_directory_v2 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM ((((((((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id)))
     LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id))
     LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id)))
     LEFT JOIN public.room_hid USING (room_id))
     LEFT JOIN public.supervisor_hid ON ((supervisor_hid.supervisor_id = lr.supervisor_id)))
  WHERE ((ps.status_id)::text = 'Current'::text)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER VIEW public.www_telephone_directory_v2 OWNER TO dev;

--
-- Name: www_telephone_directory_v3; Type: VIEW; Schema: public; Owner: dev
--

CREATE VIEW public.www_telephone_directory_v3 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    www_room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM ((((((((public.person
     LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public._latest_role_v12 lr ON ((person.id = lr.person_id)))
     LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id)))
     LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id))
     LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id)))
     LEFT JOIN public.www_room_hid USING (room_id))
     LEFT JOIN public.supervisor_hid ON ((supervisor_hid.supervisor_id = lr.supervisor_id)))
  WHERE ((ps.status_id)::text = 'Current'::text)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER VIEW public.www_telephone_directory_v3 OWNER TO dev;

--
-- Name: TABLE www_academic_staff_v1; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_academic_staff_v1 TO www_sites;


--
-- Name: TABLE www_committee_members; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_committee_members TO www_sites;


--
-- Name: TABLE www_committee_members_v2; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_committee_members_v2 TO www_sites;


--
-- Name: TABLE www_committee_members_v3; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_committee_members_v3 TO www_sites;


--
-- Name: TABLE www_committee_student_members; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_committee_student_members TO www_sites;


--
-- Name: TABLE www_telephone_directory_v1; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_telephone_directory_v1 TO www_sites;


--
-- Name: TABLE www_telephone_directory_v2; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_telephone_directory_v2 TO www_sites;


--
-- Name: TABLE www_telephone_directory_v3; Type: ACL; Schema: public; Owner: dev
--

GRANT SELECT ON TABLE public.www_telephone_directory_v3 TO www_sites;


--
-- PostgreSQL database dump complete
--

