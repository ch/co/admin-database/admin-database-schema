CREATE OR REPLACE VIEW public._chem_theor_mailinglist
AS
    WITH
        all_rig_members AS (
            SELECT mm_member_research_interest_group.member_id AS person_id,
                'all_rig_members' AS reason
            FROM mm_member_research_interest_group
            JOIN research_interest_group ON mm_member_research_interest_group.research_interest_group_id = research_interest_group.id
            WHERE
                research_interest_group.id = 1 -- use id not name here because they change the name from time to time
        ),
        rig_members_whose_groups_to_add AS (
            SELECT mm_member_research_interest_group.member_id AS person_id
            FROM mm_member_research_interest_group
            JOIN research_interest_group ON mm_member_research_interest_group.research_interest_group_id = research_interest_group.id
            WHERE
                research_interest_group.id = 1 -- use id not name here because they change the name from time to time
                AND mm_member_research_interest_group.is_primary -- primary members only
        ),
        group_members_to_add AS (
            SELECT _hr_role.person_id,
                'group_members_to_add' AS reason
            FROM _hr_role
            JOIN rig_members_whose_groups_to_add ON rig_members_whose_groups_to_add.person_id = _hr_role.supervisor_id
            WHERE
                _hr_role.predicted_role_status_id = 'Current'
                -- exclude support staff
                AND NOT _hr_role.post_category = 'Assistant staff'
                AND NOT _hr_role.post_category = 'Academic-related staff'
        ),
        goodman_group AS (
            SELECT mm_person_research_group.person_id,
                'goodman_group' AS reason
            FROM mm_person_research_group
            JOIN research_group ON research_group.id = mm_person_research_group.research_group_id
            JOIN _hr_role USING (person_id)
            WHERE 
                research_group.name = 'Goodman'
                AND _hr_role.predicted_role_status_id = 'Current'
                -- exclude support staff
                AND NOT _hr_role.post_category = 'Assistant staff'
                AND NOT _hr_role.post_category = 'Academic-related staff'
        ),
        optins AS (
            SELECT include_person_id AS person_id,
            'optin' AS reason
            FROM mm_mailinglist_include_person
            WHERE mailinglist_id = 26
        ),
        theor_mailing_list_eligible AS (
            SELECT person_id, reason
            FROM all_rig_members
            UNION
                SELECT person_id, reason
                FROM group_members_to_add
            UNION
                SELECT person_id, reason
                FROM goodman_group
        ),
        optouts AS (
            SELECT exclude_person_id AS person_id
            FROM mm_mailinglist_exclude_person
            WHERE mailinglist_id = 26
        )
    SELECT DISTINCT
        person.email_address,
        person.crsid,
        person_hid.person_hid
    FROM theor_mailing_list_eligible
    JOIN person ON theor_mailing_list_eligible.person_id = person.id
    JOIN person_hid USING (person_id)
    JOIN _physical_status_v3 USING (person_id)
    WHERE _physical_status_v3.status_id = 'Current'
    AND NOT theor_mailing_list_eligible.person_id IN ( SELECT person_id FROM optouts )
    GROUP BY person_id, email_address, crsid, person_hid
    UNION
        SELECT
            person.email_address,
            person.crsid,
            person_hid.person_hid
        FROM optins
        JOIN person ON optins.person_id = person.id
        JOIN person_hid USING (person_id)
        WHERE NOT optins.person_id IN ( SELECT person_id FROM optouts )
    ORDER BY person_hid
    ;

