CREATE VIEW hotwire3."10_View/Phones/Phones_and_Rooms" AS
WITH current_people_in_room AS (
    SELECT
        room.id AS room_id,
        string_agg(person_hid,'; ') AS people
    FROM public.room
    LEFT JOIN mm_person_room mm ON mm.room_id = room.id
    LEFT JOIN hotwire3.person_hid USING (person_id)
    LEFT JOIN public._physical_status_v3 ps USING (person_id)
    WHERE ps.status_id = 'Current'
    GROUP BY room.id
),
current_people_associated_with_extension AS (
    SELECT
        dept_telephone_number.id AS dept_telephone_number_id,
        string_agg(person_hid,'; ') AS people
    FROM public.dept_telephone_number
    LEFT JOIN mm_person_dept_telephone_number mm ON dept_telephone_number.id = mm.dept_telephone_number_id
    LEFT JOIN hotwire3.person_hid USING (person_id)
    LEFT JOIN public._physical_status_v3 ps USING (person_id)
    WHERE ps.status_id = 'Current'
    GROUP BY dept_telephone_number.id
)
SELECT
    dept_telephone_number.id,
    dept_telephone_number.extension_number,
    dept_telephone_number.room_id,
    room.name as ro_room_short_name,
    room.room_type_id AS ro_room_type_id,
    room.building_id AS ro_building_id,
    room.building_region_id AS ro_building_region_id,
    room.building_floor_id AS ro_building_floor_id,
    current_people_associated_with_extension.people AS ro_people_associated_with_extension,
    current_people_in_room.people AS ro_people_associated_with_room,
    dept_telephone_number.fax,
    dept_telephone_number.personal_line
FROM public.dept_telephone_number
LEFT JOIN public.room ON dept_telephone_number.room_id = room.id
LEFT JOIN current_people_in_room ON current_people_in_room.room_id = room.id
LEFT JOIN current_people_associated_with_extension ON current_people_associated_with_extension.dept_telephone_number_id = dept_telephone_number.id
GROUP BY dept_telephone_number.id, room.name,room.room_type_id, room.building_id, room.building_region_id, room.building_floor_id, current_people_in_room.people, current_people_associated_with_extension.people
;

ALTER VIEW  hotwire3."10_View/Phones/Phones_and_Rooms" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/Phones/Phones_and_Rooms" TO cos;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Phones/Phones_and_Rooms" TO phones_management;

CREATE RULE hotwire3_view_phones_and_rooms_del AS
    ON DELETE TO hotwire3."10_View/Phones/Phones_and_Rooms" DO INSTEAD DELETE FROM public.dept_telephone_number WHERE dept_telephone_number.id = OLD.id;

CREATE FUNCTION hotwire3.phones_and_rooms_upd() RETURNS TRIGGER AS
$body$
BEGIN
INSERT INTO public.dept_telephone_number (
    extension_number,
    room_id,
    fax,
    personal_line
    ) VALUES (
        NEW.extension_number,
        NEW.room_id,
        NEW.fax,
        NEW.personal_line
    )
ON CONFLICT (extension_number) DO UPDATE SET
        extension_number = EXCLUDED.extension_number,
        room_id = EXCLUDED.room_id,
        fax = EXCLUDED.fax,
        personal_line = EXCLUDED.personal_line
RETURNING id INTO NEW.id;
RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.phones_and_rooms_upd() OWNER TO dev;

CREATE TRIGGER hotwire3_phones_and_rooms INSTEAD OF INSERT OR UPDATE ON hotwire3."10_View/Phones/Phones_and_Rooms" FOR EACH ROW EXECUTE FUNCTION hotwire3.phones_and_rooms_upd();
