CREATE OR REPLACE VIEW public._add_ad_accounts
 AS
 SELECT person.crsid,
    person.first_names,
    person.surname,
    person.email_address,
    lr.role_tablename,
    ARRAY( SELECT research_group.name
           FROM mm_person_research_group
             JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
          WHERE mm_person_research_group.person_id = person.id) AS rg,
    lr.post_category,
    person.leaving_date,
    ps.status_id
   FROM person
     JOIN _latest_role_v12 lr ON person.id = lr.person_id
     JOIN _physical_status_v3 ps USING (person_id);

ALTER TABLE public._add_ad_accounts
    OWNER TO dev;

GRANT SELECT ON TABLE public._add_ad_accounts TO ad_accounts;
GRANT ALL ON TABLE public._add_ad_accounts TO dev;

CREATE OR REPLACE VIEW public._add_ad_accounts_research_group_parameters
 AS
 SELECT COALESCE(research_group.active_directory_container, research_group.name) AS name,
    group_fileserver.hostname_for_users,
    group_fileserver.homepath,
    group_fileserver.localhomepath,
    group_fileserver.profilepath,
    group_fileserver.localprofilepath,
    os_class_hid.os_class_hid
   FROM research_group
     LEFT JOIN group_fileserver ON research_group.group_fileserver_id = group_fileserver.id
     LEFT JOIN system_image ON system_image.id = group_fileserver.system_image_id
     LEFT JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN os_class_hid USING (os_class_id);

ALTER TABLE public._add_ad_accounts_research_group_parameters
    OWNER TO cen1001;

GRANT SELECT ON TABLE public._add_ad_accounts_research_group_parameters TO ad_accounts;
GRANT ALL ON TABLE public._add_ad_accounts_research_group_parameters TO cen1001;

