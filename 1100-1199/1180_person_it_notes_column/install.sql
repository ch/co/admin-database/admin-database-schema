ALTER TABLE public.person ADD COLUMN it_notes TEXT;

DROP VIEW hotwire3."10_View/People/COs_View";

CREATE OR REPLACE VIEW hotwire3."10_View/People/COs_View"
 AS
 SELECT a.id,
    a.ro_person_id,
    a.image,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.crsid,
    a.date_of_birth,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.location,
    a.ro_physical_status_id,
    a.ro_estimated_leaving_date,
    a.it_notes as "IT_notes",
    a.do_not_show_on_website AS hide_person_from_website,
    a.hide_photo_from_website,
    a.hide_email AS hide_email_from_website,
    a.is_spri,
    a.managed_mail_domain_target,
    a.managed_mail_domain_optout,
    a.ro_final_managed_mail_domain_destination,
    a.ro_auto_banned_from_network,
    a.override_auto_ban,
    a.ro_network_ban_log,
    a._cssclass
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo AS image,
            person.surname,
            person.first_names,
            person.title_id,
            person.name_suffix,
            person.email_address,
            person.crsid,
            person.date_of_birth,
            person.do_not_show_on_website,
            person.hide_photo_from_website,
            person.hide_email,
            person.is_spri,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            person.location,
            _physical_status.status_id AS ro_physical_status_id,
            LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
            person.it_notes,
            managed_mail_domain_v3.target AS ro_final_managed_mail_domain_destination,
            person.managed_mail_domain_target::character varying AS managed_mail_domain_target,
            person.managed_mail_domain_optout,
            person.auto_banned_from_network AS ro_auto_banned_from_network,
            person.override_auto_ban,
            person.network_ban_log::text AS ro_network_ban_log,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN person_photo ON person.id = person_photo.person_id
             LEFT JOIN _current_hr_role_for_person _latest_role ON person.id = _latest_role.person_id
             LEFT JOIN cache.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
             LEFT JOIN apps.managed_mail_domain_v3 ON person.crsid::text = managed_mail_domain_v3.alias::text) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/COs_View"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/COs_View" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO hr;


CREATE RULE hotwire3_people_cos_view_del AS
    ON DELETE TO hotwire3."10_View/People/COs_View"
    DO INSTEAD
(DELETE FROM person
  WHERE (person.id = old.id));

CREATE TRIGGER hotwire3_people_cos_view_ins
    INSTEAD OF INSERT
    ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig();


CREATE TRIGGER hotwire3_people_cos_view_upd
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig();

CREATE OR REPLACE FUNCTION hotwire3.people_cos_view_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    IF NEW.id IS NOT NULL THEN
        UPDATE
            person
        SET
            surname = NEW.surname,
            first_names = NEW.first_names,
            title_id = NEW.title_id,
            name_suffix = NEW.name_suffix,
            email_address = NEW.email_address,
            crsid = NEW.crsid,
            date_of_birth = NEW.date_of_birth,
            location = NEW.location,
            it_notes = NEW."IT_notes",
            do_not_show_on_website = NEW.hide_person_from_website,
            hide_photo_from_website = NEW.hide_photo_from_website,
            hide_email = NEW.hide_email_from_website,
            is_spri = NEW.is_spri,
            managed_mail_domain_target = NEW.managed_mail_domain_target,
            managed_mail_domain_optout = NEW.managed_mail_domain_optout,
            override_auto_ban = NEW.override_auto_ban
        WHERE
            person.id = NEW.id;
    ELSE
        NEW.id := nextval('person_id_seq');
        INSERT INTO person (id, surname, first_names, title_id, name_suffix, email_address, crsid, date_of_birth, location, it_notes, do_not_show_on_website, hide_photo_from_website, hide_email, is_spri, managed_mail_domain_target, managed_mail_domain_optout, override_auto_ban)
            VALUES (NEW.id, NEW.surname, NEW.first_names, NEW.title_id, NEW.name_suffix, NEW.email_address, NEW.crsid, NEW.date_of_birth, NEW.location, NEW."IT_notes", coalesce(NEW.hide_person_from_website, 'f'), coalesce(NEW.hide_photo_from_website, 'f'), NEW.hide_email_from_website, NEW.is_spri, NEW.managed_mail_domain_target, NEW.managed_mail_domain_optout, NEW.override_auto_ban);
    END IF;

    IF NEW.image IS NOT NULL AND NEW.image IS DISTINCT FROM OLD.image
    -- We have a picture and it's a new one. Has to be 'is distinct from' not
    -- <> for case where OLD.image is null. We do need to check the image is
    -- different between OLD and NEW because the image data in the OLD column
    -- has metadata added by the view definition, which we don't want to save
    -- in the bytea column by mistake. But if the image has changed we can
    -- assume what we've got is a new photo which should have been uploaded
    -- without any metadata being prepended, and so we can safely save that
    THEN
        INSERT INTO person_photo ( person_id, photo ) VALUES ( NEW.id, NEW.image )
        ON CONFLICT (person_id) DO UPDATE SET photo = NEW.image;
    ELSIF NEW.image IS NULL
    -- Hotwire doesn't let us set an existing bytea to null yet, but this is for when it does
    THEN
        DELETE FROM person_photo WHERE person_id = NEW.id;
    END IF;

    PERFORM
        fn_mm_array_update (NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar, 'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.research_group_id, 'mm_person_research_group'::varchar, 'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.room_id, 'mm_person_room'::varchar, 'person_id'::varchar, 'room_id'::varchar, NEW.id);
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.people_cos_view_trig()
    OWNER TO dev;
