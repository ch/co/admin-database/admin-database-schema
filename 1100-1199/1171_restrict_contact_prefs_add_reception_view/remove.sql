-- recreate personnel_phone with contact prefs
DROP VIEW hotwire3."10_View/People/Personnel_Phone";
CREATE VIEW hotwire3."10_View/People/Personnel_Phone"
 AS
 SELECT a.id,
    a.ro_person::character varying(80) AS ro_person,
    a.ro_image,
    a.surname,
    a.first_names,
    a.contact_preference_id,
    a.dept_telephone_number_id,
    a.external_work_numbers,
    a.ro_teams_chat::character varying(80) AS ro_teams_chat,
    a.other_contact_details,
    a.room_id,
    a.ro_supervisor_id,
    a.email_address,
    a.location,
    a.ro_post_category_id,
    a.ro_physical_status_id,
    a.is_external,
    a._cssclass,
    a._title_hid,
    a._known_as
   FROM ( SELECT person.id,
            ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS ro_person,
            convert_to('Content-type: image/jpeg

'::text, 'UTF8'::name) || person_photo.photo AS ro_image,
            person.surname,
            person.first_names,
            mm_person_contact_preference.contact_preference_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            person.external_work_phone_numbers AS external_work_numbers,
                CASE
                    WHEN _physical_status.status_id::text = 'Current'::text THEN person.crsid::text || '@cam.ac.uk'::text
                    ELSE NULL::text
                END AS ro_teams_chat,
            mm_person_contact_preference.further_details AS other_contact_details,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            person.email_address,
            person.location,
            _latest_role.post_category_id AS ro_post_category_id,
            _physical_status.status_id AS ro_physical_status_id,
            person.is_external,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN person.is_external = true THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass,
            title_hid.abbrev_title AS _title_hid,
            person.known_as AS _known_as
           FROM person
             LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
             LEFT JOIN person_photo ON person.id = person_photo.person_id
             LEFT JOIN title_hid USING (title_id)
             LEFT JOIN public.mm_person_contact_preference ON mm_person_contact_preference.person_id = person.id
             LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id) a
  ORDER BY a.surname, a._title_hid, a._known_as;

ALTER TABLE hotwire3."10_View/People/Personnel_Phone"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO accounts;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO building_security;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO chematcam;
GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Phone" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO interviewtest;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO phonebook_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO phones;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO reception;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Phone" TO reception_cover;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Phone" TO student_management_ro;


CREATE OR REPLACE RULE hotwire3_view_personnel_phone_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_Phone"
    DO INSTEAD
( UPDATE person SET surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address, is_external = new.is_external
  WHERE (person.id = old.id);
  INSERT INTO public.mm_person_contact_preference ( person_id, contact_preference_id, further_details ) VALUES ( OLD.id, NEW.contact_preference_id, NEW.other_contact_details ) ON CONFLICT (person_id) DO UPDATE SET contact_preference_id = EXCLUDED.contact_preference_id, further_details = EXCLUDED.further_details;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
);

UPDATE hotwire3."hw_User Preferences" SET preference_value = '10_View/People/Personnel_Phone' WHERE preference_id = 9 AND preference_value = '10_View/People/Reception_View';

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Reception_View';

DROP VIEW hotwire3."10_View/People/Reception_View";
