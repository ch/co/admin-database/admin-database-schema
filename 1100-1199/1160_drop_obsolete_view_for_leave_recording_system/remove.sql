CREATE VIEW www.leave_reporting
 AS
 SELECT person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    supervisor.crsid AS supervisor_crsid,
    supervisor.email_address AS supervisor_email,
    (COALESCE(supervisor.known_as, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS supervisor_name
   FROM _latest_role_v12 lr
     JOIN person ON lr.person_id = person.id
     LEFT JOIN person supervisor ON lr.supervisor_id = supervisor.id
     JOIN _physical_status_v3 ps USING (person_id)
  WHERE lr.post_category::text = 'Assistant staff'::text AND ps.status_id::text = 'Current'::text AND lr.status::text = 'Current'::text;

ALTER TABLE www.leave_reporting
    OWNER TO dev;

GRANT ALL ON TABLE www.leave_reporting TO dev;
GRANT SELECT ON TABLE www.leave_reporting TO www_leave_reporting;

