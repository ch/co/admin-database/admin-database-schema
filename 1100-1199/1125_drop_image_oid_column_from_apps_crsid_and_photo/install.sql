DROP VIEW apps.crsid_and_photo;

CREATE OR REPLACE VIEW apps.crsid_and_photo
 AS
 SELECT person.crsid,
    (COALESCE(person.known_as, person.first_names, ''::character varying)::text || ' '::text) || person.surname::text AS name,
    person_photo.photo
   FROM person
   LEFT JOIN person_photo ON person.id = person_photo.person_id
  WHERE person.crsid IS NOT NULL;

ALTER TABLE apps.crsid_and_photo
    OWNER TO dev;
COMMENT ON VIEW apps.crsid_and_photo
    IS 'Used by the computer office leavers process';

GRANT SELECT ON TABLE apps.crsid_and_photo TO cos;
GRANT ALL ON TABLE apps.crsid_and_photo TO dev;
GRANT SELECT ON TABLE apps.crsid_and_photo TO leavers_trigger;


