-- view apps._space_people_using_other_groups_space depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps._space_people_using_other_groups_space
 AS
 SELECT research_group.name AS research_group,
    person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    room.name AS room,
    room_group.name AS room_group
   FROM research_group
     JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id)
     JOIN _latest_role_v12 lr USING (person_id)
     JOIN mm_person_room USING (person_id)
     JOIN room ON mm_person_room.room_id = room.id
     JOIN research_group room_group ON room.responsible_group_id = room_group.id
  WHERE ps.status_id::text = 'Current'::text AND NOT (room_group.name::text IN ( SELECT rg.name
           FROM mm_person_research_group mm1
             JOIN research_group rg ON mm1.research_group_id = rg.id
          WHERE mm1.person_id = person_hid.person_id));


-- view apps.academic_staff_crsids depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.academic_staff_crsids
 AS
 SELECT person.crsid
   FROM person
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE ps.status_id::text = 'Current'::text AND (lr.post_category_id = 'sc-1'::text OR lr.post_category_id = 'sc-9'::text OR person.counts_as_academic = true) AND person.crsid IS NOT NULL;


-- view apps.firstaiders_to_pay depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.firstaiders_to_pay
 AS
 SELECT person.surname,
    person.first_names,
    person.email_address,
    lr.post_category,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider_funding.funding
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     LEFT JOIN firstaider_funding ON firstaider_funding.id = firstaider.firstaider_funding_id
     LEFT JOIN _latest_role_v12 lr ON firstaider.person_id = lr.person_id
     JOIN _physical_status_v3 ps ON ps.person_id = firstaider.person_id
  WHERE ps.status_id::text = 'Current'::text AND firstaider.requalify_date >= 'now'::text::date
  ORDER BY person.surname;

-- view apps.firstaiders_who_have_left depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.firstaiders_who_have_left
 AS
 SELECT person.surname,
    person.first_names,
    person.email_address,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider_funding.funding,
    person.leaving_date
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     LEFT JOIN firstaider_funding ON firstaider_funding.id = firstaider.firstaider_funding_id
     JOIN _physical_status_v3 ps ON ps.person_id = firstaider.person_id
  WHERE ps.status_id::text = 'Past'::text;


-- view apps.process_leavers_v3 depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.process_leavers_v3
 AS
 SELECT person.surname,
    person.first_names,
    title_hid.long_title AS title,
    person.email_address AS email,
    person_hid.person_hid AS full_name,
    supervisor_hid.supervisor_hid AS supervisor,
    supervisor.email_address AS supervisor_email,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date,
    person_futuremost_role.post_category,
    (COALESCE(title_hid.salutation_title, person.first_names)::text || ' '::text) || person.surname::text AS salutation,
    (COALESCE(pi_title.salutation_title, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS pi_salutation,
    (((COALESCE(title_hid.long_title, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS person_title_surname,
        CASE
            WHEN firstaider.id IS NOT NULL THEN 't'::text
            ELSE 'f'::text
        END AS is_firstaider
   FROM person
     LEFT JOIN title_hid USING (title_id)
     JOIN person_hid ON person.id = person_hid.person_id
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
     LEFT JOIN person supervisor ON person_futuremost_role.supervisor_id = supervisor.id
     LEFT JOIN supervisor_hid USING (supervisor_id)
     LEFT JOIN title_hid pi_title ON supervisor.title_id = pi_title.title_id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN firstaider ON person.id = firstaider.person_id
  WHERE ps.status_id::text = 'Current'::text AND person_futuremost_role.post_category::text <> 'Assistant staff'::text AND person_futuremost_role.post_category::text <> 'Part III'::text
  ORDER BY person.surname, person.first_names;

-- view apps.www_academic_staff_v2 depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.www_academic_staff_v2
 AS
 SELECT person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    person.counts_as_academic
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE ps.status_id::text = 'Current'::text AND person.do_not_show_on_website IS NOT FALSE;


-- view apps.www_academic_staff_v3 depends on view _physical_status_v2

CREATE OR REPLACE VIEW apps.www_academic_staff_v3
 AS
 SELECT person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    person.counts_as_academic
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE (ps.status_id::text = ANY (ARRAY['Current'::text, 'Future'::text])) AND person.do_not_show_on_website IS NOT TRUE;

-- view _add_ad_accounts depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._add_ad_accounts
 AS
 SELECT person.crsid,
    person.first_names,
    person.surname,
    person.email_address,
    lr.role_tablename,
    ARRAY( SELECT research_group.name
           FROM mm_person_research_group
             JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
          WHERE mm_person_research_group.person_id = person.id) AS rg,
    lr.post_category,
    person.leaving_date,
    ps.status_id
   FROM person
     JOIN _latest_role_v12 lr ON person.id = lr.person_id
     JOIN _physical_status_v3 ps USING (person_id);


-- view _chem_academic_mailinglist_2018 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chem_academic_mailinglist_2018
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE person.email_address IS NOT NULL AND (_latest_role_v12.post_category::text = 'Academic staff'::text OR _latest_role_v12.post_category::text = 'Teaching Fellow'::text OR person.counts_as_academic = true) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-academic-2018'::text))))
UNION
 SELECT person.email_address,
    person.crsid,
    NULL::character varying(80) AS person_hid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE ps.status_id::text = 'Current'::text AND person.email_address IS NOT NULL AND mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-academic-2018'::text));

-- view _chem_faculty_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chem_faculty_mailinglist
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE (lr.post_category::text = 'Academic staff'::text OR lr.post_category::text = 'Teaching Fellow'::text OR person.counts_as_academic = true) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-faculty'::text))));

-- view _chem_firewardens_mailinglist depends on view _physical_status_v2
CREATE OR REPLACE VIEW public._chem_firewardens_mailinglist
 AS
 SELECT person.email_address
   FROM ( SELECT fire_warden.person_id
           FROM fire_warden
        UNION
         SELECT mm_mailinglist_include_person.include_person_id AS person_id
           FROM mm_mailinglist_include_person
             JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
          WHERE mailinglist.name::text = 'chem-firewardens'::text) fw
     JOIN person ON fw.person_id = person.id
     JOIN _physical_status_v3 ps USING (person_id)
  WHERE ps.status_id::text = 'Current'::text;


-- view _chemacademic_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chemacademic_mailinglist
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE (lr.post_category::text = 'Academic staff'::text OR lr.post_category::text = 'Academic-related staff'::text OR person.counts_as_academic = true) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-academic'::text)))) AND person.email_address IS NOT NULL
UNION
 SELECT person.email_address,
    person.crsid,
    NULL::character varying(80) AS person_hid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-academic'::text)) AND ps.status_id::text = 'Current'::text AND person.email_address IS NOT NULL;


-- view _chemadmindatabase_mailinglist depends on view _physical_status_v2

-- this was for the chem-admin-database@lists.cam.ac.uk mailing list which has been deleted
DROP VIEW public._chemadmindatabase_mailinglist;

-- view _chemassistantstaff_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chemassistantstaff_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM post_history
     LEFT JOIN person ON post_history.person_id = person.id
     LEFT JOIN _all_roles_v12 _all_roles ON person.id = _all_roles.person_id AND _all_roles.role_tablename = 'post_history'::text
  WHERE _all_roles.status::text = 'Current'::text AND _all_roles.post_category::text = 'Assistant staff'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-assistant-staff'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-assistant-staff'::text)) AND ps.status_id::text = 'Current'::text;

-- view _chemcomprep_mailinglist depends on view _physical_status_v2

-- dropping to fix ownership/perms
DROP VIEW public._chemcomprep_mailinglist;

CREATE OR REPLACE VIEW public._chemcomprep_mailinglist
 AS
( SELECT DISTINCT person.email_address
   FROM mm_research_group_computer_rep
     LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
  WHERE ps.status_id::text = 'Current'::text OR person.is_spri = true
  ORDER BY person.email_address)
UNION
 SELECT person.email_address
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-comp-rep'::text)) AND ps.status_id::text = 'Current'::text;

ALTER TABLE public._chemcomprep_mailinglist
    OWNER TO dev;

GRANT SELECT ON TABLE public._chemcomprep_mailinglist TO leavers_trigger;
GRANT SELECT ON TABLE public._chemcomprep_mailinglist TO mailinglist_readers;
GRANT SELECT ON TABLE public._chemcomprep_mailinglist TO mailinglists;

-- view _chemgeneral_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chemgeneral_mailinglist
 AS
 SELECT person.email_address
   FROM person
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE ps.status_id::text = 'Current'::text AND lr.post_category::text <> 'Part III'::text AND lr.post_category::text <> 'Part II'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-general'::text))))
UNION
 SELECT person.email_address
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-general'::text)) AND ps.status_id::text = 'Current'::text;

-- view _chemgeneral_mailinglist_v2 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chemgeneral_mailinglist_v2
 AS
 SELECT person.email_address,
    person.crsid
   FROM person
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE ps.status_id::text = 'Current'::text AND lr.post_category::text <> 'Part III'::text AND lr.post_category::text <> 'Part II'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-general'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-general'::text)) AND ps.status_id::text = 'Current'::text;

-- view _chempg_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chempg_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM postgraduate_studentship
     LEFT JOIN person ON postgraduate_studentship.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     LEFT JOIN _all_roles_v12 ON person.id = _all_roles_v12.person_id AND _all_roles_v12.role_tablename = 'postgraduate_studentship'::text
  WHERE _all_roles_v12.status::text = 'Current'::text AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-pg'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-pg'::text)) AND ps.status_id::text = 'Current'::text;

-- view _chempostdoc_mailinglist depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._chempostdoc_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM post_history
     LEFT JOIN person ON post_history.person_id = person.id
     LEFT JOIN _all_roles_v12 _all_roles ON person.id = _all_roles.person_id AND _all_roles.role_tablename = 'post_history'::text
  WHERE _all_roles.status::text = 'Current'::text AND (_all_roles.post_category::text = 'PDRA'::text OR _all_roles.post_category::text = 'Senior PDRA'::text OR _all_roles.post_category::text = 'Research Fellow'::text OR _all_roles.post_category::text = 'Principal Research Associate'::text) AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-postdoc'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-postdoc'::text)) AND ps.status_id::text = 'Current'::text;


-- view _room_current_occupancy depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._room_current_occupancy
 AS
 SELECT room.id,
    room.name,
    count(mm_person_room.person_id) AS current_occupancy
   FROM room
     LEFT JOIN mm_person_room ON room.id = mm_person_room.room_id
     LEFT JOIN _physical_status_v3 ps USING (person_id)
  WHERE ps.status_id::text = 'Current'::text
  GROUP BY room.id, room.name;

ALTER TABLE public._room_current_occupancy
    OWNER TO dev;

REVOKE ALL ON TABLE public._room_current_occupancy FROM cen1001;
GRANT SELECT ON TABLE public._room_current_occupancy TO mgmt_ro;
GRANT SELECT ON TABLE public._room_current_occupancy TO space_management;

-- view _space_hosted_people_by_group depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._space_hosted_people_by_group
 AS
 SELECT DISTINCT person_hid.person_hid AS name,
    room.name AS room,
    lr.post_category,
    lr.estimated_leaving_date,
    supervisor_hid.supervisor_hid,
    research_group.name AS research_group,
    supervisor.crsid AS supervisor_crsid
   FROM room
     JOIN mm_person_room ON room.id = mm_person_room.room_id
     JOIN person ON mm_person_room.person_id = person.id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id)
     JOIN _latest_role_v12 lr USING (person_id)
     JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
     JOIN person supervisor ON lr.supervisor_id = supervisor.id
     JOIN research_group ON room.responsible_group_id = research_group.id
     JOIN person hog ON research_group.head_of_group_id = hog.id
  WHERE ps.status_id::text = 'Current'::text AND hog.crsid::text <> supervisor.crsid::text
  ORDER BY person_hid.person_hid, room.name, lr.post_category, lr.estimated_leaving_date, supervisor_hid.supervisor_hid, research_group.name, supervisor.crsid;

-- view _space_people_by_research_group depends on view _physical_status_v2

CREATE OR REPLACE VIEW public._space_people_by_research_group
 AS
( SELECT DISTINCT person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id) AS rooms,
    person.email_address,
    ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id) AS phones,
    research_group.name AS research_group
   FROM person
     JOIN person_hid ON person.id = person_hid.person_id
     JOIN _physical_status_v3 ps USING (person_id)
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     LEFT JOIN research_group ON research_group.id = mm_person_research_group.research_group_id
  WHERE ps.status_id::text = 'Current'::text
  ORDER BY person_hid.person_hid, lr.post_category, lr.estimated_leaving_date, (ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id)), person.email_address, (ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id)), research_group.name)
UNION
 SELECT person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    ARRAY( SELECT ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
           FROM room
             JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
             JOIN mm_person_room ON room.id = mm_person_room.room_id
             JOIN person occupant ON mm_person_room.person_id = occupant.id
          WHERE occupant.id = person.id) AS rooms,
    person.email_address,
    ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
             JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
          WHERE p2.id = person.id) AS phones,
    research_group.name AS research_group
   FROM research_group
     JOIN person ON person.id = research_group.head_of_group_id
     JOIN person_hid ON person.id = person_hid.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id;


-- view rig_member_hid depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.rig_member_hid
 AS
 SELECT person.id AS rig_member_id,
    ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS rig_member_hid
   FROM person
     LEFT JOIN title_hid USING (title_id)
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
  WHERE ps.status_id::text <> 'Past'::text
  ORDER BY person.surname, (COALESCE(person.known_as, person.first_names)), (COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text));


-- view www_academic_staff_v1 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_academic_staff_v1
 AS
 SELECT person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE ps.status_id::text = 'Current'::text;


-- view www_committee_members_v3 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_committee_members_v3
 AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE (lr.post_category_id = 'sc-1'::text OR lr.post_category_id = 'sc-2'::text OR lr.post_category_id = 'sc-3'::text OR lr.post_category_id = 'sc-9'::text OR lr.post_category_id = 'v-8'::text OR lr.post_category_id = 'sc-4'::text OR lr.post_category_id = 'sc-7'::text OR lr.post_category_id = 'sc-6'::text) AND ps.status_id::text = 'Current'::text
  ORDER BY person_hid.person_hid;

-- view www_committee_student_members depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_committee_student_members
 AS
 SELECT person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
  WHERE (lr.post_category_id = 'pg-1'::text OR lr.post_category_id = 'pg-2'::text OR lr.post_category_id = 'pg-3'::text OR lr.post_category_id = 'pg-4'::text OR lr.post_category_id = 'e-1'::text OR lr.post_category_id = 'e-2'::text OR lr.post_category_id = 'e-3'::text) AND ps.status_id::text = 'Current'::text
  ORDER BY person_hid.person_hid;

-- view www_mailinglist_academic depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_mailinglist_academic
 AS
 SELECT person.crsid,
    person_hid.person_hid
   FROM person
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  WHERE (lr.post_category::text = 'Academic staff'::text OR lr.post_category::text = 'Academic-related staff'::text) AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-academic'::text))))
UNION
 SELECT person.crsid,
    NULL::character varying(80) AS person_hid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-academic'::text)) AND ps.status_id::text = 'Current'::text;


-- view www_telephone_directory_v1 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_telephone_directory_v1
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
     LEFT JOIN room_hid USING (room_id)
     LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
  WHERE ps.status_id::text = 'Current'::text;


-- view www_telephone_directory_v2 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_telephone_directory_v2
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
     LEFT JOIN room_hid USING (room_id)
     LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
  WHERE ps.status_id::text = 'Current'::text
  ORDER BY person.surname, (COALESCE(person.known_as, person.first_names));

-- view www_telephone_directory_v3 depends on view _physical_status_v2

CREATE OR REPLACE VIEW public.www_telephone_directory_v3
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    www_room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
   FROM person
     LEFT JOIN person_hid ON person_hid.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
     LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
     LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
     LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
     LEFT JOIN www_room_hid USING (room_id)
     LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
  WHERE ps.status_id::text = 'Current'::text
  ORDER BY person.surname, (COALESCE(person.known_as, person.first_names));


-- view www.first_aiders_v3 depends on view _physical_status_v2

CREATE OR REPLACE VIEW www.first_aiders_v3
 AS
 SELECT DISTINCT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid AS person,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained,
        CASE
            WHEN firstaider.mental_health_awareness_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS mental_health_awareness_trained,
    building_hid.building_hid AS building,
    building_floor_hid.building_floor_hid AS building_floor,
    array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms,
    ps.status_id AS status,
    (building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END AS floor_sort,
        CASE
            WHEN firstaider.requalify_date > ('now'::text::date - '1 mon'::interval) THEN true
            ELSE false
        END AS qualification_up_to_date
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     LEFT JOIN mm_person_room USING (person_id)
     LEFT JOIN room ON mm_person_room.room_id = room.id
     LEFT JOIN building_hid ON room.building_id = building_hid.building_id
     LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
     JOIN _physical_status_v3 ps USING (person_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN '-0.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END), firstaider.id, (person.image_lo::bigint), person_hid.person_hid, firstaider.firstaider_funding_id, firstaider.requalify_date, (
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END), building_hid.building_hid, building_floor_hid.building_floor_hid, (array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text)), ps.status_id;


-- view www.firstaiders depends on view _physical_status_v2

CREATE OR REPLACE VIEW www.firstaiders
 AS
 SELECT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN building_hid USING (building_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN building_floor_hid USING (building_floor_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
          WHERE p2.id = firstaider.person_id
          ORDER BY room.name), '/'::text) AS ro_room,
    ps.status_id AS status
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 ps USING (person_id);

-- view www.firstaiders_v2 depends on view _physical_status_v2

CREATE OR REPLACE VIEW www.firstaiders_v2
 AS
 SELECT DISTINCT firstaider.id,
    person.image_lo::bigint AS _image_oid,
    person_hid.person_hid AS person,
    firstaider.firstaider_funding_id,
    firstaider.requalify_date,
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained,
    building_hid.building_hid AS building,
    building_floor_hid.building_floor_hid AS building_floor,
    array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms,
    ps.status_id AS status
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     JOIN person_hid USING (person_id)
     LEFT JOIN mm_person_room USING (person_id)
     LEFT JOIN room ON mm_person_room.room_id = room.id
     LEFT JOIN building_hid ON room.building_id = building_hid.building_id
     LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
     JOIN _physical_status_v3 ps USING (person_id)
  ORDER BY building_hid.building_hid, building_floor_hid.building_floor_hid, person_hid.person_hid, firstaider.id, (person.image_lo::bigint), firstaider.firstaider_funding_id, firstaider.requalify_date, (
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END), (array_to_string(ARRAY( SELECT r2.name
           FROM room r2
             JOIN mm_person_room mp2 ON r2.id = mp2.room_id
             JOIN person p2 ON mp2.person_id = p2.id
          WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text)), ps.status_id;


