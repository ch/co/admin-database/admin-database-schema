DROP FUNCTION www.person_status_for_supervisor(_person text, _supervisor text);

CREATE OR REPLACE FUNCTION www.person_status_for_supervisor(_person text, _supervisors text) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
declare
  _supervisor_crsids text[];
  _person_id int;
  _supervisor_ids int[];
begin

   -- Accept either a single CRSID or a comma-separated list. The primary (only?)
   -- caller of this function is Drupal, where the db abstraction layer doesn't
   -- support lists (presumably because they're not sql standard)
   _supervisor_crsids = string_to_array(replace(_supervisors, ' ', ''), ',');

   IF _person = any(_supervisor_crsids) THEN return 'Current'; END IF;

   SELECT person.id INTO _person_id FROM person WHERE crsid=_person;
   SELECT array_agg(person.id) INTO _supervisor_ids FROM person WHERE crsid=any(_supervisor_crsids);

   IF EXISTS ( select person_id FROM www._all_roles_v1  WHERE person_id=_person_id AND (supervisor_id=any(_supervisor_ids) OR co_supervisor_id=any(_supervisor_ids)) AND status='Current') THEN
     return 'Current';
  ELSIF EXISTS ( select person_id FROM www._all_roles_v1  WHERE person_id=_person_id AND (supervisor_id=any(_supervisor_ids) OR co_supervisor_id=any(_supervisor_ids)) AND status='Past') THEN
     return 'Past';
   ELSE
     return 'Unknown';
   END IF;
 
end;

$$;


ALTER FUNCTION www.person_status_for_supervisor(_person text, _supervisors text) OWNER TO dev;

GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO public;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO www_sites;

