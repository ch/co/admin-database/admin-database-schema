CREATE OR REPLACE VIEW public._chemgeneral_mailinglist
 AS
 SELECT person.email_address
   FROM person
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-general'::text))))
UNION
 SELECT person.email_address
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-general'::text)) AND ps.status_id::text = 'Current'::text;
