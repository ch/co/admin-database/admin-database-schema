SET ROLE dev;

-- Adding the role below does not get reversed by the remove.sql script; it edits the
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'registration_form_state') THEN
      CREATE ROLE registration_form_state NOLOGIN;
   END IF;
END
$do$;

GRANT registration_form_state TO dev;
GRANT SELECT ON registration.form TO registration_form_state;

DROP VIEW hotwire3."10_View/People/Incomplete_Registration_Forms";
DROP VIEW hotwire3."10_View/People/Registration";
DROP FUNCTION registration.state_of_form(uuid);

CREATE FUNCTION registration.state_of_form(
	form_uuid uuid)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    SECURITY DEFINER
    SET search_path = registration,public,pg_temp
AS $BODY$
DECLARE
    form registration.form%ROWTYPE;
    reason text[] := '{}';
BEGIN

-- The logic in here must match that in the registration web app

SELECT * INTO form FROM registration.form WHERE uuid = form_uuid;

IF form.submitted IS DISTINCT FROM true THEN
    reason = array_append(reason,'Main form not submitted');
END IF;

-- originally the safety inductions were done on separate paper forms
IF form.separate_safety_form = false THEN
    IF form.safety_submitted IS DISTINCT FROM true THEN
        reason = array_append(reason,'Safety form not submitted');
    END IF;

    -- if null, the date comparison returns false
    IF form.safety_induction_signed_off_date > current_date OR form.safety_induction_signed_off_date IS NULL THEN
	reason = array_append(reason,'Safety induction not signed off');
    END IF;
END IF;

IF form.safety_training_needs_signoff AND (form.safety_training_signed_off_date > current_date OR form.safety_training_signed_off_date IS NULL) THEN
    reason = array_append(reason,'Safety training requires signing off which is not yet done');
END IF;

IF reason = '{}' THEN
    RETURN 'Complete';
ELSE
    RETURN array_to_string(reason, E'\n');
END IF;

RETURN reason;

END;
$BODY$;

GRANT CREATE ON SCHEMA registration TO registration_form_state;
ALTER FUNCTION registration.state_of_form(uuid) OWNER TO registration_form_state;
REVOKE CREATE ON SCHEMA registration FROM registration_form_state;
REVOKE ALL ON FUNCTION registration.state_of_form(uuid) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION registration.state_of_form(uuid) TO hr,cos,starters_registration,student_management;

CREATE VIEW hotwire3."10_View/People/Incomplete_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form._form_started AS form_started_on,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signer_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_separate_safety_form,
    registration.state_of_form(form.uuid) AS reason,
    form.uuid AS form_uuid,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE registration.state_of_form(form.uuid) <> 'Complete'::text;

ALTER TABLE hotwire3."10_View/People/Incomplete_Registration_Forms"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO student_management;

CREATE VIEW hotwire3."10_View/People/Registration"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_separate_safety_form,
    form.hide_from_website,
    form.previously_registered,
        CASE
            WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
            ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid::character varying, form.date_of_birth)
        END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    false AS import_this_record,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE registration.state_of_form(form.uuid) = 'Complete'::text AND form._imported_on IS NULL;

ALTER TABLE hotwire3."10_View/People/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Registration" TO hr;


CREATE RULE hotwire3_view_registration_del AS
    ON DELETE TO hotwire3."10_View/People/Registration"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.form_id = old.id));

CREATE TRIGGER registration_form_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Registration"
    FOR EACH ROW
    EXECUTE FUNCTION registration.people_registration_upd();

