ALTER TABLE public.user_account ADD CONSTRAINT either_person_or_expiry_date_set CHECK ( person_id IS NOT NULL OR expiry_date IS NOT NULL OR is_disabled = 't' );

ALTER TABLE public.user_account DROP CONSTRAINT "useraccount_fk_personid";

ALTER TABLE public.user_account ADD CONSTRAINT "useraccount_fk_personid" FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE SET NULL;

CREATE OR REPLACE FUNCTION public.fn_createuser(_crsid character varying)
  RETURNS void AS
$BODY$
declare
 user_account_id bigint;
begin
 -- create postgres user account
  perform usename from pg_catalog.pg_user where usename=_crsid;
  if not found then
   execute format('create user %I with in role _autoadded, selfservice_access',_crsid); 
  end if;
 -- Create or update entry in user_account table
  if _crsid is not null then
   select id from public.user_account where username=_crsid into user_account_id;
   if not found then
    -- we set an expiry date, as if the person never becomes Current the normal expiry apparatus won't take effect and the account will never be closed
    insert into public.user_account (person_id,username,chemnet_token,expiry_date) values ((select id from public.person where crsid=_crsid),_crsid,public.random_string_lower(16),(current_date + interval '1 month'));
   else
    update public.user_account set person_id = (select id from public.person where crsid=_crsid) where id = user_account_id;
   end if;
  end if; 
 end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION public.fn_createuser(character varying) SET search_path=public, pg_temp;

ALTER FUNCTION public.fn_createuser(character varying)
  OWNER TO useradder;
COMMENT ON FUNCTION public.fn_createuser(character varying) IS 'A Privileged function which ensures that a postgres account exists for the user.';

