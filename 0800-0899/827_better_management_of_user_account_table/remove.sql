ALTER TABLE public.user_account DROP CONSTRAINT either_person_or_expiry_date_set;

ALTER TABLE public.user_account DROP CONSTRAINT "useraccount_fk_personid";

ALTER TABLE public.user_account ADD CONSTRAINT "useraccount_fk_personid" FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE CASCADE;

CREATE OR REPLACE FUNCTION public.fn_createuser(_crsid character varying)
  RETURNS void AS
$BODY$
begin
 -- create postgres user account
  perform usename from pg_catalog.pg_user where usename=_crsid;
  if not found then
   execute format('create user %I with in role _autoadded, selfservice_access',_crsid); 
  end if;
 -- Create entry in user_account table
  if _crsid is not null then
   perform id from public.user_account where username=_crsid;
   if not found then
    insert into public.user_account (person_id,username,chemnet_token) values ((select id from public.person where crsid=_crsid),_crsid,public.random_string_lower(16));
   end if;
  end if; 
 end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION public.fn_createuser(character varying) SET search_path=public, pg_temp;

ALTER FUNCTION public.fn_createuser(character varying)
  OWNER TO useradder;
COMMENT ON FUNCTION public.fn_createuser(character varying) IS 'A Privileged function which ensures that a postgres account exists for the user.';

