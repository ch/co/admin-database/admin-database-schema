-- apps.bouncing_admitto_accounts
CREATE OR REPLACE VIEW apps.bouncing_admitto_accounts AS
SELECT
    pt.long_title AS ptitle,
    p.surname AS psurname,
    COALESCE(p.known_as, p.first_names) AS pfirst,
    p.email_address AS pemail,
    p.crsid,
    ps.status_id AS pstatus,
    ts.salutation_title AS stitle,
    s.email_address AS semail,
    s.surname AS ssurname
FROM
    person p
    LEFT JOIN title_hid pt ON p.title_id = pt.title_id
    JOIN _latest_role_v12 r ON r.person_id = p.id
    JOIN _physical_status_v3 ps ON p.id = ps.person_id
    JOIN person s ON r.supervisor_id = s.id
    LEFT JOIN title_hid ts ON ts.title_id = s.title_id;

-- view apps.leavers_pi_details depends on view _latest_role_v10
CREATE OR REPLACE VIEW apps.leavers_pi_details AS
SELECT
    person.crsid AS leaver_crsid,
    person_hid.person_hid AS leaver_name,
    s.id AS pi_id,
    s.crsid AS pi_crsid,
    s.email_address AS pi_email,
    supervisor_hid.supervisor_hid AS pi_name,
    it_leaving_form.pi_date,
    last_signer_hid.person_hid AS last_signature
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    JOIN person s ON lr.supervisor_id = s.id
    LEFT JOIN supervisor_hid ON s.id = supervisor_hid.supervisor_id
    JOIN person_hid ON person.id = person_hid.person_id
    LEFT JOIN it_leaving_form ON it_leaving_form.crsid::text = person.crsid::text
    LEFT JOIN person_hid last_signer_hid ON it_leaving_form.pi_id = last_signer_hid.person_id
    LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
WHERE
    person.crsid IS NOT NULL
    AND s.email_address IS NOT NULL
    AND ps.status_id::text <> 'Past'::text;

-- view apps.people_crsid_and_supervisor_emails depends on view _latest_role_v10
CREATE OR REPLACE VIEW apps.people_crsid_and_supervisor_emails AS
SELECT
    person.crsid,
    s.email_address
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN person s ON lr.supervisor_id = s.id
WHERE
    person.crsid IS NOT NULL
    AND s.email_address IS NOT NULL;

-- view apps.remote_user_account_metrics depends on view _latest_role_v10
CREATE OR REPLACE VIEW apps.remote_user_account_metrics AS
SELECT
    person.crsid,
    name.person_hid AS name,
    person.arrival_date,
    person.leaving_date,
    person.left_but_no_leaving_date_given,
    sup.person_hid AS supervisor,
    lr.post_category,
    ps.status_id,
    person.is_spri
FROM
    person
    LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN person_hid name ON person.id = name.person_id
    LEFT JOIN person_hid sup ON lr.supervisor_id = sup.person_id
WHERE
    person.crsid IS NOT NULL;

ALTER TABLE apps.remote_user_account_metrics OWNER TO dev;

GRANT SELECT ON TABLE apps.remote_user_account_metrics TO leavers_management;

-- view _chememeritus_mailinglist depends on view _latest_role_v10
CREATE OR REPLACE VIEW public._chememeritus_mailinglist AS
SELECT
    person.email_address,
    person.crsid
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
WHERE
    lr.post_category::text = 'Retired'::text
    AND ps.status_id::text = 'Current'::text
    AND NOT (person.id IN (
            SELECT
                mm_mailinglist_exclude_person.exclude_person_id
            FROM
                mm_mailinglist_exclude_person
            WHERE
                mm_mailinglist_exclude_person.mailinglist_id = ((
                    SELECT
                        mailinglist.id FROM mailinglist
                    WHERE
                        mailinglist.name::text = 'chem-emeritus'::text))))
UNION
SELECT
    person.email_address,
    person.crsid
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-emeritus'::text))
AND ps.status_id::text = 'Current'::text;

ALTER TABLE public._chememeritus_mailinglist OWNER TO dev;

GRANT SELECT ON TABLE public._chememeritus_mailinglist TO leavers_trigger;

GRANT SELECT ON TABLE public._chememeritus_mailinglist TO mailinglists;

-- view _chemgeneral_mailinglist depends on view _latest_role_v10
CREATE OR REPLACE VIEW public._chemgeneral_mailinglist AS
SELECT
    person.email_address
FROM
    person
    LEFT JOIN _physical_status_v2 ps ON ps.id = person.id
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    ps.status_id::text = 'Current'::text
    AND lr.post_category::text <> 'Part III'::text
    AND lr.post_category::text <> 'Part II'::text
    AND NOT (person.id IN (
            SELECT
                mm_mailinglist_exclude_person.exclude_person_id
            FROM
                mm_mailinglist_exclude_person
            WHERE
                mm_mailinglist_exclude_person.mailinglist_id = ((
                    SELECT
                        mailinglist.id FROM mailinglist
                    WHERE
                        mailinglist.name::text = 'chem-general'::text))))
UNION
SELECT
    person.email_address
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v2 ps ON ps.id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-general'::text))
AND ps.status_id::text = 'Current'::text;

ALTER TABLE public._chemgeneral_mailinglist OWNER TO dev;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist TO leavers_trigger;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist TO mailinglist_readers;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist TO mailinglists;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist TO www_sites;

DROP VIEW public._latest_role_v10;

