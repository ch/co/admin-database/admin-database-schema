REVOKE SELECT, DELETE ON leave_recording.leave_audit_notified FROM gdpr_purge;
REVOKE SELECT, DELETE ON leave_recording.role_decoration FROM gdpr_purge;

ALTER TABLE leave_recording.leave_audit_notified DROP CONSTRAINT leave_audit_notified_leave_audit_id_fkey;

ALTER TABLE leave_recording.leave_audit_notified
    ADD CONSTRAINT leave_audit_notified_leave_audit_id_fkey FOREIGN KEY (leave_audit_id)
    REFERENCES leave_recording.leave_audit (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
