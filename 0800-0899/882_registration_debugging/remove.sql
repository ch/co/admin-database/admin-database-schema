CREATE OR REPLACE VIEW hotwire3."10_View/People/Registration" AS
SELECT
    form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_separate_safety_form,
    form.hide_from_website,
    form.previously_registered,
    CASE WHEN form._match_to_person_id IS NOT NULL THEN
        form._match_to_person_id
    ELSE
        registration.match_existing_person (form.surname::character varying, form.first_names::character varying, form.crsid, form.date_of_birth)
    END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    FALSE AS import_this_record,
    hotwire3.to_hwsubviewb ('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
FROM
    registration.form
WHERE
    form.submitted = TRUE
    AND form._imported_on IS NULL
    AND (form.separate_safety_form = TRUE
        OR form.safety_induction_signed_off_date <= 'now'::text::date)
    AND (form.safety_training_needs_signoff
        AND form.safety_training_signed_off_date <= 'now'::text::date
        OR form.safety_training_needs_signoff IS DISTINCT FROM TRUE);

DROP VIEW hotwire3."10_View/People/Incomplete_Registration_Forms";

DROP FUNCTION registration.state_of_form (uuid);

DROP VIEW hotwire3.safety_induction_signer_hid;

