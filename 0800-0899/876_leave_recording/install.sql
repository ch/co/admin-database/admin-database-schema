ALTER TABLE post_history DROP CONSTRAINT usual_days_worked_bitfield;
ALTER TABLE post_history ADD CONSTRAINT usual_days_worked_bitfield CHECK ((usual_days_worked IS NULL) OR (usual_days_worked ~ '^[M-][T-][W-][T-][F-][S-][S-]$'::text));
