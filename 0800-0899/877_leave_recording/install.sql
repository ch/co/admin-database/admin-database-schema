CREATE TABLE leave_recording.generic_audit
(
  audit_id bigserial NOT NULL PRIMARY KEY,
  operation character varying(6) NOT NULL, -- INSERT/DELETE/UPDATE
  username character varying(8) NULL,
  stamp timestamp with time zone NOT NULL DEFAULT Now(),
  schema TEXT NOT NULL,
  tablename TEXT NOT NULL,
  row_id bigint NOT NULL,
  old_cols text NULL,
  new_cols text NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.generic_audit
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.generic_audit TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.generic_audit TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.generic_audit_audit_id_seq TO www_leave_reporting;

GRANT UPDATE ON TABLE leave_recording.role_decoration TO www_leave_reporting;
