CREATE VIEW registration.nonstudent_visitor_type_hid AS
    SELECT
        'v-'::text || visitor_type_hid.visitor_type_id AS id,
        visitor_type_hid.registration_form_name AS hid
    FROM visitor_type_hid
    WHERE visitor_type_hid.show_on_registration_form = true AND visitor_type_hid.registration_form_name !~ '(Student|Postgraduate)';

ALTER TABLE registration.nonstudent_visitor_type_hid OWNER TO dev;
GRANT SELECT ON registration.nonstudent_visitor_type_hid TO starters_registration;

CREATE VIEW registration.student_visitor_type_hid AS
    SELECT 
        'v-'::text || visitor_type_hid.visitor_type_id AS id,
        visitor_type_hid.registration_form_name AS hid
    FROM visitor_type_hid
    WHERE visitor_type_hid.show_on_registration_form = true AND visitor_type_hid.registration_form_name ~ '(Student|Postgraduate)';

ALTER TABLE registration.student_visitor_type_hid OWNER TO dev;
GRANT SELECT ON registration.student_visitor_type_hid TO starters_registration;
