SET ROLE dev;

-- Adding the role below does not get reversed by the remove.sql script; it edits the 
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'rg_admins') THEN
      CREATE ROLE rg_admins NOLOGIN;
      GRANT ro_hid TO rg_admins;
   END IF;
END
$do$;


CREATE VIEW hotwire3."10_View/People/Group_Admins_view"
 AS
 SELECT a.id,
    a.ro_person_id,
    a.image_oid,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.ro_physical_status_id,
    a.arrival_date,
    a.ro_estimated_leaving_date,
    a._cssclass
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            person.image_lo AS image_oid,
            person.surname,
            person.first_names,
            person.title_id,
            person.name_suffix,
            person.email_address,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            person.location,
            _physical_status.status_id AS ro_physical_status_id,
            person.arrival_date,
            LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN cache._latest_role_v12 _latest_role ON person.id = _latest_role.person_id
             LEFT JOIN cache.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
  ) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/Group_Admins_view" OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Group_Admins_view" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Group_Admins_view" TO rg_admins;

INSERT INTO hotwire3."hw_User Preferences" ( preference_id, preference_value, rolename ) VALUES ( (SELECT id from hotwire3."hw_Preferences" WHERE hw_preference_name = $$Default view$$), '10_View/People/Group_Admins_view', 'rg_admins');
