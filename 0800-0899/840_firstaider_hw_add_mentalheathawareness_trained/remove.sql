DROP RULE firstaiders_del ON hotwire3."10_View/Safety/Firstaiders";
DROP RULE firstaiders_ins ON hotwire3."10_View/Safety/Firstaiders";
DROP RULE firstaiders_upd ON hotwire3."10_View/Safety/Firstaiders";

DROP VIEW hotwire3."10_View/Safety/Firstaiders";

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Firstaiders" AS 
 SELECT firstaider.id,
    person.image_lo AS ro_image,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.defibrillator_trained,
    array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_hid USING (building_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_hid.building_hid), '/'::text) AS ro_building,
    array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
             LEFT JOIN hotwire3.building_floor_hid USING (building_floor_id)
          WHERE p2.id = firstaider.person_id
          ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor,
    array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
             LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
             LEFT JOIN room ON room.id = mm_person_room.room_id
          WHERE p2.id = firstaider.person_id
          ORDER BY room.name), '/'::text) AS ro_room,
    array_to_string(ARRAY( SELECT dept_telephone_number_hid.dept_telephone_number_hid
           FROM person p2
             JOIN mm_person_dept_telephone_number ON mm_person_dept_telephone_number.person_id = p2.id
             JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
          WHERE p2.id = firstaider.person_id), ' / '::text) AS ro_dept_telephone_numbers,
    _physical_status.status_id AS status,
    firstaider.qualification_up_to_date AS ro_qualification_up_to_date,
    _latest_role.post_category AS ro_post_category,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
        CASE
            WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN firstaider.qualification_up_to_date = false THEN 'red'::text
            ELSE NULL::text
        END AS _cssclass
   FROM firstaider
     JOIN person ON firstaider.person_id = person.id
     LEFT JOIN _latest_role_v12 _latest_role ON firstaider.person_id = _latest_role.person_id
     LEFT JOIN apps.person_futuremost_role futuremost_role ON firstaider.person_id = futuremost_role.person_id
     JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = firstaider.person_id;

ALTER TABLE hotwire3."10_View/Safety/Firstaiders"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Firstaiders" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Firstaiders" TO www_sites;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Firstaiders" TO firstaiders;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Safety/Firstaiders" TO hr;

-- Rule: firstaiders_del ON hotwire3."10_View/Safety/Firstaiders"


CREATE OR REPLACE RULE firstaiders_del AS
    ON DELETE TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  DELETE FROM firstaider
  WHERE firstaider.id = old.id;

-- Rule: firstaiders_ins ON hotwire3."10_View/Safety/Firstaiders"


CREATE OR REPLACE RULE firstaiders_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  INSERT INTO firstaider (person_id, firstaider_funding_id, qualification_date, requalify_date, hf_cn_trained, defibrillator_trained)
  VALUES (new.person_id, new.firstaider_funding_id, new.qualification_date, new.requalify_date, new.hf_cn_trained, new.defibrillator_trained)
  RETURNING firstaider.id,
    NULL::oid AS oid,
    firstaider.person_id,
    firstaider.firstaider_funding_id,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider.defibrillator_trained,
    NULL::text AS text,
    NULL::text AS text,
    NULL::text AS text,
    NULL::text AS text,
    NULL::character varying(20) AS "varchar",
    NULL::boolean AS bool,
    NULL::character varying(80) AS "varchar",
    NULL::date AS date,
    NULL::text AS text;

-- Rule: firstaiders_upd ON hotwire3."10_View/Safety/Firstaiders"


CREATE OR REPLACE RULE firstaiders_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Firstaiders" DO INSTEAD  UPDATE firstaider SET person_id = new.person_id, firstaider_funding_id = new.firstaider_funding_id, qualification_date = new.qualification_date, requalify_date = new.requalify_date, hf_cn_trained = new.hf_cn_trained, defibrillator_trained = new.defibrillator_trained
  WHERE firstaider.id = old.id;


