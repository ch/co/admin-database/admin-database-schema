CREATE ROLE old_selfservice;
GRANT SELECT (id, email_address, hide_email, known_as, emergency_contact, cambridge_address, cambridge_phone_number, name_suffix, do_not_show_on_website, managed_mail_domain_optout, override_auto_ban, auto_banned_from_network) ON public.person TO old_selfservice;
GRANT UPDATE (email_address, hide_email, known_as, emergency_contact, cambridge_address, cambridge_phone_number, name_suffix, do_not_show_on_website, managed_mail_domain_optout) ON public.person TO old_selfservice;
GRANT SELECT ON public.title_hid TO old_selfservice;
GRANT SELECT ON public.person_hid TO old_selfservice;
GRANT SELECT,INSERT,DELETE ON public.mm_person_research_group TO old_selfservice;
GRANT EXECUTE ON FUNCTION selfservice.selfservice_upd() TO old_selfservice;
GRANT SELECT ON public.research_group_hid TO old_selfservice;
GRANT SELECT,INSERT,UPDATE ON public._physical_status_v3 TO old_selfservice;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_room TO old_selfservice;
GRANT ALL ON public.user_account_id_seq TO old_selfservice;
-- privileges for column username of table user_account
-- privileges for column person_id of table user_account
-- privileges for column chemnet_token of table user_account
GRANT SELECT,INSERT (username,person_id) ON public.user_account TO old_selfservice;
GRANT INSERT,UPDATE (chemnet_token) ON public.user_account TO old_selfservice;
-- privileges for view supervisor_hid
GRANT SELECT ON public.supervisor_hid TO old_selfservice;
-- privileges for table _physical_status_changelog
GRANT INSERT ON public._physical_status_changelog TO old_selfservice;
-- privileges for table gender_hid
GRANT SELECT ON public.gender_hid TO old_selfservice;
-- privileges for view post_category_hid
GRANT SELECT ON public.post_category_hid TO old_selfservice;
-- privileges for view cambridge_college_hid
GRANT SELECT ON public.cambridge_college_hid TO old_selfservice;
-- privileges for view nationality_hid
GRANT SELECT ON public.nationality_hid TO old_selfservice;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_dept_telephone_number TO old_selfservice;
