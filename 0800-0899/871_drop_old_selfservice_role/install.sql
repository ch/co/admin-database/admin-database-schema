REVOKE SELECT (id, email_address, hide_email, known_as, emergency_contact, cambridge_address, cambridge_phone_number, name_suffix, do_not_show_on_website, managed_mail_domain_optout, override_auto_ban, auto_banned_from_network) ON public.person FROM old_selfservice;
REVOKE UPDATE (email_address, hide_email, known_as, emergency_contact, cambridge_address, cambridge_phone_number, name_suffix, do_not_show_on_website, managed_mail_domain_optout) ON public.person FROM old_selfservice;
REVOKE SELECT ON public.title_hid FROM old_selfservice;
REVOKE SELECT ON public.person_hid FROM old_selfservice;
REVOKE SELECT,INSERT,DELETE ON public.mm_person_research_group FROM old_selfservice;
REVOKE EXECUTE ON FUNCTION selfservice.selfservice_upd() FROM old_selfservice;
REVOKE SELECT ON public.research_group_hid FROM old_selfservice;
REVOKE SELECT,INSERT,UPDATE ON public._physical_status_v3 FROM old_selfservice;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_room FROM old_selfservice;
REVOKE ALL ON public.user_account_id_seq FROM old_selfservice;
-- privileges for column username of table user_account
-- privileges for column person_id of table user_account
-- privileges for column chemnet_token of table user_account
REVOKE SELECT,INSERT (username,person_id) ON public.user_account FROM old_selfservice;
REVOKE INSERT,UPDATE (chemnet_token) ON public.user_account FROM old_selfservice;
-- privileges for view supervisor_hid
REVOKE SELECT ON public.supervisor_hid FROM old_selfservice;
-- privileges for table _physical_status_changelog
REVOKE INSERT ON public._physical_status_changelog FROM old_selfservice;
-- privileges for table gender_hid
REVOKE SELECT ON public.gender_hid FROM old_selfservice;
-- privileges for view post_category_hid
REVOKE SELECT ON public.post_category_hid FROM old_selfservice;
-- privileges for view cambridge_college_hid
REVOKE SELECT ON public.cambridge_college_hid FROM old_selfservice;
-- privileges for view nationality_hid
REVOKE SELECT ON public.nationality_hid FROM old_selfservice;
-- 
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_dept_telephone_number FROM old_selfservice;

DROP ROLE old_selfservice;
