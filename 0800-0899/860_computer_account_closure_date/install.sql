-- recreate apps.user_accounts_to_disable without the computer_account_closure_date column
DROP VIEW apps.user_accounts_to_disable;

CREATE VIEW apps.user_accounts_to_disable AS
SELECT
    person.crsid,
    person.surname,
    person.first_names,
    _latest_role.post_category,
    person.leaving_date,
    (
        SELECT
            min(all_future_start_dates.start_date) AS min
        FROM (
            SELECT
                a.start_date
            FROM
                _all_roles_v13 a
            WHERE
                a.person_id = person.id
                AND a.start_date > ('now'::text::date - '1 mon'::interval)) all_future_start_dates) AS next_role_start_date
FROM
    person
    JOIN _physical_status_v3 ps ON person.id = ps.person_id
    LEFT JOIN _latest_role_v12 _latest_role USING (person_id)
WHERE
    ps.status_id::text = 'Past'::text
    AND person.is_spri IS NOT TRUE;

ALTER TABLE apps.user_accounts_to_disable OWNER TO dev;

GRANT SELECT ON TABLE apps.user_accounts_to_disable TO ad_accounts;

GRANT ALL ON TABLE apps.user_accounts_to_disable TO dev;

-- rename computer_account_closure_date to leavers_it_extension_end_date
-- and give the leavers system rights on it
ALTER TABLE public.person RENAME COLUMN computer_account_closure_date TO leavers_it_extension_end_date;

GRANT SELECT (crsid, leavers_it_extension_end_date) ON public.person TO leavers_trigger;

GRANT UPDATE (leavers_it_extension_end_date) ON public.person TO leavers_trigger;

-- create a view for the leavers system to use to update the renamed column
CREATE VIEW apps.leavers_it_extensions AS
SELECT
    person.crsid,
    person.leavers_it_extension_end_date
FROM
    person;

ALTER VIEW apps.leavers_it_extensions OWNER TO dev;

GRANT SELECT, UPDATE ON apps.leavers_it_extensions TO leavers_trigger;

CREATE FUNCTION apps.leavers_it_extensions_trig ()
    RETURNS TRIGGER
    AS $BODY$
BEGIN
    UPDATE
        public.person
    SET
        leavers_it_extension_end_date = NEW.leavers_it_extension_end_date
    WHERE
        crsid = OLD.crsid;
    RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;

ALTER FUNCTION apps.leavers_it_extensions_trig () OWNER TO dev;

CREATE TRIGGER leavers_it_extensions_upd
    INSTEAD OF UPDATE ON apps.leavers_it_extensions
    FOR EACH ROW
    EXECUTE FUNCTION apps.leavers_it_extensions_trig ();

