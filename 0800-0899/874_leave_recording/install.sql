DROP VIEW leave_recording.role;
ALTER TABLE post_history DROP COLUMN leave_recording_manager;

CREATE TABLE leave_recording.role_decoration
(
  post_id bigint NOT NULL PRIMARY KEY REFERENCES public.post_history(id),
  hide_role BOOLEAN NOT NULL DEFAULT FALSE,
  leave_recording_manager BIGINT NULL REFERENCES public.post_history(id) DEFAULT NULL,
  notify_line_manager BOOLEAN NOT NULL DEFAULT TRUE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.role_decoration
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_decoration TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.role_decoration TO www_leave_reporting;

INSERT INTO leave_recording.role_decoration (post_id) SELECT distinct post_id FROM leave_recording.role_enrol;


ALTER TABLE leave_recording.leave_audit DROP COLUMN notified;

ALTER TABLE leave_recording.mm_role_shared_calendar_group ADD COLUMN valid BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE leave_recording.mm_role_shared_calendar_group RENAME COLUMN added_by TO invited_by;
ALTER TABLE leave_recording.mm_role_shared_calendar_group RENAME COLUMN added_when TO invited_when;
ALTER TABLE leave_recording.mm_role_shared_calendar_group ADD COLUMN accepted_by BIGINT REFERENCES public.person(id) NULL DEFAULT NULL;
ALTER TABLE leave_recording.mm_role_shared_calendar_group ADD COLUMN accepted_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL;

ALTER TABLE post_history ADD COLUMN is_leave_recording_global_overseer BOOLEAN NOT NULL DEFAULT FALSE;

GRANT UPDATE(person_notified, line_manager_notified) ON TABLE leave_recording.role_enrol TO www_leave_reporting;


CREATE OR REPLACE VIEW leave_recording.person AS 
 SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    TRIM(BOTH FROM COALESCE(person.known_as, person.first_names)::text) || ' '::text || TRIM(BOTH FROM person.surname::text) AS person_name,
    TRIM(BOTH FROM COALESCE(person.known_as, person.first_names)::text) AS person_friendly_name
  FROM
    person;

ALTER TABLE leave_recording.person
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.person TO dev;
GRANT SELECT ON TABLE leave_recording.person TO www_leave_reporting;


CREATE OR REPLACE VIEW leave_recording.person_status AS
 SELECT
    _physical_status_v3.person_id AS person_id,
    _physical_status_v3.status_id AS current_status
  FROM
    _physical_status_v3;

ALTER TABLE leave_recording.person_status
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.person_status TO dev;
GRANT SELECT ON TABLE leave_recording.person_status TO www_leave_reporting;


UPDATE leave_recording.leave_audit SET action='assign' WHERE action='impose';


CREATE OR REPLACE VIEW leave_recording.role AS
  SELECT
    CASE
      WHEN (staff_category.category = 'Assistant staff'::text) THEN 'leave_recording\assistant_staff_Role'::text
      WHEN (staff_category.category = 'Research Assistant'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Research Fellow'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'PDRA'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Senior PDRA'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Principal Research Associate'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Academic-related staff'::text) THEN 'leave_recording\academic_related_Role'::text
      WHEN (staff_category.category = 'Teaching Fellow'::text) THEN 'leave_recording\teaching_Role'::text
      WHEN (staff_category.category = 'Unknown'::text) THEN 'leave_recording\unknown_Role'::text
      WHEN (staff_category.category = 'Academic staff'::text) THEN 'leave_recording\academic_staff_Role'::text
      ELSE 'leave_recording\database\Role'::text
    END AS role_class_name,
    post_history.id as post_id,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    post_history.start_date,
    post_history.end_date,
    post_history.intended_end_date,
            ( SELECT min(closest_date.end_date) AS min
                   FROM ( SELECT post_history_1.id,
                            post_history_1.end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.intended_end_date AS end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.funding_end_date AS end_date
                           FROM public.post_history post_history_1
                          GROUP BY post_history_1.id) closest_date
                  WHERE (closest_date.id = post_history.id)) AS estimated_leaving_date,
    CASE
        WHEN person.continuous_employment_start_date <= post_history.start_date THEN person.continuous_employment_start_date
        ELSE NULL -- we don't record historical values
    END AS continuous_employment_start_date_as_at_start_of_role,
    post_history.hours_worked,
    TRIM(BOTH FROM post_history.job_title) AS job_title,
    --post_history.staff_category_id,
    --('sc-'::text || (post_history.staff_category_id)::text) AS post_category_id,
    staff_category.category AS post_category,
    post_history.percentage_of_fulltime_hours,
    post_history.average_percentage_of_days_worked_per_week,
    post_history.usual_days_worked,
    post_history.works_flexitime,
    post_history.standard_flexileave_rules_apply,
    post_history.departmental_closures_apply,
    post_history.leave_recording_disable,
    post_history.leave_recording_needs_approval,
    role_decoration.hide_role,
    role_decoration.notify_line_manager,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN (post_history.funding_end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name,
    leave_recording_manager.leave_recording_manager_person_id,
    leave_recording_manager.leave_recording_manager_crsid,
    leave_recording_manager.leave_recording_manager_email,
    leave_recording_manager.leave_recording_manager_name,
    leave_recording_manager_role.leave_recording_manager_post_id,
    leave_recording_manager_role.leave_recording_manager_job_title,
    leave_recording_manager_role.leave_recording_manager_end_date,
    leave_recording_manager_role.leave_recording_manager_status,
    post_history.paid_by_university as employed_by_us
 FROM public.post_history
 LEFT JOIN leave_recording.role_decoration ON (post_history.id = role_decoration.post_id)
 LEFT JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = post_history.supervisor_id
 LEFT JOIN (SELECT
    post_history.id AS leave_recording_manager_post_id,
    post_history.person_id AS leave_recording_manager_person_id,
    post_history.job_title AS leave_recording_manager_job_title,
    post_history.end_date AS leave_recording_manager_end_date,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN (post_history.funding_end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS leave_recording_manager_status
  FROM
    post_history) leave_recording_manager_role ON leave_recording_manager_role.leave_recording_manager_post_id = role_decoration.leave_recording_manager
 LEFT JOIN (SELECT
    person.id AS leave_recording_manager_person_id,
    person.crsid AS leave_recording_manager_crsid,
    person.email_address AS leave_recording_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS leave_recording_manager_name
  FROM
    person) leave_recording_manager ON leave_recording_manager.leave_recording_manager_person_id = leave_recording_manager_role.leave_recording_manager_person_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.continuous_employment_start_date
  FROM
    person) person ON person.person_id = post_history.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leave_recording.role
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role TO dev;
GRANT SELECT ON TABLE leave_recording.role TO www_leave_reporting;


CREATE OR REPLACE VIEW leave_recording.latest_role_enrol_audit AS
SELECT role_enrol_audit.*
FROM leave_recording.role_enrol_audit
JOIN
  (SELECT role_enrol_audit.post_id
  , max(action_when) AS action_when
    FROM leave_recording.role_enrol_audit
    GROUP BY role_enrol_audit.post_id) latest ON role_enrol_audit.post_id = latest.post_id
AND role_enrol_audit.action_when = latest.action_when;

ALTER VIEW leave_recording.latest_role_enrol_audit OWNER TO dev;
GRANT SELECT ON TABLE leave_recording.latest_role_enrol_audit TO www_leave_reporting;
