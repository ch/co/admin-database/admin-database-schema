CREATE OR REPLACE VIEW apps.vm_expiry
 AS
 SELECT si.id,
    ARRAY( SELECT ip.hostname
           FROM mm_system_image_ip_address mm
             JOIN ip_address ip ON mm.ip_address_id = ip.id
          WHERE mm.system_image_id = si.id) AS hostnames,
    os.os,
    si.expiry_date,
    ARRAY( SELECT cr.email_address
           FROM research_group rg
             JOIN mm_research_group_computer_rep mmcr ON mmcr.research_group_id = rg.id
             JOIN person cr ON mmcr.computer_rep_id = cr.id
          WHERE rg.id = si.research_group_id
        UNION
         SELECT u.email_address
           FROM person u
          WHERE u.id = si.user_id) AS email_addresses,
    ARRAY( SELECT s2.wired_mac_1::character varying AS m
           FROM system_image s2
          WHERE s2.id = si.id AND s2.wired_mac_1 IS NOT NULL
        UNION
         SELECT s2.wired_mac_2::character varying AS m
           FROM system_image s2
          WHERE s2.id = si.id AND s2.wired_mac_2 IS NOT NULL
        UNION
         SELECT s2.wired_mac_3::character varying AS m
           FROM system_image s2
          WHERE s2.id = si.id AND s2.wired_mac_3 IS NOT NULL
        UNION
         SELECT s2.wired_mac_4::character varying AS m
           FROM system_image s2
          WHERE s2.id = si.id AND s2.wired_mac_4 IS NOT NULL
        UNION
         SELECT s2.wireless_mac::character varying AS m
           FROM system_image s2
          WHERE s2.id = si.id AND s2.wireless_mac IS NOT NULL) AS mac_addresses
   FROM system_image si
     LEFT JOIN operating_system os ON si.operating_system_id = os.id
  WHERE si.expiry_date IS NOT NULL;
