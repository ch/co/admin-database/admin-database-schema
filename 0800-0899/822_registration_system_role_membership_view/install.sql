CREATE VIEW registration.role_data AS
     WITH RECURSIVE membership_tree(grpid, userid) AS (
         SELECT pg_roles.oid,
            pg_roles.oid
           FROM pg_roles
        UNION ALL
         SELECT m_1.roleid,
            t_1.userid
           FROM pg_auth_members m_1,
            membership_tree t_1
          WHERE m_1.member = t_1.grpid
        )
 SELECT DISTINCT r.rolname AS member,
    m.rolname AS role
   FROM membership_tree t,
    pg_roles r,
    pg_roles m
  WHERE t.grpid = m.oid AND t.userid = r.oid and r.rolname <> m.rolname
  ORDER BY r.rolname, m.rolname;

ALTER VIEW registration.role_data OWNER TO dev;
GRANT SELECT ON registration.role_data TO starters_registration;
