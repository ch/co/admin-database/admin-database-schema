DROP FUNCTION public.fn_update_itreps();

CREATE OR REPLACE FUNCTION public.fn_update_itreps(
	)
    RETURNS SETOF text 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
  itrep varchar;
  itrepid bigint;

begin
 -- IT reps without DB roles: create them
  for itrep in 
    select 
        distinct crsid 
    from person 
    inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id 
    left join pg_roles on person.crsid=rolname 
    left join _physical_status_v2 on person.id = _physical_status_v2.person_id
    where crsid is not null and rolname is null and _physical_status_v2.status_id <> 'Past'
  loop
    execute 'create role '||itrep||' with login in role groupitreps';
  end loop;

 -- IT reps who don't have groupitreps role
  for itrep in 
    select 
        distinct crsid 
    from person 
    left join _physical_status_v2 on person.id = _physical_status_v2.person_id
    inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id 
    where (
        select count(*) 
        from pg_auth_members 
        inner join pg_roles u on member=u.oid 
        inner join pg_roles g on roleid=g.oid 
        where u.rolname=crsid and g.rolname='groupitreps'
    )=0 and crsid is not null and _physical_status_v2.status_id <> 'Past'
  loop
   execute 'grant groupitreps to '||itrep;
  end loop;
 -- people with the IT rep role who shouldn't have
 for itrep in select u.rolname from pg_auth_members inner join pg_authid u on u.oid=pg_auth_members.member inner join pg_authid g on pg_auth_members.roleid=g.oid left join person on person.crsid=u.rolname left join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id where g.rolname='groupitreps' and research_group_id is null loop
   execute 'revoke groupitreps from '||itrep;
 end loop;
 -- set homepage in hotwire for people who have no other homepage
 for itrep in select distinct crsid from person join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id left join ( select rolename, preference_id from hotwire3."hw_User Preferences" where hotwire3."hw_User Preferences".preference_id = 9 ) homepages on homepages.rolename = person.crsid where homepages.preference_id is null loop
   execute 'insert into hotwire3."hw_User Preferences" ( preference_id, preference_value, rolename ) values ( 9, $$10_View/My_Groups/Computers$$, $1)' using itrep;
 end loop;

end;

$BODY$;

ALTER FUNCTION public.fn_update_itreps()
    OWNER TO dev;

