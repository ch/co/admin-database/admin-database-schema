CREATE TABLE public.mifare_access_level (
    mifare_access_level_id BIGSERIAL PRIMARY KEY,
    mifare_access_level TEXT NOT NULL );

ALTER TABLE public.mifare_access_level OWNER TO dev;

INSERT INTO public.mifare_access_level
    ( mifare_access_level )
VALUES
    ( 'PDRA' ), 
    ( 'PhD' ),
    ( 'Visitor' ),
    ( 'WREN' ),
    ( 'Other' )
    ;

CREATE VIEW hotwire3.mifare_access_level_hid AS
SELECT mifare_access_level_id, mifare_access_level AS mifare_access_level_hid
FROM public.mifare_access_level;

ALTER TABLE hotwire3.mifare_access_level_hid OWNER TO dev;
GRANT SELECT ON hotwire3.mifare_access_level_hid TO PUBLIC;

COMMENT ON TABLE public.mifare_access_level IS 'This represents standardardized levels of mifare access used when registering new members of the department';
