DROP VIEW hotwire3."90_Action/Hotwire/User Preferences";

CREATE OR REPLACE VIEW hotwire3."90_Action/Hotwire/User Preferences"
 AS
 SELECT "hw_User Preferences".id,
    "hw_User Preferences".preference_id,
    "hw_User Preferences".preference_value,
    "hw_User Preferences".user_id AS postgres_user_id
   FROM hotwire3."hw_User Preferences";

ALTER TABLE hotwire3."90_Action/Hotwire/User Preferences"
    OWNER TO dev;


CREATE OR REPLACE RULE hw3_user_preferences_del AS
    ON DELETE TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(DELETE FROM hotwire3."hw_User Preferences"
  WHERE ("hw_User Preferences".id = old.id));

CREATE OR REPLACE RULE hw3_user_preferences_ins AS
    ON INSERT TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(INSERT INTO hotwire3."hw_User Preferences" (preference_id, preference_value, user_id)
  VALUES (new.preference_id, new.preference_value, new.postgres_user_id)
  RETURNING "hw_User Preferences".id,
    "hw_User Preferences".preference_id,
    "hw_User Preferences".preference_value,
    "hw_User Preferences".user_id);

CREATE OR REPLACE RULE hw3_user_preferences_upd AS
    ON UPDATE TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(UPDATE hotwire3."hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new.postgres_user_id
  WHERE ("hw_User Preferences".id = old.id));

CREATE OR REPLACE VIEW hotwire3._preferences
 AS
 WITH all_prefs AS (
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            1 AS priority
           FROM hotwire3."hw_User Preferences" "User Preferences"
          WHERE "User Preferences".user_id IS NULL
        UNION ALL
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            100 AS priority
           FROM pg_auth_members m
             JOIN pg_roles b ON m.roleid = b.oid
             JOIN pg_roles r ON m.member = r.oid
             JOIN hotwire3."hw_User Preferences" "User Preferences" ON b.oid = "User Preferences".user_id::oid
          WHERE r.rolname = "current_user"()
        UNION ALL
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            200 AS priority
           FROM pg_roles
             JOIN hotwire3."hw_User Preferences" "User Preferences" ON pg_roles.oid = "User Preferences".user_id::oid
          WHERE pg_roles.rolname = "current_user"()
        )
 SELECT "Preferences".hw_preference_const AS preference_name,
    all_prefs.preference_value,
    hw_preference_type_hid.hw_preference_type_hid AS preference_type
   FROM all_prefs
     JOIN ( SELECT all_prefs_1.preference_id,
            max(all_prefs_1.priority) AS priority
           FROM all_prefs all_prefs_1
          GROUP BY all_prefs_1.preference_id) max_pref ON all_prefs.priority = max_pref.priority AND all_prefs.preference_id = max_pref.preference_id
     JOIN hotwire3."hw_Preferences" "Preferences" ON all_prefs.preference_id = "Preferences".id
     JOIN hotwire3.hw_preference_type_hid USING (hw_preference_type_id);

ALTER TABLE hotwire3."hw_User Preferences" DROP COLUMN rolename;
DROP VIEW hotwire3.postgres_rolename_hid;
