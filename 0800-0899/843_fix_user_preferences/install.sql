ALTER TABLE hotwire3."hw_User Preferences" ADD COLUMN rolename varchar;

UPDATE hotwire3."hw_User Preferences" set rolename = ( select rolname from pg_roles WHERE pg_roles.oid = user_id ) WHERE user_id IS NOT NULL;

-- This can't be done because the pg_authid table is a system catalogue table that's shared between all databases and we aren't allowed
-- to make foreign keys to it, so we can't have full relational integrity here
-- ALTER TABLE hotwire3."hw_User Preferences" ADD CONSTRAINT userprefs_fk_rolname FOREIGN KEY (rolename) REFERENCES pg_catalog.pg_authid(rolname) ON DELETE CASCADE;

-- create hid view in order to make Postgres generate dropdown lists
CREATE VIEW hotwire3.postgres_rolename_hid AS
SELECT rolname::varchar AS postgres_rolename_id,
       rolname::varchar AS postgres_rolename_hid
FROM pg_catalog.pg_roles;

ALTER VIEW hotwire3.postgres_rolename_hid OWNER TO dev;
GRANT SELECT ON hotwire3.postgres_rolename_hid TO PUBLIC;

-- update hotwire3."90_Action/User Preferences" to expose the new column
DROP VIEW hotwire3."90_Action/Hotwire/User Preferences";
CREATE VIEW hotwire3."90_Action/Hotwire/User Preferences" AS
SELECT
    "hw_User Preferences".id,
    "hw_User Preferences".preference_id,
    "hw_User Preferences".preference_value,
    "hw_User Preferences".rolename AS postgres_rolename_id
FROM hotwire3."hw_User Preferences";

ALTER TABLE hotwire3."90_Action/Hotwire/User Preferences" OWNER TO dev;

CREATE RULE hw3_user_preferences_del AS
    ON DELETE TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(DELETE FROM hotwire3."hw_User Preferences"
  WHERE ("hw_User Preferences".id = old.id));

CREATE RULE hw3_user_preferences_ins AS
    ON INSERT TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(INSERT INTO hotwire3."hw_User Preferences" (preference_id, preference_value, rolename)
  VALUES (new.preference_id, new.preference_value, new.postgres_rolename_id)
  RETURNING "hw_User Preferences".id,
    "hw_User Preferences".preference_id,
    "hw_User Preferences".preference_value,
    "hw_User Preferences".rolename);

CREATE OR REPLACE RULE hw3_user_preferences_upd AS
    ON UPDATE TO hotwire3."90_Action/Hotwire/User Preferences"
    DO INSTEAD
(UPDATE hotwire3."hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, rolename = new.postgres_rolename_id
  WHERE ("hw_User Preferences".id = old.id));


-- update hotwire3._preferences
CREATE OR REPLACE VIEW hotwire3._preferences AS
 WITH all_prefs AS (
         -- global preferences
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            1 AS priority
           FROM hotwire3."hw_User Preferences" "User Preferences"
          WHERE "User Preferences".rolename IS NULL
        UNION ALL
        -- group preferences
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            100 AS priority
           FROM hotwire3."hw_User Preferences" "User Preferences"
          WHERE pg_has_role(current_user,"User Preferences".rolename,'MEMBER')
        UNION ALL
         -- per user preferences
         SELECT "User Preferences".preference_id,
            "User Preferences".preference_value,
            200 AS priority
           FROM hotwire3."hw_User Preferences" "User Preferences"
          WHERE "User Preferences".rolename = current_user
        )
 SELECT "Preferences".hw_preference_const AS preference_name,
    all_prefs.preference_value,
    hw_preference_type_hid.hw_preference_type_hid AS preference_type
   FROM all_prefs
     JOIN ( SELECT all_prefs_1.preference_id,
            max(all_prefs_1.priority) AS priority
           FROM all_prefs all_prefs_1
          GROUP BY all_prefs_1.preference_id) max_pref ON all_prefs.priority = max_pref.priority AND all_prefs.preference_id = max_pref.preference_id
     JOIN hotwire3."hw_Preferences" "Preferences" ON all_prefs.preference_id = "Preferences".id
     JOIN hotwire3.hw_preference_type_hid USING (hw_preference_type_id);
