CREATE VIEW hotwire3."10_View/Safety/Registration" AS 
         SELECT form.form_id AS id,
            COALESCE(person.first_names,form.first_names)::character varying AS first_names,
            COALESCE(person.surname,form.surname)::character varying AS surname,
            COALESCE(person.known_as,form.known_as)::character varying AS known_as,
            COALESCE(person.title_id,form.title_id) as title_id,
            COALESCE(_latest_role.post_category_id,form.post_category_id) AS post_category_id,
            COALESCE(ARRAY(SELECT mm_person_room.room_id FROM mm_person_room WHERE person.id = mm_person_room.person_id),form.dept_room_id) AS room_id,
            COALESCE(ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM mm_person_dept_telephone_number WHERE person.id = mm_person_dept_telephone_number.person_id),form.dept_telephone_id) AS dept_telephone_number_id,
            COALESCE(person.email_address,form.email)::character varying AS email,
            COALESCE(person.crsid,form.crsid) AS crsid,
            COALESCE(person.arrival_date,form.start_date) AS arrival_date,
            COALESCE(_latest_role.intended_end_date,form.intended_end_date) AS intended_end_date,
            COALESCE(_latest_role.supervisor_id,form.department_host_id) AS supervisor_id,
            form.mifare_access_level_id,
            form.mifare_areas AS group_areas,
            (E'Content-type: application/pdf\n\n' || form.safety_form_pdf)::bytea as "safety_form.pdf",
            form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
            form.safety_induction_signed_off_date AS ro_safety_induction_signed_date
           FROM registration.form
           LEFT JOIN person ON form._match_to_person_id = person.id
           LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
          WHERE form.submitted = 't'  -- AND form.safety_induction_signed_off_date <= current_date AND form.safety_induction_person_signing_off_id IS NOT NULL
;

ALTER TABLE hotwire3."10_View/Safety/Registration" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Registration" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Registration" TO hr;
