DROP VIEW leave_recording.role_entitlement_adjustment_view;

CREATE OR REPLACE VIEW leave_recording.role_entitlement_adjustment_view AS
  SELECT role_entitlement_adjustment.id,
    role_entitlement_adjustment.post_id,
    role_entitlement_adjustment.leave_type,
    role_entitlement_adjustment.leave_year,
    role_entitlement_adjustment.affect_all_future_leave_years,
    role_entitlement_adjustment.factor_existing,
    role_entitlement_adjustment.delta,
    role_entitlement_adjustment.reason,
    role_entitlement_adjustment.actor,
    role_entitlement_adjustment.action_notes,
    role_entitlement_adjustment.action_when,
    role_entitlement_adjustment.deleted_when,
    role_entitlement_adjustment.deleted_by_actor,
    role_entitlement_adjustment.deleted_notes,
    person.person_crsid AS actor_crsid,
    person.person_email AS actor_email,
    person.person_name AS actor_name,
    person.person_friendly_name AS actor_friendly_name
   FROM leave_recording.role_entitlement_adjustment
     LEFT JOIN leave_recording.person ON person.person_id = role_entitlement_adjustment.actor;

ALTER TABLE leave_recording.role_entitlement_adjustment_view
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_entitlement_adjustment_view TO dev;
GRANT SELECT ON TABLE leave_recording.role_entitlement_adjustment_view TO www_leave_reporting;
