CREATE OR REPLACE VIEW public._chempg_mailinglist
 AS
 SELECT person.email_address,
    person.crsid
   FROM postgraduate_studentship
     LEFT JOIN person ON postgraduate_studentship.person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     LEFT JOIN _all_roles_v12 ON person.id = _all_roles_v12.person_id AND _all_roles_v12.role_tablename = 'postgraduate_studentship'::text
  WHERE _all_roles_v12.status::text = 'Current'::text AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-pg'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-pg'::text)) AND ps.status_id::text = 'Current'::text;
