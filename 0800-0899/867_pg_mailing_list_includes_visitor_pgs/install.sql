CREATE OR REPLACE VIEW public._chempg_mailinglist
 AS
 SELECT DISTINCT person.email_address,
    person.crsid
   FROM person
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
     LEFT JOIN _all_roles_v12 ar ON person.id = ar.person_id AND (ar.role_tablename = 'postgraduate_studentship'::text OR ar.post_category_id = 'v-3' OR ar.post_category_id = 'v-2')
  WHERE ar.status::text = 'Current'::text AND ps.status_id::text = 'Current'::text AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
                   FROM mailinglist
                  WHERE mailinglist.name::text = 'chem-pg'::text))))
UNION
 SELECT person.email_address,
    person.crsid
   FROM mm_mailinglist_include_person
     LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
     LEFT JOIN _physical_status_v3 ps ON ps.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
           FROM mailinglist
          WHERE mailinglist.name::text = 'chem-pg'::text)) AND ps.status_id::text = 'Current'::text;
