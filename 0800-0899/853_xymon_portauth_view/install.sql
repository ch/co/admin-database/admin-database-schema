CREATE OR REPLACE VIEW apps.xymon_portauth AS
SELECT ip_address.ip, switchport.name, switch_auth_id from switchport 
INNER JOIN switch ON switch.id=switch_id 
INNER JOIN system_image USING (hardware_id) 
INNER JOIN mm_system_image_ip_address ON system_image_id=system_image.id 
INNER JOIN ip_address ON ip_address.id=ip_address_id;

ALTER TABLE apps.xymon_portauth OWNER TO dev;

GRANT SELECT ON apps.xymon_portauth TO xymon_portauth;
