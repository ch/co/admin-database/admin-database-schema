-- new table for university card import from the new API, which gives
-- us slightly different data
CREATE TABLE public.university_card (
    surname text,
    forename text,
    full_name text,
    crsid text,
    id uuid,
    issued_at timestamp with time zone,
    issue_number bigint,
    expires_at timestamp with time zone,
    legacy_card_holder_id text,
    mifare_id bigint,
    mifare_number bigint,
    barcode text
);

ALTER TABLE public.university_card OWNER TO dev;
GRANT TRUNCATE,INSERT ON public.university_card TO _misd_import;
