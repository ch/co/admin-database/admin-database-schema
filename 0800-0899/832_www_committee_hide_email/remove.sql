CREATE OR REPLACE VIEW www.committee_members AS 
  SELECT person.crsid,
    person.email_address,
    www_person_hid_v2.www_person_hid,
    (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name
   FROM person
     LEFT JOIN www_person_hid_v2 ON person.id = www_person_hid_v2.person_id
     LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
     LEFT JOIN physical_status_hid ON _physical_status_v3.status_id::text = physical_status_hid.physical_status_hid::text
  WHERE physical_status_hid.physical_status_hid::text = 'Current'::text AND person.crsid IS NOT NULL;

