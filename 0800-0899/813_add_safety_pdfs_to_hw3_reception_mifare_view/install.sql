DROP VIEW hotwire3."10_View/People/MiFare_Access";

CREATE OR REPLACE VIEW hotwire3."10_View/People/MiFare_Access" AS 
 SELECT form.form_id AS id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.crsid AS ro_crsid,
    form.mifare_access_level_id AS ro_mifare_access_level_id,
    form.mifare_areas AS ro_group_areas,
    (E'Content-type: application/pdf\n\n')::bytea ||form.safety_form_pdf as "safety_checklist.pdf",
    misd_card.card_id AS ro_card,
    misd_card.mifarenum AS ro_mifarenum,
    misd_card.mifareid AS ro_mifareid,
    misd_card.barcode AS ro_barcode,
    form.mifare_access_processed
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN misd_card ON lower(misd_card.crsid::text) = person.crsid::text
  WHERE form._imported_on <= 'now'::text::date AND form.mifare_access_processed = false;

ALTER TABLE hotwire3."10_View/People/MiFare_Access"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/MiFare_Access" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO reception;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO hr;

-- Trigger: mifare_update on hotwire3."10_View/People/MiFare_Access"

-- DROP TRIGGER mifare_update ON hotwire3."10_View/People/MiFare_Access";

CREATE TRIGGER mifare_update
  INSTEAD OF UPDATE
  ON hotwire3."10_View/People/MiFare_Access"
  FOR EACH ROW
  EXECUTE PROCEDURE registration.update_mifare();
