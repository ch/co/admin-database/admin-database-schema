DROP VIEW  hotwire3."10_View/People/Processed_Registration_Forms";

DROP VIEW hotwire3."10_View/People/Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Registration" AS 
 WITH reg AS (
         SELECT form.form_id AS id,
            form.first_names::character varying AS first_names,
            form.surname::character varying AS surname,
            form.known_as::character varying AS known_as,
            form.title_id,
            form.date_of_birth,
            form.post_category_id,
            form.nationality_id,
            form.job_title::character varying AS job_title,
            form.dept_room_id AS room_id,
            form.dept_telephone_id AS dept_telephone_number_id,
            form.college_id AS cambridge_college_id,
            form.home_address,
            form.home_phone_number::character varying AS home_phone_number,
            form.mobile_number::character varying AS mobile_number,
            form.email::character varying AS email,
            form.crsid,
            form.start_date,
            form.intended_end_date,
            form.home_institution::character varying AS home_institution,
            form.department_host_id AS supervisor_id,
            form.mifare_access_level_id,
            form.mifare_areas AS group_areas,
            form.deposit_receipt::character varying AS deposit_receipt,
            form.emergency_contact_name_1,
            form.emergency_contact_number_1,
            form.emergency_contact_name_2,
            form.emergency_contact_number_2,
            'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
            form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
            form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
            form.hide_from_website,
            form.previously_registered,
                CASE
                    WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
                    ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid, form.date_of_birth)
                END AS ro_automatically_matched_person_id,
            form._match_to_person_id AS match_to_person_id,
            false AS import_this_record,
            form._imported_on AS ro_imported_on,
            hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details,
                CASE
                    WHEN form._imported_on IS NOT NULL THEN 'green'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM registration.form
          WHERE form.submitted = true
        )
 SELECT reg.id,
    reg.first_names,
    reg.surname,
    reg.known_as,
    reg.title_id,
    reg.date_of_birth,
    reg.post_category_id,
    reg.nationality_id,
    reg.job_title,
    reg.room_id,
    reg.dept_telephone_number_id,
    reg.cambridge_college_id,
    reg.home_address,
    reg.home_phone_number,
    reg.mobile_number,
    reg.email,
    reg.crsid,
    reg.start_date,
    reg.intended_end_date,
    reg.home_institution,
    reg.supervisor_id,
    reg.mifare_access_level_id,
    reg.group_areas,
    reg.deposit_receipt,
    reg.emergency_contact_name_1,
    reg.emergency_contact_number_1,
    reg.emergency_contact_name_2,
    reg.emergency_contact_number_2,
    reg.ro_safety_induction_signed_by_id,
    reg.ro_safety_induction_signed_date,
    reg."safety_form.pdf",
    reg.hide_from_website,
    reg.previously_registered,
    reg.ro_automatically_matched_person_id,
    reg.match_to_person_id,
    reg.import_this_record,
    reg.ro_imported_on,
    reg.matched_person_details,
    reg._cssclass
   FROM reg
  ORDER BY reg.ro_imported_on DESC;

ALTER TABLE hotwire3."10_View/People/Registration"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Registration" TO hr;


CREATE TRIGGER registration_form_update
  INSTEAD OF UPDATE
  ON hotwire3."10_View/People/Registration"
  FOR EACH ROW
  EXECUTE PROCEDURE registration.people_registration_upd();


