DROP VIEW leave_recording.role;

CREATE OR REPLACE VIEW leave_recording.role AS
  SELECT
    CASE
      WHEN (staff_category.category = 'Assistant staff'::text) THEN 'leave_recording\assistant_staff_Role'::text
      ELSE 'leave_recording\database\Role'::text
    END AS role_class_name,
    post_history.id as post_id,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    post_history.start_date,
    post_history.end_date,
    post_history.intended_end_date,
            ( SELECT min(closest_date.end_date) AS min
                   FROM ( SELECT post_history_1.id,
                            post_history_1.end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.intended_end_date AS end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.funding_end_date AS end_date
                           FROM public.post_history post_history_1
                          GROUP BY post_history_1.id) closest_date
                  WHERE (closest_date.id = post_history.id)) AS estimated_leaving_date,
    CASE
        WHEN person.continuous_employment_start_date <= post_history.start_date THEN person.continuous_employment_start_date
        ELSE NULL -- we don't record historical values
    END AS continuous_employment_start_date_as_at_start_of_role,
    post_history.hours_worked,
    post_history.job_title,
    --post_history.staff_category_id,
    --('sc-'::text || (post_history.staff_category_id)::text) AS post_category_id,
    staff_category.category AS post_category,
    post_history.percentage_of_fulltime_hours,
    post_history.average_percentage_of_days_worked_per_week,
    post_history.weekends_are_standard_working_days,
    post_history.works_flexitime,
    post_history.standard_flexileave_rules_apply,
    post_history.departmental_closures_apply,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name
 FROM public.post_history
 LEFT JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = post_history.supervisor_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.continuous_employment_start_date
  FROM
    person) person ON person.person_id = post_history.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leave_recording.role
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role TO dev;
GRANT SELECT ON TABLE leave_recording.role TO www_leave_reporting;
