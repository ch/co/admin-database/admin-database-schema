
DROP VIEW hotwire3."10_View/Computers/Installer_Tags";

ALTER TABLE public.installer_tag_hid DROP COLUMN description;
ALTER TABLE public.installer_tag_hid DROP CONSTRAINT uppercase_tags;
