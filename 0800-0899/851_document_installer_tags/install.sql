ALTER TABLE public.installer_tag_hid ADD COLUMN description text;

ALTER TABLE public.installer_tag_hid ADD CONSTRAINT uppercase_tags CHECK (installer_tag_hid ~ '^[A-Z0-9_]+$');

CREATE VIEW hotwire3."10_View/Computers/Installer_Tags" AS
SELECT
   installer_tag_id as id,
   installer_tag_hid AS tag,
   description
FROM public.installer_tag_hid;

ALTER VIEW hotwire3."10_View/Computers/Installer_Tags" OWNER TO dev;
GRANT INSERT,UPDATE,SELECT ON hotwire3."10_View/Computers/Installer_Tags" TO cos;

-- no need to configure update triggers as this is a simple view
