CREATE VIEW registration.mifare_access_to_process AS
SELECT E'The following need their MiFare access setting up:\n\n' || string_agg(form.surname || COALESCE(', ' || form.first_names),E'\n')
FROM registration.form
WHERE _imported_on IS NOT NULL AND mifare_access_processed IS DISTINCT FROM 't';

ALTER VIEW registration.mifare_access_to_process OWNER TO dev;
