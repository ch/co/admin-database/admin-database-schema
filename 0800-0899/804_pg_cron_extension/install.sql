-- this must be run in the postgres database
-- as postgres, so it's once per database cluster, not once per database
create extension pg_cron version '1.0';
alter extension pg_cron update;
