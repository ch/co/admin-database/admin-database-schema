

DROP VIEW hotwire3."10_View/People/Registration";
DROP VIEW hotwire3."10_View/People/Processed_Registration_Forms";
ALTER TABLE registration.form DROP COLUMN separate_safety_form;

CREATE VIEW  hotwire3."10_View/People/Processed_Registration_Forms" AS
         SELECT form.form_id AS id,
            form.first_names::character varying AS first_names,
            form.surname::character varying AS surname,
            form.known_as::character varying AS known_as,
            form.title_id,
            form.date_of_birth,
            form.post_category_id,
            form.nationality_id,
            form.job_title::character varying AS job_title,
            form.dept_room_id AS room_id,
            form.dept_telephone_id AS dept_telephone_number_id,
            form.college_id AS cambridge_college_id,
            form.home_address,
            form.home_phone_number::character varying AS home_phone_number,
            form.mobile_number::character varying AS mobile_number,
            form.email::character varying AS email,
            form.crsid,
            form.start_date,
            form.intended_end_date,
            form.home_institution::character varying AS home_institution,
            form.department_host_id AS supervisor_id,
            form.mifare_access_level_id,
            form.mifare_areas AS group_areas,
            form.deposit_receipt::character varying AS deposit_receipt,
            form.emergency_contact_name_1,
            form.emergency_contact_number_1,
            form.emergency_contact_name_2,
            form.emergency_contact_number_2,
            'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
            form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
            form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
            form.hide_from_website,
            form.previously_registered,
            form._match_to_person_id AS match_to_person_id,
            false AS import_this_record,
            form._imported_on AS ro_imported_on,
            hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
           FROM registration.form
          WHERE form._imported_on IS NOT NULL
;

ALTER TABLE hotwire3."10_View/People/Processed_Registration_Forms"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO hr;
;


CREATE OR REPLACE VIEW hotwire3."10_View/People/Registration" AS 
         SELECT form.form_id AS id,
            form.first_names::character varying AS first_names,
            form.surname::character varying AS surname,
            form.known_as::character varying AS known_as,
            form.title_id,
            form.date_of_birth,
            form.post_category_id,
            form.nationality_id,
            form.job_title::character varying AS job_title,
            form.dept_room_id AS room_id,
            form.dept_telephone_id AS dept_telephone_number_id,
            form.college_id AS cambridge_college_id,
            form.home_address,
            form.home_phone_number::character varying AS home_phone_number,
            form.mobile_number::character varying AS mobile_number,
            form.email::character varying AS email,
            form.crsid,
            form.start_date,
            form.intended_end_date,
            form.home_institution::character varying AS home_institution,
            form.department_host_id AS supervisor_id,
            form.mifare_access_level_id,
            form.mifare_areas AS group_areas,
            form.deposit_receipt::character varying AS deposit_receipt,
            form.emergency_contact_name_1,
            form.emergency_contact_number_1,
            form.emergency_contact_name_2,
            form.emergency_contact_number_2,
            'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
            form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
            form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
            form.hide_from_website,
            form.previously_registered,
                CASE
                    WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
                    ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid, form.date_of_birth)
                END AS ro_automatically_matched_person_id,
            form._match_to_person_id AS match_to_person_id,
            false AS import_this_record,
            hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
           FROM registration.form
          WHERE form.submitted = true AND form._imported_on is null
;

ALTER TABLE hotwire3."10_View/People/Registration"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Registration" TO hr;


CREATE TRIGGER registration_form_update
  INSTEAD OF UPDATE
  ON hotwire3."10_View/People/Registration"
  FOR EACH ROW
  EXECUTE PROCEDURE registration.people_registration_upd();

