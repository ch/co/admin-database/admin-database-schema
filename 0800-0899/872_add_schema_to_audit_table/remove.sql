SET ROLE dev;

CREATE OR REPLACE FUNCTION public.audit_generic()
    RETURNS trigger
    LANGUAGE 'plperl'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

    my $rv;
    my @olddata;
    my @newdata;
    my $old;
    my $new;
    my $sql = <<EOSQL;

      insert into _audit ( operation, stamp, username, tablename, oldcols, newcols, id ) values ( 
        '$_TD->{event}',
                now(),
        current_user,
        '$_TD->{relname}',

EOSQL

    # find out who we are running as
    my $whoami = 'select current_user';
    $rv = spi_exec_query($whoami);
    my $username = $rv->{rows}[0]->{current_user};

    # fill in oldcols
    if ($_TD->{event} eq 'INSERT')  {
          $sql .= 'NULL, ';
    } elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $old = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{old}})) {
            # If the table has array columns this comparison can throw an exception
            eval {
                if ($val ne $_TD->{new}{$col}) {
                    push @olddata, $col . ':' . $val ;
                }
            };
            if ($@) {
                push @olddata, $col . ':' . $val ;
            }
            unless (!@olddata) {
                $old = join("\n",sort @olddata);
            }
        }
        $sql .= quote_nullable($old) . ', ';
    }

    # fill in newcols
    if ($_TD->{event} eq 'DELETE')  {
        $sql .= 'NULL, ';
    }
    elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $new = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{new}})) {
            # If the table has array columns this comparison can throw an exception
            eval {
                if ($val ne $_TD->{old}{$col}) {
                    push @newdata, $col . ':' . $val ;
                }
            };
            if ($@) {
                push @newdata, $col . ':' . $val;
            }
            unless (!@newdata) {
                    $new = join("\n",sort @newdata);
            }
        }
        $sql .= quote_nullable($new) . ', ';
    }

    # fill in primary key
    my $long_pk = $_TD->{table_name} . '_id';
    my $pk_col;
    # look for a primary key field in either of our standard forms
    # Have to do it like this because when we are updating views the user may not have
    # sufficient rights to look at the information schema on the underlying table. 
    foreach (keys(%{$_TD->{new}}), keys(%{$_TD->{old}})) {
        if ($_ eq $long_pk ) {
            $pk_col = $long_pk;
	} elsif ($_ eq 'id')  {
		$pk_col='id';
	}

    }    
    if (defined $pk_col )  {
        if ($_TD->{event} eq 'DELETE') {
            $sql .= quote_literal($_TD->{old}{$pk_col}) . ' )';
        } else {
            $sql .= quote_literal($_TD->{new}{$pk_col}) . ' )';
        }
    } else {
        $sql .= '0' . ')';
    }

    # for debugging
    #elog(NOTICE,$sql);

    # if anything changed, write to the audit log
    if (defined $old or defined $new) {
        $rv = spi_exec_query($sql);
        unless ($rv->{status} eq 'SPI_OK_INSERT') {
            elog(NOTICE,"trigger status: $rv->{status}");
            return 'SKIP'
        }
    }
    return undef; 
    
$BODY$;

ALTER FUNCTION public.audit_generic()
    OWNER TO dev;


CREATE OR REPLACE FUNCTION public.audit_ipaddress()
    RETURNS trigger
    LANGUAGE 'plperl'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

    my $do_audit = 0; # by default don'ti do the insert into _audit
    my $debug = 'noaudit';
    my $sql = <<EOSQL;
    insert into _audit ( operation, stamp, username, tablename, oldcols, newcols, id ) values ( 
    '$_TD->{event}',
              now(),
      current_user,
      '$_TD->{relname}',
EOSQL

     $old="";
     $new="";
     $quotestring='$audit$'; # safe string to quote with
     $newline ="\n"; # this insanity is needed because you can't pass posgres '\n' inside
                     # a dollar-quoted string, so we use Perl's escapes (?) to prduce a
                     # literal newline.

    # check if we have last_seen in $_TD->{new}
    if ($_TD->{event} eq 'UPDATE') 
    {
    #$debug=$debug . ' in loop';
    #   need to check if the only difference is the last_seen column
    # 
        while(($col, $val)=each(%{$_TD->{old}}))
        {
            #$debug=$debug . $val . ':' . $_TD->{new}->{$col} . ';';
            next if $col eq 'last_seen';
            if ( $val ne $_TD->{new}->{$col} )
            {
                $do_audit=1;
                #$debug='audit';
                last;
            }
        }
        if ( $do_audit == 0 )
        {
            return;
        }
    } 

    # do regular audit trigger here
    # can we just call it? no, have to repeat it
     if ($_TD->{event} eq 'INSERT')
     {
        $sql .= "NULL, ";
     }
     else
     {
         while(($col, $val)=each(%{$_TD->{old}}))
         {
             $old .= $col . ':' . $val . $newline ;
         }
         $sql .= $quotestring . $old . $quotestring . ', ';
     }

     if ($_TD->{event} eq 'DELETE')
     {
         #$sql .= "NULL, " . $quotestring . $debug . $quotestring . ")\n";
         $sql .= "NULL , ";
     }
     else
     {
        while(($col, $val)=each(%{$_TD->{new}}))
        {
            $new .=  $col . ':' . $val . $newline;
        }
        #$sql .= $quotestring . $new . $quotestring . ',' . $quotestring .  $debug . $quotestring . ')';
        $sql .= $quotestring . $new . $quotestring . ', ';
     }

     if ($_TD->{event} eq 'DELETE')
     {
        $sql .= $quotestring . $_TD->{old}->{'id'} . $quotestring . ')';
     }
     else
     {
        $sql .= $quotestring . $_TD->{new}->{'id'} . $quotestring . ')';
     }

     my $rv = spi_exec_query($sql);

     unless ($rv->{status} eq 'SPI_OK_INSERT')
     {
         elog(NOTICE,"trigger status: $rv->{status}");
         return 'SKIP'
     }

    return undef; 
    
# vim:syntax=perl:sw=4
$BODY$;

SET ROLE cen1001;
ALTER FUNCTION public.audit_ipaddress()
    OWNER TO cen1001;

ALTER FUNCTION public.audit_it_task()
    OWNER TO cen1001;
SET ROLE dev;


CREATE OR REPLACE FUNCTION public.audit_user_account()
    RETURNS trigger
    LANGUAGE 'plperl'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
# This is a lightly edited version of the source for audit_generic()
    my $rv;
    my @olddata;
    my @newdata;
    my $old;
    my $new;
    my $sql = <<EOSQL;

      insert into _audit ( operation, stamp, username, tablename, oldcols, newcols, id ) values ( 
        '$_TD->{event}',
                now(),
        current_user,
        '$_TD->{relname}',

EOSQL

    # find out who we are running as
    my $whoami = 'select current_user';
    $rv = spi_exec_query($whoami);
    my $username = $rv->{rows}[0]->{current_user};

    # fill in oldcols
    if ($_TD->{event} eq 'INSERT')  {
          $sql .= 'NULL, ';
    } elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $old = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{old}})) {
            if ($col ne 'chemnet_token') {
                # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{new}{$col}) {
                        push @olddata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @olddata, $col . ':' . $val ;
                }
                unless (!@olddata) {
                    $old = join("\n",sort @olddata);
                }
            } elsif ($val ne $_TD->{new}{'chemnet_token'}) {
                    push @olddata, $col . ': redacted' ;
            }
        }
        $sql .= quote_nullable($old) . ', ';
    }

    # fill in newcols
    if ($_TD->{event} eq 'DELETE')  {
        $sql .= 'NULL, ';
    }
    elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $new = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{new}})) {
            if ($col ne 'chemnet_token') {
                # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{old}{$col}) {
                        push @newdata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @newdata, $col . ':' . $val;
                }
                unless (!@newdata) {
                        $new = join("\n",sort @newdata);
                }
            } elsif ($val ne $_TD->{old}{'chemnet_token'}) {
                    push @newdata, $col . ': redacted' ;
            }
        }
        $sql .= quote_nullable($new) . ', ';
    }

    # fill in primary key
    my $long_pk = $_TD->{table_name} . '_id';
    my $pk_col;
    # look for a primary key field in either of our standard forms
    # Have to do it like this because when we are updating views the user may not have
    # sufficient rights to look at the information schema on the underlying table. 
    foreach (keys(%{$_TD->{new}}), keys(%{$_TD->{old}})) {
        if ($_ eq $long_pk ) {
            $pk_col = $long_pk;
	} elsif ($_ eq 'id')  {
		$pk_col='id';
	}

    }    
    if (defined $pk_col )  {
        if ($_TD->{event} eq 'DELETE') {
            $sql .= quote_literal($_TD->{old}{$pk_col}) . ' )';
        } else {
            $sql .= quote_literal($_TD->{new}{$pk_col}) . ' )';
        }
    } else {
        $sql .= '0' . ')';
    }

    # for debugging
    #elog(NOTICE,$sql);

    # if anything changed, write to the audit log
    if (defined $old or defined $new) {
        $rv = spi_exec_query($sql);
        unless ($rv->{status} eq 'SPI_OK_INSERT') {
            elog(NOTICE,"trigger status: $rv->{status}");
            return 'SKIP'
        }
    }
    return undef; 
    
$BODY$;

ALTER FUNCTION public.audit_user_account()
    OWNER TO dev;

CREATE FUNCTION public.generic_audit()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$ 
declare
    audittable varchar(64);
    delete_command text;
    update_command text;
    insert_command text;
    new_cols text;
    old_cols text;
begin
    select into new_cols NEW.*;
    select into old_cols OLD.*;
    audittable := TG_TABLE_NAME || '_audit';
    delete_command := 'insert into ' 
		|| audittable 
		|| ' ' 
		|| quote_literal('D') 	
		|| ' , ' 
		|| quote_literal(now()) 
		|| ' , ' 
		|| quote_literal(user) 
		|| ' , ' 
		|| old_cols,
--		|| ', '
		|| quote_ident(TG_TABLE_NAME) ;
    update_command := 'insert into ' 
		|| audittable 
		|| ' ' 
		|| quote_literal('U') 	
		|| ' , ' 
		|| quote_literal(now()) 
		|| ' , ' 
		|| quote_literal(user) 
		|| ' , ' 
		|| new_cols
--		|| ', '
		|| quote_ident(TG_TABLE_NAME) ;
    insert_command := 'insert into ' 
		|| audittable 
		|| ' ' 
		|| quote_literal('I') 	
		|| ' , ' 
		|| quote_literal(now()) 
		|| ' , ' 
		|| quote_literal(user) 
		|| ' , ' 
		|| new_cols
--		|| ', '
		|| quote_ident(TG_TABLE_NAME) ;
	IF (TG_OP = 'DELETE') THEN
		execute delete_command;
		return old;
	ELSIF (TG_OP = 'UPDATE') THEN
		execute update_command;
		RETURN NEW;
	ELSIF (TG_OP = 'INSERT') THEN
		execute insert_command;
		RETURN NEW;
	END IF;
	RETURN NULL;
end ;$BODY$;

SET ROLE cen1001;
ALTER FUNCTION public.generic_audit()
    OWNER TO cen1001;
SET ROLE dev;

ALTER TABLE public._audit DROP COLUMN schemaname ;
