CREATE OR REPLACE VIEW registration.completed_form_summary AS 
 SELECT form.uuid,
    form.first_names,
    form.surname,
    form.known_as,
    title.hid AS title,
    form.date_of_birth,
    post_category.hid AS post_category,
    nationality.nationality,
    form.job_title,
    rooms_by_uuid.rooms,
    phones.phones,
    college.hid AS college,
    form.home_address,
    form.home_phone_number,
    form.mobile_number,
    form.email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution,
    supervisor.hid AS supervisor,
    form.deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    form.hide_from_website,
    form.previously_registered
   FROM registration.form
     LEFT JOIN registration.title_hid title ON title.id = form.title_id
     LEFT JOIN registration.college_hid college ON college.id = form.college_id
     LEFT JOIN registration.supervisor_hid supervisor ON supervisor.id = form.department_host_id
     LEFT JOIN registration.post_category_hid post_category ON post_category.id = form.post_category_id
     LEFT JOIN ( SELECT form_1.uuid,
            string_agg(room_hid.hid::text, ', '::character varying::text) AS rooms
           FROM registration.form form_1,
            LATERAL unnest(form_1.dept_room_id) id(id)
             JOIN registration.room_hid USING (id)
          GROUP BY form_1.uuid) rooms_by_uuid USING (uuid)
     LEFT JOIN ( SELECT form_1.uuid,
            string_agg(phone_hid.hid::text, ', '::character varying::text) AS phones
           FROM registration.form form_1,
            LATERAL unnest(form_1.dept_telephone_id) id(id)
             JOIN registration.phone_hid USING (id)
          GROUP BY form_1.uuid) phones USING (uuid)
     LEFT JOIN ( SELECT form_1.uuid,
            string_agg(nationality_hid.hid::text, ', '::character varying::text) AS nationality
           FROM registration.form form_1,
            LATERAL unnest(form_1.nationality_id) id(id)
             JOIN registration.nationality_hid USING (id)
          GROUP BY form_1.uuid) nationality USING (uuid)
  WHERE form.submitted = true;

ALTER TABLE registration.completed_form_summary
  OWNER TO dev;
GRANT ALL ON TABLE registration.completed_form_summary TO dev;
GRANT SELECT ON TABLE registration.completed_form_summary TO starters_registration;
GRANT SELECT, UPDATE ON TABLE registration.completed_form_summary TO hr;
COMMENT ON VIEW registration.completed_form_summary
  IS 'Used by the registation webapp to generate nicelly formatted email summaries of each form with foreign keys resolved';

