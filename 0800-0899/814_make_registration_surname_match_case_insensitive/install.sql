CREATE OR REPLACE FUNCTION registration.match_existing_person(
    new_surname character varying,
    new_first_names character varying,
    new_crsid character varying,
    new_date_of_birth date)
  RETURNS bigint AS
$BODY$
DECLARE
    matched_id BIGINT;
BEGIN
    -- if we have a crsid, look for that first
    IF new_crsid IS NOT NULL THEN
    -- if no crsid, try to match surname, date of birth, and initial of first_names
        SELECT id INTO matched_id FROM public.person WHERE crsid = new_crsid;
    END IF;
    BEGIN
        IF matched_id IS NULL THEN
            SELECT id INTO STRICT matched_id FROM person WHERE LOWER(surname) = LOWER(new_surname) AND date_of_birth = new_date_of_birth AND LOWER(LEFT(first_names,1)) = LOWER(LEFT(new_first_names,1));
            -- If we didn't find a unique match, there is no match
        END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
            WHEN TOO_MANY_ROWS THEN
                NULL;
        END;
    RETURN matched_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION registration.match_existing_person(character varying, character varying, character varying, date)
  OWNER TO dev;
