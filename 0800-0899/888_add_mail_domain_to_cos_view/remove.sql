DROP VIEW hotwire3."10_View/People/COs_View";

CREATE OR REPLACE VIEW hotwire3."10_View/People/COs_View" AS
SELECT
    a.id,
    a.ro_person_id,
    a.image_oid,
    a.surname,
    a.first_names,
    a.title_id,
    a.name_suffix,
    a.email_address,
    a.crsid,
    a.date_of_birth,
    a.ro_post_category_id,
    a.ro_supervisor_id,
    a.research_group_id,
    a.dept_telephone_number_id,
    a.room_id,
    a.location,
    a.ro_physical_status_id,
    a.ro_estimated_leaving_date,
    a.do_not_show_on_website AS hide_person_from_website,
    a.hide_photo_from_website,
    a.hide_email AS hide_email_from_website,
    a.is_spri,
    a.ro_auto_banned_from_network,
    a.override_auto_ban,
    a.ro_network_ban_log,
    a._cssclass
FROM (
    SELECT
        person.id,
        person.id AS ro_person_id,
        person.image_lo AS image_oid,
        person.surname,
        person.first_names,
        person.title_id,
        person.name_suffix,
        person.email_address,
        person.crsid,
        person.date_of_birth,
        person.do_not_show_on_website,
        person.hide_photo_from_website,
        person.hide_email,
        person.is_spri,
        _latest_role.post_category_id AS ro_post_category_id,
        _latest_role.supervisor_id AS ro_supervisor_id,
        ARRAY (
            SELECT
                mm_person_research_group.research_group_id
            FROM
                mm_person_research_group
            WHERE
                person.id = mm_person_research_group.person_id) AS research_group_id,
            ARRAY (
                SELECT
                    mm_person_dept_telephone_number.dept_telephone_number_id
                FROM
                    mm_person_dept_telephone_number
                WHERE
                    person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
                ARRAY (
                    SELECT
                        mm_person_room.room_id
                    FROM
                        mm_person_room
                    WHERE
                        person.id = mm_person_room.person_id) AS room_id,
                    person.location,
                    _physical_status.status_id AS ro_physical_status_id,
                    LEAST (person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
                    person.auto_banned_from_network AS ro_auto_banned_from_network,
                    person.override_auto_ban,
                    person.network_ban_log::text AS ro_network_ban_log,
                    CASE WHEN _physical_status.status_id::text = 'Past'::text THEN
                        'orange'::text
                    ELSE
                        NULL::text
                    END AS _cssclass
                FROM
                    person
                LEFT JOIN cache._latest_role_v12 _latest_role ON person.id = _latest_role.person_id
                LEFT JOIN cache.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
                LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id) a
ORDER BY
    a.surname,
    a.first_names;

ALTER TABLE hotwire3."10_View/People/COs_View" OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO cos;

GRANT ALL ON TABLE hotwire3."10_View/People/COs_View" TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/COs_View" TO hr;

CREATE OR REPLACE FUNCTION hotwire3.people_cos_view_trig ()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100 VOLATILE NOT LEAKPROOF
    AS $BODY$
BEGIN
    IF NEW.id IS NOT NULL THEN
        UPDATE
            person
        SET
            surname = NEW.surname,
            image_lo = NEW.image_oid,
            first_names = NEW.first_names,
            title_id = NEW.title_id,
            name_suffix = NEW.name_suffix,
            email_address = NEW.email_address,
            crsid = NEW.crsid,
            date_of_birth = NEW.date_of_birth,
            location = NEW.location,
            do_not_show_on_website = NEW.hide_person_from_website,
            hide_photo_from_website = NEW.hide_photo_from_website,
            hide_email = NEW.hide_email_from_website,
            is_spri = NEW.is_spri,
            override_auto_ban = NEW.override_auto_ban
        WHERE
            person.id = NEW.id;
    ELSE
        NEW.id := nextval('person_id_seq');
        INSERT INTO person (id, surname, image_lo, first_names, title_id, name_suffix, email_address, crsid, date_of_birth, location, do_not_show_on_website, hide_photo_from_website, hide_email, is_spri, override_auto_ban)
            VALUES (NEW.id, NEW.surname, NEW.image_oid, NEW.first_names, NEW.title_id, NEW.name_suffix, NEW.email_address, NEW.crsid, NEW.date_of_birth, NEW.location, coalesce(NEW.hide_person_from_website, 'f'), coalesce(NEW.hide_photo_from_website, 'f'), NEW.hide_email_from_website, NEW.is_spri, NEW.override_auto_ban);
    END IF;
    PERFORM
        fn_mm_array_update (NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar, 'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.research_group_id, 'mm_person_research_group'::varchar, 'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
    PERFORM
        fn_mm_array_update (NEW.room_id, 'mm_person_room'::varchar, 'person_id'::varchar, 'room_id'::varchar, NEW.id);
    RETURN NEW;
END;
$BODY$;

CREATE OR REPLACE RULE hotwire3_people_cos_view_del AS ON DELETE TO hotwire3."10_View/People/COs_View"
    DO INSTEAD
    (
        DELETE
    FROM
        person
    WHERE (person.id = old.id)
);

CREATE TRIGGER hotwire3_people_cos_view_ins
    INSTEAD OF INSERT ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig ();

CREATE TRIGGER hotwire3_people_cos_view_upd
    INSTEAD OF UPDATE ON hotwire3."10_View/People/COs_View"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.people_cos_view_trig ();

