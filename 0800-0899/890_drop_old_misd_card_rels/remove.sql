CREATE TABLE public.misd_card (
    surname character varying COLLATE pg_catalog."default",
    forenames character varying COLLATE pg_catalog."default",
    card_id character varying COLLATE pg_catalog."default",
    dob date,
    grade character varying COLLATE pg_catalog."default",
    crsid character varying COLLATE pg_catalog."default",
    usn character varying COLLATE pg_catalog."default",
    issued date,
    expires date,
    issuenum integer,
    mifarenum integer,
    mifareid bigint,
    barcode character varying COLLATE pg_catalog."default",
    mifarehexid character varying COLLATE pg_catalog."default"
);

ALTER TABLE public.misd_card OWNER TO dev;

GRANT INSERT, TRUNCATE ON TABLE public.misd_card TO _misd_import;

GRANT ALL ON TABLE public.misd_card TO dev;

