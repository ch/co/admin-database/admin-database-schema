CREATE OR REPLACE VIEW hotwire3."10_View/People/Photography_Registration"
 AS
WITH futuremost_role_estimated_leaving_date AS (
    SELECT DISTINCT ON (person_id) person_id,
                                   estimated_leaving_date
    FROM _all_roles_v13
    ORDER BY person_id, start_date DESC, estimated_leaving_date DESC
)
 SELECT person.id,
    person.crsid AS ro_crsid,
    person.gender_id,
    person.first_names,
    person.known_as,
    person.surname,
    person.date_of_birth,
    person.arrival_date AS ro_arrival_date,
    LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date,
    ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
    person.image_lo AS image
   FROM person
     LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     LEFT JOIN futuremost_role_estimated_leaving_date futuremost_role ON person.id = futuremost_role.person_id
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire3."10_View/People/Photography_Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Photography_Registration" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/People/Photography_Registration" TO photography;

