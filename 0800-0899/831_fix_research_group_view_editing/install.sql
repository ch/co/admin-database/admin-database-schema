DROP VIEW hotwire3."10_View/Groups/Research_Interest_Group";
DROP FUNCTION hotwire3.research_interest_group_ins();

CREATE VIEW hotwire3.other_member_hid AS
 SELECT person_hid.person_id AS other_member_id,
        person_hid.person_hid AS other_member_hid
 FROM hotwire3.person_hid;
ALTER VIEW hotwire3.other_member_hid OWNER TO dev;
GRANT SELECT ON hotwire3.other_member_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Interest_Group" AS 
 SELECT research_interest_group.id,
    research_interest_group.name::character varying(80) AS name,
    research_interest_group.chair_id,
    research_interest_group.website,
    research_interest_group.mailing_list_address,
    ARRAY( SELECT mm_member_research_interest_group.member_id
           FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id AND mm_member_research_interest_group.is_primary) AS primary_member_id,
   ARRAY( SELECT mm_member_research_interest_group.member_id
          FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id AND mm_member_research_interest_group.is_primary is distinct from 't') AS other_member_id
   FROM research_interest_group;

ALTER TABLE hotwire3."10_View/Groups/Research_Interest_Group" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO rig_management;


CREATE OR REPLACE FUNCTION hotwire3.research_interest_group_ins()
  RETURNS trigger AS
$BODY$
begin
    insert into research_interest_group (  name,chair_id,website,mailing_list_address ) values ( new.name,new.chair_id,new.website,new.mailing_list_address) returning id into new.id;
    perform fn_mm_array_update(NEW.primary_member_id||NEW.other_member_id,
                               OLD.primary_member_id||OLD.other_member_id,
                               'mm_member_research_interest_group',
                               'research_interest_group_id',
                               'member_id',
                               new.id);
    perform hotwire3.primary_rig_update(NEW.primary_member_id,OLD.primary_member_id,NEW.id);
                               
    return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.research_interest_group_ins()
  OWNER TO dev;

CREATE TRIGGER hotwire3_view_research_interest_group_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Groups/Research_Interest_Group"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.research_interest_group_ins();

CREATE OR REPLACE FUNCTION hotwire3.research_interest_group_upd()
  RETURNS trigger AS
$BODY$
begin
    UPDATE research_interest_group SET
        name = NEW.name,
        chair_id = NEW.chair_id,
        website = NEW.website,
        mailing_list_address = NEW.mailing_list_address
    WHERE id = OLD.id;
    -- Ensure all the members are actually members
    perform fn_mm_array_update(NEW.primary_member_id||NEW.other_member_id,
                               OLD.primary_member_id||OLD.other_member_id,
                               'mm_member_research_interest_group',
                               'research_interest_group_id',
                               'member_id',
                               new.id);
    -- Update the primary membership flags
    perform hotwire3.primary_rig_update(NEW.primary_member_id,OLD.primary_member_id,NEW.id);
    return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.research_interest_group_upd()
  OWNER TO dev;

CREATE TRIGGER hotwire3_view_research_interest_group_upd
  INSTEAD OF UPDATE
  ON hotwire3."10_View/Groups/Research_Interest_Group"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.research_interest_group_upd();
