DROP VIEW hotwire3."10_View/Groups/Research_Interest_Group";
DROP FUNCTION hotwire3.research_interest_group_upd();
DROP VIEW hotwire3.other_member_hid;

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Interest_Group" AS 
 SELECT research_interest_group.id,
    research_interest_group.name::character varying(80) AS name,
    research_interest_group.chair_id,
    research_interest_group.website,
    research_interest_group.mailing_list_address,
    ARRAY( SELECT mm_member_research_interest_group.member_id
           FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id) AS rig_member_id,
    ARRAY( SELECT mm_member_research_interest_group.member_id
           FROM mm_member_research_interest_group
          WHERE mm_member_research_interest_group.research_interest_group_id = research_interest_group.id AND mm_member_research_interest_group.is_primary) AS primary_member_id
   FROM research_interest_group;

ALTER TABLE hotwire3."10_View/Groups/Research_Interest_Group"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Groups/Research_Interest_Group" TO rig_management;


CREATE OR REPLACE RULE hotwire3_view_research_interest_group_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Interest_Group" DO INSTEAD ( UPDATE research_interest_group SET name = new.name, chair_id = new.chair_id, website = new.website, mailing_list_address = new.mailing_list_address
  WHERE research_interest_group.id = old.id;
 SELECT fn_mm_array_update(new.rig_member_id, old.rig_member_id, 'mm_member_research_interest_group'::character varying, 'research_interest_group_id'::character varying, 'member_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT hotwire3.primary_rig_update(new.primary_member_id, old.primary_member_id, old.id) AS fn_primary_rig_update;
);


CREATE OR REPLACE FUNCTION hotwire3.research_interest_group_ins()
  RETURNS trigger AS
$BODY$
begin
    insert into research_interest_group (  name,chair_id,website,mailing_list_address ) values ( new.name,new.chair_id,new.website,new.mailing_list_address) returning id into new.id;
    perform fn_mm_array_update(new.rig_member_id,
                               array[]::bigint[],
                               'mm_member_research_interest_group',
                               'research_interest_group_id',
                               'member_id',
                               new.id);
    perform hotwire3.primary_rig_update(new.primary_member_id,array[]::bigint[],new.id);
                               
    return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.research_interest_group_ins()
  OWNER TO dev;

CREATE TRIGGER hotwire3_view_research_interest_group_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Groups/Research_Interest_Group"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.research_interest_group_ins();
