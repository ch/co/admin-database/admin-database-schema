-- hotwire3."10_View/People/Swipe_Cards"
DROP VIEW hotwire3."10_View/People/Swipe_Cards";
CREATE OR REPLACE VIEW hotwire3."10_View/People/Swipe_Cards"
 AS
 SELECT university_card.id AS id,
    university_card.surname,
    university_card.forename,
    university_card.full_name,
    university_card.crsid,
    university_card.issued_at,
    university_card.expires_at,
    university_card.issue_number,
    university_card.legacy_card_holder_id as card_identifier,
    university_card.mifare_id as mifare_identifier,
    university_card.mifare_number,
    university_card.barcode
   FROM university_card
   ORDER BY surname,forename NULLS LAST;

ALTER TABLE hotwire3."10_View/People/Swipe_Cards" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO accounts;
GRANT ALL ON TABLE hotwire3."10_View/People/Swipe_Cards" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO reception;

-- hotwire3."10_View/People/MiFare_Access"

DROP VIEW hotwire3."10_View/People/MiFare_Access";
CREATE OR REPLACE VIEW hotwire3."10_View/People/MiFare_Access"
 AS
 SELECT form.form_id AS id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.crsid AS ro_crsid,
    form.mifare_access_level_id AS ro_mifare_access_level_id,
    form.mifare_areas AS ro_group_areas,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    university_card.legacy_card_holder_id AS ro_card,
    university_card.mifare_number AS ro_mifarenum,
    university_card.mifare_id AS ro_mifare_identifier,
    university_card.barcode AS ro_barcode,
    form.mifare_access_processed
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN university_card ON lower(university_card.crsid::text) = person.crsid::text
  WHERE form._imported_on <= 'now'::text::date AND form.mifare_access_processed = false;

ALTER TABLE hotwire3."10_View/People/MiFare_Access" OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/MiFare_Access" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO reception;

CREATE TRIGGER mifare_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/MiFare_Access"
    FOR EACH ROW
    EXECUTE PROCEDURE registration.update_mifare();


-- apps.swipe_card_expiry
-- don't drop this one, to ensure we keep the interface for the application identical
CREATE OR REPLACE VIEW apps.swipe_card_expiry
 AS
 SELECT university_card.surname::varchar,
    university_card.forename::varchar as forenames,
    university_card.expires_at::date as expires,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date
   FROM university_card
     LEFT JOIN person ON lower(university_card.crsid::text) = person.crsid::text
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id;

ALTER TABLE apps.swipe_card_expiry OWNER TO dev;

GRANT SELECT ON TABLE apps.swipe_card_expiry TO cos;
GRANT ALL ON TABLE apps.swipe_card_expiry TO dev;
GRANT SELECT ON TABLE apps.swipe_card_expiry TO leavers_management;
