-- CREATE TABLE public.misd_card
-- (
--     surname character varying COLLATE pg_catalog."default",
--     forenames character varying COLLATE pg_catalog."default",
--     card_id character varying COLLATE pg_catalog."default",
--     dob date,
--     grade character varying COLLATE pg_catalog."default",
--     crsid character varying COLLATE pg_catalog."default",
--     usn character varying COLLATE pg_catalog."default",
--     issued date,
--     expires date,
--     issuenum integer,
--     mifarenum integer,
--     mifareid bigint,
--     barcode character varying COLLATE pg_catalog."default",
--     mifarehexid character varying COLLATE pg_catalog."default"
-- );
-- 
-- ALTER TABLE public.misd_card OWNER to dev;
-- 
-- GRANT INSERT, TRUNCATE ON TABLE public.misd_card TO _misd_import;
-- 
-- GRANT ALL ON TABLE public.misd_card TO dev;

-- hotwire3."10_View/People/Swipe_Cards"
DROP VIEW hotwire3."10_View/People/Swipe_Cards";
CREATE OR REPLACE VIEW hotwire3."10_View/People/Swipe_Cards"
 AS
 SELECT misd_card.card_id AS id,
    misd_card.surname,
    misd_card.forenames,
    misd_card.card_id AS card,
    misd_card.dob,
    misd_card.grade,
    misd_card.crsid,
    misd_card.usn,
    misd_card.issued,
    misd_card.expires,
    misd_card.issuenum,
    misd_card.mifarenum,
    misd_card.mifareid,
    misd_card.barcode
   FROM misd_card;

ALTER TABLE hotwire3."10_View/People/Swipe_Cards" OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO accounts;
GRANT ALL ON TABLE hotwire3."10_View/People/Swipe_Cards" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Swipe_Cards" TO reception;

-- hotwire3."10_View/People/MiFare_Access"

DROP VIEW  hotwire3."10_View/People/MiFare_Access";
CREATE OR REPLACE VIEW hotwire3."10_View/People/MiFare_Access"
 AS
 SELECT form.form_id AS id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.crsid AS ro_crsid,
    form.mifare_access_level_id AS ro_mifare_access_level_id,
    form.mifare_areas AS ro_group_areas,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    misd_card.card_id AS ro_card,
    misd_card.mifarenum AS ro_mifarenum,
    misd_card.mifareid AS ro_mifareid,
    misd_card.barcode AS ro_barcode,
    form.mifare_access_processed
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN misd_card ON lower(misd_card.crsid::text) = person.crsid::text
  WHERE form._imported_on <= 'now'::text::date AND form.mifare_access_processed = false;

ALTER TABLE hotwire3."10_View/People/MiFare_Access" OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/MiFare_Access" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/MiFare_Access" TO reception;

CREATE TRIGGER mifare_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/MiFare_Access"
    FOR EACH ROW
    EXECUTE PROCEDURE registration.update_mifare();


-- apps.swipe_card_expiry
CREATE OR REPLACE VIEW apps.swipe_card_expiry
 AS
 SELECT misd_card.surname,
    misd_card.forenames,
    misd_card.expires,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date
   FROM misd_card
     LEFT JOIN person ON lower(misd_card.crsid::text) = person.crsid::text
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id;

ALTER TABLE apps.swipe_card_expiry
    OWNER TO dev;

GRANT SELECT ON TABLE apps.swipe_card_expiry TO cos;
GRANT ALL ON TABLE apps.swipe_card_expiry TO dev;
GRANT SELECT ON TABLE apps.swipe_card_expiry TO leavers_management;
