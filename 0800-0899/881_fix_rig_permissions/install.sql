ALTER TABLE public.research_interest_group
    OWNER to dev;

-- alt36, cen1001 are granted whatever rights they deserve by virtue of dev role so
-- shouldn't have explicitly-granted permissions
REVOKE ALL ON TABLE public.research_interest_group FROM alt36;
REVOKE ALL ON TABLE public.research_interest_group FROM cen1001;

-- If we were to ever add a new RIG it would be done with great care
REVOKE INSERT ON TABLE public.research_interest_group FROM rig_management;

GRANT UPDATE,SELECT on TABLE public.research_interest_group TO rig_management;

GRANT SELECT ON TABLE public.research_interest_group TO www_sites;
