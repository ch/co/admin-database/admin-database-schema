ALTER TABLE public.research_interest_group
    OWNER to cen1001;

GRANT ALL ON TABLE public.research_interest_group TO alt36;

GRANT ALL ON TABLE public.research_interest_group TO cen1001;

GRANT ALL ON TABLE public.research_interest_group TO dev;

GRANT INSERT ON TABLE public.research_interest_group TO rig_management;

GRANT SELECT ON TABLE public.research_interest_group TO www_sites;
