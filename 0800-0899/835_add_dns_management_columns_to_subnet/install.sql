ALTER TABLE public.subnet ADD COLUMN upload_records_to_central_dns BOOLEAN NOT NULL DEFAULT 'f';
ALTER TABLE public.subnet ADD COLUMN use_local_records_for_dns BOOLEAN NOT NULL DEFAULT 't';
