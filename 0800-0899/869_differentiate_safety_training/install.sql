ALTER TABLE public.staff_category ADD COLUMN safety_training_url text;
ALTER TABLE public.staff_category ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.visitor_type_hid ADD COLUMN safety_training_url text;
ALTER TABLE public.visitor_type_hid ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.postgraduate_studentship_type ADD COLUMN safety_training_url text;
ALTER TABLE public.postgraduate_studentship_type ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.erasmus_type_hid ADD COLUMN safety_training_url text;
ALTER TABLE public.erasmus_type_hid ADD COLUMN check_safety_training boolean default false;

-- create a view to list them all
CREATE VIEW registration.safety_training AS
SELECT
'e-'::text || erasmus_type_hid.erasmus_type_id::text AS id,
safety_training_url,
check_safety_training
FROM public.erasmus_type_hid
UNION
SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS id,
safety_training_url,
check_safety_training
FROM public.visitor_type_hid
UNION
SELECT 'pg-'::text || postgraduate_studentship_type.id::text AS id,
safety_training_url,
check_safety_training
FROM public.postgraduate_studentship_type
UNION
SELECT 'sc-'::text || staff_category.id::text AS id,
  safety_training_url,
  check_safety_training
FROM public.staff_category
;

ALTER VIEW registration.safety_training OWNER TO dev;
GRANT SELECT ON registration.safety_training TO starters_registration;
