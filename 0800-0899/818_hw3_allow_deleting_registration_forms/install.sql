CREATE OR REPLACE RULE hotwire3_view_registration_del AS
    ON DELETE TO hotwire3."10_View/People/Registration" DO INSTEAD  DELETE FROM registration.form
      WHERE form.form_id = old.id;

GRANT DELETE ON hotwire3."10_View/People/Registration" TO hr;
