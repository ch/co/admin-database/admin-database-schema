-- create trigger function

CREATE FUNCTION public.physical_status_updates() RETURNS TRIGGER AS
$body$
-- this can't look at OLD because we may be running it from cron where OLD and NEW will be the same

DECLARE
    old_physical_status varchar(20) = 'Unknown';
    new_physical_status varchar(20) = 'Unknown';

BEGIN

-- check for an entry in _physical_status_v3 and if we find it, load it
    SELECT status_id FROM public._physical_status_v3 WHERE person_id = NEW.id INTO old_physical_status;
    IF NOT FOUND
    THEN
        old_physical_status = 'Unknown';
	-- insert the missing entry; if we don't do this we end up with people with no dates set not being present
        -- in _physical_status_v3 at all because their status never changes
	INSERT INTO _physical_status_v3 ( person_id, status_id) VALUES (NEW.id, old_physical_status);
    END IF;

-- calculate new physical status, see if it changed
    SELECT fn_calculate_physical_status(NEW.arrival_date,NEW.leaving_date,NEW.left_but_no_leaving_date_given) INTO new_physical_status;

-- if it changed update _physical_status_v3 and physical_status_changelog
    IF new_physical_status IS DISTINCT FROM old_physical_status
    THEN
        UPDATE _physical_status_v3 SET status_id = new_physical_status WHERE person_id = NEW.id;
	INSERT INTO _physical_status_changelog ( person_id,old_status_id, new_status_id, stamp, username ) VALUES (NEW.id,old_physical_status, new_physical_status,current_timestamp,current_user);
	-- check for -> Past change, unset review date due
	IF new_physical_status = 'Past'
	THEN
	    NEW.next_review_due = null;
	END IF;
	-- Check for -> Current change, unset it extension date
	IF new_physical_status = 'Current'
	THEN
	    NEW.leavers_it_extension_end_date = null;
	END IF;
    END IF;
    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION public.physical_status_updates() OWNER TO dev;

-- drop old triggers

DROP TRIGGER physical_status_trig ON person;
DROP TRIGGER unset_review_due_date ON person;

-- create new trigger

CREATE TRIGGER physical_status_updates BEFORE UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given
    ON public.person
    FOR EACH ROW
    EXECUTE PROCEDURE public.physical_status_updates();

-- create new function to be called from cron

CREATE FUNCTION public.cron_physical_status_updates() RETURNS void AS
$body$
-- trivial function to fire the status change triggers
UPDATE person SET left_but_no_leaving_date_given = left_but_no_leaving_date_given;
$body$ LANGUAGE sql;

ALTER FUNCTION public.cron_physical_status_updates() OWNER TO dev;

-- drop old trigger functions

DROP FUNCTION public.unset_review_due_date_on_leaving();
DROP FUNCTION public.update_physical_status_log();
