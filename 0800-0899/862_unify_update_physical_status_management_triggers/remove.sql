-- Create old trigger functions

CREATE FUNCTION public.unset_review_due_date_on_leaving()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    new_physical_status varchar(20);
    old_physical_status varchar(20);
BEGIN
    select fn_calculate_physical_status(NEW.arrival_date,NEW.leaving_date,NEW.left_but_no_leaving_date_given) into new_physical_status;
    select fn_calculate_physical_status(OLD.arrival_date,OLD.leaving_date,OLD.left_but_no_leaving_date_given) into old_physical_status;
    if new_physical_status = 'Past' and new_physical_status is distinct from old_physical_status
    then
        new.next_review_due := NULL::date;
    end if;
    RETURN NEW;
    
END;
$BODY$;

ALTER FUNCTION public.unset_review_due_date_on_leaving()
    OWNER TO dev;

CREATE FUNCTION public.update_physical_status_log()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare
	old_physical_status varchar(20);
	new_physical_status varchar(20);
	person_pk bigint;
begin
	old_physical_status = 'Unknown';
	person_pk = NEW.id;
	if TG_OP = 'UPDATE' then
	select fn_calculate_physical_status(OLD.arrival_date,OLD.leaving_date,OLD.left_but_no_leaving_date_given) into old_physical_status;	
	end if;
	
	select fn_calculate_physical_status(NEW.arrival_date,NEW.leaving_date,NEW.left_but_no_leaving_date_given) into new_physical_status;

	if (old_physical_status <> new_physical_status) then
		update _physical_status_v3 set status_id = new_physical_status where person_id = person_pk;
	        if FOUND = 'f' then
	                insert into _physical_status_v3 ( person_id, status_id) values (person_pk, new_physical_status);
	        end if; 
		insert into _physical_status_changelog ( person_id,old_status_id, new_status_id, stamp, username ) values (person_pk,old_physical_status, new_physical_status,current_timestamp,current_user);
	end if;
	return null;
end;
$BODY$;

ALTER FUNCTION public.update_physical_status_log()
    OWNER TO dev;

COMMENT ON FUNCTION public.update_physical_status_log()
    IS 'To put entries into _physical_status_changelog to drive Mifare updates and others ';

-- drop new cron function

DROP FUNCTION public.cron_physical_status_updates();

-- drop new trigger

DROP TRIGGER physical_status_updates ON public.person;

-- reinstate old triggers

CREATE TRIGGER physical_status_trig
    AFTER INSERT OR UPDATE 
    ON public.person
    FOR EACH ROW
    EXECUTE PROCEDURE public.update_physical_status_log();

CREATE TRIGGER unset_review_due_date
    BEFORE UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given
    ON public.person
    FOR EACH ROW
    EXECUTE PROCEDURE public.unset_review_due_date_on_leaving();

-- drop new trigger function

DROP FUNCTION public.physical_status_updates();
