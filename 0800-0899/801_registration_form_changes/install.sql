DROP VIEW hotwire3."10_View/People/Registration";
DROP FUNCTION registration.people_registration_upd();
DROP VIEW registration.completed_form_summary;

ALTER TABLE registration.form DROP COLUMN mifare_out_hours_access_required;
ALTER TABLE registration.form DROP COLUMN mifare_aware_of_out_of_hours_rules;
ALTER TABLE registration.form DROP COLUMN mifare_technician_signed;
ALTER TABLE registration.form DROP COLUMN mifare_host_signed;
ALTER TABLE registration.form DROP COLUMN technician_id;
ALTER TABLE registration.form ADD COLUMN mifare_access_level_id BIGINT REFERENCES public.mifare_access_level(mifare_access_level_id);
ALTER TABLE registration.form ADD COLUMN mifare_access_processed BOOLEAN NOT NULL DEFAULT 'f';

CREATE VIEW registration.completed_form_summary AS
SELECT
    form.uuid,
    form.first_names,
    form.surname,
    form.known_as,
    title.hid as title,
    form.date_of_birth,
    post_category.hid as post_category,
    nationality.nationality,
    form.job_title,
    rooms_by_uuid.rooms,
    phones.phones,
    college.hid as college,
    form.home_address,
    form.home_phone_number,
    form.mobile_number,
    form.email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution,
    supervisor.hid as supervisor,
    form.deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    form.hide_from_website,
    form.previously_registered
FROM registration.form
LEFT JOIN registration.title_hid AS title on title.id = form.title_id
LEFT JOIN registration.college_hid AS college ON college.id = form.college_id
LEFT JOIN registration.supervisor_hid AS supervisor ON supervisor.id = form.department_host_id
JOIN registration.post_category_hid AS post_category ON post_category.id = form.post_category_id
JOIN (
    select uuid,string_agg(hid,', '::varchar) as rooms from registration.form, unnest(dept_room_id) as id join registration.room_hid using(id) group by uuid
) rooms_by_uuid USING (uuid)
JOIN (
    select uuid,string_agg(hid,', '::varchar) as phones from registration.form, unnest(dept_telephone_id) as id join registration.phone_hid using(id) group by uuid
) phones USING (uuid)
JOIN (
    select uuid,string_agg(hid,', '::varchar) as nationality from registration.form, unnest(nationality_id) as id join registration.nationality_hid using(id) group by uuid
) nationality USING (uuid)
WHERE submitted = 't'
;

ALTER TABLE registration.completed_form_summary OWNER TO dev;
GRANT SELECT ON registration.completed_form_summary TO starters_registration;
COMMENT ON VIEW registration.completed_form_summary IS 'Used by the registation webapp to generate nicelly formatted email summaries of each form with foreign keys resolved';

CREATE VIEW hotwire3."10_View/People/Registration" AS
WITH reg AS (
SELECT
    form_id AS id,
    form.first_names::varchar,
    form.surname::varchar,
    form.known_as::varchar,
    form.title_id,
    form.date_of_birth,
    form.post_category_id,
    form.nationality_id,
    form.job_title::varchar,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::varchar,
    form.mobile_number::varchar,
    form.email::varchar,
    form.crsid::varchar,
    form.start_date,
    form.intended_end_date,
    form.home_institution::varchar,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::varchar,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    form.hide_from_website,
    form.previously_registered,
    CASE
        WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
        ELSE registration.match_existing_person(surname::varchar,first_names::varchar,form.crsid,form.date_of_birth)
    END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    'f'::boolean AS import_this_record,
    form._imported_on AS ro_imported_on,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches','id','10_View/People/Personnel_Data_Entry','_matched_person_id',null) AS matched_person_details,
    CASE
        WHEN form._imported_on IS NOT NULL THEN 'green'
        ELSE NULL
    END AS _cssclass
FROM registration.form
WHERE submitted = 't'
) SELECT *
FROM reg
ORDER BY ro_imported_on DESC NULLS FIRST
;
ALTER TABLE hotwire3."10_View/People/Registration" OWNER TO dev;
GRANT SELECT,UPDATE ON registration.completed_form_summary TO hr;

CREATE FUNCTION registration.people_registration_upd() RETURNS TRIGGER AS
$BODY$
DECLARE
    new_person_id BIGINT;
    emergency_contact VARCHAR(500);
    existing_pg_id BIGINT;
BEGIN
-- first and foremost update the data
     UPDATE registration.form SET
         first_names = NEW.first_names,
         surname = NEW.surname,
         known_as = NEW.known_as,
         title_id = NEW.title_id,
         date_of_birth = NEW.date_of_birth,
         post_category_id = NEW.post_category_id,
         nationality_id = NEW.nationality_id,
         job_title = NEW.job_title,
         dept_room_id = NEW.room_id,
         dept_telephone_id = NEW.dept_telephone_number_id,
         college_id = NEW.cambridge_college_id,
         home_address = NEW.home_address,
         home_phone_number = NEW.home_phone_number,
         mobile_number = NEW.mobile_number,
         email = NEW.email,
         crsid = NEW.crsid,
         start_date = NEW.start_date,
         intended_end_date = NEW.intended_end_date,
         home_institution = NEW.home_institution,
         mifare_areas = NEW.group_areas,
         mifare_access_level_id = NEW.mifare_access_level_id,
         department_host_id = NEW.supervisor_id,
         deposit_receipt = NEW.deposit_receipt,
         emergency_contact_name_1 = NEW.emergency_contact_name_1,
         emergency_contact_number_1 = NEW.emergency_contact_number_1,
         emergency_contact_name_2 = NEW.emergency_contact_name_2,
         emergency_contact_number_2 = NEW.emergency_contact_number_2,
         previously_registered = NEW.previously_registered,
         hide_from_website = NEW.hide_from_website,
         _match_to_person_id = NEW.match_to_person_id
     WHERE form_id = OLD.id;

     IF NEW.import_this_record = 't'::boolean THEN
         -- Build a composite field
         emergency_contact := NEW.emergency_contact_name_1
                     || ' '
                     || NEW.emergency_contact_number_1
                     || (   COALESCE(E'\n'||NEW.emergency_contact_name_2||' ','')
                            || COALESCE(NEW.emergency_contact_number_2,'')
                        );
         -- Is there a match with an existing person?
         -- If match manually set, use that. 
         IF NEW.match_to_person_id IS NOT NULL THEN
             new_person_id = NEW.match_to_person_id;
         -- Or if the system has worked it out for itself use that
         -- Using OLD here because going for NEW would seem potentially fatal as an edit could change the automatic match
         ELSIF OLD.previously_registered = 't' AND OLD.ro_automatically_matched_person_id IS NOT NULL THEN
             new_person_id = OLD.ro_automatically_matched_person_id;
         END IF;
         -- If we found a match, update that person
         -- First the non-nullable fields; these can be updated unconditionally
         IF new_person_id IS NOT NULL THEN
             UPDATE person SET
                 surname = NEW.surname,
                 title_id = NEW.title_id,
                 date_of_birth = NEW.date_of_birth,
                 cambridge_address = NEW.home_address,
                 cambridge_phone_number = NEW.home_phone_number,
                 mobile_number = NEW.mobile_number,
                 email_address = NEW.email,
                 crsid = NEW.crsid,
                 do_not_show_on_website = NEW.hide_from_website
             WHERE person.id = new_person_id;
             -- These are nullable on the registration form, so if we had data in them
             -- we don't want to overwrite it with blanks
             IF NEW.first_names IS NOT NULL THEN
                 UPDATE person SET first_names = NEW.first_names WHERE person.id = new_person_id;
             END IF;
             IF NEW.known_as IS NOT NULL THEN
                 UPDATE person SET known_as = NEW.known_as WHERE person.id = new_person_id;
             END IF;
         ELSE -- create new
             INSERT INTO person (
                 first_names,
                 surname,
                 known_as,
                 title_id,
                 date_of_birth,
                 cambridge_college_id,
                 cambridge_address,
                 cambridge_phone_number,
                 mobile_number,
                 email_address,
                 crsid,
                 emergency_contact,
                 do_not_show_on_website
             ) VALUES (
                 NEW.first_names,
                 NEW.surname,
                 NEW.known_as,
                 NEW.title_id,
                 NEW.date_of_birth,
                 NEW.cambridge_college_id,
                 NEW.home_address,
                 NEW.home_phone_number,
                 NEW.mobile_number,
                 NEW.email,
                 NEW.crsid,
                 emergency_contact,
                 NEW.hide_from_website
             ) RETURNING id INTO new_person_id;
         END IF;
         -- Do mm-updates here on nationality, room, phone
         PERFORM public.fn_mm_array_update(NEW.room_id,'mm_person_room'::varchar,'person_id'::varchar,'room_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.dept_telephone_number_id,'mm_person_dept_telephone_number'::varchar,'person_id'::varchar,'dept_telephone_number_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.nationality_id,'mm_person_nationality'::varchar,'person_id'::varchar,'nationality_id'::varchar,new_person_id);
         -- create the role
         CASE rtrim(NEW.post_category_id,'-1234567890')
             WHEN 'sc' THEN
                 INSERT INTO post_history (
                     person_id,
                     start_date,
                     supervisor_id,
                     job_title,
                     intended_end_date,
                     staff_category_id
                 ) VALUES (
                     new_person_id,
                     NEW.start_date,
                     NEW.supervisor_id,
                     NEW.job_title,
                     NEW.intended_end_date,
                     ltrim(NEW.post_category_id,'sc-')::bigint
                 );
             WHEN 'v' THEN
                 INSERT INTO visitorship (
                     person_id,
                     host_person_id,
                     start_date,
                     intended_end_date,
                     home_institution,
                     visitor_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'v-')::bigint
                 );
             WHEN 'pg' THEN
               -- Need to check for an existing postgraduate_studentship of the same type here and not import if there is one
               -- because these sometimes get created in bulk at the start of the academic year
                 SELECT id INTO existing_pg_id
                 FROM postgraduate_studentship
                 WHERE person_id = new_person_id AND postgraduate_studentship_type_id = ltrim(NEW.post_category_id,'pg-')::bigint;
                 IF existing_pg_id IS NULL THEN
                     INSERT INTO postgraduate_studentship (
                         person_id,
                         first_supervisor_id,
                         start_date,
                         postgraduate_studentship_type_id
                     ) VALUES (
                         new_person_id,
                         NEW.supervisor_id,
                         NEW.start_date,
                         ltrim(NEW.post_category_id,'pg-')::bigint
                     );
                 END IF;
             WHEN 'e' THEN
                 INSERT INTO erasmus_socrates_studentship (
                     person_id,
                     supervisor_id,
                     start_date,
                     intended_end_date,
                     university,
                     erasmus_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'e-')::bigint
                 );
         END CASE;
         -- delete the form
         -- DELETE FROM registration.form WHERE form_id = OLD.id;
         RAISE INFO 'Importing a person';
         -- Record this one was imported and who it matches to
         UPDATE registration.form SET _imported_on = current_date, _match_to_person_id = new_person_id WHERE form_id = OLD.id; 
     END IF;
     RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;
ALTER FUNCTION registration.people_registration_upd() OWNER TO dev;

CREATE TRIGGER registration_form_update INSTEAD OF UPDATE ON hotwire3."10_View/People/Registration" FOR EACH ROW EXECUTE PROCEDURE registration.people_registration_upd();
