DROP VIEW leave_recording.role_entitlement_adjustment_view;
CREATE OR REPLACE VIEW leave_recording.role_entitlement_adjustment_view AS
 SELECT
    role_entitlement_adjustment.*,
    person.person_crsid AS actor_crsid,
    person.person_email AS actor_email,
    person.person_name AS actor_name,
    person.person_friendly_name AS actor_friendly_name
  FROM
    leave_recording.role_entitlement_adjustment
  JOIN leave_recording.person
  ON person.person_id = role_entitlement_adjustment.actor
  ;

ALTER TABLE leave_recording.role_entitlement_adjustment_view
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_entitlement_adjustment_view TO dev;
GRANT SELECT ON TABLE leave_recording.role_entitlement_adjustment_view TO www_leave_reporting;
