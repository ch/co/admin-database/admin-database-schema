REVOKE SELECT, DELETE ON leave_recording.standing_leave FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.leave_audit FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.role_entitlement_adjustment FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.role_enrol_audit FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.role_enrol FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.pending_leave_request FROM gdpr_purge;

REVOKE SELECT, DELETE ON leave_recording.mm_role_shared_calendar_group FROM gdpr_purge;

