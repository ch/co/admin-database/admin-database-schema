GRANT SELECT, DELETE ON leave_recording.standing_leave TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.leave_audit TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.role_entitlement_adjustment TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.role_enrol_audit TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.role_enrol TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.pending_leave_request TO gdpr_purge;

GRANT SELECT, DELETE ON leave_recording.mm_role_shared_calendar_group TO gdpr_purge;

