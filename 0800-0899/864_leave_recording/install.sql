DROP VIEW hotwire3. "10_View/Roles/Post_History";

DROP VIEW leave_recording.role;

CREATE TABLE leave_recording.shared_calendar_group
(
  id serial NOT NULL PRIMARY KEY,
  group_hid varchar(80) NOT NULL,
  added_by BIGINT REFERENCES public.person(id) NOT NULL,
  added_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  invalidated_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL, -- not null only when it should be invalid; cannot schedule a future invalidation!
  invalidated_by BIGINT REFERENCES public.person(id) NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.shared_calendar_group
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.shared_calendar_group TO dev;
GRANT SELECT, INSERT, UPDATE(group_hid) ON TABLE leave_recording.shared_calendar_group TO www_leave_reporting;

CREATE TABLE leave_recording.mm_role_shared_calendar_group
(
  id serial NOT NULL PRIMARY KEY,
  post_id BIGINT REFERENCES public.post_history(id) NOT NULL,
  shared_calendar_group_id INT REFERENCES leave_recording.shared_calendar_group(id) NOT NULL,
  is_group_manager BOOLEAN NOT NULL DEFAULT false,
  post_visible_to_others BOOLEAN NOT NULL DEFAULT true,
  can_see_others_posts BOOLEAN NOT NULL DEFAULT true,
  valid_from DATE NOT NULL DEFAULT CURRENT_DATE,
  added_by BIGINT REFERENCES public.person(id) NOT NULL,
  added_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  invalidated_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL, -- not null only when it should be invalid; cannot schedule a future invalidation!
  invalidated_by BIGINT REFERENCES public.person(id) NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.mm_role_shared_calendar_group
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.mm_role_shared_calendar_group TO dev;
GRANT SELECT, INSERT, UPDATE(invalidated_when, invalidated_by) ON TABLE leave_recording.mm_role_shared_calendar_group TO www_leave_reporting;

CREATE INDEX mm_role_shared_calendar_group_post_id_index ON leave_recording.mm_role_shared_calendar_group USING btree (post_id);
CREATE INDEX mm_role_shared_calendar_group_scgid_index ON leave_recording.mm_role_shared_calendar_group USING btree (shared_calendar_group_id);

CREATE TABLE leave_recording.mm_research_group_shared_calendar_group
(
  shared_calendar_group_id INT REFERENCES leave_recording.shared_calendar_group(id) NOT NULL,
  research_group_id BIGINT REFERENCES public.research_group(id) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.mm_research_group_shared_calendar_group
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.mm_research_group_shared_calendar_group TO dev;
GRANT SELECT, INSERT, DELETE ON TABLE leave_recording.mm_research_group_shared_calendar_group TO www_leave_reporting;

ALTER TABLE ONLY leave_recording.mm_research_group_shared_calendar_group
    ADD CONSTRAINT mm_research_group_shared_calendar_group_pk PRIMARY KEY (shared_calendar_group_id, research_group_id);

CREATE INDEX mm_research_group_shared_calendar_group_scgid_index ON leave_recording.mm_research_group_shared_calendar_group USING btree (shared_calendar_group_id);
CREATE INDEX mm_research_group_shared_calendar_group_rgid_index ON leave_recording.mm_research_group_shared_calendar_group USING btree (research_group_id);


ALTER TABLE post_history ADD COLUMN usual_days_worked CHAR(7) NULL CONSTRAINT usual_days_worked_bitfield CHECK (usual_days_worked ~ '^[M-][T-][W-][T-][F-][S-][S-]$');
-- use a bitfield because (inter alia) hotwire doesn't support nullable booleans.
COMMENT ON COLUMN post_history.usual_days_worked IS 'bitfield for days worked: Mon-Sun';
ALTER TABLE post_history ADD COLUMN leave_recording_disable TEXT NULL;
ALTER TABLE post_history ADD COLUMN leave_recording_manager BIGINT NULL REFERENCES public.post_history(id) DEFAULT NULL;
ALTER TABLE post_history ADD COLUMN leave_recording_needs_approval BOOLEAN DEFAULT TRUE;
ALTER TABLE post_history ALTER COLUMN leave_recording_needs_approval SET DEFAULT NULL;
ALTER TABLE post_history DROP COLUMN weekends_are_standard_working_days;

ALTER TABLE leave_recording.role_enrol ADD COLUMN person_notified TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL;
ALTER TABLE leave_recording.role_enrol ADD COLUMN line_manager_notified TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL;
ALTER TABLE leave_recording.role_enrol ADD COLUMN allow_use_from DATE NULL DEFAULT NULL; -- immediately usable if NULL

ALTER TABLE leave_recording.pending_leave_request ADD COLUMN post_holder_can_cancel_if_future BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE leave_recording.standing_leave ADD COLUMN post_holder_can_cancel_if_future BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE leave_recording.leave_audit ADD COLUMN post_holder_can_cancel_if_future BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE leave_recording.leave_audit ADD COLUMN notified BIGINT NULL REFERENCES public.person(id) DEFAULT NULL;

ALTER TABLE leave_recording.pending_leave_request ADD COLUMN original_confirmed_leave_type INT NULL REFERENCES leave_recording.leave_type(id);

ALTER TABLE leave_recording.pending_leave_request ADD COLUMN system_notes TEXT NULL;
ALTER TABLE leave_recording.standing_leave ADD COLUMN system_notes TEXT NULL;
ALTER TABLE leave_recording.role_enrol_audit ADD COLUMN notes TEXT NULL;

ALTER TABLE leave_recording.role_entitlement_adjustment ADD COLUMN deleted_when TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL;
ALTER TABLE leave_recording.role_entitlement_adjustment ADD COLUMN deleted_by_actor BIGINT NULL REFERENCES public.person(id);
ALTER TABLE leave_recording.role_entitlement_adjustment ADD COLUMN deleted_notes TEXT NULL;


GRANT UPDATE(deleted_when, deleted_by_actor, deleted_notes) ON TABLE leave_recording.role_entitlement_adjustment TO www_leave_reporting;
GRANT DELETE ON TABLE leave_recording.role_enrol TO www_leave_reporting;

CREATE OR REPLACE VIEW leave_recording.role AS
  SELECT
    CASE
      WHEN (staff_category.category = 'Assistant staff'::text) THEN 'leave_recording\assistant_staff_Role'::text
      WHEN (staff_category.category = 'Research Assistant'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Research Fellow'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'PDRA'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Senior PDRA'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Principal Research Associate'::text) THEN 'leave_recording\research_Role'::text
      WHEN (staff_category.category = 'Academic-related staff'::text) THEN 'leave_recording\academic_related_Role'::text
      WHEN (staff_category.category = 'Teaching Fellow'::text) THEN 'leave_recording\teaching_Role'::text
      WHEN (staff_category.category = 'Unknown'::text) THEN 'leave_recording\unknown_Role'::text
      WHEN (staff_category.category = 'Academic staff'::text) THEN 'leave_recording\academic_staff_Role'::text
      ELSE 'leave_recording\database\Role'::text
    END AS role_class_name,
    post_history.id as post_id,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    post_history.start_date,
    post_history.end_date,
    post_history.intended_end_date,
            ( SELECT min(closest_date.end_date) AS min
                   FROM ( SELECT post_history_1.id,
                            post_history_1.end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.intended_end_date AS end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.funding_end_date AS end_date
                           FROM public.post_history post_history_1
                          GROUP BY post_history_1.id) closest_date
                  WHERE (closest_date.id = post_history.id)) AS estimated_leaving_date,
    CASE
        WHEN person.continuous_employment_start_date <= post_history.start_date THEN person.continuous_employment_start_date
        ELSE NULL -- we don't record historical values
    END AS continuous_employment_start_date_as_at_start_of_role,
    post_history.hours_worked,
    post_history.job_title,
    --post_history.staff_category_id,
    --('sc-'::text || (post_history.staff_category_id)::text) AS post_category_id,
    staff_category.category AS post_category,
    post_history.percentage_of_fulltime_hours,
    post_history.average_percentage_of_days_worked_per_week,
    post_history.usual_days_worked,
    post_history.works_flexitime,
    post_history.standard_flexileave_rules_apply,
    post_history.departmental_closures_apply,
    post_history.leave_recording_disable,
    post_history.leave_recording_manager,
    post_history.leave_recording_needs_approval,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN (post_history.funding_end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name,
    leave_recording_manager.leave_recording_manager_id,
    leave_recording_manager.leave_recording_manager_crsid,
    leave_recording_manager.leave_recording_manager_email,
    leave_recording_manager.leave_recording_manager_name
 FROM public.post_history
 LEFT JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = post_history.supervisor_id
 LEFT JOIN (SELECT
    post_history.id AS leave_recording_manager_post_id,
    post_history.person_id AS leave_recording_manager_id,
    post_history.job_title AS leave_recording_manager_job_title,
    post_history.end_date AS leave_recording_manager_end_date
  FROM
    post_history) leave_recording_manager_role ON leave_recording_manager_role.leave_recording_manager_post_id = post_history.leave_recording_manager
 LEFT JOIN (SELECT
    person.id AS leave_recording_manager_id,
    person.crsid AS leave_recording_manager_crsid,
    person.email_address AS leave_recording_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS leave_recording_manager_name
  FROM
    person) leave_recording_manager ON leave_recording_manager.leave_recording_manager_id = leave_recording_manager_role.leave_recording_manager_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.continuous_employment_start_date
  FROM
    person) person ON person.person_id = post_history.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leave_recording.role
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role TO dev;
GRANT SELECT ON TABLE leave_recording.role TO www_leave_reporting;


CREATE OR REPLACE VIEW hotwire3. "10_View/Roles/Post_History" AS
SELECT
    a.id,
    a.staff_category_id,
    a.person_id,
    a.ro_role_status_id,
    a.supervisor_id,
    a.mentor_id,
    a.external_mentor,
    a.start_date_for_continuous_employment_purposes,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.funding_end_date,
    a.force_role_status_to_past,
    a.staff_review_due,
    a.chem,
    a.paid_through_payroll,
    a.hours_worked,
    a.percentage_of_fulltime_hours,
    a.average_percentage_of_days_worked_per_week,
    a.usual_days_worked,
    a.works_flexitime,
    a.standard_flexileave_rules_apply,
    a.departmental_closures_apply,
    a.disallow_leave_recording,
    a.leave_recording_manager,
    a.leave_recording_needs_approval,
    a.research_grant_number,
    a.sponsor,
    a.probation_period,
    a.date_of_first_probation_meeting,
    a.date_of_second_probation_meeting,
    a.date_of_third_probation_meeting,
    a.date_of_fourth_probation_meeting,
    a.first_probation_meeting_comments,
    a.second_probation_meeting_comments,
    a.third_probation_meeting_comments,
    a.fourth_probation_meeting_comments,
    a.probation_outcome,
    a.date_probation_letters_sent,
    a.job_title,
    a._person
FROM (
    SELECT
        post_history.id,
        post_history.staff_category_id,
        post_history.person_id,
        _all_roles.status AS ro_role_status_id,
        post_history.supervisor_id,
        post_history.mentor_id,
        post_history.external_mentor,
        person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes,
        post_history.start_date,
        post_history.intended_end_date,
        post_history.end_date,
        post_history.funding_end_date,
        person.next_review_due AS staff_review_due,
        post_history.chem,
        post_history.paid_by_university AS paid_through_payroll,
        post_history.hours_worked,
        post_history.percentage_of_fulltime_hours,
        post_history.average_percentage_of_days_worked_per_week,
        post_history.usual_days_worked,
        CASE
            WHEN post_history.staff_category_id = 3 THEN post_history.works_flexitime
            ELSE NULL
        END AS works_flexitime,
        CASE
            WHEN post_history.staff_category_id = 3 AND post_history.works_flexitime THEN post_history.standard_flexileave_rules_apply
            ELSE NULL
        END AS standard_flexileave_rules_apply,
        post_history.departmental_closures_apply,
        post_history.leave_recording_disable AS disallow_leave_recording,
        post_history.leave_recording_manager,
        post_history.leave_recording_needs_approval,
        post_history.probation_period::text AS probation_period,
        post_history.date_of_first_probation_meeting,
        post_history.date_of_second_probation_meeting,
        post_history.date_of_third_probation_meeting,
        post_history.date_of_fourth_probation_meeting,
        post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments,
        post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments,
        post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments,
        post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments,
        post_history.probation_outcome::text AS probation_outcome,
        post_history.date_probation_letters_sent,
        post_history.research_grant_number::text AS research_grant_number,
        post_history.filemaker_funding::text AS sponsor,
        post_history.job_title,
        post_history.force_role_status_to_past,
        person_hid.person_hid AS _person
    FROM
        post_history
    LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id
        AND _all_roles.role_tablename = 'post_history'::text
    JOIN person ON post_history.person_id = person.id
    JOIN person_hid ON post_history.person_id = person_hid.person_id) a
ORDER BY
    a._person,
    a.start_date DESC;

ALTER TABLE hotwire3. "10_View/Roles/Post_History" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Roles/Post_History" TO dev;

GRANT SELECT ON TABLE hotwire3. "10_View/Roles/Post_History" TO mgmt_ro;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Roles/Post_History" TO hr;

-- Rule: hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_del AS ON DELETE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    DELETE
FROM
    post_history
WHERE
    post_history.id = old.id;

-- Rule: hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_update_rules AS ON UPDATE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes,
        next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

UPDATE
    post_history
SET
    staff_category_id = new.staff_category_id,
    supervisor_id = new.supervisor_id,
    mentor_id = new.mentor_id,
    external_mentor = new.external_mentor,
    start_date = new.start_date,
    intended_end_date = new.intended_end_date,
    end_date = new.end_date,
    funding_end_date = new.funding_end_date,
    chem = new.chem,
    paid_by_university = new.paid_through_payroll,
    hours_worked = new.hours_worked,
    percentage_of_fulltime_hours = new.percentage_of_fulltime_hours,
    average_percentage_of_days_worked_per_week = new.average_percentage_of_days_worked_per_week,
    usual_days_worked = new.usual_days_worked,
    works_flexitime = leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    standard_flexileave_rules_apply = CASE
        WHEN new.staff_category_id = 3 AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    departmental_closures_apply = CASE
        WHEN new.staff_category_id = 3 THEN new.departmental_closures_apply
        ELSE NULL
    END,
    leave_recording_disable = new.disallow_leave_recording,
    leave_recording_manager = new.leave_recording_manager,
    leave_recording_needs_approval = new.leave_recording_needs_approval,
    probation_period = new.probation_period::character varying(80),
    date_of_first_probation_meeting = new.date_of_first_probation_meeting,
    date_of_second_probation_meeting = new.date_of_second_probation_meeting,
    date_of_third_probation_meeting = new.date_of_third_probation_meeting,
    date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting,
    first_probation_meeting_comments = new.first_probation_meeting_comments::character varying(500),
    second_probation_meeting_comments = new.second_probation_meeting_comments::character varying(500),
    third_probation_meeting_comments = new.third_probation_meeting_comments::character varying(500),
    fourth_probation_meeting_comments = new.fourth_probation_meeting_comments::character varying(500),
    probation_outcome = new.probation_outcome::character varying(200),
    date_probation_letters_sent = new.date_probation_letters_sent,
    research_grant_number = new.research_grant_number::character varying(500),
    filemaker_funding = new.sponsor::character varying(500),
    job_title = new.job_title,
    force_role_status_to_past = new.force_role_status_to_past
WHERE
    post_history.id = old.id;

);

-- Rule: post_history_ins ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE post_history_ins ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE post_history_ins AS ON INSERT TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes, next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

INSERT INTO post_history (staff_category_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, average_percentage_of_days_worked_per_week, usual_days_worked, works_flexitime, standard_flexileave_rules_apply, departmental_closures_apply, leave_recording_disable, leave_recording_manager, leave_recording_needs_approval, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, job_title, force_role_status_to_past)
    VALUES (new.staff_category_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_through_payroll, new.hours_worked, new.percentage_of_fulltime_hours, new.average_percentage_of_days_worked_per_week,
    new.usual_days_worked,
    leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    CASE
        WHEN (new.staff_category_id = 3) AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    new.departmental_closures_apply, new.disallow_leave_recording, new.leave_recording_manager, new.leave_recording_needs_approval, new.probation_period::character varying(80), new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments::character varying(500), new.second_probation_meeting_comments::character varying(500), new.third_probation_meeting_comments::character varying(500), new.fourth_probation_meeting_comments::character varying(500), new.probation_outcome::character varying(200), new.date_probation_letters_sent, new.research_grant_number::character varying(500), new.sponsor::character varying(500), new.job_title, new.force_role_status_to_past)
RETURNING
    post_history.id, post_history.staff_category_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.force_role_status_to_past, NULL::date AS date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.average_percentage_of_days_worked_per_week, post_history.usual_days_worked, post_history.works_flexitime, post_history.standard_flexileave_rules_apply, post_history.departmental_closures_apply, post_history.leave_recording_disable, post_history.leave_recording_manager, post_history.leave_recording_needs_approval, post_history.research_grant_number::text AS research_grant_number, post_history.filemaker_funding::text AS filemaker_funding, post_history.probation_period::text AS probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments, post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments, post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments, post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments, post_history.probation_outcome::text AS probation_outcome, post_history.date_probation_letters_sent, post_history.job_title, NULL::text AS text;

);
