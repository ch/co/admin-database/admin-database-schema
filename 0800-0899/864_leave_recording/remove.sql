DROP VIEW hotwire3. "10_View/Roles/Post_History";

DROP VIEW leave_recording.role;

DROP TABLE leave_recording.mm_research_group_shared_calendar_group;
DROP TABLE leave_recording.mm_role_shared_calendar_group;
DROP TABLE leave_recording.shared_calendar_group;

ALTER TABLE post_history DROP COLUMN usual_days_worked;
ALTER TABLE post_history DROP COLUMN leave_recording_disable;
ALTER TABLE post_history DROP COLUMN leave_recording_manager;
ALTER TABLE post_history DROP COLUMN leave_recording_needs_approval;
ALTER TABLE post_history ADD COLUMN weekends_are_standard_working_days BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE leave_recording.role_enrol DROP COLUMN person_notified;
ALTER TABLE leave_recording.role_enrol DROP COLUMN line_manager_notified;
ALTER TABLE leave_recording.role_enrol DROP COLUMN allow_use_from;
ALTER TABLE leave_recording.pending_leave_request DROP COLUMN post_holder_can_cancel_if_future;
ALTER TABLE leave_recording.standing_leave DROP COLUMN post_holder_can_cancel_if_future;
ALTER TABLE leave_recording.leave_audit DROP COLUMN post_holder_can_cancel_if_future;
ALTER TABLE leave_recording.leave_audit DROP COLUMN notified;
ALTER TABLE leave_recording.pending_leave_request DROP COLUMN original_confirmed_leave_type;
ALTER TABLE leave_recording.pending_leave_request DROP COLUMN system_notes;
ALTER TABLE leave_recording.standing_leave DROP COLUMN system_notes;
ALTER TABLE leave_recording.role_enrol_audit DROP COLUMN notes;
ALTER TABLE leave_recording.role_entitlement_adjustment DROP COLUMN deleted_when;
ALTER TABLE leave_recording.role_entitlement_adjustment DROP COLUMN deleted_by_actor;
ALTER TABLE leave_recording.role_entitlement_adjustment DROP COLUMN deleted_notes;

REVOKE DELETE ON TABLE leave_recording.role_enrol FROM www_leave_reporting;


CREATE OR REPLACE VIEW hotwire3. "10_View/Roles/Post_History" AS
SELECT
    a.id,
    a.staff_category_id,
    a.person_id,
    a.ro_role_status_id,
    a.supervisor_id,
    a.mentor_id,
    a.external_mentor,
    a.start_date_for_continuous_employment_purposes,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.funding_end_date,
    a.force_role_status_to_past,
    a.staff_review_due,
    a.chem,
    a.paid_through_payroll,
    a.hours_worked,
    a.percentage_of_fulltime_hours,
    a.average_percentage_of_days_worked_per_week,
    a.weekends_are_standard_working_days,
    a.works_flexitime,
    a.standard_flexileave_rules_apply,
    a.departmental_closures_apply,
    a.research_grant_number,
    a.sponsor,
    a.probation_period,
    a.date_of_first_probation_meeting,
    a.date_of_second_probation_meeting,
    a.date_of_third_probation_meeting,
    a.date_of_fourth_probation_meeting,
    a.first_probation_meeting_comments,
    a.second_probation_meeting_comments,
    a.third_probation_meeting_comments,
    a.fourth_probation_meeting_comments,
    a.probation_outcome,
    a.date_probation_letters_sent,
    a.job_title,
    a._person
FROM (
    SELECT
        post_history.id,
        post_history.staff_category_id,
        post_history.person_id,
        _all_roles.status AS ro_role_status_id,
        post_history.supervisor_id,
        post_history.mentor_id,
        post_history.external_mentor,
        person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes,
        post_history.start_date,
        post_history.intended_end_date,
        post_history.end_date,
        post_history.funding_end_date,
        person.next_review_due AS staff_review_due,
        post_history.chem,
        post_history.paid_by_university AS paid_through_payroll,
        post_history.hours_worked,
        post_history.percentage_of_fulltime_hours,
        post_history.average_percentage_of_days_worked_per_week,
        post_history.weekends_are_standard_working_days,
        CASE
            WHEN post_history.staff_category_id = 3 THEN post_history.works_flexitime
            ELSE NULL
        END AS works_flexitime,
        CASE
            WHEN post_history.staff_category_id = 3 AND post_history.works_flexitime THEN post_history.standard_flexileave_rules_apply
            ELSE NULL
        END AS standard_flexileave_rules_apply,
        post_history.departmental_closures_apply,
        post_history.probation_period::text AS probation_period,
        post_history.date_of_first_probation_meeting,
        post_history.date_of_second_probation_meeting,
        post_history.date_of_third_probation_meeting,
        post_history.date_of_fourth_probation_meeting,
        post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments,
        post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments,
        post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments,
        post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments,
        post_history.probation_outcome::text AS probation_outcome,
        post_history.date_probation_letters_sent,
        post_history.research_grant_number::text AS research_grant_number,
        post_history.filemaker_funding::text AS sponsor,
        post_history.job_title,
        post_history.force_role_status_to_past,
        person_hid.person_hid AS _person
    FROM
        post_history
    LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id
        AND _all_roles.role_tablename = 'post_history'::text
    JOIN person ON post_history.person_id = person.id
    JOIN person_hid ON post_history.person_id = person_hid.person_id) a
ORDER BY
    a._person,
    a.start_date DESC;

ALTER TABLE hotwire3. "10_View/Roles/Post_History" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Roles/Post_History" TO dev;

GRANT SELECT ON TABLE hotwire3. "10_View/Roles/Post_History" TO mgmt_ro;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Roles/Post_History" TO hr;

-- Rule: hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_del AS ON DELETE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    DELETE
FROM
    post_history
WHERE
    post_history.id = old.id;

CREATE OR REPLACE FUNCTION leave_recording.effective_new_works_flexitime(staff_category_id BIGINT, works_flexitime BOOLEAN) RETURNS BOOLEAN AS $$
BEGIN
    RETURN CASE
        WHEN (staff_category_id = 3) THEN works_flexitime
        ELSE NULL
    END;
END;
$$ LANGUAGE plpgsql;

-- Rule: hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_update_rules AS ON UPDATE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes,
        next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

UPDATE
    post_history
SET
    staff_category_id = new.staff_category_id,
    supervisor_id = new.supervisor_id,
    mentor_id = new.mentor_id,
    external_mentor = new.external_mentor,
    start_date = new.start_date,
    intended_end_date = new.intended_end_date,
    end_date = new.end_date,
    funding_end_date = new.funding_end_date,
    chem = new.chem,
    paid_by_university = new.paid_through_payroll,
    hours_worked = new.hours_worked,
    percentage_of_fulltime_hours = new.percentage_of_fulltime_hours,
    average_percentage_of_days_worked_per_week = new.average_percentage_of_days_worked_per_week,
    weekends_are_standard_working_days = new.weekends_are_standard_working_days,
    works_flexitime = leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    standard_flexileave_rules_apply = CASE
        WHEN new.staff_category_id = 3 AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    departmental_closures_apply = CASE
        WHEN new.staff_category_id = 3 THEN new.departmental_closures_apply
        ELSE NULL
    END,
    probation_period = new.probation_period::character varying(80),
    date_of_first_probation_meeting = new.date_of_first_probation_meeting,
    date_of_second_probation_meeting = new.date_of_second_probation_meeting,
    date_of_third_probation_meeting = new.date_of_third_probation_meeting,
    date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting,
    first_probation_meeting_comments = new.first_probation_meeting_comments::character varying(500),
    second_probation_meeting_comments = new.second_probation_meeting_comments::character varying(500),
    third_probation_meeting_comments = new.third_probation_meeting_comments::character varying(500),
    fourth_probation_meeting_comments = new.fourth_probation_meeting_comments::character varying(500),
    probation_outcome = new.probation_outcome::character varying(200),
    date_probation_letters_sent = new.date_probation_letters_sent,
    research_grant_number = new.research_grant_number::character varying(500),
    filemaker_funding = new.sponsor::character varying(500),
    job_title = new.job_title,
    force_role_status_to_past = new.force_role_status_to_past
WHERE
    post_history.id = old.id;

);

-- Rule: post_history_ins ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE post_history_ins ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE post_history_ins AS ON INSERT TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes, next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

INSERT INTO post_history (staff_category_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, average_percentage_of_days_worked_per_week, weekends_are_standard_working_days, works_flexitime, standard_flexileave_rules_apply, departmental_closures_apply, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, job_title, force_role_status_to_past)
    VALUES (new.staff_category_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_through_payroll, new.hours_worked, new.percentage_of_fulltime_hours, new.average_percentage_of_days_worked_per_week,
    new.weekends_are_standard_working_days,
    leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    CASE
        WHEN (new.staff_category_id = 3) AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    new.departmental_closures_apply, new.probation_period::character varying(80), new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments::character varying(500), new.second_probation_meeting_comments::character varying(500), new.third_probation_meeting_comments::character varying(500), new.fourth_probation_meeting_comments::character varying(500), new.probation_outcome::character varying(200), new.date_probation_letters_sent, new.research_grant_number::character varying(500), new.sponsor::character varying(500), new.job_title, new.force_role_status_to_past)
RETURNING
    post_history.id, post_history.staff_category_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.force_role_status_to_past, NULL::date AS date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.average_percentage_of_days_worked_per_week, post_history.weekends_are_standard_working_days, post_history.works_flexitime, post_history.standard_flexileave_rules_apply, post_history.departmental_closures_apply, post_history.research_grant_number::text AS research_grant_number, post_history.filemaker_funding::text AS filemaker_funding, post_history.probation_period::text AS probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments, post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments, post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments, post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments, post_history.probation_outcome::text AS probation_outcome, post_history.date_probation_letters_sent, post_history.job_title, NULL::text AS text;

);


CREATE OR REPLACE VIEW leave_recording.role AS
  SELECT
    CASE
      WHEN (staff_category.category = 'Assistant staff'::text) THEN 'leave_recording\assistant_staff_Role'::text
      ELSE 'leave_recording\database\Role'::text
    END AS role_class_name,
    post_history.id as post_id,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    post_history.start_date,
    post_history.end_date,
    post_history.intended_end_date,
            ( SELECT min(closest_date.end_date) AS min
                   FROM ( SELECT post_history_1.id,
                            post_history_1.end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.intended_end_date AS end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.funding_end_date AS end_date
                           FROM public.post_history post_history_1
                          GROUP BY post_history_1.id) closest_date
                  WHERE (closest_date.id = post_history.id)) AS estimated_leaving_date,
    CASE
        WHEN person.continuous_employment_start_date <= post_history.start_date THEN person.continuous_employment_start_date
        ELSE NULL -- we don't record historical values
    END AS continuous_employment_start_date_as_at_start_of_role,
    post_history.hours_worked,
    post_history.job_title,
    --post_history.staff_category_id,
    --('sc-'::text || (post_history.staff_category_id)::text) AS post_category_id,
    staff_category.category AS post_category,
    post_history.percentage_of_fulltime_hours,
    post_history.average_percentage_of_days_worked_per_week,
    post_history.weekends_are_standard_working_days,
    post_history.works_flexitime,
    post_history.standard_flexileave_rules_apply,
    post_history.departmental_closures_apply,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name
 FROM public.post_history
 LEFT JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))
 LEFT JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = post_history.supervisor_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.continuous_employment_start_date
  FROM
    person) person ON person.person_id = post_history.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leave_recording.role
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role TO dev;
GRANT SELECT ON TABLE leave_recording.role TO www_leave_reporting;
