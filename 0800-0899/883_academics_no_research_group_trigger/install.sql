CREATE OR REPLACE FUNCTION public.research_group_trig ()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    AS $BODY$
DECLARE
    new_supervisor_id bigint;
    old_supervisor_id bigint;
    new_research_group_id bigint;
    is_academic boolean := FALSE;
BEGIN
    -- find out who we are running as and just exit if this is
    -- a gdpr purge
    IF CURRENT_USER = 'gdpr_purge' THEN
        RETURN NULL;
    END IF;
    -- the column we look in for the supervisor data varies with table
    IF TG_TABLE_NAME = 'visitorship' THEN
        new_supervisor_id = NEW.host_person_id;
        old_supervisor_id = OLD.host_person_id;
    ELSIF TG_TABLE_NAME = 'postgraduate_studentship' THEN
        new_supervisor_id = NEW.first_supervisor_id;
        old_supervisor_id = OLD.first_supervisor_id;
    ELSE
        new_supervisor_id = NEW.supervisor_id;
        old_supervisor_id = OLD.supervisor_id;
    END IF;
    -- For updates, check if supervisor changed and exit if not
    -- This avoids accidentally putting people back into groups they left if
    -- we update old records for some reason eg schema change
    IF TG_OP = 'UPDATE' THEN
        IF new_supervisor_id IS NOT DISTINCT FROM old_supervisor_id THEN
            RETURN NULL;
        END IF;
    END IF;
    -- check if this is an academic staff member and don't make changes if so
    IF TG_TABLE_NAME = 'post_history' THEN
        IF NEW.staff_category_id = 1 THEN
            RETURN NULL;
        END IF;
    END IF;
    -- or counts as an academic
    SELECT
        person.counts_as_academic INTO is_academic
    FROM
        person
    WHERE
        person.id = NEW.person_id;
    IF is_academic THEN
        RETURN NULL;
    END IF;
    -- what if the supervisor id isn't given? in which case we can bail
    IF new_supervisor_id IS DISTINCT FROM NULL THEN
        BEGIN
            -- find the research group from the supervisor_id
            SELECT
                id INTO STRICT new_research_group_id
            FROM
                research_group
            WHERE
                head_of_group_id = new_supervisor_id
                AND internal_use_only = FALSE;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RETURN NULL;
            WHEN TOO_MANY_ROWS THEN
                -- if the group is not unique then we can't safely do anything
                RETURN NULL;
        END;
    PERFORM
        *
    FROM
        mm_person_research_group
    WHERE
        person_id = NEW.person_id
        AND research_group_id = new_research_group_id;
    IF NOT FOUND THEN
        INSERT INTO mm_person_research_group (person_id, research_group_id)
            VALUES (NEW.person_id, new_research_group_id);
        END IF;
    END IF;
    RETURN NULL;
END;

$BODY$;

