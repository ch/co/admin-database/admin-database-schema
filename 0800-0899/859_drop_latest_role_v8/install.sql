-- view apps.academic_staff_crsids depends on view _latest_role_v8
CREATE OR REPLACE VIEW apps.academic_staff_crsids AS
SELECT
    person.crsid
FROM
    person
    LEFT JOIN _physical_status_v2 USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    _physical_status_v2.status_id::text = 'Current'::text
    AND (lr.post_category_id = 'sc-1'::text
        OR lr.post_category_id = 'sc-9'::text
        OR person.counts_as_academic = TRUE)
    AND person.crsid IS NOT NULL;

ALTER TABLE apps.academic_staff_crsids OWNER TO dev;

GRANT SELECT ON TABLE apps.academic_staff_crsids TO ad_accounts;

-- view apps.firstaiders_to_pay depends on view _latest_role_v8
CREATE OR REPLACE VIEW apps.firstaiders_to_pay AS
SELECT
    person.surname,
    person.first_names,
    person.email_address,
    lr.post_category,
    firstaider.qualification_date,
    firstaider.requalify_date,
    firstaider.hf_cn_trained,
    firstaider_funding.funding
FROM
    firstaider
    JOIN person ON firstaider.person_id = person.id
    LEFT JOIN firstaider_funding ON firstaider_funding.id = firstaider.firstaider_funding_id
    LEFT JOIN _latest_role_v12 lr ON firstaider.person_id = lr.person_id
    JOIN _physical_status_v2 ps ON ps.person_id = firstaider.person_id
WHERE
    ps.status_id::text = 'Current'::text
    AND firstaider.requalify_date >= 'now'::text::date
ORDER BY
    person.surname;

-- view apps.www_academic_staff_v2 depends on view _latest_role_v8
-- view www.academic_staff_v1 depends on view apps.www_academic_staff_v2
CREATE OR REPLACE VIEW apps.www_academic_staff_v2 AS
SELECT
    person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    person.counts_as_academic
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    ps.status_id::text = 'Current'::text
    AND person.do_not_show_on_website IS NOT FALSE;

ALTER TABLE apps.www_academic_staff_v2 OWNER TO dev;

GRANT SELECT ON TABLE apps.www_academic_staff_v2 TO www_sites;

-- view apps.www_academic_staff_v3 depends on view _latest_role_v8
CREATE OR REPLACE VIEW apps.www_academic_staff_v3 AS
SELECT
    person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    person.counts_as_academic
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE (ps.status_id::text = ANY (ARRAY['Current'::text, 'Future'::text]))
AND person.do_not_show_on_website IS NOT TRUE;

ALTER TABLE apps.www_academic_staff_v3 OWNER TO dev;

GRANT SELECT ON TABLE apps.www_academic_staff_v3 TO www_sites;

-- view _add_ad_accounts depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._add_ad_accounts AS
SELECT
    person.crsid,
    person.first_names,
    person.surname,
    person.email_address,
    lr.role_tablename,
    ARRAY (
        SELECT
            research_group.name
        FROM
            mm_person_research_group
            JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
        WHERE
            mm_person_research_group.person_id = person.id) AS rg,
    lr.post_category,
    person.leaving_date,
    ps.status_id
FROM
    person
    JOIN _latest_role_v12 lr ON person.id = lr.person_id
    JOIN _physical_status_v2 ps USING (person_id);

ALTER TABLE public._add_ad_accounts OWNER TO dev;

GRANT SELECT ON TABLE public._add_ad_accounts TO ad_accounts;

-- view _chem_faculty_mailinglist depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._chem_faculty_mailinglist AS
SELECT
    person.email_address,
    person.crsid,
    person_hid.person_hid
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN _physical_status_v2 ps ON ps.person_id = person.id
    JOIN person_hid ON person.id = person_hid.person_id
WHERE (lr.post_category::text = 'Academic staff'::text
    OR lr.post_category::text = 'Teaching Fellow'::text
    OR person.counts_as_academic = TRUE)
AND ps.status_id::text = 'Current'::text
AND NOT (person.id IN (
        SELECT
            mm_mailinglist_exclude_person.exclude_person_id
        FROM
            mm_mailinglist_exclude_person
        WHERE
            mm_mailinglist_exclude_person.mailinglist_id = ((
                SELECT
                    mailinglist.id FROM mailinglist
                WHERE
                    mailinglist.name::text = 'chem-faculty'::text))));

ALTER TABLE public._chem_faculty_mailinglist OWNER TO dev;

GRANT SELECT ON TABLE public._chem_faculty_mailinglist TO leavers_trigger;

GRANT SELECT ON TABLE public._chem_faculty_mailinglist TO mailinglists;

GRANT SELECT ON TABLE public._chem_faculty_mailinglist TO www_sites;

-- view _chemacademic_mailinglist depends on view _latest_role_v8
-- view _chemacademic_adgroup depends on view _chemacademic_mailinglist
CREATE OR REPLACE VIEW public._chemacademic_mailinglist AS
SELECT
    person.email_address,
    person.crsid,
    person_hid.person_hid
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN _physical_status_v2 ps ON ps.person_id = person.id
    JOIN person_hid ON person.id = person_hid.person_id
WHERE (lr.post_category::text = 'Academic staff'::text
    OR lr.post_category::text = 'Academic-related staff'::text
    OR person.counts_as_academic = TRUE)
AND ps.status_id::text = 'Current'::text
AND NOT (person.id IN (
        SELECT
            mm_mailinglist_exclude_person.exclude_person_id
        FROM
            mm_mailinglist_exclude_person
        WHERE
            mm_mailinglist_exclude_person.mailinglist_id = ((
                SELECT
                    mailinglist.id FROM mailinglist
                WHERE
                    mailinglist.name::text = 'chem-academic'::text))))
AND person.email_address IS NOT NULL
UNION
SELECT
    person.email_address,
    person.crsid,
    NULL::character varying(80) AS person_hid
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v2 ps ON ps.person_id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-academic'::text))
    AND ps.status_id::text = 'Current'::text
    AND person.email_address IS NOT NULL;

ALTER TABLE public._chemacademic_mailinglist OWNER TO dev;

GRANT SELECT ON TABLE public._chemacademic_mailinglist TO leavers_trigger;

GRANT SELECT ON TABLE public._chemacademic_mailinglist TO mailinglists;

GRANT SELECT ON TABLE public._chemacademic_mailinglist TO www_sites;

-- view _chemacademicrelated_mailinglist depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._chemacademicrelated_mailinglist AS
SELECT
    person.email_address,
    person.crsid
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN _physical_status ps ON ps.id = person.id
WHERE
    lr.post_category::text = 'Academic-related staff'::text
    AND ps.physical_status::text = 'Current'::text
    AND NOT (person.id IN (
            SELECT
                mm_mailinglist_exclude_person.exclude_person_id
            FROM
                mm_mailinglist_exclude_person
            WHERE
                mm_mailinglist_exclude_person.mailinglist_id = ((
                    SELECT
                        mailinglist.id FROM mailinglist
                    WHERE
                        mailinglist.name::text = 'chem-academic-related'::text))))
UNION
SELECT
    person.email_address,
    person.crsid
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status ps ON ps.id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-academic-related'::text))
AND ps.physical_status::text = 'Current'::text;

ALTER TABLE public._chemacademicrelated_mailinglist OWNER TO dev;

GRANT SELECT ON TABLE public._chemacademicrelated_mailinglist TO leavers_trigger;

GRANT SELECT ON TABLE public._chemacademicrelated_mailinglist TO mailinglists;

GRANT SELECT ON TABLE public._chemacademicrelated_mailinglist TO www_sites;

-- view _chemgeneral_mailinglist_v2 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._chemgeneral_mailinglist_v2 AS
SELECT
    person.email_address,
    person.crsid
FROM
    person
    LEFT JOIN _physical_status_v2 ps ON ps.id = person.id
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    ps.status_id::text = 'Current'::text
    AND lr.post_category::text <> 'Part III'::text
    AND lr.post_category::text <> 'Part II'::text
    AND NOT (person.id IN (
            SELECT
                mm_mailinglist_exclude_person.exclude_person_id
            FROM
                mm_mailinglist_exclude_person
            WHERE
                mm_mailinglist_exclude_person.mailinglist_id = ((
                    SELECT
                        mailinglist.id FROM mailinglist
                    WHERE
                        mailinglist.name::text = 'chem-general'::text))))
UNION
SELECT
    person.email_address,
    person.crsid
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v2 ps ON ps.id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-general'::text))
AND ps.status_id::text = 'Current'::text;

ALTER TABLE public._chemgeneral_mailinglist_v2 OWNER TO dev;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist_v2 TO mailinglist_readers;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist_v2 TO mailinglists;

GRANT SELECT ON TABLE public._chemgeneral_mailinglist_v2 TO www_sites;

-- view _mailing_list_chasing depends on view _latest_role_v8
DROP VIEW public._mailing_list_chasing;

-- view _space_hosted_people_by_group depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._space_hosted_people_by_group AS SELECT DISTINCT
    person_hid.person_hid AS name,
    room.name AS room,
    lr.post_category,
    lr.estimated_leaving_date,
    supervisor_hid.supervisor_hid,
    research_group.name AS research_group,
    supervisor.crsid AS supervisor_crsid
FROM
    room
    JOIN mm_person_room ON room.id = mm_person_room.room_id
    JOIN person ON mm_person_room.person_id = person.id
    JOIN person_hid USING (person_id)
    JOIN _physical_status_v2 ps USING (person_id)
    JOIN _latest_role_v12 lr USING (person_id)
    JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
    JOIN person supervisor ON lr.supervisor_id = supervisor.id
    JOIN research_group ON room.responsible_group_id = research_group.id
    JOIN person hog ON research_group.head_of_group_id = hog.id
WHERE
    ps.status_id::text = 'Current'::text
    AND hog.crsid::text <> supervisor.crsid::text
ORDER BY
    person_hid.person_hid,
    room.name,
    lr.post_category,
    lr.estimated_leaving_date,
    supervisor_hid.supervisor_hid,
    research_group.name,
    supervisor.crsid;

ALTER TABLE public._space_hosted_people_by_group OWNER TO dev;

GRANT SELECT ON TABLE public._space_hosted_people_by_group TO space_report;

-- view _space_people_by_research_group depends on view _latest_role_v8
CREATE OR REPLACE VIEW public._space_people_by_research_group AS (
    SELECT DISTINCT
        person_hid.person_hid AS name,
        lr.post_category,
        lr.estimated_leaving_date,
        ARRAY (
            SELECT
                ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
            FROM
                room
                JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
                JOIN mm_person_room ON room.id = mm_person_room.room_id
                JOIN person occupant ON mm_person_room.person_id = occupant.id
            WHERE
                occupant.id = person.id) AS rooms,
            person.email_address,
            ARRAY (
                SELECT
                    dept_telephone_number_hid.dept_telephone_number_hid
                FROM
                    person p2
                    JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
                    JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
                WHERE
                    p2.id = person.id) AS phones,
                research_group.name AS research_group
            FROM
                person
                JOIN person_hid ON person.id = person_hid.person_id
                JOIN _physical_status_v2 ps USING (person_id)
                LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
                LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
                LEFT JOIN research_group ON research_group.id = mm_person_research_group.research_group_id
            WHERE
                ps.status_id::text = 'Current'::text
            ORDER BY
                person_hid.person_hid, lr.post_category, lr.estimated_leaving_date, (ARRAY (
                        SELECT
                            ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
                        FROM
                            room
                            JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
                            JOIN mm_person_room ON room.id = mm_person_room.room_id
                            JOIN person occupant ON mm_person_room.person_id = occupant.id
                        WHERE
                            occupant.id = person.id)), person.email_address, (ARRAY (
                            SELECT
                                dept_telephone_number_hid.dept_telephone_number_hid
                            FROM
                                person p2
                                JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
                                JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
                            WHERE
                                p2.id = person.id)), research_group.name)
            UNION
            SELECT
                person_hid.person_hid AS name,
                lr.post_category,
                lr.estimated_leaving_date,
                ARRAY (
                    SELECT
                        ((room.name::text || ' ('::text) || room_type_hid.room_type_hid::text) || ')'::text
                    FROM
                        room
                        JOIN room_type_hid ON room.room_type_id = room_type_hid.room_type_id
                        JOIN mm_person_room ON room.id = mm_person_room.room_id
                        JOIN person occupant ON mm_person_room.person_id = occupant.id
                    WHERE
                        occupant.id = person.id) AS rooms,
                person.email_address,
                ARRAY (
                    SELECT
                        dept_telephone_number_hid.dept_telephone_number_hid
                    FROM
                        person p2
                        JOIN mm_person_dept_telephone_number ON p2.id = mm_person_dept_telephone_number.person_id
                        JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
                    WHERE
                        p2.id = person.id) AS phones,
                research_group.name AS research_group
            FROM
                research_group
                JOIN person ON person.id = research_group.head_of_group_id
                JOIN person_hid ON person.id = person_hid.person_id
                LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id;

ALTER TABLE public._space_people_by_research_group OWNER TO dev;

GRANT SELECT ON TABLE public._space_people_by_research_group TO space_report;

-- view www_academic_staff_v1 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_academic_staff_v1 AS
SELECT
    person.id,
    person.crsid,
    person_hid.person_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    ps.status_id::text = 'Current'::text;

-- view www_committee_members depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_committee_members AS
SELECT
    person.id,
    person_hid.person_hid,
    lr.post_category_id
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    lr.post_category_id = 'sc-1'::text
    OR lr.post_category_id = 'sc-2'::text
    OR lr.post_category_id = 'sc-3'::text
    OR lr.post_category_id = 'sc-9'::text
ORDER BY
    person_hid.person_hid;

ALTER TABLE public.www_committee_members OWNER TO dev;

GRANT SELECT ON TABLE public.www_committee_members TO www_sites;

-- view www_committee_members_v2 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_committee_members_v2 AS
SELECT
    person.id,
    person_hid.person_hid,
    lr.post_category_id
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE
    lr.post_category_id = 'sc-1'::text
    OR lr.post_category_id = 'sc-2'::text
    OR lr.post_category_id = 'sc-3'::text
    OR lr.post_category_id = 'sc-9'::text
    OR lr.post_category_id = 'v-8'::text
    OR lr.post_category_id = 'sc-4'::text
    OR lr.post_category_id = 'sc-7'::text
ORDER BY
    person_hid.person_hid;

ALTER TABLE public.www_committee_members_v2 OWNER TO dev;

GRANT SELECT ON TABLE public.www_committee_members_v2 TO www_sites;

-- view www_committee_members_v3 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_committee_members_v3 AS
SELECT
    person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE (lr.post_category_id = 'sc-1'::text
    OR lr.post_category_id = 'sc-2'::text
    OR lr.post_category_id = 'sc-3'::text
    OR lr.post_category_id = 'sc-9'::text
    OR lr.post_category_id = 'v-8'::text
    OR lr.post_category_id = 'sc-4'::text
    OR lr.post_category_id = 'sc-7'::text
    OR lr.post_category_id = 'sc-6'::text)
AND ps.status_id::text = 'Current'::text
ORDER BY
    person_hid.person_hid;

-- view www_committee_student_members depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_committee_student_members AS
SELECT
    person.id,
    person_hid.person_hid,
    lr.post_category_id,
    ps.status_id
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
WHERE (lr.post_category_id = 'pg-1'::text
    OR lr.post_category_id = 'pg-2'::text
    OR lr.post_category_id = 'pg-3'::text
    OR lr.post_category_id = 'pg-4'::text
    OR lr.post_category_id = 'e-1'::text
    OR lr.post_category_id = 'e-2'::text
    OR lr.post_category_id = 'e-3'::text)
AND ps.status_id::text = 'Current'::text
ORDER BY
    person_hid.person_hid;

-- view www_mailinglist_academic depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_mailinglist_academic AS
SELECT
    person.crsid,
    person_hid.person_hid
FROM
    person
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN _physical_status_v2 ps ON ps.person_id = person.id
    JOIN person_hid ON person.id = person_hid.person_id
WHERE (lr.post_category::text = 'Academic staff'::text
    OR lr.post_category::text = 'Academic-related staff'::text)
AND ps.status_id::text = 'Current'::text
AND NOT (person.id IN (
        SELECT
            mm_mailinglist_exclude_person.exclude_person_id
        FROM
            mm_mailinglist_exclude_person
        WHERE
            mm_mailinglist_exclude_person.mailinglist_id = ((
                SELECT
                    mailinglist.id FROM mailinglist
                WHERE
                    mailinglist.name::text = 'chem-academic'::text))))
UNION
SELECT
    person.crsid,
    NULL::character varying(80) AS person_hid
FROM
    mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v2 ps ON ps.person_id = person.id
WHERE
    mm_mailinglist_include_person.mailinglist_id = ((
        SELECT
            mailinglist.id FROM mailinglist
        WHERE
            mailinglist.name::text = 'chem-academic'::text))
    AND ps.status_id::text = 'Current'::text;

ALTER TABLE public.www_mailinglist_academic OWNER TO dev;

-- view www_telephone_directory_v1 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_telephone_directory_v1 AS
SELECT
    person.id,
    person.crsid,
    CASE WHEN person.hide_email THEN
        '(not available)'::character varying
    ELSE
        person.email_address
    END AS email_address,
    person_hid.person_hid,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
    LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
    LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
    LEFT JOIN room_hid USING (room_id)
    LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
WHERE
    ps.status_id::text = 'Current'::text;

-- view www_telephone_directory_v2 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_telephone_directory_v2 AS
SELECT
    person.id,
    person.crsid,
    CASE WHEN person.hide_email THEN
        '(not available)'::character varying
    ELSE
        person.email_address
    END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
    LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
    LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
    LEFT JOIN room_hid USING (room_id)
    LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
WHERE
    ps.status_id::text = 'Current'::text
ORDER BY
    person.surname,
    (COALESCE(person.known_as, person.first_names));

ALTER TABLE public.www_telephone_directory_v2 OWNER TO dev;

GRANT SELECT ON TABLE public.www_telephone_directory_v2 TO www_sites;

-- view www_telephone_directory_v3 depends on view _latest_role_v8
CREATE OR REPLACE VIEW public.www_telephone_directory_v3 AS
SELECT
    person.id,
    person.crsid,
    CASE WHEN person.hide_email THEN
        '(not available)'::character varying
    ELSE
        person.email_address
    END AS email_address,
    person_hid.person_hid,
    person.surname,
    person.known_as,
    person.first_names,
    dept_telephone_number_hid.dept_telephone_number_hid AS telephone_number,
    www_room_hid.room_hid,
    ps.status_id,
    lr.post_category_id,
    lr.post_category,
    lr.supervisor_id,
    supervisor_hid.supervisor_hid
FROM
    person
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ps USING (id)
    LEFT JOIN _latest_role_v12 lr ON person.id = lr.person_id
    LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
    LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
    LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
    LEFT JOIN www_room_hid USING (room_id)
    LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = lr.supervisor_id
WHERE
    ps.status_id::text = 'Current'::text
ORDER BY
    person.surname,
    (COALESCE(person.known_as, person.first_names));

ALTER TABLE public.www_telephone_directory_v3 OWNER TO dev;

GRANT SELECT ON TABLE public.www_telephone_directory_v3 TO www_sites;

CREATE OR REPLACE VIEW apps._space_people_using_other_groups_space
 AS
 SELECT research_group.name AS research_group,
    person_hid.person_hid AS name,
    lr.post_category,
    lr.estimated_leaving_date,
    room.name AS room,
    room_group.name AS room_group
   FROM research_group
     JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v2 ps USING (person_id)
     JOIN _latest_role_v12 lr USING (person_id)
     JOIN mm_person_room USING (person_id)
     JOIN room ON mm_person_room.room_id = room.id
     JOIN research_group room_group ON room.responsible_group_id = room_group.id
  WHERE ps.status_id::text = 'Current'::text AND NOT (room_group.name::text IN ( SELECT rg.name
           FROM mm_person_research_group mm1
             JOIN research_group rg ON mm1.research_group_id = rg.id
          WHERE mm1.person_id = person_hid.person_id));

ALTER TABLE apps._space_people_using_other_groups_space OWNER TO dev;

GRANT SELECT ON TABLE apps._space_people_using_other_groups_space TO space_report;

DROP VIEW public._latest_role_v8;
