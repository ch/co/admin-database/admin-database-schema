DROP VIEW hotwire3."10_View/Registration/Additional_People_Allowed_As_Supervisors";

CREATE OR REPLACE VIEW registration.supervisor_hid AS 
 SELECT DISTINCT person.id,
    person.surname,
    COALESCE(person.known_as, person.first_names) AS first_names,
    ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS hid
   FROM person
     LEFT JOIN title_hid ON person.title_id = title_hid.title_id
     JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
     JOIN post_history ON person.id = post_history.person_id
  WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Future'::text) AND (post_history.force_role_status_to_past IS NULL OR post_history.force_role_status_to_past = false) AND (post_history.end_date IS NULL OR post_history.end_date > 'now'::text::date)
  ORDER BY person.surname, (COALESCE(person.known_as, person.first_names));

ALTER TABLE registration.supervisor_hid
  OWNER TO dev;
GRANT ALL ON TABLE registration.supervisor_hid TO dev;
GRANT SELECT ON TABLE registration.supervisor_hid TO starters_registration;

DROP TABLE registration.extra_hosts;
