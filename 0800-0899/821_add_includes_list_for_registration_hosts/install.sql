CREATE TABLE registration.extra_hosts (
    id SERIAL PRIMARY KEY,
    person_id BIGINT UNIQUE NOT NULL REFERENCES person(id),
    notes text
);

ALTER TABLE registration.extra_hosts OWNER TO dev; 
GRANT SELECT,INSERT,UPDATE,DELETE ON registration.extra_hosts TO hr,safety_management;
COMMENT ON TABLE registration.extra_hosts IS 'To allow us to nominate some special cases of non-employees as being permitted to host people in the department or carry out safety inductions';

CREATE OR REPLACE VIEW registration.supervisor_hid AS
WITH allowed_supervisors AS (
    -- currently employed people
    SELECT person.id, person.surname, person.first_names, person.known_as, person.title_id
    FROM person
    JOIN post_history ON person.id = post_history.person_id
    WHERE (post_history.force_role_status_to_past IS NULL OR post_history.force_role_status_to_past = false) AND (post_history.end_date IS NULL OR post_history.end_date > 'now'::text::date)
  UNION
    -- anyone else we specifically allow
    SELECT person.id, person.surname, person.first_names, person.known_as, person.title_id
    FROM registration.extra_hosts
    JOIN person ON extra_hosts.person_id = person.id
)
 SELECT
  id,
  surname,
  COALESCE(known_as, first_names) AS first_names,
  ((surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(known_as, first_names)::text AS hid
  FROM allowed_supervisors
  LEFT JOIN title_hid ON allowed_supervisors.title_id = title_hid.title_id
  JOIN _physical_status_v3 ON allowed_supervisors.id = _physical_status_v3.person_id
  WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Future'::text)
  ORDER BY allowed_supervisors.surname, (COALESCE(allowed_supervisors.known_as, allowed_supervisors.first_names));

ALTER TABLE registration.supervisor_hid
  OWNER TO dev;
GRANT ALL ON TABLE registration.supervisor_hid TO dev;
GRANT SELECT ON TABLE registration.supervisor_hid TO starters_registration;

CREATE VIEW hotwire3."10_View/Registration/Additional_People_Allowed_As_Supervisors" AS
SELECT
  id,
  person_id,
  notes
FROM registration.extra_hosts;
-- This is a simple view using a single table and so needs no triggers or update rules

ALTER VIEW hotwire3."10_View/Registration/Additional_People_Allowed_As_Supervisors" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Registration/Additional_People_Allowed_As_Supervisors" TO hr,safety_management;
