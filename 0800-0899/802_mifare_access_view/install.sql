GRANT SELECT(form_id,mifare_access_level_id,mifare_areas,mifare_access_processed), UPDATE(mifare_access_processed) ON registration.form TO reception;

CREATE VIEW hotwire3."10_View/People/MiFare_Access" AS
SELECT
    form.form_id as id,
    person.surname AS ro_surname,
    person.first_names AS ro_first_names,
    person.crsid AS ro_crsid,
    form.mifare_access_level_id AS ro_mifare_access_level_id,
    form.mifare_areas AS ro_group_areas,
    misd_card.card_id AS ro_card,
    misd_card.mifarenum AS ro_mifarenum,
    misd_card.mifareid AS ro_mifareid,
    misd_card.barcode AS ro_barcode,
    form.mifare_access_processed
FROM registration.form
LEFT JOIN person ON form._match_to_person_id = person.id
LEFT JOIN public.misd_card ON LOWER(misd_card.crsid) = person.crsid
WHERE _imported_on <= current_date AND mifare_access_processed = 'f';

ALTER TABLE hotwire3."10_View/People/MiFare_Access" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire3."10_View/People/MiFare_Access" TO reception;

CREATE FUNCTION registration.update_mifare() RETURNS TRIGGER AS
$body$
BEGIN
    UPDATE registration.form SET mifare_access_processed = NEW.mifare_access_processed WHERE form_id = OLD.id;
    RETURN null;
END;
$body$ LANGUAGE plpgsql;
ALTER FUNCTION registration.update_mifare() OWNER TO dev;

CREATE TRIGGER mifare_update INSTEAD OF UPDATE ON hotwire3."10_View/People/MiFare_Access" FOR EACH ROW EXECUTE PROCEDURE registration.update_mifare();
