#!/usr/bin/python3
import psycopg2
import csv

sql = '''
UPDATE nationality SET country_name = %s WHERE nationality = %s
'''

conn = psycopg2.connect("host=database.ch.private.cam.ac.uk dbname=chemistry user=cen1001")
cur = conn.cursor()

with open('nationalities.csv') as f:
    r = csv.DictReader(f)
    for row in r:
        cur.execute(sql,[row['Country'].strip(),row['Demonym'].strip()])
        

conn.commit()
