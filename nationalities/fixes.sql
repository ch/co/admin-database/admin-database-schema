-- Misspelt
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'Ukrainian' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'Ukranian');
DELETE FROM nationality WHERE nationality = 'Ukranian';

-- Misspelt
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'Colombian' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'Columbian');
DELETE FROM nationality WHERE nationality = 'Columbian';

-- Misspelt
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'Moldovan' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'Moldavian');
DELETE FROM nationality WHERE nationality = 'Moldavian';

-- Use adjective not noun
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'Mosotho' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'Lesotho');
DELETE FROM nationality WHERE nationality = 'Lesotho';

-- Not a country that issues passports
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'Swiss' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'French-Swiss');
DELETE FROM nationality WHERE nationality = 'French-Swiss';

-- Not a country that issues passports
UPDATE mm_person_nationality SET nationality_id = (SELECT id FROM nationality WHERE nationality = 'British' ) WHERE nationality_id = (SELECT id FROM nationality WHERE nationality = 'Scottish');
DELETE FROM nationality WHERE nationality = 'Scottish';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='Kabardian' );
DELETE FROM nationality WHERE nationality = 'Kabardian';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='Other' );
DELETE FROM nationality WHERE nationality = 'Other';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='Overseas' );
DELETE FROM nationality WHERE nationality = 'Overseas';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='EC' );
DELETE FROM nationality WHERE nationality = 'EC';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='African' );
DELETE FROM nationality WHERE nationality = 'African';

-- Not a country that issues passports
DELETE FROM mm_person_nationality WHERE nationality_id = ( SELECT id FROM nationality WHERE nationality ='Jewish' );
DELETE FROM nationality WHERE nationality = 'Jewish';

-- Difficult. I've removed Scottish already because 'Scottish' passports aren't
-- a thing and similarly would remove Welsh and English if anyone had added
-- them. We have no records with this one assigned currently. We also have both British
-- and Irish as options. So I'm removing it but might reinstate later. 
DELETE FROM nationality WHERE nationality = 'Northern Irish';

-- Use adjective not noun
UPDATE nationality SET nationality = 'Hong Kong Chinese', country_name = 'Hong Kong' WHERE nationality = 'Hong Kong';

-- Think this is the best compromise for this one - certainly don't want to delete it
UPDATE nationality SET country_name = 'United States' WHERE nationality = 'American Navajo';

-- Use adjective not noun; the adjective to use is very debatable though
UPDATE nationality SET nationality = 'Serbian and Montenegrin', country_name = 'Serbia and Montenegro'  WHERE nationality = 'Serbia & Montenegro';
