-- new table personal_software_compliance_declarations with date, notes, link to 'person'

CREATE SEQUENCE personal_software_licence_compliance_declaration_id_seq;
ALTER SEQUENCE personal_software_licence_compliance_declaration_id_seq OWNER TO dev;

CREATE TABLE personal_software_licence_compliance_declaration (
    id BIGINT PRIMARY KEY DEFAULT nextval('personal_software_licence_compliance_declaration_id_seq'),
    person_id BIGINT NOT NULL REFERENCES person (id) ON DELETE CASCADE, -- a person rather than a user account declares compliance
    declaration_date DATE NOT NULL,
    notes VARCHAR
);

ALTER TABLE personal_software_licence_compliance_declaration OWNER TO dev;
GRANT INSERT ON personal_software_licence_compliance_declaration TO software_licence_compliance;
GRANT USAGE ON personal_software_licence_compliance_declaration_id_seq TO software_licence_compliance;

-- For the cases where we don't have a person we just record the account name instead
-- as we are't yet in a position to have a database 'person' record for every account
CREATE SEQUENCE unidentified_software_licence_compliance_declaration_id_seq;
CREATE TABLE unidentified_software_licence_compliance_declaration (
    id BIGINT PRIMARY KEY DEFAULT nextval('unidentified_software_licence_compliance_declaration_id_seq'),
    account_name VARCHAR NOT NULL,
    declaration_date DATE NOT NULL,
    notes VARCHAR
);
ALTER TABLE unidentified_software_licence_compliance_declaration OWNER TO dev;
GRANT INSERT ON unidentified_software_licence_compliance_declaration TO software_licence_compliance;
GRANT USAGE ON unidentified_software_licence_compliance_declaration_id_seq TO software_licence_compliance;


CREATE VIEW apps.personal_software_licence_compliance_recording AS
WITH all_declarations AS (
    SELECT
        COALESCE(admitto_account.username,person.crsid) AS username,
        declaration_date
    FROM personal_software_licence_compliance_declaration
    JOIN person ON personal_software_licence_compliance_declaration.person_id = person.id
    LEFT JOIN admitto_account ON admitto_account.person_id = person.id
    UNION
    SELECT
        account_name as username,
        declaration_date 
    FROM unidentified_software_licence_compliance_declaration
)
SELECT username, max(declaration_date) AS last_declaration
FROM all_declarations
GROUP BY username
;

ALTER VIEW apps.personal_software_licence_compliance_recording OWNER TO dev;

-- These rights needed to allow the function below to run
GRANT SELECT (username, person_id) ON admitto_account TO software_licence_compliance;
GRANT SELECT (username, person_id) ON user_account TO software_licence_compliance;
GRANT SELECT (crsid, id) ON person TO software_licence_compliance;

-- This runs on insert so we always have NEW but never OLD
CREATE FUNCTION apps.personal_software_compliance_recording() RETURNS TRIGGER AS $$
DECLARE
    declarer_id bigint;
BEGIN
    -- Look for a person attached to NEW.username in admitto_account and user_account
    -- person_id first, failing that try matching username to crsid
    select person_id into declarer_id
    from (
            select username, person_id from admitto_account where person_id is not null
            union
            select username, person_id from user_account where person_id is not null
            union
            select crsid as username, id as person_id from person where crsid is not null
    ) a
    where NEW.username = a.username;
    -- if we got a person, we insert into personal_software_compliance_declaration
    -- if we didn't, insert into unidentified_software_compliance_declaration
    IF NOT FOUND THEN
        -- unidentified
        INSERT INTO unidentified_software_licence_compliance_declaration ( account_name, declaration_date ) VALUES ( NEW.username, current_date );
    ELSE
        -- person
        INSERT INTO personal_software_licence_compliance_declaration ( person_id, declaration_date ) VALUES ( declarer_id, current_date );
    END IF;
    RETURN NEW;

END;
$$ LANGUAGE PLPGSQL;
ALTER FUNCTION apps.personal_software_compliance_recording() OWNER TO dev;

CREATE TRIGGER compliance_updates INSTEAD OF INSERT ON apps.personal_software_licence_compliance_recording
    FOR EACH ROW EXECUTE PROCEDURE apps.personal_software_compliance_recording();

GRANT SELECT, INSERT ON apps.personal_software_licence_compliance_recording TO software_licence_compliance;
