DROP VIEW apps.personal_software_licence_compliance_recording;
DROP FUNCTION apps.personal_software_compliance_recording();
REVOKE SELECT (username, person_id) ON admitto_account FROM software_licence_compliance;
REVOKE SELECT (username, person_id) ON user_account FROM software_licence_compliance;
REVOKE SELECT (crsid, id) ON person FROM software_licence_compliance;

DROP TABLE personal_software_licence_compliance_declaration;
DROP TABLE unidentified_software_licence_compliance_declaration;
DROP SEQUENCE personal_software_licence_compliance_declaration_id_seq;
DROP SEQUENCE unidentified_software_licence_compliance_declaration_id_seq;
