DROP TRIGGER remove_ip_if_hostname_blank ON public.ip_address;
DROP FUNCTION public.blankhostname_remove_systemimage();

CREATE OR REPLACE FUNCTION public.tidy_ip_address_when_hostname_blank()
  RETURNS trigger AS
$BODY$
begin
if ((new.hostname is null) 
 or (new.hostname='')) THEN

 delete from mm_system_image_ip_address where ip_address_id=old.id;
 delete from dns_cnames where toname=old.hostname;

 new.last_seen=NULL;
 new.hobbit_flags=NULL;

end if;
return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

ALTER FUNCTION public.tidy_ip_address_when_hostname_blank() SET search_path=public, pg_temp;

GRANT CREATE ON SCHEMA public TO ipreg;
ALTER FUNCTION public.tidy_ip_address_when_hostname_blank() OWNER TO ipreg;
REVOKE CREATE ON SCHEMA public FROM ipreg;

GRANT EXECUTE ON FUNCTION public.tidy_ip_address_when_hostname_blank() TO ipreg;
GRANT EXECUTE ON FUNCTION public.tidy_ip_address_when_hostname_blank() TO dev;
GRANT EXECUTE ON FUNCTION public.tidy_ip_address_when_hostname_blank() TO cos;
GRANT EXECUTE ON FUNCTION public.tidy_ip_address_when_hostname_blank() TO groupitreps;
GRANT EXECUTE ON FUNCTION public.tidy_ip_address_when_hostname_blank() TO headsofgroup;
REVOKE ALL ON FUNCTION public.tidy_ip_address_when_hostname_blank() FROM public;

CREATE TRIGGER tidy_ip_address_if_hostname_blank
  BEFORE UPDATE
  ON public.ip_address
  FOR EACH ROW
  EXECUTE PROCEDURE public.tidy_ip_address_when_hostname_blank();
