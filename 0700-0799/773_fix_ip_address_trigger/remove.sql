DROP TRIGGER tidy_ip_address_if_hostname_blank on public.ip_address;
DROP FUNCTION public.tidy_ip_address_when_hostname_blank();

CREATE OR REPLACE FUNCTION public.blankhostname_remove_systemimage()
  RETURNS trigger AS
$BODY$
begin
if ((new.hostname is null) 
 or (new.hostname='')) THEN
  new.last_seen=NULL;
 delete from mm_system_image_ip_address where ip_address_id=old.id;
 new.hobbit_flags=NULL;
 delete from dns_cnames where toname=old.hostname;
end if;
return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

ALTER FUNCTION public.blankhostname_remove_systemimage() SET search_path=public, pg_temp;

GRANT CREATE ON SCHEMA public TO ipreg;
ALTER FUNCTION public.blankhostname_remove_systemimage() OWNER TO ipreg;
REVOKE CREATE ON SCHEMA public FROM ipreg;

GRANT EXECUTE ON FUNCTION public.blankhostname_remove_systemimage() TO ipreg;
GRANT EXECUTE ON FUNCTION public.blankhostname_remove_systemimage() TO dev;
GRANT EXECUTE ON FUNCTION public.blankhostname_remove_systemimage() TO cos;
GRANT EXECUTE ON FUNCTION public.blankhostname_remove_systemimage() TO groupitreps;
GRANT EXECUTE ON FUNCTION public.blankhostname_remove_systemimage() TO headsofgroup;
REVOKE ALL ON FUNCTION public.blankhostname_remove_systemimage() FROM public;

CREATE TRIGGER remove_ip_if_hostname_blank
  AFTER UPDATE
  ON public.ip_address
  FOR EACH ROW
  EXECUTE PROCEDURE public.blankhostname_remove_systemimage();
