CREATE VIEW apps.os_class_hid AS
SELECT os_class_hid.os_class_id, os_class_hid.os_class_hid FROM public.os_class_hid; 

ALTER VIEW apps.os_class_hid OWNER TO dev;

GRANT SELECT ON apps.os_class_hid TO osbuilder,cos;
