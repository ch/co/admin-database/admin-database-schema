DROP VIEW hotwire3. "10_View/Roles/Part_III_Students";

CREATE VIEW hotwire3. "10_View/Roles/Part_III_Students" AS
SELECT
    a.id,
    a.person_id,
    a.project_title_html,
    a.supervisor_id,
    a.intended_end_date,
    a.end_date,
    a.start_date,
    a.ro_role_status_id,
    a.notes,
    a.force_role_status_to_past,
    a._surname,
    a._first_names
FROM (
    SELECT
        part_iii_studentship.id,
        part_iii_studentship.person_id,
        part_iii_studentship.project_title AS project_title_html,
        part_iii_studentship.supervisor_id,
        part_iii_studentship.intended_end_date,
        part_iii_studentship.end_date,
        part_iii_studentship.start_date,
        _all_roles.status AS ro_role_status_id,
        part_iii_studentship.notes::text AS notes,
        part_iii_studentship.force_role_status_to_past,
        person.surname AS _surname,
        person.first_names AS _first_names
    FROM
        part_iii_studentship
    LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = part_iii_studentship.id
    LEFT JOIN person ON person.id = part_iii_studentship.person_id
WHERE
    _all_roles.role_tablename = 'part_iii_studentship'::text) a
ORDER BY
    a._surname,
    a._first_names;

ALTER TABLE hotwire3. "10_View/Roles/Part_III_Students" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Roles/Part_III_Students" TO dev;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Roles/Part_III_Students" TO student_management;

GRANT SELECT ON TABLE hotwire3. "10_View/Roles/Part_III_Students" TO mgmt_ro;

-- Rule: hotwire3_view_part_iii_del ON hotwire3."10_View/Roles/Part_III_Students"
-- DROP RULE hotwire3_view_part_iii_del ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_del AS ON DELETE TO hotwire3. "10_View/Roles/Part_III_Students"
    DO INSTEAD
    DELETE
FROM
    part_iii_studentship
WHERE
    part_iii_studentship.id = old.id;

-- Rule: hotwire3_view_part_iii_ins ON hotwire3."10_View/Roles/Part_III_Students"
-- DROP RULE hotwire3_view_part_iii_ins ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_ins AS ON INSERT TO hotwire3. "10_View/Roles/Part_III_Students"
    DO INSTEAD
    INSERT INTO part_iii_studentship (
        person_id, project_title, supervisor_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past)
        VALUES (
            new.person_id, new.project_title_html, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.notes ::character varying(80), new.force_role_status_to_past)
    RETURNING
        part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, NULL::character varying( 10
) AS "varchar", part_iii_studentship.notes::text AS notes, part_iii_studentship.force_role_status_to_past, NULL::character varying( 32
) AS "varchar", NULL::character varying( 32
) AS "varchar";

-- Rule: hotwire3_view_part_iii_upd ON hotwire3."10_View/Roles/Part_III_Students"
-- DROP RULE hotwire3_view_part_iii_upd ON hotwire3."10_View/Roles/Part_III_Students";

CREATE RULE hotwire3_view_part_iii_upd AS ON UPDATE TO hotwire3. "10_View/Roles/Part_III_Students"
    DO INSTEAD
    UPDATE part_iii_studentship
    SET project_title = new.project_title_html,
    person_id = new.person_id,
    supervisor_id = new.supervisor_id,
    intended_end_date = new.intended_end_date,
    end_date = new.end_date,
    start_date = new.start_date,
    notes = new.notes::character varying( 80),
force_role_status_to_past = new.force_role_status_to_past
WHERE
    part_iii_studentship.id = old.id;

