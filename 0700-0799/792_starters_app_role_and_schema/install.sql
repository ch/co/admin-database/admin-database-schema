SET ROLE dev;
-- not idempotent
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'starters_registration') THEN

      CREATE ROLE starters_registration LOGIN;
   END IF;
END
$do$;

SET ROLE slony;
CREATE SCHEMA registration;
ALTER SCHEMA registration OWNER to dev;
GRANT USAGE on SCHEMA registration to PUBLIC;
GRANT USAGE on SCHEMA registration to _pgbackup;
