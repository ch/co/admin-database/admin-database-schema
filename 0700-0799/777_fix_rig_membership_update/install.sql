CREATE OR REPLACE FUNCTION hotwire3.primary_rig_update(
    newval bigint[],
    oldval bigint[],
    myid bigint)
  RETURNS void AS
$BODY$
declare
 addarr bigint[];
 delarr bigint[];
 addid bigint;
begin
 addarr = fn_array_diff_left(newval,oldval);
 delarr = fn_array_diff_right(newval,oldval);
 -- remove old values
 if fn_array_count(delarr)>0 then
  execute 'update mm_member_research_interest_group set is_primary = false where "research_interest_group_id"='||myid||' AND "member_id" in ('||array_to_string(delarr,',')||')';
 end if;
 -- add new values
 if fn_array_count(addarr)>0 then
  for i in 1 .. array_upper(addarr,1)
  loop
      execute 'update mm_member_research_interest_group set is_primary = false where member_id = $1' using addarr[i] ;
      begin
          execute $$insert into mm_member_research_interest_group ( member_id, research_interest_group_id, is_primary ) values ($1, $2, 't')$$ using addarr[i], myid;
      exception
          when unique_violation then
          execute 'update mm_member_research_interest_group set is_primary = true where member_id = $1 and research_interest_group_id = $2' using addarr[i], myid ;
      end;
  end loop;
 end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.primary_rig_update(bigint[], bigint[], bigint)
  OWNER TO dev;

