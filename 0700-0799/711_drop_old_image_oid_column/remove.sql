ALTER TABLE public.person ADD COLUMN image_oid bigint;
ALTER TABLE public.person ALTER COLUMN image_oid SET DEFAULT 3977791;
UPDATE person SET image_oid = image_lo::bigint;
