REVOKE ALL ON user_account FROM ad_accounts;
GRANT SELECT(id,username,uid,person_id,gecos,is_disabled,expiry_date) ON user_account TO ad_accounts;
GRANT UPDATE(uid,person_id,gecos,is_disabled,expiry_date) ON user_account TO ad_accounts;
GRANT INSERT(id,username,uid,person_id,gecos,is_disabled,expiry_date,chemnet_token) ON user_account TO ad_accounts;
GRANT USAGE ON user_account_id_seq TO ad_accounts;
