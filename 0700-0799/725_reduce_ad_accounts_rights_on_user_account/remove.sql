REVOKE USAGE ON user_account_id_seq FROM ad_accounts;
REVOKE ALL ON TABLE public.user_account FROM ad_accounts;
GRANT ALL ON TABLE public.user_account TO ad_accounts;
