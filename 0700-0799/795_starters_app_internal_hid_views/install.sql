CREATE VIEW registration.title_hid AS
SELECT
    title_id as id,
    long_title as hid
FROM public.title_hid
WHERE long_title not like 'The Late%';

ALTER VIEW registration.title_hid OWNER TO dev;
GRANT SELECT ON registration.title_hid TO starters_registration;

CREATE VIEW registration.room_hid AS
SELECT
    id as id,
    ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48) as hid
FROM public.room
LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
LEFT JOIN building_region_hid ON building_region_hid.building_region_id = room.building_region_id
LEFT JOIN building_hid ON building_hid.building_id = room.building_id
WHERE name <> 'None'
AND
building_hid <> 'West Cambridge Data Centre' and building_hid <> 'SPRI' and building_hid <> 'Engineering';
;

ALTER VIEW registration.room_hid OWNER TO dev;
GRANT SELECT ON registration.room_hid TO starters_registration;

CREATE VIEW registration.phone_hid AS
SELECT
    id as id,
    extension_number as hid
FROM public.dept_telephone_number;

ALTER VIEW registration.phone_hid OWNER TO dev;
GRANT SELECT ON registration.phone_hid TO starters_registration;

CREATE VIEW registration.college_hid AS
SELECT
    id as id,
    name as hid
FROM public.cambridge_college
ORDER BY name;

ALTER VIEW registration.college_hid OWNER TO dev;
GRANT SELECT ON registration.college_hid TO starters_registration;

CREATE VIEW registration.supervisor_hid AS
SELECT DISTINCT
    person.id as id,
    person.surname,
    coalesce(person.known_as,person.first_names) as first_names,
    ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS hid
FROM public.person
LEFT JOIN title_hid ON person.title_id = title_hid.title_id
JOIN _physical_status_v3 on person.id = _physical_status_v3.person_id
JOIN post_history ON person.id = post_history.person_id
WHERE (_physical_status_v3.status_id = 'Current' OR _physical_status_v3.status_id = 'Future') -- allow for incoming PIs
      AND ( post_history.force_role_status_to_past is null OR post_history.force_role_status_to_past = 'f') AND ( post_history.end_date is null or post_history.end_date > current_date ) 
ORDER BY surname, first_names
;

ALTER VIEW registration.supervisor_hid OWNER TO dev;
GRANT SELECT ON registration.supervisor_hid TO starters_registration;

CREATE VIEW registration.nationality_hid AS
SELECT
    id AS id,
    nationality AS hid
FROM public.nationality
ORDER BY nationality;

ALTER VIEW registration.nationality_hid OWNER TO dev;
GRANT SELECT ON registration.nationality_hid TO starters_registration;
