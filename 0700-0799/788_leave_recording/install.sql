CREATE OR REPLACE VIEW leave_recording.person AS 
 SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    COALESCE(person.known_as, person.first_names)::text AS person_friendly_name
  FROM
    person;

ALTER TABLE leave_recording.person
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.person TO dev;
GRANT SELECT ON TABLE leave_recording.person TO www_leave_reporting;


CREATE OR REPLACE VIEW leave_recording.role AS
  SELECT
    CASE
      WHEN (staff_category.category = 'Assistant staff'::text) THEN 'leave_recording\assistant_staff_Role'::text
      ELSE 'leave_recording\database\Role'::text
    END AS role_class_name,
    post_history.id as post_id,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    post_history.start_date,
    post_history.end_date,
    post_history.intended_end_date,
            ( SELECT min(closest_date.end_date) AS min
                   FROM ( SELECT post_history_1.id,
                            post_history_1.end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.intended_end_date AS end_date
                           FROM public.post_history post_history_1
                        UNION
                         SELECT post_history_1.id,
                            post_history_1.funding_end_date AS end_date
                           FROM public.post_history post_history_1
                          GROUP BY post_history_1.id) closest_date
                  WHERE (closest_date.id = post_history.id)) AS estimated_leaving_date,
    CASE
        WHEN person.continuous_employment_start_date <= post_history.start_date THEN person.continuous_employment_start_date
        ELSE NULL -- we don't record historical values
    END AS continuous_employment_start_date_as_at_start_of_role,
    post_history.hours_worked,
    post_history.job_title,
    staff_category.category AS post_category,
    post_history.percentage_of_fulltime_hours,
    post_history.percentage_of_fulltime_days,
    post_history.weekends_are_standard_working_days,
    post_history.works_flexitime,
    post_history.standard_flexileave_rules_apply,
    post_history.departmental_closures_apply,
    CASE
        WHEN (post_history.force_role_status_to_past = true) THEN 'Past'::character varying(10)
        WHEN (post_history.end_date < ('now'::text)::date) THEN 'Past'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.end_date IS NULL)) THEN 'Current'::character varying(10)
        WHEN ((post_history.start_date <= ('now'::text)::date) AND (post_history.intended_end_date >= ('now'::text)::date)) THEN 'Current'::character varying(10)
        WHEN (post_history.start_date > ('now'::text)::date) THEN 'Future'::character varying(10)
        ELSE 'Unknown'::character varying(10)
    END AS status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name
 FROM public.post_history
 LEFT JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))
 JOIN (SELECT
    person.id AS line_manager_id,
    person.crsid AS line_manager_crsid,
    person.email_address AS line_manager_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS line_manager_name
  FROM
    person) line_manager ON line_manager.line_manager_id = post_history.supervisor_id
  JOIN (SELECT
    person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
    person.surname AS person_surname,
    person.continuous_employment_start_date
  FROM
    person) person ON person.person_id = post_history.person_id ORDER BY person_surname, person_name, status ASC;

ALTER TABLE leave_recording.role
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role TO dev;
GRANT SELECT ON TABLE leave_recording.role TO www_leave_reporting;


CREATE TABLE leave_recording.leave_type
(
  id serial NOT NULL PRIMARY KEY,
  leave_type varchar(80) NOT NULL,
  overseer_only BOOLEAN NOT NULL DEFAULT false,
  count_as_leave_type INT NULL -- if it should come under a different leave type, set the parent type here, otherwise leave as null to count on own merits
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.leave_type
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.leave_type TO dev;
GRANT SELECT ON TABLE leave_recording.leave_type TO www_leave_reporting;


INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (1, 'Annual leave', false, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (2, 'Flexi-leave', false, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (3, 'Sick leave', true, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (4, 'Maternity leave', true, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (5, 'Paternity leave', true, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (6, 'Shared parental leave', true, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (7, 'Special leave', true, NULL);
INSERT INTO leave_recording.leave_type (id, leave_type, overseer_only, count_as_leave_type) VALUES (8, 'Department closure', true, 1);


CREATE OR REPLACE VIEW leave_recording.viewable_leave_type AS
  SELECT
    id,
    CASE
    WHEN count_as_leave_type IS NULL THEN
      leave_type
    ELSE
      (SELECT leave_type FROM leave_recording.leave_type subquery WHERE subquery.id=leave_type.count_as_leave_type)
    END AS viewable_desc,
    CASE
    WHEN count_as_leave_type IS NULL THEN
      id
    ELSE
      count_as_leave_type
    END AS effective_leave_type_id
  FROM
    leave_recording.leave_type;
ALTER TABLE leave_recording.viewable_leave_type
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.viewable_leave_type TO dev;
GRANT SELECT ON TABLE leave_recording.viewable_leave_type TO www_leave_reporting;



-- pending leave requests are stored in condensed form
-- i.e. date_from -- date_to
-- whilst standing leave requests have one row per date, since that makes it easier to alter them
-- pending leave requests should be cancelled and resubmitted in order to alter them

CREATE TABLE leave_recording.pending_leave_request
(
  id bigserial NOT NULL PRIMARY KEY, -- GENERATED ALWAYS AS IDENTITY : not supported by our ancient version of postgres
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  leave_year varchar(10), -- e.g. "2019" or "2019/2020", to speed up lookups compared to calculating the leave year of every date_from! FIXME: maybe not needed since we index on start_date and end_date and don't currently use this field in the query. currently only used as a convenient link target in the drupal front end, but could easily make this dynamic
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,  -- easier to set to same as start date upon row insertion, rather than interpreting all queries accordingly
  leave_type INT NOT NULL REFERENCES leave_recording.leave_type(id),
  notes TEXT NULL,
  half_day_start_date BOOLEAN NOT NULL DEFAULT FALSE, -- if start_date == end_datn then AM off is encoded in half_day_end_date, while PM off is encoded in half_day_start_date
  half_day_end_date BOOLEAN NOT NULL DEFAULT FALSE,
  audit_id_requested BIGINT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.pending_leave_request
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.pending_leave_request TO dev;
GRANT SELECT, INSERT, DELETE ON TABLE leave_recording.pending_leave_request TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.pending_leave_request_id_seq TO www_leave_reporting;

CREATE INDEX pending_leave_index ON leave_recording.pending_leave_request (post_id, start_date, end_date);


CREATE TABLE leave_recording.standing_leave
(
  id bigserial NOT NULL PRIMARY KEY,
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  leave_date DATE NOT NULL,
  leave_type INT NOT NULL REFERENCES leave_recording.leave_type(id),
  notes TEXT NULL,
  am_off BOOLEAN NOT NULL DEFAULT TRUE, -- if the whole day is off, then am_off and pm_off are both TRUE
  pm_off BOOLEAN NOT NULL DEFAULT TRUE,
  audit_id_requested BIGINT NOT NULL,  -- mostly to allow keeping track of "block" bookings even after converting to one record per date. in particular, can use to GROUP BY. also to make is easier to see who requested it. -- REFERENCES leave_recording.leave_audit(id)
  audit_id_granted BIGINT NOT NULL,  -- to make it easier to see who granted it -- REFERENCES leave_recording.leave_audit(id)
  CONSTRAINT day_has_leave CHECK (am_off OR pm_off)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.standing_leave
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.standing_leave TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE leave_recording.standing_leave TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.standing_leave_id_seq TO www_leave_reporting;

CREATE INDEX standing_leave_index ON leave_recording.standing_leave (post_id, leave_date);


CREATE TABLE leave_recording.leave_audit
(
  id bigserial NOT NULL PRIMARY KEY,
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  leave_type INT NOT NULL REFERENCES leave_recording.leave_type(id),
  notes TEXT NULL,
  half_day_start_date BOOLEAN NOT NULL DEFAULT FALSE, -- if start_date == end_date then AM off is encoded in half_day_end_date, while PM off is encoded in half_day_start_date
  half_day_end_date BOOLEAN NOT NULL DEFAULT FALSE,
  action varchar(80) NOT NULL,  -- e.g. request, approve, refuse, cancel, rescind, force set
  actor BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself
  action_notes TEXT NULL,
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  ref_audit_id BIGINT  -- to make it easier to cross-reference to see the audit trail for a particular entity -- REFERENCES leave_recording.leave_audit(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.leave_audit
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.leave_audit TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.leave_audit TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.leave_audit_id_seq TO www_leave_reporting;

CREATE INDEX post_id_index ON leave_recording.leave_audit (post_id);


CREATE TABLE leave_recording.delegated_approver
(
  id bigserial NOT NULL PRIMARY KEY,
  line_manager_person_id bigint NOT NULL REFERENCES public.person(id),
  delegated_approver_person_id bigint NOT NULL REFERENCES public.person(id),
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  audit_id_added BIGINT NOT NULL -- REFERENCES leave_recording.delegated_approver_audit(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.delegated_approver
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.delegated_approver TO dev;
GRANT SELECT, INSERT, DELETE ON TABLE leave_recording.delegated_approver TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.delegated_approver_id_seq TO www_leave_reporting;

CREATE INDEX line_manager_person_id_index ON leave_recording.delegated_approver (line_manager_person_id);
CREATE INDEX delegated_approver_person_id_index ON leave_recording.delegated_approver (delegated_approver_person_id);
CREATE INDEX line_manager_based_index ON leave_recording.delegated_approver (line_manager_person_id, start_date, end_date);
CREATE INDEX delegated_approver_based_index ON leave_recording.delegated_approver (delegated_approver_person_id, start_date, end_date);


CREATE OR REPLACE VIEW leave_recording.delegated_approver_view AS
  SELECT
    delegated_approver.id,
    delegated_approver.line_manager_person_id,
    line_manager_person.person_name as line_manager_person_name,
    line_manager_person.person_crsid as line_manager_person_crsid,
    line_manager_person.person_email as line_manager_person_email,
    delegated_approver.delegated_approver_person_id,
    delegated_approver_person.person_name as delegated_approver_person_name,
    delegated_approver_person.person_crsid as delegated_approver_person_crsid,
    delegated_approver_person.person_email as delegated_approver_person_email,
    delegated_approver.start_date AS start_date,
    delegated_approver.end_date AS end_date,
    delegated_approver.audit_id_added
  FROM
    leave_recording.delegated_approver
  JOIN
    (SELECT
       person.person_id,
       person.person_crsid,
       person.person_email,
       person.person_name,
       person.person_friendly_name
     FROM
       leave_recording.person) line_manager_person
     ON line_manager_person.person_id = delegated_approver.line_manager_person_id
  JOIN
    (SELECT
       person.person_id,
       person.person_crsid,
       person.person_email,
       person.person_name,
       person.person_friendly_name
     FROM
       leave_recording.person) delegated_approver_person
     ON delegated_approver_person.person_id = delegated_approver.delegated_approver_person_id
  ;
ALTER TABLE leave_recording.delegated_approver_view
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.delegated_approver_view TO dev;
GRANT SELECT ON TABLE leave_recording.delegated_approver_view TO www_leave_reporting;


CREATE TABLE leave_recording.delegated_approver_audit
(
  id bigserial NOT NULL PRIMARY KEY,
  line_manager_person_id bigint NOT NULL REFERENCES public.person(id),
  delegated_approver_person_id bigint NOT NULL REFERENCES public.person(id),
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  action varchar(80) NOT NULL,  -- e.g. "appoint", "revoke", "force appoint", "force revoke", "decline"
  actor BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  ref_audit_id BIGINT NULL -- REFERENCES leave_recording.delegated_approver_audit(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.delegated_approver_audit
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.delegated_approver_audit TO dev;
GRANT SELECT, INSERT, UPDATE ON TABLE leave_recording.delegated_approver_audit TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.delegated_approver_audit_id_seq TO www_leave_reporting;

CREATE INDEX line_manager_index ON leave_recording.delegated_approver_audit (line_manager_person_id);


CREATE OR REPLACE VIEW leave_recording.staff_category AS 
 SELECT staff_category.id,
    staff_category.category
   FROM staff_category;

ALTER TABLE leave_recording.staff_category
  OWNER TO dev;

GRANT SELECT ON TABLE leave_recording.staff_category TO www_leave_reporting;


CREATE TABLE leave_recording.category_based_leave
(
  id bigserial NOT NULL PRIMARY KEY,
  staff_category_id bigint NOT NULL REFERENCES public.staff_category(id),
  leave_date DATE NOT NULL,
  leave_type INT NOT NULL REFERENCES leave_recording.leave_type(id),
  notes TEXT NULL,
  am_off BOOLEAN NOT NULL DEFAULT TRUE, -- if the whole day is off, then am_off and pm_off are both TRUE
  pm_off BOOLEAN NOT NULL DEFAULT TRUE,
  category_based_audit_id BIGINT NOT NULL,  -- to make it easier to see who granted it -- REFERENCES leave_recording.category_based_leave_audit(id)
  CONSTRAINT day_has_leave CHECK (am_off OR pm_off)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.category_based_leave
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.category_based_leave TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE leave_recording.category_based_leave TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.category_based_leave_id_seq TO www_leave_reporting;

CREATE INDEX category_based_leave_index ON leave_recording.category_based_leave (staff_category_id, leave_date);


CREATE OR REPLACE VIEW leave_recording.effective_category_based_leave AS
  SELECT category_based_leave.*, viewable_leave_type.effective_leave_type_id, viewable_leave_type.viewable_desc AS effective_leave_type_desc
  FROM leave_recording.category_based_leave
  JOIN leave_recording.viewable_leave_type
  ON category_based_leave.leave_type = viewable_leave_type.id
  ;
ALTER TABLE leave_recording.effective_category_based_leave
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.effective_category_based_leave TO dev;
GRANT SELECT ON TABLE leave_recording.effective_category_based_leave TO www_leave_reporting;


CREATE TABLE leave_recording.category_based_leave_audit
(
  id bigserial NOT NULL PRIMARY KEY,
  staff_category_id bigint NOT NULL REFERENCES public.staff_category(id),
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  leave_type INT NOT NULL REFERENCES leave_recording.leave_type(id),
  notes TEXT NULL,
  half_day_start_date BOOLEAN NOT NULL DEFAULT FALSE, -- if start_date == end_date then AM off is encoded in half_day_end_date, while PM off is encoded in half_day_start_date
  half_day_end_date BOOLEAN NOT NULL DEFAULT FALSE,
  action varchar(80) NOT NULL,  -- e.g. request, approve, refuse, cancel, rescind, force set
  actor BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself
  action_notes TEXT NULL,
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now(),
  ref_audit_id BIGINT  -- to make it easier to cross-reference to see the audit trail for a particular entity -- REFERENCES leave_recording.category_based_leave_audit(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.category_based_leave_audit
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.category_based_leave_audit TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.category_based_leave_audit TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.category_based_leave_audit_id_seq TO www_leave_reporting;


CREATE TABLE leave_recording.role_enrol
(
  id bigserial NOT NULL PRIMARY KEY,
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  leave_year varchar(10), -- e.g. "2019" or "2019/2020"
  enrol_all_future_leave_years BOOLEAN NOT NULL DEFAULT TRUE,
  notes TEXT NULL,
  audit_id_enrolled BIGINT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.role_enrol
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_enrol TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.role_enrol TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.role_enrol_id_seq TO www_leave_reporting;


CREATE TABLE leave_recording.role_enrol_audit
(
  id bigserial NOT NULL PRIMARY KEY,
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  leave_year varchar(10), -- e.g. "2019" or "2019/2020"
  enrol_all_future_leave_years BOOLEAN NOT NULL DEFAULT TRUE,
  action varchar(80) NOT NULL,  -- e.g. request, approve, refuse, cancel, rescind, force set
  actor BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself
  action_notes TEXT NULL,
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.role_enrol_audit
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_enrol_audit TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.role_enrol_audit TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.role_enrol_audit_id_seq TO www_leave_reporting;
