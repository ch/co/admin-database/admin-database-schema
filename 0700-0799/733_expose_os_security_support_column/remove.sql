DROP VIEW hotwire3."10_View/Computers/Operating_Systems";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Operating_Systems" AS 
 SELECT operating_system.id,
    operating_system.os,
    operating_system.nmap_identifies_as,
    operating_system.path_to_autoinstaller,
    operating_system.default_key
   FROM operating_system;

ALTER TABLE hotwire3."10_View/Computers/Operating_Systems"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Operating_Systems" TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/Computers/Operating_Systems" TO cos;

CREATE OR REPLACE RULE hotwire3_view_edit_operating_systems_ins AS
    ON INSERT TO hotwire3."10_View/Computers/Operating_Systems" DO INSTEAD  INSERT INTO operating_system (os, nmap_identifies_as, path_to_autoinstaller, default_key)
  VALUES (new.os, new.nmap_identifies_as, new.path_to_autoinstaller, new.default_key)
  RETURNING operating_system.id,
    operating_system.os,
    operating_system.nmap_identifies_as,
    operating_system.path_to_autoinstaller,
    operating_system.default_key;

CREATE OR REPLACE RULE hotwire3_view_edit_operating_systems_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Operating_Systems" DO INSTEAD  UPDATE operating_system SET os = new.os, nmap_identifies_as = new.nmap_identifies_as, path_to_autoinstaller = new.path_to_autoinstaller, default_key = new.default_key
  WHERE operating_system.id = old.id;


