REVOKE ALL ON user_account FROM ad_accounts;
GRANT SELECT(id,username,uid,person_id,gecos,is_disabled,expiry_date,description) ON user_account TO ad_accounts;
GRANT UPDATE(uid,person_id,gecos,is_disabled,expiry_date,description) ON user_account TO ad_accounts;
GRANT INSERT(id,username,uid,person_id,gecos,is_disabled,expiry_date,chemnet_token,description) ON user_account TO ad_accounts;
