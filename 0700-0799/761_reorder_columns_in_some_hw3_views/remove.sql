DROP VIEW hotwire3."10_View/Computers/System_Instances";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Instances" AS 
 SELECT system_image.id,
    system_image.hardware_id AS hardware_pk,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    ARRAY( SELECT mm_dom0_domu.dom0_id
           FROM mm_dom0_domu
          WHERE mm_dom0_domu.domu_id = system_image.id) AS host_system_image_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    hardware.date_purchased AS date_hardware_purchased,
    hardware.warranty_end_date AS hardware_warranty_end_date,
    system_image.hobbit_tier,
    system_image.hobbit_flags,
    system_image.is_development_system,
    system_image.expiry_date AS system_image_expiry_date,
    hardware.delete_after_date AS hardware_delete_after_date,
    system_image.is_managed_mac,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/System_Instances"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/System_Instances" TO cos;

-- Rule: hotwire3_view_system_image_all_del ON hotwire3."10_View/Computers/System_Instances"

-- DROP RULE hotwire3_view_system_image_all_del ON hotwire3."10_View/Computers/System_Instances";

CREATE OR REPLACE RULE hotwire3_view_system_image_all_del AS
    ON DELETE TO hotwire3."10_View/Computers/System_Instances" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;


-- Trigger: hotwire3_view_system_image_all_ins on hotwire3."10_View/Computers/System_Instances"

-- DROP TRIGGER hotwire3_view_system_image_all_ins ON hotwire3."10_View/Computers/System_Instances";

CREATE TRIGGER hotwire3_view_system_image_all_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Computers/System_Instances"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.system_image_all_upd();

-- Trigger: hotwire3_view_system_image_all_upd on hotwire3."10_View/Computers/System_Instances"

-- DROP TRIGGER hotwire3_view_system_image_all_upd ON hotwire3."10_View/Computers/System_Instances";

CREATE TRIGGER hotwire3_view_system_image_all_upd
  INSTEAD OF UPDATE
  ON hotwire3."10_View/Computers/System_Instances"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.system_image_all_upd();


DROP VIEW hotwire3."10_View/Computers/Network_Bans";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Network_Bans" AS 
 SELECT system_image.id,
    system_image.hardware_id AS hardware_pk,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    system_image.is_managed_mac,
    system_image.was_managed_machine_on AS ro_was_managed_machine_on,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/Network_Bans"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Network_Bans" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Network_Bans" TO cos;

-- Trigger: computers_network_bans_upd on hotwire3."10_View/Computers/Network_Bans"

-- DROP TRIGGER computers_network_bans_upd ON hotwire3."10_View/Computers/Network_Bans";

CREATE TRIGGER computers_network_bans_upd
  INSTEAD OF UPDATE
  ON hotwire3."10_View/Computers/Network_Bans"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.system_image_bans_upd();


DROP VIEW hotwire3."10_View/Computers/Printers";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Printers" AS 
 SELECT hardware.id,
    printer.id AS printer_pk,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.date_purchased,
    hardware.serial_number,
    hardware.asset_tag,
    hardware.date_configured,
    hardware.warranty_end_date,
    hardware.date_decommissioned,
    hardware.warranty_details,
    hardware.room_id,
    hardware.owner_id,
    printer.printer_class_id,
    printer.cups_options,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_System_Instances_ro'::character varying, 'hardware_id'::character varying, '10_View/Computers/System_Instances'::character varying, 'system_image_id'::character varying, 'id'::character varying) AS "System_Instances"
   FROM printer
     JOIN hardware ON printer.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Computers/Printers"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Printers" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/Printers" TO cos;

-- Rule: hotwire3_view_printers_del ON hotwire3."10_View/Computers/Printers"

-- DROP RULE hotwire3_view_printers_del ON hotwire3."10_View/Computers/Printers";

CREATE OR REPLACE RULE hotwire3_view_printers_del AS
    ON DELETE TO hotwire3."10_View/Computers/Printers" DO INSTEAD ( DELETE FROM printer
  WHERE printer.id = old.printer_pk;
 DELETE FROM hardware
  WHERE hardware.id = old.id;
);

-- create trigger
CREATE TRIGGER hotwire3_view_printers_upd INSTEAD OF UPDATE ON hotwire3."10_View/Computers/Printers" FOR EACH ROW EXECUTE PROCEDURE hotwire3.printers_upd();
DROP VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs" AS 
 SELECT system_image.id,
    system_image.hardware_id AS hardware_pk,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    system_image.architecture_id,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id,
    system_image.reinstall_on_next_boot,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    system_image.netboot_id,
        CASE
            WHEN system_image.reinstall_on_next_boot = true THEN true
            WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
            WHEN system_image.is_managed_mac = true THEN true
            ELSE false
        END AS ro_is_managed_machine,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN research_group ON system_image.research_group_id = research_group.id;

ALTER TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs" TO cos;

CREATE TRIGGER hotwire3_ai_for_cos_upd INSTEAD OF UPDATE ON hotwire3."10_View/Computers/Autoinstallation_for_COs" FOR EACH ROW EXECUTE PROCEDURE hotwire3.autoinstallation_for_cos_upd();
