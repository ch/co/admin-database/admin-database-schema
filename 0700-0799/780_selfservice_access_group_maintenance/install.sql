SET ROLE dev;
-- not idempotent
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'selfservice_access') THEN

      CREATE ROLE selfservice_access NOLOGIN;
   END IF;
END
$do$;

CREATE FUNCTION public.update_selfservice_access_role() RETURNS void AS
$body$
DECLARE
    username varchar;
BEGIN
    -- people without database roles
    FOR username IN
        SELECT DISTINCT crsid 
        FROM person
        LEFT JOIN pg_roles ON person.crsid=rolname
        LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
        WHERE crsid IS NOT NULL AND rolname IS NULL AND _physical_status_v2.status_id <> 'Past'
    LOOP
        -- create role
        RAISE NOTICE 'Creating role for %', username;
        EXECUTE format('CREATE ROLE %I LOGIN IN ROLE selfservice_access',username);
    END LOOP;
    -- people with roles but not in the group
    FOR username IN
        SELECT DISTINCT crsid
        FROM person
        LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
        WHERE (
            SELECT count(*)
            FROM pg_auth_members
            JOIN pg_roles u ON member=u.oid
            JOIN pg_roles g ON roleid=g.oid
            WHERE u.rolname=crsid AND g.rolname='selfservice_access'
        )=0 AND crsid IS NOT NULL AND _physical_status_v2.status_id <> 'Past'
    LOOP
        -- put into group
        RAISE NOTICE 'Putting % in group', username;
        EXECUTE format('GRANT selfservice_access TO %I',username);
    END LOOP;

END;
$body$ LANGUAGE plpgsql;

ALTER FUNCTION public.update_selfservice_access_role() OWNER TO dev;

CREATE OR REPLACE FUNCTION public.fn_createuser(_crsid varchar) RETURNS void AS
$BODY$
begin
 -- create postgres user account
  perform usename from pg_catalog.pg_user where usename=_crsid;
  if not found then
   execute format('create user %I with in role _autoadded, selfservice_access',_crsid); 
  end if;
 -- Create entry in user_account table
  if _crsid is not null then
   perform id from public.user_account where username=_crsid;
   if not found then
    insert into public.user_account (person_id,username,chemnet_token) values ((select id from public.person where crsid=_crsid),_crsid,public.random_string_lower(16));
   end if;
  end if; 
 end
$BODY$ LANGUAGE plpgsql SECURITY DEFINER
    SET search_path = public,pg_temp;

ALTER FUNCTION fn_createuser(character varying) OWNER TO useradder;
COMMENT ON FUNCTION fn_createuser(character varying) IS 'A Privileged function which ensures that a postgres account exists for the user.';
