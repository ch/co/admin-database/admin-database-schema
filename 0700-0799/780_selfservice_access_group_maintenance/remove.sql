DROP FUNCTION public.update_selfservice_access_role();

CREATE OR REPLACE FUNCTION fn_createuser(_crsid character varying)
  RETURNS void AS
$BODY$
declare

begin
 -- create postgres user account
  perform usename from pg_catalog.pg_user where usename=_crsid;
  if not found then
   execute 'create user '||_crsid||' with in group _autoadded'; 
  end if;
 -- Create entry in user_account table
  if _crsid is not null then
   perform id from public.user_account where username=_crsid;
   if not found then
    insert into public.user_account (person_id,username,chemnet_token) values ((select id from public.person where crsid=_crsid),_crsid,public.random_string_lower(16));
   end if;
  end if; 
 end
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER 
  COST 100
  SET search_path = public,pg_temp;
ALTER FUNCTION fn_createuser(character varying) OWNER TO useradder;
COMMENT ON FUNCTION fn_createuser(character varying) IS 'A Privileged function which ensures that a postgres account exists for the user.';

