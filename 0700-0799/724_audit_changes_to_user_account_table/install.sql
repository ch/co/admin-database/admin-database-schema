CREATE FUNCTION audit_user_account() RETURNS TRIGGER AS
$$
# This is a lightly edited version of the source for audit_generic()
    my $rv;
    my @olddata;
    my @newdata;
    my $old;
    my $new;
    my $sql = <<EOSQL;

      insert into _audit ( operation, stamp, username, tablename, oldcols, newcols, id ) values ( 
        '$_TD->{event}',
                now(),
        current_user,
        '$_TD->{relname}',

EOSQL

    # find out who we are running as
    my $whoami = 'select current_user';
    $rv = spi_exec_query($whoami);
    my $username = $rv->{rows}[0]->{current_user};

    # fill in oldcols
    if ($_TD->{event} eq 'INSERT')  {
          $sql .= 'NULL, ';
    } elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $old = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{old}})) {
            if ($col ne 'chemnet_token') {
                # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{new}{$col}) {
                        push @olddata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @olddata, $col . ':' . $val ;
                }
                unless (!@olddata) {
                    $old = join("\n",sort @olddata);
                }
            } elsif ($val ne $_TD->{new}{'chemnet_token'}) {
                    push @olddata, $col . ': redacted' ;
            }
        }
        $sql .= quote_nullable($old) . ', ';
    }

    # fill in newcols
    if ($_TD->{event} eq 'DELETE')  {
        $sql .= 'NULL, ';
    }
    elsif ($username eq 'gdpr_purge') {
        $sql .= quote_literal('GDPR purging: data redacted') . ', ';
        $new = 'redacted';
    } else {
        while(($col, $val)=each(%{$_TD->{new}})) {
            if ($col ne 'chemnet_token') {
                # If the table has array columns this comparison can throw an exception
                eval {
                    if ($val ne $_TD->{old}{$col}) {
                        push @newdata, $col . ':' . $val ;
                    }
                };
                if ($@) {
                    push @newdata, $col . ':' . $val;
                }
                unless (!@newdata) {
                        $new = join("\n",sort @newdata);
                }
            } elsif ($val ne $_TD->{old}{'chemnet_token'}) {
                    push @newdata, $col . ': redacted' ;
            }
        }
        $sql .= quote_nullable($new) . ', ';
    }

    # fill in primary key
    my $long_pk = $_TD->{table_name} . '_id';
    my $pk_col;
    # look for a primary key field in either of our standard forms
    # Have to do it like this because when we are updating views the user may not have
    # sufficient rights to look at the information schema on the underlying table. 
    foreach (keys(%{$_TD->{new}}), keys(%{$_TD->{old}})) {
        if ($_ eq $long_pk ) {
            $pk_col = $long_pk;
	} elsif ($_ eq 'id')  {
		$pk_col='id';
	}

    }    
    if (defined $pk_col )  {
        if ($_TD->{event} eq 'DELETE') {
            $sql .= quote_literal($_TD->{old}{$pk_col}) . ' )';
        } else {
            $sql .= quote_literal($_TD->{new}{$pk_col}) . ' )';
        }
    } else {
        $sql .= '0' . ')';
    }

    # for debugging
    #elog(NOTICE,$sql);

    # if anything changed, write to the audit log
    if (defined $old or defined $new) {
        $rv = spi_exec_query($sql);
        unless ($rv->{status} eq 'SPI_OK_INSERT') {
            elog(NOTICE,"trigger status: $rv->{status}");
            return 'SKIP'
        }
    }
    return undef; 
    
$$ LANGUAGE plperl;

ALTER FUNCTION audit_user_account() OWNER TO dev;

CREATE TRIGGER audit AFTER INSERT OR UPDATE OR DELETE ON public.user_account FOR EACH ROW EXECUTE PROCEDURE audit_user_account();
