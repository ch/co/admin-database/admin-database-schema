DROP VIEW hotwire3.computer_rep_hid;
CREATE VIEW hotwire3.computer_rep_hid AS 
 SELECT person_hid.person_id AS computer_rep_id,
    person_hid.person_hid AS computer_rep_hid
   FROM hotwire3.person_hid;

ALTER TABLE hotwire3.computer_rep_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.computer_rep_hid TO dev;
GRANT SELECT ON TABLE hotwire3.computer_rep_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.computer_rep_hid TO groupitreps;
GRANT SELECT ON TABLE hotwire3.computer_rep_hid TO headsofgroup;
