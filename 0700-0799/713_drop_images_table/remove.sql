CREATE TABLE public.images
(
  person_id bigint,
  image_data oid,
  CONSTRAINT images_person_id_fkey FOREIGN KEY (person_id)
      REFERENCES public.person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.images
  OWNER TO postgres;
