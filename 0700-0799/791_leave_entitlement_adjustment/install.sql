CREATE TABLE leave_recording.role_entitlement_adjustment -- N.B. this is a self-auditing table
(
  id bigserial NOT NULL PRIMARY KEY,
  post_id bigint NOT NULL REFERENCES public.post_history(id),
  leave_type INT NULL REFERENCES leave_recording.leave_type(id), -- can be NULL if it applies to all leave types
  leave_year varchar(10), -- e.g. "2019" or "2019/2020"
  affect_all_future_leave_years BOOLEAN NOT NULL DEFAULT TRUE,
  factor_existing REAL NOT NULL DEFAULT 1.0,  -- e.g. 1.0 to preserve existing entitlement, 0.0 to reset it, anything else as necessary
  delta varchar(10) NOT NULL,  -- how many days to add or subtract from the leave entitlement after applying the above factor. can be 0 if the only change is to apply factor_existing. can be fractional, or negative. easier to set as a character type than worrying about precision etc.
  reason TEXT NOT NULL,  -- reason for the change in leave entitlement, shown to the affected person and their line manager
  actor BIGINT NULL REFERENCES public.person(id), -- allow NULL in case the action is taken by the system itself
  action_notes TEXT NULL,
  action_when TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT Now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave_recording.role_entitlement_adjustment
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_entitlement_adjustment TO dev;
GRANT SELECT, INSERT ON TABLE leave_recording.role_entitlement_adjustment TO www_leave_reporting;
GRANT USAGE, SELECT ON SEQUENCE leave_recording.role_entitlement_adjustment_id_seq TO www_leave_reporting;


-- DROP VIEW leave_recording.role_entitlement_adjustment_view;
CREATE OR REPLACE VIEW leave_recording.role_entitlement_adjustment_view AS
 SELECT
    role_entitlement_adjustment.*,
    person.person_crsid AS actor_crsid,
    person.person_email AS actor_email,
    person.person_name AS actor_name,
    person.person_friendly_name AS actor_friendly_name
  FROM
    leave_recording.role_entitlement_adjustment
  JOIN leave_recording.person
  ON person.person_id = role_entitlement_adjustment.actor
  ;

ALTER TABLE leave_recording.role_entitlement_adjustment_view
  OWNER TO dev;
GRANT ALL ON TABLE leave_recording.role_entitlement_adjustment_view TO dev;
GRANT SELECT ON TABLE leave_recording.role_entitlement_adjustment_view TO www_leave_reporting;
