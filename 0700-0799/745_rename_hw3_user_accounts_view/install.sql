DROP VIEW hotwire3."10_View/IT_Accounts/User_Accounts";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/IT_Accounts/User_Accounts';

CREATE VIEW hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" AS
SELECT 
    id,
    username,
    owner_id,
    description,
    is_disabled,
    expiry_date,
    ro_owner_banned
FROM (
    SELECT
        user_account.id,
        username,
        person_id as owner_id,
        description,
        is_disabled,
        expiry_date,
        owner_banned as ro_owner_banned
    FROM user_account
    LEFT JOIN person ON person_id = person.id
    WHERE current_user = person.crsid OR pg_has_role('cos','MEMBER')
) accounts_allowed_to_see;

ALTER VIEW hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" TO cos;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/Personal_ChemNet_Accounts" TO public;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/IT_Accounts/Personal_ChemNet_Accounts', 'user_account' );
