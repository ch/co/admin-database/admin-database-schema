DROP VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro";
CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" AS 
 SELECT system_image_by_mac.mac AS id,
    mac_to_vlan.id AS _mac_to_vlan_id,
    system_image_by_mac.id AS system_image_id,
    system_image_by_mac.mac,
    mac_to_vlan.vid,
    vlan.name
   FROM ( SELECT system_image.id,
            system_image.wired_mac_1 AS mac
           FROM system_image
          WHERE system_image.wired_mac_1 IS NOT NULL
        UNION
         SELECT system_image.id,
            system_image.wired_mac_2 AS mac
           FROM system_image
          WHERE system_image.wired_mac_2 IS NOT NULL
        UNION
         SELECT system_image.id,
            system_image.wired_mac_3 AS mac
           FROM system_image
          WHERE system_image.wired_mac_3 IS NOT NULL
        UNION
         SELECT system_image.id,
            system_image.wired_mac_4 AS mac
           FROM system_image
          WHERE system_image.wired_mac_4 IS NOT NULL
        UNION
         SELECT system_image.id,
            system_image.wireless_mac AS mac
           FROM system_image
          WHERE system_image.wireless_mac IS NOT NULL) system_image_by_mac
     LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON system_image_by_mac.mac = mac_to_vlan.mac
     LEFT JOIN vlan USING (vid);

ALTER TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" TO cos;

