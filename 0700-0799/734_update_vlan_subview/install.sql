DROP VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro";

CREATE VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" AS 
WITH system_image_by_mac AS (
    SELECT system_image.id,
            system_image.operating_system_id,
            system_image.wired_mac_1 AS mac
    FROM system_image
    WHERE system_image.wired_mac_1 IS NOT NULL
UNION
     SELECT system_image.id,
            system_image.operating_system_id,
            system_image.wired_mac_2 AS mac
     FROM system_image
     WHERE system_image.wired_mac_2 IS NOT NULL
UNION
     SELECT system_image.id,
            system_image.operating_system_id,
            system_image.wired_mac_3 AS mac
     FROM system_image
     WHERE system_image.wired_mac_3 IS NOT NULL
UNION
    SELECT system_image.id,
           system_image.operating_system_id,
           system_image.wired_mac_4 AS mac
    FROM system_image
    WHERE system_image.wired_mac_4 IS NOT NULL
UNION
    SELECT system_image.id,
           system_image.operating_system_id,
           system_image.wireless_mac AS mac
    FROM system_image
    WHERE system_image.wireless_mac IS NOT NULL
)
SELECT
    macs_and_main_vlans.mac as id,
    macs_and_main_vlans.id as _mac_to_vlan_id,
    macs_and_main_vlans.system_image_id as system_image_id,
    macs_and_main_vlans.mac as mac,
    macs_and_main_vlans.default_vlan_name,
    macs_and_main_vlans.default_vid,
    override_vlan.name as override_name,
    override_vlan.vid as override_vid
FROM (
    SELECT system_image_by_mac.mac AS id,
        system_image_by_mac.id AS system_image_id,
        system_image_by_mac.mac,
        -- default VLAN VID. The array_to_string() wrapping is a hack that filters out NULLs.
        CASE
            WHEN operating_system.has_security_support = 'f' THEN '1106'::varchar
            ELSE array_to_string(array_agg(
                CASE
                WHEN subnet.ignore_vlan = 't' THEN 1
                ELSE subnet_vlan.vid
                END
                ),','
            )
        END AS default_vid,
        -- default VLAN name. The array_to_string() wrapping is a hack that filters out NULLs.
        CASE
            WHEN operating_system.has_security_support = 'f' THEN 'ch-legacyos'::varchar
            ELSE array_to_string(array_agg(
                CASE
                WHEN subnet.ignore_vlan = 't' THEN 'DEFAULT_VLAN'::varchar
                ELSE subnet_vlan.name
                END
                ),','
            )
        END AS default_vlan_name
        FROM system_image_by_mac
        LEFT JOIN operating_system ON system_image_by_mac.operating_system_id = operating_system.id
        LEFT JOIN mm_system_image_ip_address mm ON mm.system_image_id = system_image_by_mac.id
        LEFT JOIN ip_address ON mm.ip_address_id = ip_address.id
        LEFT JOIN subnet ON ip_address.subnet_id = subnet.id
        LEFT JOIN vlan subnet_vlan ON subnet.vlan_id = subnet_vlan.id
        GROUP BY system_image_by_mac.id,system_image_by_mac.mac,operating_system.has_security_support
) macs_and_main_vlans
LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON macs_and_main_vlans.mac = mac_to_vlan.mac
LEFT JOIN vlan override_vlan ON mac_to_vlan.vid = override_vlan.vid
;

ALTER TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" TO cos;

