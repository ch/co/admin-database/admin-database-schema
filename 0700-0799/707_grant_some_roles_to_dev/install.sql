-- It is useful for dev to be a member of these roles so it can create security
-- definer functions owned by them
-- It also needs the ability to temporarily grant schema rights to other roles when
-- setting up security definer functions

GRANT ALL ON SCHEMA PUBLIC TO dev WITH GRANT OPTION;
GRANT mailinglists,useradder,selfservice_updater,osbuilder,ipreg TO dev;
