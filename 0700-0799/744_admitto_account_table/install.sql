CREATE TABLE public.admitto_account
(
  id BIGSERIAL PRIMARY KEY,
  username character varying(32) UNIQUE NOT NULL,
  uid integer UNIQUE,
  description character varying(500),
  gecos character varying(80),
  expiry_date date,
  is_disabled boolean DEFAULT false,
  person_id bigint,
  owner_banned boolean NOT NULL DEFAULT false,
  CONSTRAINT admitto_account_fk_person_id FOREIGN KEY (person_id)
      REFERENCES public.person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE RESTRICT
);


ALTER TABLE public.admitto_account OWNER TO dev;
GRANT ALL ON TABLE public.admitto_account TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE public.admitto_account TO useradder;
COMMENT ON COLUMN public.admitto_account.uid IS 'This is both the uid and the gid of the primary Unix group';
COMMENT ON COLUMN public.admitto_account.gecos IS 'You might think we can default this from the attached Person, but not all user accounts have one.';
COMMENT ON COLUMN public.admitto_account.is_disabled IS 'We need to be able to do an instant account disable and have it stick even if the finishing date has not passed';

GRANT SELECT(id), INSERT(id) ON public.admitto_account TO ad_accounts;
GRANT SELECT(username), INSERT(username) ON public.admitto_account TO ad_accounts;
GRANT SELECT(uid), UPDATE(uid), INSERT(uid) ON public.admitto_account TO ad_accounts;
GRANT SELECT(description), UPDATE(description), INSERT(description) ON public.admitto_account TO ad_accounts;
GRANT SELECT(gecos), UPDATE(gecos), INSERT(gecos) ON public.admitto_account TO ad_accounts;
GRANT SELECT(expiry_date), UPDATE(expiry_date), INSERT(expiry_date) ON public.admitto_account TO ad_accounts;
GRANT SELECT(is_disabled), UPDATE(is_disabled), INSERT(is_disabled) ON public.admitto_account TO ad_accounts;
GRANT SELECT(person_id), UPDATE(person_id), INSERT(person_id) ON public.admitto_account TO ad_accounts;

ALTER SEQUENCE admitto_account_id_seq OWNER TO dev;
GRANT USAGE ON admitto_account_id_seq TO ad_accounts, cos, useradder;

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE
  ON public.admitto_account
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic();
