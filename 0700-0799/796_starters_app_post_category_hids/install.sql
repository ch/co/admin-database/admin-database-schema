ALTER TABLE public.postgraduate_studentship_type ADD COLUMN show_on_registration_form boolean not null default true;
UPDATE public.postgraduate_studentship_type set show_on_registration_form = 'f' where name = 'MSc' or name = 'CPGS';

ALTER TABLE public.visitor_type_hid ADD COLUMN show_on_registration_form boolean not null default false;
ALTER TABLE public.visitor_type_hid ADD COLUMN registration_form_name text ;

UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'Academic' where visitor_type_hid = 'Visitor (Academic)';
UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'PhD Postgraduate' where visitor_type_hid = 'Visitor (PhD Postgraduate)';
UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'MPhil Postgraduate' where visitor_type_hid = 'Visitor (MPhil Postgraduate)';
UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'Postdoc' where visitor_type_hid = 'Visitor (PDRA)';
UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'Student visitor' where visitor_type_hid = 'Visitor (student)';
UPDATE public.visitor_type_hid set show_on_registration_form = 't', registration_form_name = 'Summer Student' where visitor_type_hid = 'Summer Student';

ALTER TABLE public.erasmus_type_hid ADD COLUMN show_on_registration_form boolean not null default 't';
ALTER TABLE public.erasmus_type_hid ADD COLUMN registration_form_name text;
UPDATE public.erasmus_type_hid SET show_on_registration_form = 't', registration_form_name = 'Erasmus exchange student' where erasmus_type_hid = 'Erasmus exchange student';
UPDATE public.erasmus_type_hid SET show_on_registration_form = 't', registration_form_name = 'Erasmus +' where erasmus_type_hid = 'Erasmus +';
UPDATE public.erasmus_type_hid SET show_on_registration_form = 'f' where erasmus_type_hid = 'Erasmus work placement student';
UPDATE public.erasmus_type_hid SET show_on_registration_form = 'f' where erasmus_type_hid = 'Erasmus/Socrates';

CREATE VIEW registration.visitor_type_hid AS
SELECT 'v-' || visitor_type_id AS id, registration_form_name as hid 
FROM public.visitor_type_hid
WHERE show_on_registration_form = 't';

ALTER VIEW registration.visitor_type_hid OWNER TO dev;
GRANT SELECT ON registration.visitor_type_hid TO starters_registration;

CREATE VIEW registration.staff_type_hid AS
SELECT 'sc-' || id as id, category as hid 
FROM public.staff_category
WHERE category like '%staff';

ALTER VIEW registration.staff_type_hid OWNER TO dev;
GRANT SELECT ON registration.staff_type_hid TO starters_registration;

CREATE VIEW registration.erasmus_type_hid AS
SELECT 'e-' || erasmus_type_id as id, registration_form_name as hid 
FROM public.erasmus_type_hid
WHERE show_on_registration_form = 't';

ALTER VIEW registration.erasmus_type_hid OWNER TO dev;
GRANT SELECT ON registration.erasmus_type_hid TO starters_registration;

CREATE VIEW registration.postgrad_type_hid AS
SELECT 'pg-' || id as id, name as hid 
FROM public.postgraduate_studentship_type
WHERE show_on_registration_form = 't';

ALTER VIEW registration.postgrad_type_hid OWNER TO dev;
GRANT SELECT ON registration.postgrad_type_hid TO starters_registration;
