DROP VIEW registration.visitor_type_hid;
DROP VIEW registration.staff_type_hid;
DROP VIEW registration.erasmus_type_hid;
DROP VIEW registration.postgrad_type_hid;
ALTER TABLE public.postgraduate_studentship_type DROP COLUMN show_on_registration_form;
ALTER TABLE public.visitor_type_hid DROP COLUMN show_on_registration_form;
ALTER TABLE public.visitor_type_hid DROP COLUMN registration_form_name;
ALTER TABLE public.erasmus_type_hid DROP COLUMN show_on_registration_form;
ALTER TABLE public.erasmus_type_hid DROP COLUMN registration_form_name;
