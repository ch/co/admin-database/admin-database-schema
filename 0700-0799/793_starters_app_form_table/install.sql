SET ROLE dev;

CREATE SEQUENCE registration.form_id_seq;
ALTER SEQUENCE registration.form_id_seq OWNER TO dev;
GRANT USAGE ON registration.form_id_seq TO starters_registration;

CREATE TABLE registration.form (
    form_id BIGINT PRIMARY KEY DEFAULT nextval('registration.form_id_seq'),
    uuid UUID UNIQUE NOT NULL,
    first_names TEXT,
    surname TEXT,
    known_as TEXT,
    title_id BIGINT REFERENCES public.title_hid(title_id),
    date_of_birth DATE,
    post_category_id TEXT, -- cannot make this a foreign key as there's no definitive table
    nationality_id BIGINT[],
    job_title TEXT,
    dept_room_id BIGINT[],
    dept_telephone_id BIGINT[],
    college_id BIGINT REFERENCES public.cambridge_college(id),
    home_address TEXT,
    home_phone_number TEXT,
    mobile_number TEXT,
    email TEXT,
    crsid VARCHAR(7),
    start_date DATE,
    intended_end_date DATE,
    home_institution TEXT,
    mifare_areas TEXT,
    mifare_out_hours_access_required BOOLEAN,
    mifare_aware_of_out_of_hours_rules BOOLEAN,
    mifare_technician_signed DATE,
    mifare_host_signed DATE,
    department_host_id BIGINT REFERENCES public.person(id),
    technician_id BIGINT REFERENCES public.person(id),
    deposit_receipt TEXT,
    emergency_contact_name_1 TEXT,
    emergency_contact_number_1 TEXT,
    emergency_contact_name_2 TEXT,
    emergency_contact_number_2 TEXT,
    previously_registered BOOLEAN NOT NULL DEFAULT 'f',
    hide_from_website BOOLEAN NOT NULL DEFAULT 'f',
    _form_started DATE NOT NULL DEFAULT current_date,
    submitted BOOLEAN NOT NULL DEFAULT 'f',
    _match_to_person_id BIGINT REFERENCES public.person(id),
    _imported_on DATE
);

COMMENT ON TABLE registration.form IS 'Names of columns to be filled by a person registering have to match WTForms Fields in the starters webapp';
COMMENT ON COLUMN registration.form._match_to_person_id IS 'For use of HR via Hotwire to force a match when importing. Do not expose to the starters webapp';
COMMENT ON COLUMN registration.form._form_started IS 'So we can purge incomplete forms after a time';
COMMENT ON COLUMN registration.form._imported_on IS 'So we can purge imported forms after a time, and also show the admin team what remains to be imported';

ALTER TABLE registration.form OWNER TO dev;
GRANT SELECT,INSERT,UPDATE ON registration.form TO starters_registration;
GRANT SELECT,UPDATE,DELETE ON registration.form TO hr; -- to allow them to import from this
