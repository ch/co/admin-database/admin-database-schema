CREATE OR REPLACE VIEW hotwire3."10_View/Data_Protection/Access_Control_Groups" AS 
 WITH acl_groups AS (
         SELECT r.rolname AS role,
            member.rolname AS member,
            person.id AS person_id
           FROM pg_roles r
             JOIN pg_auth_members m ON m.roleid = r.oid
             JOIN pg_roles member ON m.member = member.oid
             LEFT JOIN person ON member.rolname = person.crsid::name
        )
 SELECT row_number() OVER (ORDER BY acl_groups.role, acl_groups.member, acl_groups.person_id) AS id,
    acl_groups.role,
    acl_groups.member,
    acl_groups.person_id
   FROM acl_groups
  WHERE acl_groups.role <> '_autoadded'::name
  ORDER BY acl_groups.role, acl_groups.member;

ALTER TABLE hotwire3."10_View/Data_Protection/Access_Control_Groups" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Data_Protection/Access_Control_Groups" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Data_Protection/Access_Control_Groups" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/Data_Protection/Access_Control_Groups" TO cos;

