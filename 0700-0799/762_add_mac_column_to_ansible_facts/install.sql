DROP VIEW apps.ansible_facts;

CREATE OR REPLACE VIEW apps.ansible_facts AS 
WITH system_image_and_mac AS (
        SELECT
            system_image.id, research_group_id, wired_mac_1 AS mac
        FROM system_image
    UNION
        SELECT
            system_image.id, research_group_id, wired_mac_2 AS mac
        FROM system_image
    UNION
        SELECT
            system_image.id, research_group_id, wired_mac_3 AS mac
        FROM system_image
    UNION
        SELECT
            system_image.id, research_group_id, wired_mac_4 AS mac
        FROM system_image
    UNION
        SELECT
            system_image.id, research_group_id, wireless_mac AS mac
        FROM system_image
)
SELECT DISTINCT ip_address.hostname,
    system_image.mac,
    fai_class_hid.fai_class_hid AS flavour,
    research_group_hid.research_group_hid AS research_group,
    installer_tag_hid.installer_tag_hid AS tag
   FROM ip_address
     JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
     LEFT JOIN system_image_and_mac system_image ON mm_system_image_ip_address.system_image_id = system_image.id
     LEFT JOIN mm_system_image_installer_tag ON system_image.id = mm_system_image_installer_tag.system_image_id
     LEFT JOIN installer_tag_hid ON installer_tag_hid.installer_tag_id = mm_system_image_installer_tag.installer_tag_id
     LEFT JOIN research_group ON system_image.research_group_id = research_group.id
     LEFT JOIN fai_class_hid ON research_group.fai_class_id = fai_class_hid.fai_class_id
     LEFT JOIN research_group_hid ON research_group.id = research_group_hid.research_group_id;

ALTER TABLE apps.ansible_facts OWNER TO dev;
GRANT SELECT ON TABLE apps.ansible_facts TO osbuilder;
