DROP VIEW apps.leavers_it_signature;

CREATE VIEW apps.leavers_it_signature AS
SELECT
    it_leaving_form.crsid,
    it_leaving_form.user_date,
    it_leaving_form.co_date,
    person_hid.person_hid
FROM public.it_leaving_form
LEFT JOIN public.person ON it_leaving_form.crsid::text = person.crsid::text
LEFT JOIN public.person_hid ON person.id = person_hid.person_id;

ALTER TABLE apps.leavers_it_signature OWNER TO dev;

CREATE RULE it_sig_ins AS
    ON INSERT TO apps.leavers_it_signature DO INSTEAD  INSERT INTO public.it_leaving_form (crsid, user_date, co_date)
      VALUES (new.crsid, new.user_date, new.co_date);

CREATE RULE it_sig_upd AS
    ON UPDATE TO apps.leavers_it_signature DO INSTEAD  UPDATE public.it_leaving_form SET user_date = new.user_date, co_date = new.co_date
      WHERE ((it_leaving_form.crsid)::text = (old.crsid)::text);


GRANT ALL ON TABLE apps.leavers_it_signature TO dev;
GRANT SELECT,INSERT,UPDATE ON TABLE apps.leavers_it_signature TO leavers_trigger;

ALTER TABLE public.it_leaving_form DROP COLUMN co_id ;
