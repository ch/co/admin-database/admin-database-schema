CREATE OR REPLACE VIEW hotwire3. "10_View/Computers/Virtual_Machines" AS
SELECT
    system_image.id,
    system_image.hardware_id,
    system_image.operating_system_id,
    system_image.research_group_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac,
    ARRAY (
        SELECT
            mm_system_image_ip_address.ip_address_id
        FROM
            mm_system_image_ip_address
        WHERE
            mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    system_image.comments::text AS comments,
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
FROM
    system_image;

ALTER TABLE hotwire3. "10_View/Computers/Virtual_Machines" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO dev;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO cos;
