CREATE VIEW hotwire3.tagged_vlan_vid_hid AS
SELECT vlan.vid AS tagged_vlan_vid_id,
    ((vlan.name::text || ' ['::text) || vlan.description::text) || ']'::text AS tagged_vlan_vid_hid
       FROM vlan;
ALTER VIEW hotwire3.tagged_vlan_vid_hid OWNER TO dev;
GRANT SELECT ON hotwire3.tagged_vlan_vid_hid TO public;

CREATE VIEW hotwire3."10_View/IT_Accounts/Machine_ChemNet_Accounts" AS
SELECT 
        machine_account.id,
        name as username,
        system_image_id,
        tagged_vlans as tagged_vlan_vid_id
FROM machine_account
;

ALTER VIEW hotwire3."10_View/IT_Accounts/Machine_ChemNet_Accounts" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/Machine_ChemNet_Accounts" TO cos;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/IT_Accounts/Machine_ChemNet_Accounts', 'machine_account' );
