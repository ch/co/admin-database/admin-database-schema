DROP VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs";
DROP FUNCTION hotwire3.autoinstallation_for_cos_upd();

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs" AS 
 SELECT system_image.id,
    system_image.hardware_id AS _hardware_id,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    system_image.architecture_id,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id,
    system_image.reinstall_on_next_boot,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    system_image.netboot_id,
        CASE
            WHEN system_image.reinstall_on_next_boot = true THEN true
            WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
            WHEN system_image.is_managed_mac = true THEN true
            ELSE false
        END AS ro_is_managed_machine,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN research_group ON system_image.research_group_id = research_group.id;

ALTER TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Autoinstallation_for_COs" TO cos;

-- Rule: hotwire3_view_system_image_ai_upd ON hotwire3."10_View/Computers/Autoinstallation_for_COs"

-- DROP RULE hotwire3_view_system_image_ai_upd ON hotwire3."10_View/Computers/Autoinstallation_for_COs";

CREATE OR REPLACE RULE hotwire3_view_system_image_ai_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Autoinstallation_for_COs" DO INSTEAD ( UPDATE hardware SET name = new.hardware_name, hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments, research_group_id = new.research_group_id, user_id = new.user_id, netboot_id = new.netboot_id
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.software_package_id, old.software_package_id, 'mm_system_image_software_package_jumpstart'::character varying, 'system_image_id'::character varying, 'software_package_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update;
);


