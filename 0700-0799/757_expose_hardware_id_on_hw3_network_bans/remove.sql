DROP VIEW hotwire3."10_View/Computers/Network_Bans";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Network_Bans" AS 
 SELECT system_image.id,
    system_image.hardware_id AS _hardware_id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    system_image.comments::text AS system_image_comments,
    hardware.comments::text AS hardware_comments,
    system_image.is_managed_mac,
    system_image.was_managed_machine_on AS ro_was_managed_machine_on,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/Network_Bans"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Network_Bans" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Network_Bans" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.system_image_bans_upd()
  RETURNS trigger AS
$BODY$
BEGIN
    UPDATE hardware SET
        manufacturer=NEW.manufacturer, 
        model=NEW.model, 
        name=NEW.hardware_name,
        hardware_type_id=NEW.hardware_type_id, 
        asset_tag = NEW.asset_tag,
        serial_number = NEW.serial_number,
        room_id=NEW.room_id,
        owner_id=NEW.owner_id,
        comments=NEW.hardware_comments::varchar(1000)
    WHERE hardware.id = NEW._hardware_id;
    UPDATE system_image SET
          user_id=NEW.user_id,
          research_group_id=NEW.research_group_id,
          operating_system_id = NEW.operating_system_id,
          wired_mac_1 = NEW.wired_mac_1, 
          wired_mac_2 = NEW.wired_mac_2,
          wireless_mac = NEW.wireless_mac,
          comments = NEW.system_image_comments::varchar(1000),
          is_managed_mac = NEW.is_managed_mac,
          override_auto_ban = NEW.override_auto_ban
    WHERE system_image.id = NEW.id;
    -- mm updates
    PERFORM fn_mm_array_update(NEW.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,NEW.id);
    RETURN NEW; 
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.system_image_bans_upd()
  OWNER TO dev;


-- Trigger: computers_network_bans_upd on hotwire3."10_View/Computers/Network_Bans"

-- DROP TRIGGER computers_network_bans_upd ON hotwire3."10_View/Computers/Network_Bans";

CREATE TRIGGER computers_network_bans_upd
  INSTEAD OF UPDATE
  ON hotwire3."10_View/Computers/Network_Bans"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.system_image_bans_upd();


