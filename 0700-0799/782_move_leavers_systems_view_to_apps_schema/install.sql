CREATE VIEW apps.system_image_by_user_crsid AS
 SELECT system_image.id,
    ip_address.hostname,
    operating_system.os,
    system_image.hardware_id,
    hardware_type_hid.hardware_type_hid AS type,
    hardware.manufacturer,
    hardware.model,
    room.name AS room,
    person.crsid,
    system_image.research_group_id,
    COALESCE(research_group.active_directory_container, research_group.name) AS name
   FROM ((((((((public.system_image
     JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id)))
     LEFT JOIN public.mm_system_image_ip_address ON ((system_image.id = mm_system_image_ip_address.system_image_id)))
     LEFT JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id)))
     LEFT JOIN public.hardware ON ((system_image.hardware_id = hardware.id)))
     LEFT JOIN public.hardware_type_hid USING (hardware_type_id))
     LEFT JOIN public.room ON ((hardware.room_id = room.id)))
     LEFT JOIN public.person ON ((system_image.user_id = person.id)))
     LEFT JOIN public.research_group ON ((system_image.research_group_id = research_group.id)));


ALTER TABLE apps.system_image_by_user_crsid OWNER TO dev;

REVOKE ALL ON TABLE apps.system_image_by_user_crsid FROM PUBLIC;
REVOKE ALL ON TABLE apps.system_image_by_user_crsid FROM dev;
GRANT ALL ON TABLE apps.system_image_by_user_crsid TO dev;
GRANT SELECT ON TABLE apps.system_image_by_user_crsid TO leavers_trigger;
