CREATE VIEW hotwire3."10_View/Network/_Switches_for_cabinet" AS
SELECT * FROM (
SELECT
  cabinet_id,
  switch.id,
  switch.id AS switch_id,
  switch_model.model,
  switch_model.description,
  hardware.name as _name
FROM switch
JOIN hardware ON switch.hardware_id = hardware.id
JOIN switch_model ON switch.switch_model_id = switch_model.id
ORDER BY _name ) a
;
ALTER VIEW hotwire3."10_View/Network/_Switches_for_cabinet" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/_Switches_for_cabinet" TO cos;

CREATE VIEW hotwire3."10_View/Network/_Patch_panels_for_cabinet" AS
SELECT
  cabinet_id,
  id,
  id as patch_panel_id,
  name,
  patch_panel_type_id
FROM patch_panel
;

ALTER VIEW hotwire3."10_View/Network/_Patch_panels_for_cabinet" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/_Patch_panels_for_cabinet" TO cos;

DROP VIEW hotwire3."10_View/Network/Cabinets";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Cabinets" AS 
 SELECT cabinet.id,
    cabinet.name,
    cabinet.email_local_part,
    cabinet.notes,
    cabinet.room_id,
    cabinet.cabno,
    cabinet.in_use,
    cabinet.depth,
    cabinet.fibre_u,
    cabinet.patch_u,
    cabinet.pots_u,
    cabinet.other_u,
    cabinet.height_u,
    cabinet.ports,
    cabinet.max_ports,
    cabinet.sw48,
    cabinet.sw24,
    cabinet.cabname,
    hotwire3.to_hwsubviewb('10_View/Network/_Switches_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Switches/Switches'::varchar,'switch_id'::varchar,'id'::varchar) AS "Switches",
    hotwire3.to_hwsubviewb('10_View/Network/_Patch_panels_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Patch_Panels'::varchar,'patch_panel_id'::varchar,'id'::varchar) AS "Patch_Panels",
    hotwire3.to_hwsubviewb('10_View/Network/Fibre_Panels'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panels'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panel",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/10_Fibre_Runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores"
   FROM cabinet;

ALTER TABLE hotwire3."10_View/Network/Cabinets"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Cabinets" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Cabinets" TO cos;

-- Rule: "hotwire3_10_View/Network/Cabinets_del" ON hotwire3."10_View/Network/Cabinets"

-- DROP RULE "hotwire3_10_View/Network/Cabinets_del" ON hotwire3."10_View/Network/Cabinets";

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_del" AS
    ON DELETE TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  DELETE FROM cabinet
  WHERE cabinet.id = old.id;

-- Rule: "hotwire3_10_View/Network/Cabinets_ins" ON hotwire3."10_View/Network/Cabinets"

-- DROP RULE "hotwire3_10_View/Network/Cabinets_ins" ON hotwire3."10_View/Network/Cabinets";

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  INSERT INTO cabinet (name, email_local_part, notes, room_id, cabno, in_use, depth, fibre_u, patch_u, pots_u, other_u, height_u, ports, max_ports, sw48, sw24, cabname)
  VALUES (new.name, new.email_local_part, new.notes, new.room_id, new.cabno, new.in_use, new.depth, new.fibre_u, new.patch_u, new.pots_u, new.other_u, new.height_u, new.ports, new.max_ports, new.sw48, new.sw24, new.cabname)
  RETURNING cabinet.id,
    cabinet.name,
    cabinet.email_local_part,
    cabinet.notes,
    cabinet.room_id,
    cabinet.cabno,
    cabinet.in_use,
    cabinet.depth,
    cabinet.fibre_u,
    cabinet.patch_u,
    cabinet.pots_u,
    cabinet.other_u,
    cabinet.height_u,
    cabinet.ports,
    cabinet.max_ports,
    cabinet.sw48,
    cabinet.sw24,
    cabinet.cabname,
    hotwire3.to_hwsubviewb('10_View/Network/_Switches_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Switches/Switches'::varchar,'switch_id'::varchar,'id'::varchar) AS "Switches",
    hotwire3.to_hwsubviewb('10_View/Network/_Patch_panels_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Patch_Panels'::varchar,'patch_panel_id'::varchar,'id'::varchar) AS "Patch_Panels",
    hotwire3.to_hwsubviewb('10_View/Network/Fibre_Panels'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panels'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panels",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/10_Fibre_runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores";

-- Rule: "hotwire3_10_View/Network/Cabinets_upd" ON hotwire3."10_View/Network/Cabinets"

-- DROP RULE "hotwire3_10_View/Network/Cabinets_upd" ON hotwire3."10_View/Network/Cabinets";

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  UPDATE cabinet SET id = new.id, name = new.name, email_local_part = new.email_local_part, notes = new.notes, room_id = new.room_id, cabno = new.cabno, in_use = new.in_use, depth = new.depth, fibre_u = new.fibre_u, pots_u = new.pots_u, other_u = new.other_u, height_u = new.height_u, ports = new.ports, max_ports = new.max_ports, sw48 = new.sw48, sw24 = new.sw24, patch_u = new.patch_u, cabname = new.cabname
  WHERE cabinet.id = old.id;


