CREATE VIEW apps.leavers_hardware_by_owner_crsid AS
SELECT hardware.id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.delete_after_date,
    room.name AS room,
    hardware_type_hid.hardware_type_hid AS type,
    person.crsid
   FROM hardware
     JOIN hardware_type_hid USING (hardware_type_id)
     JOIN person ON hardware.owner_id = person.id
     JOIN room ON hardware.room_id = room.id;

ALTER VIEW apps.leavers_hardware_by_owner_crsid OWNER TO dev;

GRANT SELECT, UPDATE ON apps.leavers_hardware_by_owner_crsid TO leavers_trigger;
GRANT SELECT (id,delete_after_date) ON hardware TO leavers_trigger;
GRANT UPDATE (delete_after_date) ON hardware TO leavers_trigger;

CREATE FUNCTION apps.leavers_hardware_by_owner_crsid_trig() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE hardware SET delete_after_date = NEW.delete_after_date WHERE id = OLD.id;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

ALTER FUNCTION apps.leavers_hardware_by_owner_crsid_trig() OWNER TO dev;

CREATE TRIGGER hardware_date_upd INSTEAD OF UPDATE ON apps.leavers_hardware_by_owner_crsid FOR EACH ROW EXECUTE PROCEDURE apps.leavers_hardware_by_owner_crsid_trig();
