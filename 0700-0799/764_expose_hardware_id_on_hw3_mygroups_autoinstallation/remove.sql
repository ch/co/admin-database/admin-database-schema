DROP VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation";
DROP FUNCTION hotwire3_mygroups_autoinstallation_upd();

CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Computer_Autoinstallation" AS 
 SELECT DISTINCT system_image.id,
    hardware.id AS _hardware_id,
    hardware.manufacturer,
    hardware.model,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.architecture_id,
    system_image.reinstall_on_next_boot,
    array_to_string(ARRAY( SELECT ip_address_hid.ip_address_hid
           FROM mm_system_image_ip_address
             JOIN hotwire3.ip_address_hid USING (ip_address_id)
          WHERE mm_system_image_ip_address.system_image_id = system_image.id), ','::text) AS ro_ip_address,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    hardware.asset_tag,
    hardware.serial_number,
    hardware.monitor_serial_number,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id AS ro_research_group_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.auto_banned_from_network AS ro_auto_banned_from_network,
    system_image.override_auto_ban AS ro_override_auto_ban,
    system_image.network_ban_log::text AS ro_network_ban_log,
    system_image.was_managed_machine_on AS ro_was_managed_machine_on
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN mm_research_group_computer_rep USING (research_group_id)
     LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
     JOIN research_group ON system_image.research_group_id = research_group.id
     JOIN person head_of_gp ON research_group.head_of_group_id = head_of_gp.id
     LEFT JOIN person deputy ON research_group.deputy_head_of_group_id = deputy.id
  WHERE person.crsid::name = "current_user"() OR head_of_gp.crsid::name = "current_user"() OR deputy.crsid::name = "current_user"();

ALTER TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO groupitreps;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/My_Groups/Computer_Autoinstallation" TO headsofgroup;

-- Rule: group_computers_view_del ON hotwire3."10_View/My_Groups/Computer_Autoinstallation"

-- DROP RULE group_computers_view_del ON hotwire3."10_View/My_Groups/Computer_Autoinstallation";

CREATE OR REPLACE RULE group_computers_view_del AS
    ON DELETE TO hotwire3."10_View/My_Groups/Computer_Autoinstallation" DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

-- Rule: group_computers_view_upd ON hotwire3."10_View/My_Groups/Computer_Autoinstallation"

-- DROP RULE group_computers_view_upd ON hotwire3."10_View/My_Groups/Computer_Autoinstallation";

CREATE OR REPLACE RULE group_computers_view_upd AS
    ON UPDATE TO hotwire3."10_View/My_Groups/Computer_Autoinstallation" DO INSTEAD ( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET user_id = new.user_id, operating_system_id = new.operating_system_id, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update;
);


