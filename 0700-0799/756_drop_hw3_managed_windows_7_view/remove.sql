-- If we need to reinstate this view, it needs repair work first.
-- The _hardware_id column is a problem; Hotwire doesn't pass its value back 
-- to the database in updates, and so Postgres will look up the value in the
-- current row in that view with whatever win7.id we have and that gets used in
-- the NEW record. If the hardware_id has changed since the user loaded the
-- Hotwire editing page, the wrong hardware record may get updated. We have to
-- use a visible, writable column for the hardware primary key and hide it from
-- the user with CSS.

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Managed_Windows_7" AS 
 SELECT win7.id,
    win7._hardware_id,
    win7.hardware_type_id,
    win7.operating_system_id,
    win7.architecture_id,
    win7.wired_mac_1,
    win7.wired_mac_2,
    win7.wireless_mac,
    win7.ip_address_id,
    win7.room_id,
    win7.user_id,
    win7.owner_id,
    win7.research_group_id,
    win7.ro_is_managed_machine,
    win7._all_legacy_vlan,
    win7."Contact_Details",
    win7."Hardware",
    win7."IP_addresses",
    win7."VLAN_assignments"
   FROM ( SELECT system_image.id,
            system_image.hardware_id AS _hardware_id,
            hardware.hardware_type_id,
            system_image.operating_system_id,
            system_image.architecture_id,
            system_image.wired_mac_1,
            system_image.wired_mac_2,
            system_image.wireless_mac,
            ARRAY( SELECT mm_system_image_ip_address.ip_address_id
                   FROM mm_system_image_ip_address
                  WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
            hardware.room_id,
            system_image.user_id,
            hardware.owner_id,
            system_image.research_group_id,
                CASE
                    WHEN system_image.reinstall_on_next_boot = true THEN true
                    WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
                    WHEN system_image.is_managed_mac = true THEN true
                    ELSE false
                END AS ro_is_managed_machine,
            every(COALESCE(vlan.name, 'default'::character varying)::text = 'ch-legacyos'::text) AS _all_legacy_vlan,
            hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
            hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
            hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
            hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
           FROM system_image
             LEFT JOIN ( SELECT system_image_1.id,
                    system_image_1.wired_mac_1 AS mac
                   FROM system_image system_image_1
                  WHERE system_image_1.wired_mac_1 IS NOT NULL
                UNION
                 SELECT system_image_1.id,
                    system_image_1.wired_mac_2 AS mac
                   FROM system_image system_image_1
                  WHERE system_image_1.wired_mac_2 IS NOT NULL
                UNION
                 SELECT system_image_1.id,
                    system_image_1.wired_mac_3 AS mac
                   FROM system_image system_image_1
                  WHERE system_image_1.wired_mac_3 IS NOT NULL
                UNION
                 SELECT system_image_1.id,
                    system_image_1.wired_mac_4 AS mac
                   FROM system_image system_image_1
                  WHERE system_image_1.wired_mac_4 IS NOT NULL
                UNION
                 SELECT system_image_1.id,
                    system_image_1.wireless_mac AS mac
                   FROM system_image system_image_1
                  WHERE system_image_1.wireless_mac IS NOT NULL) system_image_by_mac ON system_image_by_mac.id = system_image.id
             JOIN hardware ON hardware.id = system_image.hardware_id
             JOIN research_group ON system_image.research_group_id = research_group.id
             JOIN operating_system ON system_image.operating_system_id = operating_system.id
             LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON system_image_by_mac.mac = mac_to_vlan.mac
             LEFT JOIN vlan USING (vid)
          WHERE operating_system.os::text = 'Windows 7'::text
          GROUP BY system_image.id, hardware.id) win7
  WHERE win7.ro_is_managed_machine = true AND win7._all_legacy_vlan IS DISTINCT FROM true;

ALTER TABLE hotwire3."10_View/Computers/Managed_Windows_7"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO cos;

CREATE OR REPLACE RULE hotwire3_managed_win7_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Managed_Windows_7" DO INSTEAD ( UPDATE hardware SET hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET operating_system_id = new.operating_system_id, architecture_id = new.architecture_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, research_group_id = new.research_group_id, user_id = new.user_id
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update;
);


