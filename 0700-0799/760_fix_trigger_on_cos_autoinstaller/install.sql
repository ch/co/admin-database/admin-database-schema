CREATE OR REPLACE FUNCTION hotwire3.autoinstallation_for_cos_upd() RETURNS TRIGGER AS
$body$
    BEGIN
    UPDATE system_image SET operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments, research_group_id = new.research_group_id, user_id = new.user_id, netboot_id = new.netboot_id
    WHERE system_image.id = old.id;

    PERFORM fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update;
    PERFORM fn_mm_array_update(new.software_package_id, old.software_package_id, 'mm_system_image_software_package_jumpstart'::character varying, 'system_image_id'::character varying, 'software_package_id'::character varying, old.id) AS fn_mm_array_update;
    PERFORM fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update;

    RETURN NEW;
    END;
$body$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.autoinstallation_for_cos_upd() OWNER TO dev;
