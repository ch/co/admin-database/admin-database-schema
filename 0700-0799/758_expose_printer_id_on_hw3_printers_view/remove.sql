DROP VIEW hotwire3."10_View/Computers/Printers";
DROP FUNCTION hotwire3.printers_upd() ;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Printers" AS 
 SELECT hardware.id,
    printer.id AS _printer_id,
    hardware.id AS _hardware_id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.date_purchased,
    hardware.serial_number,
    hardware.asset_tag,
    hardware.date_configured,
    hardware.warranty_end_date,
    hardware.date_decommissioned,
    hardware.warranty_details,
    hardware.room_id,
    hardware.owner_id,
    printer.printer_class_id,
    printer.cups_options,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_System_Instances_ro'::character varying, 'hardware_id'::character varying, '10_View/Computers/System_Instances'::character varying, 'system_image_id'::character varying, 'id'::character varying) AS "System_Instances"
   FROM printer
     JOIN hardware ON printer.hardware_id = hardware.id;

ALTER TABLE hotwire3."10_View/Computers/Printers"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Printers" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/Printers" TO cos;

-- Rule: hotwire3_view_printers_del ON hotwire3."10_View/Computers/Printers"

-- DROP RULE hotwire3_view_printers_del ON hotwire3."10_View/Computers/Printers";

CREATE OR REPLACE RULE hotwire3_view_printers_del AS
    ON DELETE TO hotwire3."10_View/Computers/Printers" DO INSTEAD ( DELETE FROM printer
  WHERE printer.id = old._printer_id;
 DELETE FROM hardware
  WHERE hardware.id = old._hardware_id;
);

-- Rule: hotwire3_view_printers_upd ON hotwire3."10_View/Computers/Printers"

-- DROP RULE hotwire3_view_printers_upd ON hotwire3."10_View/Computers/Printers";

CREATE OR REPLACE RULE hotwire3_view_printers_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Printers" DO INSTEAD ( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, date_purchased = new.date_purchased, serial_number = new.serial_number, asset_tag = new.asset_tag, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id
  WHERE hardware.id = old._hardware_id;
 UPDATE printer SET printer_class_id = new.printer_class_id, cups_options = new.cups_options
  WHERE printer.id = old._printer_id;
);


