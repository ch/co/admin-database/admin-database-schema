CREATE OR REPLACE VIEW hotwire3.software_package_hid AS 
SELECT 
  software_package.id AS software_package_id, 
    (
      (
        '[' :: text || array_to_string(
          ARRAY(
            SELECT 
              DISTINCT os_class_hid.abbrev 
            FROM 
              mm_software_package_can_install_on_os 
              JOIN operating_system ON mm_software_package_can_install_on_os.operating_system_id = operating_system.id 
              JOIN os_class_hid USING (os_class_id) 
            WHERE 
              mm_software_package_can_install_on_os.software_package_id = software_package.id
          ), 
          ',' :: text
        )
      ) || '] ' :: text
    ) || software_package.name :: text ||
    (
    CASE
      WHEN software_package.sitelicence IS DISTINCT FROM 't'
      THEN ' [RESTRICTED]'::text 
      ELSE ''::text
    END
    )
  AS software_package_hid 
FROM 
  software_package 
ORDER BY 
  software_package.name;
ALTER TABLE 
  hotwire3.software_package_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.software_package_hid TO dev;
GRANT 
SELECT 
  ON TABLE hotwire3.software_package_hid TO public;


ALTER TABLE hotwire3.software_package_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.software_package_hid TO dev;
GRANT SELECT ON TABLE hotwire3.software_package_hid TO public;
