DROP VIEW hotwire3."10_View/Roles/Visitors";

CREATE VIEW hotwire3."10_View/Roles/Visitors" AS
WITH person_visitor AS (
  SELECT 
    visitorship.id, 
    visitorship.person_id, 
    _all_roles.status AS ro_role_status, 
    visitorship.home_institution, 
    visitorship.visitor_type_id, 
    visitorship.host_person_id AS host_id, 
    visitorship.start_date, 
    visitorship.intended_end_date, 
    visitorship.end_date, 
    visitorship.notes :: text AS notes, 
    visitorship.force_role_status_to_past, 
    person.surname AS _surname, 
    person.first_names AS _first_names 
  FROM 
    visitorship 
    LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = visitorship.id 
    AND _all_roles.role_tablename = 'visitorship' :: text 
    LEFT JOIN person ON person.id = visitorship.person_id
) 
SELECT 
  person_visitor.id, 
  person_visitor.person_id, 
  person_visitor.ro_role_status, 
  person_visitor.home_institution, 
  person_visitor.visitor_type_id, 
  person_visitor.host_id, 
  person_visitor.start_date, 
  person_visitor.intended_end_date, 
  person_visitor.end_date, 
  person_visitor.notes, 
  person_visitor.force_role_status_to_past, 
  person_visitor._surname, 
  person_visitor._first_names 
FROM 
  person_visitor 
ORDER BY 
  person_visitor._surname, 
  person_visitor._first_names;

ALTER TABLE hotwire3."10_View/Roles/Visitors" OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Visitors" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Visitors" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Visitors" TO hr;

-- Rule: hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors"
-- DROP RULE hotwire3_view_visitors_del ON hotwire3."10_View/Roles/Visitors";
CREATE RULE hotwire3_view_visitors_del AS ON DELETE TO hotwire3."10_View/Roles/Visitors" DO INSTEAD 
DELETE FROM 
  visitorship 
WHERE 
  visitorship.id = old.id;
-- Rule: hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors"
-- DROP RULE hotwire3_view_visitors_ins ON hotwire3."10_View/Roles/Visitors";
CREATE RULE hotwire3_view_visitors_ins AS ON INSERT TO hotwire3."10_View/Roles/Visitors" DO INSTEAD INSERT INTO visitorship (
  home_institution, visitor_type_id, 
  person_id, host_person_id, intended_end_date, 
  end_date, start_date, notes, force_role_status_to_past
) 
VALUES 
  (
    new.home_institution, 
    new.visitor_type_id, 
    new.person_id, 
    new.host_id, 
    new.intended_end_date, 
    new.end_date, 
    new.start_date, 
    new.notes :: character varying(80), 
    new.force_role_status_to_past
  ) RETURNING visitorship.id, 
  visitorship.person_id, 
  NULL :: character varying(10) AS "varchar", 
  visitorship.home_institution, 
  visitorship.visitor_type_id, 
  visitorship.host_person_id AS host_id, 
  visitorship.start_date, 
  visitorship.intended_end_date, 
  visitorship.end_date, 
  visitorship.notes :: text AS notes, 
  visitorship.force_role_status_to_past, 
  NULL :: character varying(32) AS "varchar", 
  NULL :: character varying(32) AS "varchar";
-- Rule: hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors"
-- DROP RULE hotwire3_view_visitors_upd ON hotwire3."10_View/Roles/Visitors";
CREATE RULE hotwire3_view_visitors_upd AS ON UPDATE TO hotwire3."10_View/Roles/Visitors" DO INSTEAD 
UPDATE 
  visitorship 
SET 
  home_institution = new.home_institution, 
  visitor_type_id = new.visitor_type_id, 
  person_id = new.person_id, 
  host_person_id = new.host_id, 
  intended_end_date = new.intended_end_date, 
  end_date = new.end_date, 
  start_date = new.start_date, 
  notes = new.notes :: character varying(80), 
  force_role_status_to_past = new.force_role_status_to_past 
WHERE 
  visitorship.id = old.id;

