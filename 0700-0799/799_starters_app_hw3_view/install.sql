CREATE VIEW hotwire3.floor_technician_hid AS
SELECT person_hid.person_id AS floor_technician_id,
       person_hid.person_hid AS floor_technician_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.floor_technician_hid OWNER TO dev;
GRANT SELECT ON hotwire3.floor_technician_hid TO ro_hid;

CREATE VIEW hotwire3.automatically_matched_person_hid AS
SELECT person_hid.person_id AS automatically_matched_person_id,
       person_hid.person_hid AS automatically_matched_person_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.automatically_matched_person_hid OWNER TO dev;
GRANT SELECT ON hotwire3.automatically_matched_person_hid TO ro_hid;

CREATE VIEW hotwire3.match_to_person_hid AS
SELECT person_hid.person_id AS match_to_person_id,
       person_hid.person_hid AS match_to_person_hid
FROM hotwire3.person_hid;
ALTER VIEW hotwire3.match_to_person_hid OWNER TO dev;
GRANT SELECT ON hotwire3.match_to_person_hid TO ro_hid;

-- Need a 'find a match' function
CREATE FUNCTION registration.match_existing_person(new_surname VARCHAR, new_first_names VARCHAR, new_crsid VARCHAR, new_date_of_birth DATE) RETURNS BIGINT AS
$BODY$
DECLARE
    matched_id BIGINT;
BEGIN
    -- if we have a crsid, look for that first
    IF new_crsid IS NOT NULL THEN
    -- if no crsid, try to match surname, date of birth, and initial of first_names
        SELECT id INTO matched_id FROM public.person WHERE crsid = new_crsid;
    END IF;
    BEGIN
        IF matched_id IS NULL THEN
            SELECT id INTO STRICT matched_id FROM person WHERE surname = new_surname AND date_of_birth = new_date_of_birth AND LOWER(LEFT(first_names,1)) = LOWER(LEFT(new_first_names,1));
            -- If we didn't find a unique match, there is no match
        END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
            WHEN TOO_MANY_ROWS THEN
                NULL;
        END;
    RETURN matched_id;
END;
$BODY$ LANGUAGE plpgsql;

ALTER FUNCTION registration.match_existing_person(varchar,varchar,varchar,date) OWNER TO dev;

CREATE VIEW hotwire3."10_View/People/_registration_matches" AS
SELECT
    matches.id,
    person.first_names,
    person.surname,
    person.date_of_birth,
    person.crsid,
    person.id AS _matched_person_id
FROM (
SELECT
    form_id as id,
    CASE
        WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
        ELSE registration.match_existing_person(surname::varchar,first_names::varchar,form.crsid,form.date_of_birth)
    END AS matched_person_id
FROM registration.form ) matches
JOIN person ON matched_person_id = person.id;
ALTER VIEW hotwire3."10_View/People/_registration_matches" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/People/_registration_matches" TO hr;
COMMENT ON VIEW hotwire3."10_View/People/_registration_matches" IS 'Used by the People/Registration view to allow jumping to a potential match';

CREATE VIEW hotwire3."10_View/People/Registration" AS
WITH reg AS (
SELECT
    form_id AS id,
    form.first_names::varchar,
    form.surname::varchar,
    form.known_as::varchar,
    form.title_id,
    form.date_of_birth,
    form.post_category_id,
    form.nationality_id,
    form.job_title::varchar,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::varchar,
    form.mobile_number::varchar,
    form.email::varchar,
    form.crsid::varchar,
    form.start_date,
    form.intended_end_date,
    form.home_institution::varchar,
    form.department_host_id AS supervisor_id,
    form.technician_id AS floor_technician_id,
    form.mifare_areas,
    form.mifare_out_hours_access_required AS mifare_out_of_hours_access_required,
    form.mifare_aware_of_out_of_hours_rules,
    form.mifare_technician_signed AS mifare_technician_signed_date,
    form.mifare_host_signed AS mifare_host_signed_date,
    form.deposit_receipt::varchar,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    form.hide_from_website,
    form.previously_registered,
    CASE
        WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
        ELSE registration.match_existing_person(surname::varchar,first_names::varchar,form.crsid,form.date_of_birth)
    END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    'f'::boolean AS import_this_record,
    form._imported_on AS ro_imported_on,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches','id','10_View/People/Personnel_Data_Entry','_matched_person_id',null) AS matched_person_details,
    CASE
        WHEN form._imported_on IS NOT NULL THEN 'green'
        ELSE NULL
    END AS _cssclass
FROM registration.form
WHERE submitted = 't' AND (post_category_id like 'sc-%' OR (mifare_technician_signed <= current_date AND mifare_host_signed <= current_date))
) SELECT *
FROM reg
ORDER BY ro_imported_on DESC NULLS FIRST
;
ALTER TABLE hotwire3."10_View/People/Registration" OWNER TO dev;
GRANT SELECT,UPDATE ON registration.completed_form_summary TO hr;

CREATE FUNCTION registration.people_registration_upd() RETURNS TRIGGER AS
$BODY$
DECLARE
    new_person_id BIGINT;
    emergency_contact VARCHAR(500);
    existing_pg_id BIGINT;
BEGIN
-- first and foremost update the data
     UPDATE registration.form SET
         first_names = NEW.first_names,
         surname = NEW.surname,
         known_as = NEW.known_as,
         title_id = NEW.title_id,
         date_of_birth = NEW.date_of_birth,
         post_category_id = NEW.post_category_id,
         nationality_id = NEW.nationality_id,
         job_title = NEW.job_title,
         dept_room_id = NEW.room_id,
         dept_telephone_id = NEW.dept_telephone_number_id,
         college_id = NEW.cambridge_college_id,
         home_address = NEW.home_address,
         home_phone_number = NEW.home_phone_number,
         mobile_number = NEW.mobile_number,
         email = NEW.email,
         crsid = NEW.crsid,
         start_date = NEW.start_date,
         intended_end_date = NEW.intended_end_date,
         home_institution = NEW.home_institution,
         mifare_areas = NEW.mifare_areas,
         mifare_out_hours_access_required = NEW.mifare_out_of_hours_access_required,
         mifare_aware_of_out_of_hours_rules = NEW.mifare_aware_of_out_of_hours_rules,
         mifare_technician_signed = NEW.mifare_technician_signed_date,
         mifare_host_signed = NEW.mifare_host_signed_date,
         department_host_id = NEW.supervisor_id,
         technician_id = NEW.floor_technician_id,
         deposit_receipt = NEW.deposit_receipt,
         emergency_contact_name_1 = NEW.emergency_contact_name_1,
         emergency_contact_number_1 = NEW.emergency_contact_number_1,
         emergency_contact_name_2 = NEW.emergency_contact_name_2,
         emergency_contact_number_2 = NEW.emergency_contact_number_2,
         previously_registered = NEW.previously_registered,
         hide_from_website = NEW.hide_from_website,
         _match_to_person_id = NEW.match_to_person_id
     WHERE form_id = OLD.id;

     IF NEW.import_this_record = 't'::boolean THEN
         -- Build a composite field
         emergency_contact := NEW.emergency_contact_name_1
                     || ' '
                     || NEW.emergency_contact_number_1
                     || (   COALESCE(E'\n'||NEW.emergency_contact_name_2||' ','')
                            || COALESCE(NEW.emergency_contact_number_2,'')
                        );
         -- Is there a match with an existing person?
         -- If match manually set, use that. 
         IF NEW.match_to_person_id IS NOT NULL THEN
             new_person_id = NEW.match_to_person_id;
         -- Or if the system has worked it out for itself use that
         -- Using OLD here because going for NEW would seem potentially fatal as an edit could change the automatic match
         ELSIF OLD.previously_registered = 't' AND OLD.ro_automatically_matched_person_id IS NOT NULL THEN
             new_person_id = OLD.ro_automatically_matched_person_id;
         END IF;
         -- If we found a match, update that person
         -- First the non-nullable fields; these can be updated unconditionally
         IF new_person_id IS NOT NULL THEN
             UPDATE person SET
                 surname = NEW.surname,
                 title_id = NEW.title_id,
                 date_of_birth = NEW.date_of_birth,
                 cambridge_address = NEW.home_address,
                 cambridge_phone_number = NEW.home_phone_number,
                 mobile_number = NEW.mobile_number,
                 email_address = NEW.email,
                 crsid = NEW.crsid,
                 do_not_show_on_website = NEW.hide_from_website
             WHERE person.id = new_person_id;
             -- These are nullable on the registration form, so if we had data in them
             -- we don't want to overwrite it with blanks
             IF NEW.first_names IS NOT NULL THEN
                 UPDATE person SET first_names = NEW.first_names WHERE person.id = new_person_id;
             END IF;
             IF NEW.known_as IS NOT NULL THEN
                 UPDATE person SET known_as = NEW.known_as WHERE person.id = new_person_id;
             END IF;
         ELSE -- create new
             INSERT INTO person (
                 first_names,
                 surname,
                 known_as,
                 title_id,
                 date_of_birth,
                 cambridge_college_id,
                 cambridge_address,
                 cambridge_phone_number,
                 mobile_number,
                 email_address,
                 crsid,
                 emergency_contact,
                 do_not_show_on_website
             ) VALUES (
                 NEW.first_names,
                 NEW.surname,
                 NEW.known_as,
                 NEW.title_id,
                 NEW.date_of_birth,
                 NEW.cambridge_college_id,
                 NEW.home_address,
                 NEW.home_phone_number,
                 NEW.mobile_number,
                 NEW.email,
                 NEW.crsid,
                 emergency_contact,
                 NEW.hide_from_website
             ) RETURNING id INTO new_person_id;
         END IF;
         -- Do mm-updates here on nationality, room, phone
         PERFORM public.fn_mm_array_update(NEW.room_id,'mm_person_room'::varchar,'person_id'::varchar,'room_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.dept_telephone_number_id,'mm_person_dept_telephone_number'::varchar,'person_id'::varchar,'dept_telephone_number_id'::varchar,new_person_id);
         PERFORM public.fn_mm_array_update(NEW.nationality_id,'mm_person_nationality'::varchar,'person_id'::varchar,'nationality_id'::varchar,new_person_id);
         -- create the role
         CASE rtrim(NEW.post_category_id,'-1234567890')
             WHEN 'sc' THEN
                 INSERT INTO post_history (
                     person_id,
                     start_date,
                     supervisor_id,
                     job_title,
                     intended_end_date,
                     staff_category_id
                 ) VALUES (
                     new_person_id,
                     NEW.start_date,
                     NEW.supervisor_id,
                     NEW.job_title,
                     NEW.intended_end_date,
                     ltrim(NEW.post_category_id,'sc-')::bigint
                 );
             WHEN 'v' THEN
                 INSERT INTO visitorship (
                     person_id,
                     host_person_id,
                     start_date,
                     intended_end_date,
                     home_institution,
                     visitor_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'v-')::bigint
                 );
             WHEN 'pg' THEN
               -- Need to check for an existing postgraduate_studentship of the same type here and not import if there is one
               -- because these sometimes get created in bulk at the start of the academic year
                 SELECT id INTO existing_pg_id
                 FROM postgraduate_studentship
                 WHERE person_id = new_person_id AND postgraduate_studentship_type_id = ltrim(NEW.post_category_id,'pg-')::bigint;
                 IF existing_pg_id IS NULL THEN
                     INSERT INTO postgraduate_studentship (
                         person_id,
                         first_supervisor_id,
                         start_date,
                         postgraduate_studentship_type_id
                     ) VALUES (
                         new_person_id,
                         NEW.supervisor_id,
                         NEW.start_date,
                         ltrim(NEW.post_category_id,'pg-')::bigint
                     );
                 END IF;
             WHEN 'e' THEN
                 INSERT INTO erasmus_socrates_studentship (
                     person_id,
                     supervisor_id,
                     start_date,
                     intended_end_date,
                     university,
                     erasmus_type_id
                 ) VALUES (
                     new_person_id,
                     NEW.supervisor_id,
                     NEW.start_date,
                     NEW.intended_end_date,
                     NEW.home_institution,
                     ltrim(NEW.post_category_id,'e-')::bigint
                 );
         END CASE;
         -- delete the form
         -- DELETE FROM registration.form WHERE form_id = OLD.id;
         RAISE INFO 'Importing a person';
         -- Record this one was imported
         UPDATE registration.form SET _imported_on = current_date WHERE form_id = OLD.id; 
     END IF;
     RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;
ALTER FUNCTION registration.people_registration_upd() OWNER TO dev;

CREATE TRIGGER registration_form_update INSTEAD OF UPDATE ON hotwire3."10_View/People/Registration" FOR EACH ROW EXECUTE PROCEDURE registration.people_registration_upd();
