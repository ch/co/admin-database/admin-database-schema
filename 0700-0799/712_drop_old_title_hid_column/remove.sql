ALTER TABLE public.title_hid ADD COLUMN title_hid character varying(10);
UPDATE public.title_hid SET title_hid = abbrev_title::varchar(10);
UPDATE public.title_hid SET title_hid = 'Prof (CMD)' WHERE abbrev_title = 'The Late Prof Sir';


CREATE OR REPLACE RULE hotwire3_people_titles_ins AS
    ON INSERT TO hotwire3."10_View/People/Titles" DO INSTEAD  INSERT INTO title_hid (title_hid, long_title_hid, website_title_hid, salutation_title_hid, abbrev_title, long_title, website_title, salutation_title)
  VALUES (new.title::character varying(10), new.long_title::character varying(40), new.title_shown_on_websites::character varying(40), new.title_for_written_salutation, new.title, new.long_title, new.title_shown_on_websites, new.title_for_written_salutation)
  RETURNING title_hid.title_id,
    title_hid.abbrev_title,
    title_hid.long_title,
    title_hid.website_title,
    title_hid.salutation_title;

CREATE OR REPLACE RULE hotwire3_people_titles_upd AS
    ON UPDATE TO hotwire3."10_View/People/Titles" DO INSTEAD  UPDATE title_hid SET title_hid = new.title::character varying(10), long_title_hid = new.long_title::character varying(40), website_title_hid = new.title_shown_on_websites::character varying(40), salutation_title_hid = new.title_for_written_salutation, abbrev_title = new.title, long_title = new.long_title, website_title = new.title_shown_on_websites, salutation_title = new.title_for_written_salutation
  WHERE title_hid.title_id = old.id;


ALTER TABLE public.title_hid ALTER COLUMN title_hid SET NOT NULL;
