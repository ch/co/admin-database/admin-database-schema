CREATE VIEW registration.completed_form_summary AS
SELECT
    form.uuid,
    form.first_names,
    form.surname,
    form.known_as,
    title.hid as title,
    form.date_of_birth,
    post_category.hid as post_category,
    nationality.nationality,
    form.job_title,
    rooms_by_uuid.rooms,
    phones.phones,
    college.hid as college,
    form.home_address,
    form.home_phone_number,
    form.mobile_number,
    form.email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution,
    form.mifare_areas,
    form.mifare_out_hours_access_required,
    form.mifare_aware_of_out_of_hours_rules,
    form.mifare_technician_signed,
    form.mifare_host_signed,
    supervisor.hid as supervisor,
    technician.hid as technician,
    form.deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    form.hide_from_website,
    form.previously_registered
FROM registration.form
LEFT JOIN registration.title_hid AS title on title.id = form.title_id
LEFT JOIN registration.college_hid AS college ON college.id = form.college_id
LEFT JOIN registration.supervisor_hid AS supervisor ON supervisor.id = form.department_host_id
LEFT JOIN registration.supervisor_hid AS technician ON technician.id = form.technician_id
JOIN registration.post_category_hid AS post_category ON post_category.id = form.post_category_id
JOIN (
    select uuid,string_agg(hid,', '::varchar) as rooms from registration.form, unnest(dept_room_id) as id join registration.room_hid using(id) group by uuid
) rooms_by_uuid USING (uuid)
JOIN (
    select uuid,string_agg(hid,', '::varchar) as phones from registration.form, unnest(dept_telephone_id) as id join registration.phone_hid using(id) group by uuid
) phones USING (uuid)
JOIN (
    select uuid,string_agg(hid,', '::varchar) as nationality from registration.form, unnest(nationality_id) as id join registration.nationality_hid using(id) group by uuid
) nationality USING (uuid)
WHERE submitted = 't'
;

ALTER TABLE registration.completed_form_summary OWNER TO dev;
GRANT SELECT ON registration.completed_form_summary TO starters_registration;
COMMENT ON VIEW registration.completed_form_summary IS 'Used by the registation webapp to generate nicelly formatted email summaries of each form with foreign keys resolved';
