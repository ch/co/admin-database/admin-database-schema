GRANT USAGE ON SCHEMA www TO www_leave_reporting;
CREATE VIEW www.leave_reporting AS
SELECT 
    person.crsid as person_crsid,
    person.email_address as person_email,
    coalesce(person.known_as, person.first_names) || ' ' || person.surname as person_name,
    supervisor.crsid as supervisor_crsid,
    supervisor.email_address as supervisor_email
FROM _latest_role_v12 lr
JOIN person ON lr.person_id = person.id
LEFT JOIN person supervisor ON lr.supervisor_id = supervisor.id
JOIN _physical_status_v3 ps USING (person_id)
WHERE lr.post_category = 'Assistant staff'
AND
ps.status_id = 'Current'
AND
lr.status = 'Current'
;

ALTER VIEW www.leave_reporting OWNER TO dev;
GRANT SELECT ON www.leave_reporting TO www_leave_reporting;
