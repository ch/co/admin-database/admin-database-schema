-- DROP VIEW public._crsid_and_role;
CREATE OR REPLACE VIEW public._crsid_and_role AS 
 SELECT person.crsid,
    person.image_lo::bigint AS image_oid,
    _latest_role.post_category,
    (COALESCE(person.known_as, person.first_names, ''::character varying)::text || ' '::text) || person.surname::text AS name
   FROM person
     LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
  WHERE person.crsid IS NOT NULL AND _latest_role.post_category IS NOT NULL;

ALTER TABLE public._crsid_and_role
  OWNER TO dev;
GRANT ALL ON TABLE public._crsid_and_role TO dev;
GRANT SELECT ON TABLE public._crsid_and_role TO leavers_trigger;
GRANT SELECT ON TABLE public._crsid_and_role TO ad_accounts;

-- DROP VIEW public._hardware_by_owner_crsid;
CREATE OR REPLACE VIEW public._hardware_by_owner_crsid AS 
 SELECT hardware.id,
    hardware.name,
    hardware.manufacturer,
    hardware.model,
    hardware.delete_after_date,
    room.name AS room,
    hardware_type_hid.hardware_type_hid AS type,
    person.crsid
   FROM hardware
     JOIN hardware_type_hid USING (hardware_type_id)
     JOIN person ON hardware.owner_id = person.id
     JOIN room ON hardware.room_id = room.id;

ALTER TABLE public._hardware_by_owner_crsid
  OWNER TO dev;
GRANT ALL ON TABLE public._hardware_by_owner_crsid TO dev;
GRANT SELECT, UPDATE ON TABLE public._hardware_by_owner_crsid TO leavers_trigger;
GRANT SELECT ON TABLE public._hardware_by_owner_crsid TO ad_accounts;

-- Rule: hardware_by_owner_up ON public._hardware_by_owner_crsid

-- DROP RULE hardware_by_owner_up ON public._hardware_by_owner_crsid;

CREATE OR REPLACE RULE hardware_by_owner_up AS
    ON UPDATE TO _hardware_by_owner_crsid DO INSTEAD  UPDATE hardware SET delete_after_date = new.delete_after_date
  WHERE hardware.id = old.id;

-- DROP VIEW public._system_image_by_user_crsid;
CREATE OR REPLACE VIEW public._system_image_by_user_crsid AS 
 SELECT system_image.id,
    ip_address.hostname,
    operating_system.os,
    system_image.hardware_id,
    hardware_type_hid.hardware_type_hid AS type,
    hardware.manufacturer,
    hardware.model,
    room.name AS room,
    person.crsid,
    system_image.research_group_id,
    COALESCE(research_group.active_directory_container, research_group.name) AS name
   FROM system_image
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
     LEFT JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
     LEFT JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN hardware_type_hid USING (hardware_type_id)
     LEFT JOIN room ON hardware.room_id = room.id
     LEFT JOIN person ON system_image.user_id = person.id
     LEFT JOIN research_group ON system_image.research_group_id = research_group.id;

ALTER TABLE public._system_image_by_user_crsid
  OWNER TO dev;
GRANT ALL ON TABLE public._system_image_by_user_crsid TO dev;
GRANT SELECT ON TABLE public._system_image_by_user_crsid TO leavers_trigger;
