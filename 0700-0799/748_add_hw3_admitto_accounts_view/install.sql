CREATE VIEW hotwire3."10_View/IT_Accounts/Admitto_Accounts" AS
SELECT 
    id,
    username,
    "uid",
    gecos,
    owner_id,
    description,
    is_disabled,
    expiry_date
FROM (
    SELECT
        admitto_account.id,
        username,
        "uid",
        gecos,
        person_id as owner_id,
        description,
        is_disabled,
        expiry_date
    FROM admitto_account
    LEFT JOIN person ON person_id = person.id
    WHERE current_user = person.crsid OR pg_has_role('cos','MEMBER')
) accounts_allowed_to_see;

ALTER VIEW hotwire3."10_View/IT_Accounts/Admitto_Accounts" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/Admitto_Accounts" TO cos;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/Admitto_Accounts" TO public;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/IT_Accounts/Admitto_Accounts', 'admitto_account' );
