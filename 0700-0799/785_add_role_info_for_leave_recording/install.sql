ALTER TABLE public.post_history ADD COLUMN percentage_of_fulltime_days REAL;
ALTER TABLE public.post_history ADD COLUMN weekends_are_standard_working_days BOOLEAN;
ALTER TABLE public.post_history ADD COLUMN works_flexitime BOOLEAN;  -- allowed to be null, as that should be the state for non-assistant staff, and for historic assistant staff posts
ALTER TABLE public.post_history ADD COLUMN standard_flexileave_rules_apply BOOLEAN;  -- allowed to be null, as that should be the state for non-assistant staff, or for assistant staff who do not work flexitime, or for historic posts with no information

-- set sensible defaults on existing records
UPDATE public.post_history SET percentage_of_fulltime_days = 100.0 WHERE (post_history.percentage_of_fulltime_hours = 100.0);
-- only update works_flexitime for current assistant staff.
-- actually don't: a few entries would be wrong until manually corrected.
-- can instead encourage review of current entries one by one at the time of enrolling them into the leave recording system,
-- which we've agreed will happen on a per person basis.
-- UPDATE public.post_history SET works_flexitime = TRUE, standard_flexileave_rules_apply = TRUE WHERE post_history.id IN (SELECT role_id FROM _all_roles_v13 WHERE (_all_roles_v13.role_tablename = 'post_history') AND (_all_roles_v13.role_id = post_history.id) AND (_all_roles_v13.status = 'Current')) AND (post_history.staff_category_id = 3);

COMMENT ON COLUMN public.post_history.percentage_of_fulltime_days IS 'Used (only) in the leave recording system for determining pro-rata annual leave allowances. A person who usually works 4 days a week every week would have 80% here (no matter how many hours they work on those days).';
COMMENT ON COLUMN public.post_history.weekends_are_standard_working_days IS 'Used (only) in the leave recording system for determining how to manage and account for leave booked on weekends. Only assistant staff will use that system at least initially, so this field (at the time of writing) has no effect for other categories of staff.';
COMMENT ON COLUMN public.post_history.works_flexitime IS 'Used (only) in the leave recording system. Set to null for non-assistant staff roles, for which this is not applicable. Usually true for assistant staff roles, but with some exceptions.';
COMMENT ON COLUMN public.post_history.standard_flexileave_rules_apply IS 'Used (only) in the leave recording system. Set to null for non-flexitime roles. Usually true for flexitime roles, but allows certain roles to be marked as not subject to the "at most 1 day of flexi-leave per month" rule.';

DROP VIEW hotwire3. "10_View/Roles/Post_History";

CREATE OR REPLACE VIEW hotwire3. "10_View/Roles/Post_History" AS
SELECT
    a.id,
    a.staff_category_id,
    a.person_id,
    a.ro_role_status_id,
    a.supervisor_id,
    a.mentor_id,
    a.external_mentor,
    a.start_date_for_continuous_employment_purposes,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.funding_end_date,
    a.force_role_status_to_past,
    a.staff_review_due,
    a.chem,
    a.paid_through_payroll,
    a.hours_worked,
    a.percentage_of_fulltime_hours,
    a.percentage_of_fulltime_days,
    a.weekends_are_standard_working_days,
    a.works_flexitime,
    a.standard_flexileave_rules_apply,
    a.research_grant_number,
    a.sponsor,
    a.probation_period,
    a.date_of_first_probation_meeting,
    a.date_of_second_probation_meeting,
    a.date_of_third_probation_meeting,
    a.date_of_fourth_probation_meeting,
    a.first_probation_meeting_comments,
    a.second_probation_meeting_comments,
    a.third_probation_meeting_comments,
    a.fourth_probation_meeting_comments,
    a.probation_outcome,
    a.date_probation_letters_sent,
    a.job_title,
    a._person
FROM (
    SELECT
        post_history.id,
        post_history.staff_category_id,
        post_history.person_id,
        _all_roles.status AS ro_role_status_id,
        post_history.supervisor_id,
        post_history.mentor_id,
        post_history.external_mentor,
        person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes,
        post_history.start_date,
        post_history.intended_end_date,
        post_history.end_date,
        post_history.funding_end_date,
        person.next_review_due AS staff_review_due,
        post_history.chem,
        post_history.paid_by_university AS paid_through_payroll,
        post_history.hours_worked,
        post_history.percentage_of_fulltime_hours,
        post_history.percentage_of_fulltime_days,
        post_history.weekends_are_standard_working_days,
        CASE
            WHEN post_history.staff_category_id = 3 THEN post_history.works_flexitime
            ELSE NULL
        END AS works_flexitime,
        CASE
            WHEN post_history.staff_category_id = 3 AND post_history.works_flexitime THEN post_history.standard_flexileave_rules_apply
            ELSE NULL
        END AS standard_flexileave_rules_apply,
        post_history.probation_period::text AS probation_period,
        post_history.date_of_first_probation_meeting,
        post_history.date_of_second_probation_meeting,
        post_history.date_of_third_probation_meeting,
        post_history.date_of_fourth_probation_meeting,
        post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments,
        post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments,
        post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments,
        post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments,
        post_history.probation_outcome::text AS probation_outcome,
        post_history.date_probation_letters_sent,
        post_history.research_grant_number::text AS research_grant_number,
        post_history.filemaker_funding::text AS sponsor,
        post_history.job_title,
        post_history.force_role_status_to_past,
        person_hid.person_hid AS _person
    FROM
        post_history
    LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id
        AND _all_roles.role_tablename = 'post_history'::text
    JOIN person ON post_history.person_id = person.id
    JOIN person_hid ON post_history.person_id = person_hid.person_id) a
ORDER BY
    a._person,
    a.start_date DESC;

ALTER TABLE hotwire3. "10_View/Roles/Post_History" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Roles/Post_History" TO dev;

GRANT SELECT ON TABLE hotwire3. "10_View/Roles/Post_History" TO mgmt_ro;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Roles/Post_History" TO hr;

-- Rule: hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_del ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_del AS ON DELETE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    DELETE
FROM
    post_history
WHERE
    post_history.id = old.id;

CREATE OR REPLACE FUNCTION leave_recording.effective_new_works_flexitime(staff_category_id BIGINT, works_flexitime BOOLEAN) RETURNS BOOLEAN AS $$
BEGIN
    RETURN CASE
        WHEN (staff_category_id = 3) THEN works_flexitime
        ELSE NULL
    END;
END;
$$ LANGUAGE plpgsql;

-- Rule: hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE hotwire3_view_post_history_update_rules ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire3_view_post_history_update_rules AS ON UPDATE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes,
        next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

UPDATE
    post_history
SET
    staff_category_id = new.staff_category_id,
    supervisor_id = new.supervisor_id,
    mentor_id = new.mentor_id,
    external_mentor = new.external_mentor,
    start_date = new.start_date,
    intended_end_date = new.intended_end_date,
    end_date = new.end_date,
    funding_end_date = new.funding_end_date,
    chem = new.chem,
    paid_by_university = new.paid_through_payroll,
    hours_worked = new.hours_worked,
    percentage_of_fulltime_hours = new.percentage_of_fulltime_hours,
    percentage_of_fulltime_days = new.percentage_of_fulltime_days,
    weekends_are_standard_working_days = new.weekends_are_standard_working_days,
    works_flexitime = leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    standard_flexileave_rules_apply = CASE
        WHEN new.staff_category_id = 3 AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    probation_period = new.probation_period::character varying(80),
    date_of_first_probation_meeting = new.date_of_first_probation_meeting,
    date_of_second_probation_meeting = new.date_of_second_probation_meeting,
    date_of_third_probation_meeting = new.date_of_third_probation_meeting,
    date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting,
    first_probation_meeting_comments = new.first_probation_meeting_comments::character varying(500),
    second_probation_meeting_comments = new.second_probation_meeting_comments::character varying(500),
    third_probation_meeting_comments = new.third_probation_meeting_comments::character varying(500),
    fourth_probation_meeting_comments = new.fourth_probation_meeting_comments::character varying(500),
    probation_outcome = new.probation_outcome::character varying(200),
    date_probation_letters_sent = new.date_probation_letters_sent,
    research_grant_number = new.research_grant_number::character varying(500),
    filemaker_funding = new.sponsor::character varying(500),
    job_title = new.job_title,
    force_role_status_to_past = new.force_role_status_to_past
WHERE
    post_history.id = old.id;

);

-- Rule: post_history_ins ON hotwire3."10_View/Roles/Post_History"
-- DROP RULE post_history_ins ON hotwire3."10_View/Roles/Post_History";

CREATE OR REPLACE RULE post_history_ins AS ON INSERT TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes, next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

INSERT INTO post_history (staff_category_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, percentage_of_fulltime_days, weekends_are_standard_working_days, works_flexitime, standard_flexileave_rules_apply, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, job_title, force_role_status_to_past)
    VALUES (new.staff_category_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_through_payroll, new.hours_worked, new.percentage_of_fulltime_hours, new.percentage_of_fulltime_days,
    new.weekends_are_standard_working_days,
    leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime),
    CASE
        WHEN (new.staff_category_id = 3) AND leave_recording.effective_new_works_flexitime(new.staff_category_id, new.works_flexitime) THEN new.standard_flexileave_rules_apply
        ELSE NULL
    END,
    new.probation_period::character varying(80), new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments::character varying(500), new.second_probation_meeting_comments::character varying(500), new.third_probation_meeting_comments::character varying(500), new.fourth_probation_meeting_comments::character varying(500), new.probation_outcome::character varying(200), new.date_probation_letters_sent, new.research_grant_number::character varying(500), new.sponsor::character varying(500), new.job_title, new.force_role_status_to_past)
RETURNING
    post_history.id, post_history.staff_category_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.force_role_status_to_past, NULL::date AS date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.percentage_of_fulltime_days, post_history.weekends_are_standard_working_days, post_history.works_flexitime, post_history.standard_flexileave_rules_apply, post_history.research_grant_number::text AS research_grant_number, post_history.filemaker_funding::text AS filemaker_funding, post_history.probation_period::text AS probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments, post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments, post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments, post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments, post_history.probation_outcome::text AS probation_outcome, post_history.date_probation_letters_sent, post_history.job_title, NULL::text AS text;

);
