CREATE SCHEMA leave_recording
  AUTHORIZATION dev;

GRANT ALL ON SCHEMA leave_recording TO dev;
GRANT USAGE ON SCHEMA leave_recording TO public;
GRANT USAGE ON SCHEMA leave_recording TO _pgbackup;
