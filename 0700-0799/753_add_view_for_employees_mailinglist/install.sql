CREATE VIEW _chem_employees_mailinglist AS
SELECT DISTINCT
    person.email_address,
    person.crsid,
    person_hid.person_hid
FROM post_history
JOIN person ON post_history.person_id = person.id
JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id AND _all_roles.role_tablename = 'post_history'
JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
JOIN person_hid on person_hid.person_id = person.id

WHERE _all_roles.status = 'Current' AND _physical_status.status_id = 'Current' AND NOT (person.email_address IS NULL OR person.email_address = '')
;

ALTER VIEW _chem_employees_mailinglist OWNER TO dev;

GRANT SELECT ON _chem_employees_mailinglist TO cos, mailinglists, www_sites, mailinglist_readers, leavers_trigger;
