CREATE VIEW registration.post_category_hid AS
 SELECT 'e-'::text || erasmus_type_hid.erasmus_type_id::text AS id,
    erasmus_type_hid.erasmus_type_hid::text AS hid
   FROM public.erasmus_type_hid
UNION
 SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS id,
    visitor_type_hid.visitor_type_hid AS hid
   FROM public.visitor_type_hid
UNION
 SELECT 'pg-'::text || postgraduate_studentship_type_hid.postgraduate_studentship_type_id::text AS id,
    postgraduate_studentship_type_hid.postgraduate_studentship_type_hid AS hid
   FROM public.postgraduate_studentship_type_hid
UNION
 SELECT 'sc-'::text || staff_category_hid.staff_category_id::text AS id,
    staff_category_hid.staff_category_hid AS hid
   FROM public.staff_category_hid;

ALTER VIEW registration.post_category_hid OWNER TO dev;
GRANT SELECT ON registration.post_category_hid TO starters_registration;
