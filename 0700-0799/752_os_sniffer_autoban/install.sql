CREATE OR REPLACE FUNCTION apps.autoban_macaddress(_mac macaddr, _msg varchar) RETURNS VOID AS $$
DECLARE
	_old_vid integer;
	_row_id integer;
BEGIN
	SELECT vid INTO _old_vid FROM shadow_mac_to_vlan WHERE mac=_mac;

	IF(_old_vid=191) THEN
	  -- no-op
	ELSE	
		INSERT INTO shadow_mac_to_vlan (mac, vid) VALUES (_mac, 191)
			ON CONFLICT ON CONSTRAINT uniq_mac DO UPDATE SET vid=191 WHERE shadow_mac_to_vlan.mac=_mac;
	
		SELECT id INTO _row_id FROM shadow_mac_to_vlan WHERE mac=_mac;
		INSERT INTO network_vlan_change (mac, shadow_mac_to_vlan_id, notes, timestamp, old_vid, new_vid, infraction_type_id) VALUES
			(_mac, _row_id, _msg, current_timestamp, _old_vid, 191, 1);
	END IF;


END;
$$ language plpgsql;

ALTER FUNCTION apps.autoban_macaddress(macaddr, varchar)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION apps.autoban_macaddress(macaddr, varchar) TO os_sniffer_autoban;

GRANT SELECT, INSERT on TABLE network_vlan_change to os_sniffer_autoban;
GRANT SELECT, INSERT, UPDATE on TABLE shadow_mac_to_vlan to os_sniffer_autoban;
