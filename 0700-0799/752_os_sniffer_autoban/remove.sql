DROP FUNCTION apps.autoban_macaddress(macaddr, varchar);

REVOKE ALL PRIVILEGES ON network_vlan_change FROM os_sniffer_autoban;
REVOKE ALL PRIVILEGES ON shadow_mac_to_vlan FROM os_sniffer_autoban;

