DROP VIEW hotwire3."10_View/Roles/Erasmus_Students";

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Erasmus_Students" AS 
 SELECT a.id,
    a.person_id,
    a.erasmus_type_id,
    a.supervisor_id,
    a.email_address,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.cambridge_college_id,
    a.university,
    a.ro_role_status,
    a.notes,
    a.force_role_status_to_past,
    a._surname,
    a._first_names
   FROM ( SELECT erasmus_socrates_studentship.id,
            erasmus_socrates_studentship.person_id,
            erasmus_socrates_studentship.erasmus_type_id,
            erasmus_socrates_studentship.supervisor_id,
            person.email_address,
            erasmus_socrates_studentship.start_date,
            erasmus_socrates_studentship.intended_end_date,
            erasmus_socrates_studentship.end_date,
            person.cambridge_college_id,
            erasmus_socrates_studentship.university,
            _all_roles.status AS ro_role_status,
            erasmus_socrates_studentship.notes::text AS notes,
            erasmus_socrates_studentship.force_role_status_to_past,
            person.surname AS _surname,
            person.first_names AS _first_names
           FROM erasmus_socrates_studentship
             JOIN person ON person.id = erasmus_socrates_studentship.person_id
             LEFT JOIN _all_roles_v13 _all_roles ON _all_roles.role_id = erasmus_socrates_studentship.id AND _all_roles.role_tablename = 'erasmus_socrates_studentship'::text) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire3."10_View/Roles/Erasmus_Students"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Roles/Erasmus_Students" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Roles/Erasmus_Students" TO student_management;

-- Rule: hotwire3_view_erasmus_students_del ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP RULE hotwire3_view_erasmus_students_del ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD  DELETE FROM erasmus_socrates_studentship
  WHERE erasmus_socrates_studentship.id = old.id;

-- Rule: hotwire3_view_erasmus_students_ins ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP RULE hotwire3_view_erasmus_students_ins ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD  INSERT INTO erasmus_socrates_studentship (person_id, erasmus_type_id, supervisor_id, intended_end_date, end_date, start_date, university, notes, force_role_status_to_past)
  VALUES (new.person_id, new.erasmus_type_id, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.university, new.notes::character varying(160), new.force_role_status_to_past)
  RETURNING erasmus_socrates_studentship.id,
    erasmus_socrates_studentship.erasmus_type_id,
    erasmus_socrates_studentship.person_id,
    erasmus_socrates_studentship.supervisor_id,
    NULL::character varying(48) AS "varchar",
    erasmus_socrates_studentship.start_date,
    erasmus_socrates_studentship.intended_end_date,
    erasmus_socrates_studentship.end_date,
    NULL::bigint AS int8,
    erasmus_socrates_studentship.university,
    NULL::character varying(10) AS "varchar",
    erasmus_socrates_studentship.notes::text AS notes,
    erasmus_socrates_studentship.force_role_status_to_past,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar";

-- Rule: hotwire3_view_erasmus_students_upd ON hotwire3."10_View/Roles/Erasmus_Students"

-- DROP RULE hotwire3_view_erasmus_students_upd ON hotwire3."10_View/Roles/Erasmus_Students";

CREATE OR REPLACE RULE hotwire3_view_erasmus_students_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Erasmus_Students" DO INSTEAD ( UPDATE erasmus_socrates_studentship SET person_id = new.person_id, erasmus_type_id = new.erasmus_type_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, university = new.university, notes = new.notes::character varying(160), force_role_status_to_past = new.force_role_status_to_past
  WHERE erasmus_socrates_studentship.id = old.id;
 UPDATE person SET email_address = new.email_address, cambridge_college_id = new.cambridge_college_id
  WHERE person.id = old.person_id;
);


