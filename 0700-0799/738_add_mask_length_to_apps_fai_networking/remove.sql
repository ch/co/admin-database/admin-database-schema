DROP VIEW apps.fai_networking;

CREATE OR REPLACE VIEW apps.fai_networking AS 
 SELECT ip_address.hostname AS fqdn,
    split_part(ip_address.hostname::text, '.'::text, 1) AS hostname,
    ip_address.ip,
    subnet.router,
    netmask(subnet.network_address) AS netmask,
    vlan.vid,
    ARRAY[system_image.wired_mac_1, system_image.wired_mac_2, system_image.wired_mac_3, system_image.wired_mac_4, system_image.wireless_mac] AS macs
   FROM system_image
     JOIN mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = system_image.id
     JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
     JOIN subnet ON ip_address.subnet_id = subnet.id
     JOIN vlan ON subnet.vlan_id = vlan.id;

ALTER TABLE apps.fai_networking OWNER TO dev;
GRANT ALL ON TABLE apps.fai_networking TO dev;
GRANT SELECT ON TABLE apps.fai_networking TO cos;
GRANT SELECT ON TABLE apps.fai_networking TO osbuilder;

