COMMENT ON COLUMN public.admitto_account.uid IS NULL;
ALTER TABLE admitto_account ADD COLUMN "gid" INT UNIQUE;
GRANT SELECT(gid), UPDATE(gid), INSERT(gid) ON public.admitto_account TO ad_accounts;
