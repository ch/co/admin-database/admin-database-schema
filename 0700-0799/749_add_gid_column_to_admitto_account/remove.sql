ALTER TABLE admitto_account DROP COLUMN "gid";

COMMENT ON COLUMN public.admitto_account.uid IS 'This is both the uid and the gid of the primary Unix group';
