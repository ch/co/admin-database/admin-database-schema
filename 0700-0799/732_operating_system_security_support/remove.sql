-- This is a slony table so this change must be applied via slony methods
ALTER TABLE operating_system DROP COLUMN has_security_support;
