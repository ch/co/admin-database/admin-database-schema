CREATE VIEW registration.current_people_crsids AS
SELECT id,crsid,coalesce(first_names||' ','')||surname as name, email_address
FROM person
JOIN _physical_status_v3 ps ON ps.person_id = person.id
WHERE ps.status_id = 'Current'
AND person.crsid IS NOT NULL AND person.email_address is not null;

ALTER VIEW registration.current_people_crsids OWNER TO dev;

GRANT SELECT ON registration.current_people_crsids TO starters_registration;
