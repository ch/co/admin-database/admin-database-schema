CREATE VIEW hotwire3.serves_room_hid AS
SELECT room_id AS serves_room_id, room_hid AS serves_room_hid FROM hotwire3.room_hid;
ALTER VIEW hotwire3.serves_room_hid OWNER TO dev;
GRANT SELECT ON hotwire3.serves_room_hid TO ro_hid;

DROP VIEW hotwire3."10_View/Network/Cabinets";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Cabinets" AS 
 SELECT cabinet.id,
    cabinet.name,
    cabinet.email_local_part,
    cabinet.notes,
    cabinet.room_id,
    ARRAY(
        SELECT room_id FROM mm_cabinet_serves_room WHERE mm_cabinet_serves_room.cabinet_id = cabinet.id
    ) AS serves_room_id,
    cabinet.cabno,
    cabinet.in_use,
    cabinet.depth,
    cabinet.fibre_u,
    cabinet.patch_u,
    cabinet.pots_u,
    cabinet.other_u,
    cabinet.height_u,
    cabinet.ports,
    cabinet.max_ports,
    cabinet.sw48,
    cabinet.sw24,
    cabinet.cabname,
    hotwire3.to_hwsubviewb('10_View/Network/_Switches_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Switches/Switches'::varchar,'switch_id'::varchar,'id'::varchar) AS "Switches",
    hotwire3.to_hwsubviewb('10_View/Network/_Patch_panels_for_cabinet'::varchar, 'cabinet_id'::varchar, '10_View/Network/Patch_Panels'::varchar,'patch_panel_id'::varchar,'id'::varchar) AS "Patch_Panels",
    hotwire3.to_hwsubviewb('10_View/Network/Fibre_Panels'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panels'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panel",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/10_Fibre_Runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs",
    hotwire3.to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Cores'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores"
   FROM cabinet
   ORDER BY name;

ALTER TABLE hotwire3."10_View/Network/Cabinets"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Cabinets" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Cabinets" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Cabinets_del" AS
    ON DELETE TO hotwire3."10_View/Network/Cabinets" DO INSTEAD  DELETE FROM cabinet
  WHERE cabinet.id = old.id;

CREATE FUNCTION hotwire3.network_cabinet_upd() RETURNS TRIGGER AS
$$
DECLARE
    v_id bigint;
BEGIN
    IF NEW.id IS NOT NULL THEN
        v_id = NEW.id;
        UPDATE cabinet SET name = new.name, email_local_part = new.email_local_part, notes = new.notes, room_id = new.room_id, cabno = new.cabno, in_use = new.in_use, depth = new.depth, fibre_u = new.fibre_u, pots_u = new.pots_u, other_u = new.other_u, height_u = new.height_u, ports = new.ports, max_ports = new.max_ports, sw48 = new.sw48, sw24 = new.sw24, patch_u = new.patch_u, cabname = new.cabname WHERE id = NEW.id;
    ELSE
        INSERT INTO cabinet (name, email_local_part, notes, room_id, cabno, in_use, depth, fibre_u, patch_u, pots_u, other_u, height_u, ports, max_ports, sw48, sw24, cabname)
        VALUES (new.name, new.email_local_part, new.notes, new.room_id, new.cabno, new.in_use, new.depth, new.fibre_u, new.patch_u, new.pots_u, new.other_u, new.height_u, new.ports, new.max_ports, new.sw48, new.sw24, new.cabname) RETURNING id INTO v_id;
        NEW.id := v_id;
    END IF;
    PERFORM fn_mm_array_update(NEW.serves_room_id,'mm_cabinet_serves_room'::varchar,'cabinet_id'::varchar,'room_id'::varchar,v_id);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.network_cabinet_upd() OWNER TO dev;

CREATE TRIGGER hotwire3_cabinet_view_trig INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/Network/Cabinets" FOR EACH ROW EXECUTE PROCEDURE hotwire3.network_cabinet_upd();
