CREATE OR REPLACE VIEW hotwire3."10_View/Email/Mailing_List_OptIns" AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.generation_view_name AS ro_generation_view_name, mailinglist.notes::text AS notes, pg_views.definition AS ro_generation_view_code, mailinglist.extra_addresses, ARRAY( SELECT mm_mailinglist_include_person.include_person_id
           FROM mm_mailinglist_include_person
          WHERE mm_mailinglist_include_person.mailinglist_id = mailinglist.id) AS include_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE hotwire3."10_View/Email/Mailing_List_OptIns"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Email/Mailing_List_OptIns" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Email/Mailing_List_OptIns" TO cos;

-- Rule: hotwire3_view_mailing_lists_optins_upd ON hotwire3."10_View/Email/Mailing_List_OptIns"

-- DROP RULE hotwire3_view_mailing_lists_optins_upd ON hotwire3."10_View/Email/Mailing_List_OptIns";

CREATE OR REPLACE RULE hotwire3_view_mailing_lists_optins_upd AS
    ON UPDATE TO hotwire3."10_View/Email/Mailing_List_OptIns" DO INSTEAD ( SELECT fn_mm_array_update(new.include_person_id, old.include_person_id, 'mm_mailinglist_include_person'::character varying, 'mailinglist_id'::character varying, 'include_person_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE mailinglist SET name = new.name, notes = new.notes::character varying(500), extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

CREATE OR REPLACE VIEW hotwire3."10_View/Email/Mailing_List_OptOuts" AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.notes::text AS notes, mailinglist.generation_view_name AS ro_generation_view_name, pg_views.definition AS ro_generation_view_code, mailinglist.extra_addresses, ARRAY( SELECT mm_mailinglist_exclude_person.exclude_person_id
           FROM mm_mailinglist_exclude_person
          WHERE mm_mailinglist_exclude_person.mailinglist_id = mailinglist.id) AS exclude_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE hotwire3."10_View/Email/Mailing_List_OptOuts"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Email/Mailing_List_OptOuts" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Email/Mailing_List_OptOuts" TO cos;

-- Rule: hotwire3_view_mailing_lists_optouts_upd ON hotwire3."10_View/Email/Mailing_List_OptOuts"

-- DROP RULE hotwire3_view_mailing_lists_optouts_upd ON hotwire3."10_View/Email/Mailing_List_OptOuts";

CREATE OR REPLACE RULE hotwire3_view_mailing_lists_optouts_upd AS
    ON UPDATE TO hotwire3."10_View/Email/Mailing_List_OptOuts" DO INSTEAD ( SELECT fn_mm_array_update(new.exclude_person_id, old.exclude_person_id, 'mm_mailinglist_exclude_person'::character varying, 'mailinglist_id'::character varying, 'exclude_person_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE mailinglist SET name = new.name, notes = new.notes::character varying(500), extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Email/Mailing_List_OptOuts', 'mailinglist');
INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Email/Mailing_List_OptIns', 'mailinglist');

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Mailing_Lists';
DROP VIEW hotwire3."10_View/Email/_Mailing_List_Membership";
DROP VIEW hotwire3."10_View/Email/Mailing_Lists";
DROP FUNCTION public.generate_mailinglist_membership(listname varchar);
