DROP VIEW IF EXISTS hotwire3."10_View/Email/Mailing_List_OptIns";
DROP VIEW IF EXISTS hotwire3."10_View/Email/Mailing_List_OptOuts";
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Mailing_List_OptIns';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Email/Mailing_List_OptOuts';


CREATE OR REPLACE FUNCTION public.generate_mailinglist_membership(listname varchar) RETURNS SETOF text AS
$BODY$
DECLARE
    membership text;
    viewname varchar;
    extras varchar;
BEGIN
    SELECT generation_view_name FROM mailinglist WHERE name = listname INTO viewname;
    IF NOT FOUND THEN
        RETURN;
    ELSE
        RETURN QUERY EXECUTE format('select * from ( SELECT regexp_split_to_table(extra_addresses,%L) from mailinglist where name = %L) mails where mails.regexp_split_to_table like %L','[\s]+',listname,'%@%');
        IF LEFT(viewname,length('fn_standard_rg_mailing_list')) = 'fn_standard_rg_mailing_list' THEN
            RETURN QUERY EXECUTE 'SELECT email_address::text FROM '||viewname||format(' WHERE email_address like %L ORDER BY email_address','%@%');
        ELSE
            RETURN QUERY EXECUTE format('SELECT email_address::text FROM %I WHERE email_address like %L',viewname,'%@%');
        END IF;
    END IF;
    RETURN;
END;
$BODY$ LANGUAGE plpgsql SECURITY DEFINER SET search_path = public, pg_temp;
GRANT CREATE ON SCHEMA public TO mailinglists;
ALTER FUNCTION generate_mailinglist_membership(varchar) OWNER TO mailinglists;
REVOKE CREATE ON SCHEMA public FROM mailinglists;
REVOKE ALL ON FUNCTION generate_mailinglist_membership(varchar) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION generate_mailinglist_membership(varchar) TO dev,cos;

CREATE VIEW hotwire3."10_View/Email/_Mailing_List_Membership" AS
WITH members AS (
SELECT
   id,
   name,
   generate_mailinglist_membership(name) as email
FROM mailinglist
order by name,email )
SELECT
    name || '-'|| email as id,
    id as _list_id,
    name,
    email
from members;
ALTER VIEW hotwire3."10_View/Email/_Mailing_List_Membership" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Email/_Mailing_List_Membership" TO cos;

CREATE OR REPLACE VIEW hotwire3."10_View/Email/Mailing_Lists" AS
SELECT
    mailinglist.id,
    mailinglist.name,
    mailinglist.notes::text AS notes,
    mailinglist.generation_view_name AS ro_generation_view_name,
    mailinglist.extra_addresses,
    ARRAY (
        SELECT
            mm_mailinglist_include_person.include_person_id
        FROM
            mm_mailinglist_include_person
        WHERE
            mm_mailinglist_include_person.mailinglist_id = mailinglist.id
    ) AS include_person_id,
    ARRAY (
        SELECT
            mm_mailinglist_exclude_person.exclude_person_id
        FROM
            mm_mailinglist_exclude_person
        WHERE
            mm_mailinglist_exclude_person.mailinglist_id = mailinglist.id
    ) AS exclude_person_id,
    mailinglist.auto_upload,
    pg_views.definition::text AS ro_generation_view_code,
    hotwire3.to_hwsubviewb('10_View/Email/_Mailing_List_Membership','_list_id',null::varchar,null::varchar,null::varchar) as list_members
FROM
    mailinglist
    LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE hotwire3."10_View/Email/Mailing_Lists" OWNER TO dev;

CREATE RULE hotwire3_view_mailing_lists_upd AS
    ON UPDATE TO hotwire3."10_View/Email/Mailing_Lists" DO INSTEAD (
    SELECT fn_mm_array_update(new.include_person_id, old.include_person_id, 'mm_mailinglist_include_person'::character varying, 'mailinglist_id'::character varying, 'include_person_id'::character varying, old.id) AS fn_mm_array_update;
    SELECT fn_mm_array_update(new.exclude_person_id, old.exclude_person_id, 'mm_mailinglist_exclude_person'::character varying, 'mailinglist_id'::character varying, 'exclude_person_id'::character varying, old.id) AS fn_mm_array_update;
     UPDATE mailinglist SET name = new.name, notes = new.notes::character varying(500), extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
       WHERE mailinglist.id = old.id;
       );


GRANT ALL ON TABLE hotwire3."10_View/Email/Mailing_Lists" TO dev;
INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ( '10_View/Email/Mailing_Lists', 'mailinglist');

GRANT SELECT, UPDATE ON TABLE hotwire3. "10_View/Email/Mailing_Lists" TO cos;
