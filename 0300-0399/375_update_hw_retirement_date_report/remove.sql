DROP VIEW hotwire."30_Report/Retirement_Dates";

CREATE 
OR REPLACE VIEW hotwire."30_Report/Retirement_Dates" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.date_of_birth, 
  a.staff_category_id, 
  a.start_date, 
  a.end_date, 
  a.intended_end_date, 
  a.funding_end_date, 
  a.job_title, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      post_history.id, 
      post_history.person_id, 
      person.date_of_birth, 
      staff_post.staff_category_id, 
      post_history.start_date, 
      post_history.end_date, 
      post_history.intended_end_date, 
      post_history.funding_end_date, 
      post_history.job_title, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      post_history 
      LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id 
      LEFT JOIN person ON post_history.person_id = person.id 
      LEFT JOIN staff_category ON staff_category.id = staff_post.staff_category_id 
    WHERE 
      (
        person.left_but_no_leaving_date_given = false 
        OR person.left_but_no_leaving_date_given IS NULL
      ) 
      AND COALESCE(
        post_history.end_date, post_history.intended_end_date
      ) > 'now' :: text :: date 
      AND (
        post_history.funding_end_date IS NULL 
        OR post_history.funding_end_date > COALESCE(
          post_history.funding_end_date, post_history.intended_end_date
        )
      ) 
      AND (
        staff_category.category :: text = 'Academic staff' :: text 
        OR staff_category.category :: text = 'Academic-related staff' :: text 
        OR staff_category.category :: text = 'Assistant staff' :: text
      )
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;
ALTER TABLE 
  hotwire."30_Report/Retirement_Dates" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Retirement_Dates" TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Retirement_Dates" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Retirement_Dates" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Retirement_Dates" TO hr;

