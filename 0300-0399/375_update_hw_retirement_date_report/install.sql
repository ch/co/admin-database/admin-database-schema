DROP VIEW hotwire."30_Report/Retirement_Dates";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Retirement_Dates" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.date_of_birth, 
  a.post_category, 
  a.start_date, 
  a.end_date, 
  a.intended_end_date, 
  a.funding_end_date, 
  a.job_title, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      lr.role_id as id, 
      lr.person_id, 
      person.date_of_birth, 
      lr.post_category,
      lr.start_date, 
      lr.end_date, 
      lr.intended_end_date, 
      lr.funding_end_date, 
      post_history.job_title, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      _latest_role_v12 lr 
      LEFT JOIN person ON lr.person_id = person.id 
      LEFT join post_history ON lr.role_id = post_history.id
    WHERE 
      (
        person.left_but_no_leaving_date_given = false 
        OR person.left_but_no_leaving_date_given IS NULL
      ) 
      AND lr.status = 'Current'
      AND lr.role_tablename = 'post_history'
      AND COALESCE(
        lr.end_date, lr.intended_end_date
      ) > 'now' :: text :: date 
      AND (
        lr.funding_end_date IS NULL 
        OR lr.funding_end_date > COALESCE(
          lr.funding_end_date, lr.intended_end_date
        )
      ) 
      AND (
        lr.post_category :: text = 'Academic staff' :: text 
        OR lr.post_category :: text = 'Academic-related staff' :: text 
        OR lr.post_category :: text = 'Assistant staff' :: text
      )
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;
ALTER TABLE 
  hotwire."30_Report/Retirement_Dates" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Retirement_Dates" TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Retirement_Dates" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Retirement_Dates" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Retirement_Dates" TO hr;

