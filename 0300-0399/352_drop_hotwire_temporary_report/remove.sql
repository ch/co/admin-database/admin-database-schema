CREATE OR REPLACE VIEW hotwire."30_Report/people_last_aug" AS 
 SELECT l.person_id AS id, ph.person_hid, p.surname, p.first_names, ARRAY( SELECT room.name
           FROM room
      LEFT JOIN mm_person_room mm ON mm.room_id = room.id
     WHERE mm.person_id = l.person_id) AS room, s.supervisor_hid, l.post_category
   FROM _all_roles_v10 l
   JOIN person p ON p.id = l.person_id
   LEFT JOIN person_hid ph ON p.id = ph.person_id
   LEFT JOIN supervisor_hid s ON l.supervisor_id = s.supervisor_id
  WHERE l.start_date < '2013-08-01'::date AND l.intended_end_date > '2013-08-01'::date;

ALTER TABLE hotwire."30_Report/people_last_aug"
  OWNER TO cen1001;

