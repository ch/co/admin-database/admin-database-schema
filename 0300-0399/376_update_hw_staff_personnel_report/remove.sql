DROP VIEW hotwire."30_Report/Staff_Personnel";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Staff_Personnel" AS 
SELECT 
  a.id, 
  a.supervisor, 
  a.supervisee, 
  a.staff_category_id, 
  a.job_title, 
  a.continuous_employment_start_date, 
  a.end_date, 
  a.mentor 
FROM 
  (
    SELECT 
      (person.id :: text || '-' :: text) || supervisee.id :: text AS id, 
      supervisor_hid.supervisor_hid AS supervisor, 
      supervisee_hid.supervisee_hid AS supervisee, 
      staff_post.staff_category_id, 
      post_history.job_title, 
      supervisee.continuous_employment_start_date, 
      COALESCE(
        post_history.funding_end_date, post_history.intended_end_date
      ) AS end_date, 
      mentor_hid.mentor_hid AS mentor 
    FROM 
      person 
      JOIN _latest_role_v8 ON _latest_role_v8.supervisor_id = person.id 
      JOIN post_history ON _latest_role_v8.role_id = post_history.id 
      JOIN person supervisee ON post_history.person_id = supervisee.id 
      JOIN staff_post ON post_history.staff_post_id = staff_post.id 
      JOIN _physical_status_v2 supervisor_status ON person.id = supervisor_status.person_id 
      JOIN _physical_status_v2 supervisee_status ON supervisee.id = supervisee_status.person_id 
      JOIN supervisor_hid ON supervisor_hid.supervisor_id = person.id 
      JOIN supervisee_hid ON supervisee_hid.supervisee_id = supervisee.id 
      LEFT JOIN mentor_hid ON mentor_hid.mentor_id = post_history.mentor_id 
    WHERE 
      supervisor_status.status_id :: text = 'Current' :: text 
      AND supervisee_status.status_id :: text = 'Current' :: text 
      AND _latest_role_v8.role_tablename = 'post_history' :: text
  ) a 
ORDER BY 
  a.supervisor, 
  a.supervisee;
ALTER TABLE 
  hotwire."30_Report/Staff_Personnel" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Personnel" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Personnel" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Personnel" TO hr;

