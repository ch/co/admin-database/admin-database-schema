DROP VIEW hotwire."30_Report/Staff_Personnel";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Staff_Personnel" AS 
SELECT 
  a.id, 
  a.supervisor, 
  a.supervisee, 
  a.staff_category_id, 
  a.job_title, 
  a.continuous_employment_start_date, 
  a.end_date 
FROM 
  (
    SELECT 
      (person.id :: text || '-' :: text) || supervisee.id :: text AS id, 
      supervisor_hid.person_hid AS supervisor, 
      supervisee_hid.person_hid AS supervisee, 
      post_history.staff_category_id, 
      post_history.job_title, 
      supervisee.continuous_employment_start_date, 
      COALESCE(
        post_history.funding_end_date, post_history.intended_end_date
      ) AS end_date 
    FROM 
      person 
      JOIN _latest_role_v12 _latest_role ON _latest_role.supervisor_id = person.id 
      JOIN post_history ON _latest_role.role_id = post_history.id 
      JOIN person supervisee ON post_history.person_id = supervisee.id 
      JOIN _physical_status_v2 supervisor_status ON person.id = supervisor_status.person_id 
      JOIN _physical_status_v2 supervisee_status ON supervisee.id = supervisee_status.person_id 
      JOIN person_hid supervisor_hid ON supervisor_hid.person_id = person.id 
      JOIN person_hid supervisee_hid ON supervisee_hid.person_id = supervisee.id 
    WHERE 
      -- supervisor_status.status_id :: text = 'Current' :: text 
      -- AND 
      supervisee_status.status_id :: text = 'Current' :: text 
      AND _latest_role.role_tablename = 'post_history' :: text
  ) a 
ORDER BY 
  a.supervisor, 
  a.supervisee;
ALTER TABLE 
  hotwire."30_Report/Staff_Personnel" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Personnel" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Personnel" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Personnel" TO hr;

