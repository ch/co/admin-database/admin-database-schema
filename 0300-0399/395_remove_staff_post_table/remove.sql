CREATE TABLE staff_post
(
  id bigserial NOT NULL,
  name character varying(80) NOT NULL,
  job_code character varying(80),
  staff_category_id bigint NOT NULL DEFAULT 8,
  current boolean NOT NULL DEFAULT true,
  grade character varying(80),
  post_number character varying(20),
  description character varying(500),
  post_is_suppressed boolean DEFAULT false,
  post_tenure_ended boolean DEFAULT false,
  role_code_number character varying(50),
  chris_position_reference character varying(50),
  CONSTRAINT pk_staff_post PRIMARY KEY (id),
  CONSTRAINT staff_post_fk_staff_category FOREIGN KEY (staff_category_id)
      REFERENCES staff_category (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE staff_post
  OWNER TO dev;
GRANT ALL ON TABLE staff_post TO dev;
GRANT SELECT ON TABLE staff_post TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE staff_post TO hr;
GRANT SELECT ON TABLE staff_post TO web_editors;
GRANT ALL ON TABLE staff_post TO cen1001;

CREATE TRIGGER staff_post_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON staff_post
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();

CREATE OR REPLACE VIEW staff_post_hid AS 
 SELECT staff_post.id AS staff_post_id, staff_post.name::text || COALESCE((' ('::text || staff_post.job_code::text) || ')'::text, ''::text) AS staff_post_hid, 
        CASE
            WHEN staff_post.name::text ~~ 'Generic%'::text THEN 0
            ELSE 10
        END AS _weight
   FROM staff_post
  ORDER BY 
        CASE
            WHEN staff_post.name::text ~~ 'Generic%'::text THEN 0
            ELSE 10
        END, staff_post.name;

ALTER TABLE staff_post_hid
  OWNER TO dev;
GRANT ALL ON TABLE staff_post_hid TO dev;
GRANT SELECT ON TABLE staff_post_hid TO ro_hid;
GRANT ALL ON TABLE staff_post_hid TO postgres;

CREATE OR REPLACE VIEW hotwire.staff_post_hid AS 
 SELECT staff_post.id AS staff_post_id, staff_post.name::text || COALESCE((' ('::text || staff_post.job_code::text) || ')'::text, ''::text) AS staff_post_hid, 
        CASE
            WHEN staff_post.name::text ~~ 'Generic%'::text THEN 0
            ELSE 10
        END AS staff_post_sort
   FROM staff_post
  ORDER BY 
        CASE
            WHEN staff_post.name::text ~~ 'Generic%'::text THEN 0
            ELSE 10
        END, staff_post.name;

ALTER TABLE hotwire.staff_post_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire.staff_post_hid TO dev;
GRANT ALL ON TABLE hotwire.staff_post_hid TO ro_hid;
GRANT ALL ON TABLE hotwire.staff_post_hid TO postgres;

