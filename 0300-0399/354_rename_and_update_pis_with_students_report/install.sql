DROP VIEW hotwire."30_Report/Principal_Investigators_with_Students_ro";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Principal_Investigators_with_Students" AS 
SELECT 
  a.id, 
  a.supervisor_id, 
  a.email_address 
FROM 
  (
    SELECT 
      DISTINCT person.id, 
      postgraduate_studentship.first_supervisor_id AS supervisor_id, 
      person.email_address 
    FROM 
      postgraduate_studentship 
      LEFT JOIN person ON postgraduate_studentship.first_supervisor_id = person.id 
      LEFT JOIN _all_roles_v12 _all_roles ON postgraduate_studentship.id = _all_roles.role_id 
      AND _all_roles.role_tablename = 'postgraduate_studentship' :: text 
    WHERE 
      _all_roles.status :: text = 'Current' :: text
  ) a 
ORDER BY 
  a.id, 
  a.supervisor_id, 
  a.email_address;
ALTER TABLE 
  hotwire."30_Report/Principal_Investigators_with_Students" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Principal_Investigators_with_Students" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students" TO hr;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students" TO mgmt_ro;

