DROP VIEW hotwire."30_Report/Principal_Investigators_with_Students";
CREATE 
OR REPLACE VIEW hotwire."30_Report/Principal_Investigators_with_Students_ro" AS 
SELECT 
  a.id, 
  a.supervisor_id, 
  a.email_address 
FROM 
  (
    SELECT 
      DISTINCT person.id, 
      postgraduate_studentship.first_supervisor_id AS supervisor_id, 
      person.email_address 
    FROM 
      postgraduate_studentship 
      LEFT JOIN person ON postgraduate_studentship.first_supervisor_id = person.id 
      LEFT JOIN _all_roles_v9 ON postgraduate_studentship.id = _all_roles_v9.role_id 
      AND _all_roles_v9.role_tablename = 'postgraduate_studentship' :: text 
    WHERE 
      _all_roles_v9.status :: text = 'Current' :: text
  ) a 
ORDER BY 
  a.id, 
  a.supervisor_id, 
  a.email_address;
ALTER TABLE 
  hotwire."30_Report/Principal_Investigators_with_Students_ro" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Principal_Investigators_with_Students_ro" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students_ro" TO hr;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students_ro" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Principal_Investigators_with_Students_ro" TO mgmt_ro;

