CREATE OR REPLACE FUNCTION primary_supervisor(v_person_id bigint)
  RETURNS bigint AS
$BODY$

    declare v_supervisor_id bigint;

    begin

    select primary_supervisor.supervisor_id into v_supervisor_id from (

        select supervisor_id, max(start_date) from (
            SELECT person_id AS id, host_person_id as supervisor_id, start_date
            FROM visitorship

            UNION

            select person_id AS id,
                first_supervisor_person_id as supervisor_id,
                start_date
            from postgraduate_studentship

            UNION

            select person_id AS id,
                supervisor_person_id as supervisor_id,
                start_date
            from erasmus_socrates_studentship

            UNION

            select person_id AS id,
                supervisor_person_id as supervisor_id,
                start_date
            from part_iii_studentship

            UNION

            select person_id as id,
                staff_post.supervisor_person_id as supervisor_id,
                start_date
            from post_history
            join staff_post on staff_post.id = post_history.staff_post_id

        ) history where history.id = v_person_id group by supervisor_id
    ) primary_supervisor ;

    return v_supervisor_id;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION primary_supervisor(bigint)
  OWNER TO cen1001;

