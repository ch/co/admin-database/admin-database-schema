CREATE FUNCTION post_history_staff_category_trig() RETURNS TRIGGER AS
$body$
    DECLARE
        new_staff_category_id bigint;
    BEGIN
        IF TG_OP = 'INSERT' OR (NEW.staff_post_id <> OLD.staff_post_id) THEN
            SELECT staff_category_id FROM staff_post WHERE staff_post.id = NEW.staff_post_id INTO new_staff_category_id;
            NEW.staff_category_id := new_staff_category_id;
            RAISE NOTICE 'Updated post_history id % staff_category_id to %', NEW.id, new_staff_category_id;
        END IF;
        RETURN NEW;
    END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION post_history_staff_category_trig() OWNER TO dev;

CREATE FUNCTION staff_post_update_post_history_trig() RETURNS TRIGGER AS
$body$
    DECLARE
        post_history_id bigint;
    BEGIN
        FOR post_history_id IN SELECT post_history.id FROM staff_post JOIN post_history ON staff_post.id = post_history.staff_post_id WHERE staff_post.id = NEW.id LOOP
            UPDATE post_history SET staff_category_id = NEW.staff_category_id WHERE post_history.id = post_history_id;
            RAISE NOTICE 'Updated post_history id % staff_category_id to %', post_history_id, NEW.staff_category_id;
        END LOOP;
    RETURN NULL; -- after trigger so no need to return value
    END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION staff_post_update_post_history_trig() OWNER TO dev;


