DROP TRIGGER post_history_update_staff_category ON post_history;
DROP FUNCTION post_history_staff_category_trig();

DROP TRIGGER staff_post_update_post_history ON staff_post;
DROP FUNCTION staff_post_update_post_history_trig();

ALTER TABLE post_history ALTER COLUMN staff_category_id DROP NOT NULL;
ALTER TABLE post_history DROP CONSTRAINT post_history_fk_staff_category ;
ALTER TABLE post_history DROP COLUMN staff_category_id  ;
