CREATE OR REPLACE VIEW _latest_role_v11 AS 
 SELECT l.person_id, l.start_date, l.intended_end_date, l.estimated_leaving_date, l.funding_end_date, l.end_date, l.supervisor_id, l.co_supervisor_id, l.mentor_id, l.post_category_id, l.post_category, l.status, l.funding, l.fees_funding, l.research_grant_number, l.paid_by_university, l.hesa_leaving_code, l.chem, l.role_id, l.role_tablename, l.role_target_viewname
   FROM _all_roles_v11 l
   JOIN ( SELECT lengths.person_id, lengths.fixed_start_date, max(lengths.length_of_role) AS max_length
           FROM _all_roles_v12 lengths
      JOIN ( SELECT starts.person_id, max(starts.fixed_start_date) AS max_start
                   FROM _all_roles_v12 starts
                  WHERE starts.fixed_start_date <= now()
                  GROUP BY starts.person_id) newest_start ON newest_start.person_id = lengths.person_id AND newest_start.max_start = lengths.fixed_start_date
     GROUP BY lengths.person_id, lengths.fixed_start_date) latest ON latest.person_id = l.person_id AND latest.max_length = l.length_of_role AND latest.fixed_start_date = l.fixed_start_date
  ORDER BY l.person_id;

ALTER TABLE _latest_role_v11
  OWNER TO dev;
GRANT ALL ON TABLE _latest_role_v11 TO dev;
GRANT SELECT ON TABLE _latest_role_v11 TO adreconcile;
GRANT SELECT ON TABLE _latest_role_v11 TO public;
