CREATE 
OR REPLACE VIEW _chem_theor_mailinglist AS 
SELECT 
  person.email_address, 
  person.crsid, 
  person_hid.person_hid 
FROM 
  (
    SELECT 
      all_possible.id 
    FROM 
      (
        (
          (
            (
              SELECT 
                mm_member_research_interest_group.member_id AS id 
              FROM 
                person 
                LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id 
                LEFT JOIN mm_member_research_interest_group ON mm_member_research_interest_group.member_id = person.id 
                LEFT JOIN research_interest_group ON mm_member_research_interest_group.research_interest_group_id = research_interest_group.id 
              WHERE 
                _physical_status_v3.status_id :: text = 'Current' :: text 
                AND research_interest_group.id = 1 
              UNION 
              SELECT 
                postgraduate_studentship.person_id AS id 
              FROM 
                mm_member_research_interest_group 
                JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
                JOIN postgraduate_studentship ON postgraduate_studentship.first_supervisor_id = supervisor.id 
                JOIN _postgrad_end_dates_v5 ON _postgrad_end_dates_v5.id = postgraduate_studentship.id 
                JOIN _physical_status_v3 ON _physical_status_v3.person_id = postgraduate_studentship.person_id 
              WHERE 
                mm_member_research_interest_group.research_interest_group_id = 1 
                AND mm_member_research_interest_group.is_primary = true 
                AND _postgrad_end_dates_v5.status_id :: text = 'Current' :: text 
                AND _physical_status_v3.status_id :: text = 'Current' :: text
            ) 
            UNION 
            SELECT 
              DISTINCT post_history.person_id 
            FROM 
              mm_member_research_interest_group 
              JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
              JOIN post_history ON post_history.supervisor_id = supervisor.id 
              JOIN staff_post ON post_history.staff_post_id = staff_post.id 
              JOIN staff_category ON staff_post.staff_category_id = staff_category.id 
              JOIN _latest_role_v10 ON _latest_role_v10.person_id = post_history.person_id 
              JOIN _physical_status_v3 ON _physical_status_v3.person_id = post_history.person_id 
            WHERE 
              mm_member_research_interest_group.research_interest_group_id = 1 
              AND mm_member_research_interest_group.is_primary = true 
              AND _latest_role_v10.status :: text = 'Current' :: text 
              AND _physical_status_v3.status_id :: text = 'Current' :: text 
              AND (
                staff_category.category :: text = 'PDRA' :: text 
                OR staff_category.category :: text = 'Senior PDRA' :: text 
                OR staff_category.category :: text = 'Research Fellow' :: text 
                OR staff_category.category :: text = 'Research Assistant' :: text 
                OR staff_category.category :: text = 'Teaching Fellow' :: text
              )
          ) 
          UNION 
          SELECT 
            DISTINCT visitorship.person_id 
          FROM 
            mm_member_research_interest_group 
            JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
            JOIN visitorship ON supervisor.id = visitorship.host_person_id 
            JOIN _latest_role_v10 ON _latest_role_v10.person_id = visitorship.person_id 
            JOIN _physical_status_v3 ON _physical_status_v3.person_id = visitorship.person_id 
          WHERE 
            mm_member_research_interest_group.research_interest_group_id = 1 
            AND mm_member_research_interest_group.is_primary = true 
            AND _latest_role_v10.post_category_id ~~ 'v-%' :: text 
            AND _latest_role_v10.status :: text = 'Current' :: text 
            AND _physical_status_v3.status_id :: text = 'Current' :: text
        ) 
        UNION 
        SELECT 
          piii.person_id 
        FROM 
          mm_member_research_interest_group 
          JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
          JOIN part_iii_studentship piii ON supervisor.id = piii.supervisor_id 
          JOIN _latest_role_v10 ON _latest_role_v10.person_id = piii.person_id 
          JOIN _physical_status_v3 ON _physical_status_v3.person_id = piii.person_id 
        WHERE 
          mm_member_research_interest_group.research_interest_group_id = 1 
          AND mm_member_research_interest_group.is_primary = true 
          AND _latest_role_v10.post_category_id = 'p3-1' :: text 
          AND _latest_role_v10.status :: text = 'Current' :: text 
          AND _physical_status_v3.status_id :: text = 'Current' :: text
      ) all_possible 
    WHERE 
      NOT (
        all_possible.id IN (
          SELECT 
            person.id 
          FROM 
            person 
            JOIN mm_mailinglist_exclude_person mm ON person.id = mm.exclude_person_id 
          WHERE 
            mm.mailinglist_id = 26
        )
      ) 
    UNION 
    SELECT 
      mm.include_person_id 
    FROM 
      mm_mailinglist_include_person mm 
    WHERE 
      mm.mailinglist_id = 26
  ) all_ids 
  JOIN person ON all_ids.id = person.id 
  JOIN person_hid ON person.id = person_hid.person_id 
ORDER BY 
  person_hid.person_hid;
ALTER TABLE 
  _chem_theor_mailinglist OWNER TO dev;
GRANT ALL ON TABLE _chem_theor_mailinglist TO dev;
GRANT 
SELECT 
  ON TABLE _chem_theor_mailinglist TO mailinglists;
GRANT 
SELECT 
  ON TABLE _chem_theor_mailinglist TO leavers_trigger;

