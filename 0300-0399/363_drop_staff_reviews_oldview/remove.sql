-- View: staff_reviews_oldview

-- DROP VIEW staff_reviews_oldview;

CREATE OR REPLACE VIEW staff_reviews_oldview AS 
 SELECT staff_review_meeting.id, post_history.id AS post_history_id, post_history.reviewer_id AS usual_reviewer_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, post_history.next_review_due, staff_review_meeting.notes, staff_post.name AS _name
   FROM staff_review_meeting
   JOIN post_history ON post_history.id = staff_review_meeting.post_history_id
   LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id
  ORDER BY staff_post.name;

ALTER TABLE staff_reviews_oldview
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_reviews_oldview TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE staff_reviews_oldview TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE staff_reviews_oldview TO dev;
GRANT SELECT ON TABLE staff_reviews_oldview TO mgmt_ro;

-- Rule: staff_reviews_del ON staff_reviews_oldview

-- DROP RULE staff_reviews_del ON staff_reviews_oldview;

CREATE OR REPLACE RULE staff_reviews_del AS
    ON DELETE TO staff_reviews_oldview DO INSTEAD  DELETE FROM staff_review_meeting
  WHERE staff_review_meeting.id = old.id;

-- Rule: staff_reviews_ins ON staff_reviews_oldview

-- DROP RULE staff_reviews_ins ON staff_reviews_oldview;

CREATE OR REPLACE RULE staff_reviews_ins AS
    ON INSERT TO staff_reviews_oldview DO INSTEAD ( UPDATE post_history SET next_review_due = new.next_review_due
  WHERE post_history.id = new.post_history_id;
 INSERT INTO staff_review_meeting (post_history_id, date_of_meeting, reviewer_id, notes) 
  VALUES (new.post_history_id, new.date_of_meeting, COALESCE(new.reviewer_id, ( SELECT post_history.reviewer_id
           FROM post_history
          WHERE post_history.id = new.post_history_id)), new.notes)
  RETURNING staff_review_meeting.id, NULL::bigint AS int8, NULL::bigint AS int8, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, NULL::date AS date, staff_review_meeting.notes, NULL::character varying(80) AS "varchar";
);

-- Rule: staff_reviews_upd ON staff_reviews_oldview

-- DROP RULE staff_reviews_upd ON staff_reviews_oldview;

CREATE OR REPLACE RULE staff_reviews_upd AS
    ON UPDATE TO staff_reviews_oldview DO INSTEAD ( UPDATE staff_review_meeting SET date_of_meeting = new.date_of_meeting, reviewer_id = COALESCE(new.reviewer_id, new.usual_reviewer_id), notes = new.notes
  WHERE staff_review_meeting.id = old.id;
 UPDATE post_history SET next_review_due = new.next_review_due, reviewer_id = new.usual_reviewer_id
  WHERE post_history.id = old.post_history_id;
);


