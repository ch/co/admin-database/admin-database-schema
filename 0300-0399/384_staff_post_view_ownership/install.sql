ALTER TABLE hotwire."10_View/Staff_Post"
  OWNER TO dev;
  GRANT ALL ON TABLE hotwire."10_View/Staff_Post" TO cen1001;
  GRANT ALL ON TABLE hotwire."10_View/Staff_Post" TO dev;
  GRANT SELECT ON TABLE hotwire."10_View/Staff_Post" TO mgmt_ro;
  GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Staff_Post" TO hr;
