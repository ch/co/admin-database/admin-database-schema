DROP RULE hotwire_view_personnel_data_entry_del ON hotwire."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire_view_personnel_data_entry_ins ON hotwire."10_View/People/Personnel_Data_Entry";
DROP RULE hotwire_view_personnel_data_entry_upd ON hotwire."10_View/People/Personnel_Data_Entry";
DROP FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry");
DROP VIEW hotwire."10_View/People/Personnel_Data_Entry";

CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Data_Entry" AS 
SELECT 
  a.id, 
  a.ro_person_id, 
  a.image_oid, 
  a.surname, 
  a.first_names, 
  a.title_id, 
  a.known_as, 
  a.name_suffix, 
  a.maiden_name, 
  a.date_of_birth, 
  a.ro_age, 
  a.gender_id, 
  a.ro_post_category_id, 
  a.ro_chem, 
  a.crsid, 
  a.email_address, 
  a.hide_email, 
  a.hide_from_website, 
  a.arrival_date, 
  a.leaving_date, 
  a.ro_physical_status_id, 
  a.left_but_no_leaving_date_given, 
  a.dept_telephone_number_id, 
  a.hide_phone_no_from_website, 
  a.room_id, 
  a.ro_supervisor_id, 
  a.ro_co_supervisor_id, 
  a.ro_mentor_id, 
  a.research_group_id, 
  a.external_work_phone_numbers, 
  a.pager, 
  a.cambridge_address, 
  a.cambridge_phone_number, 
  a.cambridge_college_id, 
  a.mobile_number, 
  a.college_phone_number, 
  a.nationality_id, 
  a.home_address, 
  a.home_phone_number, 
  a.emergency_contact, 
  a.location, 
  a.other_information, 
  a.notes, 
  a.forwarding_address, 
  a.new_employer_address, 
  a.permission_to_give_out_details, 
  a.chem_at_cam AS "chem_@_cam", 
  a.retired_staff_mailing_list, 
  a.payroll_personal_reference_number, 
  a.ni_number, 
  a.certificate_of_sponsorship_number, 
  a.continuous_employment_start_date, 
  a.paper_file_held_by, 
  a.visa_start_date, 
  a.visa_end_date, 
  a.registration_completed, 
  a.clearance_cert_signed, 
  a._cssclass, 
  a.treat_as_academic_staff 
FROM 
  (
    SELECT 
      person.id, 
      person.id AS ro_person_id, 
      ROW(
        'image/jpeg' :: character varying, 
        person.image_oid
      ):: blobtype AS image_oid, 
      person.surname, 
      person.first_names, 
      person.title_id, 
      person.known_as, 
      person.name_suffix, 
      person.maiden_name, 
      person.date_of_birth, 
      date_part(
        'year' :: text, 
        age(
          'now' :: text :: date :: timestamp with time zone, 
          person.date_of_birth :: timestamp with time zone
        )
      ) AS ro_age, 
      person.gender_id, 
      _latest_role.post_category_id AS ro_post_category_id, 
      _latest_role.chem AS ro_chem, 
      person.crsid, 
      person.email_address, 
      person.hide_email, 
      person.do_not_show_on_website AS hide_from_website, 
      person.arrival_date, 
      person.leaving_date, 
      _physical_status_v3.status_id AS ro_physical_status_id, 
      person.left_but_no_leaving_date_given, 
      ARRAY(
        SELECT 
          mm_person_dept_telephone_number.dept_telephone_number_id 
        FROM 
          mm_person_dept_telephone_number 
        WHERE 
          person.id = mm_person_dept_telephone_number.person_id
      ) AS dept_telephone_number_id, 
      person.hide_phone_no_from_website, 
      ARRAY(
        SELECT 
          mm_person_room.room_id 
        FROM 
          mm_person_room 
        WHERE 
          person.id = mm_person_room.person_id
      ) AS room_id, 
      _latest_role.supervisor_id AS ro_supervisor_id, 
      _latest_role.co_supervisor_id AS ro_co_supervisor_id, 
      _latest_role.mentor_id AS ro_mentor_id, 
      ARRAY(
        SELECT 
          mm_person_research_group.research_group_id 
        FROM 
          mm_person_research_group 
        WHERE 
          person.id = mm_person_research_group.person_id
      ) AS research_group_id, 
      person.external_work_phone_numbers, 
      person.pager, 
      person.cambridge_address, 
      person.cambridge_phone_number, 
      person.cambridge_college_id, 
      person.mobile_number, 
      person.college_phone_number, 
      ARRAY(
        SELECT 
          mm_person_nationality.nationality_id 
        FROM 
          mm_person_nationality 
        WHERE 
          mm_person_nationality.person_id = person.id
      ) AS nationality_id, 
      person.home_address, 
      person.home_phone_number, 
      person.emergency_contact, 
      person.location, 
      person.other_information, 
      person.notes, 
      person.forwarding_address, 
      person.new_employer_address, 
      person.permission_to_give_out_details, 
      person.chem_at_cam, 
      CASE WHEN retired_staff_list.include_person_id IS NOT NULL THEN true ELSE false END AS retired_staff_mailing_list, 
      person.payroll_personal_reference_number, 
      person.ni_number, 
      person.certificate_of_sponsorship_number, 
      person.continuous_employment_start_date, 
      person.paper_file_details AS paper_file_held_by, 
      person.visa_start_date, 
      person.visa_end_date, 
      person.registration_completed, 
      person.clearance_cert_signed, 
      person.counts_as_academic AS treat_as_academic_staff, 
      CASE WHEN _physical_status_v3.status_id :: text = 'Past' :: text THEN 'orange' :: text WHEN _physical_status_v3.status_id :: text = 'Unknown' :: text 
      AND person._status :: text = 'Past' :: text THEN 'orange' :: text ELSE NULL :: text END AS _cssclass 
    FROM 
      person 
      LEFT JOIN _latest_role_v10 _latest_role ON person.id = _latest_role.person_id 
      LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id 
      LEFT JOIN (
        SELECT 
          mm_mailinglist_include_person.mailinglist_id, 
          mm_mailinglist_include_person.include_person_id 
        FROM 
          mm_mailinglist_include_person 
          JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id 
        WHERE 
          mailinglist.name :: text = 'chem-retiredstaff' :: text
      ) retired_staff_list ON person.id = retired_staff_list.include_person_id
  ) a 
ORDER BY 
  a.surname, 
  a.first_names;


ALTER TABLE hotwire."10_View/People/Personnel_Data_Entry" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Data_Entry" TO tkd25;


CREATE OR REPLACE FUNCTION hw_fn_personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_oid=(select (v.image_oid).val from (select v.image_oid) as x), 
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as,
                                name_suffix = v.name_suffix,
                                maiden_name = v.maiden_name, 
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                external_work_phone_numbers = v.external_work_phone_numbers,
                                pager = v.pager,
                                cambridge_address = v.cambridge_address,
                                cambridge_phone_number = v.cambridge_phone_number, 
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number,
                                college_phone_number = v.college_phone_number, 
                                home_address = v.home_address, 
                                home_phone_number = v.home_phone_number, 
                                emergency_contact = v.emergency_contact, 
                                location = v.location,
                                other_information = v.other_information,
                                notes = v.notes,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                permission_to_give_out_details = v.permission_to_give_out_details,
                                chem_at_cam = v."chem_@_cam",
                                payroll_personal_reference_number = v.payroll_personal_reference_number, 
                                ni_number = v.ni_number,
                                certificate_of_sponsorship_number = v.certificate_of_sponsorship_number,
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_held_by, 
                                visa_start_date = v.visa_start_date, 
                                visa_end_date = v.visa_end_date,
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                      image_oid, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        maiden_name, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        external_work_phone_numbers,
                                        pager,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        college_phone_number, 
                                        home_address, 
                                        home_phone_number, 
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        permission_to_give_out_details,
                                        chem_at_cam,
                                        payroll_personal_reference_number, 
                                        ni_number,
                                        certificate_of_sponsorship_number,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        visa_start_date, 
                                        visa_end_date,
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        (select (v.image_oid).val from (select v.image_oid) as x),
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as,
                                        v.name_suffix,
                                        v.maiden_name, 
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.external_work_phone_numbers,
                                        v.pager,
                                        v.cambridge_address,
                                        v.cambridge_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number,
                                        v.college_phone_number, 
                                        v.home_address, 
                                        v.home_phone_number, 
                                        v.emergency_contact, 
                                        v.location,
                                        v.other_information,
                                        v.notes,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v.permission_to_give_out_details,
                                        v."chem_@_cam",
                                        v.payroll_personal_reference_number, 
                                        v.ni_number,
                                        v.certificate_of_sponsorship_number,
                                        v.continuous_employment_start_date,
                                        v.paper_file_held_by, 
                                        v.visa_start_date, 
                                        v.visa_end_date,
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry") OWNER TO dev;

CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_del AS
    ON DELETE TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_ins AS
    ON INSERT TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hw_fn_personnel_data_entry_upd(new.*) AS id;

CREATE OR REPLACE RULE hotwire_view_personnel_data_entry_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hw_fn_personnel_data_entry_upd(new.*) AS id;


