ALTER TABLE staff_post_hid OWNER TO postgres;
GRANT ALL ON TABLE staff_post_hid TO postgres;
GRANT SELECT ON TABLE staff_post_hid TO ro_hid;

ALTER TABLE hotwire.staff_post_hid OWNER TO postgres;
GRANT ALL ON TABLE hotwire.staff_post_hid TO postgres;
GRANT ALL ON TABLE hotwire.staff_post_hid TO ro_hid;

