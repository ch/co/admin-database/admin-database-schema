CREATE OR REPLACE VIEW post_history_hid AS 
 SELECT post_history.id AS post_history_id, (person_hid.person_hid || COALESCE(' - '::text || staff_category.category, ''::text)) || COALESCE(' - '::text || post_history.start_date, ''::text) AS post_history_hid
   FROM post_history
   JOIN staff_category ON post_history.staff_category_id = staff_category.id
   JOIN person_hid USING (person_id)
  ORDER BY person_hid.person_hid, post_history.start_date desc, staff_category.category asc;

ALTER TABLE post_history_hid OWNER TO dev;
GRANT ALL ON TABLE post_history_hid TO dev;
GRANT SELECT ON TABLE post_history_hid TO ro_hid;
GRANT ALL ON TABLE post_history_hid TO postgres;

CREATE OR REPLACE VIEW hotwire.post_history_hid AS 
 SELECT post_history.id AS post_history_id, (person_hid.person_hid || COALESCE(' - '::text || staff_category.category, ''::text)) || COALESCE(' - '::text || post_history.start_date, ''::text) AS post_history_hid
   FROM post_history
   JOIN staff_category ON post_history.staff_category_id = staff_category.id
   JOIN hotwire.person_hid USING (person_id)
  ORDER BY person_hid.person_hid, post_history.start_date desc, staff_category.category asc;



ALTER TABLE hotwire.post_history_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire.post_history_hid TO dev;
GRANT ALL ON TABLE hotwire.post_history_hid TO ro_hid;
GRANT ALL ON TABLE hotwire.post_history_hid TO postgres;
