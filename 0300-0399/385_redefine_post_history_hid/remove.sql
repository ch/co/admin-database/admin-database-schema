CREATE OR REPLACE VIEW post_history_hid AS 
 SELECT post_history.id AS post_history_id, (person_hid.person_hid || COALESCE('- '::text || staff_post_hid.staff_post_hid::character varying(20)::text, ''::text)) || COALESCE(' - '::text || post_history.start_date, ''::text) AS post_history_hid
   FROM post_history
   JOIN staff_post_hid USING (staff_post_id)
   JOIN person_hid USING (person_id)
  ORDER BY person_hid.person_hid || COALESCE(' - '::text || staff_post_hid.staff_post_hid, ''::text);

ALTER TABLE post_history_hid OWNER TO dev;
GRANT ALL ON TABLE post_history_hid TO dev;
GRANT SELECT ON TABLE post_history_hid TO ro_hid;
GRANT ALL ON TABLE post_history_hid TO postgres;

CREATE OR REPLACE VIEW hotwire.post_history_hid AS 
 SELECT post_history.id AS post_history_id, (person_hid.person_hid || COALESCE('- '::text || staff_post_hid.staff_post_hid::character varying(20)::text, ''::text)) || COALESCE(' - '::text || post_history.start_date, ''::text) AS post_history_hid
   FROM post_history
   JOIN staff_post_hid USING (staff_post_id)
   JOIN person_hid USING (person_id)
  ORDER BY person_hid.person_hid || COALESCE(' - '::text || staff_post_hid.staff_post_hid, ''::text);

ALTER TABLE hotwire.post_history_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire.post_history_hid TO dev;
GRANT ALL ON TABLE hotwire.post_history_hid TO ro_hid;
GRANT ALL ON TABLE hotwire.post_history_hid TO postgres;
