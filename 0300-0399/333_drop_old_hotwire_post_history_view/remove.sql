-- DROP VIEW post_history_view;

CREATE OR REPLACE VIEW post_history_view AS 
 SELECT post_history.id, post_history.staff_post_id, post_history.person_id, _all_roles_v9.status AS ro_role_status_id, post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.research_grant_number, post_history.filemaker_funding AS sponsor, post_history.hesa_leaving_code, _all_roles_v9.post_category_id AS ro_post_category_id, post_history.job_title, post_history.force_role_status_to_past, staff_post.name AS _name
   FROM post_history
   LEFT JOIN _all_roles_v9 ON post_history.id = _all_roles_v9.role_id AND _all_roles_v9.role_tablename = 'post_history'::text
   JOIN staff_post ON post_history.staff_post_id = staff_post.id
   JOIN staff_category ON staff_post.staff_category_id = staff_category.id
   JOIN person ON post_history.person_id = person.id
  ORDER BY staff_post.name;

ALTER TABLE post_history_view
  OWNER TO cen1001;
GRANT ALL ON TABLE post_history_view TO cen1001;
GRANT ALL ON TABLE post_history_view TO dev;
GRANT SELECT ON TABLE post_history_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE post_history_view TO hr;

-- Rule: post_history_del ON post_history_view

-- DROP RULE post_history_del ON post_history_view;

CREATE OR REPLACE RULE post_history_del AS
    ON DELETE TO post_history_view DO INSTEAD  DELETE FROM post_history
  WHERE post_history.id = old.id;

-- Rule: post_history_ins ON post_history_view

-- DROP RULE post_history_ins ON post_history_view;

CREATE OR REPLACE RULE post_history_ins AS
    ON INSERT TO post_history_view DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 INSERT INTO post_history (staff_post_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, hesa_leaving_code, job_title, force_role_status_to_past) 
  VALUES (new.staff_post_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_by_university, new.hours_worked, new.percentage_of_fulltime_hours, new.probation_period, new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments, new.second_probation_meeting_comments, new.third_probation_meeting_comments, new.fourth_probation_meeting_comments, new.probation_outcome, new.date_probation_letters_sent, new.research_grant_number, new.sponsor, new.hesa_leaving_code, new.job_title, new.force_role_status_to_past)
  RETURNING post_history.id, post_history.staff_post_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.research_grant_number, post_history.filemaker_funding, post_history.hesa_leaving_code, NULL::text AS text, post_history.job_title, post_history.force_role_status_to_past, NULL::character varying(80) AS "varchar";
);

-- Rule: post_history_update_rules ON post_history_view

-- DROP RULE post_history_update_rules ON post_history_view;

CREATE OR REPLACE RULE post_history_update_rules AS
    ON UPDATE TO post_history_view DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 UPDATE post_history SET staff_post_id = new.staff_post_id, supervisor_id = new.supervisor_id, mentor_id = new.mentor_id, external_mentor = new.external_mentor, start_date = new.start_date, intended_end_date = new.intended_end_date, end_date = new.end_date, funding_end_date = new.funding_end_date, chem = new.chem, paid_by_university = new.paid_by_university, hours_worked = new.hours_worked, percentage_of_fulltime_hours = new.percentage_of_fulltime_hours, probation_period = new.probation_period, date_of_first_probation_meeting = new.date_of_first_probation_meeting, date_of_second_probation_meeting = new.date_of_second_probation_meeting, date_of_third_probation_meeting = new.date_of_third_probation_meeting, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments, second_probation_meeting_comments = new.second_probation_meeting_comments, third_probation_meeting_comments = new.third_probation_meeting_comments, fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, probation_outcome = new.probation_outcome, date_probation_letters_sent = new.date_probation_letters_sent, research_grant_number = new.research_grant_number, filemaker_funding = new.sponsor, hesa_leaving_code = new.hesa_leaving_code, job_title = new.job_title, force_role_status_to_past = new.force_role_status_to_past
  WHERE post_history.id = old.id;
);


