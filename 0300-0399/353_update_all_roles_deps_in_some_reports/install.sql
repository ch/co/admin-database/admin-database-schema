-- Staff_Review_and_Development
CREATE 
OR REPLACE VIEW hotwire."30_Report/Staff_Review_and_Development" AS 
SELECT 
  DISTINCT person.id, 
  person.id AS person_id, 
  lr.post_category_id, 
  lr.chem, 
  lr.supervisor_id, 
  person.usual_reviewer_id, 
  ur.email_address AS usual_reviewer_email, 
  staff_review_meeting.reviewer_id, 
  r.email_address AS reviewer_email, 
  most_recent.date_of_meeting, 
  ARRAY(
    SELECT 
      other_meetings.date_of_meeting 
    FROM 
      (
        SELECT 
          staff_review_meeting.date_of_meeting, 
          staff_review_meeting.person_id 
        FROM 
          staff_review_meeting 
        EXCEPT 
        SELECT 
          max(
            staff_review_meeting.date_of_meeting
          ) AS max, 
          staff_review_meeting.person_id 
        FROM 
          staff_review_meeting 
        GROUP BY 
          staff_review_meeting.person_id
      ) other_meetings 
    WHERE 
      other_meetings.person_id = person.id
  ) AS others, 
  person.next_review_due, 
  person.continuous_employment_start_date, 
  newest_funding.funding_end_date 
FROM 
  person 
  LEFT JOIN cache._latest_role _latest_role ON person.id = _latest_role.person_id 
  LEFT JOIN staff_review_meeting ON staff_review_meeting.person_id = person.id 
  LEFT JOIN (
    SELECT 
      max(
        staff_review_meeting.date_of_meeting
      ) AS date_of_meeting, 
      staff_review_meeting.person_id 
    FROM 
      staff_review_meeting 
    GROUP BY 
      staff_review_meeting.person_id
  ) most_recent ON person.id = most_recent.person_id 
  LEFT JOIN person ur ON person.usual_reviewer_id = ur.id 
  LEFT JOIN person r ON staff_review_meeting.reviewer_id = r.id 
  JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id 
  JOIN cache._latest_role lr ON person.id = lr.person_id 
  JOIN (
    SELECT 
      max(_all_roles.funding_end_date) AS funding_end_date, 
      _all_roles.person_id 
    FROM 
      _all_roles_v12 _all_roles 
    GROUP BY 
      _all_roles.person_id
  ) newest_funding ON newest_funding.person_id = person.id 
WHERE 
  _physical_status_v3.status_id :: text = 'Current' :: text 
  AND lr.role_tablename = 'post_history' :: text;
ALTER TABLE 
  hotwire."30_Report/Staff_Review_and_Development" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Review_and_Development" TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Review_and_Development" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Review_and_Development" TO hr;

-- Staff_Review_and_Development_Historical
CREATE 
OR REPLACE VIEW hotwire."30_Report/Staff_Review_and_Development_Historical" AS 
SELECT 
  DISTINCT person.id, 
  person.id AS person_id, 
  person.gender_id, 
  lr.post_category_id, 
  lr.supervisor_id, 
  person.usual_reviewer_id, 
  ur.email_address AS usual_reviewer_email, 
  staff_review_meeting.reviewer_id, 
  r.email_address AS reviewer_email, 
  most_recent.date_of_meeting, 
  ARRAY(
    SELECT 
      other_meetings.date_of_meeting 
    FROM 
      (
        SELECT 
          staff_review_meeting.date_of_meeting, 
          staff_review_meeting.person_id 
        FROM 
          staff_review_meeting 
        EXCEPT 
        SELECT 
          max(
            staff_review_meeting.date_of_meeting
          ) AS max, 
          staff_review_meeting.person_id 
        FROM 
          staff_review_meeting 
        GROUP BY 
          staff_review_meeting.person_id
      ) other_meetings 
    WHERE 
      other_meetings.person_id = person.id
  ) AS others, 
  person.next_review_due, 
  person.continuous_employment_start_date, 
  newest_funding.funding_end_date, 
  _physical_status_v3.status_id 
FROM 
  person 
  LEFT JOIN cache._latest_role _latest_role ON person.id = _latest_role.person_id 
  LEFT JOIN staff_review_meeting ON staff_review_meeting.person_id = person.id 
  LEFT JOIN person ur ON ur.id = person.usual_reviewer_id 
  LEFT JOIN person r ON staff_review_meeting.reviewer_id = r.id 
  LEFT JOIN (
    SELECT 
      max(
        staff_review_meeting.date_of_meeting
      ) AS date_of_meeting, 
      staff_review_meeting.person_id 
    FROM 
      staff_review_meeting 
    GROUP BY 
      staff_review_meeting.person_id
  ) most_recent ON person.id = most_recent.person_id 
  JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id 
  JOIN cache._latest_role lr ON person.id = lr.person_id 
  JOIN (
    SELECT 
      max(_all_roles.funding_end_date) AS funding_end_date, 
      _all_roles.person_id 
    FROM 
      _all_roles_v12 _all_roles 
    GROUP BY 
      _all_roles.person_id
  ) newest_funding ON newest_funding.person_id = person.id 
WHERE 
  lr.role_tablename = 'post_history' :: text;
ALTER TABLE 
  hotwire."30_Report/Staff_Review_and_Development_Historical" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Review_and_Development_Historical" TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Review_and_Development_Historical" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Staff_Review_and_Development_Historical" TO hr;
COMMENT ON VIEW hotwire."30_Report/Staff_Review_and_Development_Historical" IS 'see ticket 113519 but like Staff_Review_and_Development but with gender and past staff included';

-- People_Without_Roles
CREATE 
OR REPLACE VIEW hotwire."30_Report/People_Without_Roles" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.email_address, 
  a.crsid, 
  a.arrival_date, 
  a.leaving_date, 
  a.location, 
  a.date_of_birth, 
  a.other_information, 
  a.notes, 
  a.extra_filemaker_data, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      role_count.id, 
      role_count.id AS person_id, 
      person.email_address, 
      person.crsid, 
      person.arrival_date, 
      person.leaving_date, 
      person.location, 
      person.date_of_birth, 
      person.other_information, 
      person.notes, 
      person.extra_filemaker_data, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      (
        SELECT 
          person.id, 
          count(_all_roles.person_id) AS number_of_roles 
        FROM 
          person 
          LEFT JOIN _all_roles_v12 _all_roles ON person.id = _all_roles.person_id 
        WHERE 
          person.is_spri IS NOT TRUE 
        GROUP BY 
          person.id
      ) role_count 
      JOIN person USING (id) 
    WHERE 
      role_count.number_of_roles = 0
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;
ALTER TABLE 
  hotwire."30_Report/People_Without_Roles" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/People_Without_Roles" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/People_Without_Roles" TO mgmt_ro;
GRANT ALL ON TABLE hotwire."30_Report/People_Without_Roles" TO cen1001;

