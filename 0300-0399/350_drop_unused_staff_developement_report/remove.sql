CREATE OR REPLACE VIEW hotwire."30_Report/Staff_Review_and_Development2" AS 
 SELECT DISTINCT person.id, person.id AS person_id, lr.post_category_id, lr.supervisor_id, person.usual_reviewer_id, ur.email_address AS usual_reviewer_email, staff_review_meeting.reviewer_id, r.email_address AS reviewer_email, most_recent.date_of_meeting, ARRAY( SELECT other_meetings.date_of_meeting
           FROM (         SELECT staff_review_meeting.date_of_meeting, staff_review_meeting.person_id
                           FROM staff_review_meeting
                EXCEPT 
                         SELECT max(staff_review_meeting.date_of_meeting) AS max, staff_review_meeting.person_id
                           FROM staff_review_meeting
                          GROUP BY staff_review_meeting.person_id) other_meetings
          WHERE other_meetings.person_id = person.id) AS others, person.next_review_due, person.continuous_employment_start_date, newest_funding.funding_end_date
   FROM person
   LEFT JOIN cache._latest_role _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN staff_review_meeting ON staff_review_meeting.person_id = person.id
   LEFT JOIN person ur ON ur.id = person.usual_reviewer_id
   LEFT JOIN person r ON staff_review_meeting.reviewer_id = r.id
   LEFT JOIN ( SELECT max(staff_review_meeting.date_of_meeting) AS date_of_meeting, staff_review_meeting.person_id
   FROM staff_review_meeting
  GROUP BY staff_review_meeting.person_id) most_recent ON person.id = most_recent.person_id
   JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
   JOIN cache._latest_role lr ON person.id = lr.person_id
   JOIN ( SELECT max(_all_roles_v9.funding_end_date) AS funding_end_date, _all_roles_v9.person_id
   FROM _all_roles_v9
  GROUP BY _all_roles_v9.person_id) newest_funding ON newest_funding.person_id = person.id
  WHERE _physical_status_v3.status_id::text = 'Current'::text AND lr.role_tablename = 'post_history'::text;

ALTER TABLE hotwire."30_Report/Staff_Review_and_Development2"
  OWNER TO cen1001;

