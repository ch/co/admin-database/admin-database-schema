CREATE OR REPLACE VIEW hotwire."30_Report/Post_Details" AS 
 SELECT a.id, a.post_name_for_database, a.person_id, a.supervisor_id, a.staff_category_id, a.continuous_employment_start_date, a.end_date, a.paid_by_university
   FROM ( SELECT staff_post.id, staff_post.name AS post_name_for_database, person.id AS person_id, post_history.supervisor_id, staff_post.staff_category_id, person.continuous_employment_start_date, post_history.end_date, post_history.paid_by_university
           FROM post_history
      LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id
   LEFT JOIN person ON post_history.person_id = person.id) a
  ORDER BY a.post_name_for_database;

ALTER TABLE hotwire."30_Report/Post_Details"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Post_Details" TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Post_Details" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Post_Details" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Post_Details" TO hr;

