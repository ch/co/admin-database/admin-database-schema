CREATE VIEW apps.user_accounts_to_disable AS
SELECT
    person.crsid,
    person.surname,
    person.first_names,
    _latest_role.post_category,
    person.computer_account_closure_date,
    person.leaving_date,
    ( 
        SELECT 
            min(all_future_start_dates.start_date) AS min
        FROM (
            SELECT a.start_date
            FROM _all_roles_v12 a
            WHERE a.person_id = person.id AND a.start_date > ('now'::text::date - '1 mon'::interval)
        ) all_future_start_dates
    ) AS next_role_start_date
FROM person
JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
LEFT JOIN _latest_role_v12 AS _latest_role USING (person_id)
WHERE _physical_status_v3.status_id::text = 'Past'::text AND person.is_spri IS NOT TRUE;

ALTER TABLE apps.user_accounts_to_disable OWNER TO dev;
GRANT ALL ON TABLE apps.user_accounts_to_disable TO dev;
GRANT SELECT ON TABLE apps.user_accounts_to_disable TO ad_accounts;

