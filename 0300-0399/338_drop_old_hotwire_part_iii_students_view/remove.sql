CREATE OR REPLACE VIEW part_iii_students_view AS 
 SELECT part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title AS project_title_html, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, _all_roles_v9.status AS ro_role_status_id, part_iii_studentship.notes, part_iii_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
   FROM part_iii_studentship
   LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = part_iii_studentship.id
   LEFT JOIN person ON person.id = part_iii_studentship.person_id
  WHERE _all_roles_v9.role_tablename = 'part_iii_studentship'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE part_iii_students_view
  OWNER TO cen1001;
GRANT ALL ON TABLE part_iii_students_view TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE part_iii_students_view TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE part_iii_students_view TO student_management;
GRANT SELECT ON TABLE part_iii_students_view TO mgmt_ro;

-- Rule: part_iii_del ON part_iii_students_view

-- DROP RULE part_iii_del ON part_iii_students_view;

CREATE OR REPLACE RULE part_iii_del AS
    ON DELETE TO part_iii_students_view DO INSTEAD  DELETE FROM part_iii_studentship
  WHERE part_iii_studentship.id = old.id;

-- Rule: part_iii_ins ON part_iii_students_view

-- DROP RULE part_iii_ins ON part_iii_students_view;

CREATE OR REPLACE RULE part_iii_ins AS
    ON INSERT TO part_iii_students_view DO INSTEAD  INSERT INTO part_iii_studentship (person_id, project_title, supervisor_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past) 
  VALUES (new.person_id, new.project_title_html, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.notes, new.force_role_status_to_past)
  RETURNING part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, NULL::character varying(10) AS "varchar", part_iii_studentship.notes, part_iii_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

-- Rule: part_iii_upd ON part_iii_students_view

-- DROP RULE part_iii_upd ON part_iii_students_view;

CREATE OR REPLACE RULE part_iii_upd AS
    ON UPDATE TO part_iii_students_view DO INSTEAD  UPDATE part_iii_studentship SET project_title = new.project_title_html, person_id = new.person_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past
  WHERE part_iii_studentship.id = old.id;


