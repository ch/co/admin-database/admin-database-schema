CREATE OR REPLACE VIEW hotwire."30_Report/Staff_Mentors" AS 
 SELECT a.id, a.person_id, a.staff_category_id, a.continuous_employment_start_date, a.email_address, a.supervisor_id, a.supervisor_email_address, a.mentor_id, a.mentor_email_address, a._surname, a._first_names
   FROM ( SELECT post_history.id, post_history.person_id, sp.staff_category_id, person.continuous_employment_start_date, person.email_address, post_history.supervisor_id, supervisor.email_address AS supervisor_email_address, post_history.mentor_id, mentor.email_address AS mentor_email_address, person.surname AS _surname, person.first_names AS _first_names
           FROM post_history
      LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN person mentor ON post_history.mentor_id = mentor.id
   LEFT JOIN person supervisor ON post_history.supervisor_id = supervisor.id
   LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = post_history.id AND _all_roles_v9.role_tablename = 'post_history'::text
   LEFT JOIN staff_post sp ON post_history.staff_post_id = sp.id
  WHERE _all_roles_v9.status::text = 'Current'::text) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."30_Report/Staff_Mentors"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Staff_Mentors" TO cen1001;
GRANT SELECT ON TABLE hotwire."30_Report/Staff_Mentors" TO hr;
GRANT SELECT ON TABLE hotwire."30_Report/Staff_Mentors" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Staff_Mentors" TO dev;

