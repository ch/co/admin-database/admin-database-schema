-- DROP VIEW post_details_report;

CREATE OR REPLACE VIEW post_details_report AS 
 SELECT staff_post.id, staff_post.name AS post_name_for_database, person.id AS person_id, post_history.supervisor_id, staff_post.staff_category_id, person.continuous_employment_start_date, post_history.end_date, post_history.paid_by_university
   FROM post_history
   LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id
   LEFT JOIN person ON post_history.person_id = person.id
  ORDER BY staff_post.name;

ALTER TABLE post_details_report
  OWNER TO cen1001;
GRANT ALL ON TABLE post_details_report TO cen1001;
GRANT ALL ON TABLE post_details_report TO dev;
GRANT SELECT ON TABLE post_details_report TO mgmt_ro;
GRANT SELECT ON TABLE post_details_report TO hr;


-- DROP VIEW retirement_dates_report;

CREATE OR REPLACE VIEW retirement_dates_report AS 
 SELECT post_history.id, post_history.person_id, person.date_of_birth, staff_post.staff_category_id, post_history.start_date, post_history.end_date, post_history.intended_end_date, post_history.funding_end_date, post_history.job_title, person.surname AS _surname, person.first_names AS _first_names
   FROM post_history
   LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id
   LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN staff_category ON staff_category.id = staff_post.staff_category_id
  WHERE (person.left_but_no_leaving_date_given = false OR person.left_but_no_leaving_date_given IS NULL) AND COALESCE(post_history.end_date, post_history.intended_end_date) > 'now'::text::date AND (post_history.funding_end_date IS NULL OR post_history.funding_end_date > COALESCE(post_history.funding_end_date, post_history.intended_end_date)) AND (staff_category.category::text = 'Academic staff'::text OR staff_category.category::text = 'Academic-related staff'::text OR staff_category.category::text = 'Assistant staff'::text)
  ORDER BY person.surname, person.first_names;

ALTER TABLE retirement_dates_report
  OWNER TO cen1001;
GRANT ALL ON TABLE retirement_dates_report TO cen1001;
GRANT ALL ON TABLE retirement_dates_report TO dev;
GRANT SELECT ON TABLE retirement_dates_report TO mgmt_ro;
GRANT SELECT ON TABLE retirement_dates_report TO hr;


-- DROP VIEW staff_personnel_report;

CREATE OR REPLACE VIEW staff_personnel_report AS 
 SELECT (person.id::text || '-'::text) || supervisee.id::text AS id, supervisor_hid.supervisor_hid AS supervisor, supervisee_hid.supervisee_hid AS supervisee, staff_post.staff_category_id, post_history.job_title, supervisee.continuous_employment_start_date, COALESCE(post_history.funding_end_date, post_history.intended_end_date) AS end_date, mentor_hid.mentor_hid AS mentor
   FROM person
   JOIN _latest_role_v8 ON _latest_role_v8.supervisor_id = person.id
   JOIN post_history ON _latest_role_v8.role_id = post_history.id
   JOIN person supervisee ON post_history.person_id = supervisee.id
   JOIN staff_post ON post_history.staff_post_id = staff_post.id
   JOIN _physical_status_v2 supervisor_status ON person.id = supervisor_status.person_id
   JOIN _physical_status_v2 supervisee_status ON supervisee.id = supervisee_status.person_id
   JOIN supervisor_hid ON supervisor_hid.supervisor_id = person.id
   JOIN supervisee_hid ON supervisee_hid.supervisee_id = supervisee.id
   LEFT JOIN mentor_hid ON mentor_hid.mentor_id = post_history.mentor_id
  WHERE supervisor_status.status_id::text = 'Current'::text AND supervisee_status.status_id::text = 'Current'::text AND _latest_role_v8.role_tablename = 'post_history'::text
  ORDER BY supervisor_hid.supervisor_hid, supervisee_hid.supervisee_hid;

ALTER TABLE staff_personnel_report
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_personnel_report TO cen1001;
GRANT SELECT ON TABLE staff_personnel_report TO dev;
GRANT SELECT ON TABLE staff_personnel_report TO hr;

