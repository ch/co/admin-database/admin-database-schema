CREATE OR REPLACE VIEW staff_review_and_development_report AS 
 SELECT DISTINCT person.id, person.id AS person_id, lr.post_category_id, lr.supervisor_id, person.usual_reviewer_id, staff_review_meeting.reviewer_id, most_recent.date_of_meeting, ARRAY( SELECT other_meetings.date_of_meeting
           FROM (         SELECT staff_review_meeting.date_of_meeting, staff_review_meeting.person_id
                           FROM staff_review_meeting
                EXCEPT 
                         SELECT max(staff_review_meeting.date_of_meeting) AS max, staff_review_meeting.person_id
                           FROM staff_review_meeting
                          GROUP BY staff_review_meeting.person_id) other_meetings
          WHERE other_meetings.person_id = person.id) AS others, person.next_review_due, person.continuous_employment_start_date, newest_funding.funding_end_date
   FROM person
   LEFT JOIN cache._latest_role _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN staff_review_meeting ON staff_review_meeting.person_id = person.id
   LEFT JOIN ( SELECT max(staff_review_meeting.date_of_meeting) AS date_of_meeting, staff_review_meeting.person_id
   FROM staff_review_meeting
  GROUP BY staff_review_meeting.person_id) most_recent ON person.id = most_recent.person_id
   JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
   JOIN cache._latest_role lr ON person.id = lr.person_id
   JOIN ( SELECT max(_all_roles_v9.funding_end_date) AS funding_end_date, _all_roles_v9.person_id
   FROM _all_roles_v9
  GROUP BY _all_roles_v9.person_id) newest_funding ON newest_funding.person_id = person.id
  WHERE _physical_status_v3.status_id::text = 'Current'::text AND lr.role_tablename = 'post_history'::text;

ALTER TABLE staff_review_and_development_report
  OWNER TO dev;
GRANT ALL ON TABLE staff_review_and_development_report TO dev;
GRANT ALL ON TABLE staff_review_and_development_report TO cen1001;
GRANT SELECT ON TABLE staff_review_and_development_report TO hr;
