CREATE OR REPLACE VIEW hotwire."30_Report/Current_Postdocs2" AS 
 SELECT post_history.id, post_history.person_id, post_history.supervisor_id, post_history.chem, _latest_role_v8.post_category_id, person.arrival_date, post_history.start_date AS latest_funding_start_date, post_history.intended_end_date, post_history.funding_end_date
   FROM post_history
   LEFT JOIN _latest_role_v8 ON post_history.id = _latest_role_v8.role_id AND _latest_role_v8.role_tablename = 'post_history'::text
   LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
  WHERE _physical_status_v2.status_id::text = 'Current'::text AND (_latest_role_v8.post_category::text = 'PDRA'::text OR _latest_role_v8.post_category::text = 'Senior PDRA'::text OR _latest_role_v8.post_category::text = 'Research Fellow'::text OR _latest_role_v8.post_category::text = 'Research Assistant'::text OR _latest_role_v8.post_category::text = 'Teaching Fellow'::text);

ALTER TABLE hotwire."30_Report/Current_Postdocs2"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs2" TO cen1001;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs2" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs2" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs2" TO hr;

CREATE OR REPLACE VIEW hotwire."30_Report/Current_Postdocs" AS 
 SELECT post_history.id, post_history.person_id, post_history.supervisor_id, post_history.chem, _all_roles.post_category_id, person.arrival_date, post_history.start_date AS latest_funding_start_date, post_history.intended_end_date, post_history.funding_end_date
   FROM post_history
   LEFT JOIN _all_roles_v12 _all_roles ON post_history.id = _all_roles.role_id AND _all_roles.role_tablename = 'post_history'::text
   LEFT JOIN person ON post_history.person_id = person.id
  WHERE _all_roles.status::text = 'Current'::text AND (_all_roles.post_category::text = 'PDRA'::text OR _all_roles.post_category::text = 'Senior PDRA'::text OR _all_roles.post_category::text = 'Research Fellow'::text OR _all_roles.post_category::text = 'Principal Research Associate'::text);

ALTER TABLE hotwire."30_Report/Current_Postdocs"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs" TO hr;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs" TO cen1001;

