DROP VIEW hotwire."30_Report/Current_Postdocs2";

CREATE OR REPLACE VIEW hotwire."30_Report/Current_Postdocs" AS 
 SELECT post_history.id, post_history.person_id, post_history.supervisor_id, post_history.chem, _all_roles.post_category_id, person.arrival_date, post_history.start_date AS latest_funding_start_date, post_history.intended_end_date, post_history.funding_end_date
   FROM post_history
   LEFT JOIN _all_roles_v12 _all_roles ON post_history.id = _all_roles.role_id AND _all_roles.role_tablename = 'post_history'::text
   LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.person_id = post_history.person_id
  WHERE _all_roles.status::text = 'Current'::text AND _physical_status_v2.status_id = 'Current' AND (_all_roles.post_category::text = 'PDRA'::text OR _all_roles.post_category::text = 'Senior PDRA'::text OR _all_roles.post_category::text = 'Research Fellow'::text OR _all_roles.post_category::text = 'Principal Research Associate'::text);

ALTER TABLE hotwire."30_Report/Current_Postdocs"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Current_Postdocs" TO hr;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs" TO cen1001;

