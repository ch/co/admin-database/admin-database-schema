CREATE 
OR REPLACE VIEW hotwire."30_Report/Searching" AS 
SELECT 
  (
    person.id || COALESCE(
      roles.post_category, '' :: character varying
    ):: text
  ) || COALESCE(
    roles.start_date :: text, '' :: text
  ) AS id, 
  person.surname, 
  person.first_names, 
  person.title_id, 
  person.known_as, 
  person.gender_id, 
  date_part(
    'year' :: text, 
    age(
      'now' :: text :: date :: timestamp with time zone, 
      person.date_of_birth :: timestamp with time zone
    )
  ) AS ro_age, 
  person.arrival_date, 
  person.leaving_date, 
  _physical_status_v2.status_id AS physical_status_id, 
  roles.start_date, 
  roles.intended_end_date, 
  roles.end_date, 
  person.visa_start_date, 
  person.visa_end_date, 
  roles.funding_end_date, 
  roles.post_category, 
  roles.status AS role_status_id, 
  roles.chem, 
  person.date_of_birth, 
  person.registration_completed, 
  person.clearance_cert_signed, 
  person.email_address, 
  person.crsid, 
  person.hide_email, 
  person.permission_to_give_out_details, 
  ARRAY(
    SELECT 
      mm_person_room.room_id 
    FROM 
      mm_person_room 
    WHERE 
      mm_person_room.person_id = person.id
  ) AS room_id, 
  person.location, 
  ARRAY(
    SELECT 
      mm_person_dept_telephone_number.dept_telephone_number_id 
    FROM 
      mm_person_dept_telephone_number 
    WHERE 
      mm_person_dept_telephone_number.person_id = person.id
  ) AS dept_telephone_number_id, 
  person.external_work_phone_numbers, 
  person.college_phone_number, 
  person.mobile_number, 
  person.pager, 
  person.cambridge_phone_number, 
  person.home_phone_number, 
  ARRAY(
    SELECT 
      mm_person_research_group.research_group_id 
    FROM 
      mm_person_research_group 
    WHERE 
      mm_person_research_group.person_id = person.id
  ) AS research_group_id, 
  ARRAY(
    SELECT 
      research_group.sector_id 
    FROM 
      mm_person_research_group 
      JOIN research_group ON research_group.id = mm_person_research_group.research_group_id 
    WHERE 
      mm_person_research_group.person_id = person.id
  ) AS sector_id, 
  roles.supervisor_id, 
  roles.manager_id, 
  roles.paid_by_university, 
  person.continuous_employment_start_date, 
  person.payroll_personal_reference_number, 
  person.paper_file_details, 
  person.ni_number, 
  roles.job_title, 
  roles.hesa_leaving_code, 
  roles.current_funding, 
  roles.research_grant_number, 
  roles.university, 
  ARRAY(
    SELECT 
      mm_person_nationality.nationality_id 
    FROM 
      mm_person_nationality 
    WHERE 
      mm_person_nationality.person_id = person.id
  ) AS nationality_id, 
  person.cambridge_college_id, 
  roles.cpgs_or_mphil_date_submission_due, 
  roles.cpgs_or_mphil_date_submitted, 
  roles.cpgs_or_mphil_date_awarded, 
  roles.mphil_date_submission_due, 
  roles.mphil_date_submitted, 
  roles.mphil_date_awarded, 
  roles.date_registered_for_phd, 
  roles.phd_date_title_approved, 
  roles.date_phd_submission_due, 
  roles.phd_four_year_submission_date, 
  roles.phd_date_submitted, 
  roles.phd_date_examiners_appointed, 
  roles.phd_date_of_viva, 
  roles.phd_date_awarded, 
  roles.co_supervisor_id, 
  roles.mentor_id, 
  roles.co_mentor_id, 
  roles.gaf_number, 
  roles.emplid_number, 
  roles.cpgs_title_html, 
  roles.mphil_title_html, 
  roles.msc_title_html, 
  roles.phd_thesis_title_html, 
  roles.next_mentor_meeting_due, 
  roles.fees_funding, 
  roles.role_or_job_code_number, 
  roles.position_reference_number, 
  roles.part_iii_project_title_html, 
  roles.date_of_first_probation_meeting, 
  roles.date_of_second_probation_meeting, 
  roles.date_of_third_probation_meeting, 
  roles.date_probation_letters_sent, 
  roles.probation_period, 
  person.usual_reviewer_id AS reviewer_id, 
  roles.next_review_due, 
  roles.timing_of_reviews, 
  roles.transferable_skills_days_1st_year, 
  roles.transferable_skills_days_2nd_year, 
  roles.transferable_skills_days_3rd_year, 
  roles.transferable_skills_days_4th_year, 
  person.forwarding_address, 
  person.emergency_contact, 
  person.home_address, 
  person.new_employer_address, 
  person.certificate_of_sponsorship_number, 
  person.cambridge_address, 
  person.maiden_name, 
  person.other_information, 
  person.notes, 
  person.extra_filemaker_data, 
  roles.progress_notes 
FROM 
  person 
  LEFT JOIN (
    (
      (
        (
          SELECT 
            v.person_id, 
            v.start_date, 
            v.intended_end_date, 
            v.end_date, 
            NULL :: date AS funding_end_date, 
            false AS chem, 
            _all_roles.post_category, 
            NULL :: text AS job_title, 
            NULL :: bigint AS manager_id, 
            NULL :: text AS progress_notes, 
            v.host_person_id AS supervisor_id, 
            NULL :: text AS university, 
            NULL :: text AS gaf_number, 
            NULL :: bigint AS postgraduate_studentship_type_id, 
            NULL :: text AS cpgs_title_html, 
            NULL :: text AS mphil_title_html, 
            NULL :: text AS msc_title_html, 
            NULL :: date AS cpgs_or_mphil_date_submission_due, 
            NULL :: date AS cpgs_or_mphil_date_submitted, 
            NULL :: date AS cpgs_or_mphil_date_awarded, 
            NULL :: date AS mphil_date_submission_due, 
            NULL :: date AS mphil_date_submitted, 
            NULL :: date AS mphil_date_awarded, 
            NULL :: date AS date_registered_for_phd, 
            NULL :: text AS phd_thesis_title_html, 
            NULL :: date AS phd_date_title_approved, 
            NULL :: date AS date_phd_submission_due, 
            NULL :: date AS phd_four_year_submission_date, 
            NULL :: date AS phd_date_submitted, 
            NULL :: date AS phd_date_examiners_appointed, 
            NULL :: date AS phd_date_of_viva, 
            NULL :: date AS phd_date_awarded, 
            NULL :: bigint AS co_supervisor_id, 
            NULL :: bigint AS mentor_id, 
            NULL :: bigint AS co_mentor_id, 
            NULL :: date AS next_mentor_meeting_due, 
            NULL :: text AS current_funding, 
            NULL :: text AS fees_funding, 
            NULL :: text AS role_or_job_code_number, 
            NULL :: text AS position_reference_number, 
            NULL :: text AS research_grant_number, 
            NULL :: text AS part_iii_project_title_html, 
            NULL :: boolean AS paid_by_university, 
            NULL :: date AS date_of_first_probation_meeting, 
            NULL :: date AS date_of_second_probation_meeting, 
            NULL :: date AS date_of_third_probation_meeting, 
            NULL :: date AS date_probation_letters_sent, 
            NULL :: text AS probation_period, 
            NULL :: text AS emplid_number, 
            NULL :: text AS hesa_leaving_code, 
            NULL :: date AS next_review_due, 
            NULL :: text AS timing_of_reviews, 
            NULL :: real AS transferable_skills_days_1st_year, 
            NULL :: real AS transferable_skills_days_2nd_year, 
            NULL :: real AS transferable_skills_days_3rd_year, 
            NULL :: real AS transferable_skills_days_4th_year, 
            _all_roles.status 
          FROM 
            visitorship v 
            LEFT JOIN _all_roles_v12 _all_roles ON v.id = _all_roles.role_id 
          WHERE 
            _all_roles.role_tablename = 'visitorship' :: text 
          UNION 
          SELECT 
            e.person_id, 
            e.start_date, 
            e.intended_end_date, 
            e.end_date, 
            NULL :: date AS funding_end_date, 
            false AS chem, 
            _all_roles.post_category, 
            NULL :: text AS job_title, 
            NULL :: bigint AS manager_id, 
            NULL :: text AS progress_notes, 
            e.supervisor_id, 
            e.university, 
            NULL :: text AS gaf_number, 
            NULL :: bigint AS postgraduate_studentship_type_id, 
            NULL :: text AS cpgs_title_html, 
            NULL :: text AS mphil_title_html, 
            NULL :: text AS msc_title_html, 
            NULL :: date AS cpgs_or_mphil_date_submission_due, 
            NULL :: date AS cpgs_or_mphil_date_submitted, 
            NULL :: date AS cpgs_or_mphil_date_awarded, 
            NULL :: date AS mphil_date_submission_due, 
            NULL :: date AS mphil_date_submitted, 
            NULL :: date AS mphil_date_awarded, 
            NULL :: date AS date_registered_for_phd, 
            NULL :: text AS phd_thesis_title_html, 
            NULL :: date AS phd_date_title_approved, 
            NULL :: date AS date_phd_submission_due, 
            NULL :: date AS phd_four_year_submission_date, 
            NULL :: date AS phd_date_submitted, 
            NULL :: date AS phd_date_examiners_appointed, 
            NULL :: date AS phd_date_of_viva, 
            NULL :: date AS phd_date_awarded, 
            NULL :: bigint AS co_supervisor_id, 
            NULL :: bigint AS mentor_id, 
            NULL :: bigint AS co_mentor_id, 
            NULL :: date AS next_mentor_meeting_due, 
            NULL :: text AS current_funding, 
            NULL :: text AS fees_funding, 
            NULL :: text AS role_or_job_code_number, 
            NULL :: text AS position_reference_number, 
            NULL :: text AS research_grant_number, 
            NULL :: text AS part_iii_project_title_html, 
            NULL :: boolean AS paid_by_university, 
            NULL :: date AS date_of_first_probation_meeting, 
            NULL :: date AS date_of_second_probation_meeting, 
            NULL :: date AS date_of_third_probation_meeting, 
            NULL :: date AS date_probation_letters_sent, 
            NULL :: text AS probation_period, 
            NULL :: text AS emplid_number, 
            NULL :: text AS hesa_leaving_code, 
            NULL :: date AS next_review_due, 
            NULL :: text AS timing_of_reviews, 
            NULL :: real AS transferable_skills_days_1st_year, 
            NULL :: real AS transferable_skills_days_2nd_year, 
            NULL :: real AS transferable_skills_days_3rd_year, 
            NULL :: real AS transferable_skills_days_4th_year, 
            _all_roles.status 
          FROM 
            erasmus_socrates_studentship e 
            LEFT JOIN _all_roles_v12 _all_roles ON e.id = _all_roles.role_id 
          WHERE 
            _all_roles.role_tablename = 'erasmus_socrates_studentship' :: text
        ) 
        UNION 
        SELECT 
          p.person_id, 
          p.start_date, 
          p.intended_end_date, 
          p.end_date, 
          NULL :: date AS funding_end_date, 
          false AS chem, 
          _all_roles.post_category, 
          NULL :: text AS job_title, 
          NULL :: bigint AS manager_id, 
          NULL :: text AS progress_notes, 
          p.supervisor_id, 
          NULL :: text AS university, 
          NULL :: text AS gaf_number, 
          NULL :: bigint AS postgraduate_studentship_type_id, 
          NULL :: text AS cpgs_title_html, 
          NULL :: text AS mphil_title_html, 
          NULL :: text AS msc_title_html, 
          NULL :: date AS cpgs_or_mphil_date_submission_due, 
          NULL :: date AS cpgs_or_mphil_date_submitted, 
          NULL :: date AS cpgs_or_mphil_date_awarded, 
          NULL :: date AS mphil_date_submission_due, 
          NULL :: date AS mphil_date_submitted, 
          NULL :: date AS mphil_date_awarded, 
          NULL :: date AS date_registered_for_phd, 
          NULL :: text AS phd_thesis_title_html, 
          NULL :: date AS phd_date_title_approved, 
          NULL :: date AS date_phd_submission_due, 
          NULL :: date AS phd_four_year_submission_date, 
          NULL :: date AS phd_date_submitted, 
          NULL :: date AS phd_date_examiners_appointed, 
          NULL :: date AS phd_date_of_viva, 
          NULL :: date AS phd_date_awarded, 
          NULL :: bigint AS co_supervisor_id, 
          NULL :: bigint AS mentor_id, 
          NULL :: bigint AS co_mentor_id, 
          NULL :: date AS next_mentor_meeting_due, 
          NULL :: text AS current_funding, 
          NULL :: text AS fees_funding, 
          NULL :: text AS role_or_job_code_number, 
          NULL :: text AS position_reference_number, 
          NULL :: text AS research_grant_number, 
          p.project_title AS part_iii_project_title_html, 
          NULL :: boolean AS paid_by_university, 
          NULL :: date AS date_of_first_probation_meeting, 
          NULL :: date AS date_of_second_probation_meeting, 
          NULL :: date AS date_of_third_probation_meeting, 
          NULL :: date AS date_probation_letters_sent, 
          NULL :: text AS probation_period, 
          NULL :: text AS emplid_number, 
          NULL :: text AS hesa_leaving_code, 
          NULL :: date AS next_review_due, 
          NULL :: text AS timing_of_reviews, 
          NULL :: real AS transferable_skills_days_1st_year, 
          NULL :: real AS transferable_skills_days_2nd_year, 
          NULL :: real AS transferable_skills_days_3rd_year, 
          NULL :: real AS transferable_skills_days_4th_year, 
          _all_roles.status 
        FROM 
          part_iii_studentship p 
          LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = p.id 
        WHERE 
          _all_roles.role_tablename = 'part_iii_studentship' :: text
      ) 
      UNION 
      SELECT 
        ph.person_id, 
        ph.start_date, 
        ph.intended_end_date, 
        ph.end_date, 
        ph.funding_end_date, 
        ph.chem, 
        _all_roles.post_category, 
        ph.job_title, 
        ph.supervisor_id AS manager_id, 
        NULL :: text AS progress_notes, 
        ph.supervisor_id, 
        NULL :: text AS university, 
        NULL :: text AS gaf_number, 
        NULL :: bigint AS postgraduate_studentship_type_id, 
        NULL :: text AS cpgs_title_html, 
        NULL :: text AS mphil_title_html, 
        NULL :: text AS msc_title_html, 
        NULL :: date AS cpgs_or_mphil_date_submission_due, 
        NULL :: date AS cpgs_or_mphil_date_submitted, 
        NULL :: date AS cpgs_or_mphil_date_awarded, 
        NULL :: date AS mphil_date_submission_due, 
        NULL :: date AS mphil_date_submitted, 
        NULL :: date AS mphil_date_awarded, 
        NULL :: date AS date_registered_for_phd, 
        NULL :: text AS phd_thesis_title_html, 
        NULL :: date AS phd_date_title_approved, 
        NULL :: date AS date_phd_submission_due, 
        NULL :: date AS phd_four_year_submission_date, 
        NULL :: date AS phd_date_submitted, 
        NULL :: date AS phd_date_examiners_appointed, 
        NULL :: date AS phd_date_of_viva, 
        NULL :: date AS phd_date_awarded, 
        NULL :: bigint AS co_supervisor_id, 
        ph.mentor_id, 
        NULL :: bigint AS co_mentor_id, 
        ph.next_mentor_meeting_due, 
        ph.filemaker_funding AS current_funding, 
        NULL :: text AS fees_funding, 
        staff_post.role_code_number AS role_or_job_code_number, 
        staff_post.chris_position_reference AS position_reference_number, 
        ph.research_grant_number, 
        NULL :: text AS part_iii_project_title_html, 
        ph.paid_by_university, 
        ph.date_of_first_probation_meeting, 
        ph.date_of_second_probation_meeting, 
        ph.date_of_third_probation_meeting, 
        ph.date_probation_letters_sent, 
        ph.probation_period, 
        NULL :: text AS emplid_number, 
        ph.hesa_leaving_code, 
        ph.next_review_due, 
        ph.timing_of_reviews, 
        NULL :: real AS transferable_skills_days_1st_year, 
        NULL :: real AS transferable_skills_days_2nd_year, 
        NULL :: real AS transferable_skills_days_3rd_year, 
        NULL :: real AS transferable_skills_days_4th_year, 
        _all_roles.status 
      FROM 
        post_history ph 
        LEFT JOIN staff_post ON staff_post.id = ph.staff_post_id 
        LEFT JOIN staff_category ON staff_post.staff_category_id = staff_category.id 
        LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = ph.id 
      WHERE 
        _all_roles.role_tablename = 'post_history' :: text
    ) 
    UNION 
    SELECT 
      pg.person_id, 
      pg.start_date, 
      _all_roles.intended_end_date, 
      _all_roles.end_date, 
      pg.funding_end_date, 
      true AS chem, 
      _all_roles.post_category, 
      NULL :: text AS job_title, 
      NULL :: bigint AS manager_id, 
      pg.progress_notes, 
      pg.first_supervisor_id AS supervisor_id, 
      pg.university, 
      pg.gaf_number, 
      pg.postgraduate_studentship_type_id, 
      pg.cpgs_title AS cpgs_title_html, 
      pg.mphil_title AS mphil_title_html, 
      pg.msc_title AS msc_title_html, 
      pg.cpgs_or_mphil_date_submission_due, 
      pg.cpgs_or_mphil_date_submitted, 
      pg.cpgs_or_mphil_date_awarded, 
      pg.mphil_date_submission_due, 
      pg.mphil_date_submitted, 
      pg.mphil_date_awarded, 
      pg.date_registered_for_phd, 
      pg.phd_thesis_title AS phd_title_html, 
      pg.phd_date_title_approved, 
      pg.date_phd_submission_due, 
      pg.end_of_registration_date AS phd_four_year_submission_date, 
      pg.phd_date_submitted, 
      pg.phd_date_examiners_appointed, 
      pg.phd_date_of_viva, 
      pg.phd_date_awarded, 
      pg.second_supervisor_id AS co_supervisor_id, 
      pg.first_mentor_id AS mentor_id, 
      pg.second_mentor_id AS co_mentor_id, 
      pg.next_mentor_meeting_due, 
      pg.filemaker_funding AS current_funding, 
      pg.filemaker_fees_funding AS fees_funding, 
      NULL :: text AS role_or_job_code_number, 
      NULL :: text AS position_reference_number, 
      NULL :: text AS research_grant_number, 
      NULL :: text AS part_iii_project_title_html, 
      pg.paid_through_payroll AS paid_by_university, 
      NULL :: date AS date_of_first_probation_meeting, 
      NULL :: date AS date_of_second_probation_meeting, 
      NULL :: date AS date_of_third_probation_meeting, 
      NULL :: date AS date_probation_letters_sent, 
      NULL :: text AS probation_period, 
      pg.emplid_number, 
      pg.hesa_leaving_code, 
      NULL :: date AS next_review_due, 
      NULL :: text AS timing_of_reviews, 
      pg.transferable_skills_days_1st_year, 
      pg.transferable_skills_days_2nd_year, 
      pg.transferable_skills_days_3rd_year, 
      pg.transferable_skills_days_4th_year, 
      _all_roles.status 
    FROM 
      postgraduate_studentship pg 
      LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = pg.postgraduate_studentship_type_id 
      LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = pg.id 
    WHERE 
      _all_roles.role_tablename = 'postgraduate_studentship' :: text
  ) roles ON roles.person_id = person.id 
  LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id 
ORDER BY 
  person.surname, 
  person.first_names, 
  roles.start_date;
ALTER TABLE 
  hotwire."30_Report/Searching" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Searching" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Searching" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Searching" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Searching" TO hr;

