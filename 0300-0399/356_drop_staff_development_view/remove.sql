CREATE OR REPLACE VIEW hotwire."10_View/Staff_Development" AS 
 SELECT post_history.id, post_history.supervisor_id, post_history.person_id, _all_roles_v9.status AS ro_role_status_id, post_history.mentor_id, post_history.external_mentor, person.arrival_date AS arrival_date_in_department, person.continuous_employment_start_date, post_history.start_date AS contract_start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.research_grant_number, post_history.filemaker_funding AS sponsor, _all_roles_v9.post_category_id AS ro_post_category_id
   FROM post_history
   LEFT JOIN _all_roles_v9 ON post_history.id = _all_roles_v9.role_id AND _all_roles_v9.role_tablename = 'post_history'::text
   JOIN staff_post ON post_history.staff_post_id = staff_post.id
   JOIN staff_category ON staff_post.staff_category_id = staff_category.id
   JOIN person ON post_history.person_id = person.id
   LEFT JOIN supervisor_hid ON post_history.supervisor_id = supervisor_hid.supervisor_id
   JOIN person_hid ON post_history.person_id = person_hid.person_id
  WHERE _all_roles_v9.status::text = 'Current'::text
  ORDER BY supervisor_hid.supervisor_hid, person_hid.person_hid;

ALTER TABLE hotwire."10_View/Staff_Development"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Staff_Development" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Staff_Development" TO dev;

