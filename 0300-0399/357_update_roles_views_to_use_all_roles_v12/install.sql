-- hotwire."10_View/Roles/Erasmus_Students"

CREATE 
OR REPLACE VIEW hotwire."10_View/Roles/Erasmus_Students" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.erasmus_type_id, 
  a.supervisor_id, 
  a.email_address, 
  a.start_date, 
  a.intended_end_date, 
  a.end_date, 
  a.cambridge_college_id, 
  a.university, 
  a.ro_role_status_id, 
  a.notes, 
  a.force_role_status_to_past, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      erasmus_socrates_studentship.id, 
      erasmus_socrates_studentship.person_id, 
      erasmus_socrates_studentship.erasmus_type_id, 
      erasmus_socrates_studentship.supervisor_id, 
      person.email_address, 
      erasmus_socrates_studentship.start_date, 
      erasmus_socrates_studentship.intended_end_date, 
      erasmus_socrates_studentship.end_date, 
      person.cambridge_college_id, 
      erasmus_socrates_studentship.university, 
      _all_roles.status AS ro_role_status_id, 
      erasmus_socrates_studentship.notes, 
      erasmus_socrates_studentship.force_role_status_to_past, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      erasmus_socrates_studentship 
      JOIN person ON person.id = erasmus_socrates_studentship.person_id 
      LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = erasmus_socrates_studentship.id 
      AND _all_roles.role_tablename = 'erasmus_socrates_studentship' :: text
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;
ALTER TABLE 
  hotwire."10_View/Roles/Erasmus_Students" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Erasmus_Students" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Roles/Erasmus_Students" TO dev;
GRANT 
SELECT 
  , 
UPDATE 
  , 
  INSERT, 
  DELETE ON TABLE hotwire."10_View/Roles/Erasmus_Students" TO student_management;


-- hotwire."10_View/Roles/Part_III_Students"

CREATE 
OR REPLACE VIEW hotwire."10_View/Roles/Part_III_Students" AS 
SELECT 
  a.id, 
  a.person_id, 
  a.project_title_html, 
  a.supervisor_id, 
  a.intended_end_date, 
  a.end_date, 
  a.start_date, 
  a.ro_role_status_id, 
  a.notes, 
  a.force_role_status_to_past, 
  a._surname, 
  a._first_names 
FROM 
  (
    SELECT 
      part_iii_studentship.id, 
      part_iii_studentship.person_id, 
      part_iii_studentship.project_title AS project_title_html, 
      part_iii_studentship.supervisor_id, 
      part_iii_studentship.intended_end_date, 
      part_iii_studentship.end_date, 
      part_iii_studentship.start_date, 
      _all_roles.status AS ro_role_status_id, 
      part_iii_studentship.notes, 
      part_iii_studentship.force_role_status_to_past, 
      person.surname AS _surname, 
      person.first_names AS _first_names 
    FROM 
      part_iii_studentship 
      LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.role_id = part_iii_studentship.id 
      LEFT JOIN person ON person.id = part_iii_studentship.person_id 
    WHERE 
      _all_roles.role_tablename = 'part_iii_studentship' :: text
  ) a 
ORDER BY 
  a._surname, 
  a._first_names;
ALTER TABLE 
  hotwire."10_View/Roles/Part_III_Students" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Part_III_Students" TO cen1001;
GRANT 
SELECT 
  , 
UPDATE 
  , 
  INSERT, 
  DELETE ON TABLE hotwire."10_View/Roles/Part_III_Students" TO dev;
GRANT 
SELECT 
  , 
UPDATE 
  , 
  INSERT, 
  DELETE ON TABLE hotwire."10_View/Roles/Part_III_Students" TO student_management;
GRANT 
SELECT 
  ON TABLE hotwire."10_View/Roles/Part_III_Students" TO mgmt_ro;


-- hotwire."10_View/Roles/Post_History"

CREATE 
OR REPLACE VIEW hotwire."10_View/Roles/Post_History" AS 
SELECT 
  a.id, 
  a.staff_post_id, 
  a.person_id, 
  a.ro_role_status_id, 
  a.supervisor_id, 
  a.mentor_id, 
  a.external_mentor, 
  a.start_date_for_continuous_employment_purposes, 
  a.start_date, 
  a.intended_end_date, 
  a.end_date, 
  a.funding_end_date, 
  a.chem, 
  a.paid_by_university, 
  a.hours_worked, 
  a.percentage_of_fulltime_hours, 
  a.probation_period, 
  a.date_of_first_probation_meeting, 
  a.date_of_second_probation_meeting, 
  a.date_of_third_probation_meeting, 
  a.date_of_fourth_probation_meeting, 
  a.first_probation_meeting_comments, 
  a.second_probation_meeting_comments, 
  a.third_probation_meeting_comments, 
  a.fourth_probation_meeting_comments, 
  a.probation_outcome, 
  a.date_probation_letters_sent, 
  a.research_grant_number, 
  a.sponsor, 
  a.hesa_leaving_code, 
  a.ro_post_category_id, 
  a.job_title, 
  a.force_role_status_to_past, 
  a._name 
FROM 
  (
    SELECT 
      post_history.id, 
      post_history.staff_post_id, 
      post_history.person_id, 
      _all_roles.status AS ro_role_status_id, 
      post_history.supervisor_id, 
      post_history.mentor_id, 
      post_history.external_mentor, 
      person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, 
      post_history.start_date, 
      post_history.intended_end_date, 
      post_history.end_date, 
      post_history.funding_end_date, 
      post_history.chem, 
      post_history.paid_by_university, 
      post_history.hours_worked, 
      post_history.percentage_of_fulltime_hours, 
      post_history.probation_period, 
      post_history.date_of_first_probation_meeting, 
      post_history.date_of_second_probation_meeting, 
      post_history.date_of_third_probation_meeting, 
      post_history.date_of_fourth_probation_meeting, 
      post_history.first_probation_meeting_comments, 
      post_history.second_probation_meeting_comments, 
      post_history.third_probation_meeting_comments, 
      post_history.fourth_probation_meeting_comments, 
      post_history.probation_outcome, 
      post_history.date_probation_letters_sent, 
      post_history.research_grant_number, 
      post_history.filemaker_funding AS sponsor, 
      post_history.hesa_leaving_code, 
      _all_roles.post_category_id AS ro_post_category_id, 
      post_history.job_title, 
      post_history.force_role_status_to_past, 
      staff_post.name AS _name 
    FROM 
      post_history 
      LEFT JOIN _all_roles_v12 _all_roles ON post_history.id = _all_roles.role_id 
      AND _all_roles.role_tablename = 'post_history' :: text 
      JOIN staff_post ON post_history.staff_post_id = staff_post.id 
      JOIN staff_category ON staff_post.staff_category_id = staff_category.id 
      JOIN person ON post_history.person_id = person.id
  ) a 
ORDER BY 
  a._name;
ALTER TABLE 
  hotwire."10_View/Roles/Post_History" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Post_History" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Roles/Post_History" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."10_View/Roles/Post_History" TO mgmt_ro;
GRANT 
SELECT 
  , 
UPDATE 
  , 
  INSERT, 
  DELETE ON TABLE hotwire."10_View/Roles/Post_History" TO hr;

