-- DROP VIEW public.principal_investigators_with_students_report;

CREATE OR REPLACE VIEW principal_investigators_with_students_report AS 
 SELECT DISTINCT person.id, postgraduate_studentship.first_supervisor_id AS supervisor_id, person.email_address
   FROM postgraduate_studentship
   LEFT JOIN person ON postgraduate_studentship.first_supervisor_id = person.id
   LEFT JOIN _all_roles_v9 ON postgraduate_studentship.id = _all_roles_v9.role_id AND _all_roles_v9.role_tablename = 'postgraduate_studentship'::text
  WHERE _all_roles_v9.status::text = 'Current'::text
  ORDER BY person.id, postgraduate_studentship.first_supervisor_id, person.email_address;

ALTER TABLE principal_investigators_with_students_report
  OWNER TO cen1001;
GRANT ALL ON TABLE principal_investigators_with_students_report TO cen1001;
GRANT SELECT ON TABLE principal_investigators_with_students_report TO hr;
GRANT SELECT ON TABLE principal_investigators_with_students_report TO dev;
GRANT SELECT ON TABLE principal_investigators_with_students_report TO mgmt_ro;


-- DROP VIEW public.staff_mentors_report;


CREATE OR REPLACE VIEW staff_mentors_report AS 
 SELECT post_history.id, post_history.person_id, sp.staff_category_id, person.continuous_employment_start_date, person.email_address, post_history.supervisor_id, supervisor.email_address AS supervisor_email_address, post_history.mentor_id, mentor.email_address AS mentor_email_address, person.surname AS _surname, person.first_names AS _first_names
   FROM post_history
   LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN person mentor ON post_history.mentor_id = mentor.id
   LEFT JOIN person supervisor ON post_history.supervisor_id = supervisor.id
   LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = post_history.id AND _all_roles_v9.role_tablename = 'post_history'::text
   LEFT JOIN staff_post sp ON post_history.staff_post_id = sp.id
  WHERE _all_roles_v9.status::text = 'Current'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE staff_mentors_report
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_mentors_report TO cen1001;
GRANT SELECT ON TABLE staff_mentors_report TO hr;
GRANT SELECT ON TABLE staff_mentors_report TO mgmt_ro;
GRANT SELECT ON TABLE staff_mentors_report TO dev;


-- DROP VIEW public.current_postdocs_report;

CREATE OR REPLACE VIEW current_postdocs_report AS 
 SELECT post_history.id, post_history.person_id, post_history.supervisor_id, post_history.chem, _all_roles_v9.post_category_id, person.arrival_date, post_history.start_date AS latest_funding_start_date, post_history.intended_end_date, post_history.filemaker_funding AS funding, post_history.job_title, post_history.funding_end_date
   FROM post_history
   LEFT JOIN _all_roles_v9 ON post_history.id = _all_roles_v9.role_id AND _all_roles_v9.role_tablename = 'post_history'::text
   LEFT JOIN person ON post_history.person_id = person.id
  WHERE _all_roles_v9.status::text = 'Current'::text AND (_all_roles_v9.post_category::text = 'PDRA'::text OR _all_roles_v9.post_category::text = 'Senior PDRA'::text OR _all_roles_v9.post_category::text = 'Research Fellow'::text);

ALTER TABLE current_postdocs_report
  OWNER TO cen1001;
GRANT ALL ON TABLE current_postdocs_report TO cen1001;
GRANT SELECT ON TABLE current_postdocs_report TO dev;
GRANT SELECT ON TABLE current_postdocs_report TO mgmt_ro;
GRANT SELECT ON TABLE current_postdocs_report TO hr;


-- DROP VIEW public.staff_development_report;

CREATE OR REPLACE VIEW staff_development_report AS 
 SELECT post_history.id, post_history.supervisor_id, post_history.person_id, _all_roles_v9.status AS ro_role_status_id, post_history.mentor_id, post_history.external_mentor, person.arrival_date AS arrival_date_in_department, person.continuous_employment_start_date, post_history.start_date AS contract_start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.research_grant_number, post_history.filemaker_funding AS sponsor, _all_roles_v9.post_category_id AS ro_post_category_id
   FROM post_history
   LEFT JOIN _all_roles_v9 ON post_history.id = _all_roles_v9.role_id AND _all_roles_v9.role_tablename = 'post_history'::text
   JOIN staff_post ON post_history.staff_post_id = staff_post.id
   JOIN staff_category ON staff_post.staff_category_id = staff_category.id
   JOIN person ON post_history.person_id = person.id
   LEFT JOIN supervisor_hid ON post_history.supervisor_id = supervisor_hid.supervisor_id
   JOIN person_hid ON post_history.person_id = person_hid.person_id
  WHERE _all_roles_v9.status::text = 'Current'::text
  ORDER BY supervisor_hid.supervisor_hid, person_hid.person_hid;

ALTER TABLE staff_development_report
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_development_report TO cen1001;
GRANT ALL ON TABLE staff_development_report TO dev;
GRANT SELECT ON TABLE staff_development_report TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE staff_development_report TO hr;


-- DROP VIEW public.funding_report;

CREATE OR REPLACE VIEW funding_report AS 
 SELECT searching_report.id, searching_report.surname, searching_report.first_names, searching_report.title_id, searching_report.current_funding, searching_report.research_grant_number, searching_report.start_date, searching_report.end_date, searching_report.post_category, searching_report.paid_by_university, searching_report.supervisor_id
   FROM searching_report
  ORDER BY searching_report.surname, searching_report.first_names;

ALTER TABLE funding_report
  OWNER TO cen1001;
GRANT ALL ON TABLE funding_report TO cen1001;
GRANT ALL ON TABLE funding_report TO dev;
GRANT SELECT ON TABLE funding_report TO mgmt_ro;
GRANT SELECT ON TABLE funding_report TO student_management;
GRANT SELECT ON TABLE funding_report TO hr;


-- DROP VIEW public.short_searching_report;

CREATE OR REPLACE VIEW short_searching_report AS 
 SELECT searching_report.id, searching_report.surname, searching_report.first_names, searching_report.title_id, searching_report.known_as, searching_report.gender_id, date_part('year'::text, age('now'::text::date::timestamp with time zone, searching_report.date_of_birth::timestamp with time zone)) AS ro_age, searching_report.start_date, searching_report.end_date, searching_report.physical_status_id, searching_report.funding_end_date, searching_report.post_category, searching_report.role_status_id, searching_report.chem, searching_report.registration_completed, searching_report.clearance_cert_signed, searching_report.email_address, searching_report.hide_email, searching_report.permission_to_give_out_details, searching_report.mm_room, searching_report.mm_room_id, searching_report.location, searching_report.mm_dept_telephone_number_id, searching_report.mm_dept_telephone_number, searching_report.external_work_phone_numbers, searching_report.college_phone_number, searching_report.mm_research_group_id, searching_report.mm_research_group, searching_report.supervisor_id, searching_report.manager_id, searching_report.paid_by_university, searching_report.continuous_employment_start_date, searching_report.payroll_personal_reference_number, searching_report.paper_file_details, searching_report.ni_number, searching_report.job_title, searching_report.hesa_leaving_code, searching_report.current_funding, searching_report.research_grant_number, searching_report.university, searching_report.mm_nationality, searching_report.mm_nationality_id, searching_report.cambridge_college_id, searching_report.cpgs_or_mphil_date_submission_due, searching_report.cpgs_or_mphil_date_submitted, searching_report.cpgs_or_mphil_date_awarded, searching_report.mphil_date_submission_due, searching_report.mphil_date_submitted, searching_report.mphil_date_awarded, searching_report.date_registered_for_phd, searching_report.phd_date_title_approved, searching_report.date_phd_submission_due, searching_report.phd_four_year_submission_date, searching_report.phd_date_submitted, searching_report.phd_date_examiners_appointed, searching_report.phd_date_of_viva, searching_report.phd_date_awarded, searching_report.co_supervisor_id, searching_report.mentor_id, searching_report.co_mentor_id, searching_report.next_mentor_meeting_due, searching_report.fees_funding, searching_report.role_or_job_code_number, searching_report.position_reference_number, searching_report.part_iii_project_title_html, searching_report.emplid_number, searching_report.reviewer_id, searching_report.next_review_due, searching_report.timing_of_reviews
   FROM searching_report
  ORDER BY searching_report.surname, searching_report.first_names, searching_report.start_date;

ALTER TABLE short_searching_report
  OWNER TO cen1001;
GRANT ALL ON TABLE short_searching_report TO cen1001;
GRANT SELECT ON TABLE short_searching_report TO dev;
GRANT SELECT ON TABLE short_searching_report TO mgmt_ro;
GRANT SELECT ON TABLE short_searching_report TO hr;


-- DROP VIEW public.source_of_funds_report;

CREATE OR REPLACE VIEW source_of_funds_report AS 
 SELECT searching_report.id, searching_report.surname, searching_report.first_names, searching_report.post_category, searching_report.start_date, searching_report.current_funding, searching_report.date_registered_for_phd, searching_report.mm_nationality, searching_report.mm_nationality_id, searching_report.supervisor_id
   FROM searching_report
  ORDER BY searching_report.surname, searching_report.first_names;

ALTER TABLE source_of_funds_report
  OWNER TO cen1001;
GRANT ALL ON TABLE source_of_funds_report TO cen1001;
GRANT ALL ON TABLE source_of_funds_report TO dev;
GRANT SELECT ON TABLE source_of_funds_report TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE source_of_funds_report TO student_management;

