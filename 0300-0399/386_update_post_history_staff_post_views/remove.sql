-- This isn't strictly rversible because there's the possibility that data has been entered
-- which has no staff_post_id set. The first statement should fail if that's the case.

ALTER TABLE post_history ALTER COLUMN staff_post_id SET NOT NULL;

CREATE TRIGGER post_history_update_staff_category
    BEFORE UPDATE OR INSERT ON post_history
    FOR EACH ROW
    EXECUTE PROCEDURE post_history_staff_category_trig();

CREATE TRIGGER staff_post_update_post_history
    AFTER UPDATE ON staff_post
    FOR EACH ROW
    EXECUTE PROCEDURE staff_post_update_post_history_trig();

CREATE OR REPLACE VIEW hotwire."10_View/Staff_Post" AS 
 SELECT a.id, a.post_name_for_database, a.staff_category_id, a.grade, a.role_or_job_code_number, a.position_reference_number, a.post_is_suppressed, a.post_tenure_ended, a.current
   FROM ( SELECT staff_post.id, staff_post.name AS post_name_for_database, staff_post.staff_category_id, staff_post.grade, staff_post.role_code_number AS role_or_job_code_number, staff_post.chris_position_reference AS position_reference_number, staff_post.post_is_suppressed, staff_post.post_tenure_ended, staff_post.current
           FROM staff_post) a
  ORDER BY a.post_name_for_database;

ALTER TABLE hotwire."10_View/Staff_Post"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Staff_Post" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Staff_Post" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Staff_Post" TO hr;
GRANT ALL ON TABLE hotwire."10_View/Staff_Post" TO cen1001;

-- Rule: hotwire_view_staff_post_del ON hotwire."10_View/Staff_Post"

-- DROP RULE hotwire_view_staff_post_del ON hotwire."10_View/Staff_Post";

CREATE OR REPLACE RULE hotwire_view_staff_post_del AS
    ON DELETE TO hotwire."10_View/Staff_Post" DO INSTEAD  DELETE FROM staff_post
  WHERE staff_post.id = old.id;

-- Rule: hotwire_view_staff_post_ins ON hotwire."10_View/Staff_Post"

-- DROP RULE hotwire_view_staff_post_ins ON hotwire."10_View/Staff_Post";

CREATE OR REPLACE RULE hotwire_view_staff_post_ins AS
    ON INSERT TO hotwire."10_View/Staff_Post" DO INSTEAD  INSERT INTO staff_post (name, staff_category_id, grade, role_code_number, chris_position_reference, post_is_suppressed, post_tenure_ended, current) 
  VALUES (new.post_name_for_database, new.staff_category_id, new.grade, new.role_or_job_code_number, new.position_reference_number, new.post_is_suppressed, new.post_tenure_ended, new.current)
  RETURNING staff_post.id, staff_post.name, staff_post.staff_category_id, staff_post.grade, staff_post.role_code_number, staff_post.chris_position_reference, staff_post.post_is_suppressed, staff_post.post_tenure_ended, staff_post.current;

-- Rule: hotwire_view_staff_post_upd ON hotwire."10_View/Staff_Post"

-- DROP RULE hotwire_view_staff_post_upd ON hotwire."10_View/Staff_Post";

CREATE OR REPLACE RULE hotwire_view_staff_post_upd AS
    ON UPDATE TO hotwire."10_View/Staff_Post" DO INSTEAD  UPDATE staff_post SET name = new.post_name_for_database, grade = new.grade, role_code_number = new.role_or_job_code_number, chris_position_reference = new.position_reference_number, post_is_suppressed = new.post_is_suppressed, post_tenure_ended = new.post_tenure_ended, current = new.current
  WHERE staff_post.id = old.id;


DROP VIEW hotwire."10_View/Roles/Post_History";

CREATE OR REPLACE VIEW hotwire."10_View/Roles/Post_History" AS 
 SELECT a.id, a.staff_post_id, a.person_id, a.ro_role_status_id, a.supervisor_id, a.mentor_id, a.external_mentor, a.start_date_for_continuous_employment_purposes, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.chem, a.paid_by_university, a.hours_worked, a.percentage_of_fulltime_hours, a.probation_period, a.date_of_first_probation_meeting, a.date_of_second_probation_meeting, a.date_of_third_probation_meeting, a.date_of_fourth_probation_meeting, a.first_probation_meeting_comments, a.second_probation_meeting_comments, a.third_probation_meeting_comments, a.fourth_probation_meeting_comments, a.probation_outcome, a.date_probation_letters_sent, a.research_grant_number, a.sponsor, a.hesa_leaving_code, a.ro_post_category_id, a.job_title, a.force_role_status_to_past, a._name
   FROM ( SELECT post_history.id, post_history.staff_post_id, post_history.person_id, _all_roles.status AS ro_role_status_id, post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.research_grant_number, post_history.filemaker_funding AS sponsor, post_history.hesa_leaving_code, _all_roles.post_category_id AS ro_post_category_id, post_history.job_title, post_history.force_role_status_to_past, staff_post.name AS _name
           FROM post_history
      LEFT JOIN _all_roles_v12 _all_roles ON post_history.id = _all_roles.role_id AND _all_roles.role_tablename = 'post_history'::text
   JOIN staff_post ON post_history.staff_post_id = staff_post.id
   JOIN staff_category ON staff_post.staff_category_id = staff_category.id
   JOIN person ON post_history.person_id = person.id) a
  ORDER BY a._name;

ALTER TABLE hotwire."10_View/Roles/Post_History"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Post_History" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Roles/Post_History" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Roles/Post_History" TO hr;
GRANT ALL ON TABLE hotwire."10_View/Roles/Post_History" TO cen1001;

-- Rule: hotwire_view_post_history_del ON hotwire."10_View/Roles/Post_History"

-- DROP RULE hotwire_view_post_history_del ON hotwire."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire_view_post_history_del AS
    ON DELETE TO hotwire."10_View/Roles/Post_History" DO INSTEAD  DELETE FROM post_history
  WHERE post_history.id = old.id;

-- Rule: hotwire_view_post_history_update_rules ON hotwire."10_View/Roles/Post_History"

-- DROP RULE hotwire_view_post_history_update_rules ON hotwire."10_View/Roles/Post_History";

CREATE OR REPLACE RULE hotwire_view_post_history_update_rules AS
    ON UPDATE TO hotwire."10_View/Roles/Post_History" DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 UPDATE post_history SET staff_post_id = new.staff_post_id, supervisor_id = new.supervisor_id, mentor_id = new.mentor_id, external_mentor = new.external_mentor, start_date = new.start_date, intended_end_date = new.intended_end_date, end_date = new.end_date, funding_end_date = new.funding_end_date, chem = new.chem, paid_by_university = new.paid_by_university, hours_worked = new.hours_worked, percentage_of_fulltime_hours = new.percentage_of_fulltime_hours, probation_period = new.probation_period, date_of_first_probation_meeting = new.date_of_first_probation_meeting, date_of_second_probation_meeting = new.date_of_second_probation_meeting, date_of_third_probation_meeting = new.date_of_third_probation_meeting, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments, second_probation_meeting_comments = new.second_probation_meeting_comments, third_probation_meeting_comments = new.third_probation_meeting_comments, fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, probation_outcome = new.probation_outcome, date_probation_letters_sent = new.date_probation_letters_sent, research_grant_number = new.research_grant_number, filemaker_funding = new.sponsor, hesa_leaving_code = new.hesa_leaving_code, job_title = new.job_title, force_role_status_to_past = new.force_role_status_to_past
  WHERE post_history.id = old.id;
);

-- Rule: post_history_ins ON hotwire."10_View/Roles/Post_History"

-- DROP RULE post_history_ins ON hotwire."10_View/Roles/Post_History";

CREATE OR REPLACE RULE post_history_ins AS
    ON INSERT TO hotwire."10_View/Roles/Post_History" DO INSTEAD ( UPDATE person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes
  WHERE person.id = new.person_id;
 INSERT INTO post_history (staff_post_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, hesa_leaving_code, job_title, force_role_status_to_past) 
  VALUES (new.staff_post_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_by_university, new.hours_worked, new.percentage_of_fulltime_hours, new.probation_period, new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments, new.second_probation_meeting_comments, new.third_probation_meeting_comments, new.fourth_probation_meeting_comments, new.probation_outcome, new.date_probation_letters_sent, new.research_grant_number, new.sponsor, new.hesa_leaving_code, new.job_title, new.force_role_status_to_past)
  RETURNING post_history.id, post_history.staff_post_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.research_grant_number, post_history.filemaker_funding, post_history.hesa_leaving_code, NULL::text AS text, post_history.job_title, post_history.force_role_status_to_past, NULL::character varying(80) AS "varchar";
);


