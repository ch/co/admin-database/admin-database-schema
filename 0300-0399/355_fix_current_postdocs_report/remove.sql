CREATE 
OR REPLACE VIEW hotwire."30_Report/Current_Postdocs" AS 
SELECT 
  post_history.id, 
  post_history.person_id, 
  post_history.supervisor_id, 
  post_history.chem, 
  _all_roles_v9.post_category_id, 
  person.arrival_date, 
  post_history.start_date AS latest_funding_start_date, 
  post_history.intended_end_date, 
  post_history.funding_end_date 
FROM 
  post_history 
  LEFT JOIN _all_roles_v9 ON post_history.id = _all_roles_v9.role_id 
  AND _all_roles_v9.role_tablename = 'post_history' :: text 
  LEFT JOIN person ON post_history.person_id = person.id 
WHERE 
  _all_roles_v9.status :: text = 'Current' :: text 
  AND (
    _all_roles_v9.post_category :: text = 'PDRA' :: text 
    OR _all_roles_v9.post_category :: text = 'Senior PDRA' :: text 
    OR _all_roles_v9.post_category :: text = 'Research Fellow' :: text
  );
ALTER TABLE 
  hotwire."30_Report/Current_Postdocs" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Current_Postdocs" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Current_Postdocs" TO dev;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Current_Postdocs" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Current_Postdocs" TO hr;

