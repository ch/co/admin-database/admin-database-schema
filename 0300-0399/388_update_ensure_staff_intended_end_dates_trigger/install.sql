CREATE OR REPLACE FUNCTION ensure_staff_intended_end_dates()
  RETURNS trigger AS
$BODY$
declare
	sc_id bigint;
	assistant_staff bigint;
	intended_end_date date;
	sp_pk bigint;
begin

	intended_end_date = NEW.intended_end_date;
        sc_id = NEW.staff_category_id;

	select id from staff_category where category = 'Assistant staff' into assistant_staff;

        if (sc_id <> assistant_staff) and (intended_end_date is null) then
		raise exception 'Intended end date cannot be null for non-assistant staff';
		return null;
	else
		return NEW;
        end if;
	
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION ensure_staff_intended_end_dates()
  OWNER TO dev;
COMMENT ON FUNCTION ensure_staff_intended_end_dates() IS 'To ensure intended end dates are filled in on recs for non-Assistant staff';

