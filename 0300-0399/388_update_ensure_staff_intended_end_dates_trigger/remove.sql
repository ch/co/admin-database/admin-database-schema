CREATE OR REPLACE FUNCTION ensure_staff_intended_end_dates()
  RETURNS trigger AS
$BODY$
declare
	sc_id bigint;
	assistant_staff bigint;
	intended_end_date date;
	sp_pk bigint;
begin

	sp_pk = NEW.staff_post_id;
	intended_end_date= NEW.intended_end_date;

	select id from staff_category where category = 'Assistant staff' into assistant_staff;
	select staff_category_id from staff_post where staff_post.id = sp_pk into sc_id;
	-- raise exception 'sp_pk % sc % as % ied %', sp_pk, sc_id, assistant_staff, intended_end_date;	

        if (sc_id <> assistant_staff) and (intended_end_date is null) then
		raise exception 'Intended end date cannot be null for non-assistant staff';
		return null;
	else
		return NEW;
        end if;
	
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION ensure_staff_intended_end_dates()
  OWNER TO dev;
COMMENT ON FUNCTION ensure_staff_intended_end_dates() IS 'To ensure intended end dates are filled in on recs for non-Assistant staff';

