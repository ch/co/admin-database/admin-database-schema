ALTER TABLE erasmus_type_hid OWNER TO cen1001;
GRANT SELECT ON erasmus_type_hid TO ro_hid;
GRANT ALL ON erasmus_type_hid TO cen1001;
REVOKE ALL ON erasmus_type_hid FROM dev;
