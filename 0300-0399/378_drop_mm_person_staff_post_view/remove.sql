CREATE VIEW mm_person_staff_post AS
    SELECT post_history.person_id, post_history.staff_post_id FROM post_history;

ALTER TABLE public.mm_person_staff_post OWNER TO dev;

REVOKE ALL ON TABLE mm_person_staff_post FROM PUBLIC;
REVOKE ALL ON TABLE mm_person_staff_post FROM dev;
GRANT ALL ON TABLE mm_person_staff_post TO dev;
GRANT SELECT ON TABLE mm_person_staff_post TO mgmt_ro;

