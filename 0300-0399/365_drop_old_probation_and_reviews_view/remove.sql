CREATE OR REPLACE VIEW probation_and_reviews_view AS 
 SELECT post_history.id, post_history.staff_post_id AS ro_staff_post_id, post_history.person_id AS ro_person_id, post_history.supervisor_id, post_history.start_date, staff_category.category AS ro_post_category, person.continuous_employment_start_date, post_history.probation_period, post_history.date_probation_letters_sent, post_history.date_of_first_probation_meeting, post_history.first_probation_meeting_comments, post_history.date_of_second_probation_meeting, post_history.second_probation_meeting_comments, post_history.date_of_third_probation_meeting, post_history.third_probation_meeting_comments, post_history.date_of_fourth_probation_meeting, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, person.usual_reviewer_id AS reviewer_id, person.next_review_due, post_history.timing_of_reviews, person.surname AS _surname, person.first_names AS _first_names
   FROM post_history
   JOIN person ON post_history.person_id = person.id
   LEFT JOIN staff_post ON post_history.staff_post_id = staff_post.id
   LEFT JOIN staff_category ON staff_post.staff_category_id = staff_category.id
  ORDER BY person.surname, person.first_names;

ALTER TABLE probation_and_reviews_view
  OWNER TO cen1001;
GRANT ALL ON TABLE probation_and_reviews_view TO cen1001;
GRANT ALL ON TABLE probation_and_reviews_view TO dev;
GRANT SELECT ON TABLE probation_and_reviews_view TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE probation_and_reviews_view TO hr;

-- Rule: probation_and_reviews_upd ON probation_and_reviews_view

-- DROP RULE probation_and_reviews_upd ON probation_and_reviews_view;

CREATE OR REPLACE RULE probation_and_reviews_upd AS
    ON UPDATE TO probation_and_reviews_view DO INSTEAD ( UPDATE post_history SET supervisor_id = new.supervisor_id, start_date = new.start_date, probation_period = new.probation_period, date_probation_letters_sent = new.date_probation_letters_sent, date_of_first_probation_meeting = new.date_of_first_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments, date_of_second_probation_meeting = new.date_of_second_probation_meeting, second_probation_meeting_comments = new.second_probation_meeting_comments, date_of_third_probation_meeting = new.date_of_third_probation_meeting, third_probation_meeting_comments = new.third_probation_meeting_comments, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, probation_outcome = new.probation_outcome, timing_of_reviews = new.timing_of_reviews
  WHERE post_history.id = old.id;
 UPDATE person SET continuous_employment_start_date = new.continuous_employment_start_date, next_review_due = new.next_review_due, usual_reviewer_id = new.reviewer_id
  WHERE person.id = old.ro_person_id;
);
