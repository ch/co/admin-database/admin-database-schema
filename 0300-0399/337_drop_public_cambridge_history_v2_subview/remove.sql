CREATE OR REPLACE VIEW cambridge_history_v2_subview AS 
 SELECT _all_roles_v9.role_id AS id, _all_roles_v9.person_id, _all_roles_v9.start_date, _all_roles_v9.intended_end_date, _all_roles_v9.end_date, _all_roles_v9.funding_end_date, _all_roles_v9.post_category, _all_roles_v9.job_title, _all_roles_v9.supervisor_id, _all_roles_v9.funding, _all_roles_v9.fees_funding, _all_roles_v9.research_grant_number, _all_roles_v9.paid_by_university, _all_roles_v9.hesa_leaving_code, _all_roles_v9.role_target_viewname AS _targetview
   FROM _all_roles_v9
  ORDER BY _all_roles_v9.start_date DESC;

ALTER TABLE cambridge_history_v2_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE cambridge_history_v2_subview TO cen1001;
GRANT ALL ON TABLE cambridge_history_v2_subview TO dev;
GRANT SELECT ON TABLE cambridge_history_v2_subview TO mgmt_ro;
GRANT SELECT ON TABLE cambridge_history_v2_subview TO hr;
GRANT SELECT ON TABLE cambridge_history_v2_subview TO reception;
GRANT SELECT ON TABLE cambridge_history_v2_subview TO accounts;
GRANT SELECT ON TABLE cambridge_history_v2_subview TO student_management;
