CREATE OR REPLACE VIEW personnel_history_for_hrnj_report AS 
 SELECT person.id, person_hid.person_hid AS ro_person, _latest_role.chem AS ro_chem, person.surname, person.first_names, person.title_id, date_part('year'::text, age('now'::text::date::timestamp with time zone, person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, 
        CASE
            WHEN person.continuous_employment_start_date <= 'now'::text::date THEN btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_length_of_continuous_service, 
        CASE
            WHEN _latest_employment.start_date <= 'now'::text::date THEN btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_length_of_service_in_current_contract, 
        CASE
            WHEN person.continuous_employment_start_date <= 'now'::text::date THEN btrim(age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_final_length_of_continuous_service, 
        CASE
            WHEN _latest_employment.start_date <= 'now'::text::date THEN btrim(age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, person.notes, person.other_information, person.extra_filemaker_data AS ro_extra_filemaker_data, person.filemaker_history AS ro_filemaker_history, 
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN _latest_role_v8 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_history_for_hrnj_report
  OWNER TO dev;
GRANT ALL ON TABLE personnel_history_for_hrnj_report TO postgres;
GRANT ALL ON TABLE personnel_history_for_hrnj_report TO dev;
GRANT SELECT ON TABLE personnel_history_for_hrnj_report TO mgmt_ro;
GRANT SELECT ON TABLE personnel_history_for_hrnj_report TO reception;
GRANT SELECT ON TABLE personnel_history_for_hrnj_report TO accounts;
GRANT SELECT ON TABLE personnel_history_for_hrnj_report TO hr;

CREATE OR REPLACE VIEW personnel_history_view AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age('now'::text::date::timestamp with time zone, person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, 
        CASE
            WHEN person.continuous_employment_start_date <= 'now'::text::date THEN btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_length_of_continuous_service, 
        CASE
            WHEN _latest_employment.start_date <= 'now'::text::date THEN btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_length_of_service_in_current_contract, 
        CASE
            WHEN person.continuous_employment_start_date <= 'now'::text::date THEN btrim(age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_final_length_of_continuous_service, 
        CASE
            WHEN _latest_employment.start_date <= 'now'::text::date THEN btrim(age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text)
            ELSE NULL::text
        END AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, person.notes, person.other_information, person.extra_filemaker_data AS ro_extra_filemaker_data, person.filemaker_history AS ro_filemaker_history, 
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN _latest_role_v8 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_history_view
  OWNER TO cen1001;
GRANT ALL ON TABLE personnel_history_view TO cen1001;
GRANT ALL ON TABLE personnel_history_view TO dev;
GRANT SELECT ON TABLE personnel_history_view TO mgmt_ro;
GRANT SELECT ON TABLE personnel_history_view TO reception;
GRANT SELECT ON TABLE personnel_history_view TO accounts;
GRANT SELECT, UPDATE ON TABLE personnel_history_view TO hr;

CREATE OR REPLACE RULE personnel_history_upd AS
    ON UPDATE TO personnel_history_view DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, notes = new.notes, other_information = new.other_information
  WHERE person.id = old.id;


