-- DROP VIEW visitors_view;

CREATE OR REPLACE VIEW visitors_view AS 
 SELECT visitorship.id, visitorship.person_id, _all_roles_v9.status AS ro_role_status_id, visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.intended_end_date, visitorship.end_date, visitorship.start_date, visitorship.funding, visitorship.notes, visitorship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
   FROM visitorship
   LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = visitorship.id AND _all_roles_v9.role_tablename = 'visitorship'::text
   LEFT JOIN person ON person.id = visitorship.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE visitors_view
  OWNER TO cen1001;
GRANT ALL ON TABLE visitors_view TO cen1001;
GRANT SELECT ON TABLE visitors_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE visitors_view TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE visitors_view TO dev;

-- Rule: visitors_del ON visitors_view

-- DROP RULE visitors_del ON visitors_view;

CREATE OR REPLACE RULE visitors_del AS
    ON DELETE TO visitors_view DO INSTEAD  DELETE FROM visitorship
  WHERE visitorship.id = old.id;

-- Rule: visitors_ins ON visitors_view

-- DROP RULE visitors_ins ON visitors_view;

CREATE OR REPLACE RULE visitors_ins AS
    ON INSERT TO visitors_view DO INSTEAD  INSERT INTO visitorship (home_institution, visitor_type_id, person_id, host_person_id, intended_end_date, end_date, start_date, funding, notes, force_role_status_to_past) 
  VALUES (new.home_institution, new.visitor_type_id, new.person_id, new.host_id, new.intended_end_date, new.end_date, new.start_date, new.funding, new.notes, new.force_role_status_to_past)
  RETURNING visitorship.id, visitorship.person_id, NULL::character varying(10) AS "varchar", visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.intended_end_date, visitorship.end_date, visitorship.start_date, visitorship.funding, visitorship.notes, visitorship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

-- Rule: visitors_upd ON visitors_view

-- DROP RULE visitors_upd ON visitors_view;

CREATE OR REPLACE RULE visitors_upd AS
    ON UPDATE TO visitors_view DO INSTEAD  UPDATE visitorship SET home_institution = new.home_institution, visitor_type_id = new.visitor_type_id, person_id = new.person_id, host_person_id = new.host_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, funding = new.funding, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past
  WHERE visitorship.id = old.id;


