-- DROP VIEW erasmus_students_view;

CREATE OR REPLACE VIEW erasmus_students_view AS 
 SELECT erasmus_socrates_studentship.id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.supervisor_id, person.email_address, erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, person.cambridge_college_id, erasmus_socrates_studentship.university, _all_roles_v9.status AS ro_role_status_id, erasmus_socrates_studentship.notes, erasmus_socrates_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
   FROM erasmus_socrates_studentship
   JOIN person ON person.id = erasmus_socrates_studentship.person_id
   LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = erasmus_socrates_studentship.id AND _all_roles_v9.role_tablename = 'erasmus_socrates_studentship'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE erasmus_students_view
  OWNER TO cen1001;
GRANT ALL ON TABLE erasmus_students_view TO cen1001;
GRANT ALL ON TABLE erasmus_students_view TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE erasmus_students_view TO student_management;

-- Rule: erasmus_students_del ON erasmus_students_view

-- DROP RULE erasmus_students_del ON erasmus_students_view;

CREATE OR REPLACE RULE erasmus_students_del AS
    ON DELETE TO erasmus_students_view DO INSTEAD  DELETE FROM erasmus_socrates_studentship
  WHERE erasmus_socrates_studentship.id = old.id;

-- Rule: erasmus_students_ins ON erasmus_students_view

-- DROP RULE erasmus_students_ins ON erasmus_students_view;

CREATE OR REPLACE RULE erasmus_students_ins AS
    ON INSERT TO erasmus_students_view DO INSTEAD  INSERT INTO erasmus_socrates_studentship (person_id, erasmus_type_id, supervisor_id, intended_end_date, end_date, start_date, university, notes, force_role_status_to_past) 
  VALUES (new.person_id, new.erasmus_type_id, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.university, new.notes, new.force_role_status_to_past)
  RETURNING erasmus_socrates_studentship.id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.supervisor_id, NULL::character varying(48) AS "varchar", erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, NULL::bigint AS int8, erasmus_socrates_studentship.university, NULL::character varying(10) AS "varchar", erasmus_socrates_studentship.notes, erasmus_socrates_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

-- Rule: erasmus_students_upd ON erasmus_students_view

-- DROP RULE erasmus_students_upd ON erasmus_students_view;

CREATE OR REPLACE RULE erasmus_students_upd AS
    ON UPDATE TO erasmus_students_view DO INSTEAD ( UPDATE erasmus_socrates_studentship SET person_id = new.person_id, erasmus_type_id = new.erasmus_type_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, university = new.university, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past
  WHERE erasmus_socrates_studentship.id = old.id;
 UPDATE person SET email_address = new.email_address, cambridge_college_id = new.cambridge_college_id
  WHERE person.id = old.person_id;
);


