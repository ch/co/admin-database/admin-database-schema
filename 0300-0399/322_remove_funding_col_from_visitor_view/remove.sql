DROP VIEW hotwire."10_View/Roles/Visitors";

CREATE OR REPLACE VIEW hotwire."10_View/Roles/Visitors" AS 
 SELECT a.id, a.person_id, a.ro_role_status_id, a.home_institution, a.visitor_type_id, a.host_id, a.start_date, a.intended_end_date, a.end_date, a.funding, a.notes, a.force_role_status_to_past, a._surname, a._first_names
   FROM ( SELECT visitorship.id, visitorship.person_id, _all_roles_v9.status AS ro_role_status_id, visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.funding, visitorship.notes, visitorship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names
           FROM visitorship
      LEFT JOIN _all_roles_v9 ON _all_roles_v9.role_id = visitorship.id AND _all_roles_v9.role_tablename = 'visitorship'::text
   LEFT JOIN person ON person.id = visitorship.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."10_View/Roles/Visitors"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Roles/Visitors" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Roles/Visitors" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Roles/Visitors" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Roles/Visitors" TO dev;

-- Rule: hotwire_view_visitors_del ON hotwire."10_View/Roles/Visitors"

-- DROP RULE hotwire_view_visitors_del ON hotwire."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire_view_visitors_del AS
    ON DELETE TO hotwire."10_View/Roles/Visitors" DO INSTEAD  DELETE FROM visitorship
  WHERE visitorship.id = old.id;

-- Rule: hotwire_view_visitors_ins ON hotwire."10_View/Roles/Visitors"

-- DROP RULE hotwire_view_visitors_ins ON hotwire."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire_view_visitors_ins AS
    ON INSERT TO hotwire."10_View/Roles/Visitors" DO INSTEAD  INSERT INTO visitorship (home_institution, visitor_type_id, person_id, host_person_id, intended_end_date, end_date, start_date, funding, notes, force_role_status_to_past) 
  VALUES (new.home_institution, new.visitor_type_id, new.person_id, new.host_id, new.intended_end_date, new.end_date, new.start_date, new.funding, new.notes, new.force_role_status_to_past)
  RETURNING visitorship.id, visitorship.person_id, NULL::character varying(10) AS "varchar", visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.funding, visitorship.notes, visitorship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

-- Rule: hotwire_view_visitors_upd ON hotwire."10_View/Roles/Visitors"

-- DROP RULE hotwire_view_visitors_upd ON hotwire."10_View/Roles/Visitors";

CREATE OR REPLACE RULE hotwire_view_visitors_upd AS
    ON UPDATE TO hotwire."10_View/Roles/Visitors" DO INSTEAD  UPDATE visitorship SET home_institution = new.home_institution, visitor_type_id = new.visitor_type_id, person_id = new.person_id, host_person_id = new.host_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, funding = new.funding, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past
  WHERE visitorship.id = old.id;

