DROP VIEW hotwire."30_Report/Female_Researchers";
CREATE VIEW hotwire."30_Report/Female_Researchers" AS 
SELECT 
  person.id, 
  person.surname, 
  person.first_names, 
  person.email_address, 
  current_researchers.supervisor_id 
FROM 
  person 
  JOIN (
    SELECT 
      post_history.person_id, 
      post_history.supervisor_id 
    FROM 
      post_history 
      JOIN staff_post ON post_history.staff_post_id = staff_post.id 
      JOIN staff_category ON staff_post.staff_category_id = staff_category.id 
    WHERE 
      (
        post_history.end_date > 'now' :: text :: date 
        OR post_history.end_date IS NULL
      ) 
      AND staff_category.category :: text <> 'Assistant staff' :: text 
      AND staff_category.category :: text <> 'Academic-related staff' :: text 
    UNION 
    SELECT 
      postgraduate_studentship.person_id, 
      postgraduate_studentship.first_supervisor_id AS supervisor_id 
    FROM 
      postgraduate_studentship 
      JOIN _postgrad_end_dates_v5 USING (id) 
    WHERE 
      _postgrad_end_dates_v5.status_id :: text = 'Current' :: text
  ) current_researchers ON person.id = current_researchers.person_id 
WHERE 
  person.gender_id = (
    (
      SELECT 
        gender_hid.gender_id 
      FROM 
        gender_hid 
      WHERE 
        gender_hid.gender_hid :: text = 'Female' :: text
    )
  ) 
  AND person._status :: text <> 'Past' :: text 
  AND person._status :: text <> 'Future' :: text 
ORDER BY 
  person.surname, 
  person.first_names;
ALTER TABLE 
  hotwire."30_Report/Female_Researchers" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Female_Researchers" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO hr;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO dev;

