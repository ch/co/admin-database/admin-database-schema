DROP VIEW hotwire."30_Report/Female_Researchers";
CREATE VIEW hotwire."30_Report/Female_Researchers" AS 
SELECT 
  person.id, 
  person.surname, 
  person.first_names, 
  person.email_address, 
  current_researchers.post_category,
  current_researchers.supervisor_id 
FROM person 
JOIN gender_hid USING (gender_id)
JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
JOIN (
    SELECT 
      _latest_role.person_id, 
      _latest_role.supervisor_id ,
      _latest_role.post_category
    FROM 
      _latest_role_v12 _latest_role 
    WHERE 
      _latest_role.status = 'Current'
      AND ( _latest_role.role_tablename = 'post_history' OR _latest_role.role_tablename = 'postgraduate_studentship' )
      AND _latest_role.post_category <> 'Assistant staff' :: text 
      AND _latest_role.post_category <> 'Academic-related staff' :: text 
  ) current_researchers ON person.id = current_researchers.person_id 
WHERE gender_hid.gender_hid :: text = 'Female' :: text
AND _physical_status_v2.status_id = 'Current'
ORDER BY 
  person.surname, 
  person.first_names;
ALTER TABLE 
  hotwire."30_Report/Female_Researchers" OWNER TO dev;
GRANT ALL ON TABLE hotwire."30_Report/Female_Researchers" TO cen1001;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO hr;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO mgmt_ro;
GRANT 
SELECT 
  ON TABLE hotwire."30_Report/Female_Researchers" TO dev;

