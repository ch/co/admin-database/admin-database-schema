ALTER TABLE hotwire.post_history_hid OWNER TO postgres;
GRANT ALL ON TABLE hotwire.post_history_hid TO postgres;
GRANT ALL ON TABLE hotwire.post_history_hid TO ro_hid;

ALTER TABLE post_history_hid OWNER TO postgres;
GRANT ALL ON TABLE post_history_hid TO postgres;
GRANT SELECT ON TABLE post_history_hid TO ro_hid;
