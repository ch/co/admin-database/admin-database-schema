CREATE 
OR REPLACE VIEW _chem_tmi_mailinglist AS (
  (
    (
      SELECT 
        person.email_address, 
        person.crsid, 
        person_hid.person_hid 
      FROM 
        person 
        JOIN person_hid ON person.id = person_hid.person_id 
        LEFT JOIN _physical_status ON _physical_status.id = person.id 
        LEFT JOIN mm_member_research_interest_group ON mm_member_research_interest_group.member_id = person.id 
        LEFT JOIN research_interest_group ON mm_member_research_interest_group.research_interest_group_id = research_interest_group.id 
      WHERE 
        _physical_status.physical_status :: text = 'Current' :: text 
        AND research_interest_group.id = 1 
        AND NOT (
          person.id IN (
            SELECT 
              mm_mailinglist_exclude_person.exclude_person_id 
            FROM 
              mm_mailinglist_exclude_person 
            WHERE 
              mm_mailinglist_exclude_person.mailinglist_id = (
                (
                  SELECT 
                    mailinglist.id 
                  FROM 
                    mailinglist 
                  WHERE 
                    mailinglist.name :: text = 'chem-tmi' :: text
                )
              )
          )
        ) 
      UNION 
      SELECT 
        person.email_address, 
        person.crsid, 
        person_hid.person_hid 
      FROM 
        mm_mailinglist_include_person 
        LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id 
        JOIN person_hid ON person.id = person_hid.person_id 
        LEFT JOIN _physical_status ON _physical_status.id = person.id 
      WHERE 
        mm_mailinglist_include_person.mailinglist_id = (
          (
            SELECT 
              mailinglist.id 
            FROM 
              mailinglist 
            WHERE 
              mailinglist.name :: text = 'chem-tmi' :: text 
              AND _physical_status.physical_status :: text = 'Current' :: text
          )
        )
    ) 
    UNION 
    SELECT 
      student.email_address, 
      student.crsid, 
      person_hid.person_hid 
    FROM 
      mm_member_research_interest_group 
      JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
      JOIN postgraduate_studentship ON postgraduate_studentship.first_supervisor_id = supervisor.id 
      JOIN person student ON postgraduate_studentship.person_id = student.id 
      JOIN person_hid ON person_hid.person_id = student.id 
      JOIN _postgrad_end_dates_v5 ON _postgrad_end_dates_v5.id = postgraduate_studentship.id 
      JOIN _physical_status ON _physical_status.id = student.id 
    WHERE 
      mm_member_research_interest_group.research_interest_group_id = 1 
      AND _postgrad_end_dates_v5.status_id :: text = 'Current' :: text 
      AND _physical_status.physical_status :: text = 'Current' :: text
  ) 
  UNION 
    (
      SELECT 
        DISTINCT postdoc.email_address, 
        postdoc.crsid, 
        person_hid.person_hid 
      FROM 
        mm_member_research_interest_group 
        JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
        JOIN post_history ON post_history.supervisor_id = supervisor.id 
        JOIN staff_post ON post_history.staff_post_id = staff_post.id 
        JOIN staff_category ON staff_post.staff_category_id = staff_category.id 
        JOIN person postdoc ON post_history.person_id = postdoc.id 
        JOIN person_hid ON person_hid.person_id = postdoc.id 
        JOIN _latest_role_v10 ON _latest_role_v10.person_id = postdoc.id 
        JOIN _physical_status ON _physical_status.id = postdoc.id 
      WHERE 
        mm_member_research_interest_group.research_interest_group_id = 1 
        AND _latest_role_v10.status :: text = 'Current' :: text 
        AND _physical_status.physical_status :: text = 'Current' :: text 
        AND (
          staff_category.category :: text = 'PDRA' :: text 
          OR staff_category.category :: text = 'Senior PDRA' :: text 
          OR staff_category.category :: text = 'Research Fellow' :: text 
          OR staff_category.category :: text = 'Research Assistant' :: text 
          OR staff_category.category :: text = 'Teaching Fellow' :: text
        ) 
      ORDER BY 
        postdoc.email_address, 
        postdoc.crsid, 
        person_hid.person_hid
    )
) 
UNION 
  (
    SELECT 
      DISTINCT visitor.email_address, 
      visitor.crsid, 
      person_hid.person_hid 
    FROM 
      mm_member_research_interest_group 
      JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id 
      JOIN visitorship ON supervisor.id = visitorship.host_person_id 
      JOIN person visitor ON visitorship.person_id = visitor.id 
      JOIN person_hid ON person_hid.person_id = visitor.id 
      JOIN _latest_role_v10 ON _latest_role_v10.person_id = visitor.id 
      JOIN _physical_status_v3 ON _physical_status_v3.person_id = visitor.id 
    WHERE 
      mm_member_research_interest_group.research_interest_group_id = 1 
      AND _latest_role_v10.post_category_id ~~ 'v-%' :: text 
      AND _latest_role_v10.status :: text = 'Current' :: text 
      AND _physical_status_v3.status_id :: text = 'Current' :: text 
    ORDER BY 
      visitor.email_address, 
      visitor.crsid, 
      person_hid.person_hid
  );
ALTER TABLE 
  _chem_tmi_mailinglist OWNER TO cen1001;
GRANT ALL ON TABLE _chem_tmi_mailinglist TO cen1001;
GRANT 
SELECT 
  ON TABLE _chem_tmi_mailinglist TO mailinglists;
GRANT 
SELECT 
  ON TABLE _chem_tmi_mailinglist TO leavers_trigger;

