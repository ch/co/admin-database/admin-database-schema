ALTER TABLE post_history OWNER TO dev;
GRANT ALL ON TABLE post_history TO cen1001;
GRANT ALL ON TABLE post_history TO dev;
GRANT SELECT ON TABLE post_history TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE post_history TO hr;
GRANT SELECT(id) ON post_history TO www_sites;
GRANT SELECT(start_date) ON post_history TO www_sites;
GRANT SELECT(person_id) ON post_history TO www_sites;
GRANT SELECT(job_title) ON post_history TO www_sites;
