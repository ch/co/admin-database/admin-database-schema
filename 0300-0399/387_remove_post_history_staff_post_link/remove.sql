ALTER TABLE post_history ADD COLUMN staff_post_id bigint;
ALTER TABLE post_history ADD CONSTRAINT post_history_fk_staff_post FOREIGN KEY (staff_post_id) REFERENCES staff_post(id) ON DELETE SET NULL;
