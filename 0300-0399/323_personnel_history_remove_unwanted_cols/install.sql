DROP VIEW hotwire."10_View/People/Personnel_History";
DROP VIEW hotwire."10_View/Roles/Cambridge_History_V2";

CREATE OR REPLACE VIEW hotwire."10_View/Roles/_Cambridge_History_V2" AS
SELECT 
    a.id,
    a.person_id,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.funding_end_date,
    a.post_category,
    a.job_title,
    a.supervisor_id,
    a.funding,
    a.fees_funding,
    a.research_grant_number,
    a.paid_by_university,
    a.role_id AS _role_xid,
    a.role_target_viewname AS _target_viewname
FROM ( 
    SELECT 
        _all_roles.role_id AS id,
        _all_roles.person_id,
        _all_roles.start_date,
        _all_roles.intended_end_date,
        _all_roles.end_date,
        _all_roles.funding_end_date,
        _all_roles.post_category,
        _all_roles.job_title,
        _all_roles.supervisor_id,
        _all_roles.funding,
        _all_roles.fees_funding,
        _all_roles.research_grant_number,
        _all_roles.paid_by_university,
        _all_roles.role_id,
        _all_roles.role_target_viewname
    FROM _all_roles_v12 as _all_roles
    ) a
ORDER BY a.start_date DESC;

ALTER TABLE hotwire."10_View/Roles/_Cambridge_History_V2" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO reception;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO accounts;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO student_management;
GRANT SELECT ON TABLE hotwire."10_View/Roles/_Cambridge_History_V2" TO personnel_history_ro;
;

CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_History" AS 
WITH personnel_history AS ( 
    SELECT
        person.id,
        person_hid.person_hid AS ro_person,
        person.surname,
        person.first_names,
        person.title_id,
        date_part('year',age('now', person.date_of_birth)) AS ro_age,
        _latest_role.post_category_id AS ro_post_category_id,
        person.continuous_employment_start_date,
        _latest_employment.start_date AS ro_latest_employment_start_date,
        COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date,
        age(COALESCE(_latest_employment.end_date, current_date), person.continuous_employment_start_date)::text AS ro_length_of_continuous_service,
        age(COALESCE(_latest_employment.end_date, current_date), _latest_employment.start_date)::text AS ro_length_of_service_in_current_contract,
        age(_latest_employment.end_date, person.continuous_employment_start_date) AS ro_final_length_of_continuous_service, 
        age(_latest_employment.end_date, _latest_employment.start_date) AS ro_final_length_of_service_in_last_contract,
        _latest_role.supervisor_id AS ro_supervisor_id,
        person.notes,
        person.other_information,
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _cssclass,
        _to_hwsubviewb('10_View/People/Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Review_History'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, 
        _to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview
    FROM person
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
    LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id
)
SELECT 
    id,
    ro_person,
    surname,
    first_names,
    title_id,
    ro_age,
    ro_post_category_id,
    continuous_employment_start_date,
    ro_latest_employment_start_date,
    ro_employment_end_date,
    ro_length_of_continuous_service,
    ro_length_of_service_in_current_contract,
    ro_final_length_of_continuous_service,
    ro_final_length_of_service_in_last_contract,
    ro_supervisor_id,
    notes,
    other_information,
    _cssclass,
    staff_review_history_subview,
    cambridge_history_subview
FROM personnel_history
ORDER BY surname, first_names;

ALTER TABLE hotwire."10_View/People/Personnel_History" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_History" TO accounts;

-- Rule: hotwire_view_personnel_history_v2_upd ON hotwire."10_View/People/Personnel_History"

-- DROP RULE hotwire_view_personnel_history_v2_upd ON hotwire."10_View/People/Personnel_History";

CREATE OR REPLACE RULE hotwire_view_personnel_history_v2_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_History" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, notes = new.notes, other_information = new.other_information
  WHERE person.id = old.id;

