CREATE OR REPLACE VIEW _chempostdoc_mailinglist AS 
         SELECT person.email_address, person.crsid
           FROM post_history
      LEFT JOIN person ON post_history.person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.person_id = person.id
   LEFT JOIN _all_roles_v12 _all_roles ON person.id = _all_roles.person_id AND _all_roles.role_tablename = 'post_history'::text
  WHERE _all_roles.status::text = 'Current'::text AND (_all_roles.post_category::text = 'PDRA'::text OR _all_roles.post_category::text = 'Senior PDRA'::text OR _all_roles.post_category::text = 'Research Fellow'::text OR _all_roles.post_category::text = 'Principal Research Associate'::text) AND NOT (person.id IN ( SELECT mm_mailinglist_exclude_person.exclude_person_id
    FROM mm_mailinglist_exclude_person
   WHERE mm_mailinglist_exclude_person.mailinglist_id = (( SELECT mailinglist.id
            FROM mailinglist
           WHERE mailinglist.name::text = 'chem-postdoc'::text))))
UNION 
         SELECT person.email_address, person.crsid
           FROM mm_mailinglist_include_person
      LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.person_id = person.id
  WHERE mm_mailinglist_include_person.mailinglist_id = (( SELECT mailinglist.id
         FROM mailinglist
        WHERE mailinglist.name::text = 'chem-postdoc'::text)) AND _physical_status_v2.status_id::text = 'Current'::text;

ALTER TABLE _chempostdoc_mailinglist
  OWNER TO dev;
GRANT ALL ON TABLE _chempostdoc_mailinglist TO dev;
GRANT SELECT ON TABLE _chempostdoc_mailinglist TO mailinglists;
GRANT SELECT ON TABLE _chempostdoc_mailinglist TO www_sites;
GRANT SELECT ON TABLE _chempostdoc_mailinglist TO leavers_trigger;

