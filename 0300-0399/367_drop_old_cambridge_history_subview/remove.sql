CREATE OR REPLACE VIEW cambridge_history_subview AS 
 SELECT history.id, history.person_id, history.start_date, history.end_date, history.funding_end_date, history.post_category::character varying(80) AS post_category, history.supervisor_id, history.funding, history.fees_funding, history.research_grant_number, history.paid_by_university, history.hesa_leaving_code, history._view AS _targetview
   FROM (        (        (        (         SELECT ph.id, ph.person_id, ph.start_date, COALESCE(ph.end_date, ph.intended_end_date) AS end_date, ph.funding_end_date, sc.category AS post_category, ph.supervisor_id, ph.filemaker_funding AS funding, NULL::character varying AS fees_funding, ph.research_grant_number, ph.paid_by_university, ph.hesa_leaving_code, 'post_history_view'::text AS _view
                                           FROM post_history ph
                                      LEFT JOIN staff_post sp ON sp.id = ph.staff_post_id
                                 LEFT JOIN staff_category sc ON sc.id = sp.staff_category_id
                                UNION 
                                         SELECT pg.id, pg.person_id, pg.start_date, 
                                                CASE
                                                    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN COALESCE(pg.phd_date_awarded, pg.date_withdrawn_from_register)
                                                    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(pg.cpgs_or_mphil_date_awarded, pg.date_withdrawn_from_register)
                                                    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN COALESCE(pg.cpgs_or_mphil_date_awarded, pg.date_withdrawn_from_register)
                                                    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN COALESCE(pg.msc_date_awarded, pg.date_withdrawn_from_register)
                                                    ELSE NULL::date
                                                END AS end_date, NULL::date AS funding_end_date, postgraduate_studentship_type.name AS post_category, pg.first_supervisor_id AS supervisor_id, pg.filemaker_funding AS funding, pg.filemaker_fees_funding AS fees_funding, NULL::character varying AS research_grant_number, pg.paid_through_payroll AS paid_by_university, pg.hesa_leaving_code, 'postgrad_students_view'::text AS _view
                                           FROM postgraduate_studentship pg
                                      JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = pg.postgraduate_studentship_type_id)
                        UNION 
                                 SELECT e.id, e.person_id, e.start_date, COALESCE(e.end_date, e.intended_end_date) AS end_date, NULL::date AS funding_end_date, 'Erasmus/Socrates Student'::character varying(80) AS post_category, e.supervisor_id, NULL::character varying(500) AS funding, NULL::character varying(500) AS fees_funding, NULL::character varying(500) AS research_grant_number, NULL::boolean AS paid_by_university, NULL::character varying(2) AS hesa_leaving_code, 'erasmus_students_view'::text AS _view
                                   FROM erasmus_socrates_studentship e)
                UNION 
                         SELECT v.id, v.person_id, v.start_date, COALESCE(v.end_date, v.intended_end_date) AS end_date, NULL::date AS funding_end_date, visitor_type_hid.visitor_type_hid AS post_category, v.host_person_id AS supervisor_id, v.funding, NULL::character varying AS fees_funding, NULL::character varying AS research_grant_number, NULL::boolean AS paid_by_university, NULL::character varying AS hesa_leaving_code, 'visitors_view'::text AS _view
                           FROM visitorship v
                      LEFT JOIN visitor_type_hid USING (visitor_type_id))
        UNION 
                 SELECT p3.id, p3.person_id, p3.start_date, COALESCE(p3.end_date, p3.intended_end_date) AS end_date, NULL::date AS funding_end_date, 'Part III'::character varying AS post_category, p3.supervisor_id, NULL::character varying AS funding, NULL::character varying AS fees_funding, NULL::character varying AS research_grant_number, NULL::boolean AS paid_by_university, NULL::character varying AS hesa_leaving_code, 'part_iii_students_view'::text AS _view
                   FROM part_iii_studentship p3) history
  ORDER BY history.start_date DESC;

ALTER TABLE cambridge_history_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE cambridge_history_subview TO cen1001;
GRANT ALL ON TABLE cambridge_history_subview TO dev;
GRANT SELECT ON TABLE cambridge_history_subview TO mgmt_ro;
GRANT SELECT ON TABLE cambridge_history_subview TO hr;
GRANT SELECT ON TABLE cambridge_history_subview TO reception;
GRANT SELECT ON TABLE cambridge_history_subview TO accounts;
GRANT SELECT ON TABLE cambridge_history_subview TO student_management;

