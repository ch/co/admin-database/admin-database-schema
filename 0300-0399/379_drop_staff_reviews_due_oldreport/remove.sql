CREATE OR REPLACE VIEW staff_reviews_due_oldreport AS 
 SELECT post_history.id, post_history.staff_post_id, post_history.person_id, _latest_employment.post_category_id, person.email_address, post_history.reviewer_id, reviewer.email_address AS reviewer_email, next_review.next_review_due, post_history.timing_of_reviews, post_history.intended_end_date, post_history.funding_end_date, person.surname AS _surname, person.first_names AS _first_names
   FROM _latest_employment
   JOIN person ON person.id = _latest_employment.person_id
   JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
   JOIN post_history ON _latest_employment.role_id = post_history.id
   LEFT JOIN person reviewer ON post_history.reviewer_id = reviewer.id
   LEFT JOIN ( SELECT post_history.person_id, max(post_history.next_review_due) AS next_review_due
   FROM post_history
  GROUP BY post_history.person_id) next_review ON next_review.person_id = person.id
  WHERE _latest_employment.status::text = 'Current'::text AND _physical_status_v2.status_id::text = 'Current'::text AND _latest_employment.post_category::text <> 'Assistant staff'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE staff_reviews_due_oldreport
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_reviews_due_oldreport TO cen1001;
GRANT SELECT ON TABLE staff_reviews_due_oldreport TO hr;
GRANT SELECT ON TABLE staff_reviews_due_oldreport TO dev;
GRANT SELECT ON TABLE staff_reviews_due_oldreport TO mgmt_ro;

