CREATE OR REPLACE VIEW staff_post_view AS 
 SELECT staff_post.id, staff_post.name AS post_name_for_database, staff_post.staff_category_id, staff_post.grade, staff_post.role_code_number AS role_or_job_code_number, staff_post.chris_position_reference AS position_reference_number, staff_post.post_is_suppressed, staff_post.post_tenure_ended, staff_post.current
   FROM staff_post
  ORDER BY staff_post.name;

ALTER TABLE staff_post_view
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_post_view TO cen1001;
GRANT ALL ON TABLE staff_post_view TO dev;
GRANT SELECT ON TABLE staff_post_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE staff_post_view TO hr;

-- Rule: staff_post_del ON staff_post_view

-- DROP RULE staff_post_del ON staff_post_view;

CREATE OR REPLACE RULE staff_post_del AS
    ON DELETE TO staff_post_view DO INSTEAD  DELETE FROM staff_post
  WHERE staff_post.id = old.id;

-- Rule: staff_post_ins ON staff_post_view

-- DROP RULE staff_post_ins ON staff_post_view;

CREATE OR REPLACE RULE staff_post_ins AS
    ON INSERT TO staff_post_view DO INSTEAD  INSERT INTO staff_post (name, staff_category_id, grade, role_code_number, chris_position_reference, post_is_suppressed, post_tenure_ended, current) 
  VALUES (new.post_name_for_database, new.staff_category_id, new.grade, new.role_or_job_code_number, new.position_reference_number, new.post_is_suppressed, new.post_tenure_ended, new.current)
  RETURNING staff_post.id, staff_post.name, staff_post.staff_category_id, staff_post.grade, staff_post.role_code_number, staff_post.chris_position_reference, staff_post.post_is_suppressed, staff_post.post_tenure_ended, staff_post.current;

-- Rule: staff_post_upd ON staff_post_view

-- DROP RULE staff_post_upd ON staff_post_view;

CREATE OR REPLACE RULE staff_post_upd AS
    ON UPDATE TO staff_post_view DO INSTEAD  UPDATE staff_post SET name = new.post_name_for_database, staff_category_id = new.staff_category_id, grade = new.grade, role_code_number = new.role_or_job_code_number, chris_position_reference = new.position_reference_number, post_is_suppressed = new.post_is_suppressed, post_tenure_ended = new.post_tenure_ended, current = new.current
  WHERE staff_post.id = old.id;


