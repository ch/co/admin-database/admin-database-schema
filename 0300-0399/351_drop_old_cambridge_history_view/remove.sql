CREATE OR REPLACE VIEW hotwire."10_View/Cambridge_History_V2" AS 
 SELECT a.id, a.person_id, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.post_category, a.job_title, a.supervisor_id, a.funding, a.fees_funding, a.research_grant_number, a.paid_by_university, a.hesa_leaving_code, a.role_id AS _role_xid
   FROM ( SELECT _all_roles_v9.role_id AS id, _all_roles_v9.person_id, _all_roles_v9.start_date, _all_roles_v9.intended_end_date, _all_roles_v9.end_date, _all_roles_v9.funding_end_date, _all_roles_v9.post_category, _all_roles_v9.job_title, _all_roles_v9.supervisor_id, _all_roles_v9.funding, _all_roles_v9.fees_funding, _all_roles_v9.research_grant_number, _all_roles_v9.paid_by_university, _all_roles_v9.hesa_leaving_code, _all_roles_v9.role_id
           FROM _all_roles_v9) a
  ORDER BY a.start_date DESC;

ALTER TABLE hotwire."10_View/Cambridge_History_V2"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Cambridge_History_V2" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Cambridge_History_V2" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Cambridge_History_V2" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Cambridge_History_V2" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Cambridge_History_V2" TO reception;
GRANT SELECT ON TABLE hotwire."10_View/Cambridge_History_V2" TO accounts;
GRANT SELECT ON TABLE hotwire."10_View/Cambridge_History_V2" TO student_management;
