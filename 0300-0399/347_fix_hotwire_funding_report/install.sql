CREATE OR REPLACE VIEW hotwire."30_Report/Funding" AS 
 SELECT 
  person.id::text || '-' || _all_roles.role_id::text as id,
  person.surname,
  person.first_names,
  person.title_id,
  _all_roles.funding::text as current_funding,
  _all_roles.research_grant_number::text,
  _all_roles.start_date,
  _all_roles.end_date,
  _all_roles.post_category,
  _all_roles.paid_by_university,
  _all_roles.supervisor_id
FROM person 
LEFT JOIN _all_roles_v12 _all_roles ON _all_roles.person_id = person.id
ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."30_Report/Funding" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Funding" TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Funding" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO student_management;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO hr;

