CREATE OR REPLACE VIEW hotwire."30_Report/Funding" AS 
 SELECT searching_report.id,
  searching_report.surname,
  searching_report.first_names,
  searching_report.title_id,
  searching_report.current_funding,
  searching_report.research_grant_number,
  searching_report.start_date,
  searching_report.end_date,
  searching_report.post_category,
  searching_report.paid_by_university,
  searching_report.supervisor_id
FROM searching_report
ORDER BY searching_report.surname, searching_report.first_names;

ALTER TABLE hotwire."30_Report/Funding" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Funding" TO cen1001;
GRANT ALL ON TABLE hotwire."30_Report/Funding" TO dev;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO student_management;
GRANT SELECT ON TABLE hotwire."30_Report/Funding" TO hr;

