CREATE OR REPLACE VIEW hotwire."SelfService/Personal_Data_View" AS 
 SELECT person.id, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix AS name_suffix_eg_frs, person.gender_id, person.maiden_name, mm_person_nationality.nationality_id AS mm_ro_nationality_id, nationality_hid.nationality_hid AS mm_nationality, person.email_address, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, person.college_phone_number, person.cambridge_college_id, person.cambridge_address, person.cambridge_phone_number, person.home_address AS home_address_if_different, person.home_phone_number AS home_phone_number_if_different, person.emergency_contact, person.external_work_phone_numbers, person.ni_number, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, _latest_role_v8.post_category_id AS ro_post_category_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, person.arrival_date AS ro_arrival_date, _latest_role_v8.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v8.intended_end_date, _latest_role_v8.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, user_account.chemnet_token AS ro_chemnet_token, false AS "Create_New_Token", person.do_not_show_on_website AS show_on_website
   FROM person
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid ON room_hid.room_id = mm_person_room.room_id
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
   LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   LEFT JOIN research_group_hid USING (research_group_id)
   LEFT JOIN mm_person_nationality ON person.id = mm_person_nationality.person_id
   LEFT JOIN nationality_hid USING (nationality_id)
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id
   LEFT JOIN user_account ON person.id = user_account.person_id
  WHERE person.ban_from_self_service <> true;

ALTER TABLE hotwire."SelfService/Personal_Data_View"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."SelfService/Personal_Data_View" TO cen1001;
GRANT SELECT, UPDATE ON TABLE hotwire."SelfService/Personal_Data_View" TO selfservice;

CREATE OR REPLACE FUNCTION fn_personal_data_upd(hotwire."SelfService/Personal_Data_View")
  RETURNS bigint AS
$BODY$
       declare          v alias for $1;
 _crsid varchar;
       begin
 
       UPDATE person SET
 
       surname=v.surname, first_names=v.first_names, title_id=v.title_id,
       name_suffix=v.name_suffix_eg_FRS,
       known_as=v.known_as, gender_id=v.gender_id, maiden_name=v.maiden_name,
       email_address=v.email_address, 
       college_phone_number=v.college_phone_number, 
       cambridge_address=v.cambridge_address, 
       cambridge_phone_number=v.cambridge_phone_number, 
       cambridge_college_id = v.cambridge_college_id,
       home_address=v.home_address_if_different,
       home_phone_number=v.home_phone_number_if_different,
       emergency_contact=v.emergency_contact, 
       external_work_phone_numbers=v.external_work_phone_numbers, 
       ni_number=v.ni_number
 
       WHERE person.id = v.id;
       if v."Create_New_Token" then
         update user_account set chemnet_token=random_string_lower(16) where person_id=v.id;
         if not found then
           -- Can only add people with CRSIDs
           select crsid from person where person.id=v.id into _crsid;
           if found then
             insert into user_account (username, person_id, chemnet_token) values (_crsid, v.id, random_string_lower(16));
           end if;
         end if;
       end if;
       PERFORM fn_upd_many_many (v.id, v.mm_research_group, 'person', 'research_group');
       PERFORM fn_upd_many_many (v.id, v.mm_room, 'person', 'room');
       PERFORM fn_upd_many_many (v.id, v.mm_dept_telephone_number, 'person', 'dept_telephone_number');
 
       return v.id;
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_personal_data_upd(hotwire."SelfService/Personal_Data_View")
  OWNER TO cen1001;

CREATE OR REPLACE RULE personal_data_upd AS
    ON UPDATE TO hotwire."SelfService/Personal_Data_View" DO INSTEAD  SELECT fn_personal_data_upd(new.*) AS id;

