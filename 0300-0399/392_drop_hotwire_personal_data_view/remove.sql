CREATE OR REPLACE VIEW hotwire."10_View/Personal_Data" AS 
 SELECT person.id, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix AS name_suffix_eg_frs, person.gender_id, person.maiden_name, ARRAY( SELECT mm_person_nationality.nationality_id
           FROM mm_person_nationality
          WHERE mm_person_nationality.person_id = person.id) AS nationality_id, person.email_address, ARRAY( SELECT mm_person_room.room_id
           FROM mm_person_room
          WHERE person.id = mm_person_room.person_id) AS room_id, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.college_phone_number, person.cambridge_college_id, person.cambridge_address, person.cambridge_phone_number, person.home_address AS home_address_if_different, person.home_phone_number AS home_phone_number_if_different, person.emergency_contact, person.external_work_phone_numbers, person.ni_number, ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id, _latest_role_v8.post_category_id::character varying AS ro_post_category_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, person.arrival_date AS ro_arrival_date, _latest_role_v8.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v8.intended_end_date, _latest_role_v8.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid
   FROM person
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id;

ALTER TABLE hotwire."10_View/Personal_Data"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Personal_Data" TO cen1001;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Personal_Data" TO selfservice;

CREATE OR REPLACE FUNCTION hw_fn_personal_data_upd(hotwire."10_View/Personal_Data")
  RETURNS bigint AS
$BODY$
       declare          v alias for $1;
 
       begin
 
       UPDATE person SET
 
       surname=v.surname, first_names=v.first_names, title_id=v.title_id,
       name_suffix=v.name_suffix_eg_FRS,
       known_as=v.known_as, gender_id=v.gender_id, maiden_name=v.maiden_name,
       email_address=v.email_address, 
       college_phone_number=v.college_phone_number, 
       cambridge_address=v.cambridge_address, 
       cambridge_phone_number=v.cambridge_phone_number, 
       cambridge_college_id = v.cambridge_college_id,
       home_address=v.home_address_if_different,
       home_phone_number=v.home_phone_number_if_different,
       emergency_contact=v.emergency_contact, 
       external_work_phone_numbers=v.external_work_phone_numbers, 
       ni_number=v.ni_number
 
       WHERE person.id = v.id;
 PERFORM fn_mm_array_update (v.research_group_id,
                             'mm_person_research_group'::varchar,
                             'research_group_id'::varchar,
                             'person_id'::varchar,
                             v.id);

 PERFORM fn_mm_array_update (v.room_id,
                             'mm_person_room'::varchar,
                             'room_id'::varchar,
                             'person_id'::varchar,
                             v.id);
                             
 PERFORM fn_mm_array_update (v.dept_telephone_number_id,
                             'mm_person_dept_telephone_number'::varchar,
                             'dept_telephone_number_id'::varchar,
                             'person_id'::varchar,
                             v.id);
 
       return v.id;
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personal_data_upd(hotwire."10_View/Personal_Data")
  OWNER TO cen1001;


-- Rule: hotwire_view_personal_data_upd ON hotwire."10_View/Personal_Data"

-- DROP RULE hotwire_view_personal_data_upd ON hotwire."10_View/Personal_Data";

CREATE OR REPLACE RULE hotwire_view_personal_data_upd AS
    ON UPDATE TO hotwire."10_View/Personal_Data" DO INSTEAD  SELECT hw_fn_personal_data_upd(new.*) AS id;

