CREATE OR REPLACE VIEW personnel_data_entry_view AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.date_of_birth, date_part('year'::text, age('now'::text::date::timestamp with time zone, person.date_of_birth::timestamp with time zone)) AS ro_age, person.gender_id, _latest_role_v8.post_category_id AS ro_post_category_id, _latest_role_v8.chem AS ro_chem, person.crsid, person.email_address, person.hide_email, person.arrival_date, person.leaving_date, _physical_status_v2.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, _latest_role_v8.co_supervisor_id AS ro_co_supervisor_id, _latest_role_v8.mentor_id AS ro_mentor_id, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, person.external_work_phone_numbers, person.pager, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.college_phone_number, nationality_hid.nationality_hid AS mm_nationality, mm_person_nationality.nationality_id AS mm_nationality_id, person.home_address, person.home_phone_number, person.emergency_contact, person.location, person.other_information, person.notes, person.forwarding_address, person.new_employer_address, person.permission_to_give_out_details, person.payroll_personal_reference_number, person.ni_number, person.certificate_of_sponsorship_number, person.continuous_employment_start_date, person.paper_file_details AS paper_file_held_by, person.visa_start_date, person.visa_end_date, person.registration_completed, person.clearance_cert_signed, person.counts_as_academic AS treat_as_academic_staff, 
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid USING (room_id)
   LEFT JOIN mm_person_nationality ON person.id = mm_person_nationality.person_id
   LEFT JOIN nationality_hid USING (nationality_id)
   LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   LEFT JOIN research_group_hid USING (research_group_id)
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v2 USING (id)
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_data_entry_view
  OWNER TO cen1001;
GRANT ALL ON TABLE personnel_data_entry_view TO cen1001;
GRANT ALL ON TABLE personnel_data_entry_view TO dev;
GRANT SELECT ON TABLE personnel_data_entry_view TO mgmt_ro;
GRANT SELECT ON TABLE personnel_data_entry_view TO accounts;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE personnel_data_entry_view TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE personnel_data_entry_view TO reception;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE personnel_data_entry_view TO student_management;

CREATE OR REPLACE FUNCTION fn_personnel_data_entry_upd(personnel_data_entry_view)
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;

        begin
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname, 
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as,
                                name_suffix = v.name_suffix,
                                maiden_name = v.maiden_name, 
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                external_work_phone_numbers = v.external_work_phone_numbers,
                                pager = v.pager,
                                cambridge_address = v.cambridge_address,
                                cambridge_phone_number = v.cambridge_phone_number, 
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number,
                                college_phone_number = v.college_phone_number, 
                                home_address = v.home_address, 
                                home_phone_number = v.home_phone_number, 
                                emergency_contact = v.emergency_contact, 
                                location = v.location,
                                other_information = v.other_information,
                                notes = v.notes,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                permission_to_give_out_details = v.permission_to_give_out_details,
                                payroll_personal_reference_number = v.payroll_personal_reference_number, 
                                ni_number = v.ni_number,
                                certificate_of_sponsorship_number = v.certificate_of_sponsorship_number,
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_held_by, 
                                visa_start_date = v.visa_start_date, 
                                visa_end_date = v.visa_end_date,
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic = v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        v_person_id=nextval('person_id_seq');
                        insert into person (
                                        id,
                                        surname, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        maiden_name, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        arrival_date,
                                        leaving_date,
                                        external_work_phone_numbers,
                                        pager,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        college_phone_number, 
                                        home_address, 
                                        home_phone_number, 
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        permission_to_give_out_details,
                                        payroll_personal_reference_number, 
                                        ni_number,
                                        certificate_of_sponsorship_number,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        visa_start_date, 
                                        visa_end_date,
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic

                                ) values (
                                        v_person_id,
                                        v.surname, 
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as,
                                        v.name_suffix,
                                        v.maiden_name, 
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.external_work_phone_numbers,
                                        v.pager,
                                        v.cambridge_address,
                                        v.cambridge_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number,
                                        v.college_phone_number, 
                                        v.home_address, 
                                        v.home_phone_number, 
                                        v.emergency_contact, 
                                        v.location,
                                        v.other_information,
                                        v.notes,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v.permission_to_give_out_details,
                                        v.payroll_personal_reference_number, 
                                        v.ni_number,
                                        v.certificate_of_sponsorship_number,
                                        v.continuous_employment_start_date,
                                        v.paper_file_held_by, 
                                        v.visa_start_date, 
                                        v.visa_end_date,
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff


                                ) ;
                end if;

                -- many-many updates here
                perform fn_upd_many_many (v_person_id, v.mm_research_group, 'person', 'research_group');
                perform fn_upd_many_many (v_person_id, v.mm_dept_telephone_number, 'person','dept_telephone_number');                                        perform fn_upd_many_many (v_person_id, v.mm_room, 'person', 'room');                                               
                perform fn_upd_many_many (v_person_id, v.mm_nationality, 'person','nationality');
                perform fn_upd_many_many (v_person_id, v.mm_room, 'person', 'room');                                               


                return v_person_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_personnel_data_entry_upd(personnel_data_entry_view)
  OWNER TO cen1001;


-- Rule: personnel_data_entry_del ON personnel_data_entry_view

-- DROP RULE personnel_data_entry_del ON personnel_data_entry_view;

CREATE OR REPLACE RULE personnel_data_entry_del AS
    ON DELETE TO personnel_data_entry_view DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

-- Rule: personnel_data_entry_ins ON personnel_data_entry_view

-- DROP RULE personnel_data_entry_ins ON personnel_data_entry_view;

CREATE OR REPLACE RULE personnel_data_entry_ins AS
    ON INSERT TO personnel_data_entry_view DO INSTEAD  SELECT fn_personnel_data_entry_upd(new.*) AS id;

-- Rule: personnel_data_entry_upd ON personnel_data_entry_view

-- DROP RULE personnel_data_entry_upd ON personnel_data_entry_view;

CREATE OR REPLACE RULE personnel_data_entry_upd AS
    ON UPDATE TO personnel_data_entry_view DO INSTEAD  SELECT fn_personnel_data_entry_upd(new.*) AS id;


CREATE OR REPLACE VIEW personnel_data_entry2_view AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.maiden_name, person.date_of_birth, date_part('year'::text, age('now'::text::date::timestamp with time zone, person.date_of_birth::timestamp with time zone)) AS ro_age, person.gender_id, _latest_role_v8.post_category_id AS ro_post_category_id, _latest_role_v8.chem AS ro_chem, person.crsid, person.email_address, person.hide_email, person.arrival_date, person.leaving_date, _physical_status_v2.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, _latest_role_v8.supervisor_id AS ro_supervisor_id, _latest_role_v8.co_supervisor_id AS ro_co_supervisor_id, _latest_role_v8.mentor_id AS ro_mentor_id, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, person.external_work_phone_numbers, person.pager, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.college_phone_number, nationality_hid.nationality_hid AS mm_nationality, mm_person_nationality.nationality_id AS mm_nationality_id, person.home_address, person.home_phone_number, person.emergency_contact, person.location, person.other_information, person.notes, person.forwarding_address, person.new_employer_address, person.permission_to_give_out_details, person.payroll_personal_reference_number, person.ni_number, person.certificate_of_sponsorship_number, person.continuous_employment_start_date, person.paper_file_details AS paper_file_held_by, person.visa_start_date, person.visa_end_date, person.registration_completed, person.clearance_cert_signed, 
        CASE
            WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid USING (room_id)
   LEFT JOIN mm_person_nationality ON person.id = mm_person_nationality.person_id
   LEFT JOIN nationality_hid USING (nationality_id)
   LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   LEFT JOIN research_group_hid USING (research_group_id)
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v2 USING (id)
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_data_entry2_view
  OWNER TO cen1001;

