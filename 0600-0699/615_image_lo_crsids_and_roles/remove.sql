CREATE OR REPLACE VIEW _crsid_and_role AS 
 SELECT person.crsid, person.image_oid, _latest_role_v8.post_category, (COALESCE(person.known_as, person.first_names, ''::character varying)::text || ' '::text) || person.surname::text AS name
   FROM person
   LEFT JOIN _latest_role_v8 ON person.id = _latest_role_v8.person_id
  WHERE person.crsid IS NOT NULL AND _latest_role_v8.post_category IS NOT NULL;

ALTER TABLE _crsid_and_role
  OWNER TO alt36;
GRANT ALL ON TABLE _crsid_and_role TO alt36;
GRANT SELECT ON TABLE _crsid_and_role TO leavers_trigger;
GRANT SELECT ON TABLE _crsid_and_role TO ad_accounts;
GRANT ALL ON TABLE _crsid_and_role TO dev;

DROP VIEW apps.crsid_and_role;
