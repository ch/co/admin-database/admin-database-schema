CREATE OR REPLACE VIEW hotwire3.person_hid AS 
 SELECT person.id AS person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text);

ALTER TABLE hotwire3.person_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.person_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.person_hid TO groupitreps;
GRANT SELECT ON TABLE hotwire3.person_hid TO headsofgroup;
GRANT SELECT ON TABLE hotwire3.person_hid TO ad_accounts;

