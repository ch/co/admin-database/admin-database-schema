DROP VIEW hotwire3."10_View/Network/Switches/Switch_Config_Fragments";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Config_Fragments" AS 
 SELECT switch_config_fragment.id, switch_config_fragment.name, switch_config_fragment.config, switch_config_fragment.switch_config_goal_id, ARRAY( SELECT mm_switch_config_fragment_switch_model.switch_model_id
           FROM mm_switch_config_fragment_switch_model
          WHERE mm_switch_config_fragment_switch_model.switch_config_fragment_id = switch_config_fragment.id) AS switch_model_id
   FROM switch_config_fragment
  ORDER BY switch_config_fragment.name;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Config_Fragments"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Config_Fragments" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Config_Fragments" TO cos;

CREATE OR REPLACE RULE del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Config_Fragments" DO INSTEAD  DELETE FROM switch_config_fragment
  WHERE switch_config_fragment.id = old.id;

-- Rule: upd ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments"

-- DROP RULE upd ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments";

CREATE OR REPLACE RULE upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Config_Fragments" DO INSTEAD ( UPDATE switch_config_fragment SET name = new.name, config = new.config, switch_config_goal_id = new.switch_config_goal_id
  WHERE switch_config_fragment.id = old.id;
 SELECT fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id) AS fn_mm_array_update;
 UPDATE switch_config_fragment SET id = switch_config_fragment.id
  WHERE false
  RETURNING new.id, new.name, new.config, new.switch_config_goal_id, new.switch_model_id;
);

CREATE OR REPLACE FUNCTION hotwire3.switch_config_fragments_ins()
  RETURNS trigger AS
$BODY$
  
  BEGIN

  NEW.id := nextval('switch_config_fragments_id_seq');

  INSERT INTO switch_config_fragment 
        ( id, name, config, switch_config_goal_id ) 
        VALUES 
        ( NEW.id, NEW.name, NEW.config, NEW.switch_config_goal_id );

  PERFORM fn_mm_array_update(new.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, new.id) ;

  RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.switch_config_fragments_ins()
  OWNER TO dev;



-- Trigger: switch_config_fragments_ins on hotwire3."10_View/Network/Switches/Switch_Config_Fragments"

-- DROP TRIGGER switch_config_fragments_ins ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments";

CREATE TRIGGER switch_config_fragments_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Network/Switches/Switch_Config_Fragments"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.switch_config_fragments_ins();


