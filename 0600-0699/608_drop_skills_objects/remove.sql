CREATE SEQUENCE skills_course_rxmlid
    START WITH -1
    INCREMENT BY -1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_course_rxmlid OWNER TO postgres;
GRANT ALL ON SEQUENCE skills_course_rxmlid TO skills;


CREATE SEQUENCE skills_session_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_session_id OWNER TO postgres;
GRANT ALL ON SEQUENCE skills_session_id TO skills;


CREATE SEQUENCE skills_courses_year_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_courses_year_seq OWNER TO postgres;
GRANT ALL ON SEQUENCE skills_courses_year_seq TO skills;


SELECT pg_catalog.setval('skills_courses_year_seq', 4, true);
SELECT pg_catalog.setval('skills_course_rxmlid', -26, true);
SELECT pg_catalog.setval('skills_session_id', 59, true);



CREATE TABLE skills_courses_years
(
  id integer NOT NULL DEFAULT nextval('skills_courses_year_seq'::regclass),
  year character varying(25),
  CONSTRAINT pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE skills_courses_years
  OWNER TO postgres;
GRANT ALL ON TABLE skills_courses_years TO postgres;
GRANT ALL ON TABLE skills_courses_years TO old_cos;
GRANT ALL ON TABLE skills_courses_years TO skills;

CREATE UNIQUE INDEX fki_skills_courses_year
  ON skills_courses_years
  USING btree
  (id);


COPY skills_courses_years (id, year) FROM stdin;
1	1st Year PhD Student
2	2nd Year PhD student
5	Masters student
4	3rd Year+ PhD student
3	Research staff
\.

