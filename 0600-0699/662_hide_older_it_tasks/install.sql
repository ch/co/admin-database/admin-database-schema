CREATE 
OR REPLACE VIEW hotwire3."10_View/10_IT_Tasks/Tasks" AS 
SELECT 
  a.id, 
  a.task_number, 
  a.name, 
  a.task_description :: text AS task_description, 
  a.task_notes :: text AS task_notes, 
  a."Primary_strategic_goal_id", 
  a.it_strategic_goal_id, 
  a.it_project_id, 
  a.person_id, 
  a.all_cos, 
  a."IT_task_leader_id", 
  a.computer_officer_id, 
  a.created, 
  a.completed, 
  a.needed_by, 
  a.time_estimate_id, 
  a."IT_status_id", 
  a.priority, 
  a."order", 
  a.progress, 
  a.predecessor_task_id, 
  a._cssclass, 
  a.changes, 
  a.anticipated_completion_date 
FROM 
  (
    SELECT 
      it_task.id, 
      it_task.task_number, 
      it_task.name, 
      it_task.task_description, 
      it_task.task_notes, 
      it_task.anticipated_completion_date, 
      it_task."Primary_strategic_goal_id", 
      ARRAY(
        SELECT 
          mm_it_task_strategic_goal.it_strategic_goal_id 
        FROM 
          mm_it_task_strategic_goal 
        WHERE 
          mm_it_task_strategic_goal.it_task_id = it_task.id
      ) AS it_strategic_goal_id, 
      it_task.it_project_id, 
      ARRAY(
        SELECT 
          mm_it_task_interested_parties.person_id 
        FROM 
          mm_it_task_interested_parties 
        WHERE 
          mm_it_task_interested_parties.it_task_id = it_task.id
      ) AS person_id, 
      (
        (
          SELECT 
            computer_officer_hid.computer_officer_hid 
          FROM 
            hotwire3.computer_officer_hid 
          WHERE 
            computer_officer_hid.computer_officer_id = it_task."IT_task_leader_id" 
          LIMIT 
            1
        )
      ) || COALESCE(
        '; ' :: text || array_to_string(
          ARRAY(
            SELECT 
              computer_officer_hid.computer_officer_hid 
            FROM 
              mm_it_task_other_cos 
              JOIN hotwire3.computer_officer_hid USING (computer_officer_id) 
            WHERE 
              mm_it_task_other_cos.it_task_id = it_task.id
          ), 
          '; ' :: text
        ), 
        '' :: text
      ) AS all_cos, 
      it_task."IT_task_leader_id", 
      ARRAY(
        SELECT 
          mm_it_task_other_cos.computer_officer_id 
        FROM 
          mm_it_task_other_cos 
        WHERE 
          mm_it_task_other_cos.it_task_id = it_task.id
      ) AS computer_officer_id, 
      it_task.created, 
      it_task.completed, 
      it_task.needed_by, 
      it_task.time_estimate_id, 
      it_task."IT_status_id", 
      it_task.priority, 
      it_task."order", 
      trunc(
        (10 :: numeric * it_task.progress):: integer :: numeric * 10.0, 
        0
      ) AS progress, 
      ARRAY(
        SELECT 
          mm_it_task_depends_on.it_predecessor_task_id 
        FROM 
          mm_it_task_depends_on 
        WHERE 
          mm_it_task_depends_on.it_successor_task_id = it_task.id
      ) AS predecessor_task_id, 
      (
        (
          'complete' :: text || trunc(
            10 :: numeric * COALESCE(it_task.progress, 0 :: numeric), 
            0
          )
        ) || ' status' :: text
      ) || COALESCE(
        it_task."IT_status_id", 0 :: bigint
      ) AS _cssclass, 
      hotwire3.to_hwsubviewb(
        '10_View/10_IT_Tasks/_audit_ro' :: character varying, 
        'it_task_id' :: character varying, 
        NULL :: character varying, NULL :: character varying, 
        NULL :: character varying
      ) AS changes 
    FROM 
      it_task,
      (
        SELECT CASE 
          WHEN current_date >= (date_part('year',current_date)|| '-10-01')::date THEN ((date_part('year',current_date) - 1)|| '-10-01')::date
          ELSE ((date_part('year',current_date) - 2)|| '-10-01')::date
        END AS cutoff_date
      ) cutoff_date
    WHERE 
      it_task.deleted IS NULL 
    AND
      (
          -- Tasks that aren't cancelled and either are still incomplete, or were completed recently
          ( it_task."IT_status_id" IS DISTINCT FROM 5 AND ( it_task.completed IS NULL OR it_task.completed >= cutoff_date.cutoff_date ) )
          OR
          -- Recently cancelled tasks
          ( it_task."IT_status_id" = 5 AND ( it_task.created IS NULL OR it_task.created >= cutoff_date.cutoff_date ))
      )
    ORDER BY 
      COALESCE(it_task.priority, 5), 
      it_task.weight
  ) a;

ALTER TABLE hotwire3."10_View/10_IT_Tasks/Tasks" OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO cos;
GRANT 
SELECT 
  ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO groupitreps;
GRANT 
SELECT 
  ON TABLE hotwire3."10_View/10_IT_Tasks/Tasks" TO it_committee;

