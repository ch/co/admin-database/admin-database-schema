
-- selfservice/001_add_selfservice_schema/install.sql

CREATE SCHEMA selfservice;
ALTER SCHEMA selfservice OWNER to dev;
GRANT USAGE on SCHEMA selfservice to PUBLIC;
GRANT USAGE on SCHEMA selfservice to _pgbackup;

-- selfservice/002_add_selfservice_preferences_tables/install.sql

CREATE TABLE selfservice.hw_preference_type_hid (
    hw_preference_type_id bigserial NOT NULL,
    hw_preference_type_hid character varying
);
ALTER TABLE selfservice.hw_preference_type_hid OWNER TO dev;

INSERT INTO selfservice.hw_preference_type_hid VALUES (1, 'boolean');
INSERT INTO selfservice.hw_preference_type_hid VALUES (2, 'integer');
INSERT INTO selfservice.hw_preference_type_hid VALUES (3, 'string');

ALTER TABLE ONLY selfservice.hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);

GRANT ALL ON TABLE selfservice.hw_preference_type_hid TO postgres;
GRANT ALL ON TABLE selfservice.hw_preference_type_hid TO dev;

SELECT setval('selfservice.hw_preference_type_hid_hw_preference_type_id_seq',3);

CREATE TABLE selfservice."hw_Preferences"
( 
  id serial NOT NULL ,
  hw_preference_name character varying,
  hw_preference_type_id integer,
  hw_preference_const varchar,
  CONSTRAINT "Preferences_pkey" PRIMARY KEY (id),
  CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id)
      REFERENCES selfservice.hw_preference_type_hid (hw_preference_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

)
WITH (
  OIDS=FALSE
);
ALTER TABLE selfservice."hw_Preferences" OWNER TO dev;
GRANT ALL ON TABLE selfservice."hw_Preferences" TO postgres;
GRANT ALL ON TABLE selfservice."hw_Preferences" TO dev;

INSERT INTO selfservice."hw_Preferences" VALUES (1, 'Floating table headers', 1, 'FLOATHEAD');
INSERT INTO selfservice."hw_Preferences" VALUES (2, 'Jump on a single search result', 1, 'JUMPSEARCH');
INSERT INTO selfservice."hw_Preferences" VALUES (3, 'Jump on a single view result', 1, 'JUMPVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (4, 'CSS path', 3, 'CSSPATH');
INSERT INTO selfservice."hw_Preferences" VALUES (5, 'Warn if record data changes', 1, 'CHANGE');
INSERT INTO selfservice."hw_Preferences" VALUES (6, 'AJAX min dd size', 2, 'AJAXMIN');
INSERT INTO selfservice."hw_Preferences" VALUES (7, 'JS path', 3, 'JSPATH');
INSERT INTO selfservice."hw_Preferences" VALUES (9, 'Default view', 3, 'DEFVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (10, 'Warn if view data changes', 1, 'CHECKSUM');
INSERT INTO selfservice."hw_Preferences" VALUES (11, 'Alternative views enabled', 1, 'ALTVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (12, 'Related Records enabled', 1, 'RELREC');
INSERT INTO selfservice."hw_Preferences" VALUES (13, 'Resizable Table columns', 1, 'RESIZECOLS');
INSERT INTO selfservice."hw_Preferences" VALUES (14, 'Sort NULLS in numerical order', 1, 'NUMNULL');
INSERT INTO selfservice."hw_Preferences" VALUES (15, 'Hideable Table columns', 1, 'HIDECOLS');
INSERT INTO selfservice."hw_Preferences" VALUES (16, 'Reorderable Table columns', 1, 'COLREORDER');
INSERT INTO selfservice."hw_Preferences" VALUES (17, 'Show old search', 1, 'OLDSEARCH');
INSERT INTO selfservice."hw_Preferences" VALUES (18, 'Include "... show next" buttons', 1, 'SHOWNEXT');
INSERT INTO selfservice."hw_Preferences" VALUES (19, 'Warn on missing rules', 1, 'WARNRULES');
INSERT INTO selfservice."hw_Preferences" VALUES (20, 'Cache menu structure', 1, 'CACHEMENU');
INSERT INTO selfservice."hw_Preferences" VALUES (21, 'Remove accents from search', 1, 'UNACCENT');
INSERT INTO selfservice."hw_Preferences" VALUES (22, 'Date format', 3, 'DATEFORMAT');
INSERT INTO selfservice."hw_Preferences" VALUES (23, 'Show web interface hints', 1, 'SHOWHINTS');
INSERT INTO selfservice."hw_Preferences" VALUES (24, 'Show post-update options', 1, 'POSTUPDATEOPT');
INSERT INTO selfservice."hw_Preferences" VALUES (25, 'Show post-insert options', 1, 'POSTINSERTOPT');
INSERT INTO selfservice."hw_Preferences" VALUES (26, 'Offer xdebug option to developers', 1, 'XDEBUG');
INSERT INTO selfservice."hw_Preferences" VALUES (27, 'DB Development group', 3, 'DBDEV');
INSERT INTO selfservice."hw_Preferences" VALUES (28, 'Database internal date format', 3, 'DBDATEFORMAT');
INSERT INTO selfservice."hw_Preferences" VALUES (29, 'Use Ajax many-many selects', 1, 'MM_UI_AJAX');
INSERT INTO selfservice."hw_Preferences" VALUES (30, 'Use many-many tag interface', 1, 'MM_UI_TAG');
INSERT INTO selfservice."hw_Preferences" VALUES (31, 'No JS on many-many selects', 1, 'MM_UI_NOJS');
INSERT INTO selfservice."hw_Preferences" VALUES (32, 'First day of the week', 2, 'JS_UI_DAYSTART');
INSERT INTO selfservice."hw_Preferences" VALUES (33, 'Open record in own tab', 1, 'OWNTAB');

SELECT setval('selfservice."hw_Preferences_id_seq"',(select max(id) from selfservice."hw_Preferences"));

CREATE TABLE selfservice."hw_User Preferences" (
    id serial NOT NULL,
    preference_id integer not null,
    preference_value character varying not null,
    user_id bigint,
    CONSTRAINT "User_Preferences_pkey" PRIMARY KEY (id),
    CONSTRAINT "User_Preferences_fk_preference_id" FOREIGN KEY (preference_id) REFERENCES selfservice."hw_Preferences"(id) ON DELETE CASCADE
);
ALTER TABLE selfservice."hw_User Preferences" OWNER TO dev;
GRANT ALL ON TABLE selfservice."hw_User Preferences" TO postgres;
GRANT ALL ON TABLE selfservice."hw_User Preferences" TO dev;

-- populate defaults
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 2, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 3, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 4, 'user-css');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 5, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 7, 'user-js');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 9, '40_Selfservice/Database_Selfservice');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 11, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 12, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 18, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 28, 'd/m/Y');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 29, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 30, 'TRUE');

SELECT setval('selfservice."hw_User Preferences_id_seq"',(select max(id) from selfservice."hw_User Preferences"));

-- selfservice/003_add_selfservice_view_data/install.sql

CREATE TABLE selfservice._primary_table (
	view_name varchar primary key,
	primary_table varchar);

ALTER TABLE selfservice._primary_table OWNER TO dev;
GRANT SELECT ON selfservice._primary_table TO PUBLIC;


CREATE VIEW selfservice._view_data AS 
 SELECT 
	pgns.nspname AS schema, 
	pg_class.relname AS view, 
	pg_class.relacl AS perms, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'insert'::text) AS insert, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'update'::text) AS update, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'delete'::text) AS delete, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'select'::text) AS "select", 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '4'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS delete_rule, 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS insert_rule, 
	( 
		SELECT r.ev_action::text ~~ '%returningList:%'::text
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	) AS insert_returning, 
	(( SELECT count(*) AS count
           FROM pg_rewrite r
      JOIN pg_class c ON c.oid = r.ev_class
   LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE r.ev_type = '2'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace)) > 0 AS update_rule, pg_views.definition, 
	_primary_table.primary_table, 
        CASE
            WHEN regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ '%ORDER BY%'::character varying::text THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text)
            ELSE NULL::text
        END AS order_by
   FROM pg_class
   JOIN pg_namespace pgns ON pg_class.relnamespace = pgns.oid
   JOIN pg_views ON pg_class.relname = pg_views.viewname AND pgns.nspname = pg_views.schemaname
   LEFT JOIN selfservice._primary_table on pg_class.relname = _primary_table.view_name
  WHERE pg_class.relname ~~ '%/%'::text AND (pgns.nspname = ANY (ARRAY['selfservice'::name, 'public'::name]));


ALTER TABLE selfservice._view_data OWNER TO dev;
GRANT ALL ON TABLE selfservice._view_data TO dev;
GRANT SELECT ON TABLE selfservice._view_data TO public;


-- selfservice/004_add_selfservice_role_data/install.sql

-- What permissions/roles do we have?
CREATE OR REPLACE VIEW selfservice._role_data AS 
 SELECT member.rolname AS member, role.rolname AS role
   FROM pg_roles role
   LEFT JOIN pg_auth_members ON role.oid = pg_auth_members.roleid
   RIGHT JOIN pg_roles member ON member.oid = pg_auth_members.member;

ALTER TABLE selfservice._role_data OWNER TO dev;
GRANT ALL ON TABLE selfservice._role_data TO dev;
GRANT SELECT ON TABLE selfservice._role_data TO public;

-- selfservice/005_add_selfservice_column_data/install.sql

CREATE VIEW selfservice._column_data AS 
 SELECT pg_attribute.attname, pg_type.typname, pg_attribute.atttypmod, pg_class.relname, pg_attribute.attnotnull, pg_attribute.atthasdef, pg_type.typelem > 0::oid AS "array", ( SELECT child.typname
           FROM pg_type child
          WHERE child.oid = pg_type.typelem) AS elementtype
   FROM pg_attribute
   JOIN pg_type ON pg_attribute.atttypid = pg_type.oid
   JOIN pg_class ON pg_attribute.attrelid = pg_class.oid
   JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
  WHERE (pg_namespace.nspname = 'public'::name OR pg_namespace.nspname = 'selfservice'::name) AND pg_attribute.attnum > 0;

ALTER TABLE selfservice._column_data OWNER TO dev;
GRANT ALL ON TABLE selfservice._column_data TO dev;
GRANT SELECT ON TABLE selfservice._column_data TO public;

-- selfservice/006_add_selfservice_preferences_views/install.sql

CREATE VIEW selfservice._preferences AS 
 SELECT "Preferences".hw_preference_const AS preference_name, a.preference_value, hw_preference_type_hid.hw_preference_type_hid AS preference_type
   FROM (        (         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM selfservice."hw_User Preferences" "User Preferences"
                          WHERE "User Preferences".user_id IS NULL
                UNION ALL 
                         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM pg_auth_members m
                      JOIN pg_roles b ON m.roleid = b.oid
                 JOIN pg_roles r ON m.member = r.oid
            JOIN selfservice."hw_User Preferences" "User Preferences" ON b.oid = "User Preferences".user_id::oid
           WHERE r.rolname = "current_user"())
        UNION ALL 
                 SELECT "User Preferences".preference_id, "User Preferences".preference_value
                   FROM pg_roles
              JOIN selfservice."hw_User Preferences" "User Preferences" ON pg_roles.oid = "User Preferences".user_id::oid
             WHERE pg_roles.rolname = "current_user"()) a
   JOIN selfservice."hw_Preferences" "Preferences" ON a.preference_id = "Preferences".id
   JOIN selfservice.hw_preference_type_hid USING (hw_preference_type_id);

ALTER TABLE selfservice._preferences OWNER TO dev;
GRANT ALL ON TABLE selfservice._preferences TO dev;
GRANT SELECT ON TABLE selfservice._preferences TO public;


-- selfservice/007_add_selfservice_main_view/install.sql

CREATE VIEW selfservice."40_Selfservice/Database_Selfservice" AS
SELECT DISTINCT	person.id,
        person.image_lo AS image_oid,
	title_hid.title_hid::varchar AS ro_title,
	person.first_names::varchar AS ro_first_names,
	person.surname::varchar AS ro_surname,
	person.known_as,
	person.name_suffix AS name_suffix_eg_frs,
	gender_hid.gender_hid::varchar AS ro_gender,
	person.previous_surname::varchar AS ro_previous_name,
        nationality_string.ro_nationality,
	person.email_address,
	person.hide_email AS hide_email_from_dept_websites,
	ARRAY ( 
		SELECT mm.room_id
		FROM mm_person_room mm
		WHERE mm.person_id = person.id
	) AS room_id,
	ARRAY ( 
		SELECT mm.dept_telephone_number_id
		FROM mm_person_dept_telephone_number mm
		WHERE mm.person_id = person.id
	) AS dept_telephone_number_id,
	cambridge_college_hid.cambridge_college_hid::varchar AS ro_cambridge_college,
	person.cambridge_address::text AS home_address,
	person.cambridge_phone_number AS home_phone_number,
	person.emergency_contact::text,
	research_groups_string.ro_research_group,
	post_category_hid.post_category_hid::varchar AS ro_post_category,
	supervisor_hid.supervisor_hid::varchar AS ro_supervisor,
	person.arrival_date AS ro_arrival_date,
	_latest_role_v12.funding_end_date AS ro_funding_end_date,
	COALESCE(_latest_role_v12.intended_end_date,_latest_role_v12.end_date) AS ro_expected_leaving_date,
	person.crsid AS ro_crsid,
	person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain,
	person.do_not_show_on_website AS hide_from_website,
	user_account.chemnet_token AS ro_chemnet_token,
	false::boolean AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username = person.crsid
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
   LEFT JOIN (
       SELECT person.id AS person_id, string_agg(nationality.nationality,', ')::varchar AS ro_nationality
       FROM person
       LEFT JOIN mm_person_nationality ON mm_person_nationality.person_id = person.id
       LEFT JOIN nationality ON mm_person_nationality.nationality_id = nationality.id
       GROUP BY person.id
   ) nationality_string ON nationality_string.person_id = person.id
   LEFT JOIN (
       SELECT person.id AS person_id, string_agg(research_group.name,', ')::varchar AS ro_research_group
       FROM person
       LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
       LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
       GROUP BY person.id
   ) research_groups_string ON research_groups_string.person_id = person.id
  WHERE person.crsid = current_user AND person.ban_from_self_service <> true
GROUP BY person.id, title_hid.title_hid, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, intended_end_date, end_date, chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group
;

ALTER VIEW selfservice."40_Selfservice/Database_Selfservice" OWNER TO dev;
GRANT SELECT,UPDATE ON selfservice."40_Selfservice/Database_Selfservice" TO public;

CREATE OR REPLACE FUNCTION selfservice.selfservice_upd() RETURNS TRIGGER AS
$body$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            image_oid = new.image_oid::bigint,
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address::varchar(500),
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact::varchar(500),
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$body$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

GRANT UPDATE(image_lo,image_oid) ON person TO selfservice_updater;
-- The selfservice_updater role already has the other permissions it needs

GRANT CREATE ON SCHEMA PUBLIC TO selfservice_updater;
ALTER FUNCTION selfservice.selfservice_upd() OWNER TO selfservice_updater;
REVOKE CREATE ON SCHEMA PUBLIC FROM selfservice_updater;
REVOKE ALL ON FUNCTION selfservice.selfservice_upd() FROM PUBLIC;
GRANT EXECUTE ON FUNCTION selfservice.selfservice_upd() TO selfservice;

CREATE TRIGGER seflservice_trig INSTEAD OF UPDATE ON selfservice."40_Selfservice/Database_Selfservice" FOR EACH ROW EXECUTE PROCEDURE selfservice.selfservice_upd();

-- selfservice/008_add_hid_views/install.sql

CREATE VIEW selfservice.room_hid AS
SELECT room.id AS room_id, ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48) AS room_hid
   FROM room
      LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
         LEFT JOIN building_region_hid ON building_region_hid.building_region_id = room.building_region_id
            LEFT JOIN building_hid ON building_hid.building_id = room.building_id
              ORDER BY ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48);

ALTER VIEW selfservice.room_hid OWNER TO dev;
GRANT SELECT ON selfservice.room_hid TO PUBLIC;

CREATE VIEW selfservice.dept_telephone_number_hid AS
SELECT dept_telephone_number.id AS dept_telephone_number_id, dept_telephone_number.extension_number::text || 
        CASE
            WHEN dept_telephone_number.fax = true THEN ' (fax)'::text
            WHEN dept_telephone_number.personal_line = false THEN ' (shared)'::text
            ELSE ''::text
        END AS dept_telephone_number_hid
   FROM dept_telephone_number
  ORDER BY dept_telephone_number.extension_number;
;
ALTER VIEW selfservice.dept_telephone_number_hid OWNER TO dev;
GRANT SELECT ON selfservice.dept_telephone_number_hid TO PUBLIC;

