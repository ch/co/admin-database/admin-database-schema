
-- selfservice/008_add_hid_views/remove.sql

DROP VIEW selfservice.room_hid;
DROP VIEW selfservice.dept_telephone_number_hid;

-- selfservice/007_add_selfservice_main_view/remove.sql

DROP VIEW selfservice."40_Selfservice/Database_Selfservice";
DROP FUNCTION selfservice.selfservice_upd();
REVOKE UPDATE(image_lo,image_oid) ON person FROM selfservice_updater;

-- selfservice/006_add_selfservice_preferences_views/remove.sql

DROP VIEW selfservice._preferences;

-- selfservice/005_add_selfservice_column_data/remove.sql

DROP VIEW selfservice._column_data;

-- selfservice/004_add_selfservice_role_data/remove.sql

DROP VIEW selfservice._role_data;

-- selfservice/003_add_selfservice_view_data/remove.sql

DROP VIEW selfservice._view_data;
DROP TABLE selfservice._primary_table;

-- selfservice/002_add_selfservice_preferences_tables/remove.sql

DROP TABLE selfservice."hw_User Preferences";
DROP TABLE selfservice."hw_Preferences";
DROP TABLE selfservice.hw_preference_type_hid;

-- selfservice/001_add_selfservice_schema/remove.sql

DROP SCHEMA selfservice;
