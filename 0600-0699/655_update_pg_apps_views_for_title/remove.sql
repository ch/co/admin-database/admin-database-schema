DROP VIEW apps.postgrad_applications_app_people_and_posts;

DROP VIEW apps.postgrad_applications_app_people_v2;

CREATE OR REPLACE VIEW apps.postgrad_applications_app_people_v2 AS 
 SELECT person.id AS person_id, person.surname, COALESCE(person.known_as, person.first_names) AS first_names, title_hid.title_hid AS title, person.crsid, role.post_category_id
   FROM person
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN _latest_role_v12 role ON person.id = role.person_id
   LEFT JOIN _physical_status_v3 presence ON person.id = presence.person_id
  WHERE presence.status_id::text = 'Current'::text;

ALTER TABLE apps.postgrad_applications_app_people_v2
  OWNER TO dev;
GRANT ALL ON TABLE apps.postgrad_applications_app_people_v2 TO dev;
GRANT SELECT ON TABLE apps.postgrad_applications_app_people_v2 TO postgrad_applications_app;

CREATE OR REPLACE VIEW apps.postgrad_applications_app_people_and_posts AS 
 SELECT postgrad_applications_app_people_v2.person_id, postgrad_applications_app_people_v2.surname, postgrad_applications_app_people_v2.first_names, postgrad_applications_app_people_v2.title, postgrad_applications_app_people_v2.crsid, postgrad_applications_app_people_v2.post_category_id, postgrad_applications_app_post_categories.post_category
   FROM apps.postgrad_applications_app_people_v2
   JOIN apps.postgrad_applications_app_post_categories USING (post_category_id);

ALTER TABLE apps.postgrad_applications_app_people_and_posts
  OWNER TO dev;
GRANT ALL ON TABLE apps.postgrad_applications_app_people_and_posts TO dev;
GRANT SELECT ON TABLE apps.postgrad_applications_app_people_and_posts TO postgrad_applications_app;

