DROP VIEW apps.computer_reps_groups;

CREATE OR REPLACE VIEW apps.computer_reps_groups AS 
 SELECT research_group.id, research_group.name, person.crsid, person.first_names, person.surname, person_hid.person_hid, person.email_address, title_hid.title_hid
   FROM research_group
   LEFT JOIN mm_research_group_computer_rep ON mm_research_group_computer_rep.research_group_id = research_group.id
   LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN title_hid ON person.title_id = title_hid.title_id;

ALTER TABLE apps.computer_reps_groups
  OWNER TO cen1001;
GRANT ALL ON TABLE apps.computer_reps_groups TO cen1001;
GRANT SELECT ON TABLE apps.computer_reps_groups TO dev;
GRANT SELECT ON TABLE apps.computer_reps_groups TO cos;
GRANT SELECT ON TABLE apps.computer_reps_groups TO computer_reps_reports;

DROP VIEW apps.dms_computer_reps;

CREATE OR REPLACE VIEW apps.dms_computer_reps AS 
 SELECT research_group.id, research_group.name, person.crsid, person.id AS person_id, person.first_names, person.surname, person_hid.person_hid, person.email_address, title_hid.title_hid, COALESCE(research_group.active_directory_container, research_group.name) AS ad_container
   FROM research_group
   LEFT JOIN mm_research_group_computer_rep ON mm_research_group_computer_rep.research_group_id = research_group.id
   LEFT JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN title_hid ON person.title_id = title_hid.title_id;

ALTER TABLE apps.dms_computer_reps
  OWNER TO cen1001;
GRANT ALL ON TABLE apps.dms_computer_reps TO cen1001;
GRANT SELECT ON TABLE apps.dms_computer_reps TO ad_accounts;

