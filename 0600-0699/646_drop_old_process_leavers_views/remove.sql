CREATE OR REPLACE VIEW apps.process_leavers_v2 AS 
 SELECT person.surname, person.first_names, title_hid.title_hid AS title, person.email_address AS email, person_hid.person_hid AS full_name, supervisor_hid.supervisor_hid AS supervisor, supervisor.email_address AS supervisor_email, person_futuremost_role.estimated_leaving_date AS probable_leaving_date, person_futuremost_role.post_category, (COALESCE(title_hid.title_hid, person.first_names)::text || ' '::text) || person.surname::text AS salutation, (COALESCE(pi_title.title_hid, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS pi_salutation, (((COALESCE(title_hid.title_hid, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS person_title_surname, 
        CASE
            WHEN firstaider.id IS NOT NULL THEN 't'::text
            ELSE 'f'::text
        END AS is_firstaider
   FROM person
   LEFT JOIN title_hid USING (title_id)
   JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
   LEFT JOIN person supervisor ON person_futuremost_role.supervisor_id = supervisor.id
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN title_hid pi_title ON supervisor.title_id = pi_title.title_id
   LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
   LEFT JOIN firstaider ON person.id = firstaider.person_id
  WHERE _physical_status_v2.status_id::text = 'Current'::text AND person_futuremost_role.post_category::text <> 'Assistant staff'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE apps.process_leavers_v2
  OWNER TO dev;
GRANT ALL ON TABLE apps.process_leavers_v2 TO dev;
GRANT SELECT ON TABLE apps.process_leavers_v2 TO leavers_management;
GRANT ALL ON TABLE apps.process_leavers_v2 TO cen1001;

CREATE OR REPLACE VIEW apps.process_leavers AS 
 SELECT person.surname, person.first_names, title_hid.title_hid, person.email_address, person_hid.person_hid, supervisor_hid.supervisor_hid, supervisor.email_address AS supervisor_email, person_futuremost_role.estimated_leaving_date, person_futuremost_role.post_category, (COALESCE(title_hid.title_hid, person.first_names)::text || ' '::text) || person.surname::text AS salutation, (COALESCE(pi_title.title_hid, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS pi_salutation, (((COALESCE(title_hid.title_hid, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS person_fullname
   FROM person
   LEFT JOIN title_hid USING (title_id)
   JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
   LEFT JOIN person supervisor ON person_futuremost_role.supervisor_id = supervisor.id
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN title_hid pi_title ON supervisor.title_id = pi_title.title_id
   LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
  WHERE _physical_status_v2.status_id::text = 'Current'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE apps.process_leavers
  OWNER TO cen1001;
GRANT ALL ON TABLE apps.process_leavers TO cen1001;
GRANT SELECT ON TABLE apps.process_leavers TO leavers_management;

CREATE OR REPLACE VIEW _process_leavers AS 
 SELECT person.surname, person.first_names, title_hid.title_hid, person.email_address, person_hid.person_hid, supervisor_hid.supervisor_hid, supervisor.email_address AS supervisor_email, _latest_role_v8.estimated_leaving_date, _latest_role_v8.post_category, (COALESCE(title_hid.title_hid, person.first_names)::text || ' '::text) || person.surname::text AS salutation, (COALESCE(pi_title.title_hid, supervisor.first_names)::text || ' '::text) || supervisor.surname::text AS pi_salutation, (((COALESCE(title_hid.title_hid, ''::character varying)::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS person_fullname
   FROM person
   LEFT JOIN title_hid USING (title_id)
   JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN _latest_role_v8 ON _latest_role_v8.person_id = person.id
   LEFT JOIN person supervisor ON _latest_role_v8.supervisor_id = supervisor.id
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN title_hid pi_title ON supervisor.title_id = pi_title.title_id
   LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
  WHERE _physical_status_v2.status_id::text = 'Current'::text
  ORDER BY person.surname, person.first_names;

ALTER TABLE _process_leavers
  OWNER TO cen1001;
GRANT ALL ON TABLE _process_leavers TO cen1001;
GRANT SELECT ON TABLE _process_leavers TO leavers_management;

