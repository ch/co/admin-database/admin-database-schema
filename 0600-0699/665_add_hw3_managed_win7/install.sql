-- DROP VIEW hotwire3."10_View/Computers/Managed_Windows_7";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Managed_Windows_7" AS 
select * from (
 SELECT 
    system_image.id,
    system_image.hardware_id AS _hardware_id,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.architecture_id, 
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id
    ) AS ip_address_id,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    CASE
        WHEN system_image.reinstall_on_next_boot = true THEN true
        WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
        WHEN system_image.is_managed_mac = true THEN true
        ELSE false
    END AS ro_is_managed_machine,
    every(COALESCE(vlan.name,'default') = 'ch-legacyos') as _all_legacy_vlan,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, NULL::character varying, NULL::character varying) AS "VLAN_assignments"
   FROM system_image
   LEFT JOIN (
      select id, wired_mac_1 as mac from system_image where wired_mac_1 is not null
      union
      select id, wired_mac_2 as mac from system_image where wired_mac_2 is not null
      union
      select id, wired_mac_3 as mac from system_image where wired_mac_3 is not null
      union
      select id, wired_mac_4 as mac from system_image where wired_mac_4 is not null
      union
      select id, wireless_mac as mac from system_image where wireless_mac is not null
   ) system_image_by_mac ON system_image_by_mac.id = system_image.id
   JOIN hardware ON hardware.id = system_image.hardware_id
   JOIN research_group ON system_image.research_group_id = research_group.id
   JOIN operating_system on operating_system_id = operating_system.id
   LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON system_image_by_mac.mac = mac_to_vlan.mac
   LEFT JOIN vlan USING (vid)
   WHERE operating_system.os = 'Windows 7'
   GROUP BY system_image.id, hardware.id
) win7
WHERE ro_is_managed_machine = 't' and _all_legacy_vlan is distinct from 't';

ALTER TABLE hotwire3."10_View/Computers/Managed_Windows_7" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO cos;

CREATE OR REPLACE RULE hotwire3_managed_win7_upd AS
    ON UPDATE TO hotwire3."10_View/Computers/Managed_Windows_7" DO INSTEAD ( UPDATE hardware SET hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET operating_system_id = new.operating_system_id, architecture_id = new.architecture_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, research_group_id = new.research_group_id, user_id = new.user_id
  WHERE system_image.id = old.id;
 SELECT fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update;
);


INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Computers/Managed_Windows_7', 'system_image');
