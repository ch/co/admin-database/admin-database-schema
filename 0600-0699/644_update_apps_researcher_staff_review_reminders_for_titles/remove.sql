DROP VIEW apps.researcher_staff_review_reminders;

CREATE OR REPLACE VIEW apps.researcher_staff_review_reminders AS 
 SELECT person.id, person.surname, person.first_names, person_title.title_hid, ((COALESCE(person_title.salutation_title_hid::text || ' '::text, ''::text) || COALESCE(person.known_as, person.first_names)::text) || ' '::text) || person.surname::text AS reviewee, person.email_address AS reviewee_email_address, _latest_role.post_category_id, 
        CASE
            WHEN person.usual_reviewer_id IS NOT NULL THEN (COALESCE(reviewer_title.salutation_title_hid, COALESCE(usual_reviewer.known_as, usual_reviewer.first_names))::text || ' '::text) || usual_reviewer.surname::text
            ELSE (COALESCE(supervisor_title.salutation_title_hid, COALESCE(supervisor.known_as, supervisor.first_names))::text || ' '::text) || supervisor.surname::text
        END AS reviewer, 
        CASE
            WHEN person.usual_reviewer_id IS NOT NULL THEN usual_reviewer.email_address
            ELSE supervisor.email_address
        END AS reviewer_email_address, 
        CASE
            WHEN person.usual_reviewer_id IS NOT NULL THEN ((COALESCE(reviewer_title.salutation_title_hid::text || ' '::text, ''::text) || COALESCE(usual_reviewer.known_as, usual_reviewer.first_names)::text) || ' '::text) || usual_reviewer.surname::text
            ELSE ((COALESCE(supervisor_title.salutation_title_hid::text || ' '::text, ''::text) || COALESCE(supervisor.known_as, supervisor.first_names)::text) || ' '::text) || supervisor.surname::text
        END AS reviewer_full_name, person.next_review_due, last_review.date_of_meeting AS date_of_last_review, person.silence_staff_review_reminders, COALESCE(usual_reviewer.silence_staff_review_reminders, supervisor.silence_staff_review_reminders) AS silence_staff_review_reminders_for_reviewer
   FROM person
   JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
   JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   JOIN person supervisor ON _latest_role.supervisor_id = supervisor.id
   LEFT JOIN person usual_reviewer ON person.usual_reviewer_id = usual_reviewer.id
   LEFT JOIN ( SELECT staff_review_meeting.person_id, max(staff_review_meeting.date_of_meeting) AS date_of_meeting
   FROM staff_review_meeting
  GROUP BY staff_review_meeting.person_id) last_review ON last_review.person_id = person.id
   LEFT JOIN title_hid person_title ON person.title_id = person_title.title_id
   LEFT JOIN title_hid reviewer_title ON usual_reviewer.title_id = reviewer_title.title_id
   LEFT JOIN title_hid supervisor_title ON supervisor.title_id = supervisor_title.title_id
  WHERE _physical_status.status_id::text = 'Current'::text AND (_latest_role.post_category_id = 'sc-7'::text OR _latest_role.post_category_id = 'sc-4'::text OR _latest_role.post_category_id = 'sc-10'::text OR _latest_role.post_category_id = 'sc-6'::text OR _latest_role.post_category_id = 'sc-5'::text) AND _latest_role.paid_by_university <> false;

ALTER TABLE apps.researcher_staff_review_reminders
  OWNER TO dev;
GRANT ALL ON TABLE apps.researcher_staff_review_reminders TO dev;
GRANT SELECT ON TABLE apps.researcher_staff_review_reminders TO staff_reviews_management;

