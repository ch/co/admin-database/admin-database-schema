CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Basic" AS 
 SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, a.name_suffix, a.email_address, a.crsid, a.date_of_birth, a.ro_post_category_id, a.research_group_id, a.dept_telephone_number_id, a.room_id, a.location, a.ro_physical_status_id, a.ro_estimated_leaving_date, a.is_spri, a._cssclass
   FROM ( SELECT person.id, person.id AS ro_person_id, ROW('image/jpeg'::character varying, person.image_lo)::blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.name_suffix, person.email_address, person.crsid, person.date_of_birth, person.is_spri, _latest_role.post_category_id AS ro_post_category_id, ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, person.location, _physical_status_v2.status_id AS ro_physical_status_id, LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date, 
                CASE
                    WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
      LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN apps.person_futuremost_role futuremost_role ON person.id = futuremost_role.person_id
   LEFT JOIN _physical_status_v2 USING (id)) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_Basic"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Basic" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Basic" TO cos;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Basic" TO phonebook_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/People/Personnel_Basic" TO hr;

CREATE OR REPLACE FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    		v_person_id BIGINT;                                                                                        
      begin                                                                                                              
      IF v.id IS NOT NULL THEN                                                                                           
        v_person_id= v.id;                                                                                               
	UPDATE person SET
		surname=v.surname,
		image_lo=(select (v.image_oid).val from (select v.image_oid) as x),
		image_oid=(select (v.image_oid).val from (select v.image_oid) as x),
	        first_names=v.first_names,
		title_id=v.title_id,                                                       
		name_suffix = v.name_suffix,
		email_address=v.email_address, 
		crsid=v.crsid,
		date_of_birth=v.date_of_birth,
		location=v.location,
		is_spri=v.is_spri                                                                        
        WHERE person.id = v.id;                                                                                          
      ELSE    
	INSERT INTO person (surname, image_lo, image_oid, first_names, title_id, name_suffix, email_address,crsid, date_of_birth, location, is_spri) VALUES (v.surname, (select (v.image_oid).val from (select v.image_oid) as x), (select (v.image_oid).val from (select v.image_oid) as x), v.first_names, v.title_id, v.name_suffix, v.email_address,v.crsid, v.date_of_birth, v.location, v.is_spri) returning id into v_person_id; 
    END IF; 

 perform fn_mm_array_update(v.dept_telephone_number_id,
                            'mm_person_dept_telephone_number'::varchar,
			    'person_id'::varchar,
                            'dept_telephone_number_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.research_group_id,
                            'mm_person_research_group'::varchar,
			    'person_id'::varchar,
                            'research_group_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.room_id,
                            'mm_person_room'::varchar,
                            'person_id'::varchar,
                            'room_id'::varchar,
                            v_person_id);
                                               
      return v_person_id;                                                                                                
      end;                                                

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  OWNER TO dev;


-- Rule: hotwire_view_personnel_basic_del ON hotwire."10_View/People/Personnel_Basic"

-- DROP RULE hotwire_view_personnel_basic_del ON hotwire."10_View/People/Personnel_Basic";

CREATE OR REPLACE RULE hotwire_view_personnel_basic_del AS
    ON DELETE TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  DELETE FROM person
  WHERE person.id = old.id;

-- Rule: hotwire_view_personnel_basic_ins ON hotwire."10_View/People/Personnel_Basic"

-- DROP RULE hotwire_view_personnel_basic_ins ON hotwire."10_View/People/Personnel_Basic";

CREATE OR REPLACE RULE hotwire_view_personnel_basic_ins AS
    ON INSERT TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  SELECT hw_fn_personnel_basic_upd(new.*) AS id;

-- Rule: hotwire_view_personnel_basic_upd ON hotwire."10_View/People/Personnel_Basic"

-- DROP RULE hotwire_view_personnel_basic_upd ON hotwire."10_View/People/Personnel_Basic";

CREATE OR REPLACE RULE hotwire_view_personnel_basic_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Basic" DO INSTEAD  SELECT hw_fn_personnel_basic_upd(new.*) AS id;


