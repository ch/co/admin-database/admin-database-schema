DROP VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro";
CREATE VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" AS
SELECT
    system_image_by_mac.mac AS id,
    mac_to_vlan.id as _mac_to_vlan_id,
    system_image_by_mac.id as system_image_id,
    system_image_by_mac.mac,
    mac_to_vlan.vid,
    vlan.name
FROM
    ( select id, wired_mac_1 as mac from system_image where wired_mac_1 is not null
      union
      select id, wired_mac_2 as mac from system_image where wired_mac_2 is not null
      union
      select id, wired_mac_3 as mac from system_image where wired_mac_3 is not null
      union
      select id, wired_mac_4 as mac from system_image where wired_mac_4 is not null
      union
      select id, wireless_mac as mac from system_image where wireless_mac is not null
    ) system_image_by_mac
LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON system_image_by_mac.mac = mac_to_vlan.mac
LEFT JOIN vlan USING (vid);
ALTER VIEW hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Computers/System_Image/_MAC_to_VLAN_ro" TO cos;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/System_Instances" AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, (((COALESCE(system_image.wired_mac_1::text || ' '::text, ''::text) || COALESCE(system_image.wired_mac_2::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_3::text || ' '::text, ''::text)) || COALESCE(system_image.wired_mac_4::text || ' '::text, ''::text)) || COALESCE(system_image.wireless_mac::text || ' '::text, ''::text) AS all_mac_addresses_ro, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, ARRAY( SELECT mm_dom0_domu.dom0_id
           FROM mm_dom0_domu
          WHERE mm_dom0_domu.domu_id = system_image.id) AS host_system_image_id, system_image.comments::text AS system_image_comments, hardware.comments::text AS hardware_comments, hardware.date_purchased AS date_hardware_purchased, hardware.warranty_end_date AS hardware_warranty_end_date, system_image.hobbit_tier, system_image.hobbit_flags, system_image.is_development_system, system_image.expiry_date AS system_image_expiry_date, hardware.delete_after_date AS hardware_delete_after_date, system_image.is_managed_mac, hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/COs_View'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
   JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE hotwire3."10_View/Computers/System_Instances"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/System_Instances" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Computers/System_Instances" TO cos;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation" AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, system_image.architecture_id, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id, ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id, system_image.reinstall_on_next_boot, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, research_group.fai_class_id AS ro_fai_class_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.netboot_id, 
        CASE
            WHEN system_image.reinstall_on_next_boot = true THEN true
            WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
            WHEN system_image.is_managed_mac = true THEN true
            ELSE false
        END AS ro_is_managed_machine, hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
   JOIN hardware ON hardware.id = system_image.hardware_id
   JOIN research_group ON system_image.research_group_id = research_group.id;

ALTER TABLE hotwire3."10_View/Computers/Autoinstallation"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Autoinstallation" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Autoinstallation" TO cos;

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Managed_Windows_7" AS 
 SELECT win7.id, win7._hardware_id, win7.hardware_type_id, win7.operating_system_id, win7.architecture_id, win7.wired_mac_1, win7.wired_mac_2, win7.wireless_mac, win7.ip_address_id, win7.room_id, win7.user_id, win7.owner_id, win7.research_group_id, win7.ro_is_managed_machine, win7._all_legacy_vlan, win7."Contact_Details", win7."Hardware", win7."IP_addresses", win7."VLAN_assignments"
   FROM ( SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.hardware_type_id, system_image.operating_system_id, system_image.architecture_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
                   FROM mm_system_image_ip_address
                  WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, 
                CASE
                    WHEN system_image.reinstall_on_next_boot = true THEN true
                    WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
                    WHEN system_image.is_managed_mac = true THEN true
                    ELSE false
                END AS ro_is_managed_machine, every(COALESCE(vlan.name, 'default'::character varying)::text = 'ch-legacyos'::text) AS _all_legacy_vlan, hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses", hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
           FROM system_image
      LEFT JOIN (        (        (        (         SELECT system_image.id, system_image.wired_mac_1 AS mac
                                                   FROM system_image
                                                  WHERE system_image.wired_mac_1 IS NOT NULL
                                        UNION 
                                                 SELECT system_image.id, system_image.wired_mac_2 AS mac
                                                   FROM system_image
                                                  WHERE system_image.wired_mac_2 IS NOT NULL)
                                UNION 
                                         SELECT system_image.id, system_image.wired_mac_3 AS mac
                                           FROM system_image
                                          WHERE system_image.wired_mac_3 IS NOT NULL)
                        UNION 
                                 SELECT system_image.id, system_image.wired_mac_4 AS mac
                                   FROM system_image
                                  WHERE system_image.wired_mac_4 IS NOT NULL)
                UNION 
                         SELECT system_image.id, system_image.wireless_mac AS mac
                           FROM system_image
                          WHERE system_image.wireless_mac IS NOT NULL) system_image_by_mac ON system_image_by_mac.id = system_image.id
   JOIN hardware ON hardware.id = system_image.hardware_id
   JOIN research_group ON system_image.research_group_id = research_group.id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN shadow_mac_to_vlan mac_to_vlan ON system_image_by_mac.mac = mac_to_vlan.mac
   LEFT JOIN vlan USING (vid)
  WHERE operating_system.os::text = 'Windows 7'::text
  GROUP BY system_image.id, hardware.id) win7
  WHERE win7.ro_is_managed_machine = true AND win7._all_legacy_vlan IS DISTINCT FROM true;

ALTER TABLE hotwire3."10_View/Computers/Managed_Windows_7"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Managed_Windows_7" TO cos;

