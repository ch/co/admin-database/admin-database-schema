DROP VIEW hotwire3.title_hid;

CREATE OR REPLACE VIEW hotwire3.title_hid AS 
 SELECT title_hid.title_id, title_hid.title_hid
   FROM title_hid;

ALTER TABLE hotwire3.title_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.title_hid TO dev;
GRANT SELECT ON TABLE hotwire3.title_hid TO public;

