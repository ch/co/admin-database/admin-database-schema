DROP VIEW person_pick;

DROP VIEW mailing_lists_optins_view;

DROP VIEW mailing_lists_optouts_view;

DROP VIEW edit_list_of_titles_view;

DROP RULE research_interest_group_ins ON research_interest_group_view;
DROP RULE research_interest_group_upd ON research_interest_group_view;
DROP FUNCTION fn_research_interest_group_upd(research_interest_group_view);
DROP VIEW research_interest_group_view;


