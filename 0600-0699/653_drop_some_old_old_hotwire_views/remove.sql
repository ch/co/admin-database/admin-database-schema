CREATE OR REPLACE VIEW person_pick AS 
 SELECT person.id AS person_id, (((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text) || COALESCE((' ('::text || person.arrival_date::text) || ')'::text, ''::text) AS person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY (((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text) || COALESCE((' ('::text || person.arrival_date::text) || ')'::text, ''::text);

ALTER TABLE person_pick
  OWNER TO cen1001;
GRANT ALL ON TABLE person_pick TO cen1001;
GRANT SELECT ON TABLE person_pick TO ro_hid;

CREATE OR REPLACE VIEW mailing_lists_optins_view AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.generation_view_name AS ro_generation_view_name, mailinglist.notes, pg_views.definition::character varying(1000) AS ro_generation_view_code, mailinglist.extra_addresses::character varying(3000) AS extra_addresses, include_person_hid.include_person_hid AS mm_include_person, mm_mailinglist_include_person.include_person_id AS mm_include_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN mm_mailinglist_include_person ON mailinglist.id = mm_mailinglist_include_person.mailinglist_id
   LEFT JOIN include_person_hid USING (include_person_id)
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE mailing_lists_optins_view
  OWNER TO cen1001;
GRANT ALL ON TABLE mailing_lists_optins_view TO cen1001;

-- Rule: mailing_lists_optins_upd ON mailing_lists_optins_view

-- DROP RULE mailing_lists_optins_upd ON mailing_lists_optins_view;

CREATE OR REPLACE RULE mailing_lists_optins_upd AS
    ON UPDATE TO mailing_lists_optins_view DO INSTEAD ( SELECT fn_upd_many_many_perl(old.id, new.mm_include_person, 'mailinglist'::text, 'include_person'::text) AS fn_upd_many_many_perl;
 UPDATE mailinglist SET name = new.name, notes = new.notes, extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

CREATE OR REPLACE VIEW mailing_lists_optouts_view AS 
 SELECT mailinglist.id, mailinglist.name, mailinglist.notes, mailinglist.generation_view_name AS ro_generation_view_name, pg_views.definition::character varying(1000) AS ro_generation_view_code, mailinglist.extra_addresses::character varying(3000) AS extra_addresses, exclude_person_hid.exclude_person_hid AS mm_exclude_person, mm_mailinglist_exclude_person.exclude_person_id AS mm_exclude_person_id, mailinglist.auto_upload
   FROM mailinglist
   LEFT JOIN mm_mailinglist_exclude_person ON mailinglist.id = mm_mailinglist_exclude_person.mailinglist_id
   LEFT JOIN exclude_person_hid USING (exclude_person_id)
   LEFT JOIN pg_views ON pg_views.viewname = mailinglist.generation_view_name::name;

ALTER TABLE mailing_lists_optouts_view
  OWNER TO cen1001;
GRANT ALL ON TABLE mailing_lists_optouts_view TO cen1001;

-- Rule: mailing_lists_optouts_upd ON mailing_lists_optouts_view

-- DROP RULE mailing_lists_optouts_upd ON mailing_lists_optouts_view;

CREATE OR REPLACE RULE mailing_lists_optouts_upd AS
    ON UPDATE TO mailing_lists_optouts_view DO INSTEAD ( SELECT fn_upd_many_many_perl(old.id, new.mm_exclude_person, 'mailinglist'::text, 'exclude_person'::text) AS fn_upd_many_many_perl;
 UPDATE mailinglist SET name = new.name, notes = new.notes, extra_addresses = new.extra_addresses, auto_upload = new.auto_upload
  WHERE mailinglist.id = old.id;
);

CREATE OR REPLACE VIEW edit_list_of_titles_view AS 
 SELECT title_hid.title_id AS id, title_hid.title_hid AS title
   FROM title_hid;

ALTER TABLE edit_list_of_titles_view
  OWNER TO cen1001;
GRANT ALL ON TABLE edit_list_of_titles_view TO cen1001;
GRANT SELECT, INSERT ON TABLE edit_list_of_titles_view TO hr;
GRANT SELECT, INSERT ON TABLE edit_list_of_titles_view TO student_management;
GRANT SELECT, INSERT ON TABLE edit_list_of_titles_view TO dev;

-- Rule: edit_list_of_titles_ins ON edit_list_of_titles_view

-- DROP RULE edit_list_of_titles_ins ON edit_list_of_titles_view;

CREATE OR REPLACE RULE edit_list_of_titles_ins AS
    ON INSERT TO edit_list_of_titles_view DO INSTEAD  INSERT INTO title_hid (title_hid) 
  VALUES (new.title);

CREATE OR REPLACE VIEW research_interest_group_view AS 
 SELECT research_interest_group.id, research_interest_group.name::character varying(80) AS name, research_interest_group.website, research_interest_group.mailing_list_address, rig_member_hid.rig_member_hid AS mm_rig_member, mm_member_research_interest_group.member_id AS mm_rig_member_id
   FROM research_interest_group
   LEFT JOIN mm_member_research_interest_group ON research_interest_group.id = mm_member_research_interest_group.research_interest_group_id
   LEFT JOIN rig_member_hid ON mm_member_research_interest_group.member_id = rig_member_hid.rig_member_id;

ALTER TABLE research_interest_group_view
  OWNER TO cen1001;
GRANT ALL ON TABLE research_interest_group_view TO cen1001;
GRANT SELECT ON TABLE research_interest_group_view TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE research_interest_group_view TO rig_management;
GRANT SELECT, UPDATE ON TABLE research_interest_group_view TO dev;

CREATE OR REPLACE FUNCTION fn_research_interest_group_upd(research_interest_group_view)
  RETURNS bigint AS
$BODY$
       declare v alias for $1;
               v_rig_id bigint;
       begin
                v_rig_id = nextval('research_interest_group_id_seq');
                insert into research_interest_group ( id, name,website,mailing_list_address ) values ( v_rig_id, v.name,v.website,v.mailing_list_address);
                perform fn_upd_many_many ( v_rig_id, v.mm_rig_member, 'research_interest_group','member');
                return v_rig_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_research_interest_group_upd(research_interest_group_view)
  OWNER TO cen1001;


-- Rule: research_interest_group_ins ON research_interest_group_view

-- DROP RULE research_interest_group_ins ON research_interest_group_view;

CREATE OR REPLACE RULE research_interest_group_ins AS
    ON INSERT TO research_interest_group_view DO INSTEAD  SELECT fn_research_interest_group_upd(new.*) AS id;

-- Rule: research_interest_group_upd ON research_interest_group_view

-- DROP RULE research_interest_group_upd ON research_interest_group_view;

CREATE OR REPLACE RULE research_interest_group_upd AS
    ON UPDATE TO research_interest_group_view DO INSTEAD ( UPDATE research_interest_group SET name = new.name, website = new.website, mailing_list_address = new.mailing_list_address
  WHERE research_interest_group.id = old.id;
 SELECT fn_upd_many_many(old.id, new.mm_rig_member, 'research_interest_group'::text, 'member'::text) AS fn_upd_many_many;
);
