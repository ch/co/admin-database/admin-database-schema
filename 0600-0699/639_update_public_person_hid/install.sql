CREATE OR REPLACE VIEW person_hid AS 
 SELECT person.id AS person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.abbrev_title::text || ' '::text, ''::text);

ALTER TABLE person_hid
  OWNER TO dev;
GRANT ALL ON TABLE person_hid TO dev;
GRANT SELECT ON TABLE person_hid TO mgmt_ro;
GRANT SELECT ON TABLE person_hid TO space_management;
GRANT SELECT ON TABLE person_hid TO ro_hid;
GRANT SELECT ON TABLE person_hid TO old_selfservice;
GRANT SELECT ON TABLE person_hid TO www_sites;
GRANT ALL ON TABLE person_hid TO cos;
GRANT SELECT ON TABLE person_hid TO ipreg;

