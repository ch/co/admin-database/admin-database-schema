--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = www;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: website_roles; Type: TABLE; Schema: www; Owner: postgres; Tablespace: 
--

CREATE TABLE website_roles (
    id integer NOT NULL,
    role character varying(80),
    parent_id integer DEFAULT 0,
    weight integer DEFAULT 0
);


ALTER TABLE www.website_roles OWNER TO postgres;

--
-- Name: website_roles_id_seq; Type: SEQUENCE; Schema: www; Owner: postgres
--

CREATE SEQUENCE website_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE www.website_roles_id_seq OWNER TO postgres;

--
-- Name: website_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: www; Owner: postgres
--

ALTER SEQUENCE website_roles_id_seq OWNED BY website_roles.id;


--
-- Name: id; Type: DEFAULT; Schema: www; Owner: postgres
--

ALTER TABLE ONLY website_roles ALTER COLUMN id SET DEFAULT nextval('website_roles_id_seq'::regclass);


--
-- Data for Name: website_roles; Type: TABLE DATA; Schema: www; Owner: postgres
--

COPY website_roles (id, role, parent_id, weight) FROM stdin;
1	Floor Technicians	0	0
2	Specialist Technicians	0	1
3	Teaching Technicians	0	2
4	Workshops	0	3
5	Support	0	4
6	1st Floor	1	0
7	2nd Floor	1	1
8	3rd Floor	1	2
9	CBC	1	3
10	Jones/Dobson/Jenkins	1	4
11	Melville Laboratory	1	5
12	Whiffen Laboratory	1	6
13	NMR	2	0
14	Mass Spec.	2	1
15	Microanalysis	2	2
16	Part IA	3	0
17	Part IB	3	1
18	Part IB/II Physical and Theoretical	3	2
19	Part II	3	3
20	Electrical/Electronics	4	0
21	Glassblower	4	1
22	Maintenance	4	2
23	Mechanical	4	3
24	Accounts	5	0
25	Admin Office	5	1
26	Custodian/Security	5	2
27	Cyber Cafe	5	3
28	Library	5	4
29	Photography	5	5
30	Reception	5	6
31	Reprographics	5	7
32	Secretaries	5	8
33	Stores	5	9
34	Unilever Office	5	10
35	Secretaries of Department	0	0
36	Research Facilitator	0	0
37	Science Writer in Residence	0	0
38	Research and Facilities Manager	0	0
39	Departmental Safety Officer	0	0
40	Senior Technical Officers	0	0
41	Technical Officers	0	0
42	Head of IT	0	0
43	Computer Officers	0	0
\.


--
-- Name: website_roles_id_seq; Type: SEQUENCE SET; Schema: www; Owner: postgres
--

SELECT pg_catalog.setval('website_roles_id_seq', 43, true);


--
-- Name: website_roles_pkey; Type: CONSTRAINT; Schema: www; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY website_roles
    ADD CONSTRAINT website_roles_pkey PRIMARY KEY (id);


--
-- Name: website_roles; Type: ACL; Schema: www; Owner: postgres
--

REVOKE ALL ON TABLE website_roles FROM PUBLIC;
REVOKE ALL ON TABLE website_roles FROM postgres;
GRANT ALL ON TABLE website_roles TO postgres;
GRANT SELECT ON TABLE website_roles TO www_sites;
GRANT ALL ON TABLE website_roles TO alt36;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = www;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mm_person_website_roles; Type: TABLE; Schema: www; Owner: postgres; Tablespace: 
--

CREATE TABLE mm_person_website_roles (
    website_role_id integer NOT NULL,
    person_id integer NOT NULL,
    weight integer DEFAULT 0
);


ALTER TABLE www.mm_person_website_roles OWNER TO postgres;

--
-- Name: mm_person_website_roles_pkey; Type: CONSTRAINT; Schema: www; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mm_person_website_roles
    ADD CONSTRAINT mm_person_website_roles_pkey PRIMARY KEY (website_role_id, person_id);


--
-- Name: mm_person_website_roles_person_id_fkey; Type: FK CONSTRAINT; Schema: www; Owner: postgres
--

ALTER TABLE ONLY mm_person_website_roles
    ADD CONSTRAINT mm_person_website_roles_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: mm_person_website_roles_website_role_id_fkey; Type: FK CONSTRAINT; Schema: www; Owner: postgres
--

ALTER TABLE ONLY mm_person_website_roles
    ADD CONSTRAINT mm_person_website_roles_website_role_id_fkey FOREIGN KEY (website_role_id) REFERENCES website_roles(id);


--
-- Name: mm_person_website_roles; Type: ACL; Schema: www; Owner: postgres
--

REVOKE ALL ON TABLE mm_person_website_roles FROM PUBLIC;
REVOKE ALL ON TABLE mm_person_website_roles FROM postgres;
GRANT ALL ON TABLE mm_person_website_roles TO postgres;
GRANT SELECT ON TABLE mm_person_website_roles TO www_sites;
GRANT ALL ON TABLE mm_person_website_roles TO alt36;
GRANT SELECT ON TABLE mm_person_website_roles TO dev;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = www;

--
-- Name: person_website_roles; Type: VIEW; Schema: www; Owner: alt36
--

CREATE VIEW person_website_roles AS
    SELECT mm_person_website_roles.website_role_id, person.crsid, www_person_notitles_hid.www_person_hid, person_hid.person_hid, mm_person_website_roles.weight FROM ((((mm_person_website_roles LEFT JOIN public.www_person_notitles_hid ON ((www_person_notitles_hid.person_id = mm_person_website_roles.person_id))) LEFT JOIN public.person ON ((person.id = mm_person_website_roles.person_id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.id))) LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text) ORDER BY mm_person_website_roles.weight, person_hid.person_hid;


ALTER TABLE www.person_website_roles OWNER TO alt36;

--
-- Name: person_website_roles; Type: ACL; Schema: www; Owner: alt36
--

REVOKE ALL ON TABLE person_website_roles FROM PUBLIC;
REVOKE ALL ON TABLE person_website_roles FROM alt36;
GRANT ALL ON TABLE person_website_roles TO alt36;
GRANT ALL ON TABLE person_website_roles TO postgres;
GRANT ALL ON TABLE person_website_roles TO dev;
GRANT ALL ON TABLE person_website_roles TO www_sites;


--
-- PostgreSQL database dump complete
--

