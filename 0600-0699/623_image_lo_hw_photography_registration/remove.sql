CREATE OR REPLACE VIEW hotwire."10_View/People/Photography_Registration" AS 
 SELECT person.id, person.crsid AS ro_crsid, person.gender_id, person.first_names, person.known_as, person.surname, person.date_of_birth, person.arrival_date AS ro_arrival_date, ARRAY( SELECT mm_person_research_group.research_group_id
           FROM mm_person_research_group
          WHERE person.id = mm_person_research_group.person_id) AS research_group_id, ROW('image/jpeg'::character varying, person.image_oid)::blobtype AS photo
   FROM person
   LEFT JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
  WHERE person.is_spri IS NOT TRUE
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/People/Photography_Registration"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Photography_Registration" TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire."10_View/People/Photography_Registration" TO photography;

CREATE OR REPLACE FUNCTION hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  RETURNS bigint AS
$BODY$
	declare v alias for $1;
		v_person_id bigint;
	begin
		if v.id is not null
		then
			v_person_id = v.id;
			update person set
				gender_id = v.gender_id,
				first_names = v.first_names,
				known_as = v.known_as,
				surname = v.surname,
				date_of_birth = v.date_of_birth,
				image_oid = ( select (v.photo).val as val from ( select v.photo ) x) 
			where person.id = v_person_id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
				image_oid
			) values (
				v.gender_id,
				v.first_names,
				v.known_as,
				v.surname,
				v.date_of_birth,
				( select (v.photo).val as val from ( select v.photo ) x)
			) returning id into v_person_id;
		end if;
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);	
		return v_person_id;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  OWNER TO dev;


-- Rule: "hotwire_10_View/People/Photography_Registration_ins" ON hotwire."10_View/People/Photography_Registration"

-- DROP RULE "hotwire_10_View/People/Photography_Registration_ins" ON hotwire."10_View/People/Photography_Registration";

CREATE OR REPLACE RULE "hotwire_10_View/People/Photography_Registration_ins" AS
    ON INSERT TO hotwire."10_View/People/Photography_Registration" DO INSTEAD  SELECT hw_fn_personnel_photography_reg_upd(new.*) AS id;

-- Rule: "hotwire_10_View/People/Photography_Registration_upd" ON hotwire."10_View/People/Photography_Registration"

-- DROP RULE "hotwire_10_View/People/Photography_Registration_upd" ON hotwire."10_View/People/Photography_Registration";

CREATE OR REPLACE RULE "hotwire_10_View/People/Photography_Registration_upd" AS
    ON UPDATE TO hotwire."10_View/People/Photography_Registration" DO INSTEAD  SELECT hw_fn_personnel_photography_reg_upd(new.*) AS id;


