CREATE OR REPLACE VIEW ucc_hardware_view AS 
 SELECT hardware_full_view.id, hardware_full_view.manufacturer, hardware_full_view.model, hardware_full_view.name, hardware_full_view.serial_number, hardware_full_view.monitor_serial_number, hardware_full_view.asset_tag, hardware_full_view.processor_type, hardware_full_view.processor_speed, hardware_full_view.number_of_disks, hardware_full_view.value_when_new, hardware_full_view.date_purchased, hardware_full_view.date_configured, hardware_full_view.date_decommissioned, hardware_full_view.delete_after_date, hardware_full_view.warranty_end_date, hardware_full_view.warranty_details, hardware_full_view.room_id, hardware_full_view.hardware_type_id, hardware_full_view.person_id
   FROM hardware_full_view
  WHERE (hardware_full_view.person_id IN ( SELECT mm_person_research_group.person_id
           FROM research_group
      JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     WHERE research_group.name::text = 'UCC'::text));

ALTER TABLE ucc_hardware_view
  OWNER TO cen1001;
GRANT ALL ON TABLE ucc_hardware_view TO cen1001;
GRANT SELECT ON TABLE ucc_hardware_view TO dev;
GRANT SELECT ON TABLE ucc_hardware_view TO ucc;

CREATE OR REPLACE VIEW hotwire."10_View/UCC_Hardware" AS 
 SELECT hardware_full_view.id, hardware_full_view.manufacturer, hardware_full_view.model, hardware_full_view.name, hardware_full_view.serial_number, hardware_full_view.monitor_serial_number, hardware_full_view.asset_tag, hardware_full_view.processor_type, hardware_full_view.processor_speed, hardware_full_view.number_of_disks, hardware_full_view.value_when_new, hardware_full_view.date_purchased, hardware_full_view.date_configured, hardware_full_view.date_decommissioned, hardware_full_view.delete_after_date, hardware_full_view.warranty_end_date, hardware_full_view.warranty_details, hardware_full_view.room_id, hardware_full_view.hardware_type_id, hardware_full_view.person_id
   FROM hardware_full_view
  WHERE (hardware_full_view.person_id IN ( SELECT mm_person_research_group.person_id
           FROM research_group
      JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id
     WHERE research_group.name::text = 'UCC'::text));

ALTER TABLE hotwire."10_View/UCC_Hardware"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/UCC_Hardware" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/UCC_Hardware" TO ucc;

