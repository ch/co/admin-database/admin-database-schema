CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_Phone" AS 
 SELECT a.id, a.ro_person, a.image_oid, a.surname, a.first_names, a.dept_telephone_number_id, a.external_work_numbers, a.room_id, a.ro_supervisor_id, a.email_address, a.location, a.ro_post_category_id, a.ro_physical_status_id, a.is_external, a._cssclass, a._title_hid, a._known_as
   FROM ( SELECT person.id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS ro_person, person.image_lo AS image_oid, person.surname, person.first_names, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, person.external_work_phone_numbers AS external_work_numbers, ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, person.email_address, person.location, _latest_role.post_category_id AS ro_post_category_id, _physical_status.status_id AS ro_physical_status_id, person.is_external, 
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN person.is_external = true THEN 'blue'::text
                    ELSE NULL::text
                END AS _cssclass, title_hid.title_hid AS _title_hid, person.known_as AS _known_as
           FROM person
      LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN cache._latest_role _latest_role ON _latest_role.person_id = person.id) a
  ORDER BY a.surname, a._title_hid, a._known_as;

ALTER TABLE hotwire."10_View/People/Personnel_Phone"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Phone" TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Phone" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO accounts;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO reception;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO hr;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO phones;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO student_management_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO building_security;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO interviewtest;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO chematcam;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO phonebook_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Phone" TO reception_cover;
GRANT SELECT ON TABLE hotwire."10_View/People/Personnel_Phone" TO safety_management;

-- Rule: hotwire_view_personnel_phone_upd ON hotwire."10_View/People/Personnel_Phone"

-- DROP RULE hotwire_view_personnel_phone_upd ON hotwire."10_View/People/Personnel_Phone";

CREATE OR REPLACE RULE hotwire_view_personnel_phone_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_Phone" DO INSTEAD ( UPDATE person SET surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address, is_external = new.is_external
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
);


