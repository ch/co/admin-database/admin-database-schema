CREATE OR REPLACE VIEW apps.person_hid AS 
 SELECT person.id AS person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.title_hid::text || ' '::text, ''::text);

ALTER TABLE apps.person_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE apps.person_hid TO cen1001;
GRANT SELECT ON TABLE apps.person_hid TO dev;

CREATE OR REPLACE VIEW apps.computers_by_research_group AS 
 SELECT system_image_hid.system_image_hid AS known_as, research_group.id AS research_group_id, research_group.name AS research_group, hardware.manufacturer, hardware.model, room_hid.room_hid AS room, user_hid.person_hid AS "user", owner_hid.person_hid AS owner, hardware.asset_tag, hardware.serial_number, operating_system.os, hardware.date_purchased, age(hardware.date_purchased::timestamp with time zone) AS age, ARRAY( SELECT ((COALESCE(ip_address.hostname, ''::character varying)::text || ' ('::text) || ip_address.ip) || ')'::text
           FROM ip_address
      JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
   JOIN system_image s2 ON mm_system_image_ip_address.system_image_id = s2.id
  WHERE s2.id = system_image.id) AS ip_addresses
   FROM system_image
   JOIN apps.system_image_hid ON system_image_hid.system_image_id = system_image.id
   JOIN research_group ON system_image.research_group_id = research_group.id
   LEFT JOIN hardware ON hardware.id = system_image.hardware_id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN apps.person_hid user_hid ON user_hid.person_id = system_image.user_id
   LEFT JOIN apps.person_hid owner_hid ON owner_hid.person_id = hardware.owner_id
   LEFT JOIN apps.room_hid ON hardware.room_id = room_hid.room_id;

ALTER TABLE apps.computers_by_research_group
  OWNER TO cen1001;
GRANT ALL ON TABLE apps.computers_by_research_group TO cen1001;
GRANT SELECT ON TABLE apps.computers_by_research_group TO dev;
GRANT SELECT ON TABLE apps.computers_by_research_group TO cos;
GRANT SELECT ON TABLE apps.computers_by_research_group TO computer_reps_reports;

ALTER TABLE apps.system_image_hid
  OWNER TO cen1001;
