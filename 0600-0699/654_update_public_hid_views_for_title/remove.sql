CREATE OR REPLACE VIEW exclude_person_hid AS 
 SELECT person.id AS exclude_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS exclude_person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE exclude_person_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE exclude_person_hid TO cen1001;
GRANT SELECT ON TABLE exclude_person_hid TO ro_hid;

CREATE OR REPLACE VIEW include_person_hid AS 
 SELECT person.id AS include_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS include_person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE include_person_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE include_person_hid TO cen1001;
GRANT SELECT ON TABLE include_person_hid TO ro_hid;

CREATE OR REPLACE VIEW rig_member_hid AS 
 SELECT person.id AS rig_member_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS rig_member_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN _physical_status_v2 ON person.id = _physical_status_v2.person_id
  WHERE _physical_status_v2.status_id::text <> 'Past'::text
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.title_hid::text || ' '::text, ''::text);

ALTER TABLE rig_member_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE rig_member_hid TO cen1001;
GRANT SELECT ON TABLE rig_member_hid TO ro_hid;

