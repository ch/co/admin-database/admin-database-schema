CREATE OR REPLACE FUNCTION person_update_image_lo()
  RETURNS trigger AS
$BODY$
BEGIN
        NEW.image_lo := NEW.image_oid::oid;
RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION person_update_image_lo()
  OWNER TO dev;

CREATE TRIGGER image_lo_from_image_oid
  BEFORE INSERT OR UPDATE
  ON person
  FOR EACH ROW
  EXECUTE PROCEDURE person_update_image_lo();
