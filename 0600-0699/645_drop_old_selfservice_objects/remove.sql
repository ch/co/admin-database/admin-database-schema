CREATE OR REPLACE FUNCTION apps_personal_data_v2_upd()
  RETURNS trigger AS
$BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address,
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact,
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_research_group_id,
                                'mm_person_research_group'::varchar,
                                'person_id'::varchar,
                                'research_group_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION apps_personal_data_v2_upd()
  OWNER TO dev;

CREATE OR REPLACE FUNCTION apps_personal_data_v3_upd()
  RETURNS trigger AS
$BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address,
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact,
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_research_group_id,
                                'mm_person_research_group'::varchar,
                                'person_id'::varchar,
                                'research_group_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION apps_personal_data_v3_upd()
  OWNER TO dev;

CREATE OR REPLACE FUNCTION apps_personal_data_v4_upd()
  RETURNS trigger AS
$BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address,
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact,
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION apps_personal_data_v4_upd()
  OWNER TO dev;

CREATE OR REPLACE FUNCTION apps_personal_data_v5_upd()
  RETURNS trigger AS
$BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address::varchar(500),
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact::varchar(500),
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION apps_personal_data_v5_upd()
  OWNER TO dev;

CREATE OR REPLACE FUNCTION apps_personal_data_v6_upd()
  RETURNS trigger AS
$BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address::varchar(500),
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact::varchar(500),
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION apps_personal_data_v6_upd() SET search_path=public, pg_temp;

ALTER FUNCTION apps_personal_data_v6_upd()
  OWNER TO selfservice_updater;
GRANT EXECUTE ON FUNCTION apps_personal_data_v6_upd() TO selfservice_updater;
GRANT EXECUTE ON FUNCTION apps_personal_data_v6_upd() TO old_selfservice;
REVOKE ALL ON FUNCTION apps_personal_data_v6_upd() FROM public;



CREATE OR REPLACE VIEW apps.personal_data_selfservice_v2 AS 
 SELECT person.id, person.surname AS ro_surname, person.first_names AS ro_first_names, person.title_id AS ro_title_id, person.known_as, person.name_suffix AS name_suffix_eg_frs, person.gender_id AS ro_gender_id, person.previous_surname AS ro_maiden_name, ARRAY( SELECT mm.nationality_id
           FROM mm_person_nationality mm
          WHERE mm.person_id = person.id) AS ro_mm_nationality_id, person.email_address, person.hide_email AS hide_email_from_dept_websites, ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS mm_room_id, ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS mm_dept_telephone_number_id, person.cambridge_college_id AS ro_cambridge_college_id, person.cambridge_address AS home_address, person.cambridge_phone_number AS home_phone_number, person.emergency_contact, ARRAY( SELECT mm.research_group_id
           FROM mm_person_research_group mm
          WHERE mm.person_id = person.id) AS mm_research_group_id, _latest_role_v12.post_category_id AS ro_post_category_id, _latest_role_v12.supervisor_id AS ro_supervisor_id, person.arrival_date AS ro_arrival_date, _latest_role_v12.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain, person.do_not_show_on_website AS hide_from_website, user_account.chemnet_token AS ro_chemnet_token, false AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username::text = person.crsid::text
  WHERE person.ban_from_self_service <> true;

ALTER TABLE apps.personal_data_selfservice_v2
  OWNER TO dev;
GRANT ALL ON TABLE apps.personal_data_selfservice_v2 TO dev;
GRANT SELECT, UPDATE ON TABLE apps.personal_data_selfservice_v2 TO old_selfservice;

-- Trigger: apps_personal_data_v2_trig on apps.personal_data_selfservice_v2

-- DROP TRIGGER apps_personal_data_v2_trig ON apps.personal_data_selfservice_v2;

CREATE TRIGGER apps_personal_data_v2_trig
  INSTEAD OF UPDATE
  ON apps.personal_data_selfservice_v2
  FOR EACH ROW
  EXECUTE PROCEDURE apps_personal_data_v2_upd();

CREATE OR REPLACE VIEW apps.personal_data_selfservice_v3 AS 
 SELECT person.id, person.surname AS ro_surname, person.first_names AS ro_first_names, person.title_id AS ro_title_id, person.known_as, person.name_suffix AS name_suffix_eg_frs, person.gender_id AS ro_gender_id, person.previous_surname AS ro_previous_name, ARRAY( SELECT mm.nationality_id
           FROM mm_person_nationality mm
          WHERE mm.person_id = person.id) AS ro_mm_nationality_id, person.email_address, person.hide_email AS hide_email_from_dept_websites, ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS mm_room_id, ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS mm_dept_telephone_number_id, person.cambridge_college_id AS ro_cambridge_college_id, person.cambridge_address AS home_address, person.cambridge_phone_number AS home_phone_number, person.emergency_contact, ARRAY( SELECT mm.research_group_id
           FROM mm_person_research_group mm
          WHERE mm.person_id = person.id) AS mm_research_group_id, _latest_role_v12.post_category_id AS ro_post_category_id, _latest_role_v12.supervisor_id AS ro_supervisor_id, person.arrival_date AS ro_arrival_date, _latest_role_v12.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain, person.do_not_show_on_website AS hide_from_website, user_account.chemnet_token AS ro_chemnet_token, false AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username::text = person.crsid::text
  WHERE person.ban_from_self_service <> true;

ALTER TABLE apps.personal_data_selfservice_v3
  OWNER TO dev;
GRANT ALL ON TABLE apps.personal_data_selfservice_v3 TO dev;
GRANT SELECT, UPDATE ON TABLE apps.personal_data_selfservice_v3 TO old_selfservice;

-- Trigger: apps_personal_data_v3_trig on apps.personal_data_selfservice_v3

-- DROP TRIGGER apps_personal_data_v3_trig ON apps.personal_data_selfservice_v3;

CREATE TRIGGER apps_personal_data_v3_trig
  INSTEAD OF UPDATE
  ON apps.personal_data_selfservice_v3
  FOR EACH ROW
  EXECUTE PROCEDURE apps_personal_data_v3_upd();


CREATE OR REPLACE VIEW apps.personal_data_selfservice_v4 AS 
 SELECT person.id, person.surname AS ro_surname, person.first_names AS ro_first_names, title_hid.title_hid AS ro_title, person.known_as, person.name_suffix AS name_suffix_eg_frs, gender_hid.gender_hid AS ro_gender, person.previous_surname AS ro_previous_name, ARRAY( SELECT mm.nationality_id
           FROM mm_person_nationality mm
          WHERE mm.person_id = person.id) AS ro_mm_nationality_id, person.email_address, person.hide_email AS hide_email_from_dept_websites, ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS mm_room_id, ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS mm_dept_telephone_number_id, cambridge_college_hid.cambridge_college_hid AS ro_cambridge_college, person.cambridge_address AS home_address, person.cambridge_phone_number AS home_phone_number, person.emergency_contact, ARRAY( SELECT mm.research_group_id
           FROM mm_person_research_group mm
          WHERE mm.person_id = person.id) AS ro_mm_research_group_id, post_category_hid.post_category_hid AS ro_post_category, supervisor_hid.supervisor_hid AS ro_supervisor, person.arrival_date AS ro_arrival_date, _latest_role_v12.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain, person.do_not_show_on_website AS hide_from_website, user_account.chemnet_token AS ro_chemnet_token, false AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username::text = person.crsid::text
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
  WHERE person.ban_from_self_service <> true;

ALTER TABLE apps.personal_data_selfservice_v4
  OWNER TO dev;
GRANT ALL ON TABLE apps.personal_data_selfservice_v4 TO dev;
GRANT SELECT, UPDATE ON TABLE apps.personal_data_selfservice_v4 TO old_selfservice;

-- Trigger: apps_personal_data_v4_trig on apps.personal_data_selfservice_v4

-- DROP TRIGGER apps_personal_data_v4_trig ON apps.personal_data_selfservice_v4;

CREATE TRIGGER apps_personal_data_v4_trig
  INSTEAD OF UPDATE
  ON apps.personal_data_selfservice_v4
  FOR EACH ROW
  EXECUTE PROCEDURE apps_personal_data_v4_upd();


CREATE OR REPLACE VIEW apps.personal_data_selfservice_v5 AS 
 SELECT DISTINCT person.id, person.surname AS ro_surname, person.first_names AS ro_first_names, title_hid.title_hid AS ro_title, person.known_as, person.name_suffix AS name_suffix_eg_frs, gender_hid.gender_hid AS ro_gender, person.previous_surname AS ro_previous_name, nationality_string.ro_nationality, person.email_address, person.hide_email AS hide_email_from_dept_websites, ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS mm_room_id, ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS mm_dept_telephone_number_id, cambridge_college_hid.cambridge_college_hid AS ro_cambridge_college, person.cambridge_address::text AS home_address, person.cambridge_phone_number AS home_phone_number, person.emergency_contact::text AS emergency_contact, research_groups_string.ro_research_group, post_category_hid.post_category_hid::character varying AS ro_post_category, supervisor_hid.supervisor_hid::character varying AS ro_supervisor, person.arrival_date AS ro_arrival_date, _latest_role_v12.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain, person.do_not_show_on_website AS hide_from_website, user_account.chemnet_token AS ro_chemnet_token, false AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username::text = person.crsid::text
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
   LEFT JOIN ( SELECT person.id AS person_id, string_agg(nationality.nationality::text, ', '::text)::character varying AS ro_nationality
   FROM person
   LEFT JOIN mm_person_nationality ON mm_person_nationality.person_id = person.id
   LEFT JOIN nationality ON mm_person_nationality.nationality_id = nationality.id
  GROUP BY person.id) nationality_string ON nationality_string.person_id = person.id
   LEFT JOIN ( SELECT person.id AS person_id, string_agg(research_group.name::text, ', '::text)::character varying AS ro_research_group
   FROM person
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
  GROUP BY person.id) research_groups_string ON research_groups_string.person_id = person.id
  WHERE person.ban_from_self_service <> true
  GROUP BY person.id, title_hid.title_hid, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, _latest_role_v12.intended_end_date, _latest_role_v12.end_date, user_account.chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group;

ALTER TABLE apps.personal_data_selfservice_v5
  OWNER TO dev;
GRANT ALL ON TABLE apps.personal_data_selfservice_v5 TO dev;
GRANT SELECT, UPDATE ON TABLE apps.personal_data_selfservice_v5 TO old_selfservice;

-- Trigger: apps_personal_data_v5_trig on apps.personal_data_selfservice_v5

-- DROP TRIGGER apps_personal_data_v5_trig ON apps.personal_data_selfservice_v5;

CREATE TRIGGER apps_personal_data_v5_trig
  INSTEAD OF UPDATE
  ON apps.personal_data_selfservice_v5
  FOR EACH ROW
  EXECUTE PROCEDURE apps_personal_data_v5_upd();

CREATE OR REPLACE VIEW apps.personal_data_selfservice_v6 AS 
 SELECT DISTINCT person.id, title_hid.title_hid AS ro_title, person.first_names AS ro_first_names, person.surname AS ro_surname, person.known_as, person.name_suffix AS name_suffix_eg_frs, gender_hid.gender_hid AS ro_gender, person.previous_surname AS ro_previous_name, nationality_string.ro_nationality, person.email_address, person.hide_email AS hide_email_from_dept_websites, ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS mm_room_id, ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS mm_dept_telephone_number_id, cambridge_college_hid.cambridge_college_hid AS ro_cambridge_college, person.cambridge_address::text AS home_address, person.cambridge_phone_number AS home_phone_number, person.emergency_contact::text AS emergency_contact, research_groups_string.ro_research_group, post_category_hid.post_category_hid::character varying AS ro_post_category, supervisor_hid.supervisor_hid::character varying AS ro_supervisor, person.arrival_date AS ro_arrival_date, _latest_role_v12.funding_end_date AS ro_funding_end_date, COALESCE(_latest_role_v12.intended_end_date, _latest_role_v12.end_date) AS ro_expected_leaving_date, person.crsid AS ro_crsid, person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain, person.do_not_show_on_website AS hide_from_website, user_account.chemnet_token AS ro_chemnet_token, false AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username::text = person.crsid::text
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
   LEFT JOIN ( SELECT person.id AS person_id, string_agg(nationality.nationality::text, ', '::text)::character varying AS ro_nationality
   FROM person
   LEFT JOIN mm_person_nationality ON mm_person_nationality.person_id = person.id
   LEFT JOIN nationality ON mm_person_nationality.nationality_id = nationality.id
  GROUP BY person.id) nationality_string ON nationality_string.person_id = person.id
   LEFT JOIN ( SELECT person.id AS person_id, string_agg(research_group.name::text, ', '::text)::character varying AS ro_research_group
   FROM person
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
  GROUP BY person.id) research_groups_string ON research_groups_string.person_id = person.id
  WHERE person.ban_from_self_service <> true
  GROUP BY person.id, title_hid.title_hid, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, _latest_role_v12.intended_end_date, _latest_role_v12.end_date, user_account.chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group;

ALTER TABLE apps.personal_data_selfservice_v6
  OWNER TO dev;
GRANT ALL ON TABLE apps.personal_data_selfservice_v6 TO dev;
GRANT SELECT, UPDATE ON TABLE apps.personal_data_selfservice_v6 TO old_selfservice;

-- Trigger: apps_personal_data_v6_trig on apps.personal_data_selfservice_v6

-- DROP TRIGGER apps_personal_data_v6_trig ON apps.personal_data_selfservice_v6;

CREATE TRIGGER apps_personal_data_v6_trig
  INSTEAD OF UPDATE
  ON apps.personal_data_selfservice_v6
  FOR EACH ROW
  EXECUTE PROCEDURE apps_personal_data_v6_upd();


