-- People/All_Contact_Details
CREATE OR REPLACE RULE hw_all_contact_details_upd AS ON UPDATE TO hotwire3."10_View/People/All_Contact_Details" DO INSTEAD (
  UPDATE 
    person 
  SET 
    image_oid = new.image_oid :: bigint, 
    image_lo = new.image_oid,
    surname = new.surname, 
    first_names = new.first_names, 
    title_id = new.title_id, 
    known_as = new.known_as, 
    name_suffix = new.name_suffix, 
    previous_surname = new.previous_surname, 
    gender_id = new.gender_id, 
    crsid = new.crsid, 
    email_address = new.email_address, 
    arrival_date = new.arrival_date, 
    leaving_date = new.leaving_date, 
    left_but_no_leaving_date_given = new.left_but_no_leaving_date_given, 
    external_work_phone_numbers = new.external_work_phone_numbers, 
    cambridge_address = new.home_address :: character varying(500), 
    cambridge_phone_number = new.home_phone_number, 
    cambridge_college_id = new.cambridge_college_id, 
    mobile_number = new.mobile_number, 
    emergency_contact = new.emergency_contact :: character varying(500), 
    location = new.location, 
    forwarding_address = new.forwarding_address :: character varying(500), 
    new_employer_address = new.new_employer_address :: character varying(500) 
  WHERE 
    person.id = old.id;
SELECT 
  fn_mm_array_update(
    new.dept_telephone_number_id, old.dept_telephone_number_id, 
    'mm_person_dept_telephone_number' :: character varying, 
    'person_id' :: character varying, 
    'dept_telephone_number_id' :: character varying, 
    old.id
  ) AS fn_mm_array_update;
SELECT 
  fn_mm_array_update(
    new.room_id, old.room_id, 'mm_person_room' :: character varying, 
    'person_id' :: character varying, 
    'room_id' :: character varying, old.id
  ) AS fn_mm_array_update;
SELECT 
  fn_mm_array_update(
    new.research_group_id, old.research_group_id, 
    'mm_person_research_group' :: character varying, 
    'person_id' :: character varying, 
    'research_group_id' :: character varying, 
    old.id
  ) AS fn_mm_array_update;
SELECT 
  fn_mm_array_update(
    new.nationality_id, old.nationality_id, 
    'mm_person_nationality' :: character varying, 
    'person_id' :: character varying, 
    'nationality_id' :: character varying, 
    old.id
  ) AS fn_mm_array_update;
);

-- People/COS_View
CREATE OR REPLACE FUNCTION hotwire3.people_cos_view_trig()
  RETURNS trigger AS
$BODY$
begin
    if new.id is not null then
	update person set
            surname = new.surname,
            image_oid = new.image_oid::bigint,
            image_lo = new.image_oid,
            first_names = new.first_names,
            title_id = new.title_id,
            name_suffix = new.name_suffix,
            email_address = new.email_address,
            crsid = new.crsid,
            date_of_birth = new.date_of_birth,
            location = new.location,
            do_not_show_on_website = new.hide_person_from_website,
            hide_email = new.hide_email_from_website,
            is_spri = new.is_spri,
            override_auto_ban = new.override_auto_ban
        where person.id = new.id;
    else
        new.id := nextval('person_id_seq');
        insert into person (
            id,
            surname,
            image_oid,
            image_lo,
            first_names,
            title_id,
            name_suffix,
            email_address,
            crsid,
            date_of_birth,
            location,
            do_not_show_on_website,
            hide_email,
            is_spri,
            override_auto_ban
        ) values (
            new.id,
            new.surname,
            new.image_oid::bigint,
            new.image_oid,
            new.first_names,
            new.title_id,
            new.name_suffix,
            new.email_address,
            new.crsid,
            new.date_of_birth,
            new.location,
            coalesce(new.hide_person_from_website,'f'),
            new.hide_email_from_website,
            new.is_spri,
            new.override_auto_ban
        );
    end if;
    perform fn_mm_array_update(new.dept_telephone_number_id,
                               'mm_person_dept_telephone_number'::varchar,
			       'person_id'::varchar,
                               'dept_telephone_number_id'::varchar,
                               new.id);

    perform fn_mm_array_update(new.research_group_id,
                               'mm_person_research_group'::varchar,
			       'person_id'::varchar,
                               'research_group_id'::varchar,
                               new.id);

    perform fn_mm_array_update(new.room_id,
                               'mm_person_room'::varchar,
                               'person_id'::varchar,
                               'room_id'::varchar,
                               new.id);
    return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.people_cos_view_trig()
  OWNER TO dev;

-- People/Personnel_Data_Entry
CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry()
  RETURNS trigger AS
$BODY$
declare 
	d_do_not_show_on_website boolean := 'f';
	d_hide_phone_no_from_website boolean := 'f';
	on_retired_staff_list bigint;
	retired_staff_list_id bigint;

begin
	if NEW.id is not null 
	then
		update person set
			surname = NEW.surname,
			image_oid = NEW.image_oid::bigint,
			image_lo = NEW.image_oid,
			first_names = NEW.first_names, 
			title_id = NEW.title_id, 
			known_as = NEW.known_as,
			name_suffix = NEW.name_suffix,
			previous_surname = NEW.previous_surname,
			date_of_birth = NEW.date_of_birth, 
			gender_id = NEW.gender_id, 
			crsid = NEW.crsid,
			email_address = NEW.email_address, 
			hide_email = NEW.hide_email, 
			do_not_show_on_website = COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
			hide_phone_no_from_website = COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
			arrival_date = NEW.arrival_date,
			leaving_date = NEW.leaving_date,
			left_but_no_leaving_date_given = NEW.left_but_no_leaving_date_given,
			cambridge_address = NEW.home_address::varchar(500),
			cambridge_phone_number = NEW.home_phone_number,
			cambridge_college_id = NEW.cambridge_college_id,
			mobile_number = NEW.mobile_number,
			emergency_contact = NEW.emergency_contact::varchar(500), 
			location = NEW.location,
			other_information = NEW.other_information,
			notes = NEW.notes,
			forwarding_address = NEW.forwarding_address,
			new_employer_address = NEW.new_employer_address, 
			chem_at_cam = NEW."chem_@_cam",
			continuous_employment_start_date = NEW.continuous_employment_start_date,
			paper_file_details = NEW.paper_file_status, 
			registration_completed = NEW.registration_completed,
			clearance_cert_signed = NEW.clearance_cert_signed,
			counts_as_academic=NEW.treat_as_academic_staff
		where person.id = OLD.id;
	else
                NEW.id := nextval('person_id_seq');
		insert into person (
                                id,
				surname,
				image_oid, 
				image_lo, 
				first_names, 
				title_id, 
				known_as,
				name_suffix,
				previous_surname, 
				date_of_birth, 
				gender_id, 
				crsid,
				email_address, 
				hide_email, 
				do_not_show_on_website,
				arrival_date,
				leaving_date,
				cambridge_address,
				cambridge_phone_number, 
				cambridge_college_id,
				mobile_number,
				emergency_contact, 
				location,
				other_information,
				notes,
				forwarding_address,
				new_employer_address, 
				chem_at_cam,
				continuous_employment_start_date,
				paper_file_details, 
				registration_completed,
				clearance_cert_signed,
				counts_as_academic,
				hide_phone_no_from_website

			) values (
                                NEW.id,
				NEW.surname, 
				NEW.image_oid::bigint,
				NEW.image_oid,
				NEW.first_names, 
				NEW.title_id, 
				NEW.known_as,
				NEW.name_suffix,
				NEW.previous_surname,
				NEW.date_of_birth, 
				NEW.gender_id, 
				NEW.crsid,
				NEW.email_address, 
				NEW.hide_email, 
				COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
				NEW.arrival_date,
				NEW.leaving_date,
				NEW.home_address::varchar(500),
				NEW.home_phone_number, 
				NEW.cambridge_college_id,
				NEW.mobile_number,
				NEW.emergency_contact::varchar(500), 
				NEW.location,
				NEW.other_information,
				NEW.notes,
				NEW.forwarding_address,
				NEW.new_employer_address, 
				NEW."chem_@_cam",
				NEW.continuous_employment_start_date,
				NEW.paper_file_status, 
				NEW.registration_completed,
				NEW.clearance_cert_signed,
				NEW.treat_as_academic_staff,
				COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website)

			) ;
	end if;

	-- do retired staff list update
	select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
	select include_person_id from mm_mailinglist_include_person where include_person_id = NEW.id and mailinglist_id = retired_staff_list_id into on_retired_staff_list;
	if NEW.retired_staff_mailing_list = 't' and on_retired_staff_list is null
	then
	   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, NEW.id); 
	end if;
	if NEW.retired_staff_mailing_list = 'f' and on_retired_staff_list is not null
	then
	   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = NEW.id; 
	end if;

	-- many-many updates here
	perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, NEW.id);

	
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.personnel_data_entry()
  OWNER TO dev;

-- People/Personnel_Phone
CREATE OR REPLACE RULE hotwire3_view_personnel_phone_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_Phone" DO INSTEAD ( UPDATE person SET image_oid = new.image::bigint, image_lo = NEW.image, surname = new.surname, first_names = new.first_names, location = new.location, external_work_phone_numbers = new.external_work_numbers, email_address = new.email_address, is_external = new.is_external
  WHERE person.id = old.id;
 SELECT fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update;
);

-- People/Photography_Registration
CREATE OR REPLACE FUNCTION hotwire3.photography_registration_update()
  RETURNS trigger AS
$BODY$
	begin
		if NEW.id is not null
		then
			update person set
				gender_id = NEW.gender_id,
				first_names = NEW.first_names,
				known_as = NEW.known_as,
				surname = NEW.surname,
				date_of_birth = NEW.date_of_birth,
				image_oid = NEW.image::bigint,
                                image_lo = NEW.image
			where person.id = NEW.id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
				image_oid,
                                image_lo
			) values (
				NEW.gender_id,
				NEW.first_names,
				NEW.known_as,
				NEW.surname,
				NEW.date_of_birth,
				NEW.image::bigint,
                                NEW.image
			) returning id into NEW.id;
		end if;
		perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);	
		return NEW;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.photography_registration_update()
  OWNER TO dev;
