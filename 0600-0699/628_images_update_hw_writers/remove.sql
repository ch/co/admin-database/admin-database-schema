-- People/Personnel_Data_Entry
CREATE OR REPLACE FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_oid=(select (v.image_oid).val from (select v.image_oid) as x), 
                                image_lo=(select (v.image_oid).val from (select v.image_oid) as x), 
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as::varchar(80),
                                name_suffix = v.name_suffix,
                                previous_surname = v.previous_surname::varchar(80), 
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                cambridge_address = v.home_address,
                                cambridge_phone_number = v.home_phone_number::varchar(80), 
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number::varchar(80),
                                emergency_contact = v.emergency_contact, 
                                location = v.location::varchar(80),
                                other_information = v.other_information::text,
                                notes = v.notes::text,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                chem_at_cam = v."chem_@_cam",
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_status::varchar(80), 
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                        image_oid, 
                                        image_lo, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        previous_surname, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        chem_at_cam,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        (select (v.image_oid).val from (select v.image_oid) as x),
                                        (select (v.image_oid).val from (select v.image_oid) as x),
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as::varchar(80),
                                        v.name_suffix,
                                        v.previous_surname::varchar(80), 
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.home_address,
                                        v.home_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number::varchar(80),
                                        v.emergency_contact, 
                                        v.location::varchar(80),
                                        v.other_information::text,
                                        v.notes::text,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v."chem_@_cam",
                                        v.continuous_employment_start_date,
                                        v.paper_file_status::varchar(80), 
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire.personnel_data_entry_upd(hotwire."10_View/People/Personnel_Data_Entry")
  OWNER TO dev;

-- People/Photography_Registration
CREATE OR REPLACE FUNCTION hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  RETURNS bigint AS
$BODY$
	declare v alias for $1;
		v_person_id bigint;
	begin
		if v.id is not null
		then
			v_person_id = v.id;
			update person set
				gender_id = v.gender_id,
				first_names = v.first_names,
				known_as = v.known_as,
				surname = v.surname,
				date_of_birth = v.date_of_birth,
				image_oid = ( select (v.photo).val as val from ( select v.photo ) x) ,
				image_lo = ( select (v.photo).val as val from ( select v.photo ) x) 
			where person.id = v_person_id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
				image_oid,
                                image_lo
			) values (
				v.gender_id,
				v.first_names,
				v.known_as,
				v.surname,
				v.date_of_birth,
				( select (v.photo).val as val from ( select v.photo ) x),
				( select (v.photo).val as val from ( select v.photo ) x)
			) returning id into v_person_id;
		end if;
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);	
		return v_person_id;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  OWNER TO dev;

-- People/Personnel_Basic
CREATE OR REPLACE FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    		v_person_id BIGINT;                                                                                        
      begin                                                                                                              
      IF v.id IS NOT NULL THEN                                                                                           
        v_person_id= v.id;                                                                                               
	UPDATE person SET
		surname=v.surname,
		image_lo=(select (v.image_oid).val from (select v.image_oid) as x),
		image_oid=(select (v.image_oid).val from (select v.image_oid) as x),
	        first_names=v.first_names,
		title_id=v.title_id,                                                       
		name_suffix = v.name_suffix,
		email_address=v.email_address, 
		crsid=v.crsid,
		date_of_birth=v.date_of_birth,
		location=v.location,
		is_spri=v.is_spri                                                                        
        WHERE person.id = v.id;                                                                                          
      ELSE    
	INSERT INTO person (surname, image_lo, image_oid, first_names, title_id, name_suffix, email_address,crsid, date_of_birth, location, is_spri) VALUES (v.surname, (select (v.image_oid).val from (select v.image_oid) as x), (select (v.image_oid).val from (select v.image_oid) as x), v.first_names, v.title_id, v.name_suffix, v.email_address,v.crsid, v.date_of_birth, v.location, v.is_spri) returning id into v_person_id; 
    END IF; 

 perform fn_mm_array_update(v.dept_telephone_number_id,
                            'mm_person_dept_telephone_number'::varchar,
			    'person_id'::varchar,
                            'dept_telephone_number_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.research_group_id,
                            'mm_person_research_group'::varchar,
			    'person_id'::varchar,
                            'research_group_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.room_id,
                            'mm_person_room'::varchar,
                            'person_id'::varchar,
                            'room_id'::varchar,
                            v_person_id);
                                               
      return v_person_id;                                                                                                
      end;                                                

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  OWNER TO dev;

