CREATE OR REPLACE VIEW hotwire3._preferences AS 
WITH all_prefs AS (
    SELECT 
        "User Preferences".preference_id,
        "User Preferences".preference_value,
        1 as priority
    FROM hotwire3."hw_User Preferences" "User Preferences"
    WHERE "User Preferences".user_id IS NULL
UNION ALL
    SELECT 
        "User Preferences".preference_id,
        "User Preferences".preference_value,
        100 as priority
    FROM pg_auth_members m
    JOIN pg_roles b ON m.roleid = b.oid
    JOIN pg_roles r ON m.member = r.oid
    JOIN hotwire3."hw_User Preferences" "User Preferences" ON b.oid = "User Preferences".user_id::oid
    WHERE r.rolname = "current_user"()
UNION ALL
    SELECT
        "User Preferences".preference_id,
        "User Preferences".preference_value,
        200 as priority
    FROM pg_roles
    JOIN hotwire3."hw_User Preferences" "User Preferences" ON pg_roles.oid = "User Preferences".user_id::oid
    WHERE pg_roles.rolname = "current_user"()
    )
SELECT
    "Preferences".hw_preference_const AS preference_name,
    all_prefs.preference_value,
    hw_preference_type_hid.hw_preference_type_hid AS preference_type
FROM all_prefs
JOIN ( 
    SELECT
        preference_id,
        max(priority) as priority
    FROM all_prefs
    GROUP BY preference_id
) max_pref ON all_prefs.priority = max_pref.priority AND all_prefs.preference_id = max_pref.preference_id
JOIN hotwire3."hw_Preferences" "Preferences" ON all_prefs.preference_id = "Preferences".id
JOIN hotwire3.hw_preference_type_hid USING (hw_preference_type_id)
;

ALTER TABLE hotwire3._preferences OWNER TO dev;
GRANT ALL ON TABLE hotwire3._preferences TO dev;
GRANT SELECT ON TABLE hotwire3._preferences TO public;

