DROP VIEW hotwire3.exclude_person_hid;

CREATE OR REPLACE VIEW hotwire3.exclude_person_hid AS 
SELECT person_hid.person_id AS exclude_person_id, person_hid.person_hid AS exclude_person_hid
   FROM hotwire3.person_hid;

ALTER TABLE hotwire3.exclude_person_hid OWNER TO dev;
GRANT ALL ON TABLE hotwire3.exclude_person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.exclude_person_hid TO cos;

DROP VIEW hotwire3.include_person_hid;

CREATE OR REPLACE VIEW hotwire3.include_person_hid AS 
SELECT person_hid.person_id AS include_person_id, person_hid.person_hid AS include_person_hid
   FROM hotwire3.person_hid;

ALTER TABLE hotwire3.include_person_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.include_person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.include_person_hid TO cos;

DROP VIEW hotwire3.member_hid;

CREATE OR REPLACE VIEW hotwire3.member_hid AS 
SELECT person_hid.person_id AS member_id, person_hid.person_hid AS member_hid
   FROM hotwire3.person_hid;

ALTER TABLE hotwire3.member_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.member_hid TO dev;
GRANT SELECT ON TABLE hotwire3.member_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.member_hid TO headsofgroup;
GRANT SELECT ON TABLE hotwire3.member_hid TO groupitreps;

