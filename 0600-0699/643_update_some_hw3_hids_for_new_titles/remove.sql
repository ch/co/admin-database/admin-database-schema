DROP VIEW hotwire3.exclude_person_hid;

CREATE OR REPLACE VIEW hotwire3.exclude_person_hid AS 
 SELECT person.id AS exclude_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS exclude_person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE hotwire3.exclude_person_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.exclude_person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.exclude_person_hid TO cos;

DROP VIEW hotwire3.include_person_hid;

CREATE OR REPLACE VIEW hotwire3.include_person_hid AS 
 SELECT person.id AS include_person_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS include_person_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE hotwire3.include_person_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.include_person_hid TO dev;
GRANT SELECT ON TABLE hotwire3.include_person_hid TO cos;

DROP VIEW hotwire3.member_hid;

CREATE OR REPLACE VIEW hotwire3.member_hid AS 
 SELECT person.id AS member_id, ((person.surname::text || ', '::text) || COALESCE(title_hid.title_hid::text || ' '::text, ''::text)) || COALESCE(person.known_as, person.first_names)::text AS member_hid
   FROM person
   LEFT JOIN title_hid USING (title_id)
  ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(title_hid.title_hid::text || ' '::text, ''::text);

ALTER TABLE hotwire3.member_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.member_hid TO dev;
GRANT SELECT ON TABLE hotwire3.member_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.member_hid TO headsofgroup;
GRANT SELECT ON TABLE hotwire3.member_hid TO groupitreps;

