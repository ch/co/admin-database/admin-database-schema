CREATE OR REPLACE VIEW hotwire3._role_data AS 
 SELECT member.rolname AS member, role.rolname AS role
   FROM pg_roles role
   LEFT JOIN pg_auth_members ON role.oid = pg_auth_members.roleid
   RIGHT JOIN pg_roles member ON member.oid = pg_auth_members.member;

ALTER TABLE hotwire3._role_data OWNER TO dev;
GRANT SELECT ON TABLE hotwire3._role_data TO public;

CREATE OR REPLACE VIEW hotwire._role_data AS 
 SELECT member.rolname AS member, role.rolname AS role
   FROM pg_roles role
   LEFT JOIN pg_auth_members ON role.oid = pg_auth_members.roleid
   RIGHT JOIN pg_roles member ON member.oid = pg_auth_members.member;

ALTER TABLE hotwire._role_data OWNER TO dev;
GRANT ALL ON TABLE hotwire._role_data TO dev;
GRANT ALL ON TABLE hotwire._role_data TO public;
GRANT ALL ON TABLE hotwire._role_data TO cos;

