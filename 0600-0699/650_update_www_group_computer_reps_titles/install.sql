CREATE OR REPLACE VIEW www.group_computer_reps AS 
 SELECT (((title_hid.abbrev_title::text || ' '::text) || person.first_names::text) || ' '::text) || person.surname::text AS name, person.crsid, person.email_address, mm_research_group_computer_rep.research_group_id
   FROM mm_research_group_computer_rep
   JOIN person ON mm_research_group_computer_rep.computer_rep_id = person.id
   JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
   JOIN title_hid ON person.title_id = title_hid.title_id
  WHERE _physical_status_v3.status_id::text = 'Current'::text OR person.is_spri = true;

ALTER TABLE www.group_computer_reps
  OWNER TO dev;
GRANT ALL ON TABLE www.group_computer_reps TO dev;
GRANT SELECT ON TABLE www.group_computer_reps TO public;

