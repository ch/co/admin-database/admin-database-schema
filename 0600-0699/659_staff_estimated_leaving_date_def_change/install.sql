-- v11


CREATE OR REPLACE VIEW _all_roles_v11 AS
SELECT
    role_data.person_id,
    role_data.start_date,
    role_data.intended_end_date,
    role_data.estimated_leaving_date,
    role_data.end_date,
    role_data.funding_end_date,
    role_data.supervisor_id,
    role_data.co_supervisor_id,
    role_data.mentor_id,
    role_data.post_category_id,
    role_data.post_category,
    CASE WHEN role_data.force_role_status_to_past = TRUE THEN
        'Past'::character varying(10)
    WHEN role_data.end_date < 'now'::text::date THEN
        'Past'::character varying(10)
    WHEN role_data.funding_end_date < 'now'::text::date THEN
        'Past'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.end_date IS NULL THEN
        'Current'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.intended_end_date >= 'now'::text::date THEN
        'Current'::character varying(10)
    WHEN role_data.start_date > 'now'::text::date THEN
        'Future'::character varying(10)
    ELSE
        'Unknown'::character varying(10)
    END AS status,
    role_data.funding,
    role_data.fees_funding,
    role_data.research_grant_number,
    role_data.paid_by_university,
    role_data.hesa_leaving_code,
    role_data.chem,
    role_data.job_title,
    role_data.role_id,
    role_data.role_tablename,
    role_data.role_target_viewname,
    COALESCE((role_data.start_date - '1900-01-01'::date) * 10000 + COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, 'now'::text::date) - role_data.start_date, 0), 0) AS weight,
    COALESCE(role_data.start_date, '1900-01-01'::date) AS fixed_start_date,
    COALESCE(COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, now()::date) - role_data.start_date, 0), 0) AS length_of_role
FROM ((((
                SELECT
                    visitorship.person_id,
                    visitorship.start_date,
                    visitorship.intended_end_date,
                    visitorship.intended_end_date AS estimated_leaving_date,
                    visitorship.end_date,
                    NULL::date AS funding_end_date,
                    visitorship.host_person_id AS supervisor_id,
                    NULL::bigint AS co_supervisor_id,
                    NULL::bigint AS mentor_id,
                    'v-'::text || visitorship.visitor_type_id::text AS post_category_id,
                    visitor_type_hid.visitor_type_hid AS post_category,
                    visitorship.funding,
                    NULL::character varying(500) AS fees_funding,
                    NULL::character varying(500) AS research_grant_number,
                    NULL::boolean AS paid_by_university,
                    NULL::character varying(2) AS hesa_leaving_code,
                    FALSE AS chem,
                    NULL::character varying(120) AS job_title,
                    visitorship.id AS role_id,
                    visitorship.force_role_status_to_past,
                    'visitorship'::text AS role_tablename,
                    '10_View/Roles/Visitors'::text AS role_target_viewname
                FROM
                    visitorship
                LEFT JOIN visitor_type_hid USING (visitor_type_id)
        UNION
        SELECT
            part_iii_studentship.person_id, part_iii_studentship.start_date, part_iii_studentship.intended_end_date, part_iii_studentship.intended_end_date AS estimated_leaving_date,
            part_iii_studentship.end_date,
            NULL::date AS funding_end_date,
            part_iii_studentship.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'p3-1'::text AS post_category_id,
            'Part III'::character varying(80) AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            NULL::character varying(2) AS hesa_leaving_code,
            FALSE AS chem,
            NULL::character varying(120) AS job_title,
            part_iii_studentship.id AS role_id,
            part_iii_studentship.force_role_status_to_past,
            'part_iii_studentship'::text AS role_tablename,
            '10_View/Roles/Part_III_Students'::text AS role_target_viewname
        FROM
            part_iii_studentship)
    UNION
    SELECT
        erasmus_socrates_studentship.person_id,
        erasmus_socrates_studentship.start_date,
        erasmus_socrates_studentship.intended_end_date,
        erasmus_socrates_studentship.intended_end_date AS estimated_leaving_date,
        erasmus_socrates_studentship.end_date,
        NULL::date AS funding_end_date,
        erasmus_socrates_studentship.supervisor_id,
        NULL::bigint AS co_supervisor_id,
        NULL::bigint AS mentor_id,
        'e-'::text || erasmus_socrates_studentship.erasmus_type_id AS post_category_id,
        erasmus_type_hid.erasmus_type_hid AS post_category,
        NULL::character varying(500) AS funding,
        NULL::character varying(500) AS fees_funding,
        NULL::character varying(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        NULL::character varying(2) AS hesa_leaving_code,
        FALSE AS chem,
        NULL::character varying(120) AS job_title,
        erasmus_socrates_studentship.id AS role_id,
        erasmus_socrates_studentship.force_role_status_to_past,
        'erasmus_socrates_studentship'::text AS role_tablename,
        '10_View/Roles/Erasmus_Students'::text AS role_target_viewname
    FROM
        erasmus_socrates_studentship
        JOIN erasmus_type_hid USING (erasmus_type_id))
UNION
SELECT
    post_history.person_id,
    post_history.start_date,
    post_history.intended_end_date,
--    CASE WHEN post_history.funding_end_date > post_history.intended_end_date THEN
--        post_history.funding_end_date
--    ELSE
--        COALESCE(post_history.intended_end_date, post_history.funding_end_date)
--    END AS estimated_leaving_date,
    (
        SELECT 
            min(end_date)
        FROM ( 
                SELECT
                    id,
                    end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    intended_end_date AS end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    funding_end_date AS end_date
                FROM post_history
            GROUP BY id
        ) closest_date
        WHERE closest_date.id = post_history.id 
    ) AS estimated_leaving_date,
    post_history.end_date,
    post_history.funding_end_date,
    post_history.supervisor_id,
    NULL::bigint AS co_supervisor_id,
    post_history.mentor_id,
    'sc-'::text || post_history.staff_category_id::text AS post_category_id,
    staff_category.category AS post_category,
    post_history.filemaker_funding AS funding,
    NULL::character varying(500) AS fees_funding,
    post_history.research_grant_number,
    post_history.paid_by_university,
    NULL::character varying(2) AS hesa_leaving_code,
    post_history.chem,
    post_history.job_title,
    post_history.id AS role_id,
    post_history.force_role_status_to_past,
    'post_history'::text AS role_tablename,
    '10_View/Roles/Post_History'::text AS role_target_viewname
FROM
    post_history
    LEFT JOIN staff_category ON post_history.staff_category_id = staff_category.id)
UNION
SELECT
    postgraduate_studentship.person_id,
    postgraduate_studentship.start_date,
    postgraduate_studentship.intended_end_date,
    CASE WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        CASE WHEN postgraduate_studentship.end_of_registration_date IS NULL THEN
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.start_date + '4 years'::interval + '3 mons'::interval)::date)
        ELSE
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.end_of_registration_date + '3 mons'::interval)::date)
        END
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.msc_date_submission_due)
    ELSE
        NULL::date
    END AS estimated_leaving_date,
    CASE WHEN postgraduate_studentship.force_role_status_to_past = TRUE THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.date_withdrawn_from_register, 'now'::text::date - 1)
    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    ELSE
        NULL::date
    END AS end_date,
    postgraduate_studentship.funding_end_date,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
    postgraduate_studentship.first_mentor_id AS mentor_id,
    'pg-'::text || postgraduate_studentship.postgraduate_studentship_type_id::text AS post_category_id,
    postgraduate_studentship_type.name AS post_category,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.filemaker_fees_funding AS fees_funding,
    NULL::character varying(500) AS research_grant_number,
    postgraduate_studentship.paid_through_payroll AS paid_by_university,
    NULL::character varying(2) AS hesa_leaving_code,
    TRUE AS chem,
    NULL::character varying(120) AS job_title,
    postgraduate_studentship.id AS role_id,
    postgraduate_studentship.force_role_status_to_past,
    'postgraduate_studentship'::text AS role_tablename,
    '10_View/Roles/Postgrad_Students'::text AS role_target_viewname
FROM
    postgraduate_studentship
    LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
    LEFT JOIN person ON person.id = postgraduate_studentship.person_id) role_data;

ALTER TABLE _all_roles_v11 OWNER TO cen1001;

GRANT ALL ON TABLE _all_roles_v11 TO cen1001;

GRANT SELECT ON TABLE _all_roles_v11 TO dev;


-- v12

CREATE OR REPLACE VIEW _all_roles_v12 AS
SELECT
    role_data.person_id,
    role_data.start_date,
    role_data.intended_end_date,
    role_data.estimated_leaving_date,
    role_data.end_date,
    role_data.funding_end_date,
    role_data.supervisor_id,
    role_data.co_supervisor_id,
    role_data.mentor_id,
    role_data.post_category_id,
    role_data.post_category,
    CASE WHEN role_data.force_role_status_to_past = TRUE THEN
        'Past'::character varying(10)
    WHEN role_data.end_date < 'now'::text::date THEN
        'Past'::character varying(10)
    WHEN role_data.funding_end_date < 'now'::text::date
        AND role_data.role_tablename <> 'postgraduate_studentship'::text THEN
        'Past'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.end_date IS NULL THEN
        'Current'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.intended_end_date >= 'now'::text::date THEN
        'Current'::character varying(10)
    WHEN role_data.start_date > 'now'::text::date THEN
        'Future'::character varying(10)
    ELSE
        'Unknown'::character varying(10)
    END AS status,
    role_data.funding,
    role_data.fees_funding,
    role_data.research_grant_number,
    role_data.paid_by_university,
    role_data.hesa_leaving_code,
    role_data.chem,
    role_data.job_title,
    role_data.role_id,
    role_data.role_tablename,
    role_data.role_target_viewname,
    COALESCE((role_data.start_date - '1900-01-01'::date) * 10000 + COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, 'now'::text::date) - role_data.start_date, 0), 0) AS weight,
    COALESCE(role_data.start_date, '1900-01-01'::date) AS fixed_start_date,
    COALESCE(COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, now()::date) - role_data.start_date, 0), 0) AS length_of_role
FROM ((((
                SELECT
                    visitorship.person_id,
                    visitorship.start_date,
                    visitorship.intended_end_date,
                    visitorship.intended_end_date AS estimated_leaving_date,
                    visitorship.end_date,
                    NULL::date AS funding_end_date,
                    visitorship.host_person_id AS supervisor_id,
                    NULL::bigint AS co_supervisor_id,
                    NULL::bigint AS mentor_id,
                    'v-'::text || visitorship.visitor_type_id::text AS post_category_id,
                    visitor_type_hid.visitor_type_hid AS post_category,
                    visitorship.funding,
                    NULL::character varying(500) AS fees_funding,
                    NULL::character varying(500) AS research_grant_number,
                    NULL::boolean AS paid_by_university,
                    NULL::character varying(2) AS hesa_leaving_code,
                    FALSE AS chem,
                    NULL::character varying(120) AS job_title,
                    visitorship.id AS role_id,
                    visitorship.force_role_status_to_past,
                    'visitorship'::text AS role_tablename,
                    '10_View/Roles/Visitors'::text AS role_target_viewname
                FROM
                    visitorship
                LEFT JOIN visitor_type_hid USING (visitor_type_id)
        UNION
        SELECT
            part_iii_studentship.person_id, part_iii_studentship.start_date, part_iii_studentship.intended_end_date, part_iii_studentship.intended_end_date AS estimated_leaving_date,
            part_iii_studentship.end_date,
            NULL::date AS funding_end_date,
            part_iii_studentship.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'p3-1'::text AS post_category_id,
            'Part III'::character varying(80) AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            NULL::character varying(2) AS hesa_leaving_code,
            FALSE AS chem,
            NULL::character varying(120) AS job_title,
            part_iii_studentship.id AS role_id,
            part_iii_studentship.force_role_status_to_past,
            'part_iii_studentship'::text AS role_tablename,
            '10_View/Roles/Part_III_Students'::text AS role_target_viewname
        FROM
            part_iii_studentship)
    UNION
    SELECT
        erasmus_socrates_studentship.person_id,
        erasmus_socrates_studentship.start_date,
        erasmus_socrates_studentship.intended_end_date,
        erasmus_socrates_studentship.intended_end_date AS estimated_leaving_date,
        erasmus_socrates_studentship.end_date,
        NULL::date AS funding_end_date,
        erasmus_socrates_studentship.supervisor_id,
        NULL::bigint AS co_supervisor_id,
        NULL::bigint AS mentor_id,
        'e-'::text || erasmus_socrates_studentship.erasmus_type_id AS post_category_id,
        erasmus_type_hid.erasmus_type_hid AS post_category,
        NULL::character varying(500) AS funding,
        NULL::character varying(500) AS fees_funding,
        NULL::character varying(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        NULL::character varying(2) AS hesa_leaving_code,
        FALSE AS chem,
        NULL::character varying(120) AS job_title,
        erasmus_socrates_studentship.id AS role_id,
        erasmus_socrates_studentship.force_role_status_to_past,
        'erasmus_socrates_studentship'::text AS role_tablename,
        '10_View/Roles/Erasmus_Students'::text AS role_target_viewname
    FROM
        erasmus_socrates_studentship
        JOIN erasmus_type_hid USING (erasmus_type_id))
UNION
SELECT
    post_history.person_id,
    post_history.start_date,
    post_history.intended_end_date,
--    CASE WHEN post_history.funding_end_date > post_history.intended_end_date THEN
--        post_history.funding_end_date
--    ELSE
--        COALESCE(post_history.intended_end_date, post_history.funding_end_date)
--    END AS estimated_leaving_date,
    (
        SELECT 
            min(end_date)
        FROM ( 
                SELECT
                    id,
                    end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    intended_end_date AS end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    funding_end_date AS end_date
                FROM post_history
            GROUP BY id
        ) closest_date
        WHERE closest_date.id = post_history.id 
    ) AS estimated_leaving_date,
    post_history.end_date,
    post_history.funding_end_date,
    post_history.supervisor_id,
    NULL::bigint AS co_supervisor_id,
    post_history.mentor_id,
    'sc-'::text || post_history.staff_category_id::text AS post_category_id,
    staff_category.category AS post_category,
    post_history.filemaker_funding AS funding,
    NULL::character varying(500) AS fees_funding,
    post_history.research_grant_number,
    post_history.paid_by_university,
    NULL::character varying(2) AS hesa_leaving_code,
    post_history.chem,
    post_history.job_title,
    post_history.id AS role_id,
    post_history.force_role_status_to_past,
    'post_history'::text AS role_tablename,
    '10_View/Roles/Post_History'::text AS role_target_viewname
FROM
    post_history
    LEFT JOIN staff_category ON post_history.staff_category_id = staff_category.id)
UNION
SELECT
    postgraduate_studentship.person_id,
    postgraduate_studentship.start_date,
    postgraduate_studentship.intended_end_date,
    CASE WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        CASE WHEN postgraduate_studentship.end_of_registration_date IS NULL THEN
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.start_date + '4 years'::interval + '3 mons'::interval)::date)
        ELSE
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.end_of_registration_date + '3 mons'::interval)::date)
        END
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.msc_date_submission_due)
    ELSE
        NULL::date
    END AS estimated_leaving_date,
    CASE WHEN postgraduate_studentship.force_role_status_to_past = TRUE THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.date_withdrawn_from_register, 'now'::text::date - 1)
    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    ELSE
        NULL::date
    END AS end_date,
    postgraduate_studentship.funding_end_date,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
    postgraduate_studentship.first_mentor_id AS mentor_id,
    'pg-'::text || postgraduate_studentship.postgraduate_studentship_type_id::text AS post_category_id,
    postgraduate_studentship_type.name AS post_category,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.filemaker_fees_funding AS fees_funding,
    NULL::character varying(500) AS research_grant_number,
    postgraduate_studentship.paid_through_payroll AS paid_by_university,
    NULL::character varying(2) AS hesa_leaving_code,
    TRUE AS chem,
    NULL::character varying(120) AS job_title,
    postgraduate_studentship.id AS role_id,
    postgraduate_studentship.force_role_status_to_past,
    'postgraduate_studentship'::text AS role_tablename,
    '10_View/Roles/Postgrad_Students'::text AS role_target_viewname
FROM
    postgraduate_studentship
    LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
    LEFT JOIN person ON person.id = postgraduate_studentship.person_id) role_data;

ALTER TABLE _all_roles_v12 OWNER TO dev;

GRANT ALL ON TABLE _all_roles_v12 TO dev;

GRANT ALL ON TABLE _all_roles_v12 TO cen1001;


-- v13

CREATE OR REPLACE VIEW _all_roles_v13 AS
SELECT
    role_data.person_id,
    role_data.start_date,
    role_data.intended_end_date,
    role_data.estimated_leaving_date,
    role_data.end_date,
    role_data.funding_end_date,
    role_data.supervisor_id,
    role_data.co_supervisor_id,
    role_data.mentor_id,
    role_data.post_category_id,
    role_data.post_category,
    CASE WHEN role_data.force_role_status_to_past = TRUE THEN
        'Past'::character varying(10)
    WHEN role_data.end_date < 'now'::text::date THEN
        'Past'::character varying(10)
    WHEN role_data.funding_end_date < 'now'::text::date
        AND role_data.role_tablename <> 'postgraduate_studentship'::text THEN
        'Past'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.end_date IS NULL THEN
        'Current'::character varying(10)
    WHEN role_data.start_date <= 'now'::text::date
        AND role_data.intended_end_date >= 'now'::text::date THEN
        'Current'::character varying(10)
    WHEN role_data.start_date > 'now'::text::date THEN
        'Future'::character varying(10)
    ELSE
        'Unknown'::character varying(10)
    END AS status,
    role_data.funding,
    role_data.fees_funding,
    role_data.research_grant_number,
    role_data.paid_by_university,
    role_data.chem,
    role_data.job_title,
    role_data.role_id,
    role_data.role_tablename,
    role_data.role_target_viewname,
    COALESCE((role_data.start_date - '1900-01-01'::date) * 10000 + COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, 'now'::text::date) - role_data.start_date, 0), 0) AS weight,
    COALESCE(role_data.start_date, '1900-01-01'::date) AS fixed_start_date,
    COALESCE(COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, now()::date) - role_data.start_date, 0), 0) AS length_of_role
FROM ((((
                SELECT
                    visitorship.person_id,
                    visitorship.start_date,
                    visitorship.intended_end_date,
                    visitorship.intended_end_date AS estimated_leaving_date,
                    visitorship.end_date,
                    NULL::date AS funding_end_date,
                    visitorship.host_person_id AS supervisor_id,
                    NULL::bigint AS co_supervisor_id,
                    NULL::bigint AS mentor_id,
                    'v-'::text || visitorship.visitor_type_id::text AS post_category_id,
                    visitor_type_hid.visitor_type_hid AS post_category,
                    NULL::character varying(500) AS funding,
                    NULL::character varying(500) AS fees_funding,
                    NULL::character varying(500) AS research_grant_number,
                    NULL::boolean AS paid_by_university,
                    FALSE AS chem,
                    NULL::character varying(120) AS job_title,
                    visitorship.id AS role_id,
                    visitorship.force_role_status_to_past,
                    'visitorship'::text AS role_tablename,
                    '10_View/Roles/Visitors'::text AS role_target_viewname
                FROM
                    visitorship
                LEFT JOIN visitor_type_hid USING (visitor_type_id)
        UNION
        SELECT
            part_iii_studentship.person_id, part_iii_studentship.start_date, part_iii_studentship.intended_end_date, part_iii_studentship.intended_end_date AS estimated_leaving_date,
            part_iii_studentship.end_date,
            NULL::date AS funding_end_date,
            part_iii_studentship.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'p3-1'::text AS post_category_id,
            'Part III'::character varying(80) AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            FALSE AS chem,
            NULL::character varying(120) AS job_title,
            part_iii_studentship.id AS role_id,
            part_iii_studentship.force_role_status_to_past,
            'part_iii_studentship'::text AS role_tablename,
            '10_View/Roles/Part_III_Students'::text AS role_target_viewname
        FROM
            part_iii_studentship)
    UNION
    SELECT
        erasmus_socrates_studentship.person_id,
        erasmus_socrates_studentship.start_date,
        erasmus_socrates_studentship.intended_end_date,
        erasmus_socrates_studentship.intended_end_date AS estimated_leaving_date,
        erasmus_socrates_studentship.end_date,
        NULL::date AS funding_end_date,
        erasmus_socrates_studentship.supervisor_id,
        NULL::bigint AS co_supervisor_id,
        NULL::bigint AS mentor_id,
        'e-'::text || erasmus_socrates_studentship.erasmus_type_id AS post_category_id,
        erasmus_type_hid.erasmus_type_hid AS post_category,
        NULL::character varying(500) AS funding,
        NULL::character varying(500) AS fees_funding,
        NULL::character varying(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        FALSE AS chem,
        NULL::character varying(120) AS job_title,
        erasmus_socrates_studentship.id AS role_id,
        erasmus_socrates_studentship.force_role_status_to_past,
        'erasmus_socrates_studentship'::text AS role_tablename,
        '10_View/Roles/Erasmus_Students'::text AS role_target_viewname
    FROM
        erasmus_socrates_studentship
        JOIN erasmus_type_hid USING (erasmus_type_id))
UNION
SELECT
    post_history.person_id,
    post_history.start_date,
    post_history.intended_end_date, 
--    CASE WHEN post_history.funding_end_date > post_history.intended_end_date THEN
--        post_history.funding_end_date
--    ELSE
--        COALESCE(post_history.intended_end_date, post_history.funding_end_date)
--    END AS estimated_leaving_date,
    (
        SELECT 
            min(end_date)
        FROM ( 
                SELECT
                    id,
                    end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    intended_end_date AS end_date
                FROM post_history
            UNION
                SELECT
                    id,
                    funding_end_date AS end_date
                FROM post_history
            GROUP BY id
        ) closest_date
        WHERE closest_date.id = post_history.id 
    ) AS estimated_leaving_date,
    post_history.end_date,
    post_history.funding_end_date,
    post_history.supervisor_id,
    NULL::bigint AS co_supervisor_id,
    post_history.mentor_id,
    'sc-'::text || post_history.staff_category_id::text AS post_category_id,
    staff_category.category AS post_category,
    post_history.filemaker_funding AS funding,
    NULL::character varying(500) AS fees_funding,
    post_history.research_grant_number,
    post_history.paid_by_university,
    post_history.chem,
    post_history.job_title,
    post_history.id AS role_id,
    post_history.force_role_status_to_past,
    'post_history'::text AS role_tablename,
    '10_View/Roles/Post_History'::text AS role_target_viewname
FROM
    post_history
    LEFT JOIN staff_category ON post_history.staff_category_id = staff_category.id)
UNION
SELECT
    postgraduate_studentship.person_id,
    postgraduate_studentship.start_date,
    postgraduate_studentship.intended_end_date,
    CASE WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        CASE WHEN postgraduate_studentship.end_of_registration_date IS NULL THEN
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.start_date + '4 years'::interval + '3 mons'::interval)::date)
        ELSE
            COALESCE(postgraduate_studentship.intended_end_date, (postgraduate_studentship.end_of_registration_date + '3 mons'::interval)::date)
        END
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.intended_end_date, postgraduate_studentship.msc_date_submission_due)
    ELSE
        NULL::date
    END AS estimated_leaving_date,
    CASE WHEN postgraduate_studentship.force_role_status_to_past = TRUE THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.date_withdrawn_from_register, 'now'::text::date - 1)
    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
        COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN
        COALESCE(postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN
        COALESCE(postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN
        COALESCE(postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
    ELSE
        NULL::date
    END AS end_date,
    postgraduate_studentship.funding_end_date,
    postgraduate_studentship.first_supervisor_id AS supervisor_id,
    postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
    postgraduate_studentship.first_mentor_id AS mentor_id,
    'pg-'::text || postgraduate_studentship.postgraduate_studentship_type_id::text AS post_category_id,
    postgraduate_studentship_type.name AS post_category,
    postgraduate_studentship.filemaker_funding AS funding,
    postgraduate_studentship.filemaker_fees_funding AS fees_funding,
    NULL::character varying(500) AS research_grant_number,
    postgraduate_studentship.paid_through_payroll AS paid_by_university,
    TRUE AS chem,
    NULL::character varying(120) AS job_title,
    postgraduate_studentship.id AS role_id,
    postgraduate_studentship.force_role_status_to_past,
    'postgraduate_studentship'::text AS role_tablename,
    '10_View/Roles/Postgrad_Students'::text AS role_target_viewname
FROM
    postgraduate_studentship
    LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
    LEFT JOIN person ON person.id = postgraduate_studentship.person_id) role_data;

ALTER TABLE _all_roles_v13 OWNER TO dev;

GRANT ALL ON TABLE _all_roles_v13 TO dev;



