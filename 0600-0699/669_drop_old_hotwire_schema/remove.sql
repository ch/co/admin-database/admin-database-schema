CREATE SCHEMA hotwire;


ALTER SCHEMA hotwire OWNER TO postgres;

SET search_path = hotwire, pg_catalog;

--
-- Name: 10_View/Safety/Fire_warden_areas; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Safety/Fire_warden_areas" AS
    SELECT fire_warden_area.fire_warden_area_id AS id, fire_warden_area.name, fire_warden_area.location, fire_warden_area.building_id, fire_warden_area.building_region_id, fire_warden_area.building_floor_id, primary_fw.fire_warden_id, ARRAY(SELECT fw3.fire_warden_id FROM public.fire_warden fw3 WHERE ((fw3.fire_warden_area_id = fire_warden_area.fire_warden_area_id) AND (fw3.is_primary = false))) AS deputy_fire_warden_id FROM (public.fire_warden_area LEFT JOIN (SELECT a2.fire_warden_area_id, fw2.fire_warden_id FROM (public.fire_warden_area a2 JOIN public.fire_warden fw2 USING (fire_warden_area_id)) WHERE (fw2.is_primary = true)) primary_fw USING (fire_warden_area_id));


ALTER TABLE hotwire."10_View/Safety/Fire_warden_areas" OWNER TO dev;

--
-- Name: fn_fire_warden_area_upd("10_View/Safety/Fire_warden_areas"); Type: FUNCTION; Schema: hotwire; Owner: dev
--

CREATE FUNCTION fn_fire_warden_area_upd("10_View/Safety/Fire_warden_areas") RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
declare
    v alias for $1;
    v_fire_warden_area_id bigint;
begin
    if v.id is not null 
    then
        v_fire_warden_area_id = v.id;
        update fire_warden_area set 
            name = v.name,
            location = v.location,
            building_id = v.building_id,
            building_region_id = v.building_region_id,
            building_floor_id = v.building_floor_id
        where fire_warden_area_id = v_fire_warden_area_id;
    else
        insert into fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) values (
            v.name,
            v.location,
            v.building_id,
            v.building_region_id,
            v.building_floor_id
        ) returning fire_warden_area_id into v_fire_warden_area_id;
    end if;
    -- remove all fire wardens for this area
    update fire_warden set fire_warden_area_id = null where fire_warden.fire_warden_area_id = v_fire_warden_area_id;
    -- update the primary fire warden
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 't' where fire_warden.fire_warden_id = v.fire_warden_id;
    -- update secondary fire wardens
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 'f' where fire_warden.fire_warden_id = any(v.deputy_fire_warden_id);
    return v_fire_warden_area_id;
end;
$_$;


ALTER FUNCTION hotwire.fn_fire_warden_area_upd("10_View/Safety/Fire_warden_areas") OWNER TO dev;

--
-- Name: 10_View/Safety/Fire_wardens; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Safety/Fire_wardens" AS
    SELECT fire_warden.fire_warden_id AS id, fire_warden.person_id, fire_warden.qualification_date, fire_warden_area.fire_warden_area_id, fire_warden_area.building_id AS ro_building_id, fire_warden_area.building_region_id AS ro_building_region_id, fire_warden_area.building_floor_id AS ro_building_floor_id, fire_warden.is_primary AS main_firewarden_for_area, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (fire_warden.person_id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, _physical_status_v3.status_id AS ro_physical_status, (_fire_warden_status.requalification_date)::date AS ro_requalification_date, _fire_warden_status.in_date AS ro_qualification_in_date, _latest_role_v12.post_category AS ro_post_category, CASE WHEN ((_physical_status_v3.status_id)::text <> 'Current'::text) THEN 'orange'::text WHEN (_fire_warden_status.in_date = false) THEN 'red'::text WHEN (_fire_warden_status.in_date IS NULL) THEN 'blue'::text ELSE NULL::text END AS _cssclass FROM ((((public.fire_warden LEFT JOIN public._latest_role_v12 USING (person_id)) LEFT JOIN public.fire_warden_area USING (fire_warden_area_id)) LEFT JOIN public._physical_status_v3 USING (person_id)) LEFT JOIN public._fire_warden_status USING (fire_warden_id));


ALTER TABLE hotwire."10_View/Safety/Fire_wardens" OWNER TO dev;

--
-- Name: fn_fire_warden_upd("10_View/Safety/Fire_wardens"); Type: FUNCTION; Schema: hotwire; Owner: dev
--

CREATE FUNCTION fn_fire_warden_upd("10_View/Safety/Fire_wardens") RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
declare
    v alias for $1;
    v_fire_warden_id bigint;
begin
    if v.main_firewarden_for_area = 't' and v.fire_warden_area_id is not null
    then
        update fire_warden set is_primary = 'f' where fire_warden_area_id = v.fire_warden_area_id;
    end if;
    if v.id is not null 
    then
        v_fire_warden_id = v.id;
        update fire_warden set 
            qualification_date = v.qualification_date,
            fire_warden_area_id = v.fire_warden_area_id,
            is_primary = v.main_firewarden_for_area
        where fire_warden_id = v_fire_warden_id;
    else
        insert into fire_warden ( 
            qualification_date,
            person_id,
            fire_warden_area_id,
            is_primary
        ) values (
            v.qualification_date,
            v.person_id,
            v.fire_warden_area_id,
            coalesce(v.main_firewarden_for_area,'t')
        ) returning fire_warden_id into v_fire_warden_id;
    end if;
    return v_fire_warden_id;
end;
$_$;


ALTER FUNCTION hotwire.fn_fire_warden_upd("10_View/Safety/Fire_wardens") OWNER TO dev;

--
-- Name: 10_View/People/Personnel_Data_Entry; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Personnel_Data_Entry" AS
    SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, (a.known_as)::text AS known_as, a.name_suffix, a.previous_surname, a.date_of_birth, a.ro_age, a.gender_id, a.ro_post_category_id, a.ro_chem, a.crsid, a.email_address, a.hide_email, a.arrival_date, a.leaving_date, a.ro_physical_status_id, a.left_but_no_leaving_date_given, a.dept_telephone_number_id, a.hide_phone_no_from_website, a.hide_from_website, a.room_id, a.ro_supervisor_id, a.ro_co_supervisor_id, a.research_group_id, a.home_address, (a.home_phone_number)::text AS home_phone_number, a.cambridge_college_id, (a.mobile_number)::text AS mobile_number, a.nationality_id, a.emergency_contact, (a.location)::text AS location, a.other_information, a.notes, a.forwarding_address, a.new_employer_address, a.chem_at_cam AS "chem_@_cam", a.retired_staff_mailing_list, a.continuous_employment_start_date, (a.paper_file_details)::text AS paper_file_status, a.registration_completed, a.clearance_cert_signed, a._cssclass, a.treat_as_academic_staff FROM (SELECT person.id, person.id AS ro_person_id, ROW('image/jpeg'::character varying, (person.image_lo)::bigint)::public.blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.previous_surname, person.date_of_birth, date_part('year'::text, age((person.date_of_birth)::timestamp with time zone)) AS ro_age, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, _latest_role.chem AS ro_chem, person.crsid, person.email_address, person.hide_email, person.do_not_show_on_website AS hide_from_website, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (person.id = mm_person_room.person_id)) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.co_supervisor_id AS ro_co_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, person.cambridge_address AS home_address, person.cambridge_phone_number AS home_phone_number, person.cambridge_college_id, person.mobile_number, ARRAY(SELECT mm_person_nationality.nationality_id FROM public.mm_person_nationality WHERE (mm_person_nationality.person_id = person.id)) AS nationality_id, person.emergency_contact, person.location, (person.other_information)::character varying(5000) AS other_information, (person.notes)::character varying(5000) AS notes, person.forwarding_address, person.new_employer_address, person.chem_at_cam, CASE WHEN (retired_staff_list.include_person_id IS NOT NULL) THEN true ELSE false END AS retired_staff_mailing_list, person.continuous_employment_start_date, person.paper_file_details, person.registration_completed, person.clearance_cert_signed, person.counts_as_academic AS treat_as_academic_staff, CASE WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass FROM (((public.person LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id))) LEFT JOIN (SELECT mm_mailinglist_include_person.mailinglist_id, mm_mailinglist_include_person.include_person_id FROM (public.mm_mailinglist_include_person JOIN public.mailinglist ON ((mm_mailinglist_include_person.mailinglist_id = mailinglist.id))) WHERE ((mailinglist.name)::text = 'chem-retiredstaff'::text)) retired_staff_list ON ((person.id = retired_staff_list.include_person_id)))) a ORDER BY a.surname, a.first_names;


ALTER TABLE hotwire."10_View/People/Personnel_Data_Entry" OWNER TO dev;

--
-- Name: personnel_data_entry_upd("10_View/People/Personnel_Data_Entry"); Type: FUNCTION; Schema: hotwire; Owner: dev
--

CREATE FUNCTION personnel_data_entry_upd("10_View/People/Personnel_Data_Entry") RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_lo=(select (v.image_oid).val from (select v.image_oid) as x), 
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as::varchar(80),
                                name_suffix = v.name_suffix,
                                previous_surname = v.previous_surname::varchar(80), 
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                cambridge_address = v.home_address,
                                cambridge_phone_number = v.home_phone_number::varchar(80), 
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number::varchar(80),
                                emergency_contact = v.emergency_contact, 
                                location = v.location::varchar(80),
                                other_information = v.other_information::text,
                                notes = v.notes::text,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                chem_at_cam = v."chem_@_cam",
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_status::varchar(80), 
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                        image_lo, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        previous_surname, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        chem_at_cam,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        (select (v.image_oid).val from (select v.image_oid) as x),
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as::varchar(80),
                                        v.name_suffix,
                                        v.previous_surname::varchar(80), 
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.home_address,
                                        v.home_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number::varchar(80),
                                        v.emergency_contact, 
                                        v.location::varchar(80),
                                        v.other_information::text,
                                        v.notes::text,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v."chem_@_cam",
                                        v.continuous_employment_start_date,
                                        v.paper_file_status::varchar(80), 
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;
$_$;


ALTER FUNCTION hotwire.personnel_data_entry_upd("10_View/People/Personnel_Data_Entry") OWNER TO dev;

--
-- Name: suggest_view(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: hotwire; Owner: postgres
--

CREATE FUNCTION suggest_view(tablename character varying, schemaname character varying, omenuname character varying, oviewname character varying) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $_$
declare
colcount bigint;
keycount bigint;
colinf record;
query text;
pkeyfield varchar;
fullname varchar;
colalias varchar;
-- Type of table/view
qtype varchar;
updrule text;
delrule text;
insrule text;
insruleb text:=') values (';
insrulec text:=') RETURNING ';
hidtable varchar;
viewname varchar:=Oviewname;
menuname varchar:=Omenuname;
testschema varchar;
istable boolean;
roid integer;
rtable varchar;
rfieldnum integer;
begin
-- Viewname is strictly optional.
if (viewname is NULL) THEN
 viewname=tablename;
END IF;
-- Menuname is strictly optional.
if (menuname is NULL) THEN
 menuname='Menu';
END IF;

-- Is this a table or a view?
select count(*) from pg_catalog.pg_tables where pg_tables.schemaname=schemaname and pg_tables.tablename=tablename into colcount;
istable=(colcount=1);
if (istable) THEN
 qtype='table';
else
 qtype='view';
END IF;

return next '-- [II]  Hotwire suggesting a view definition for the '||qtype||' "'||tablename||'" ';
return next '--       in the "'||schemaname||'" schema'::varchar;
-- Check that this table exists
select count(*) from information_schema.columns where table_name=tablename and table_schema=schemaname into colcount;
if (colcount<1) THEN
 return next '-- [EE]  Hotwire was unable to find a definition for that '||qtype||'. Sorry.';
 EXIT;
END IF;
return next '-- [II]  Found '||colcount||' fields in that '||qtype;

-- Check that this table has a primary key
if (istable) THEN
  SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=tablename into pkeyfield;
  if (pkeyfield IS NULL) THEN
   return next '-- [EE]  Hotwire was unable to find a primary key for that '||qtype||'. Perhaps you need to add one?';
   return;
   EXIT;
  else
   return next '-- Primary key on this '||qtype||' is '||pkeyfield;
  END IF;
ELSE
  -- How do we find a primary key for a view?
  return next '-- [II]  Attempting to identify primary key on the view '||tablename;
  -- Find the _RETURN rule for this view
  select distinct objid from pg_depend 
                  inner join pg_rewrite on pg_depend.objid=pg_rewrite.oid 
                       where refobjid=tablename::regclass 
                         and classid='pg_rewrite'::regclass  
                         and rulename='_RETURN'into roid;
  select count(*) from (select distinct refobjid::regclass 
                                   from pg_depend 
                                  where objid=roid 
                                    and deptype='n' 
                                    and refobjsubid!=0) a into colcount;
  if (colcount>1) THEN
    return next '-- [EE]  Sorry, hotwire cannot (yet) suggest definitions for views coming from more';
    return next '--       than one table.';
    return;
    exit;
  else 

    select distinct refobjid::regclass from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 limit 1 into rtable;
    
    return next '-- [II]  Good, this view uses a single table ('||rtable||')';
    SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=rtable into pkeyfield;
    return next '--       Checking for presence of that table''s primary key ('||pkeyfield||')';
    return next '         in the columns of the view';
    -- Find column number
    select attnum from pg_attribute where attrelid='person'::regclass and attname='id' into rfieldnum;
    -- Is column number in view?
    select count(*) from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 and refobjsubid=rfieldnum into colcount;
    if (colcount=1) then
      return next '-- [II]  Good, the primary key ('||pkeyfield||') on that table appears in the view.';
    else
      return next '-- [EE]  Oh dear, the primary key ('||pkeyfield||') doesn''t appear in the view. ';
      return next '--       Try adding it to the view and repeating this operation...';
      return;
      exit;
    end if;
  END IF;
--   execute select distinct objid from pg_depend inner join pg_rewrite on 
-- pg_depend.objid=pg_rewrite.oid where refobjid='tview'::regclass 
-- and classid='pg_rewrite'::regclass  and rulename='_RETURN';
END IF;
fullname=menuname||'/'||viewname;

query='CREATE VIEW hotwire."'||fullname||'" AS SELECT ';
updrule='CREATE RULE "hotwire_'||fullname||'_upd" AS ON UPDATE TO hotwire."'||fullname||'" DO INSTEAD UPDATE "'||tablename||'" SET ';
delrule='CREATE RULE "hotwire_'||fullname||'_del" AS ON DELETE TO hotwire."'||fullname||'" DO INSTEAD '||
        'DELETE FROM "'||tablename||'" WHERE "'||pkeyfield||'" = old.id;';
insrule='CREATE RULE "hotwire_'||fullname||'_ins" AS ON INSERT TO hotwire."'||fullname||'" DO INSTEAD '||
        'INSERT INTO "'||tablename||'" (';

-- Iterate over all fields in the table
for colinf in (select * from information_schema.columns where table_name=tablename and table_schema=schemaname) LOOP
 colalias=NULL;
 -- return next '-- Considering field '||colinf.column_name;
  -- Only tables have foreign keys. (Well, views sort of do but...)
  IF (istable) THEN
    -- Is this a foreign key? 
    SELECT count(*) FROM information_schema.table_constraints tc 
                      LEFT JOIN information_schema.key_column_usage kcu 
                        ON tc.constraint_catalog = kcu.constraint_catalog 
                          AND tc.constraint_schema = kcu.constraint_schema 
                          AND tc.constraint_name = kcu.constraint_name 
                      LEFT JOIN information_schema.referential_constraints rc 
                        ON tc.constraint_catalog = rc.constraint_catalog 
                          AND tc.constraint_schema = rc.constraint_schema 
                          AND tc.constraint_name = rc.constraint_name 
                      LEFT JOIN information_schema.constraint_column_usage ccu 
                        ON rc.unique_constraint_catalog = ccu.constraint_catalog 
                          AND rc.unique_constraint_schema = ccu.constraint_schema 
                          AND rc.unique_constraint_name = ccu.constraint_name
                  WHERE lower(tc.constraint_type) in ('foreign key') 
                    AND kcu.column_name=colinf.column_name
                    AND tc.table_name=tablename into keycount;
    IF (keycount>0) THEN
      if ((length(colinf.column_name)>3) AND 
          (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)!='_id')) then
        colalias=colinf.column_name||'_id';
      end if;
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
        into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema and column_name=hidtable 
          into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF; -- Is this a foreign key?  
  ELSE
    if (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)='_id') THEN
      return next '-- [II]  The column '||colinf.column_name||' will be treated as a foreign key';
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=hidtable into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF;
  END IF;
 -- Is this the primary key?
 if (colinf.column_name=pkeyfield) THEN
  if (pkeyfield!='id') THEN
   colalias='id';
  END IF;
 ELSE
  -- We don't insert to the primary key of a table.
  insrule=insrule||' "'||colinf.column_name||'", ';
  insruleb=insruleb||' new."'||coalesce(colalias, colinf.column_name)||'", ';
 END IF;
 insrulec=insrulec||' "'||tablename||'"."'||colinf.column_name||'", ';
 query=query||' "'||colinf.column_name||'"'||coalesce(' AS "'||colalias||'"','')||',';
 updrule=updrule||'"'||colinf.column_name||'"=new."'||coalesce(colalias,colinf.column_name)||'", ';
END LOOP;
updrule=rtrim(updrule,', ')||' WHERE "'||tablename||'"."'||pkeyfield||'"=old.id;';
insrule=rtrim(insrule,', ')||rtrim(insruleb,', ')||rtrim(insrulec,', ')||';';
query=rtrim(query,', ')||' FROM "'||schemaname||'"."'||tablename||'";';
return next '-- Hotwire suggests the following query definition:';
return next query::varchar;
return next '-- Rules:';
return next updrule::varchar;
return next delrule::varchar;
return next insrule::varchar;
end
$_$;


ALTER FUNCTION hotwire.suggest_view(tablename character varying, schemaname character varying, omenuname character varying, oviewname character varying) OWNER TO postgres;

--
-- Name: FUNCTION suggest_view(tablename character varying, schemaname character varying, omenuname character varying, oviewname character varying); Type: COMMENT; Schema: hotwire; Owner: postgres
--

COMMENT ON FUNCTION suggest_view(tablename character varying, schemaname character varying, omenuname character varying, oviewname character varying) IS 'To suggest a hotwire view from a table''s definition';


--
-- Name: 10_View/Roles/Bulk_update_supervisors; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Bulk_update_supervisors" AS
    SELECT person.id, person.id AS old_supervisor_id, NULL::bigint AS new_supervisor_id, public._to_hwsubviewb('10_View/Roles/_current_people_supervised_by'::character varying, 'supervisor_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS current_people_supervised_by_old_supervisor_subview FROM public.person;


ALTER TABLE hotwire."10_View/Roles/Bulk_update_supervisors" OWNER TO dev;

--
-- Name: update_supervisors("10_View/Roles/Bulk_update_supervisors"); Type: FUNCTION; Schema: hotwire; Owner: dev
--

CREATE FUNCTION update_supervisors("10_View/Roles/Bulk_update_supervisors") RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
declare 
    v alias for $1;
begin
    update post_history set supervisor_id = v.new_supervisor_id where post_history.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'post_history' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update visitorship set host_person_id = v.new_supervisor_id where visitorship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'visitorship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update postgraduate_studentship set first_supervisor_id = v.new_supervisor_id where postgraduate_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'postgraduate_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update erasmus_socrates_studentship set supervisor_id = v.new_supervisor_id where erasmus_socrates_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'erasmus_socrates_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    update part_iii_studentship set supervisor_id = v.new_supervisor_id where part_iii_studentship.id in (select role_id from _latest_role_v12 lr join _physical_status_v3 ps using (person_id) where lr.role_tablename = 'part_iii_studentship' and lr.supervisor_id = v.old_supervisor_id and lr.status = 'Current' and ps.status_id = 'Current' );
    return v.old_supervisor_id;
end;
$_$;


ALTER FUNCTION hotwire.update_supervisors("10_View/Roles/Bulk_update_supervisors") OWNER TO dev;

--
-- Name: 30_WPKG/10_software; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_WPKG/10_software" AS
    SELECT software_package.id, software_package.name, software_package.wpkg_package_name, software_package.munki_package_name, software_package.revision, software_package.priority, software_package.reporting_string, software_package.reporting_version, software_package.reporting_path, software_package.min_os_ver, software_package.max_os_ver, software_package.reboot_id, software_package.sitelicence, software_package.on_all_macs, ARRAY(SELECT mm_software_package_can_install_on_os.operating_system_id FROM public.mm_software_package_can_install_on_os WHERE (mm_software_package_can_install_on_os.software_package_id = software_package.id)) AS operating_system_id, ARRAY(SELECT mm_software_package_depends_on.depended_software_package_id FROM public.mm_software_package_depends_on WHERE (mm_software_package_depends_on.depending_software_package_id = software_package.id)) AS depended_software_package_id, ARRAY(SELECT mm_software_package_installs_on_architecture.architecture_id FROM public.mm_software_package_installs_on_architecture WHERE (mm_software_package_installs_on_architecture.software_package_id = software_package.id)) AS architecture_id, ARRAY(SELECT mm_software_package_updates_software_package.updated_software_package_id FROM public.mm_software_package_updates_software_package WHERE (mm_software_package_updates_software_package.updating_software_package_id = software_package.id)) AS updated_software_package_id, ARRAY(SELECT mm_software_package_is_licensed_by_licence.licence_id FROM public.mm_software_package_is_licensed_by_licence WHERE (mm_software_package_is_licensed_by_licence.software_package_id = software_package.id)) AS licence_id, ARRAY(SELECT mm_system_image_has_installed_software_package.system_image_id FROM public.mm_system_image_has_installed_software_package WHERE (mm_system_image_has_installed_software_package.software_package_id = software_package.id)) AS system_image_id FROM public.software_package;


ALTER TABLE hotwire."30_WPKG/10_software" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.fn_software_package_insupd(hotwire."30_WPKG/10_software")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_id bigint;

        begin
                -- Detect update
	if v.id is not null then
		v_id = v.id;
		update software_package set
			reporting_string=v.reporting_string,
			name=v.name,
			revision=v.revision,
			priority=v.priority,
			reporting_version=v.reporting_version,
			wpkg_package_name=v.wpkg_package_name,
			munki_package_name=v.munki_package_name,
			min_os_ver=v.min_os_ver,
			max_os_ver=v.max_os_ver,
			reporting_path=v.reporting_path,
			reboot_id=v.reboot_id,
			sitelicence=v.sitelicence,
			on_all_macs=v.on_all_macs
		where id=v.id;
	else
		v_id=nextval('software_package_id_seq');
		insert into software_package (
			id,
			reporting_string,
			name,
			revision,
			priority,
			reporting_version,
			wpkg_package_name,
			munki_package_name,
			min_os_ver,
			max_os_ver,
			reporting_path,
			reboot_id,
			sitelicence,
			on_all_macs
		) values (
			v_id,
			v.reporting_string,
			v.name,
			v.revision,
			v.priority,
			v.reporting_version,
			v.wpkg_package_name,
			v.munki_package_name,
			v.min_os_ver,
			v.max_os_ver,
			v.reporting_path,
			v.reboot_id,
			v.sitelicence,
			v.on_all_macs
		);
	end if;
	perform fn_mm_update('mm_software_package_can_install_on_os'::varchar,
			     'operating_system_id'::varchar,v.operating_system_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_depends_on'::varchar,
	                     'depended_software_package_id'::varchar, v.depended_software_package_id,
	                     'depending_software_package_id'::varchar, v_id);
	perform fn_mm_update('mm_software_package_installs_on_architecture'::varchar,
			     'architecture_id'::varchar,v.architecture_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_updates_software_package'::varchar,
			     'updated_software_package_id'::varchar,v.updated_software_package_id,
			     'updating_software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_software_package_is_licensed_by_licence'::varchar,
			     'licence_id'::varchar,v.licence_id,
			     'software_package_id'::varchar,v_id);
	perform fn_mm_update('mm_system_image_has_installed_software_package'::varchar,
			     'system_image_id'::varchar,v.system_image_id,
			     'software_package_id'::varchar,v_id);
        return v_id;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_software_package_insupd(hotwire."30_WPKG/10_software")
  OWNER TO dev;


--
-- Name: 30_WPKG/15_licence_instance; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_WPKG/15_licence_instance" AS
    SELECT licence_instance.id, licence_instance.licence_id, licence_instance.paper_licence_certificate_id, licence_instance.order_ref, licence_instance.personal, licence_instance.number_of_seats, licence_instance.person_id, ARRAY(SELECT mm_system_image_licence_instance.system_image_id FROM public.mm_system_image_licence_instance WHERE (mm_system_image_licence_instance.licence_instance_id = licence_instance.id)) AS system_image_id, ARRAY(SELECT mm_licence_instance_research_group.research_group_id FROM public.mm_licence_instance_research_group WHERE (mm_licence_instance_research_group.licence_instance_id = licence_instance.id)) AS research_group_id FROM public.licence_instance;


ALTER TABLE hotwire."30_WPKG/15_licence_instance" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.fn_wpkg_licence_instance_insupd(hotwire."30_WPKG/15_licence_instance")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_id bigint;

        begin
                -- Detect update
                if v.id is not null then
                        v_id = v.id;
                        update licence_instance set
                                personal=v.personal,
                                number_of_seats=v.number_of_seats,
                                paper_licence_certificate_id=v.paper_licence_certificate_id,
                                person_id=v.person_id,
                                -- research_group_id=v.research_group_id,
                                licence_id=v.licence_id,
                                order_ref=v.order_ref
                        where id=v_id;
                else
                        insert into licence_instance (
				personal,
				number_of_seats,
				paper_licence_certificate_id,
				person_id,
				-- research_group_id,
				licence_id,
				order_ref
			) values (
				v.personal,
				v.number_of_seats,
				v.paper_licence_certificate_id,
				v.person_id,
				-- v.research_group_id,
				v.licence_id,
				v.order_ref
			) returning id into v_id;
                end if;

                -- many-many updates here:
                -- Args: array of other values, table to update, column for this record, column for other values, id of this record
                perform fn_mm_update('mm_system_image_licence_instance'::varchar,
                             'system_image_id'::varchar, v.system_image_id,
                             'licence_instance_id'::varchar, v_id);
                perform fn_mm_update('mm_licence_instance_research_group'::varchar, 'licence_instance_id'::varchar, v.id, 'research_group_id'::varchar, v.research_group_id);
                return v_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_wpkg_licence_instance_insupd(hotwire."30_WPKG/15_licence_instance")
  OWNER TO dev;
COMMENT ON FUNCTION public.fn_wpkg_licence_instance_insupd(hotwire."30_WPKG/15_licence_instance") IS 'x';


--
-- Name: 30_WPKG/40_licence; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_WPKG/40_licence" AS
    SELECT licence.id, licence.description, licence.short_name, licence.is_upgrade, licence.can_install_on_any_managed_machine, ARRAY(SELECT mm_software_package_is_licensed_by_licence.software_package_id FROM public.mm_software_package_is_licensed_by_licence WHERE (mm_software_package_is_licensed_by_licence.licence_id = licence.id)) AS software_package_id, ARRAY(SELECT mm_licence_upgrades_licence.upgraded_licence_id FROM public.mm_licence_upgrades_licence WHERE (mm_licence_upgrades_licence.upgrading_licence_id = licence.id)) AS upgraded_licence_id, ARRAY(SELECT mm_licence_upgrades_licence.upgrading_licence_id FROM public.mm_licence_upgrades_licence WHERE (mm_licence_upgrades_licence.upgraded_licence_id = licence.id)) AS upgrading_licence_id, ARRAY(SELECT mm_licence_downgrades_licence.licence_downgraded_to_id FROM public.mm_licence_downgrades_licence WHERE (mm_licence_downgrades_licence.licence_id = licence.id)) AS licence_downgraded_to_id, ARRAY(SELECT mm_licence_downgrades_licence.licence_id FROM public.mm_licence_downgrades_licence WHERE (mm_licence_downgrades_licence.licence_downgraded_to_id = licence.id)) AS allowed_by_other_licence_id FROM public.licence;


ALTER TABLE hotwire."30_WPKG/40_licence" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.fn_wpkg_licence_insupd(hotwire."30_WPKG/40_licence")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_id bigint;

        begin
                -- Detect update
                if v.id is not null then
                        v_id = v.id;
                        update licence set 
                                is_upgrade=v.is_upgrade,
                                short_name=v.short_name,
                                description=v.description,
                                can_install_on_any_managed_machine=v.can_install_on_any_managed_machine
                        where licence.id=v_id;
                else
                        insert into licence (
				is_upgrade,
				short_name,
				description,
				can_install_on_any_managed_machine) 
			values (
				v.is_upgrade,
				v.short_name,
				v.description,
				v.can_install_on_any_managed_machine) returning id into v_id;
                end if;

                -- many-many updates here:
                -- Args: array of other values, table to update, column for this record, column for other values, id of this record
                perform fn_mm_update('mm_software_package_is_licensed_by_licence'::varchar,
                                     'software_package_id'::varchar,v.software_package_id,
                                     'licence_id'::varchar,v_id);
                perform fn_mm_update('mm_licence_upgrades_licence'::varchar,
                                     'upgraded_licence_id'::varchar, v.upgraded_licence_id,
                                     'upgrading_licence_id'::varchar, v_id); 
                perform fn_mm_update('mm_licence_upgrades_licence'::varchar,
                                     'upgrading_licence_id'::varchar, v.upgrading_licence_id,
                                     'upgraded_licence_id'::varchar, v_id);                     
                perform fn_mm_update('mm_licence_downgrades_licence'::varchar,
                                     'licence_downgraded_to_id'::varchar, v.licence_downgraded_to_id,
                                     'licence_id'::varchar, v_id);                     
                perform fn_mm_update('mm_licence_downgrades_licence'::varchar,
                                     'licence_id'::varchar, v.allowed_by_other_licence_id,
                                     'licence_downgraded_to_id'::varchar, v_id);                     

                return v_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_wpkg_licence_insupd(hotwire."30_WPKG/40_licence")
  OWNER TO dev;


--
-- Name: 10_View/Dom0_DomU_Mapping; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Dom0_DomU_Mapping" AS
    SELECT DISTINCT a.dom0_id AS id, a.dom0_id, ARRAY(SELECT u.domu_id FROM public.mm_dom0_domu u WHERE (u.dom0_id = a.dom0_id)) AS domu_id FROM public.mm_dom0_domu a;


ALTER TABLE hotwire."10_View/Dom0_DomU_Mapping" OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.hw_fn_dom0_domu_mapping_upd(hotwire."10_View/Dom0_DomU_Mapping")
  RETURNS bigint AS
$BODY$
 
  declare
          v alias for $1;
  begin
   PERFORM fn_mm_array_update(v.domu_id, 'mm_dom0_domu'::varchar,'dom0_id'::varchar,'domu_id'::varchar,v.dom0_id);
   return v.dom0_id;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_dom0_domu_mapping_upd(hotwire."10_View/Dom0_DomU_Mapping")
  OWNER TO cen1001;


--
-- Name: 10_View/10_IT_Tasks/IT_Projects; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/IT_Projects" AS
    SELECT it_project.id, it_project.project_number, it_project.name, it_project.project_purpose, ARRAY(SELECT it_task.id FROM public.it_task WHERE (it_task.it_project_id = it_project.id)) AS it_task_id FROM public.it_project ORDER BY it_project.project_number;


ALTER TABLE hotwire."10_View/10_IT_Tasks/IT_Projects" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_it_projects_upd(hotwire."10_View/10_IT_Tasks/IT_Projects")
  RETURNS bigint AS
$BODY$
 
  declare
          v alias for $1;
          v_project_id BIGINT;
  begin

  -- Update
  IF v.id IS NOT NULL THEN
 
    v_project_id := v.id;

    UPDATE it_project SET
          name=v.name,
          project_number=v.project_number,
          project_purpose=v.project_purpose
    WHERE it_project.id = v.id;
 
  ELSE
 
    insert into it_project (name, project_number,project_purpose) values (
     v.name,v.project_number,v.project_purpose
    ) returning id into v_project_id;
 
  END IF;
  -- it tasks
  update it_task set it_project_id = null where it_task.it_project_id = v_project_id;
  update it_task set it_project_id = v_project_id where it_task.id = any(v.it_task_id);
  
  
  return v_project_id;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_it_projects_upd(hotwire."10_View/10_IT_Tasks/IT_Projects")
  OWNER TO dev;


--
-- Name: person_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW person_hid AS
    SELECT person.id AS person_id, ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text) AS person_hid FROM (public.person LEFT JOIN public.title_hid USING (title_id)) ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text);


ALTER TABLE hotwire.person_hid OWNER TO postgres;

--
-- Name: computer_officer_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW computer_officer_hid AS
    SELECT person_hid.person_id AS computer_officer_id, person_hid.person_hid AS computer_officer_hid FROM ((person_hid JOIN public.mm_person_research_group USING (person_id)) JOIN public.research_group ON ((research_group.id = mm_person_research_group.research_group_id))) WHERE ((research_group.name)::text = 'COs'::text) ORDER BY person_hid.person_hid;


ALTER TABLE hotwire.computer_officer_hid OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Tasks; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Tasks" AS
    SELECT a.id, a.task_number, a.name, a.task_description, a.task_notes, a."Primary_strategic_goal_id", a.it_strategic_goal_id, a.it_project_id, a.person_id, a.all_cos, a."IT_task_leader_id", a.computer_officer_id, a.created, a.completed, a.needed_by, a.time_estimate_id, a."IT_status_id", a.priority, a."order", a.progress, a.predecessor_task_id, a._cssclass, a.changes, a.anticipated_completion_date FROM (SELECT it_task.id, it_task.task_number, it_task.name, it_task.task_description, it_task.task_notes, it_task.anticipated_completion_date, it_task."Primary_strategic_goal_id", ARRAY(SELECT mm_it_task_strategic_goal.it_strategic_goal_id FROM public.mm_it_task_strategic_goal WHERE (mm_it_task_strategic_goal.it_task_id = it_task.id)) AS it_strategic_goal_id, it_task.it_project_id, ARRAY(SELECT mm_it_task_interested_parties.person_id FROM public.mm_it_task_interested_parties WHERE (mm_it_task_interested_parties.it_task_id = it_task.id)) AS person_id, ((SELECT computer_officer_hid.computer_officer_hid FROM computer_officer_hid WHERE (computer_officer_hid.computer_officer_id = it_task."IT_task_leader_id") LIMIT 1) || COALESCE(('; '::text || array_to_string(ARRAY(SELECT computer_officer_hid.computer_officer_hid FROM (public.mm_it_task_other_cos JOIN computer_officer_hid USING (computer_officer_id)) WHERE (mm_it_task_other_cos.it_task_id = it_task.id)), '; '::text)), ''::text)) AS all_cos, it_task."IT_task_leader_id", ARRAY(SELECT mm_it_task_other_cos.computer_officer_id FROM public.mm_it_task_other_cos WHERE (mm_it_task_other_cos.it_task_id = it_task.id)) AS computer_officer_id, it_task.created, it_task.completed, it_task.needed_by, it_task.time_estimate_id, it_task."IT_status_id", it_task.priority, it_task."order", trunc((((((10)::numeric * it_task.progress))::integer)::numeric * 10.0), 0) AS progress, ARRAY(SELECT mm_it_task_depends_on.it_predecessor_task_id FROM public.mm_it_task_depends_on WHERE (mm_it_task_depends_on.it_successor_task_id = it_task.id)) AS predecessor_task_id, ((('complete'::text || trunc(((10)::numeric * COALESCE(it_task.progress, (0)::numeric)), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", (0)::bigint)) AS _cssclass, public._to_hwsubviewb('10_View/10_IT_Tasks/_audit_ro'::character varying, 'it_task_id'::character varying, NULL::character varying, NULL::character varying, NULL::character varying) AS changes FROM public.it_task WHERE (it_task.deleted IS NULL) ORDER BY COALESCE(it_task.priority, 5), it_task.weight) a;


ALTER TABLE hotwire."10_View/10_IT_Tasks/Tasks" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_it_tasks_upd(hotwire."10_View/10_IT_Tasks/Tasks")
  RETURNS bigint AS
$BODY$
 
  declare
          v alias for $1;
          v_task_id BIGINT;
  begin

  -- Update
  IF v.id IS NOT NULL THEN
 
    v_task_id := v.id;

    UPDATE it_task SET
          name=v.name,
          task_number=v.task_number,
          task_description=v.task_description,
          task_notes = v.task_notes,
          "Primary_strategic_goal_id" = v."Primary_strategic_goal_id",
          it_project_id=v.it_project_id, 
          "IT_task_leader_id" = v."IT_task_leader_id",
          created=v.created,
          completed=v.completed,
          needed_by =v.needed_by,
          time_estimate_id=v.time_estimate_id,
          "IT_status_id"=v."IT_status_id",
          priority=v.priority,
          "order"=v."order",
          progress=v.progress/100.0,
          anticipated_completion_date=v.anticipated_completion_date
    WHERE it_task.id = v.id;
 
  ELSE
 
    insert into it_task (name, task_number,task_description, task_notes, "IT_task_leader_id",
     created,completed,needed_by,time_estimate_id,"IT_status_id", priority,"order",progress,"Primary_strategic_goal_id",it_project_id,anticipated_completion_date) values (
     v.name,coalesce(v.task_number, (select max(task_number)+1 from it_task)), v.task_description, v.task_notes, v."IT_task_leader_id",
     coalesce(v.created,now()), v.completed, v.needed_by, v.time_estimate_id,
     v."IT_status_id", v.priority, v."order", v.progress/100.0,v."Primary_strategic_goal_id",v.it_project_id,v.anticipated_completion_date
    ) returning id into v_task_id;
 
  END IF;
  -- strategic goals
  PERFORM fn_mm_array_update(v.it_strategic_goal_id, 'mm_it_task_strategic_goal'::varchar,'it_task_id'::varchar,'it_strategic_goal_id'::varchar,v_task_id);
  -- interested parties
  PERFORM fn_mm_array_update(v.person_id, 'mm_it_task_interested_parties'::varchar,'it_task_id'::varchar,'person_id'::varchar,v_task_id);
  -- Dependant tasks
  PERFORM fn_mm_array_update(v.predecessor_task_id, 'mm_it_task_depends_on'::varchar,'it_successor_task_id'::varchar,'it_predecessor_task_id'::varchar,v_task_id);  
  -- other COs 
  PERFORM fn_mm_array_update(v.computer_officer_id, 'mm_it_task_other_cos'::varchar,'it_task_id'::varchar,'computer_officer_id'::varchar,v_task_id);
 
  return v_task_id;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_it_tasks_upd(hotwire."10_View/10_IT_Tasks/Tasks")
  OWNER TO dev;


--
-- Name: 10_View/People/Personnel_Basic; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Personnel_Basic" AS
    SELECT a.id, a.ro_person_id, a.image_oid, a.surname, a.first_names, a.title_id, a.name_suffix, a.email_address, a.crsid, a.date_of_birth, a.ro_post_category_id, a.research_group_id, a.dept_telephone_number_id, a.room_id, a.location, a.ro_physical_status_id, a.ro_estimated_leaving_date, a.is_spri, a._cssclass FROM (SELECT person.id, person.id AS ro_person_id, ROW('image/jpeg'::character varying, (person.image_lo)::bigint)::public.blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.name_suffix, person.email_address, person.crsid, person.date_of_birth, person.is_spri, _latest_role.post_category_id AS ro_post_category_id, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (person.id = mm_person_room.person_id)) AS room_id, person.location, _physical_status_v2.status_id AS ro_physical_status_id, LEAST(person.leaving_date, futuremost_role.estimated_leaving_date) AS ro_estimated_leaving_date, CASE WHEN ((_physical_status_v2.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass FROM (((public.person LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN apps.person_futuremost_role futuremost_role ON ((person.id = futuremost_role.person_id))) LEFT JOIN public._physical_status_v2 USING (id))) a ORDER BY a.surname, a.first_names;


ALTER TABLE hotwire."10_View/People/Personnel_Basic" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    		v_person_id BIGINT;                                                                                        
      begin                                                                                                              
      IF v.id IS NOT NULL THEN                                                                                           
        v_person_id= v.id;                                                                                               
	UPDATE person SET
		surname=v.surname,
		image_lo=(select (v.image_oid).val from (select v.image_oid) as x),
	        first_names=v.first_names,
		title_id=v.title_id,                                                       
		name_suffix = v.name_suffix,
		email_address=v.email_address, 
		crsid=v.crsid,
		date_of_birth=v.date_of_birth,
		location=v.location,
		is_spri=v.is_spri                                                                        
        WHERE person.id = v.id;                                                                                          
      ELSE    
	INSERT INTO person (surname, image_lo, first_names, title_id, name_suffix, email_address,crsid, date_of_birth, location, is_spri) VALUES (v.surname, (select (v.image_oid).val from (select v.image_oid) as x), v.first_names, v.title_id, v.name_suffix, v.email_address,v.crsid, v.date_of_birth, v.location, v.is_spri) returning id into v_person_id; 
    END IF; 

 perform fn_mm_array_update(v.dept_telephone_number_id,
                            'mm_person_dept_telephone_number'::varchar,
			    'person_id'::varchar,
                            'dept_telephone_number_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.research_group_id,
                            'mm_person_research_group'::varchar,
			    'person_id'::varchar,
                            'research_group_id'::varchar,
                            v_person_id);

 perform fn_mm_array_update(v.room_id,
                            'mm_person_room'::varchar,
                            'person_id'::varchar,
                            'room_id'::varchar,
                            v_person_id);
                                               
      return v_person_id;                                                                                                
      end;                                                

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_personnel_basic_upd(hotwire."10_View/People/Personnel_Basic")
  OWNER TO dev;


--
-- Name: 10_View/People/Photography_Registration; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Photography_Registration" AS
    SELECT person.id, person.crsid AS ro_crsid, person.gender_id, person.first_names, person.known_as, person.surname, person.date_of_birth, person.arrival_date AS ro_arrival_date, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, ROW('image/jpeg'::character varying, (person.image_lo)::bigint)::public.blobtype AS photo FROM (public.person LEFT JOIN public._physical_status_v3 ON ((person.id = _physical_status_v3.person_id))) WHERE (person.is_spri IS NOT TRUE) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."10_View/People/Photography_Registration" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  RETURNS bigint AS
$BODY$
	declare v alias for $1;
		v_person_id bigint;
	begin
		if v.id is not null
		then
			v_person_id = v.id;
			update person set
				gender_id = v.gender_id,
				first_names = v.first_names,
				known_as = v.known_as,
				surname = v.surname,
				date_of_birth = v.date_of_birth,
				image_lo = ( select (v.photo).val as val from ( select v.photo ) x) 
			where person.id = v_person_id;
		else
			insert into person (
				gender_id,
				first_names,
				known_as,
				surname,
				date_of_birth,
                                image_lo
			) values (
				v.gender_id,
				v.first_names,
				v.known_as,
				v.surname,
				v.date_of_birth,
				( select (v.photo).val as val from ( select v.photo ) x)
			) returning id into v_person_id;
		end if;
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);	
		return v_person_id;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_personnel_photography_reg_upd(hotwire."10_View/People/Photography_Registration")
  OWNER TO dev;


--
-- Name: 10_View/Research_Group_Editing; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Research_Group_Editing" AS
    SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, research_group.website_address, research_group.sector_id, ARRAY(SELECT mm_research_group_computer_rep.computer_rep_id FROM public.mm_research_group_computer_rep WHERE (mm_research_group_computer_rep.research_group_id = research_group.id)) AS computer_rep_id, ARRAY(SELECT mm_person_research_group.person_id FROM public.mm_person_research_group WHERE (mm_person_research_group.research_group_id = research_group.id)) AS person_id FROM public.research_group ORDER BY research_group.name;


ALTER TABLE hotwire."10_View/Research_Group_Editing" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_research_group_ins(hotwire."10_View/Research_Group_Editing")
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin

insert into research_group (id, name, head_of_group_id, administrator_id, website_address, sector_id ) values (r_id, r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id ) returning id into r_id;

perform fn_mm_array_update(r.person_id,'mm_person_research_group'::varchar,
                           'research_group_id'::varchar,
                           'person_id'::varchar,
                           r_id);

 perform fn_mm_array_update(r.computer_rep_id,
                           'mm_research_group_computer_rep'::varchar,
                           'research_group_id'::varchar,
                           'computer_rep_id'::varchar,
                           r_id);
                           
return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_research_group_ins(hotwire."10_View/Research_Group_Editing")
  OWNER TO dev;


--
-- Name: 10_View/Research_Groups_Computing; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Research_Groups_Computing" AS
    SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, array_to_string(ARRAY(SELECT person_hid.person_hid FROM ((public.mm_person_research_group JOIN public._physical_status_v3 member_status USING (person_id)) JOIN person_hid USING (person_id)) WHERE ((mm_person_research_group.research_group_id = research_group.id) AND ((member_status.status_id)::text = 'Current'::text))), ';
'::text) AS ro_list_of_members_who_are_current, ARRAY(SELECT mm_person_research_group.person_id FROM public.mm_person_research_group WHERE (mm_person_research_group.research_group_id = research_group.id)) AS member_id, ARRAY(SELECT mm_research_group_computer_rep.computer_rep_id FROM public.mm_research_group_computer_rep WHERE (mm_research_group_computer_rep.research_group_id = research_group.id)) AS computer_rep_id, research_group.website_address, research_group.sector_id, research_group.fai_class_id, research_group.active_directory_container, research_group.internal_use_only, research_group.ignore_for_software_compliance_purposes, research_group.subnet_id, research_group.group_fileserver_id, CASE WHEN ((pi_status.status_id)::text = 'Current'::text) THEN 'Active'::character varying WHEN (has_current_members.has_current_members = true) THEN 'Active'::character varying ELSE 'Inactive'::character varying END AS ro_group_status, CASE WHEN ((pi_status.status_id)::text = 'Current'::text) THEN NULL::text WHEN (has_current_members.has_current_members = true) THEN NULL::text ELSE 'orange'::text END AS _cssclass FROM ((public.research_group JOIN public._physical_status_v3 pi_status ON ((research_group.head_of_group_id = pi_status.person_id))) JOIN (SELECT research_group.id, (('Current'::character varying(20))::text = ANY ((ARRAY(SELECT _physical_status.status_id FROM (public.mm_person_research_group JOIN public._physical_status_v3 _physical_status USING (person_id)) WHERE (mm_person_research_group.research_group_id = research_group.id)))::text[])) AS has_current_members FROM public.research_group) has_current_members USING (id)) ORDER BY research_group.name;


ALTER TABLE hotwire."10_View/Research_Groups_Computing" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_research_groups_computing_upd(hotwire."10_View/Research_Groups_Computing")
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin
 IF r.id is not null then
   update research_group set name=r.name, head_of_group_id=r.head_of_group_id, administrator_id=r.alternate_admin_contact_id,  
          website_address=r.website_address, sector_id=r.sector_id, fai_class_id=r.fai_class_id,
          active_directory_container=r.active_directory_container, internal_use_only=r.internal_use_only, ignore_for_software_compliance_purposes=r.ignore_for_software_compliance_purposes,
          subnet_id=r.subnet_id, group_fileserver_id=r.group_fileserver_id where id=r.id;
   r_id=r.id;
 else
   insert into research_group (name, head_of_group_id, administrator_id, website_address, sector_id, fai_class_id, active_directory_container, internal_use_only, ignore_for_software_compliance_purposes, subnet_id, group_fileserver_id ) values 
               (r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id, r.fai_class_id, r.active_directory_container, coalesce(r.internal_use_only,'f'), coalesce(r.ignore_for_software_compliance_purposes,'f'), r.subnet_id, r.group_fileserver_id ) returning id into r_id;
 end if;

perform fn_mm_array_update(r.member_id,
                           'mm_person_research_group'::varchar,
                           'research_group_id'::varchar,
                           'person_id'::varchar,
                           r_id);
perform fn_mm_array_update(r.computer_rep_id,
                           'mm_research_group_computer_rep'::varchar,
                           'research_group_id'::varchar,
                           'computer_rep_id'::varchar,
                           r_id);
return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_research_groups_computing_upd(hotwire."10_View/Research_Groups_Computing")
  OWNER TO dev;


--
-- Name: 10_View/Research_Interest_Group; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Research_Interest_Group" AS
    SELECT research_interest_group.id, (research_interest_group.name)::character varying(80) AS name, research_interest_group.chair_id, research_interest_group.website, research_interest_group.mailing_list_address, ARRAY(SELECT mm_member_research_interest_group.member_id FROM public.mm_member_research_interest_group WHERE (mm_member_research_interest_group.research_interest_group_id = research_interest_group.id)) AS rig_member_id, ARRAY(SELECT mm_member_research_interest_group.member_id FROM public.mm_member_research_interest_group WHERE ((mm_member_research_interest_group.research_interest_group_id = research_interest_group.id) AND mm_member_research_interest_group.is_primary)) AS primary_member_id FROM public.research_interest_group;


ALTER TABLE hotwire."10_View/Research_Interest_Group" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_research_interest_group_upd(hotwire."10_View/Research_Interest_Group")
  RETURNS bigint AS
$BODY$
       declare v alias for $1;
               v_rig_id bigint;
       begin
                insert into research_interest_group (  name,chair_id,website,mailing_list_address ) values ( v.name,v.chair_id,v.website,v.mailing_list_address)returning id into v_rig_id;
                perform fn_mm_array_update(v.rig_member_id,
                                          'mm_member_research_interest_group'::varchar,
                                          'member_id'::varchar,
                                          'research_interest_group_id'::varchar,
                                          v_rig_id);
                perform fn_primary_rig_update(v.primary_member_id,'{}',v_rig_id);
                return v_rig_id;
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_research_interest_group_upd(hotwire."10_View/Research_Interest_Group")
  OWNER TO dev;

--
-- Name: 10_View/Socket; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Socket" AS
    SELECT socket.id, socket.room_label, socket.panel_label, socket.room_id, socket.patch_panel_id, socket.cable_type_id, socket.connector_type_id, socket.linked_socket_id, switchport.id AS switchport_id FROM (public.socket LEFT JOIN public.switchport ON ((switchport.socket_id = socket.id))) ORDER BY socket.patch_panel_id, socket.panel_label;


ALTER TABLE hotwire."10_View/Socket" OWNER TO cen1001;

CREATE OR REPLACE FUNCTION public.hw_fn_socket_insert_or_update(hotwire."10_View/Socket")
  RETURNS bigint AS
$BODY$
        declare
                s alias for $1;
                s_id bigint;
        begin
                -- updating
                if s.id is not null then
                    s_id = s.id;
                    update socket set
                        room_label= s.room_label,
                        panel_label= s.panel_label,
                        room_id= s.room_id,
                        patch_panel_id= s.patch_panel_id,
                        connector_type_id= s.connector_type_id,
                        linked_socket_id= s.linked_socket_id
                    where socket.id = s_id;

                else
                        s_id := nextval('socket_id_seq');
                -- inserting
                        insert into socket (
                                id, 
                                room_label,
                                panel_label,
                                room_id,
                                patch_panel_id,
                                cable_type_id,
                                connector_type_id,
                                linked_socket_id
                        ) values (
                                s_id, 
                                s.room_label,
                                s.panel_label,
                                s.room_id,
                                s.patch_panel_id,
                                s.cable_type_id,
                                s.connector_type_id,
                                s.linked_socket_id
                        );
                end if;
                -- now do linked sockets and switchports
                if s.switchport_id is not null then
                            update switchport set
                            socket_id = s_id where switchport.id =
                            s.switchport_id;
                end if;
                if s.linked_socket_id is not null then
                        insert into _debug ( debug ) values ( 'update socket set linked_socket_id = ' || s_id );
                        update socket set linked_socket_id = s_id where
                        id = s.linked_socket_id;
                else
                        update socket set linked_socket_id = null where
                        linked_socket_id = s_id;
                end if;
        
                return s_id;
                
        end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_socket_insert_or_update(hotwire."10_View/Socket")
  OWNER TO cen1001;


--
-- Name: 10_View/Network/Switch_With_Rooms; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_With_Rooms" AS
    SELECT switch.id, hardware.name, hardware.id AS _hardware_id, system_image.id AS _system_image_id, hardware.manufacturer, hardware.model, hardware.hardware_type_id, system_image.wired_mac_1, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, hardware.serial_number, hardware.asset_tag, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id AS physical_room_id, hardware.owner_id, system_image.research_group_id, hardware.comments, ARRAY(SELECT mm_switch_serves_room.room_id FROM public.mm_switch_serves_room WHERE (mm_switch_serves_room.switch_id = switch.id)) AS room_id FROM ((public.switch JOIN public.hardware ON ((switch.hardware_id = hardware.id))) JOIN public.system_image ON ((system_image.hardware_id = hardware.id)));


ALTER TABLE hotwire."10_View/Network/Switch_With_Rooms" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_switch_upd(hotwire."10_View/Network/Switch_With_Rooms")
  RETURNS bigint AS
$BODY$
         declare 
                 v alias for $1;
                 v_id bigint;
                 v_system_image_id bigint;
                 v_hardware_id bigint;
                 v_os_id bigint;
         begin
                 -- v_id := nextval('switch_id_seq');
                 v_hardware_id := nextval('hardware_id_seq');
                 insert into _debug ( debug ) values ( v_hardware_id );
                 v_system_image_id := nextval('system_image_id_seq');
                 v_os_id := (select id from operating_system where os = 'None');
                 insert into hardware 
                         ( id, name, manufacturer, model, hardware_type_id,
                         serial_number, asset_tag, date_purchased,
                         date_configured, warranty_end_date,
                         date_decommissioned, warranty_details, room_id,
                         owner_id, comments )
                 values 
                         ( v_hardware_id, v.name, v.manufacturer, v.model,
                         v.hardware_type_id, v.serial_number,
                         v.asset_tag, v.date_purchased, v.date_configured,
                         v.warranty_end_date, v.date_decommissioned,
                         v.warranty_details, v.room_id, v.owner_id,
                         v.comments);
                 insert into system_image
                         ( id, hardware_id, research_group_id, wired_mac_1,
                         operating_system_id)
                 values
                         ( v_system_image_id, v_hardware_id, v.research_group_id, v.wired_mac_1, v_os_id); 
                 insert into _debug ( debug ) values ( v_hardware_id );
                 -- insert into switch
                 --         ( id, hardware_id, switchstack_id)
                 --values
                 --        ( v_id, v_hardware_id, v.switchstack_id );
 
                  perform fn_mm_array_update(v.ip_address_id,
                                            'mm_system_image_ip_address'::varchar,
                                            'ip_address_id'::varchar,
					    'system_image_id'::varchar,
					    v_system_image_id);
                 v_id := ( select id from switch where hardware_id = v_hardware_id );
                 return v_id;
         end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_switch_upd(hotwire."10_View/Network/Switch_With_Rooms")
  OWNER TO dev;


--
-- Name: 10_View/System_Image_All; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/System_Image_All" AS
    SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ((((COALESCE(((system_image.wired_mac_1)::text || ' '::text), ''::text) || COALESCE(((system_image.wired_mac_2)::text || ' '::text), ''::text)) || COALESCE(((system_image.wired_mac_3)::text || ' '::text), ''::text)) || COALESCE(((system_image.wired_mac_4)::text || ' '::text), ''::text)) || COALESCE(((system_image.wireless_mac)::text || ' '::text), ''::text)) AS all_mac_addresses_ro, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, ARRAY(SELECT mm_dom0_domu.dom0_id FROM public.mm_dom0_domu WHERE (mm_dom0_domu.domu_id = system_image.id)) AS host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.hobbit_tier, system_image.hobbit_flags, system_image.is_development_system, system_image.expiry_date, system_image.is_managed_mac, public._to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details", public._to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Hardware_Full'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware", public._to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses" FROM (public.system_image JOIN public.hardware ON ((hardware.id = system_image.hardware_id)));


ALTER TABLE hotwire."10_View/System_Image_All" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_system_image_all_upd(hotwire."10_View/System_Image_All")
  RETURNS bigint AS
$BODY$
 
  declare
          v alias for $1;
          v_system_image_id BIGINT;
          v_hardware_id BIGINT;
  begin
 
  IF v.id IS NOT NULL THEN
 
    v_system_image_id := v.id;
 
    UPDATE hardware SET
         manufacturer=v.manufacturer, 
         model=v.model, 
         name=v.hardware_name,
         hardware_type_id=v.hardware_type_id, 
         asset_tag = v.asset_tag,
         serial_number = v.serial_number,
         monitor_serial_number = v.monitor_serial_number,
         room_id=v.room_id,
         owner_id=v.owner_id,
         comments=v.hardware_comments
    WHERE hardware.id = v._hardware_id;
 
    UPDATE system_image SET
          user_id=v.user_id,
          research_group_id=v.research_group_id,
          operating_system_id = v.operating_system_id,
          wired_mac_1 = v.wired_mac_1, 
          wired_mac_2 = v.wired_mac_2,
          wireless_mac = v.wireless_mac,
          comments = v.system_image_comments,
          hobbit_tier = v.hobbit_tier,
          hobbit_flags = v.hobbit_flags,
          is_development_system = v.is_development_system,
          expiry_date = v.expiry_date,
          is_managed_mac = v.is_managed_mac
    WHERE system_image.id = v.id;
 
  ELSE
 
    v_system_image_id := nextval('system_image_id_seq');
    v_hardware_id := nextval('hardware_id_seq');
 
    INSERT INTO hardware
          (manufacturer, model, name, hardware_type_id, asset_tag, serial_number, monitor_serial_number, room_id, owner_id, comments )
    VALUES
          (v.manufacturer, v.model, v.hardware_name, v.hardware_type_id, v.asset_tag, v.serial_number, v.monitor_serial_number,
           v.room_id, v.owner_id, v.hardware_comments) returning id INTO v_hardware_id;
 
    INSERT INTO system_image
          (hardware_id, operating_system_id, wired_mac_1, wired_mac_2,
  wireless_mac,
  comments,user_id,research_group_id,hobbit_tier,hobbit_flags,is_development_system,expiry_date,is_managed_mac)
    VALUES
          (v_hardware_id, v.operating_system_id,
           v.wired_mac_1, v.wired_mac_2, v.wireless_mac,
           v.system_image_comments,v.user_id,v.research_group_id,v.hobbit_tier,v.hobbit_flags,v.is_development_system,v.expiry_date,v.is_managed_mac) RETURNING id into v_system_image_id;
 
  END IF;
  PERFORM fn_mm_array_update(v.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,v_system_image_id);
 
  -- PERFORM fn_upd_many_many (v_system_image_id, v.mm_ip_address, 'system_image',
  --                         'ip_address');
 
  return v_system_image_id;
 
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_system_image_all_upd(hotwire."10_View/System_Image_All")
  OWNER TO dev;


--
-- Name: 10_View/System_Image_Asset; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/System_Image_Asset" AS
    SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name, hardware.hardware_type_id, hardware.serial_number, hardware.monitor_serial_number, system_image.operating_system_id, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.owner_id, hardware.value_when_new, hardware.personally_owned, hardware.comments FROM (public.system_image JOIN public.hardware ON ((hardware.id = system_image.hardware_id)));


ALTER TABLE hotwire."10_View/System_Image_Asset" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_system_image_asset_ins(hotwire."10_View/System_Image_Asset")
  RETURNS bigint AS
$BODY$

declare
        v alias for $1;
        new_hardware_id BIGINT;
        new_system_image_id BIGINT;
begin

new_hardware_id := nextval('hardware_id_seq');        

insert into hardware
        (id, manufacturer, model, name, hardware_type_id, serial_number, 
 monitor_serial_number, date_purchased, date_configured, 
 warranty_end_date, date_decommissioned, warranty_details,
 owner_id, value_when_new, personally_owned, comments)
        values
        (new_hardware_id, v.manufacturer, v.model, v.name, v.hardware_type_id,
 v.serial_number, v.monitor_serial_number, v.date_purchased,
 v.date_configured, v.warranty_end_date, v.date_decommissioned, 
 v.warranty_details, v.owner_id, v.value_when_new, v.personally_owned, 
 v.comments);

new_system_image_id := nextval('system_image_id_seq');

insert into system_image
        (id, hardware_id, operating_system_id)
values
        (new_system_image_id, new_hardware_id, v.operating_system_id);

return new_system_image_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_system_image_asset_ins(hotwire."10_View/System_Image_Asset")
  OWNER TO dev;


--
-- Name: 10_View/System_Image_User; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/System_Image_User" AS
    SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name, system_image.operating_system_id, system_image.wired_mac_1, system_image.wireless_mac, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, hardware.hardware_type_id, hardware.room_id, system_image.user_id, hardware.owner_id, hardware.personally_owned, hardware.comments FROM (public.system_image JOIN public.hardware ON ((hardware.id = system_image.hardware_id)));


ALTER TABLE hotwire."10_View/System_Image_User" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_system_image_user_upd(hotwire."10_View/System_Image_User")
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         v_system_image_id BIGINT;
 v_hardware_id BIGINT;
 begin

 IF v.id IS NOT NULL THEN

   v_system_image_id := v.id;

   UPDATE hardware SET
         manufacturer=v.manufacturer, model=v.model, name=v.name,
 hardware_type_id=v.hardware_type_id, room_id=v.room_id,
 owner_id=v.owner_id,
 personally_owned=v.personally_owned, comments=v.comments
   WHERE hardware.id = v._hardware_id;

   UPDATE system_image SET
 user_id = v.user_id, operating_system_id = v.operating_system_id,
 wired_mac_1 = v.wired_mac_1, wireless_mac = v.wireless_mac
   WHERE system_image.id = v.id;

 ELSE
 v_system_image_id := nextval('system_image_id_seq');

   INSERT INTO hardware
         (manufacturer, model, name, hardware_type_id, room_id,
         owner_id, personally_owned, comments)
   VALUES
         (v.manufacturer, v.model, v.name, v.hardware_type_id,
          v.room_id, v.owner_id, v.personally_owned, v.comments) returning id into v_hardware_id;

   INSERT INTO system_image
         (hardware_id, user_id, operating_system_id, wired_mac_1, wireless_mac)
   VALUES
         (v_hardware_id, v.user_id, v.operating_system_id,
          v.wired_mac_1, v.wireless_mac) returning id into v_system_image_id;

 END IF;

 perform fn_mm_array_update(v.ip_address_id,
                            'mm_system_image_ip_address'::varchar,
                            'ip_address_id'::varchar,
                            'system_image_id'::varchar,
                            v_system_image_id);
                            
 return v_system_image_id;
 end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_system_image_user_upd(hotwire."10_View/System_Image_User")
  OWNER TO dev;


--
-- Name: 10_View/Telephone_Basic; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Telephone_Basic" AS
    SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, ARRAY(SELECT mm_person_dept_telephone_number.person_id FROM public.mm_person_dept_telephone_number WHERE (mm_person_dept_telephone_number.dept_telephone_number_id = dept_telephone_number.id)) AS person_id, dept_telephone_number.fax FROM public.dept_telephone_number;


ALTER TABLE hotwire."10_View/Telephone_Basic" OWNER TO dev;

CREATE OR REPLACE FUNCTION public.hw_fn_telephone_basic_upd(hotwire."10_View/Telephone_Basic")
  RETURNS bigint AS
$BODY$

declare
        v alias for $1;
        v_dept_telephone_number_id BIGINT;
begin

IF v.id IS NOT NULL THEN

  v_dept_telephone_number_id := v.id;

  UPDATE dept_telephone_number SET
        extension_number=v.extension_number, room_id=v.room_id, fax=v.fax
  WHERE dept_telephone_number.id = v.id;

ELSE

  INSERT INTO dept_telephone_number
        (extension_number, room_id, fax)
  VALUES (v.extension_number, v.room_id, v.fax) returning id into v_dept_telephone_number_id;

END IF;

perform fn_mm_array_update(v.person_id,
                          'mm_person_dept_telephone_number'::varchar,
                          'dept_telephone_number_id'::varchar,
                          'person_id'::varchar,
                          v_dept_telephone_number_id);

return v_dept_telephone_number_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_telephone_basic_upd(hotwire."10_View/Telephone_Basic")
  OWNER TO dev;


--
-- Name: 10_View/Telephone_Management; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Telephone_Management" AS
    SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, dept_telephone_number.fax, dept_telephone_number.personal_line, ARRAY(SELECT mm_person_dept_telephone_number.person_id FROM public.mm_person_dept_telephone_number WHERE (dept_telephone_number.id = mm_person_dept_telephone_number.dept_telephone_number_id)) AS person_id FROM public.dept_telephone_number;


ALTER TABLE hotwire."10_View/Telephone_Management" OWNER TO cen1001;

CREATE OR REPLACE FUNCTION public.hw_fn_telephone_mgmt_upd(hotwire."10_View/Telephone_Management")
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         v_dept_telephone_number_id BIGINT;
 begin
 
 IF v.id IS NOT NULL THEN
 
   v_dept_telephone_number_id := v.id;
 
   UPDATE dept_telephone_number 
   SET
       extension_number= v.extension_number,
       room_id = v.room_id,
       fax = v.fax,
       personal_line = v.personal_line
   WHERE dept_telephone_number.id = v.id;
 
 ELSE
 
   INSERT INTO dept_telephone_number
         (extension_number, room_id, fax, personal_line)
   VALUES
         (v.extension_number, v.room_id, v.fax, v.personal_line) 
   returning id into v_dept_telephone_number_id;
 
 END IF;

 perform fn_mm_array_update(v.person_id,
                            'mm_person_dept_telephone_number'::varchar,
                            'person_id'::varchar,
                            'dept_telephone_number_id'::varchar,
                            v_dept_telephone_number_id);
 
 return v_dept_telephone_number_id;
 
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.hw_fn_telephone_mgmt_upd(hotwire."10_View/Telephone_Management")
  OWNER TO cen1001;


--
-- Name: 10_View/Easy_Add_Virtual_Machine; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Easy_Add_Virtual_Machine" AS
    SELECT (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS id, ''::character varying(63) AS hostname, NULL::integer AS subnet_id, NULL::integer AS hardware_id, NULL::macaddr AS wired_mac_1, (SELECT operating_system_hid.operating_system_id FROM public.operating_system_hid WHERE ((operating_system_hid.operating_system_hid)::text ~~* '%debian%'::text) ORDER BY operating_system_hid.operating_system_id DESC LIMIT 1) AS operating_system_id, ''::character varying AS system_image_comments, NULL::integer AS host_system_image_id, NULL::macaddr AS wired_mac_2, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS user_id, (SELECT mm_person_research_group.research_group_id FROM (public.person JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) WHERE ((person.crsid)::name = "current_user"()) LIMIT 1) AS research_group_id;


ALTER TABLE hotwire."10_View/Easy_Add_Virtual_Machine" OWNER TO dev;

CREATE FUNCTION public.upd_easy_add_virtual_machine(hotwire."10_View/Easy_Add_Virtual_Machine")
  RETURNS bigint AS
$BODY$

     declare          v alias for $1;                                                                                    
                      ip_id BIGINT;                                                                                        
     begin                                                                                                              
	ip_id =  (SELECT ip_address.id
			FROM ip_address
			WHERE ip_address.subnet_id = v.subnet_id 
				AND ip_address.reserved <> true 
				AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
			LIMIT 1);

	INSERT INTO system_image 
		(wired_mac_1, wired_mac_2, hardware_id, 
		operating_system_id, comments, host_system_image_id, 
		user_id, research_group_id) 
		VALUES (v.wired_mac_1, v.wired_mac_2, v.hardware_id, 
		v.operating_system_id, v.system_image_comments, v.host_system_image_id, 
		v.user_id, v.research_group_id);
	update ip_address set hostname = v.hostname where id = ip_id;
	insert into mm_system_image_ip_address ( ip_address_id, system_image_id )
		values ( ip_id, currval('system_image_id_seq'::regclass));
	return currval('system_image_id_seq'::regclass);
	end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.upd_easy_add_virtual_machine(hotwire."10_View/Easy_Add_Virtual_Machine")
  OWNER TO dev;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//01_header; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//01_header" AS
    SELECT it_strategic_goal.id, ((it_strategic_goal.id || ' '::text) || (it_strategic_goal.name)::text) AS "Goals" FROM public.it_strategic_goal;


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//01_header" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//09_hdr; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" AS
    SELECT it_strategic_goal.id, it_strategic_goal.name FROM public.it_strategic_goal WHERE (it_strategic_goal.id = 1);


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" OWNER TO postgres;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//10_projects; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" AS
    SELECT it_task.id, ((it_task.task_number || ' '::text) || (it_task.name)::text) AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", ((('complete'::text || trunc(((10)::numeric * COALESCE(it_task.progress, (0)::numeric)), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", (0)::bigint)) AS _cssclass FROM (public.it_task JOIN public.it_strategic_goal ON ((it_strategic_goal.id = it_task."Primary_strategic_goal_id"))) WHERE (it_task."Primary_strategic_goal_id" = 1) ORDER BY it_strategic_goal.name, ((COALESCE(it_task.priority, 5))::numeric + COALESCE(it_task.weight, 0.9));


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//10_projects" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//19_hdr; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" AS
    SELECT it_strategic_goal.id, it_strategic_goal.name FROM public.it_strategic_goal WHERE (it_strategic_goal.id = 2);


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" OWNER TO postgres;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//20_projects; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" AS
    SELECT it_task.id, ((it_task.task_number || ' '::text) || (it_task.name)::text) AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", ((('complete'::text || trunc(((10)::numeric * COALESCE(it_task.progress, (0)::numeric)), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", (0)::bigint)) AS _cssclass FROM (public.it_task JOIN public.it_strategic_goal ON ((it_strategic_goal.id = it_task."Primary_strategic_goal_id"))) WHERE (it_task."Primary_strategic_goal_id" = 2) ORDER BY it_strategic_goal.name, ((COALESCE(it_task.priority, 5))::numeric + COALESCE(it_task.weight, 0.9));


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//20_projects" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//29_hdr; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" AS
    SELECT it_strategic_goal.id, it_strategic_goal.name FROM public.it_strategic_goal WHERE (it_strategic_goal.id = 3);


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" OWNER TO postgres;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//30_projects; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" AS
    SELECT it_task.id, ((it_task.task_number || ' '::text) || (it_task.name)::text) AS "Description", it_task.priority, it_task.task_notes, it_task."IT_status_id", ((('complete'::text || trunc(((10)::numeric * COALESCE(it_task.progress, (0)::numeric)), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", (0)::bigint)) AS _cssclass FROM (public.it_task JOIN public.it_strategic_goal ON ((it_strategic_goal.id = it_task."Primary_strategic_goal_id"))) WHERE (it_task."Primary_strategic_goal_id" = 3) ORDER BY it_strategic_goal.name, ((COALESCE(it_task.priority, 5))::numeric + COALESCE(it_task.weight, 0.9));


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//30_projects" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//89_hdr; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" AS
    SELECT (0)::bigint AS id, 'Completed projects'::character varying AS description;


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" OWNER TO postgres;

--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//90_completed; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" AS
    SELECT it_task.id, ((it_task.task_number || ' '::text) || (it_task.name)::text) AS "Description", it_task.completed AS "Date Completed", ((('complete'::text || trunc(((10)::numeric * COALESCE(it_task.progress, (0)::numeric)), 0)) || ' status'::text) || COALESCE(it_task."IT_status_id", (0)::bigint)) AS _cssclass FROM public.it_task WHERE (it_task."IT_status_id" = 3);


ALTER TABLE hotwire."10_View/10_IT_Tasks/Report_for_School_IT//90_completed" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/Strategic Goals; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/Strategic Goals" AS
    SELECT it_strategic_goal.id, it_strategic_goal.name FROM public.it_strategic_goal;


ALTER TABLE hotwire."10_View/10_IT_Tasks/Strategic Goals" OWNER TO dev;

--
-- Name: 10_View/10_IT_Tasks/_audit_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/10_IT_Tasks/_audit_ro" AS
    SELECT it_task_audit.id, it_task_audit.operation, it_task_audit.stamp, it_task_audit.username, it_task_audit.oldcols, it_task_audit.newcols, it_task_audit.task_id AS it_task_id FROM public.it_task_audit WHERE ((it_task_audit.tablename = 'it_task'::text) AND (NOT ((it_task_audit.oldcols = ''::text) AND (it_task_audit.newcols = ''::text))));


ALTER TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" OWNER TO dev;

--
-- Name: 10_View/Building_Floors; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Building_Floors" AS
    SELECT building_floor_hid.building_floor_id AS id, building_floor_hid.building_floor_hid AS building_floor FROM public.building_floor_hid;


ALTER TABLE hotwire."10_View/Building_Floors" OWNER TO cen1001;

--
-- Name: 10_View/Building_Regions; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Building_Regions" AS
    SELECT building_region_hid.building_region_id AS id, building_region_hid.building_region_hid AS building_region FROM public.building_region_hid;


ALTER TABLE hotwire."10_View/Building_Regions" OWNER TO cen1001;

--
-- Name: 10_View/Buildings; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Buildings" AS
    SELECT building_hid.building_id AS id, building_hid.building_hid AS building FROM public.building_hid;


ALTER TABLE hotwire."10_View/Buildings" OWNER TO cen1001;

--
-- Name: 10_View/CPGS_Submission; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/CPGS_Submission" AS
    SELECT a.id, a.person_id, a._surname, a._first_names, a.postgraduate_studentship_type_id, a.cpgs_or_mphil_date_submission_due, a.cpgs_or_mphil_date_submitted, a.cpgs_or_mphil_date_awarded, a.mphil_date_submission_due, a.mphil_date_submitted, a.mphil_date_awarded, a.supervisor_id FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, person.surname AS _surname, person.first_names AS _first_names, postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.cpgs_or_mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submitted, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.mphil_date_submitted, postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.first_supervisor_id AS supervisor_id FROM ((public.postgraduate_studentship LEFT JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((person.id = postgraduate_studentship.person_id))) WHERE ((_postgrad_end_dates_v5.status_id)::text = 'Current'::text)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."10_View/CPGS_Submission" OWNER TO cen1001;

--
-- Name: 10_View/Computers/System_Image/_Contact_Details_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Computers/System_Image/_Contact_Details_ro" AS
    SELECT (all_people.person_id || (all_people.role)::text) AS id, system_image.id AS system_image_id, all_people.person_id, all_people.role, person.email_address, dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone, room_hid.room_hid AS mm_room, 'personnel_basic_view'::text AS _targetview FROM ((((((public.system_image JOIN ((SELECT system_image.id, hardware.owner_id AS person_id, 'Hardware owner'::character varying(20) AS role FROM (public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) UNION SELECT system_image.id, system_image.user_id AS person_id, 'System user'::character varying(20) AS role FROM public.system_image WHERE (system_image.user_id IS NOT NULL)) UNION SELECT system_image.id, mm_research_group_computer_rep.computer_rep_id AS person_id, 'Group computer rep'::character varying(20) AS role FROM ((public.system_image JOIN public.research_group ON ((system_image.research_group_id = research_group.id))) JOIN public.mm_research_group_computer_rep ON ((mm_research_group_computer_rep.research_group_id = research_group.id)))) all_people USING (id)) JOIN public.person ON ((all_people.person_id = person.id))) LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id));


ALTER TABLE hotwire."10_View/Computers/System_Image/_Contact_Details_ro" OWNER TO cen1001;

--
-- Name: 10_View/Computers/System_Image/_Hardware_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Computers/System_Image/_Hardware_ro" AS
    SELECT system_image.hardware_id AS id, system_image.id AS system_image_id, hardware.id AS hardware_id, hardware.name, hardware.manufacturer, hardware.model, room_hid.room_hid, 'hardware_basic_view'::text AS _targetview FROM ((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) LEFT JOIN public.room_hid USING (room_id));


ALTER TABLE hotwire."10_View/Computers/System_Image/_Hardware_ro" OWNER TO dev;

--
-- Name: 10_View/Computers/System_Image/_IP_address_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Computers/System_Image/_IP_address_ro" AS
    SELECT ip_address.id, system_image.id AS system_image_id, ip_address.ip AS "IP", ip_address.hostname FROM ((public.ip_address JOIN public.mm_system_image_ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) JOIN public.system_image ON ((mm_system_image_ip_address.system_image_id = system_image.id)));


ALTER TABLE hotwire."10_View/Computers/System_Image/_IP_address_ro" OWNER TO cen1001;

--
-- Name: 10_View/DNS/ANames; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/DNS/ANames" AS
    SELECT dns_anames.id, dns_anames.name, dns_anames.dns_domain_id, dns_anames.ip_address_id FROM public.dns_anames;


ALTER TABLE hotwire."10_View/DNS/ANames" OWNER TO cen1001;

--
-- Name: 10_View/DNS/CNames; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/DNS/CNames" AS
    SELECT dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date, dns_cnames.research_group_id FROM public.dns_cnames;


ALTER TABLE hotwire."10_View/DNS/CNames" OWNER TO postgres;

--
-- Name: 10_View/DNS/MX_Records; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/DNS/MX_Records" AS
    SELECT dns_mxrecords.id, dns_mxrecords.domainname, dns_mxrecords.mailhost FROM public.dns_mxrecords;


ALTER TABLE hotwire."10_View/DNS/MX_Records" OWNER TO cen1001;

--
-- Name: 10_View/Database/Access_Control_Groups; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Database/Access_Control_Groups" AS
    WITH acl_groups AS (SELECT r.rolname AS role, member.rolname AS member, person.id AS person_id FROM (((pg_roles r JOIN pg_auth_members m ON ((m.roleid = r.oid))) JOIN pg_roles member ON ((m.member = member.oid))) LEFT JOIN public.person ON ((member.rolname = (person.crsid)::name)))) SELECT row_number() OVER (ORDER BY acl_groups.role, acl_groups.member, acl_groups.person_id) AS id, acl_groups.role, acl_groups.member, acl_groups.person_id FROM acl_groups WHERE (acl_groups.role <> '_autoadded'::name) ORDER BY acl_groups.role, acl_groups.member;


ALTER TABLE hotwire."10_View/Database/Access_Control_Groups" OWNER TO dev;

--
-- Name: 10_View/Database/Access_Control_Lists; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Database/Access_Control_Lists" AS
    WITH acls AS (SELECT c.relname AS "Name", array_to_string(c.relacl, '
'::text) AS "Access privileges" FROM (pg_class c LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE ((c.relkind = 'v'::"char") AND (n.nspname ~ '^(hotwire)$'::text)) ORDER BY c.relname, array_to_string(c.relacl, '
'::text)) SELECT row_number() OVER (ORDER BY acls."Name") AS id, ltrim((acls."Name")::text, '0123456789_'::text) AS "Name", acls."Access privileges" FROM acls;


ALTER TABLE hotwire."10_View/Database/Access_Control_Lists" OWNER TO dev;

--
-- Name: 10_View/Database/Database_Users; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Database/Database_Users" AS
    WITH dbusers AS (SELECT r.rolname AS rolename, ARRAY(SELECT b.rolname FROM (pg_auth_members m JOIN pg_roles b ON ((m.roleid = b.oid))) WHERE ((m.member = r.oid) AND (b.rolname <> '_autoadded'::name))) AS member_of, person_hid.person_hid AS person FROM ((pg_roles r LEFT JOIN public.person ON ((r.rolname = (person.crsid)::name))) LEFT JOIN public.person_hid ON ((person.id = person_hid.person_id)))) SELECT row_number() OVER () AS id, dbusers.rolename, dbusers.member_of, dbusers.person FROM dbusers ORDER BY dbusers.rolename, dbusers.member_of, dbusers.person;


ALTER TABLE hotwire."10_View/Database/Database_Users" OWNER TO dev;

--
-- Name: easy_addable_subnet_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW easy_addable_subnet_hid AS
    SELECT subnet.id AS easy_addable_subnet_id, (((((subnet.network_address || ' ('::text) || (subnet.notes)::text) || ', '::text) || (SELECT count(*) AS count FROM public.ip_address WHERE (((ip_address.subnet_id = subnet.id) AND (NOT ip_address.reserved)) AND ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text))))) || ' free)'::text) AS easy_addable_subnet_hid FROM public.subnet WHERE (subnet.can_easy_add = true) ORDER BY subnet.notes;


ALTER TABLE hotwire.easy_addable_subnet_hid OWNER TO dev;

--
-- Name: 10_View/Easy_Add_Machine; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Easy_Add_Machine" AS
    SELECT (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS id, ''::character varying(63) AS hostname, (SELECT easy_addable_subnet_hid.easy_addable_subnet_id FROM easy_addable_subnet_hid LIMIT 1) AS easy_addable_subnet_id, NULL::macaddr AS wired_mac_1, (SELECT operating_system_hid.operating_system_id FROM public.operating_system_hid WHERE ((operating_system_hid.operating_system_hid)::text = 'Windows 10'::text) ORDER BY operating_system_hid.operating_system_id DESC LIMIT 1) AS operating_system_id, 'Unknown'::character varying(20) AS manufacturer, 'Unknown'::character varying(20) AS model, ''::character varying(20) AS hardware_name, (SELECT hardware_type_hid.hardware_type_id FROM public.hardware_type_hid WHERE ((hardware_type_hid.hardware_type_hid)::text = 'PC'::text) LIMIT 1) AS hardware_type_id, ''::character varying(80) AS system_image_comments, NULL::integer AS host_system_image_id, (SELECT mm_person_room.room_id FROM (public.person JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) WHERE ((person.crsid)::name = "current_user"()) LIMIT 1) AS room_id, NULL::macaddr AS wired_mac_2, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS user_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS owner_id, (SELECT mm_person_research_group.research_group_id FROM (public.person JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) WHERE ((person.crsid)::name = "current_user"()) LIMIT 1) AS research_group_id;


ALTER TABLE hotwire."10_View/Easy_Add_Machine" OWNER TO dev;

--
-- Name: 10_View/Easy_Add_Switch; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Easy_Add_Switch" AS
    SELECT (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS id, NULL::character varying(30) AS name, (SELECT ip_address.id FROM (public.ip_address JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) WHERE ((((subnet.domain_name)::text ~~ '%-switches%'::text) AND (ip_address.hostname IS NULL)) AND (ip_address.reserved <> true)) ORDER BY ip_address.ip LIMIT 1) AS switch_ip_id, NULL::macaddr AS wired_mac_1, (SELECT operating_system.id FROM public.operating_system WHERE ((operating_system.os)::text ~~ 'HP-SwitchOS'::text) LIMIT 1) AS operating_system_id, 'HP'::character varying(20) AS manufacturer, NULL::character varying(80) AS serial_number, NULL::character varying(80) AS asset_tag, (SELECT hardware_type.id FROM public.hardware_type WHERE ((hardware_type.name)::text ~~ 'Data Switch'::text) LIMIT 1) AS switch_hardware_type_id, NULL::bigint AS switch_model_id, NULL::bigint AS room_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS user_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) AS owner_id, (SELECT mm_person_research_group.research_group_id FROM (public.person JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) WHERE ((person.crsid)::name = "current_user"()) LIMIT 1) AS research_group_id;


ALTER TABLE hotwire."10_View/Easy_Add_Switch" OWNER TO dev;

--
-- Name: 10_View/Edit_Hardware_Types; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Edit_Hardware_Types" AS
    SELECT hardware_type.id, hardware_type.name, hardware_type.subnet_id FROM public.hardware_type;


ALTER TABLE hotwire."10_View/Edit_Hardware_Types" OWNER TO cen1001;

--
-- Name: 10_View/Edit_List_of_Titles; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Edit_List_of_Titles" AS
    SELECT title_hid.title_id AS id, title_hid.title_hid AS title FROM public.title_hid;


ALTER TABLE hotwire."10_View/Edit_List_of_Titles" OWNER TO cen1001;

--
-- Name: 10_View/Edit_Operating_Systems; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Edit_Operating_Systems" AS
    SELECT operating_system.id, operating_system.os, operating_system.nmap_identifies_as, operating_system.path_to_autoinstaller, operating_system.default_key FROM public.operating_system;


ALTER TABLE hotwire."10_View/Edit_Operating_Systems" OWNER TO cen1001;

--
-- Name: 10_View/Group_Computers; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Group_Computers" AS
    SELECT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ip_address_hid.ip_address_hid AS ro_ip_address, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id AS ro_research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.auto_banned_from_network AS ro_auto_banned_from_network, system_image.override_auto_ban AS ro_override_auto_ban, system_image.network_ban_log AS ro_network_ban_log, system_image.was_managed_machine_on AS ro_was_managed_machine_on FROM ((((((((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) LEFT JOIN public.mm_system_image_ip_address ON ((system_image.id = mm_system_image_ip_address.system_image_id))) LEFT JOIN public.ip_address_hid USING (ip_address_id)) JOIN public.mm_research_group_computer_rep USING (research_group_id)) JOIN public.person ON ((mm_research_group_computer_rep.computer_rep_id = person.id))) JOIN public.research_group ON ((system_image.research_group_id = research_group.id))) JOIN public.person head_of_gp ON ((research_group.head_of_group_id = head_of_gp.id))) LEFT JOIN public.person deputy ON ((research_group.deputy_head_of_group_id = deputy.id))) WHERE ((((person.crsid)::name = "current_user"()) OR ((head_of_gp.crsid)::name = "current_user"())) OR ((deputy.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_View/Group_Computers" OWNER TO dev;

--
-- Name: 10_View/Group_Computers_Autoinstaller; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Group_Computers_Autoinstaller" AS
    SELECT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.reinstall_on_next_boot, system_image.architecture_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ARRAY(SELECT mm_system_image_installer_tag.installer_tag_id FROM public.mm_system_image_installer_tag WHERE (mm_system_image_installer_tag.system_image_id = system_image.id)) AS installer_tag_id, research_group.fai_class_id AS ro_fai_class_id, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id AS ro_research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.was_managed_machine_on AS ro_was_managed_machine_on FROM ((((((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) JOIN public.mm_research_group_computer_rep USING (research_group_id)) JOIN public.person ON ((mm_research_group_computer_rep.computer_rep_id = person.id))) JOIN public.research_group ON ((system_image.research_group_id = research_group.id))) JOIN public.person head_of_gp ON ((research_group.head_of_group_id = head_of_gp.id))) LEFT JOIN public.person deputy ON ((research_group.deputy_head_of_group_id = deputy.id))) WHERE ((((person.crsid)::name = "current_user"()) OR ((head_of_gp.crsid)::name = "current_user"())) OR ((deputy.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_View/Group_Computers_Autoinstaller" OWNER TO dev;

--
-- Name: 10_View/Group_Fileservers; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Group_Fileservers" AS
    SELECT group_fileserver.id, group_fileserver.hostname_for_users, group_fileserver.system_image_id, group_fileserver.homepath, group_fileserver.localhomepath, group_fileserver.profilepath, group_fileserver.localprofilepath, operating_system.os_class_id FROM ((public.group_fileserver JOIN public.system_image ON ((group_fileserver.system_image_id = system_image.id))) JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id)));


ALTER TABLE hotwire."10_View/Group_Fileservers" OWNER TO cen1001;

--
-- Name: 10_View/Hardware_Basic; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Hardware_Basic" AS
    SELECT hardware.id, hardware.manufacturer, hardware.model, hardware.name, hardware.room_id, hardware.hardware_type_id, hardware.owner_id AS person_id FROM public.hardware;


ALTER TABLE hotwire."10_View/Hardware_Basic" OWNER TO cen1001;

--
-- Name: 10_View/Hardware_Full; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Hardware_Full" AS
    SELECT hardware.id, hardware.manufacturer, hardware.model, hardware.name, hardware.serial_number, hardware.monitor_serial_number, hardware.asset_tag, hardware.processor_type, hardware.processor_speed, hardware.number_of_disks, hardware.value_when_new, hardware.date_purchased, hardware.date_configured, hardware.date_decommissioned, hardware.delete_after_date, hardware.warranty_end_date, hardware.warranty_details, hardware.room_id, hardware.hardware_type_id, hardware.owner_id AS person_id, hardware.rackmount, hardware.redundant_psu, hardware.power, hardware.height FROM public.hardware;


ALTER TABLE hotwire."10_View/Hardware_Full" OWNER TO cen1001;

--
-- Name: 10_View/Hobbit/Clients_Cfg; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Hobbit/Clients_Cfg" AS
    SELECT hobbit_clients_config_stanza.id, hobbit_clients_config_stanza.system_image_id, hobbit_clients_config_stanza.handle, hobbit_clients_config_stanza.rules, hobbit_clients_config_stanza.settings FROM public.hobbit_clients_config_stanza;


ALTER TABLE hotwire."10_View/Hobbit/Clients_Cfg" OWNER TO dev;

--
-- Name: 10_View/Hotwire_Test; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Hotwire_Test" AS
    SELECT (1)::bigint AS id, (1)::bigint AS "BigInt", (0)::bit(1) AS "Bit", false AS "Boolean", 'c'::character(1) AS "Character", 'hello'::character varying(25) AS "Varchar(25)", (now())::date AS "Date", (now())::time without time zone AS "Time", '127.0.0.1/24'::inet AS "IP Address", '00:01:02:03:04:05'::macaddr AS "MAC Address", (1745795)::oid AS image_oid;


ALTER TABLE hotwire."10_View/Hotwire_Test" OWNER TO postgres;

--
-- Name: 10_View/IP_address; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/IP_address" AS
    SELECT ip_address.id, ip_address.ip, ip_address.hostname, ip_address.subnet_id, ip_address.hobbit_flags FROM public.ip_address WHERE (NOT ip_address.reserved);


ALTER TABLE hotwire."10_View/IP_address" OWNER TO dev;

--
-- Name: 10_View/IT_Sales/Sales; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/IT_Sales/Sales" AS
    SELECT itsales_item.id, itsales_item.itsales_type_id, itsales_item.research_group_id, itsales_item.person_id, itsales_item.quantity, itsales_item.itsales_unit_id, itsales_item.delivery_date, itsales_item.expires_date, itsales_item.notes, itsales_item.itsales_status_id, itsales_item.unit_cost, itsales_item.charge_account FROM public.itsales_item;


ALTER TABLE hotwire."10_View/IT_Sales/Sales" OWNER TO dev;

--
-- Name: 10_View/Mailing_Lists/Chemistry_Mail_Domain; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Mailing_Lists/Chemistry_Mail_Domain" AS
    SELECT mail_alias.mail_alias_id AS id, mail_alias.alias, mail_alias.addresses, mail_alias.comment FROM public.mail_alias ORDER BY mail_alias.alias;


ALTER TABLE hotwire."10_View/Mailing_Lists/Chemistry_Mail_Domain" OWNER TO cen1001;

--
-- Name: 10_View/Mailing_Lists/OptIns; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Mailing_Lists/OptIns" AS
    SELECT mailinglist.id, mailinglist.name, mailinglist.generation_view_name AS ro_generation_view_name, mailinglist.notes, (pg_views.definition)::character varying(1000) AS ro_generation_view_code, (mailinglist.extra_addresses)::character varying(3000) AS extra_addresses, ARRAY(SELECT mm_mailinglist_include_person.include_person_id FROM public.mm_mailinglist_include_person WHERE (mm_mailinglist_include_person.mailinglist_id = mailinglist.id)) AS include_person_id, mailinglist.auto_upload FROM (public.mailinglist LEFT JOIN pg_views ON ((pg_views.viewname = (mailinglist.generation_view_name)::name)));


ALTER TABLE hotwire."10_View/Mailing_Lists/OptIns" OWNER TO cen1001;

--
-- Name: 10_View/Mailing_Lists/OptOuts; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Mailing_Lists/OptOuts" AS
    SELECT mailinglist.id, mailinglist.name, mailinglist.notes, mailinglist.generation_view_name AS ro_generation_view_name, (pg_views.definition)::character varying(1000) AS ro_generation_view_code, (mailinglist.extra_addresses)::character varying(3000) AS extra_addresses, ARRAY(SELECT mm_mailinglist_exclude_person.exclude_person_id FROM public.mm_mailinglist_exclude_person WHERE (mm_mailinglist_exclude_person.mailinglist_id = mailinglist.id)) AS exclude_person_id, mailinglist.auto_upload FROM (public.mailinglist LEFT JOIN pg_views ON ((pg_views.viewname = (mailinglist.generation_view_name)::name)));


ALTER TABLE hotwire."10_View/Mailing_Lists/OptOuts" OWNER TO cen1001;

--
-- Name: 10_View/Network/Cabinet; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Cabinet" AS
    SELECT cabinet.id, cabinet.name, cabinet.email_local_part, cabinet.notes, cabinet.room_id, cabinet.cabno, cabinet.in_use, cabinet.depth, cabinet.fibre_u, cabinet.patch_u, cabinet.pots_u, cabinet.other_u, cabinet.height_u, cabinet.ports, cabinet.max_ports, cabinet.sw48, cabinet.sw24, cabinet.cabname, public._to_hwsubviewb('10_View/Network/Fibre_Panel'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panel'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panel", public._to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/10_Fibre_run'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs", public._to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Core'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores" FROM public.cabinet;


ALTER TABLE hotwire."10_View/Network/Cabinet" OWNER TO dev;

--
-- Name: 10_View/Network/DB_Admin/Fibre Core Type; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/DB_Admin/Fibre Core Type" AS
    SELECT fibre_core_type.id, fibre_core_type.name, fibre_core_type.fibre_type_id FROM public.fibre_core_type;


ALTER TABLE hotwire."10_View/Network/DB_Admin/Fibre Core Type" OWNER TO postgres;

--
-- Name: 10_View/Network/Fibre_Panel; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Fibre_Panel" AS
    SELECT fibre_panel.id, fibre_panel.cabinet_id, fibre_panel.name, fibre_panel.termination_type_id AS fibre_termination_id FROM public.fibre_panel;


ALTER TABLE hotwire."10_View/Network/Fibre_Panel" OWNER TO dev;

--
-- Name: 10_View/Network/Fibres/10_Fibre_run; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/Fibres/10_Fibre_run" AS
    SELECT fibre_run.id, fibre_run.fibre_type, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id, fibre_run.quantity, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_run.notes, public._to_hwsubviewb('10_View/Network/Fibres/_FibreCore_ro'::character varying, 'fibre_run_id'::character varying, '10_View/Network/Fibres/15_Fibre_Core'::character varying, NULL::character varying, NULL::character varying) AS "Cores" FROM public.fibre_run;


ALTER TABLE hotwire."10_View/Network/Fibres/10_Fibre_run" OWNER TO postgres;

--
-- Name: 10_View/Network/Fibres/15_Fibre_Core; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/Fibres/15_Fibre_Core" AS
    SELECT fibre_core.id, fibre_core.fibre_run_id, fibre_core.fibre_core_type_id, fibre_core.label_a, fibre_core.label_b, fibre_core.illuminate, public._to_hwsubviewb('10_View/Network/Fibres/Tests'::character varying, 'fibre_core_id'::character varying, '10_View/Network/Fibres/Tests'::character varying, NULL::character varying, NULL::character varying) AS "Tests" FROM public.fibre_core;


ALTER TABLE hotwire."10_View/Network/Fibres/15_Fibre_Core" OWNER TO postgres;

--
-- Name: 10_View/Network/Fibres/Tests; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Fibres/Tests" AS
    SELECT fibre_test.fibre_core_id, fibre_test.fibre_test_time, fibre_test.fibre_test_type_id, fibre_test.fibre_test_result, fibre_test.passed, fibre_test.id FROM public.fibre_test;


ALTER TABLE hotwire."10_View/Network/Fibres/Tests" OWNER TO dev;

--
-- Name: 10_View/Network/Fibres/_FibreCore_for_Cabinet_ro; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/Fibres/_FibreCore_for_Cabinet_ro" AS
    SELECT fibre_core.id, fibre_panel.cabinet_id, fibre_core.id AS fibre_core_id, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_core.fibre_core_type_id, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id, fibre_run.notes, (SELECT fibre_test.passed FROM public.fibre_test WHERE (fibre_test.fibre_core_id = fibre_core.id) ORDER BY fibre_test.fibre_test_time DESC LIMIT 1) AS passed, fibre_core.illuminate FROM ((public.fibre_panel JOIN public.fibre_run ON (((fibre_panel.id = fibre_run.panel_a_id) OR (fibre_panel.id = fibre_run.panel_b_id)))) JOIN public.fibre_core ON ((fibre_core.fibre_run_id = fibre_run.id)));


ALTER TABLE hotwire."10_View/Network/Fibres/_FibreCore_for_Cabinet_ro" OWNER TO postgres;

--
-- Name: fibre_core_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_core_type_hid AS
    SELECT fibre_core_type.id AS fibre_core_type_id, fibre_core_type.name AS fibre_core_type_hid FROM public.fibre_core_type;


ALTER TABLE hotwire.fibre_core_type_hid OWNER TO postgres;

--
-- Name: fibre_panel_a_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_panel_a_hid AS
    SELECT fibre_panel.id AS fibre_panel_a_id, (((cabinet.cabname)::text || '.'::text) || (fibre_panel.name)::text) AS fibre_panel_a_hid FROM (public.fibre_panel JOIN public.cabinet ON ((fibre_panel.cabinet_id = cabinet.id)));


ALTER TABLE hotwire.fibre_panel_a_hid OWNER TO postgres;

--
-- Name: fibre_panel_b_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_panel_b_hid AS
    SELECT fibre_panel.id AS fibre_panel_b_id, (((cabinet.cabname)::text || '.'::text) || (fibre_panel.name)::text) AS fibre_panel_b_hid FROM (public.fibre_panel JOIN public.cabinet ON ((fibre_panel.cabinet_id = cabinet.id)));


ALTER TABLE hotwire.fibre_panel_b_hid OWNER TO postgres;

--
-- Name: fibre_core_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_core_hid AS
    SELECT fibre_core.id AS fibre_core_id, ((((((((fibre_panel_a_hid.fibre_panel_a_hid || '.'::text) || (fibre_core.label_a)::text) || '-'::text) || fibre_panel_b_hid.fibre_panel_b_hid) || '.'::text) || (fibre_core.label_b)::text) || ' - '::text) || (fibre_core_type_hid.fibre_core_type_hid)::text) AS fibre_core_hid FROM ((((public.fibre_core JOIN fibre_core_type_hid USING (fibre_core_type_id)) JOIN public.fibre_run ON ((fibre_run.id = fibre_core.fibre_run_id))) JOIN fibre_panel_a_hid ON ((fibre_run.panel_a_id = fibre_panel_a_hid.fibre_panel_a_id))) JOIN fibre_panel_b_hid ON ((fibre_run.panel_b_id = fibre_panel_b_hid.fibre_panel_b_id)));


ALTER TABLE hotwire.fibre_core_hid OWNER TO postgres;

--
-- Name: 10_View/Network/Fibres/_FibreCore_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Fibres/_FibreCore_ro" AS
    SELECT fibre_core_hid.fibre_core_id AS id, fibre_core.fibre_run_id, fibre_core_hid.fibre_core_hid, fibre_core.label_a, fibre_core.label_b, (SELECT fibre_test.fibre_test_time FROM public.fibre_test WHERE (fibre_test.fibre_core_id = fibre_core.id) ORDER BY fibre_test.fibre_test_time DESC LIMIT 1) AS fibre_test_time, (SELECT fibre_test.fibre_test_type_id FROM public.fibre_test WHERE (fibre_test.fibre_core_id = fibre_core.id) ORDER BY fibre_test.fibre_test_time DESC LIMIT 1) AS fibre_test_type_id, (SELECT fibre_test.passed FROM public.fibre_test WHERE (fibre_test.fibre_core_id = fibre_core.id) ORDER BY fibre_test.fibre_test_time DESC LIMIT 1) AS passed, fibre_core.illuminate FROM (public.fibre_core JOIN fibre_core_hid ON ((fibre_core_hid.fibre_core_id = fibre_core.id)));


ALTER TABLE hotwire."10_View/Network/Fibres/_FibreCore_ro" OWNER TO dev;

--
-- Name: 10_View/Network/Fibres/_FibreRuns_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Fibres/_FibreRuns_ro" AS
    SELECT fibre_run.id, fibre_panel.cabinet_id, fibre_run.panel_a_id AS fibre_panel_a_id, fibre_run.panel_b_id AS fibre_panel_b_id, fibre_run.fibre_type, fibre_run.quantity, fibre_run.term_a_id AS fibre_termination_a_id, fibre_run.term_b_id AS fibre_termination_b_id FROM (public.fibre_panel JOIN public.fibre_run ON (((fibre_panel.id = fibre_run.panel_a_id) OR (fibre_panel.id = fibre_run.panel_b_id))));


ALTER TABLE hotwire."10_View/Network/Fibres/_FibreRuns_ro" OWNER TO dev;

--
-- Name: 10_View/Network/GBIC; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/GBIC" AS
    SELECT network_gbic.id, network_gbic.name, network_gbic.description, network_gbic.network_electrical_connection_id, network_gbic.fibre_type_id FROM public.network_gbic;


ALTER TABLE hotwire."10_View/Network/GBIC" OWNER TO postgres;

--
-- Name: 10_View/Network/MAC_to_VLAN; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/MAC_to_VLAN" AS
    SELECT a.id, a.mac, a.vlan_vid_id, a.infraction_type_id, a.ticket, a."Infraction notes", a.last_change, a."Logged change" FROM (SELECT shadow_mac_to_vlan.id, shadow_mac_to_vlan.mac, shadow_mac_to_vlan.vid AS vlan_vid_id, NULL::integer AS infraction_type_id, NULL::integer AS ticket, NULL::character varying AS "Infraction notes", (SELECT ((((((to_char(network_vlan_change."timestamp", 'YYYY-MM-DD HH24:MI:SS'::text) || ' : '::text) || (infraction_type_hid.infraction_type_hid)::text) || ' ['::text) || (network_vlan_change.actor)::text) || ']'::text))::character varying AS history FROM (public.network_vlan_change JOIN public.infraction_type_hid USING (infraction_type_id)) WHERE (network_vlan_change.mac = shadow_mac_to_vlan.mac) ORDER BY network_vlan_change."timestamp" DESC LIMIT 1) AS last_change, public._to_hwsubviewb('10_View/Network/_vlanchange_ro'::character varying, '_shmacid'::character varying, NULL::character varying, NULL::character varying, NULL::character varying) AS "Logged change" FROM public.shadow_mac_to_vlan) a ORDER BY a.last_change DESC NULLS LAST;


ALTER TABLE hotwire."10_View/Network/MAC_to_VLAN" OWNER TO dev;

--
-- Name: 10_View/Network/Subnet; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Subnet" AS
    SELECT subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id AS domain_for_dns_server_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1 AS ro_live_dns1, subnet.dns2 AS ro_live_dns2, subnet.dns3 AS ro_live_dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes FROM public.subnet;


ALTER TABLE hotwire."10_View/Network/Subnet" OWNER TO dev;

--
-- Name: 10_View/Network/Switch; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch" AS
    SELECT switch.id, (hardware.name)::character varying(40) AS name, hardware.id AS _hardware_id, system_image.id AS _system_image_id, (hardware.manufacturer)::character varying(60) AS manufacturer, hardware.hardware_type_id, system_image.wired_mac_1, switch.switch_model_id, switch.ignore_config, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, (hardware.serial_number)::character varying(40) AS serial_number, (hardware.asset_tag)::character varying(40) AS asset_tag, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id, hardware.owner_id, system_image.research_group_id, hardware.comments, ARRAY(SELECT mm_switch_goal_applies_to_switch.switch_config_goal_id FROM public.mm_switch_goal_applies_to_switch WHERE (mm_switch_goal_applies_to_switch.switch_id = switch.id)) AS switch_config_goal_id, switch.extraconfig, public._to_hwsubviewb('10_View/Network/_Switchport_ro'::character varying, 'switch_id'::character varying, '10_View/Network/Switch_Port'::character varying, NULL::character varying, NULL::character varying) AS "Ports", public._to_hwsubviewb('10_View/Network/_switch_config_subview'::character varying, 'id'::character varying, '10_View/Network/Switch_Configs_ro'::character varying, NULL::character varying, NULL::character varying) AS "Config", (('<a href="http://netdisco.ch.private.cam.ac.uk/netdisco/device.html?ip='::text || (SELECT ip_address.ip FROM (public.mm_system_image_ip_address JOIN public.ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (mm_system_image_ip_address.system_image_id = system_image.id) LIMIT 1)) || '"> Netdisco</a>'::text) AS "Netdisco view_html" FROM ((public.switch JOIN public.hardware ON ((switch.hardware_id = hardware.id))) JOIN public.system_image ON ((system_image.hardware_id = hardware.id)));


ALTER TABLE hotwire."10_View/Network/Switch" OWNER TO dev;

--
-- Name: 10_View/Network/Switch Config Fragments; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch Config Fragments" AS
    SELECT switch_config_fragment.id, switch_config_fragment.name, switch_config_fragment.config, switch_config_fragment.switch_config_goal_id, ARRAY(SELECT mm_switch_config_fragment_switch_model.switch_model_id FROM public.mm_switch_config_fragment_switch_model WHERE (mm_switch_config_fragment_switch_model.switch_config_fragment_id = switch_config_fragment.id)) AS switch_model_id FROM public.switch_config_fragment ORDER BY switch_config_fragment.name;


ALTER TABLE hotwire."10_View/Network/Switch Config Fragments" OWNER TO dev;

--
-- Name: 10_View/Network/Switch Models; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/Switch Models" AS
    SELECT switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports FROM public.switch_model;


ALTER TABLE hotwire."10_View/Network/Switch Models" OWNER TO postgres;

--
-- Name: 10_View/Network/Switch_Config_Goals; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_Config_Goals" AS
    SELECT switch_config_goal.id, switch_config_goal.name, ARRAY(SELECT mm_switch_goal_applies_to_switch.switch_id FROM public.mm_switch_goal_applies_to_switch WHERE (mm_switch_goal_applies_to_switch.switch_config_goal_id = switch_config_goal.id)) AS switch_id FROM public.switch_config_goal;


ALTER TABLE hotwire."10_View/Network/Switch_Config_Goals" OWNER TO dev;

--
-- Name: 10_View/Network/Switch_Configs_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_Configs_ro" AS
    SELECT switch.id, array_to_string(ARRAY(SELECT public._switch_config_b(switch.id) AS _switch_config), ''::text) AS ro_config FROM public.switch;


ALTER TABLE hotwire."10_View/Network/Switch_Configs_ro" OWNER TO dev;

--
-- Name: 10_View/Network/Switch_Port; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_Port" AS
    SELECT switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY(SELECT mm_switch_config_switch_port.switch_port_config_goal_id FROM public.mm_switch_config_switch_port WHERE (mm_switch_config_switch_port.switch_port_id = switchport.id)) AS switch_port_goal_id FROM public.switchport;


ALTER TABLE hotwire."10_View/Network/Switch_Port" OWNER TO dev;

--
-- Name: 10_View/Network/Switch_Port_Config_Fragments; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_Port_Config_Fragments" AS
    SELECT switch_port_config_fragment.id, switch_port_config_fragment.switch_port_config_goal_id, switch_port_config_fragment.config, ARRAY(SELECT mm_switch_port_config_fragment_switch_model.switch_model_id FROM public.mm_switch_port_config_fragment_switch_model WHERE (mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id)) AS switch_model_id FROM public.switch_port_config_fragment;


ALTER TABLE hotwire."10_View/Network/Switch_Port_Config_Fragments" OWNER TO dev;

--
-- Name: 10_View/Network/Switch_Port_Config_Goals; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/Switch_Port_Config_Goals" AS
    SELECT switch_port_config_goal.id, switch_port_config_goal.name, switch_port_config_goal.goal_class AS goal_class_id FROM public.switch_port_config_goal;


ALTER TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" OWNER TO dev;

--
-- Name: 10_View/Network/VLAN; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/VLAN" AS
    SELECT vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid FROM public.vlan ORDER BY vlan.vid;


ALTER TABLE hotwire."10_View/Network/VLAN" OWNER TO dev;

--
-- Name: speed_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW speed_hid AS
    SELECT switch_port_config_goal.id AS speed_id, switch_port_config_goal.name AS speed_hid FROM public.switch_port_config_goal WHERE (switch_port_config_goal.goal_class = 2);


ALTER TABLE hotwire.speed_hid OWNER TO dev;

--
-- Name: switch_auth_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW switch_auth_hid AS
    SELECT switch_port_config_goal.id AS switch_auth_id, switch_port_config_goal.name AS switch_auth_hid FROM public.switch_port_config_goal WHERE (switch_port_config_goal.goal_class = 1);


ALTER TABLE hotwire.switch_auth_hid OWNER TO dev;

--
-- Name: 10_View/Network/_Switchport_ro; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/_Switchport_ro" AS
    SELECT switchport.id, switchport.switch_id, switchport.name, switchport.socket_id, switchport.ifacename, switch_auth_hid.switch_auth_hid, speed_hid.speed_hid FROM ((public.switchport LEFT JOIN switch_auth_hid USING (switch_auth_id)) LEFT JOIN speed_hid USING (speed_id)) ORDER BY CASE WHEN ((switchport.name)::text ~ '^[A-Za-z]+'::text) THEN ((10 * ascii(substr((switchport.name)::text, 1, 1))) + (regexp_replace((switchport.name)::text, '[A-Za-z]+'::text, ''::text))::integer) ELSE (switchport.name)::integer END;


ALTER TABLE hotwire."10_View/Network/_Switchport_ro" OWNER TO dev;

--
-- Name: 10_View/Network/_switch_config_subview; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Network/_switch_config_subview" AS
    SELECT switch.id, 'Config for switch - click here to show'::character varying AS "Link to config" FROM public.switch;


ALTER TABLE hotwire."10_View/Network/_switch_config_subview" OWNER TO dev;

--
-- Name: 10_View/Network/_vlanchange_ro; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/Network/_vlanchange_ro" AS
    SELECT a.id, a._shmacid, a.infraction_type_id, a.ticket, a.notes, a.actor, a.time_stamp, a.old_vid, a.new_vid FROM (SELECT network_vlan_change.id, network_vlan_change.shadow_mac_to_vlan_id AS _shmacid, network_vlan_change.infraction_type_id, network_vlan_change.ticket, network_vlan_change.notes, network_vlan_change.actor, network_vlan_change."timestamp" AS time_stamp, network_vlan_change.old_vid, network_vlan_change.new_vid FROM public.network_vlan_change) a ORDER BY a.time_stamp DESC;


ALTER TABLE hotwire."10_View/Network/_vlanchange_ro" OWNER TO postgres;

--
-- Name: 10_View/Patch_Panel; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Patch_Panel" AS
    SELECT patch_panel.id, patch_panel.name, patch_panel.cabinet_id, patch_panel.patch_panel_type_id FROM public.patch_panel;


ALTER TABLE hotwire."10_View/Patch_Panel" OWNER TO dev;

--
-- Name: 10_View/People/All_Contact_Details; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/All_Contact_Details" AS
    WITH contact_details AS (SELECT person.id, ROW('image/jpeg'::character varying, (person.image_lo)::bigint)::public.blobtype AS image_oid, person.surname, person.first_names, person.title_id, person.known_as, person.name_suffix, person.previous_surname, person.gender_id, _latest_role.post_category_id AS ro_post_category_id, person.crsid, person.email_address, person.arrival_date, person.leaving_date, _physical_status.status_id AS ro_physical_status_id, person.left_but_no_leaving_date_given, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, person.hide_phone_no_from_website, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (person.id = mm_person_room.person_id)) AS room_id, _latest_role.supervisor_id AS ro_supervisor_id, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, person.external_work_phone_numbers, person.cambridge_address, person.cambridge_phone_number, person.cambridge_college_id, person.mobile_number, person.emergency_contact, person.location, person.forwarding_address, person.new_employer_address, ARRAY(SELECT mm_person_nationality.nationality_id FROM public.mm_person_nationality WHERE (person.id = mm_person_nationality.person_id)) AS nationality_id, CASE WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass FROM ((public.person LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public._physical_status_v3 _physical_status ON ((person.id = _physical_status.person_id)))) SELECT contact_details.id, contact_details.id AS ro_person_id, contact_details.image_oid, contact_details.surname, contact_details.first_names, contact_details.title_id, contact_details.known_as, contact_details.name_suffix, contact_details.previous_surname, contact_details.gender_id, contact_details.ro_post_category_id, contact_details.crsid, contact_details.email_address, contact_details.arrival_date, contact_details.leaving_date, contact_details.ro_physical_status_id, contact_details.left_but_no_leaving_date_given, contact_details.dept_telephone_number_id, contact_details.room_id, contact_details.research_group_id, contact_details.external_work_phone_numbers, contact_details.cambridge_address AS home_address, contact_details.cambridge_phone_number AS home_phone_number, contact_details.cambridge_college_id, contact_details.mobile_number, contact_details.emergency_contact, contact_details.location, contact_details.forwarding_address, contact_details.new_employer_address, contact_details.nationality_id, contact_details._cssclass FROM contact_details ORDER BY contact_details.surname, contact_details.first_names;


ALTER TABLE hotwire."10_View/People/All_Contact_Details" OWNER TO dev;

--
-- Name: 10_View/People/Personnel_History; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Personnel_History" AS
    WITH personnel_history AS (SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age((person.date_of_birth)::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, (age((COALESCE(_latest_employment.end_date, ('now'::text)::date))::timestamp with time zone, (person.continuous_employment_start_date)::timestamp with time zone))::text AS ro_length_of_continuous_service, (age((COALESCE(_latest_employment.end_date, ('now'::text)::date))::timestamp with time zone, (_latest_employment.start_date)::timestamp with time zone))::text AS ro_length_of_service_in_current_contract, age((_latest_employment.end_date)::timestamp with time zone, (person.continuous_employment_start_date)::timestamp with time zone) AS ro_final_length_of_continuous_service, age((_latest_employment.end_date)::timestamp with time zone, (_latest_employment.start_date)::timestamp with time zone) AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, (person.other_information)::character varying(5000) AS other_information, (person.notes)::character varying(5000) AS notes, emplids.emplid_number AS ro_emplid_number, CASE WHEN ((_physical_status.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass, public._to_hwsubviewb('10_View/People/Staff_Review_History'::character varying, 'person_id'::character varying, '10_View/People/Staff_Review_History'::character varying, NULL::character varying, NULL::character varying) AS staff_review_history_subview, public._to_hwsubviewb('10_View/Roles/_Cambridge_History_V2'::character varying, 'person_id'::character varying, '_target_viewname'::character varying, '_role_xid'::character varying, NULL::character varying) AS cambridge_history_subview FROM (((((public.person LEFT JOIN public._latest_role_v12 _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public._latest_employment ON ((person.id = _latest_employment.person_id))) LEFT JOIN public.person_hid ON ((person_hid.person_id = person.id))) LEFT JOIN public._physical_status_v3 _physical_status ON ((_physical_status.person_id = person.id))) LEFT JOIN (SELECT all_emplids.person_id, (string_agg((all_emplids.emplid_number)::text, '/'::text))::character varying AS emplid_number FROM (SELECT DISTINCT postgraduate_studentship.person_id, postgraduate_studentship.emplid_number FROM public.postgraduate_studentship WHERE (postgraduate_studentship.emplid_number IS NOT NULL)) all_emplids GROUP BY all_emplids.person_id) emplids ON ((person.id = emplids.person_id)))) SELECT personnel_history.id, personnel_history.ro_person, personnel_history.surname, personnel_history.first_names, personnel_history.title_id, personnel_history.ro_age, personnel_history.ro_post_category_id, personnel_history.continuous_employment_start_date, personnel_history.ro_latest_employment_start_date, personnel_history.ro_employment_end_date, personnel_history.ro_length_of_continuous_service, personnel_history.ro_length_of_service_in_current_contract, personnel_history.ro_final_length_of_continuous_service, personnel_history.ro_final_length_of_service_in_last_contract, personnel_history.ro_supervisor_id, personnel_history.other_information, personnel_history.notes, personnel_history.ro_emplid_number, personnel_history._cssclass, personnel_history.staff_review_history_subview, personnel_history.cambridge_history_subview FROM personnel_history ORDER BY personnel_history.surname, personnel_history.first_names;


ALTER TABLE hotwire."10_View/People/Personnel_History" OWNER TO dev;

--
-- Name: 10_View/People/Research_Group_Members; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/People/Research_Group_Members" AS
    SELECT a.id, a.research_group_id, a.person, a.post_category_id, a.start_date, a.end_date, a.mm_room, a.mm_phone, a.email_address, a._surname, a._first_names FROM (SELECT person.id, mm_person_research_group.research_group_id, person_hid.person_hid AS person, _latest_role_v8.post_category_id, person.arrival_date AS start_date, COALESCE(_latest_role_v8.end_date, _latest_role_v8.intended_end_date) AS end_date, room_hid.room_hid AS mm_room, dept_telephone_number_hid.dept_telephone_number_hid AS mm_phone, person.email_address, person.surname AS _surname, person.first_names AS _first_names FROM ((((((((public.person JOIN public.person_hid ON ((person.id = person_hid.person_id))) JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public.room_hid USING (room_id)) LEFT JOIN public.mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN public.dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN public._latest_role_v8 ON ((person.id = _latest_role_v8.person_id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."10_View/People/Research_Group_Members" OWNER TO cen1001;

--
-- Name: 10_View/People/Room_Occupants; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/People/Room_Occupants" AS
    SELECT a.id, a.room_id, a.person_hid, a.supervisor_id, a.post_category_id, a.start_date, a.end_date, a._supervisor FROM (SELECT person.id, mm_person_room.room_id, person_hid.person_hid, _latest_role_v8.supervisor_id, _latest_role_v8.post_category_id, person.arrival_date AS start_date, COALESCE(_latest_role_v8.end_date, _latest_role_v8.intended_end_date) AS end_date, supervisor_hid.person_hid AS _supervisor FROM (((((public.person LEFT JOIN public.person_hid ON ((person.id = person_hid.person_id))) LEFT JOIN public.mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN public._latest_role_v8 ON ((person.id = _latest_role_v8.person_id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) LEFT JOIN public.person_hid supervisor_hid ON ((_latest_role_v8.supervisor_id = person_hid.person_id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text)) a ORDER BY a._supervisor;


ALTER TABLE hotwire."10_View/People/Room_Occupants" OWNER TO cen1001;

--
-- Name: 10_View/People/Staff_Review_History; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Staff_Review_History" AS
    SELECT staff_review_meeting.id, person.id AS person_id, staff_review_meeting.date_of_meeting, person.next_review_due, staff_review_meeting.reviewer_id, staff_review_meeting.notes FROM (public.staff_review_meeting JOIN public.person ON ((staff_review_meeting.person_id = person.id)));


ALTER TABLE hotwire."10_View/People/Staff_Review_History" OWNER TO dev;

--
-- Name: 10_View/People/Swipe_Cards; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/People/Swipe_Cards" AS
    SELECT misd_card.card_id AS id, misd_card.surname, misd_card.forenames, misd_card.card_id AS card, misd_card.dob, misd_card.grade, misd_card.crsid, misd_card.usn, misd_card.issued, misd_card.expires, misd_card.issuenum, misd_card.mifarenum, misd_card.mifareid, misd_card.barcode FROM public.misd_card;


ALTER TABLE hotwire."10_View/People/Swipe_Cards" OWNER TO postgres;

--
-- Name: 10_View/People/Website_View; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/People/Website_View" AS
    SELECT person.id, person.crsid AS ro_crsid, CASE WHEN person.hide_email THEN NULL::character varying ELSE person.email_address END AS ro_email_address, ROW('image/jpeg'::character varying, (person.image_lo)::bigint)::public.blobtype AS ro_row, www_person_hid_v2.www_person_hid AS ro_display_name, (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text) AS ro_name_for_sorting, cambridge_college.name AS ro_college_name, cambridge_college.website AS ro_college_website, telephone_numbers.telephone_numbers AS ro_telephone_numbers, CASE WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR (person.counts_as_academic IS TRUE)) THEN 'Academic'::text WHEN (((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR (person.counts_as_postdoc IS TRUE)) AND (person.counts_as_academic IS NOT TRUE)) THEN 'Postdoc'::text WHEN (_latest_role.post_category_id = 'sc-2'::text) THEN 'Academic-Related'::text WHEN ((_latest_role.post_category_id = 'v-8'::text) AND (person.counts_as_academic IS NOT TRUE)) THEN 'Emeritus'::text ELSE 'Other'::text END AS ro_post_category, person.website_staff_category_id, person.counts_as_academic AS ro_counts_as_academic, s.status_id AS ro_physical_status, r.rigs AS ro_rigs, pr.primary_rig AS ro_primary_rig, rgs.research_groups AS ro_research_groups, rgs.website_addresses AS ro_website_addresses, post_history.job_title FROM (((((((((public.person LEFT JOIN public.www_person_hid_v2 ON ((www_person_hid_v2.person_id = person.id))) LEFT JOIN www.person_research_groups rgs ON ((person.id = rgs.person_id))) LEFT JOIN www.telephone_numbers ON ((person.id = telephone_numbers.id))) LEFT JOIN public.cambridge_college ON ((person.cambridge_college_id = cambridge_college.id))) LEFT JOIN cache._latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public._physical_status_v3 s ON ((s.person_id = person.id))) LEFT JOIN www.rig_membership_v1 r ON ((r.id = person.id))) LEFT JOIN www.primary_rig_membership_v1 pr ON ((pr.id = person.id))) LEFT JOIN public.post_history ON ((post_history.id = _latest_role.role_id))) WHERE (((person.do_not_show_on_website IS FALSE) AND (person.is_spri IS NOT TRUE)) AND (_latest_role.role_tablename = 'post_history'::text)) ORDER BY (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text);


ALTER TABLE hotwire."10_View/People/Website_View" OWNER TO dev;

--
-- Name: 10_View/Postgrad_Mentor_Meetings; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Postgrad_Mentor_Meetings" AS
    SELECT mentor_meeting.id, postgraduate_studentship.id AS student_name_id, postgraduate_studentship.first_mentor_id AS usual_mentor_id, mentor_meeting.date_of_meeting, mentor_meeting.mentor_id, postgraduate_studentship.next_mentor_meeting_due, mentor_meeting.notes FROM (public.mentor_meeting JOIN public.postgraduate_studentship ON ((postgraduate_studentship.id = mentor_meeting.postgraduate_studentship_id))) WHERE (mentor_meeting.postgraduate_studentship_id IS NOT NULL);


ALTER TABLE hotwire."10_View/Postgrad_Mentor_Meetings" OWNER TO cen1001;

--
-- Name: 10_View/Postgrad_Training; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Postgrad_Training" AS
    SELECT a.id, a.person_id, a.ro_surname, a.ro_first_names, a.postgraduate_studentship_type_id, a.supervisor_id, a.start_date, a.ro_status_id, a.funding, a.fees_funding, a.transferable_skills_days_1st_year, a.transferable_skills_days_2nd_year, a.transferable_skills_days_3rd_year, a.transferable_skills_days_4th_year, a.total_days_ro FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, person.surname AS ro_surname, person.first_names AS ro_first_names, postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.first_supervisor_id AS supervisor_id, postgraduate_studentship.start_date, _postgrad_end_dates_v5.status_id AS ro_status_id, postgraduate_studentship.filemaker_funding AS funding, postgraduate_studentship.filemaker_fees_funding AS fees_funding, postgraduate_studentship.transferable_skills_days_1st_year, postgraduate_studentship.transferable_skills_days_2nd_year, postgraduate_studentship.transferable_skills_days_3rd_year, postgraduate_studentship.transferable_skills_days_4th_year, (((COALESCE(postgraduate_studentship.transferable_skills_days_1st_year, (0)::real) + COALESCE(postgraduate_studentship.transferable_skills_days_2nd_year, (0)::real)) + COALESCE(postgraduate_studentship.transferable_skills_days_3rd_year, (0)::real)) + COALESCE(postgraduate_studentship.transferable_skills_days_4th_year, (0)::real)) AS total_days_ro FROM ((public.postgraduate_studentship LEFT JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((person.id = postgraduate_studentship.person_id)))) a ORDER BY a.ro_surname, a.ro_first_names;


ALTER TABLE hotwire."10_View/Postgrad_Training" OWNER TO cen1001;

--
-- Name: 10_View/Printers; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Printers" AS
    SELECT printer.id, hardware.id AS _hardware_id, hardware.name, hardware.manufacturer, hardware.model, hardware.date_purchased, hardware.serial_number, hardware.asset_tag, hardware.log_usage, hardware.usage_url, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id, hardware.owner_id, printer.printer_class_id, printer.cups_options FROM (public.printer JOIN public.hardware ON ((printer.hardware_id = hardware.id)));


ALTER TABLE hotwire."10_View/Printers" OWNER TO cen1001;

--
-- Name: 10_View/RFLtest; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "10_View/RFLtest" AS
    SELECT rfltest.id, rfltest.t FROM public.rfltest;


ALTER TABLE hotwire."10_View/RFLtest" OWNER TO postgres;

--
-- Name: 10_View/Registration_and_Leaving; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Registration_and_Leaving" AS
    SELECT a.id, a.ro_person, a.date_starting, a.registration_completed, a.date_left, a.clearance_cert_signed, a._surname, a._first_names FROM (SELECT person.id, person_hid.person_hid AS ro_person, person.arrival_date AS date_starting, person.registration_completed, person.leaving_date AS date_left, person.clearance_cert_signed, person.surname AS _surname, person.first_names AS _first_names FROM (public.person LEFT JOIN public.person_hid ON ((person.id = person_hid.person_id)))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."10_View/Registration_and_Leaving" OWNER TO cen1001;

--
-- Name: 10_View/Research_Group_Test2_Editing; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Research_Group_Test2_Editing" AS
    SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, research_group.website_address, research_group.sector_id, ARRAY(SELECT mm_person_research_group.person_id FROM public.mm_person_research_group WHERE (mm_person_research_group.research_group_id = research_group.id)) AS person_id, ARRAY(SELECT mm_research_group_computer_rep.computer_rep_id FROM public.mm_research_group_computer_rep WHERE (mm_research_group_computer_rep.research_group_id = research_group.id)) AS computer_rep_id, public._to_hwsubview('research_group_id'::text, '10_View/People/Research_Group_Members'::text, 'id'::text) AS research_group_members_subview FROM public.research_group ORDER BY research_group.name;


ALTER TABLE hotwire."10_View/Research_Group_Test2_Editing" OWNER TO cen1001;

--
-- Name: 10_View/Research_Group_Test_Editing; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Research_Group_Test_Editing" AS
    SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, research_group.website_address, research_group.sector_id FROM public.research_group ORDER BY research_group.name;


ALTER TABLE hotwire."10_View/Research_Group_Test_Editing" OWNER TO cen1001;

--
-- Name: 10_View/Research_Groups/Computer_reps; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Research_Groups/Computer_reps" AS
    SELECT rg.id, rg.name AS group_name, rg.head_of_group_id, ARRAY(SELECT mm.computer_rep_id FROM public.mm_research_group_computer_rep mm WHERE (mm.research_group_id = rg.id)) AS computer_rep_id FROM (((public.research_group rg JOIN public.person head_of_group ON ((rg.head_of_group_id = head_of_group.id))) LEFT JOIN public.person deputy_head_of_group ON ((rg.deputy_head_of_group_id = deputy_head_of_group.id))) LEFT JOIN public.person administrator ON ((rg.administrator_id = administrator.id))) WHERE (((head_of_group.crsid)::name = "current_user"()) OR ("current_user"() = ANY ((ARRAY(SELECT rep.crsid FROM (public.mm_research_group_computer_rep mm JOIN public.person rep ON ((mm.computer_rep_id = rep.id))) WHERE (mm.research_group_id = rg.id)))::name[])));


ALTER TABLE hotwire."10_View/Research_Groups/Computer_reps" OWNER TO dev;

--
-- Name: 10_View/Research_Groups/Software_Licence_Declaration; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Research_Groups/Software_Licence_Declaration" AS
    WITH q AS (SELECT research_group.id, research_group.name AS ro_name, research_group.head_of_group_id AS ro_head_of_group_id, 'I have instructed my group that that all software used by members of the group must be properly licensed and the group members have certified to me that that the software they use is properly licensed'::character varying(500) AS ro_software_policy_declaration, CASE WHEN (latest_software_declaration.declaration_date IS NULL) THEN false WHEN ((latest_software_declaration.declaration_date + '1 year'::interval) > ('now'::text)::date) THEN true ELSE false END AS declaration_made, latest_software_declaration.declaration_date AS ro_date_of_last_declaration, latest_software_declaration.username_of_signer AS ro_declared_by FROM (((((public.research_group LEFT JOIN (SELECT group_software_licence_declarations.research_group_id, max(group_software_licence_declarations.declaration_date) AS declaration_date FROM public.group_software_licence_declarations GROUP BY group_software_licence_declarations.research_group_id) latest_software_declaration_date ON ((latest_software_declaration_date.research_group_id = research_group.id))) LEFT JOIN public.group_software_licence_declarations latest_software_declaration ON (((latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date) AND (latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id)))) JOIN public.person head_of_group ON ((research_group.head_of_group_id = head_of_group.id))) JOIN public._physical_status_v3 ps ON ((head_of_group.id = ps.person_id))) LEFT JOIN (SELECT members.head_of_group_id, count(members.person_id) AS members FROM (SELECT _latest_role.supervisor_id AS head_of_group_id, _latest_role.person_id FROM (cache._latest_role JOIN public._physical_status_v3 ps USING (person_id)) WHERE ((ps.status_id)::text = 'Current'::text) UNION SELECT _latest_role.co_supervisor_id AS head_of_group_id, _latest_role.person_id FROM (cache._latest_role JOIN public._physical_status_v3 ps USING (person_id)) WHERE ((ps.status_id)::text = 'Current'::text)) members GROUP BY members.head_of_group_id) group_members ON ((group_members.head_of_group_id = head_of_group.id))) WHERE (("current_user"() = (head_of_group.crsid)::name) OR ((pg_has_role("current_user"(), 'cos'::name, 'member'::text) AND (((ps.status_id)::text = 'Current'::text) OR (group_members.members > 0))) AND (research_group.ignore_for_software_compliance_purposes IS NOT TRUE)))) SELECT q.id, q.ro_name, q.ro_head_of_group_id, q.ro_software_policy_declaration, q.declaration_made, q.ro_date_of_last_declaration, q.ro_declared_by FROM q ORDER BY q.ro_name;


ALTER TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" OWNER TO dev;

--
-- Name: 10_View/Research_Groups_Computing_Computers; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Research_Groups_Computing_Computers" AS
    SELECT system_image.id, research_group.id AS research_group_id, system_image.id AS system_image_id, hardware.manufacturer, hardware.model, hardware.room_id, system_image.user_id, hardware.owner_id, hardware.asset_tag, hardware.serial_number, operating_system.os, ARRAY(SELECT ((((COALESCE(ip_address.hostname, ''::character varying))::text || ' ('::text) || ip_address.ip) || ')'::text) FROM ((public.ip_address JOIN public.mm_system_image_ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) JOIN public.system_image s2 ON ((mm_system_image_ip_address.system_image_id = s2.id))) WHERE (s2.id = system_image.id)) AS ip_addresses FROM (((public.system_image JOIN public.research_group ON ((system_image.research_group_id = research_group.id))) JOIN public.hardware ON ((hardware.id = system_image.hardware_id))) JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id)));


ALTER TABLE hotwire."10_View/Research_Groups_Computing_Computers" OWNER TO cen1001;

--
-- Name: 10_View/Roles/Erasmus_Students; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Erasmus_Students" AS
    SELECT a.id, a.person_id, a.erasmus_type_id, a.supervisor_id, a.email_address, a.start_date, a.intended_end_date, a.end_date, a.cambridge_college_id, a.university, a.ro_role_status_id, a.notes, a.force_role_status_to_past, a._surname, a._first_names FROM (SELECT erasmus_socrates_studentship.id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.supervisor_id, person.email_address, erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, person.cambridge_college_id, erasmus_socrates_studentship.university, _all_roles.status AS ro_role_status_id, erasmus_socrates_studentship.notes, erasmus_socrates_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names FROM ((public.erasmus_socrates_studentship JOIN public.person ON ((person.id = erasmus_socrates_studentship.person_id))) LEFT JOIN public._all_roles_v12 _all_roles ON (((_all_roles.role_id = erasmus_socrates_studentship.id) AND (_all_roles.role_tablename = 'erasmus_socrates_studentship'::text))))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."10_View/Roles/Erasmus_Students" OWNER TO dev;

--
-- Name: 10_View/Roles/Part_III_Students; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Part_III_Students" AS
    SELECT a.id, a.person_id, a.project_title_html, a.supervisor_id, a.intended_end_date, a.end_date, a.start_date, a.ro_role_status_id, a.notes, a.force_role_status_to_past, a._surname, a._first_names FROM (SELECT part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title AS project_title_html, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, _all_roles.status AS ro_role_status_id, part_iii_studentship.notes, part_iii_studentship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names FROM ((public.part_iii_studentship LEFT JOIN public._all_roles_v12 _all_roles ON ((_all_roles.role_id = part_iii_studentship.id))) LEFT JOIN public.person ON ((person.id = part_iii_studentship.person_id))) WHERE (_all_roles.role_tablename = 'part_iii_studentship'::text)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."10_View/Roles/Part_III_Students" OWNER TO dev;

--
-- Name: 10_View/Roles/Post_History; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Post_History" AS
    SELECT a.id, a.staff_category_id, a.person_id, a.ro_role_status_id, a.supervisor_id, a.mentor_id, a.external_mentor, a.start_date_for_continuous_employment_purposes, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.chem, a.paid_through_payroll, a.hours_worked, a.percentage_of_fulltime_hours, a.research_grant_number, a.sponsor, a.probation_period, a.date_of_first_probation_meeting, a.date_of_second_probation_meeting, a.date_of_third_probation_meeting, a.date_of_fourth_probation_meeting, a.first_probation_meeting_comments, a.second_probation_meeting_comments, a.third_probation_meeting_comments, a.fourth_probation_meeting_comments, a.probation_outcome, a.date_probation_letters_sent, a.job_title, a.force_role_status_to_past, a._person FROM (SELECT post_history.id, post_history.staff_category_id, post_history.person_id, _all_roles.status AS ro_role_status_id, post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university AS paid_through_payroll, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.research_grant_number, post_history.filemaker_funding AS sponsor, post_history.job_title, post_history.force_role_status_to_past, person_hid.person_hid AS _person FROM (((public.post_history LEFT JOIN public._all_roles_v13 _all_roles ON (((post_history.id = _all_roles.role_id) AND (_all_roles.role_tablename = 'post_history'::text)))) JOIN public.person ON ((post_history.person_id = person.id))) JOIN public.person_hid ON ((post_history.person_id = person_hid.person_id)))) a ORDER BY a._person, a.start_date DESC;


ALTER TABLE hotwire."10_View/Roles/Post_History" OWNER TO dev;

--
-- Name: 10_View/Roles/Postgrad_Students; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Postgrad_Students" AS
    WITH a AS (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, person.surname AS ro_surname, person.first_names AS ro_first_names, _postgrad_end_dates_v5.status_id AS ro_role_status_id, person.email_address AS ro_email_address, postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.part_time, postgraduate_studentship.first_supervisor_id AS supervisor_id, postgraduate_studentship.second_supervisor_id AS co_supervisor_id, postgraduate_studentship.external_co_supervisor, postgraduate_studentship.substitute_supervisor_id, postgraduate_studentship.first_mentor_id AS mentor_id, postgraduate_studentship.second_mentor_id AS co_mentor_id, postgraduate_studentship.external_mentor, postgraduate_studentship.next_mentor_meeting_due, postgraduate_studentship.university, person.cambridge_college_id, postgraduate_studentship.college_history, postgraduate_studentship.cpgs_title AS cpgs_title_html, postgraduate_studentship.msc_title AS msc_title_html, postgraduate_studentship.mphil_title AS mphil_title_html, postgraduate_studentship.phd_thesis_title AS phd_thesis_title_html, postgraduate_studentship.first_year_probationary_report_title AS first_year_probationary_report_title_html, postgraduate_studentship.paid_through_payroll, postgraduate_studentship.emplid_number, postgraduate_studentship.gaf_number, postgraduate_studentship.start_date, person.leaving_date, postgraduate_studentship.cpgs_or_mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submitted, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.first_year_probationary_report_due, postgraduate_studentship.first_year_probationary_report_submitted, postgraduate_studentship.first_year_probationary_report_approved, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.mphil_date_submitted, postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.msc_date_submission_due, postgraduate_studentship.msc_date_submitted, postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.phd_date_title_approved, postgraduate_studentship.thesis_submission_due_date AS "PhD_three_year_submission_date", postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date, postgraduate_studentship.date_phd_submission_due AS "Revised_PhD_submission_date", postgraduate_studentship.phd_date_submitted, postgraduate_studentship.phd_date_examiners_appointed, postgraduate_studentship.phd_date_of_viva, postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register, postgraduate_studentship.date_reinstated_on_register, postgraduate_studentship.intended_end_date, postgraduate_studentship.filemaker_funding AS funding, postgraduate_studentship.filemaker_fees_funding AS fees_funding, postgraduate_studentship.funding_end_date, _postgrad_end_dates_v5.phd_duration AS ro_phd_duration, _postgrad_end_dates_v5.phd_duration_days AS ro_phd_duration_days, postgraduate_studentship.progress_notes, postgraduate_studentship.force_role_status_to_past, ARRAY(SELECT mm2.nationality_id FROM (public.person p3 JOIN public.mm_person_nationality mm2 ON ((p3.id = mm2.person_id))) WHERE (p3.id = postgraduate_studentship.person_id)) AS nationality_id, CASE WHEN ((_postgrad_end_dates_v5.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass, (((('<a href="edit.php?_view=10_View/People/Personnel_Data_Entry&_id='::text || postgraduate_studentship.person_id) || '">Personnel Data Entry</a>&nbsp;<a href="edit.php?_view=10_View/People/Personnel_History&_id='::text) || postgraduate_studentship.person_id) || '">Personnel History</a>'::text) AS ro_shortcuts_html FROM ((public.postgraduate_studentship JOIN public._postgrad_end_dates_v5 USING (id)) JOIN public.person ON ((postgraduate_studentship.person_id = person.id)))) SELECT a.id, a.person_id, a.ro_surname, a.ro_first_names, a.ro_role_status_id, a.ro_email_address, a.postgraduate_studentship_type_id, a.part_time, a.supervisor_id, a.co_supervisor_id, a.external_co_supervisor, a.substitute_supervisor_id, a.mentor_id, a.co_mentor_id, a.external_mentor, a.next_mentor_meeting_due, a.university, a.cambridge_college_id, a.college_history, a.cpgs_title_html, a.first_year_probationary_report_title_html, a.msc_title_html, a.mphil_title_html, a.phd_thesis_title_html, a.paid_through_payroll, a.emplid_number, a.gaf_number, a.nationality_id, a.start_date, a.leaving_date, a.cpgs_or_mphil_date_submission_due, a.cpgs_or_mphil_date_submitted, a.cpgs_or_mphil_date_awarded, a.first_year_probationary_report_due, a.first_year_probationary_report_submitted, a.first_year_probationary_report_approved, a.mphil_date_submission_due, a.mphil_date_submitted, a.mphil_date_awarded, a.msc_date_submission_due, a.msc_date_submitted, a.msc_date_awarded, a.date_registered_for_phd, a.phd_date_title_approved, a."PhD_three_year_submission_date", a.phd_four_year_submission_date, a."Revised_PhD_submission_date", a.phd_date_submitted, a.phd_date_examiners_appointed, a.phd_date_of_viva, a.phd_date_awarded, a.date_withdrawn_from_register, a.date_reinstated_on_register, a.intended_end_date, a.funding, a.fees_funding, a.funding_end_date, a.ro_phd_duration, a.ro_phd_duration_days, a.progress_notes, a.force_role_status_to_past, a._cssclass, a.ro_shortcuts_html FROM a ORDER BY a.ro_surname, a.ro_first_names;


ALTER TABLE hotwire."10_View/Roles/Postgrad_Students" OWNER TO dev;

--
-- Name: 10_View/Roles/Visitors; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/Visitors" AS
    WITH person_visitor AS (SELECT visitorship.id, visitorship.person_id, _all_roles.status AS ro_role_status_id, visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.notes, visitorship.force_role_status_to_past, person.surname AS _surname, person.first_names AS _first_names FROM ((public.visitorship LEFT JOIN public._all_roles_v12 _all_roles ON (((_all_roles.role_id = visitorship.id) AND (_all_roles.role_tablename = 'visitorship'::text)))) LEFT JOIN public.person ON ((person.id = visitorship.person_id)))) SELECT person_visitor.id, person_visitor.person_id, person_visitor.ro_role_status_id, person_visitor.home_institution, person_visitor.visitor_type_id, person_visitor.host_id, person_visitor.start_date, person_visitor.intended_end_date, person_visitor.end_date, person_visitor.notes, person_visitor.force_role_status_to_past, person_visitor._surname, person_visitor._first_names FROM person_visitor ORDER BY person_visitor._surname, person_visitor._first_names;


ALTER TABLE hotwire."10_View/Roles/Visitors" OWNER TO dev;

--
-- Name: 10_View/Roles/_Cambridge_History_V2; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/_Cambridge_History_V2" AS
    SELECT a.id, a.person_id, a.start_date, a.intended_end_date, a.end_date, a.funding_end_date, a.post_category, a.job_title, a.supervisor_id, a.funding, a.fees_funding, a.research_grant_number, a.paid_through_payroll, a.role_id AS _role_xid, a.role_target_viewname AS _target_viewname FROM (SELECT _all_roles.role_id AS id, _all_roles.person_id, _all_roles.start_date, _all_roles.intended_end_date, _all_roles.end_date, _all_roles.funding_end_date, _all_roles.post_category, _all_roles.job_title, _all_roles.supervisor_id, _all_roles.funding, _all_roles.fees_funding, _all_roles.research_grant_number, _all_roles.paid_by_university AS paid_through_payroll, _all_roles.role_id, _all_roles.role_target_viewname FROM public._all_roles_v13 _all_roles) a ORDER BY a.start_date DESC;


ALTER TABLE hotwire."10_View/Roles/_Cambridge_History_V2" OWNER TO dev;

--
-- Name: 10_View/Roles/_current_people_supervised_by; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Roles/_current_people_supervised_by" AS
    SELECT lr.role_id AS id, lr.person_id, lr.start_date, lr.intended_end_date, lr.end_date, lr.funding_end_date, lr.post_category, lr.supervisor_id, lr.funding, lr.fees_funding, lr.research_grant_number, lr.paid_by_university, lr.role_target_viewname AS _target_viewname, lr.role_id AS _role_xid FROM (public._latest_role_v12 lr JOIN public._physical_status_v3 ps USING (person_id)) WHERE (((ps.status_id)::text = 'Current'::text) AND ((lr.status)::text = 'Current'::text));


ALTER TABLE hotwire."10_View/Roles/_current_people_supervised_by" OWNER TO dev;

--
-- Name: 10_View/Rooms/Room_Attributes; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Rooms/Room_Attributes" AS
    SELECT room.id, room.name, room.building_id, room.building_region_id, room.building_floor_id, room.area, room.room_type_id, room.number_of_desks, room.number_of_benches, room.maximum_occupancy, room.last_refurbished_date, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, room.comments, room.cooling_power, room.power_supply FROM (public.room LEFT JOIN public.research_group ON ((room.responsible_group_id = research_group.id))) ORDER BY room.name;


ALTER TABLE hotwire."10_View/Rooms/Room_Attributes" OWNER TO cen1001;

--
-- Name: 10_View/Rooms/Room_Occupancy; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Rooms/Room_Occupancy" AS
    SELECT room.id, room.name, room.room_type_id, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, _room_current_occupancy.current_occupancy AS ro_current_occupancy, room.number_of_desks, room.maximum_occupancy, _room_fume_hoods.number_of_fumehoods AS ro_number_of_fumehoods, room.area, room.last_refurbished_date, room.comments, public._to_hwsubview('room_id'::text, '10_View/People/Room_Occupants'::text, 'id'::text) AS room_occupants_subview FROM (((public.room LEFT JOIN public.research_group ON ((room.responsible_group_id = research_group.id))) LEFT JOIN public._room_current_occupancy ON ((room.id = _room_current_occupancy.id))) JOIN public._room_fume_hoods ON ((room.id = _room_fume_hoods.id))) ORDER BY room.name;


ALTER TABLE hotwire."10_View/Rooms/Room_Occupancy" OWNER TO cen1001;

--
-- Name: 10_View/Rooms/Room_Occupants; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Rooms/Room_Occupants" AS
    SELECT room.id, room.name, room.room_type_id, room.responsible_group_id, research_group.head_of_group_id AS ro_responsible_person_id, o.current_occupancy AS ro_current_occupancy, o.person_id, room.number_of_desks, room.maximum_occupancy, _room_fume_hoods.number_of_fumehoods AS ro_number_of_fumehoods, room.area, room.last_refurbished_date, room.comments FROM (((public.room LEFT JOIN public.research_group ON ((room.responsible_group_id = research_group.id))) LEFT JOIN (SELECT r2.id, count(mm.person_id) AS current_occupancy, array_agg(mm.person_id) AS person_id FROM ((public.room r2 LEFT JOIN public.mm_person_room mm ON ((mm.room_id = r2.id))) LEFT JOIN public._physical_status_v3 USING (person_id)) WHERE ((_physical_status_v3.status_id)::text = 'Current'::text) GROUP BY r2.id) o ON ((o.id = room.id))) JOIN public._room_fume_hoods ON ((room.id = _room_fume_hoods.id))) ORDER BY room.name;


ALTER TABLE hotwire."10_View/Rooms/Room_Occupants" OWNER TO dev;

--
-- Name: building_floor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW building_floor_hid AS
    SELECT building_floor_hid.building_floor_id, building_floor_hid.building_floor_hid FROM public.building_floor_hid;


ALTER TABLE hotwire.building_floor_hid OWNER TO postgres;

--
-- Name: building_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW building_hid AS
    SELECT building_hid.building_id, building_hid.building_hid FROM public.building_hid;


ALTER TABLE hotwire.building_hid OWNER TO postgres;

--
-- Name: 10_View/Safety/Firstaiders; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Safety/Firstaiders" AS
    SELECT firstaider.id, (person.image_lo)::bigint AS _image_oid, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, firstaider.defibrillator_trained, array_to_string(ARRAY(SELECT DISTINCT building_hid.building_hid FROM (((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) LEFT JOIN building_hid USING (building_id)) WHERE (p2.id = firstaider.person_id) ORDER BY building_hid.building_hid), '/'::text) AS ro_building, array_to_string(ARRAY(SELECT DISTINCT building_floor_hid.building_floor_hid FROM (((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) LEFT JOIN building_floor_hid USING (building_floor_id)) WHERE (p2.id = firstaider.person_id) ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor, array_to_string(ARRAY(SELECT DISTINCT room.name FROM ((public.person p2 LEFT JOIN public.mm_person_room ON ((p2.id = mm_person_room.person_id))) LEFT JOIN public.room ON ((room.id = mm_person_room.room_id))) WHERE (p2.id = firstaider.person_id) ORDER BY room.name), '/'::text) AS ro_room, array_to_string(ARRAY(SELECT dept_telephone_number_hid.dept_telephone_number_hid FROM ((public.person p2 JOIN public.mm_person_dept_telephone_number ON ((mm_person_dept_telephone_number.person_id = p2.id))) JOIN public.dept_telephone_number_hid ON ((dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id))) WHERE (p2.id = firstaider.person_id)), ' / '::text) AS ro_dept_telephone_numbers, _physical_status_v3.status_id AS status, firstaider.qualification_up_to_date AS ro_qualification_up_to_date, _latest_role_v8.post_category AS ro_post_category, CASE WHEN ((_physical_status_v3.status_id)::text = 'Past'::text) THEN 'orange'::text WHEN (firstaider.qualification_up_to_date = false) THEN 'red'::text ELSE NULL::text END AS _cssclass FROM (((public.firstaider JOIN public.person ON ((firstaider.person_id = person.id))) LEFT JOIN public._latest_role_v8 ON ((firstaider.person_id = _latest_role_v8.person_id))) JOIN public._physical_status_v3 ON ((_physical_status_v3.person_id = firstaider.person_id)));


ALTER TABLE hotwire."10_View/Safety/Firstaiders" OWNER TO dev;

--
-- Name: 10_View/Safety/Fume_Hoods; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Safety/Fume_Hoods" AS
    SELECT fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id FROM public.fume_hood;


ALTER TABLE hotwire."10_View/Safety/Fume_Hoods" OWNER TO cen1001;

--
-- Name: 10_View/Staff_Reviews; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Staff_Reviews" AS
    SELECT staff_review_meeting.id, staff_review_meeting.person_id, person.usual_reviewer_id, _latest_role_v10.supervisor_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, person.next_review_due, staff_review_meeting.notes, staff_review_meeting.last_updated FROM ((public.staff_review_meeting JOIN public.person ON ((person.id = staff_review_meeting.person_id))) JOIN public._latest_role_v10 ON ((person.id = _latest_role_v10.person_id))) ORDER BY staff_review_meeting.last_updated DESC NULLS LAST;


ALTER TABLE hotwire."10_View/Staff_Reviews" OWNER TO dev;

--
-- Name: 10_View/Switchport; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/Switchport" AS
    SELECT switchport.id, switchport.name, switchport.switch_id, switchport.socket_id FROM public.switchport;


ALTER TABLE hotwire."10_View/Switchport" OWNER TO cen1001;

--
-- Name: 10_View/System_Image_Autoinstaller; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/System_Image_Autoinstaller" AS
    SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, system_image.architecture_id, ARRAY(SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = system_image.id)) AS ip_address_id, ARRAY(SELECT mm_system_image_installer_tag.installer_tag_id FROM public.mm_system_image_installer_tag WHERE (mm_system_image_installer_tag.system_image_id = system_image.id)) AS installer_tag_id, ARRAY(SELECT mm_system_image_software_package_jumpstart.software_package_id FROM public.mm_system_image_software_package_jumpstart WHERE (mm_system_image_software_package_jumpstart.system_image_id = system_image.id)) AS software_package_id, system_image.reinstall_on_next_boot, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, research_group.fai_class_id AS ro_fai_class_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments, system_image.netboot_id FROM ((public.system_image JOIN public.hardware ON ((hardware.id = system_image.hardware_id))) JOIN public.research_group ON ((system_image.research_group_id = research_group.id)));


ALTER TABLE hotwire."10_View/System_Image_Autoinstaller" OWNER TO dev;

--
-- Name: 10_View/System_Image_Basic; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/System_Image_Basic" AS
    SELECT system_image.id, system_image.hardware_id, system_image.operating_system_id, system_image.comments FROM public.system_image;


ALTER TABLE hotwire."10_View/System_Image_Basic" OWNER TO dev;

--
-- Name: 10_View/Systems/ChemNet; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "10_View/Systems/ChemNet" AS
    SELECT machine_account.id, machine_account.system_image_id, machine_account.name AS token_name, machine_account.chemnet_token FROM public.machine_account;


ALTER TABLE hotwire."10_View/Systems/ChemNet" OWNER TO dev;

--
-- Name: VIEW "10_View/Systems/ChemNet"; Type: COMMENT; Schema: hotwire; Owner: dev
--

COMMENT ON VIEW "10_View/Systems/ChemNet" IS 'ChemNet tokens for systems not people';


--
-- Name: 10_View/User_Account; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "10_View/User_Account" AS
    SELECT user_account.id, user_account.person_id AS x_person_id, user_account.username, user_account.uid, user_account.description, user_account.gecos, user_account.expiry_date, user_account.is_disabled FROM public.user_account;


ALTER TABLE hotwire."10_View/User_Account" OWNER TO cen1001;

--
-- Name: 30_Report/All_Group_Computers; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "30_Report/All_Group_Computers" AS
    SELECT system_image.id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ip_address_hid.ip_address_hid AS ro_mm_ip_address, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments FROM (((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) LEFT JOIN public.mm_system_image_ip_address ON ((system_image.id = mm_system_image_ip_address.system_image_id))) LEFT JOIN public.ip_address_hid USING (ip_address_id)) WHERE (ip_address_hid.ip_address_hid !~~ '%personal.private%'::text);


ALTER TABLE hotwire."30_Report/All_Group_Computers" OWNER TO postgres;

--
-- Name: 30_Report/ChemAtCam/OptIns; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/ChemAtCam/OptIns" AS
    SELECT person.id, person.id AS person_id, person.title_id, person.first_names, person.surname, ps.status_id, lr.post_category_id, person.email_address, person.forwarding_address FROM ((public.person JOIN public._latest_role_v10 lr ON ((person.id = lr.person_id))) JOIN public._physical_status_v3 ps USING (person_id)) WHERE (person.chem_at_cam = true) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."30_Report/ChemAtCam/OptIns" OWNER TO dev;

--
-- Name: VIEW "30_Report/ChemAtCam/OptIns"; Type: COMMENT; Schema: hotwire; Owner: dev
--

COMMENT ON VIEW "30_Report/ChemAtCam/OptIns" IS 'Ticket 115667. For chematcam mailings.';


--
-- Name: 30_Report/Current_People_by_Sector_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Current_People_by_Sector_ro" AS
    SELECT person.id, person.surname, person.first_names, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (person.id = mm_person_room.person_id)) AS room_id, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, ARRAY(SELECT DISTINCT sector.id FROM ((public.mm_person_research_group JOIN public.research_group ON ((research_group.id = mm_person_research_group.research_group_id))) JOIN public.sector ON ((research_group.sector_id = sector.id))) WHERE (person.id = mm_person_research_group.person_id) ORDER BY sector.id) AS sector_id, _latest_role_v8.supervisor_id, CASE WHEN (person.hide_email = true) THEN (NULL::text)::character varying ELSE person.email_address END AS email_address, _latest_role_v8.post_category_id, _latest_role_v8.chem FROM ((public.person LEFT JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) LEFT JOIN public._physical_status ON ((_physical_status.id = person.id))) WHERE ((_physical_status.physical_status)::text <> 'Past'::text) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."30_Report/Current_People_by_Sector_ro" OWNER TO cen1001;

--
-- Name: 30_Report/Current_Postdocs; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Current_Postdocs" AS
    SELECT post_history.id, post_history.person_id, post_history.supervisor_id, post_history.chem, _all_roles.post_category_id, person.arrival_date, post_history.start_date AS latest_funding_start_date, post_history.intended_end_date, post_history.funding_end_date FROM (((public.post_history LEFT JOIN public._all_roles_v12 _all_roles ON (((post_history.id = _all_roles.role_id) AND (_all_roles.role_tablename = 'post_history'::text)))) LEFT JOIN public.person ON ((post_history.person_id = person.id))) LEFT JOIN public._physical_status_v2 ON ((_physical_status_v2.person_id = post_history.person_id))) WHERE ((((_all_roles.status)::text = 'Current'::text) AND ((_physical_status_v2.status_id)::text = 'Current'::text)) AND (((((_all_roles.post_category)::text = 'PDRA'::text) OR ((_all_roles.post_category)::text = 'Senior PDRA'::text)) OR ((_all_roles.post_category)::text = 'Research Fellow'::text)) OR ((_all_roles.post_category)::text = 'Principal Research Associate'::text)));


ALTER TABLE hotwire."30_Report/Current_Postdocs" OWNER TO dev;

--
-- Name: 30_Report/Current_Postgrad_Summary_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Current_Postgrad_Summary_ro" AS
    SELECT a.id, a.person_id, a.phd_thesis_title_html, a.filemaker_funding, a.filemaker_fees_funding, a.first_supervisor_id, a.postgraduate_studentship_type_id, a.phd_duration, a.phd_duration_days, a._surname, a._first_names FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, postgraduate_studentship.phd_thesis_title AS phd_thesis_title_html, postgraduate_studentship.filemaker_funding, postgraduate_studentship.filemaker_fees_funding, postgraduate_studentship.first_supervisor_id, postgraduate_studentship.postgraduate_studentship_type_id, _postgrad_end_dates_v5.phd_duration, _postgrad_end_dates_v5.phd_duration_days, person.surname AS _surname, person.first_names AS _first_names FROM ((public.postgraduate_studentship LEFT JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((postgraduate_studentship.person_id = person.id))) WHERE ((_postgrad_end_dates_v5.status_id)::text = 'Current'::text)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/Current_Postgrad_Summary_ro" OWNER TO cen1001;

--
-- Name: 30_Report/Female_Researchers; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Female_Researchers" AS
    SELECT person.id, person.surname, person.first_names, person.email_address, current_researchers.post_category, current_researchers.supervisor_id FROM (((public.person JOIN public.gender_hid USING (gender_id)) JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) JOIN (SELECT _latest_role.person_id, _latest_role.supervisor_id, _latest_role.post_category FROM public._latest_role_v12 _latest_role WHERE (((((_latest_role.status)::text = 'Current'::text) AND ((_latest_role.role_tablename = 'post_history'::text) OR (_latest_role.role_tablename = 'postgraduate_studentship'::text))) AND ((_latest_role.post_category)::text <> 'Assistant staff'::text)) AND ((_latest_role.post_category)::text <> 'Academic-related staff'::text))) current_researchers ON ((person.id = current_researchers.person_id))) WHERE (((gender_hid.gender_hid)::text = 'Female'::text) AND ((_physical_status_v2.status_id)::text = 'Current'::text)) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."30_Report/Female_Researchers" OWNER TO dev;

--
-- Name: 30_Report/Funding; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Funding" AS
    SELECT (((person.id)::text || '-'::text) || (_all_roles.role_id)::text) AS id, person.surname, person.first_names, person.title_id, (_all_roles.funding)::text AS current_funding, (_all_roles.research_grant_number)::text AS research_grant_number, _all_roles.start_date, _all_roles.end_date, _all_roles.post_category, _all_roles.paid_by_university, _all_roles.supervisor_id FROM (public.person LEFT JOIN public._all_roles_v12 _all_roles ON ((_all_roles.person_id = person.id))) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."30_Report/Funding" OWNER TO cen1001;

--
-- Name: 30_Report/HRNJ_Postgrad_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/HRNJ_Postgrad_ro" AS
    SELECT a.id, a.person_id, a.postgraduate_studentship_type_id, a.phd_duration, a.phd_duration_days, a.nationality_id, a._surname, a._first_names FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, postgraduate_studentship.postgraduate_studentship_type_id, _postgrad_end_dates_v5.phd_duration, _postgrad_end_dates_v5.phd_duration_days, ARRAY(SELECT mm_person_nationality.nationality_id FROM public.mm_person_nationality WHERE (mm_person_nationality.person_id = person.id)) AS nationality_id, person.surname AS _surname, person.first_names AS _first_names FROM ((public.postgraduate_studentship LEFT JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((postgraduate_studentship.person_id = person.id))) WHERE ((_postgrad_end_dates_v5.status_id)::text = 'Current'::text)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/HRNJ_Postgrad_ro" OWNER TO cen1001;

--
-- Name: 30_Report/IPs_without_Hardware_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/IPs_without_Hardware_ro" AS
    SELECT ip_address.id, ip_address.ip, ip_address.hostname FROM (public.ip_address LEFT JOIN public.mm_system_image_ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (mm_system_image_ip_address.system_image_id IS NULL) ORDER BY ip_address.ip;


ALTER TABLE hotwire."30_Report/IPs_without_Hardware_ro" OWNER TO cen1001;

--
-- Name: 30_Report/IPs_without_Hostnames_ro; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/IPs_without_Hostnames_ro" AS
    SELECT ip_address.id, ip_address.ip, ip_address.hostname FROM public.ip_address WHERE (((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text)) AND (NOT ip_address.reserved)) ORDER BY ip_address.ip;


ALTER TABLE hotwire."30_Report/IPs_without_Hostnames_ro" OWNER TO cen1001;

--
-- Name: 30_Report/Inconsistent_Status; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Inconsistent_Status" AS
    SELECT _physical_status_v2.person_id AS id, _physical_status_v2.person_id, _latest_role_v8.post_category_id, _physical_status_v2.status_id AS physical_status, person.arrival_date AS physical_arrival_date, person.leaving_date AS physical_leaving_date, _latest_role_v8.status AS newest_role_status, _latest_role_v8.start_date AS newest_role_start_date, _latest_role_v8.intended_end_date AS newest_role_intended_end_date, _latest_role_v8.end_date AS newest_role_end_date FROM ((public._physical_status_v2 LEFT JOIN public._latest_role_v8 USING (person_id)) LEFT JOIN public.person ON ((person.id = _physical_status_v2.person_id))) WHERE ((_physical_status_v2.status_id)::text <> (_latest_role_v8.status)::text);


ALTER TABLE hotwire."30_Report/Inconsistent_Status" OWNER TO cen1001;

--
-- Name: 30_Report/Leavers; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Leavers" AS
    SELECT a.id, a.person_id, a.intended_end_date, a.end_date, a.supervisor_id, a.post_category_id, a.role_status_id, a.physical_status_id, a._surname, a._first_names FROM (SELECT ((_latest_role_v8.person_id || '-'::text) || _latest_role_v8.role_id) AS id, _latest_role_v8.person_id, _latest_role_v8.intended_end_date, _latest_role_v8.end_date, _latest_role_v8.supervisor_id, _latest_role_v8.post_category_id, _latest_role_v8.status AS role_status_id, _physical_status.physical_status AS physical_status_id, person.surname AS _surname, person.first_names AS _first_names FROM ((public._latest_role_v8 LEFT JOIN public._physical_status ON ((_physical_status.id = _latest_role_v8.person_id))) LEFT JOIN public.person ON ((person.id = _latest_role_v8.person_id))) WHERE ((_latest_role_v8.intended_end_date < ('now'::text)::date) AND ((_physical_status.physical_status)::text = 'Current'::text))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/Leavers" OWNER TO cen1001;

--
-- Name: 30_Report/PDRA_Status; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/PDRA_Status" AS
    SELECT _latest_role_v10.role_id AS id, _latest_role_v10.person_id, _latest_role_v10.post_category_id, _latest_role_v10.chem, _latest_role_v10.paid_by_university, person.continuous_employment_start_date, _latest_role_v10.intended_end_date, _latest_role_v10.funding_end_date FROM ((public._latest_role_v10 JOIN public.person ON ((person.id = _latest_role_v10.person_id))) JOIN public._physical_status_v3 USING (person_id)) WHERE (((((_latest_role_v10.post_category)::text = 'Senior PDRA'::text) OR ((_latest_role_v10.post_category)::text = 'PDRA'::text)) OR ((_latest_role_v10.post_category)::text = 'Research Assistant'::text)) AND ((_physical_status_v3.status_id)::text = 'Current'::text));


ALTER TABLE hotwire."30_Report/PDRA_Status" OWNER TO cen1001;

--
-- Name: VIEW "30_Report/PDRA_Status"; Type: COMMENT; Schema: hotwire; Owner: cen1001
--

COMMENT ON VIEW "30_Report/PDRA_Status" IS 'Ticket 71821 ejg49';


--
-- Name: 30_Report/People_Without_Roles; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/People_Without_Roles" AS
    SELECT a.id, a.person_id, a.email_address, a.crsid, a.arrival_date, a.leaving_date, a.location, a.date_of_birth, a.other_information, a.notes, a._surname, a._first_names FROM (SELECT role_count.id, role_count.id AS person_id, person.email_address, person.crsid, person.arrival_date, person.leaving_date, person.location, person.date_of_birth, (person.other_information)::character varying(5000) AS other_information, (person.notes)::character varying(5000) AS notes, person.surname AS _surname, person.first_names AS _first_names FROM ((SELECT person.id, count(_all_roles.person_id) AS number_of_roles FROM (public.person LEFT JOIN public._all_roles_v12 _all_roles ON ((person.id = _all_roles.person_id))) WHERE (person.is_spri IS NOT TRUE) GROUP BY person.id) role_count JOIN public.person USING (id)) WHERE (role_count.number_of_roles = 0)) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/People_Without_Roles" OWNER TO dev;

--
-- Name: 30_Report/PhD_Submission; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/PhD_Submission" AS
    SELECT a.id, a.supervisor_id, a.person_id, a.ro_duration, a.date_registered_for_phd, a.date_phd_submission_due, a.phd_four_year_submission_date, a.phd_date_submitted, a.cpgs_or_mphil_date_submitted, a.start_date, a.funding, a.postgraduate_studentship_type_id, a.ro_duration_days, a.ro_status_id, a._surname, a._first_names FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.first_supervisor_id AS supervisor_id, postgraduate_studentship.person_id, _postgrad_end_dates_v5.phd_duration AS ro_duration, postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.date_phd_submission_due, postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.cpgs_or_mphil_date_submitted, postgraduate_studentship.start_date, postgraduate_studentship.filemaker_funding AS funding, postgraduate_studentship.postgraduate_studentship_type_id, _postgrad_end_dates_v5.phd_duration_days AS ro_duration_days, _postgrad_end_dates_v5.status_id AS ro_status_id, person.surname AS _surname, person.first_names AS _first_names FROM ((public.postgraduate_studentship LEFT JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((person.id = postgraduate_studentship.person_id)))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/PhD_Submission" OWNER TO cen1001;

--
-- Name: 30_Report/Phone_Directory; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Phone_Directory" AS
    SELECT person.id, person.surname, person.first_names, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (person.id = mm_person_dept_telephone_number.person_id)) AS dept_telephone_number_id, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (person.id = mm_person_room.person_id)) AS room_id, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (person.id = mm_person_research_group.person_id)) AS research_group_id, ARRAY(SELECT research_group.sector_id FROM ((public.mm_person_research_group JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id))) JOIN public.sector ON ((research_group.sector_id = sector.id))) WHERE (person.id = mm_person_research_group.person_id)) AS sector_id, _latest_role_v8.supervisor_id, CASE WHEN (person.hide_email = true) THEN (NULL::text)::character varying ELSE person.email_address END AS email_address, _latest_role_v8.post_category_id FROM ((public.person LEFT JOIN public._latest_role_v8 ON ((_latest_role_v8.person_id = person.id))) LEFT JOIN public._physical_status ON ((_physical_status.id = person.id))) WHERE ((_physical_status.physical_status)::text <> 'Past'::text) ORDER BY person.surname, person.first_names;


ALTER TABLE hotwire."30_Report/Phone_Directory" OWNER TO cen1001;

--
-- Name: 30_Report/Postdoc_Mentors; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Postdoc_Mentors" AS
    SELECT a.id, a.person_id, a.job_title, a.email_address, a.supervisor_id, a.supervisor_email_address, a.mentor_id, a.mentor_email_address, a._surname, a._first_names FROM (SELECT post_history.id, post_history.person_id, COALESCE(post_history.job_title, _latest_role_v8.post_category) AS job_title, person.email_address, post_history.supervisor_id, supervisor.email_address AS supervisor_email_address, post_history.mentor_id, mentor.email_address AS mentor_email_address, person.surname AS _surname, person.first_names AS _first_names FROM (((((public._latest_role_v8 LEFT JOIN public.post_history ON (((_latest_role_v8.role_id = post_history.id) AND (_latest_role_v8.role_tablename = 'post_history'::text)))) LEFT JOIN public.person ON ((post_history.person_id = person.id))) LEFT JOIN public.person mentor ON ((post_history.mentor_id = mentor.id))) LEFT JOIN public.person supervisor ON ((post_history.supervisor_id = supervisor.id))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) WHERE (((_latest_role_v8.post_category)::text = ANY (ARRAY[('PDRA'::character varying)::text, ('Senior PDRA'::character varying)::text, ('Research
Fellow'::character varying)::text])) AND ((_physical_status_v2.status_id)::text = 'Current'::text))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/Postdoc_Mentors" OWNER TO cen1001;

--
-- Name: 30_Report/Postgrad_Mentors; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Postgrad_Mentors" AS
    SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, postgraduate_studentship.postgraduate_studentship_type_id, person.email_address, postgraduate_studentship.first_supervisor_id AS supervisor_id, supervisor.email_address AS supervisor_email_address, postgraduate_studentship.first_mentor_id AS mentor_id, mentor.email_address AS mentor_email_address, postgraduate_studentship.second_mentor_id AS co_mentor_id, second_mentor.email_address AS co_mentor_email_address, postgraduate_studentship.external_mentor FROM ((((((public.postgraduate_studentship LEFT JOIN public.person ON ((postgraduate_studentship.person_id = person.id))) LEFT JOIN public.person mentor ON ((postgraduate_studentship.first_mentor_id = mentor.id))) LEFT JOIN public.person second_mentor ON ((postgraduate_studentship.second_mentor_id = second_mentor.id))) LEFT JOIN public.person supervisor ON ((postgraduate_studentship.first_supervisor_id = supervisor.id))) JOIN public._latest_role_v8 ON (((_latest_role_v8.role_id = postgraduate_studentship.id) AND (_latest_role_v8.role_tablename = 'postgraduate_studentship'::text)))) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) WHERE ((_physical_status_v2.status_id)::text = 'Current'::text);


ALTER TABLE hotwire."30_Report/Postgrad_Mentors" OWNER TO cen1001;

--
-- Name: 30_Report/Postgrad_Personnel; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Postgrad_Personnel" AS
    SELECT a.id, a.supervisor, a.person, a.studentship_type, a.second_supervisor, a.start_date, a.end_of_registration_date, a.source_of_funds, a.first_mentor, a.second_mentor FROM (SELECT postgraduate_studentship.id, supervisor_hid.supervisor_hid AS supervisor, person_hid.person_hid AS person, postgraduate_studentship_type.name AS studentship_type, second_supervisor_hid.supervisor_hid AS second_supervisor, postgraduate_studentship.start_date, postgraduate_studentship.end_of_registration_date, postgraduate_studentship.filemaker_funding AS source_of_funds, first_mentor_hid.mentor_hid AS first_mentor, second_mentor_hid.mentor_hid AS second_mentor FROM (((((((public.postgraduate_studentship JOIN public.supervisor_hid ON ((postgraduate_studentship.first_supervisor_id = supervisor_hid.supervisor_id))) LEFT JOIN public.supervisor_hid second_supervisor_hid ON ((postgraduate_studentship.second_supervisor_id = second_supervisor_hid.supervisor_id))) LEFT JOIN public.mentor_hid first_mentor_hid ON ((postgraduate_studentship.first_mentor_id = first_mentor_hid.mentor_id))) LEFT JOIN public.mentor_hid second_mentor_hid ON ((postgraduate_studentship.second_mentor_id = second_mentor_hid.mentor_id))) JOIN public.person_hid USING (person_id)) JOIN public._postgrad_end_dates_v5 USING (id)) JOIN public.postgraduate_studentship_type ON ((postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id))) WHERE ((((postgraduate_studentship_type.name)::text = 'PhD'::text) OR ((postgraduate_studentship_type.name)::text = 'MPhil'::text)) AND ((_postgrad_end_dates_v5.status_id)::text = 'Current'::text))) a ORDER BY a.supervisor, a.person;


ALTER TABLE hotwire."30_Report/Postgrad_Personnel" OWNER TO cen1001;

--
-- Name: 30_Report/Postgrad_Students_with_DOB; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Postgrad_Students_with_DOB" AS
    SELECT a.id, a.person_id, a.ro_surname, a.ro_first_names, a.ro_role_status_id, a.ro_email_address, a.postgraduate_studentship_type_id, a.supervisor_id, a.university, a.cambridge_college_id, a.nationality_id, a.start_date, a.date_of_birth FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, person.surname AS ro_surname, person.first_names AS ro_first_names, person.date_of_birth, _postgrad_end_dates_v5.status_id AS ro_role_status_id, person.email_address AS ro_email_address, postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.part_time, postgraduate_studentship.first_supervisor_id AS supervisor_id, postgraduate_studentship.second_supervisor_id AS co_supervisor_id, postgraduate_studentship.external_co_supervisor, postgraduate_studentship.substitute_supervisor_id, postgraduate_studentship.first_mentor_id AS mentor_id, postgraduate_studentship.second_mentor_id AS co_mentor_id, postgraduate_studentship.external_mentor, postgraduate_studentship.next_mentor_meeting_due, postgraduate_studentship.university, person.cambridge_college_id, postgraduate_studentship.cpgs_title AS cpgs_title_html, postgraduate_studentship.msc_title AS msc_title_html, postgraduate_studentship.mphil_title AS mphil_title_html, postgraduate_studentship.phd_thesis_title AS phd_thesis_title_html, postgraduate_studentship.paid_through_payroll, postgraduate_studentship.emplid_number, postgraduate_studentship.gaf_number, postgraduate_studentship.start_date, person.leaving_date, postgraduate_studentship.cpgs_or_mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submitted, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.mphil_date_submitted, postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.msc_date_submission_due, postgraduate_studentship.msc_date_submitted, postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.phd_date_title_approved, postgraduate_studentship.thesis_submission_due_date AS "PhD_three_year_submission_date", postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date, postgraduate_studentship.date_phd_submission_due AS "Revised_PhD_submission_date", postgraduate_studentship.phd_date_submitted, postgraduate_studentship.phd_date_examiners_appointed, postgraduate_studentship.phd_date_of_viva, postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register, postgraduate_studentship.date_reinstated_on_register, postgraduate_studentship.intended_end_date, postgraduate_studentship.filemaker_funding AS funding, postgraduate_studentship.filemaker_fees_funding AS fees_funding, postgraduate_studentship.funding_end_date, _postgrad_end_dates_v5.phd_duration AS ro_phd_duration, _postgrad_end_dates_v5.phd_duration_days AS ro_phd_duration_days, postgraduate_studentship.progress_notes, postgraduate_studentship.force_role_status_to_past, ARRAY(SELECT DISTINCT rg.sector_id FROM ((public.person p2 LEFT JOIN public.mm_person_research_group mm ON ((p2.id = mm.person_id))) LEFT JOIN public.research_group rg ON ((mm.research_group_id = rg.id))) WHERE ((p2.id = postgraduate_studentship.person_id) AND (rg.sector_id IS NOT NULL))) AS sector_id, ARRAY(SELECT mm2.nationality_id FROM (public.person p3 LEFT JOIN public.mm_person_nationality mm2 ON ((p3.id = mm2.person_id))) WHERE (p3.id = postgraduate_studentship.person_id)) AS nationality_id, CASE WHEN ((_postgrad_end_dates_v5.status_id)::text = 'Past'::text) THEN 'orange'::text ELSE NULL::text END AS _cssclass FROM ((public.postgraduate_studentship JOIN public._postgrad_end_dates_v5 USING (id)) JOIN public.person ON ((postgraduate_studentship.person_id = person.id)))) a ORDER BY a.ro_surname, a.ro_first_names;


ALTER TABLE hotwire."30_Report/Postgrad_Students_with_DOB" OWNER TO dev;

--
-- Name: 30_Report/Principal_Investigators_with_Students; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Principal_Investigators_with_Students" AS
    SELECT a.id, a.supervisor_id, a.email_address FROM (SELECT DISTINCT person.id, postgraduate_studentship.first_supervisor_id AS supervisor_id, person.email_address FROM ((public.postgraduate_studentship LEFT JOIN public.person ON ((postgraduate_studentship.first_supervisor_id = person.id))) LEFT JOIN public._all_roles_v12 _all_roles ON (((postgraduate_studentship.id = _all_roles.role_id) AND (_all_roles.role_tablename = 'postgraduate_studentship'::text)))) WHERE ((_all_roles.status)::text = 'Current'::text)) a ORDER BY a.id, a.supervisor_id, a.email_address;


ALTER TABLE hotwire."30_Report/Principal_Investigators_with_Students" OWNER TO dev;

--
-- Name: 30_Report/REF_2021; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/REF_2021" AS
    WITH ref2021 AS (SELECT le.person_id AS id, le.person_id, p.crsid, p.continuous_employment_start_date, le.start_date AS current_contract_start_date, le.intended_end_date AS current_contract_end_date, le.end_date AS date_contract_ended, le.funding, le.post_category, ph.job_title, ph.percentage_of_fulltime_hours, p.arrival_date AS date_arrived_in_department, p.leaving_date AS date_left_department_if_known, ps.status_id AS presence, le.supervisor_id, p.surname AS _surname, p.first_names AS _first_names FROM (((public._latest_employment le JOIN public.person p ON ((p.id = le.person_id))) JOIN public._physical_status_v3 ps USING (person_id)) JOIN public.post_history ph ON ((ph.id = le.role_id))) WHERE (le.role_tablename = 'post_history'::text)) SELECT ref2021.id, ref2021.person_id, ref2021.crsid, ref2021.continuous_employment_start_date, ref2021.current_contract_start_date, ref2021.current_contract_end_date, ref2021.date_contract_ended, ref2021.funding, ref2021.post_category, ref2021.job_title, ref2021.percentage_of_fulltime_hours, ref2021.date_arrived_in_department, ref2021.date_left_department_if_known, ref2021.presence, ref2021.supervisor_id, ref2021._surname, ref2021._first_names FROM ref2021 ORDER BY ref2021._surname, ref2021._first_names;


ALTER TABLE hotwire."30_Report/REF_2021" OWNER TO dev;

--
-- Name: 30_Report/Retirement_Dates; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Retirement_Dates" AS
    SELECT a.id, a.person_id, a.date_of_birth, a.post_category, a.start_date, a.end_date, a.intended_end_date, a.funding_end_date, a.job_title, a._surname, a._first_names FROM (SELECT lr.role_id AS id, lr.person_id, person.date_of_birth, lr.post_category, lr.start_date, lr.end_date, lr.intended_end_date, lr.funding_end_date, post_history.job_title, person.surname AS _surname, person.first_names AS _first_names FROM ((public._latest_role_v12 lr LEFT JOIN public.person ON ((lr.person_id = person.id))) LEFT JOIN public.post_history ON ((lr.role_id = post_history.id))) WHERE (((((((person.left_but_no_leaving_date_given = false) OR (person.left_but_no_leaving_date_given IS NULL)) AND ((lr.status)::text = 'Current'::text)) AND (lr.role_tablename = 'post_history'::text)) AND (COALESCE(lr.end_date, lr.intended_end_date) > ('now'::text)::date)) AND ((lr.funding_end_date IS NULL) OR (lr.funding_end_date > COALESCE(lr.funding_end_date, lr.intended_end_date)))) AND ((((lr.post_category)::text = 'Academic staff'::text) OR ((lr.post_category)::text = 'Academic-related staff'::text)) OR ((lr.post_category)::text = 'Assistant staff'::text)))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/Retirement_Dates" OWNER TO dev;

--
-- Name: 30_Report/Rooms_by_RIG; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Rooms_by_RIG" AS
    SELECT (((room.id)::text || '-'::text) || COALESCE((rig.id)::text, ''::text)) AS id, room.id AS room_id, room.room_type_id, room.area, room.responsible_group_id, rg.head_of_group_id, rig.id AS research_interest_group_id FROM ((((public.room LEFT JOIN public.research_group rg ON ((room.responsible_group_id = rg.id))) LEFT JOIN public.person ON ((rg.head_of_group_id = person.id))) LEFT JOIN public.mm_member_research_interest_group mm ON ((person.id = mm.member_id))) LEFT JOIN public.research_interest_group rig ON ((rig.id = mm.research_interest_group_id)));


ALTER TABLE hotwire."30_Report/Rooms_by_RIG" OWNER TO cen1001;

--
-- Name: 30_Report/Rooms_by_Research_Group; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Rooms_by_Research_Group" AS
    SELECT a.id, a.responsible_group_id, a.head_of_group_id, a.name, a.room_type_id, a.area, a.number_of_desks, a.maximum_occupancy, a.current_occupancy, a.number_of_fumehoods, a.person_id, a.occupant_group_id FROM (SELECT DISTINCT (((((room.id || '-'::text) || research_group.id) || '-'::text) || COALESCE(mm_person_room.person_id, (0)::bigint)) || COALESCE(occupant_group.id, (0)::bigint)) AS id, research_group.id AS responsible_group_id, research_group.head_of_group_id, room.name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods, mm_person_room.person_id, occupant_group.id AS occupant_group_id FROM (((((((public.research_group LEFT JOIN public.room ON ((room.responsible_group_id = research_group.id))) LEFT JOIN public._room_current_occupancy ON ((room.id = _room_current_occupancy.id))) LEFT JOIN public._room_fume_hoods ON ((room.id = _room_fume_hoods.id))) LEFT JOIN public.mm_person_room ON ((room.id = mm_person_room.room_id))) LEFT JOIN public._physical_status_v2 USING (person_id)) LEFT JOIN public.mm_person_research_group USING (person_id)) LEFT JOIN public.research_group occupant_group ON ((mm_person_research_group.research_group_id = occupant_group.id))) WHERE (((_physical_status_v2.status_id)::text = 'Current'::text) OR (_physical_status_v2.status_id IS NULL))) a ORDER BY a.id, a.responsible_group_id, a.head_of_group_id, a.name, a.room_type_id, a.area, a.number_of_desks, a.maximum_occupancy, a.current_occupancy, a.number_of_fumehoods, a.person_id, a.occupant_group_id;


ALTER TABLE hotwire."30_Report/Rooms_by_Research_Group" OWNER TO cen1001;

--
-- Name: 30_Report/Rooms_everything; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Rooms_everything" AS
    SELECT room.id, room.name, room.building_id, room.building_region_id, room.building_floor_id, (SELECT count(fume_hood.id) AS count FROM public.fume_hood WHERE (fume_hood.room_id = room.id)) AS number_of_fumehoods, ((SELECT count(person.id) AS count FROM ((public.person JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id))) JOIN public.mm_person_room mm ON ((person.id = mm.person_id))) WHERE ((mm.room_id = room.id) AND ((ps.status_id)::text = 'Current'::text))))::integer AS number_of_occupants, room.area, room.room_type_id, room.responsible_group_id, research_group.head_of_group_id, room.responsible_person_id, room.last_refurbished_date, room.maximum_occupancy, room.number_of_desks, room.number_of_benches, room.comments, room.embs_name, room.suggested_new_name, room.survey_date, room.survey_comments, room.survey_door_label FROM (public.room LEFT JOIN public.research_group ON ((room.responsible_group_id = research_group.id)));


ALTER TABLE hotwire."30_Report/Rooms_everything" OWNER TO cen1001;

--
-- Name: 30_Report/Searching; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Searching" AS
    SELECT ((person.id || (COALESCE(roles.post_category, ''::character varying))::text) || COALESCE((roles.start_date)::text, ''::text)) AS id, person.surname, person.first_names, person.title_id, person.known_as, person.gender_id, date_part('year'::text, age((person.date_of_birth)::timestamp with time zone)) AS ro_age, person.arrival_date, person.leaving_date, _physical_status_v2.status_id AS physical_status_id, roles.start_date, roles.intended_end_date, roles.end_date, roles.funding_end_date, roles.post_category, roles.status AS role_status_id, roles.chem, person.date_of_birth, person.registration_completed, person.clearance_cert_signed, person.email_address, person.crsid, person.hide_email, ARRAY(SELECT mm_person_room.room_id FROM public.mm_person_room WHERE (mm_person_room.person_id = person.id)) AS room_id, person.location, ARRAY(SELECT mm_person_dept_telephone_number.dept_telephone_number_id FROM public.mm_person_dept_telephone_number WHERE (mm_person_dept_telephone_number.person_id = person.id)) AS dept_telephone_number_id, person.mobile_number, person.cambridge_phone_number, ARRAY(SELECT mm_person_research_group.research_group_id FROM public.mm_person_research_group WHERE (mm_person_research_group.person_id = person.id)) AS research_group_id, ARRAY(SELECT research_group.sector_id FROM (public.mm_person_research_group JOIN public.research_group ON ((research_group.id = mm_person_research_group.research_group_id))) WHERE (mm_person_research_group.person_id = person.id)) AS sector_id, roles.supervisor_id, roles.manager_id, roles.paid_through_payroll, person.continuous_employment_start_date, person.paper_file_details, roles.job_title, roles.current_funding, roles.research_grant_number, roles.university, ARRAY(SELECT mm_person_nationality.nationality_id FROM public.mm_person_nationality WHERE (mm_person_nationality.person_id = person.id)) AS nationality_id, person.cambridge_college_id, roles.cpgs_or_mphil_date_submission_due, roles.cpgs_or_mphil_date_submitted, roles.cpgs_or_mphil_date_awarded, roles.mphil_date_submission_due, roles.mphil_date_submitted, roles.mphil_date_awarded, roles.date_registered_for_phd, roles.phd_date_title_approved, roles.date_phd_submission_due, roles.phd_four_year_submission_date, roles.phd_date_submitted, roles.phd_date_examiners_appointed, roles.phd_date_of_viva, roles.phd_date_awarded, roles.co_supervisor_id, roles.mentor_id, roles.co_mentor_id, roles.gaf_number, roles.emplid_number, roles.cpgs_title_html, roles.mphil_title_html, roles.msc_title_html, roles.phd_thesis_title_html, roles.next_mentor_meeting_due, roles.fees_funding, roles.part_iii_project_title_html, roles.date_of_first_probation_meeting, roles.date_of_second_probation_meeting, roles.date_of_third_probation_meeting, roles.date_probation_letters_sent, roles.probation_period, person.usual_reviewer_id AS reviewer_id, roles.next_review_due, roles.timing_of_reviews, roles.transferable_skills_days_1st_year, roles.transferable_skills_days_2nd_year, roles.transferable_skills_days_3rd_year, roles.transferable_skills_days_4th_year, person.forwarding_address, person.emergency_contact, person.new_employer_address, person.cambridge_address, person.previous_surname, (person.other_information)::character varying(5000) AS other_information, (person.notes)::character varying(5000) AS notes, roles.progress_notes FROM ((public.person LEFT JOIN ((((SELECT v.person_id, v.start_date, v.intended_end_date, v.end_date, NULL::date AS funding_end_date, false AS chem, _all_roles.post_category, NULL::text AS job_title, NULL::bigint AS manager_id, NULL::text AS progress_notes, v.host_person_id AS supervisor_id, NULL::text AS university, NULL::text AS gaf_number, NULL::bigint AS postgraduate_studentship_type_id, NULL::text AS cpgs_title_html, NULL::text AS mphil_title_html, NULL::text AS msc_title_html, NULL::date AS cpgs_or_mphil_date_submission_due, NULL::date AS cpgs_or_mphil_date_submitted, NULL::date AS cpgs_or_mphil_date_awarded, NULL::date AS mphil_date_submission_due, NULL::date AS mphil_date_submitted, NULL::date AS mphil_date_awarded, NULL::date AS date_registered_for_phd, NULL::text AS phd_thesis_title_html, NULL::date AS phd_date_title_approved, NULL::date AS date_phd_submission_due, NULL::date AS phd_four_year_submission_date, NULL::date AS phd_date_submitted, NULL::date AS phd_date_examiners_appointed, NULL::date AS phd_date_of_viva, NULL::date AS phd_date_awarded, NULL::bigint AS co_supervisor_id, NULL::bigint AS mentor_id, NULL::bigint AS co_mentor_id, NULL::date AS next_mentor_meeting_due, NULL::text AS current_funding, NULL::text AS fees_funding, NULL::text AS research_grant_number, NULL::text AS part_iii_project_title_html, NULL::boolean AS paid_through_payroll, NULL::date AS date_of_first_probation_meeting, NULL::date AS date_of_second_probation_meeting, NULL::date AS date_of_third_probation_meeting, NULL::date AS date_probation_letters_sent, NULL::text AS probation_period, NULL::text AS emplid_number, NULL::date AS next_review_due, NULL::text AS timing_of_reviews, NULL::real AS transferable_skills_days_1st_year, NULL::real AS transferable_skills_days_2nd_year, NULL::real AS transferable_skills_days_3rd_year, NULL::real AS transferable_skills_days_4th_year, _all_roles.status FROM (public.visitorship v LEFT JOIN public._all_roles_v12 _all_roles ON ((v.id = _all_roles.role_id))) WHERE (_all_roles.role_tablename = 'visitorship'::text) UNION SELECT e.person_id, e.start_date, e.intended_end_date, e.end_date, NULL::date AS funding_end_date, false AS chem, _all_roles.post_category, NULL::text AS job_title, NULL::bigint AS manager_id, NULL::text AS progress_notes, e.supervisor_id, e.university, NULL::text AS gaf_number, NULL::bigint AS postgraduate_studentship_type_id, NULL::text AS cpgs_title_html, NULL::text AS mphil_title_html, NULL::text AS msc_title_html, NULL::date AS cpgs_or_mphil_date_submission_due, NULL::date AS cpgs_or_mphil_date_submitted, NULL::date AS cpgs_or_mphil_date_awarded, NULL::date AS mphil_date_submission_due, NULL::date AS mphil_date_submitted, NULL::date AS mphil_date_awarded, NULL::date AS date_registered_for_phd, NULL::text AS phd_thesis_title_html, NULL::date AS phd_date_title_approved, NULL::date AS date_phd_submission_due, NULL::date AS phd_four_year_submission_date, NULL::date AS phd_date_submitted, NULL::date AS phd_date_examiners_appointed, NULL::date AS phd_date_of_viva, NULL::date AS phd_date_awarded, NULL::bigint AS co_supervisor_id, NULL::bigint AS mentor_id, NULL::bigint AS co_mentor_id, NULL::date AS next_mentor_meeting_due, NULL::text AS current_funding, NULL::text AS fees_funding, NULL::text AS research_grant_number, NULL::text AS part_iii_project_title_html, NULL::boolean AS paid_through_payroll, NULL::date AS date_of_first_probation_meeting, NULL::date AS date_of_second_probation_meeting, NULL::date AS date_of_third_probation_meeting, NULL::date AS date_probation_letters_sent, NULL::text AS probation_period, NULL::text AS emplid_number, NULL::date AS next_review_due, NULL::text AS timing_of_reviews, NULL::real AS transferable_skills_days_1st_year, NULL::real AS transferable_skills_days_2nd_year, NULL::real AS transferable_skills_days_3rd_year, NULL::real AS transferable_skills_days_4th_year, _all_roles.status FROM (public.erasmus_socrates_studentship e LEFT JOIN public._all_roles_v12 _all_roles ON ((e.id = _all_roles.role_id))) WHERE (_all_roles.role_tablename = 'erasmus_socrates_studentship'::text)) UNION SELECT p.person_id, p.start_date, p.intended_end_date, p.end_date, NULL::date AS funding_end_date, false AS chem, _all_roles.post_category, NULL::text AS job_title, NULL::bigint AS manager_id, NULL::text AS progress_notes, p.supervisor_id, NULL::text AS university, NULL::text AS gaf_number, NULL::bigint AS postgraduate_studentship_type_id, NULL::text AS cpgs_title_html, NULL::text AS mphil_title_html, NULL::text AS msc_title_html, NULL::date AS cpgs_or_mphil_date_submission_due, NULL::date AS cpgs_or_mphil_date_submitted, NULL::date AS cpgs_or_mphil_date_awarded, NULL::date AS mphil_date_submission_due, NULL::date AS mphil_date_submitted, NULL::date AS mphil_date_awarded, NULL::date AS date_registered_for_phd, NULL::text AS phd_thesis_title_html, NULL::date AS phd_date_title_approved, NULL::date AS date_phd_submission_due, NULL::date AS phd_four_year_submission_date, NULL::date AS phd_date_submitted, NULL::date AS phd_date_examiners_appointed, NULL::date AS phd_date_of_viva, NULL::date AS phd_date_awarded, NULL::bigint AS co_supervisor_id, NULL::bigint AS mentor_id, NULL::bigint AS co_mentor_id, NULL::date AS next_mentor_meeting_due, NULL::text AS current_funding, NULL::text AS fees_funding, NULL::text AS research_grant_number, p.project_title AS part_iii_project_title_html, NULL::boolean AS paid_through_payroll, NULL::date AS date_of_first_probation_meeting, NULL::date AS date_of_second_probation_meeting, NULL::date AS date_of_third_probation_meeting, NULL::date AS date_probation_letters_sent, NULL::text AS probation_period, NULL::text AS emplid_number, NULL::date AS next_review_due, NULL::text AS timing_of_reviews, NULL::real AS transferable_skills_days_1st_year, NULL::real AS transferable_skills_days_2nd_year, NULL::real AS transferable_skills_days_3rd_year, NULL::real AS transferable_skills_days_4th_year, _all_roles.status FROM (public.part_iii_studentship p LEFT JOIN public._all_roles_v12 _all_roles ON ((_all_roles.role_id = p.id))) WHERE (_all_roles.role_tablename = 'part_iii_studentship'::text)) UNION SELECT ph.person_id, ph.start_date, ph.intended_end_date, ph.end_date, ph.funding_end_date, ph.chem, _all_roles.post_category, ph.job_title, ph.supervisor_id AS manager_id, NULL::text AS progress_notes, ph.supervisor_id, NULL::text AS university, NULL::text AS gaf_number, NULL::bigint AS postgraduate_studentship_type_id, NULL::text AS cpgs_title_html, NULL::text AS mphil_title_html, NULL::text AS msc_title_html, NULL::date AS cpgs_or_mphil_date_submission_due, NULL::date AS cpgs_or_mphil_date_submitted, NULL::date AS cpgs_or_mphil_date_awarded, NULL::date AS mphil_date_submission_due, NULL::date AS mphil_date_submitted, NULL::date AS mphil_date_awarded, NULL::date AS date_registered_for_phd, NULL::text AS phd_thesis_title_html, NULL::date AS phd_date_title_approved, NULL::date AS date_phd_submission_due, NULL::date AS phd_four_year_submission_date, NULL::date AS phd_date_submitted, NULL::date AS phd_date_examiners_appointed, NULL::date AS phd_date_of_viva, NULL::date AS phd_date_awarded, NULL::bigint AS co_supervisor_id, ph.mentor_id, NULL::bigint AS co_mentor_id, ph.next_mentor_meeting_due, ph.filemaker_funding AS current_funding, NULL::text AS fees_funding, ph.research_grant_number, NULL::text AS part_iii_project_title_html, ph.paid_by_university, ph.date_of_first_probation_meeting, ph.date_of_second_probation_meeting, ph.date_of_third_probation_meeting, ph.date_probation_letters_sent, ph.probation_period, NULL::text AS emplid_number, ph.next_review_due, ph.timing_of_reviews, NULL::real AS transferable_skills_days_1st_year, NULL::real AS transferable_skills_days_2nd_year, NULL::real AS transferable_skills_days_3rd_year, NULL::real AS transferable_skills_days_4th_year, _all_roles.status FROM ((public.post_history ph LEFT JOIN public.staff_category ON ((ph.staff_category_id = staff_category.id))) LEFT JOIN public._all_roles_v12 _all_roles ON ((_all_roles.role_id = ph.id))) WHERE (_all_roles.role_tablename = 'post_history'::text)) UNION SELECT pg.person_id, pg.start_date, _all_roles.intended_end_date, _all_roles.end_date, pg.funding_end_date, true AS chem, _all_roles.post_category, NULL::text AS job_title, NULL::bigint AS manager_id, pg.progress_notes, pg.first_supervisor_id AS supervisor_id, pg.university, pg.gaf_number, pg.postgraduate_studentship_type_id, pg.cpgs_title AS cpgs_title_html, pg.mphil_title AS mphil_title_html, pg.msc_title AS msc_title_html, pg.cpgs_or_mphil_date_submission_due, pg.cpgs_or_mphil_date_submitted, pg.cpgs_or_mphil_date_awarded, pg.mphil_date_submission_due, pg.mphil_date_submitted, pg.mphil_date_awarded, pg.date_registered_for_phd, pg.phd_thesis_title AS phd_title_html, pg.phd_date_title_approved, pg.date_phd_submission_due, pg.end_of_registration_date AS phd_four_year_submission_date, pg.phd_date_submitted, pg.phd_date_examiners_appointed, pg.phd_date_of_viva, pg.phd_date_awarded, pg.second_supervisor_id AS co_supervisor_id, pg.first_mentor_id AS mentor_id, pg.second_mentor_id AS co_mentor_id, pg.next_mentor_meeting_due, pg.filemaker_funding AS current_funding, pg.filemaker_fees_funding AS fees_funding, NULL::text AS research_grant_number, NULL::text AS part_iii_project_title_html, pg.paid_through_payroll, NULL::date AS date_of_first_probation_meeting, NULL::date AS date_of_second_probation_meeting, NULL::date AS date_of_third_probation_meeting, NULL::date AS date_probation_letters_sent, NULL::text AS probation_period, pg.emplid_number, NULL::date AS next_review_due, NULL::text AS timing_of_reviews, pg.transferable_skills_days_1st_year, pg.transferable_skills_days_2nd_year, pg.transferable_skills_days_3rd_year, pg.transferable_skills_days_4th_year, _all_roles.status FROM ((public.postgraduate_studentship pg LEFT JOIN public.postgraduate_studentship_type ON ((postgraduate_studentship_type.id = pg.postgraduate_studentship_type_id))) LEFT JOIN public._all_roles_v12 _all_roles ON ((_all_roles.role_id = pg.id))) WHERE (_all_roles.role_tablename = 'postgraduate_studentship'::text)) roles ON ((roles.person_id = person.id))) LEFT JOIN public._physical_status_v2 ON ((_physical_status_v2.id = person.id))) ORDER BY person.surname, person.first_names, roles.start_date;


ALTER TABLE hotwire."30_Report/Searching" OWNER TO dev;

--
-- Name: 30_Report/Short_Searching; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Short_Searching" AS
    SELECT a.id, a.surname, a.first_names, a.title_id, a.known_as, a.gender_id, a.ro_age, a.start_date, a.end_date, a.physical_status_id, a.funding_end_date, a.post_category, a.role_status_id, a.chem, a.registration_completed, a.clearance_cert_signed, a.email_address, a.hide_email, a.room_id, a.location, a.dept_telephone_number_id, a.research_group_id, a.supervisor_id, a.manager_id, a.paid_through_payroll, a.continuous_employment_start_date, a.paper_file_details, a.job_title, a.current_funding, a.research_grant_number, a.university, a.nationality_id, a.cambridge_college_id, a.cpgs_or_mphil_date_submission_due, a.cpgs_or_mphil_date_submitted, a.cpgs_or_mphil_date_awarded, a.mphil_date_submission_due, a.mphil_date_submitted, a.mphil_date_awarded, a.date_registered_for_phd, a.phd_date_title_approved, a.date_phd_submission_due, a.phd_four_year_submission_date, a.phd_date_submitted, a.phd_date_examiners_appointed, a.phd_date_of_viva, a.phd_date_awarded, a.co_supervisor_id, a.mentor_id, a.co_mentor_id, a.next_mentor_meeting_due, a.fees_funding, a.part_iii_project_title_html, a.emplid_number, a.reviewer_id, a.next_review_due, a.timing_of_reviews FROM (SELECT "30_Report/Searching".id, "30_Report/Searching".surname, "30_Report/Searching".first_names, "30_Report/Searching".title_id, "30_Report/Searching".known_as, "30_Report/Searching".gender_id, date_part('year'::text, age(("30_Report/Searching".date_of_birth)::timestamp with time zone)) AS ro_age, "30_Report/Searching".start_date, "30_Report/Searching".end_date, "30_Report/Searching".physical_status_id, "30_Report/Searching".funding_end_date, "30_Report/Searching".post_category, "30_Report/Searching".role_status_id, "30_Report/Searching".chem, "30_Report/Searching".registration_completed, "30_Report/Searching".clearance_cert_signed, "30_Report/Searching".email_address, "30_Report/Searching".hide_email, "30_Report/Searching".room_id, "30_Report/Searching".location, "30_Report/Searching".dept_telephone_number_id, "30_Report/Searching".research_group_id, "30_Report/Searching".supervisor_id, "30_Report/Searching".manager_id, "30_Report/Searching".paid_through_payroll, "30_Report/Searching".continuous_employment_start_date, "30_Report/Searching".paper_file_details, "30_Report/Searching".job_title, "30_Report/Searching".current_funding, "30_Report/Searching".research_grant_number, "30_Report/Searching".university, "30_Report/Searching".nationality_id, "30_Report/Searching".cambridge_college_id, "30_Report/Searching".cpgs_or_mphil_date_submission_due, "30_Report/Searching".cpgs_or_mphil_date_submitted, "30_Report/Searching".cpgs_or_mphil_date_awarded, "30_Report/Searching".mphil_date_submission_due, "30_Report/Searching".mphil_date_submitted, "30_Report/Searching".mphil_date_awarded, "30_Report/Searching".date_registered_for_phd, "30_Report/Searching".phd_date_title_approved, "30_Report/Searching".date_phd_submission_due, "30_Report/Searching".phd_four_year_submission_date, "30_Report/Searching".phd_date_submitted, "30_Report/Searching".phd_date_examiners_appointed, "30_Report/Searching".phd_date_of_viva, "30_Report/Searching".phd_date_awarded, "30_Report/Searching".co_supervisor_id, "30_Report/Searching".mentor_id, "30_Report/Searching".co_mentor_id, "30_Report/Searching".next_mentor_meeting_due, "30_Report/Searching".fees_funding, "30_Report/Searching".part_iii_project_title_html, "30_Report/Searching".emplid_number, "30_Report/Searching".reviewer_id, "30_Report/Searching".next_review_due, "30_Report/Searching".timing_of_reviews FROM "30_Report/Searching") a ORDER BY a.surname, a.first_names, a.start_date;


ALTER TABLE hotwire."30_Report/Short_Searching" OWNER TO dev;

--
-- Name: 30_Report/Source_of_Funds; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Source_of_Funds" AS
    SELECT a.id, a.surname, a.first_names, a.post_category, a.start_date, a.current_funding, a.date_registered_for_phd, a.nationality_id, a.supervisor_id FROM (SELECT "30_Report/Searching".id, "30_Report/Searching".surname, "30_Report/Searching".first_names, "30_Report/Searching".post_category, "30_Report/Searching".start_date, "30_Report/Searching".current_funding, "30_Report/Searching".date_registered_for_phd, "30_Report/Searching".nationality_id, "30_Report/Searching".supervisor_id FROM "30_Report/Searching") a ORDER BY a.surname, a.first_names;


ALTER TABLE hotwire."30_Report/Source_of_Funds" OWNER TO dev;

--
-- Name: 30_Report/Space_used_by_Research_Group; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Space_used_by_Research_Group" AS
    SELECT a.id, a.research_group_id, a.head_of_group_id, a.person_id, a.room_name, a.room_type_id, a.area, a.number_of_desks, a.maximum_occupancy, a.current_occupancy, a.number_of_fumehoods FROM (SELECT DISTINCT ((((research_group.id || '-'::text) || mm_person_research_group.person_id) || '-'::text) || mm_person_room.room_id) AS id, research_group.id AS research_group_id, research_group.head_of_group_id, mm_person_research_group.person_id, room.name AS room_name, room.room_type_id, room.area, room.number_of_desks, room.maximum_occupancy, _room_current_occupancy.current_occupancy, _room_fume_hoods.number_of_fumehoods FROM ((((((public.research_group LEFT JOIN public.mm_person_research_group ON ((mm_person_research_group.research_group_id = research_group.id))) JOIN public.mm_person_room USING (person_id)) JOIN public._physical_status_v2 USING (person_id)) JOIN public.room ON ((mm_person_room.room_id = room.id))) LEFT JOIN public._room_current_occupancy ON ((room.id = _room_current_occupancy.id))) LEFT JOIN public._room_fume_hoods ON ((room.id = _room_fume_hoods.id))) WHERE (((_physical_status_v2.status_id)::text = 'Current'::text) OR (_physical_status_v2.status_id IS NULL))) a ORDER BY a.id, a.research_group_id, a.head_of_group_id, a.person_id, a.room_name, a.room_type_id, a.area, a.number_of_desks, a.maximum_occupancy, a.current_occupancy, a.number_of_fumehoods;


ALTER TABLE hotwire."30_Report/Space_used_by_Research_Group" OWNER TO cen1001;

--
-- Name: 30_Report/Staff_Personnel; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Staff_Personnel" AS
    SELECT a.id, a.supervisor, a.supervisee, a.staff_category_id, a.job_title, a.continuous_employment_start_date, a.end_date FROM (SELECT (((person.id)::text || '-'::text) || (supervisee.id)::text) AS id, supervisor_hid.person_hid AS supervisor, supervisee_hid.person_hid AS supervisee, post_history.staff_category_id, post_history.job_title, supervisee.continuous_employment_start_date, COALESCE(post_history.funding_end_date, post_history.intended_end_date) AS end_date FROM (((((((public.person JOIN public._latest_role_v12 _latest_role ON ((_latest_role.supervisor_id = person.id))) JOIN public.post_history ON ((_latest_role.role_id = post_history.id))) JOIN public.person supervisee ON ((post_history.person_id = supervisee.id))) JOIN public._physical_status_v2 supervisor_status ON ((person.id = supervisor_status.person_id))) JOIN public._physical_status_v2 supervisee_status ON ((supervisee.id = supervisee_status.person_id))) JOIN public.person_hid supervisor_hid ON ((supervisor_hid.person_id = person.id))) JOIN public.person_hid supervisee_hid ON ((supervisee_hid.person_id = supervisee.id))) WHERE (((supervisee_status.status_id)::text = 'Current'::text) AND (_latest_role.role_tablename = 'post_history'::text))) a ORDER BY a.supervisor, a.supervisee;


ALTER TABLE hotwire."30_Report/Staff_Personnel" OWNER TO dev;

--
-- Name: 30_Report/Staff_Review_and_Development; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Staff_Review_and_Development" AS
    SELECT DISTINCT person.id, person.id AS person_id, lr.post_category_id, lr.chem, lr.supervisor_id, person.usual_reviewer_id, ur.email_address AS usual_reviewer_email, staff_review_meeting.reviewer_id, r.email_address AS reviewer_email, most_recent.date_of_meeting, ARRAY(SELECT other_meetings.date_of_meeting FROM (SELECT staff_review_meeting.date_of_meeting, staff_review_meeting.person_id FROM public.staff_review_meeting EXCEPT SELECT max(staff_review_meeting.date_of_meeting) AS max, staff_review_meeting.person_id FROM public.staff_review_meeting GROUP BY staff_review_meeting.person_id) other_meetings WHERE (other_meetings.person_id = person.id)) AS others, person.next_review_due, person.continuous_employment_start_date, newest_funding.funding_end_date FROM ((((((((public.person LEFT JOIN cache._latest_role _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public.staff_review_meeting ON ((staff_review_meeting.person_id = person.id))) LEFT JOIN (SELECT max(staff_review_meeting.date_of_meeting) AS date_of_meeting, staff_review_meeting.person_id FROM public.staff_review_meeting GROUP BY staff_review_meeting.person_id) most_recent ON ((person.id = most_recent.person_id))) LEFT JOIN public.person ur ON ((person.usual_reviewer_id = ur.id))) LEFT JOIN public.person r ON ((staff_review_meeting.reviewer_id = r.id))) JOIN public._physical_status_v3 ON ((_physical_status_v3.person_id = person.id))) JOIN cache._latest_role lr ON ((person.id = lr.person_id))) JOIN (SELECT max(_all_roles.funding_end_date) AS funding_end_date, _all_roles.person_id FROM public._all_roles_v12 _all_roles GROUP BY _all_roles.person_id) newest_funding ON ((newest_funding.person_id = person.id))) WHERE (((_physical_status_v3.status_id)::text = 'Current'::text) AND (lr.role_tablename = 'post_history'::text));


ALTER TABLE hotwire."30_Report/Staff_Review_and_Development" OWNER TO dev;

--
-- Name: 30_Report/Staff_Review_and_Development_Historical; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "30_Report/Staff_Review_and_Development_Historical" AS
    SELECT DISTINCT person.id, person.id AS person_id, person.gender_id, lr.post_category_id, lr.supervisor_id, person.usual_reviewer_id, ur.email_address AS usual_reviewer_email, staff_review_meeting.reviewer_id, r.email_address AS reviewer_email, most_recent.date_of_meeting, ARRAY(SELECT other_meetings.date_of_meeting FROM (SELECT staff_review_meeting.date_of_meeting, staff_review_meeting.person_id FROM public.staff_review_meeting EXCEPT SELECT max(staff_review_meeting.date_of_meeting) AS max, staff_review_meeting.person_id FROM public.staff_review_meeting GROUP BY staff_review_meeting.person_id) other_meetings WHERE (other_meetings.person_id = person.id)) AS others, person.next_review_due, person.continuous_employment_start_date, newest_funding.funding_end_date, _physical_status_v3.status_id FROM ((((((((public.person LEFT JOIN cache._latest_role _latest_role ON ((person.id = _latest_role.person_id))) LEFT JOIN public.staff_review_meeting ON ((staff_review_meeting.person_id = person.id))) LEFT JOIN public.person ur ON ((ur.id = person.usual_reviewer_id))) LEFT JOIN public.person r ON ((staff_review_meeting.reviewer_id = r.id))) LEFT JOIN (SELECT max(staff_review_meeting.date_of_meeting) AS date_of_meeting, staff_review_meeting.person_id FROM public.staff_review_meeting GROUP BY staff_review_meeting.person_id) most_recent ON ((person.id = most_recent.person_id))) JOIN public._physical_status_v3 ON ((_physical_status_v3.person_id = person.id))) JOIN cache._latest_role lr ON ((person.id = lr.person_id))) JOIN (SELECT max(_all_roles.funding_end_date) AS funding_end_date, _all_roles.person_id FROM public._all_roles_v12 _all_roles GROUP BY _all_roles.person_id) newest_funding ON ((newest_funding.person_id = person.id))) WHERE (lr.role_tablename = 'post_history'::text);


ALTER TABLE hotwire."30_Report/Staff_Review_and_Development_Historical" OWNER TO dev;

--
-- Name: VIEW "30_Report/Staff_Review_and_Development_Historical"; Type: COMMENT; Schema: hotwire; Owner: dev
--

COMMENT ON VIEW "30_Report/Staff_Review_and_Development_Historical" IS 'see ticket 113519 but like Staff_Review_and_Development but with gender and past staff included';


--
-- Name: 30_Report/Submission_Time_Taken; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW "30_Report/Submission_Time_Taken" AS
    SELECT a.id, a.person_id, a.funding, a.date_registered_for_phd, a.phd_date_submitted, a.supervisor_id, a.phd_duration, a.phd_duration_days, a.status_id, a.end_date, a._surname, a._first_names FROM (SELECT postgraduate_studentship.id, postgraduate_studentship.person_id, postgraduate_studentship.filemaker_funding AS funding, postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.first_supervisor_id AS supervisor_id, _postgrad_end_dates_v5.phd_duration, _postgrad_end_dates_v5.phd_duration_days, _postgrad_end_dates_v5.status_id, _postgrad_end_dates_v5.end_date, person.surname AS _surname, person.first_names AS _first_names FROM ((public.postgraduate_studentship JOIN public._postgrad_end_dates_v5 USING (id)) LEFT JOIN public.person ON ((person.id = postgraduate_studentship.person_id)))) a ORDER BY a._surname, a._first_names;


ALTER TABLE hotwire."30_Report/Submission_Time_Taken" OWNER TO cen1001;

--
-- Name: 30_WPKG/25_paper_licence_certs; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "30_WPKG/25_paper_licence_certs" AS
    SELECT paper_licence_certificate.id, paper_licence_certificate.asset_tag, ROW('application/pdf'::character varying, paper_licence_certificate.image_oid)::public.blobtype AS image_oid FROM public.paper_licence_certificate;


ALTER TABLE hotwire."30_WPKG/25_paper_licence_certs" OWNER TO postgres;

--
-- Name: 30_WPKG/50_Host_to_package_ro; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "30_WPKG/50_Host_to_package_ro" AS
    SELECT DISTINCT mm_system_image_licence_instance.system_image_id AS id, split_part((ip_address.hostname)::text, '.'::text, 1) AS hostname, ARRAY(SELECT software_package.name FROM (((public.mm_system_image_licence_instance s JOIN public.licence_instance ON ((s.licence_instance_id = licence_instance.id))) JOIN public.mm_software_package_is_licensed_by_licence t ON ((t.licence_id = licence_instance.licence_id))) JOIN public.software_package ON ((t.software_package_id = software_package.id))) WHERE (s.system_image_id = mm_system_image_licence_instance.system_image_id)) AS package_names FROM ((public.mm_system_image_licence_instance JOIN public.mm_system_image_ip_address USING (system_image_id)) JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) ORDER BY mm_system_image_licence_instance.system_image_id, split_part((ip_address.hostname)::text, '.'::text, 1), ARRAY(SELECT software_package.name FROM (((public.mm_system_image_licence_instance s JOIN public.licence_instance ON ((s.licence_instance_id = licence_instance.id))) JOIN public.mm_software_package_is_licensed_by_licence t ON ((t.licence_id = licence_instance.licence_id))) JOIN public.software_package ON ((t.software_package_id = software_package.id))) WHERE (s.system_image_id = mm_system_image_licence_instance.system_image_id));


ALTER TABLE hotwire."30_WPKG/50_Host_to_package_ro" OWNER TO postgres;

--
-- Name: hw_user_pref_seq; Type: SEQUENCE; Schema: hotwire; Owner: postgres
--

CREATE SEQUENCE hw_user_pref_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_user_pref_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hw_User Preferences; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE "hw_User Preferences" (
    id integer DEFAULT nextval('hw_user_pref_seq'::regclass) NOT NULL,
    preference_id integer,
    preference_value character varying,
    user_id bigint
);


ALTER TABLE hotwire."hw_User Preferences" OWNER TO postgres;

--
-- Name: 90_Action/Hotwire/User Preferences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "90_Action/Hotwire/User Preferences" AS
    SELECT "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id AS "Postgres_User_id" FROM "hw_User Preferences";


ALTER TABLE hotwire."90_Action/Hotwire/User Preferences" OWNER TO postgres;

--
-- Name: IT_status_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "IT_status_hid" AS
    SELECT it_task_status.id AS "IT_status_id", it_task_status.name AS "IT_status_hid" FROM public.it_task_status;


ALTER TABLE hotwire."IT_status_hid" OWNER TO dev;

--
-- Name: IT_strategic_goal_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "IT_strategic_goal_hid" AS
    SELECT it_strategic_goal.id AS "IT_strategic_goal_id", it_strategic_goal.name AS "IT_strategic_goal_hid" FROM public.it_strategic_goal;


ALTER TABLE hotwire."IT_strategic_goal_hid" OWNER TO dev;

--
-- Name: IT_task_leader_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "IT_task_leader_hid" AS
    SELECT computer_officer_hid.computer_officer_id AS "IT_task_leader_id", computer_officer_hid.computer_officer_hid AS "IT_task_leader_hid" FROM computer_officer_hid;


ALTER TABLE hotwire."IT_task_leader_hid" OWNER TO dev;

--
-- Name: IT_task_status_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "IT_task_status_hid" AS
    SELECT it_task_status.id AS "IT_task_status_id", it_task_status.name AS "IT_task_status_hid" FROM public.it_task_status;


ALTER TABLE hotwire."IT_task_status_hid" OWNER TO dev;

--
-- Name: Postgres_User_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Postgres_User_hid" AS
    SELECT (pg_roles.oid)::bigint AS "Postgres_User_id", pg_roles.rolname AS "Postgres_User_hid" FROM pg_roles;


ALTER TABLE hotwire."Postgres_User_hid" OWNER TO postgres;

--
-- Name: Primary_strategic_goal_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW "Primary_strategic_goal_hid" AS
    SELECT "IT_strategic_goal_hid"."IT_strategic_goal_id" AS "Primary_strategic_goal_id", "IT_strategic_goal_hid"."IT_strategic_goal_hid" AS "Primary_strategic_goal_hid" FROM "IT_strategic_goal_hid";


ALTER TABLE hotwire."Primary_strategic_goal_hid" OWNER TO dev;

--
-- Name: SelfService/Licences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "SelfService/Licences" AS
    SELECT (1)::bigint AS id, '131.111.112.12'::inet AS ip, 'A Package'::character varying AS software, false AS installed, false AS locked, 'Installed by Someone. At some stage'::character varying AS note;


ALTER TABLE hotwire."SelfService/Licences" OWNER TO postgres;

--
-- Name: _column_data; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW _column_data AS
    SELECT pg_attribute.attname, pg_type.typname, pg_attribute.atttypmod, pg_class.relname, pg_attribute.attnotnull, pg_attribute.atthasdef, (pg_type.typelem > (0)::oid) AS "array", (SELECT child.typname FROM pg_type child WHERE (child.oid = pg_type.typelem)) AS elementtype FROM (((pg_attribute JOIN pg_type ON ((pg_attribute.atttypid = pg_type.oid))) JOIN pg_class ON ((pg_attribute.attrelid = pg_class.oid))) JOIN pg_namespace ON ((pg_class.relnamespace = pg_namespace.oid))) WHERE (((pg_namespace.nspname = 'public'::name) OR (pg_namespace.nspname = 'hotwire'::name)) AND (pg_attribute.attnum > 0));


ALTER TABLE hotwire._column_data OWNER TO dev;

--
-- Name: _deptest; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _deptest AS
    WITH RECURSIVE tree AS (SELECT 'person'::text AS tree, 0 AS level, 'pg_class'::regclass AS classid, (16856)::oid AS objid UNION ALL SELECT ((((tree.tree || ' <-- '::text) || ((pg_depend.classid)::regclass)::text) || '-'::text) || (pg_depend.objid)::text), (tree.level + 1), pg_depend.classid, pg_depend.objid FROM (tree JOIN pg_depend ON ((((tree.classid)::oid = pg_depend.refclassid) AND (tree.objid = pg_depend.refobjid))))) SELECT tree.tree FROM tree WHERE (tree.level < 10);


ALTER TABLE hotwire._deptest OWNER TO postgres;

--
-- Name: hw_pref_seq; Type: SEQUENCE; Schema: hotwire; Owner: dev
--

CREATE SEQUENCE hw_pref_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_pref_seq OWNER TO dev;

--
-- Name: hw_Preferences; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE "hw_Preferences" (
    id integer DEFAULT nextval('hw_pref_seq'::regclass) NOT NULL,
    hw_preference_name character varying,
    hw_preference_type_id integer,
    hw_preference_const character varying
);


ALTER TABLE hotwire."hw_Preferences" OWNER TO postgres;

--
-- Name: hw_preference_type_seq; Type: SEQUENCE; Schema: hotwire; Owner: postgres
--

CREATE SEQUENCE hw_preference_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_preference_type_seq OWNER TO postgres;

--
-- Name: hw_preference_type_hid; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE hw_preference_type_hid (
    hw_preference_type_id integer DEFAULT nextval('hw_preference_type_seq'::regclass) NOT NULL,
    hw_preference_type_hid character varying
);


ALTER TABLE hotwire.hw_preference_type_hid OWNER TO postgres;

--
-- Name: _preferences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _preferences AS
    SELECT "Preferences".hw_preference_const AS preference_name, a.preference_value, hw_preference_type_hid.hw_preference_type_hid AS preference_type FROM ((((SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM "hw_User Preferences" "User Preferences" WHERE ("User Preferences".user_id IS NULL) UNION ALL SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM (((pg_auth_members m JOIN pg_roles b ON ((m.roleid = b.oid))) JOIN pg_roles r ON ((m.member = r.oid))) JOIN "hw_User Preferences" "User Preferences" ON ((b.oid = ("User Preferences".user_id)::oid))) WHERE (r.rolname = "current_user"())) UNION ALL SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM (pg_roles JOIN "hw_User Preferences" "User Preferences" ON ((pg_roles.oid = ("User Preferences".user_id)::oid))) WHERE (pg_roles.rolname = "current_user"())) a JOIN "hw_Preferences" "Preferences" ON ((a.preference_id = "Preferences".id))) JOIN hw_preference_type_hid USING (hw_preference_type_id));


ALTER TABLE hotwire._preferences OWNER TO postgres;

--
-- Name: _role_data; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW _role_data AS
    WITH RECURSIVE membership_tree(grpid, userid) AS (SELECT pg_roles.oid, pg_roles.oid FROM pg_roles UNION ALL SELECT m_1.roleid, t_1.userid FROM pg_auth_members m_1, membership_tree t_1 WHERE (m_1.member = t_1.grpid)) SELECT DISTINCT r.rolname AS member, m.rolname AS role FROM membership_tree t, pg_roles r, pg_roles m WHERE ((t.grpid = m.oid) AND (t.userid = r.oid)) ORDER BY r.rolname, m.rolname;


ALTER TABLE hotwire._role_data OWNER TO dev;

--
-- Name: _subviews; Type: TABLE; Schema: hotwire; Owner: dev; Tablespace: 
--

CREATE TABLE _subviews (
    view name NOT NULL,
    subview name NOT NULL,
    target name
);


ALTER TABLE hotwire._subviews OWNER TO dev;

--
-- Name: _view_data; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _view_data AS
    SELECT pgns.nspname AS schema, pg_class.relname AS view, pg_class.relacl AS perms, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'insert'::text) AS insert, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'update'::text) AS update, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'delete'::text) AS delete, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'select'::text) AS "select", ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE ((((r.ev_type = '4'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname)) AND (c.relnamespace = pg_class.relnamespace))) > 0) AS delete_rule, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE ((((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname)) AND (c.relnamespace = pg_class.relnamespace))) > 0) AS insert_rule, (SELECT ((r.ev_action)::text ~~ '%returningList:%'::text) FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE ((((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname)) AND (c.relnamespace = pg_class.relnamespace))) AS insert_returning, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE ((((r.ev_type = '2'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname)) AND (c.relnamespace = pg_class.relnamespace))) > 0) AS update_rule, pg_views.definition, public._primarytable((pg_class.relname)::character varying) AS primary_table, CASE WHEN (regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ ('%ORDER BY%'::character varying)::text) THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text) ELSE NULL::text END AS order_by FROM ((pg_class JOIN pg_namespace pgns ON ((pg_class.relnamespace = pgns.oid))) JOIN pg_views ON (((pg_class.relname = pg_views.viewname) AND (pgns.nspname = pg_views.schemaname)))) WHERE ((pg_class.relname ~~ '%/%'::text) AND (pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name])));


ALTER TABLE hotwire._view_data OWNER TO postgres;

--
-- Name: _view_data2; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _view_data2 AS
    SELECT pgns.nspname AS schema, pg_class.relname AS view, pg_class.relacl AS perms, has_table_privilege(pg_class.oid, 'insert'::text) AS insert, has_table_privilege(pg_class.oid, 'update'::text) AS update, has_table_privilege(pg_class.oid, 'delete'::text) AS delete, has_table_privilege(pg_class.oid, 'select'::text) AS "select", ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '4'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS delete_rule, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS insert_rule, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '2'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS update_rule, pg_views.definition, regexp_replace(regexp_replace(pg_views.definition, '.*FROM \(*'::text, ''::text), '[ ;].*'::text, ''::text) AS primarytable FROM ((pg_class JOIN pg_namespace pgns ON ((pg_class.relnamespace = pgns.oid))) JOIN pg_views ON ((pg_class.relname = pg_views.viewname))) WHERE ((pg_class.relname ~~ '%/%'::text) AND (pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name])));


ALTER TABLE hotwire._view_data2 OWNER TO postgres;

--
-- Name: _view_data_new; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _view_data_new AS
    SELECT pgns.nspname AS schema, pg_class.relname AS view, pg_class.relacl AS perms, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'insert'::text) AS insert, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'update'::text) AS update, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'delete'::text) AS delete, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'select'::text) AS "select", ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '4'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS delete_rule, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS insert_rule, (SELECT ((r.ev_action)::text ~~ '%returningList:%'::text) FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) AS insert_returning, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '2'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS update_rule, pg_views.definition, public._primarytable((pg_class.relname)::character varying) AS primary_table FROM ((pg_class JOIN pg_namespace pgns ON ((pg_class.relnamespace = pgns.oid))) JOIN pg_views ON ((pg_class.relname = pg_views.viewname))) WHERE ((pg_class.relname ~~ '%/%'::text) AND (pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name])));


ALTER TABLE hotwire._view_data_new OWNER TO postgres;

--
-- Name: academic_title_(optional)_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "academic_title_(optional)_hid" AS
    SELECT user_prereg_title_hid.user_prereg_title_id AS "academic_title_(optional)_id", user_prereg_title_hid.user_prereg_title_hid AS "academic_title_(optional)_hid" FROM public.user_prereg_title_hid;


ALTER TABLE hotwire."academic_title_(optional)_hid" OWNER TO postgres;

--
-- Name: all_cos_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW all_cos_hid AS
    SELECT computer_officer_hid.computer_officer_id AS all_cos_id, computer_officer_hid.computer_officer_hid AS all_cos_hid FROM computer_officer_hid;


ALTER TABLE hotwire.all_cos_hid OWNER TO dev;

--
-- Name: licence_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW licence_hid AS
    SELECT licence.id AS licence_id, (((licence.short_name)::text || CASE WHEN licence.is_upgrade THEN ' [upgrade]'::text ELSE ''::text END) || CASE WHEN licence.can_install_on_any_managed_machine THEN ' [global]'::text ELSE ''::text END) AS licence_hid FROM public.licence;


ALTER TABLE hotwire.licence_hid OWNER TO postgres;

--
-- Name: allowed_by_other_licence_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW allowed_by_other_licence_hid AS
    SELECT licence_hid.licence_id AS allowed_by_other_licence_id, licence_hid.licence_hid AS allowed_by_other_licence_hid FROM licence_hid;


ALTER TABLE hotwire.allowed_by_other_licence_hid OWNER TO postgres;

--
-- Name: alternate_admin_contact_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW alternate_admin_contact_hid AS
    SELECT person_hid.person_id AS alternate_admin_contact_id, person_hid.person_hid AS alternate_admin_contact_hid FROM public.person_hid;


ALTER TABLE hotwire.alternate_admin_contact_hid OWNER TO postgres;

--
-- Name: architecture_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW architecture_hid AS
    SELECT architectures.id AS architecture_id, architectures.arch AS architecture_hid FROM public.architectures;


ALTER TABLE hotwire.architecture_hid OWNER TO postgres;

--
-- Name: backup_strategy_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW backup_strategy_hid AS
    SELECT backup_strategy.id AS backup_strategy_id, backup_strategy.name AS backup_strategy_hid FROM public.backup_strategy;


ALTER TABLE hotwire.backup_strategy_hid OWNER TO postgres;

--
-- Name: building_region_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW building_region_hid AS
    SELECT building_region_hid.building_region_id, building_region_hid.building_region_hid FROM public.building_region_hid;


ALTER TABLE hotwire.building_region_hid OWNER TO postgres;

--
-- Name: cabinet_a_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW cabinet_a_hid AS
    SELECT cabinet.id AS cabinet_a_id, ((cabinet.cabno || ':'::text) || (cabinet.name)::text) AS cabinet_a_hid FROM public.cabinet;


ALTER TABLE hotwire.cabinet_a_hid OWNER TO postgres;

--
-- Name: cabinet_b_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW cabinet_b_hid AS
    SELECT cabinet.id AS cabinet_b_id, ((cabinet.cabno || ':'::text) || (cabinet.name)::text) AS cabinet_b_hid FROM public.cabinet;


ALTER TABLE hotwire.cabinet_b_hid OWNER TO postgres;

--
-- Name: cabinet_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW cabinet_hid AS
    SELECT cabinet.id AS cabinet_id, cabinet.name AS cabinet_hid FROM public.cabinet;


ALTER TABLE hotwire.cabinet_hid OWNER TO postgres;

--
-- Name: cable_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW cable_type_hid AS
    SELECT cable_type_hid.cable_type_id, cable_type_hid.cable_type_hid FROM public.cable_type_hid;


ALTER TABLE hotwire.cable_type_hid OWNER TO postgres;

--
-- Name: cambridge_college_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW cambridge_college_hid AS
    SELECT cambridge_college.id AS cambridge_college_id, cambridge_college.name AS cambridge_college_hid FROM public.cambridge_college;


ALTER TABLE hotwire.cambridge_college_hid OWNER TO postgres;

--
-- Name: chair_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW chair_hid AS
    SELECT person_hid.person_id AS chair_id, person_hid.person_hid AS chair_hid FROM person_hid;


ALTER TABLE hotwire.chair_hid OWNER TO cen1001;

--
-- Name: co_mentor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW co_mentor_hid AS
    SELECT person_hid.person_id AS co_mentor_id, person_hid.person_hid AS co_mentor_hid FROM public.person_hid;


ALTER TABLE hotwire.co_mentor_hid OWNER TO postgres;

--
-- Name: co_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW co_supervisor_hid AS
    SELECT person_hid.person_id AS co_supervisor_id, person_hid.person_hid AS co_supervisor_hid FROM public.person_hid;


ALTER TABLE hotwire.co_supervisor_hid OWNER TO postgres;

--
-- Name: column_data; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW column_data AS
    SELECT pg_attribute.attname, pg_type.typname, pg_attribute.atttypmod, pg_class.relname, pg_attribute.attnotnull, pg_attribute.atthasdef FROM (((pg_attribute JOIN pg_type ON ((pg_attribute.atttypid = pg_type.oid))) JOIN pg_class ON ((pg_attribute.attrelid = pg_class.oid))) JOIN pg_namespace ON ((pg_class.relnamespace = pg_namespace.oid))) WHERE ((pg_namespace.nspname = 'public'::name) AND (pg_attribute.attnum > 0));


ALTER TABLE hotwire.column_data OWNER TO postgres;

--
-- Name: computer_rep_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW computer_rep_hid AS
    SELECT person_hid.person_id AS computer_rep_id, person_hid.person_hid AS computer_rep_hid FROM public.person_hid;


ALTER TABLE hotwire.computer_rep_hid OWNER TO dev;

--
-- Name: connector_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW connector_type_hid AS
    SELECT connector_type_hid.connector_type_id, connector_type_hid.connector_type_hid FROM public.connector_type_hid;


ALTER TABLE hotwire.connector_type_hid OWNER TO postgres;

--
-- Name: default_system_image_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW default_system_image_hid AS
    SELECT system_image_hid.system_image_id AS default_system_image_id, system_image_hid.system_image_hid AS default_system_image_hid FROM public.system_image_hid;


ALTER TABLE hotwire.default_system_image_hid OWNER TO postgres;

--
-- Name: software_package_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW software_package_hid AS
    SELECT software_package.id AS software_package_id, (((('['::text || array_to_string(ARRAY(SELECT DISTINCT os_class_hid.abbrev FROM ((public.mm_software_package_can_install_on_os JOIN public.operating_system ON ((mm_software_package_can_install_on_os.operating_system_id = operating_system.id))) JOIN public.os_class_hid USING (os_class_id)) WHERE (mm_software_package_can_install_on_os.software_package_id = software_package.id)), ','::text)) || '] '::text) || (((((software_package.name)::text || ' ['::text) || COALESCE(((software_package.wpkg_package_name)::text || ', '::text), ''::text)) || ' '::text) || (COALESCE(software_package.reporting_version, ''::character varying))::text)) || ']'::text) AS software_package_hid FROM public.software_package ORDER BY software_package.name;


ALTER TABLE hotwire.software_package_hid OWNER TO postgres;

--
-- Name: depended_software_package_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW depended_software_package_hid AS
    SELECT software_package_hid.software_package_id AS depended_software_package_id, software_package_hid.software_package_hid AS depended_software_package_hid FROM software_package_hid;


ALTER TABLE hotwire.depended_software_package_hid OWNER TO cen1001;

--
-- Name: dept_telephone_number_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW dept_telephone_number_hid AS
    SELECT dept_telephone_number.id AS dept_telephone_number_id, ((dept_telephone_number.extension_number)::text || CASE WHEN (dept_telephone_number.fax = true) THEN ' (fax)'::text WHEN (dept_telephone_number.personal_line = false) THEN ' (shared)'::text ELSE ''::text END) AS dept_telephone_number_hid FROM public.dept_telephone_number ORDER BY dept_telephone_number.extension_number;


ALTER TABLE hotwire.dept_telephone_number_hid OWNER TO postgres;

--
-- Name: fire_warden_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW fire_warden_hid AS
    SELECT fire_warden.fire_warden_id, person_hid.person_hid AS fire_warden_hid FROM (public.fire_warden JOIN person_hid USING (person_id));


ALTER TABLE hotwire.fire_warden_hid OWNER TO cen1001;

--
-- Name: deputy_fire_warden_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW deputy_fire_warden_hid AS
    SELECT fire_warden_hid.fire_warden_id AS deputy_fire_warden_id, fire_warden_hid.fire_warden_hid AS deputy_fire_warden_hid FROM fire_warden_hid;


ALTER TABLE hotwire.deputy_fire_warden_hid OWNER TO cen1001;

--
-- Name: dns_domain_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW dns_domain_hid AS
    SELECT dns_domains.id AS dns_domain_id, dns_domains.name AS dns_domain_hid FROM public.dns_domains;


ALTER TABLE hotwire.dns_domain_hid OWNER TO cen1001;

--
-- Name: dom0_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW dom0_hid AS
    SELECT system_image.id AS dom0_id, ((((COALESCE(ip_address.hostname, ((COALESCE(((hardware.name)::text || ' '::text), ' '::text) || COALESCE((hardware.manufacturer)::text, ' '::text)))::character varying))::text || ' ('::text) || (operating_system.os)::text) || ')'::text) AS dom0_hid FROM ((((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id))) LEFT JOIN public.mm_system_image_ip_address ON ((system_image.id = mm_system_image_ip_address.system_image_id))) LEFT JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) ORDER BY ((((COALESCE(ip_address.hostname, ((COALESCE(((hardware.name)::text || ' '::text), ' '::text) || COALESCE((hardware.manufacturer)::text, ' '::text)))::character varying))::text || ' ('::text) || (operating_system.os)::text) || ')'::text);


ALTER TABLE hotwire.dom0_hid OWNER TO postgres;

--
-- Name: domain_for_dns_server_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW domain_for_dns_server_hid AS
    SELECT dns_domain_hid.dns_domain_id AS domain_for_dns_server_id, dns_domain_hid.dns_domain_hid AS domain_for_dns_server_hid FROM dns_domain_hid;


ALTER TABLE hotwire.domain_for_dns_server_hid OWNER TO cen1001;

--
-- Name: domu_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW domu_hid AS
    SELECT system_image.id AS domu_id, ((((COALESCE(ip_address.hostname, ((COALESCE(((hardware.name)::text || ' '::text), ' '::text) || COALESCE((hardware.manufacturer)::text, ' '::text)))::character varying))::text || ' ('::text) || (operating_system.os)::text) || ')'::text) AS domu_hid FROM ((((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id))) LEFT JOIN public.mm_system_image_ip_address ON ((system_image.id = mm_system_image_ip_address.system_image_id))) LEFT JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) ORDER BY ((((COALESCE(ip_address.hostname, ((COALESCE(((hardware.name)::text || ' '::text), ' '::text) || COALESCE((hardware.manufacturer)::text, ' '::text)))::character varying))::text || ' ('::text) || (operating_system.os)::text) || ')'::text);


ALTER TABLE hotwire.domu_hid OWNER TO postgres;

--
-- Name: erasmus_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW erasmus_type_hid AS
    SELECT erasmus_type_hid.erasmus_type_id, erasmus_type_hid.erasmus_type_hid FROM public.erasmus_type_hid;


ALTER TABLE hotwire.erasmus_type_hid OWNER TO postgres;

--
-- Name: exclude_person_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW exclude_person_hid AS
    SELECT person.id AS exclude_person_id, ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text) AS exclude_person_hid FROM (public.person LEFT JOIN public.title_hid USING (title_id)) ORDER BY ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text);


ALTER TABLE hotwire.exclude_person_hid OWNER TO postgres;

--
-- Name: fai_class_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fai_class_hid AS
    SELECT fai_class_hid.fai_class_id, fai_class_hid.fai_class_hid FROM public.fai_class_hid;


ALTER TABLE hotwire.fai_class_hid OWNER TO postgres;

--
-- Name: fibre_termination_a_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_termination_a_hid AS
    SELECT fibre_termination.id AS fibre_termination_a_id, fibre_termination.name AS fibre_termination_a_hid FROM public.fibre_termination;


ALTER TABLE hotwire.fibre_termination_a_hid OWNER TO postgres;

--
-- Name: fibre_termination_b_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_termination_b_hid AS
    SELECT fibre_termination.id AS fibre_termination_b_id, fibre_termination.name AS fibre_termination_b_hid FROM public.fibre_termination;


ALTER TABLE hotwire.fibre_termination_b_hid OWNER TO postgres;

--
-- Name: fibre_run_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fibre_run_hid AS
    SELECT fibre_run.id AS fibre_run_id, (((((fibre_panel_a_hid.fibre_panel_a_hid || '-'::text) || fibre_panel_b_hid.fibre_panel_b_hid) || ' ['::text) || (SELECT array_to_string(ARRAY(SELECT ((count(*) || '/'::text) || (fibre_type_hid.fibre_type_abbrev)::text) FROM ((public.fibre_core JOIN public.fibre_core_type ON ((fibre_core.fibre_core_type_id = fibre_core_type.id))) JOIN public.fibre_type_hid USING (fibre_type_id)) WHERE (fibre_core.fibre_run_id = fibre_run.id) GROUP BY fibre_type_hid.fibre_type_abbrev), ','::text) AS array_to_string)) || ']'::text) AS fibre_run_hid FROM ((((public.fibre_run JOIN fibre_termination_a_hid ON ((fibre_termination_a_hid.fibre_termination_a_id = fibre_run.term_a_id))) JOIN fibre_termination_b_hid ON ((fibre_termination_b_hid.fibre_termination_b_id = fibre_run.term_b_id))) JOIN fibre_panel_a_hid ON ((fibre_panel_a_hid.fibre_panel_a_id = fibre_run.panel_a_id))) JOIN fibre_panel_b_hid ON ((fibre_panel_b_hid.fibre_panel_b_id = fibre_run.panel_b_id)));


ALTER TABLE hotwire.fibre_run_hid OWNER TO postgres;

--
-- Name: fibre_termination_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW fibre_termination_hid AS
    SELECT fibre_termination.id AS fibre_termination_id, fibre_termination.name AS fibre_termination_hid FROM public.fibre_termination;


ALTER TABLE hotwire.fibre_termination_hid OWNER TO dev;

--
-- Name: fibre_test_type_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW fibre_test_type_hid AS
    SELECT fibre_test_type.id AS fibre_test_type_id, ((fibre_test_type.test)::text || COALESCE((fibre_test_type.gbs || 'GB/s'::text), ''::text)) AS fibre_test_type_hid FROM public.fibre_test_type;


ALTER TABLE hotwire.fibre_test_type_hid OWNER TO dev;

--
-- Name: fibre_type_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW fibre_type_hid AS
    SELECT fibre_type_hid.fibre_type_id, fibre_type_hid.fibre_type_hid, fibre_type_hid.fibre_type_abbrev FROM public.fibre_type_hid;


ALTER TABLE hotwire.fibre_type_hid OWNER TO dev;

--
-- Name: fire_warden_area_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW fire_warden_area_hid AS
    SELECT fire_warden_area.fire_warden_area_id, fire_warden_area.name AS fire_warden_area_hid FROM public.fire_warden_area;


ALTER TABLE hotwire.fire_warden_area_hid OWNER TO cen1001;

--
-- Name: firetrace_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW firetrace_type_hid AS
    SELECT firetrace_type_hid.firetrace_type_id, firetrace_type_hid.firetrace_type_hid FROM public.firetrace_type_hid;


ALTER TABLE hotwire.firetrace_type_hid OWNER TO postgres;

--
-- Name: first_mentor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW first_mentor_hid AS
    SELECT person.id AS first_mentor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS first_mentor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE hotwire.first_mentor_hid OWNER TO postgres;

--
-- Name: first_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW first_supervisor_hid AS
    SELECT person.id AS first_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS first_supervisor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE hotwire.first_supervisor_hid OWNER TO postgres;

--
-- Name: firstaider_funding_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW firstaider_funding_hid AS
    SELECT firstaider_funding.id AS firstaider_funding_id, firstaider_funding.funding AS firstaider_funding_hid FROM public.firstaider_funding;


ALTER TABLE hotwire.firstaider_funding_hid OWNER TO postgres;

--
-- Name: fume_hood_device_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fume_hood_device_type_hid AS
    SELECT fume_hood_device_type_hid.fume_hood_device_type_id, fume_hood_device_type_hid.fume_hood_device_type_hid FROM public.fume_hood_device_type_hid;


ALTER TABLE hotwire.fume_hood_device_type_hid OWNER TO postgres;

--
-- Name: fume_hood_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW fume_hood_type_hid AS
    SELECT fume_hood_type_hid.fume_hood_type_id, fume_hood_type_hid.fume_hood_type_hid FROM public.fume_hood_type_hid;


ALTER TABLE hotwire.fume_hood_type_hid OWNER TO postgres;

--
-- Name: gender_(required)_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "gender_(required)_hid" AS
    SELECT gender_hid.gender_id AS "gender_(required)_id", gender_hid.gender_hid AS "gender_(required)_hid" FROM public.gender_hid;


ALTER TABLE hotwire."gender_(required)_hid" OWNER TO postgres;

--
-- Name: gender_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW gender_hid AS
    SELECT gender_hid.gender_id, gender_hid.gender_hid FROM public.gender_hid;


ALTER TABLE hotwire.gender_hid OWNER TO postgres;

--
-- Name: goal_class_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW goal_class_hid AS
    SELECT goal_class_hid.id AS goal_class_id, goal_class_hid.hid AS goal_class_hid FROM public.goal_class_hid;


ALTER TABLE hotwire.goal_class_hid OWNER TO postgres;

--
-- Name: group_fileserver_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW group_fileserver_hid AS
    SELECT group_fileserver.id AS group_fileserver_id, group_fileserver.hostname_for_users AS group_fileserver_hid FROM public.group_fileserver;


ALTER TABLE hotwire.group_fileserver_hid OWNER TO postgres;

--
-- Name: hardware_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hardware_hid AS
    SELECT hardware.id AS hardware_id, (COALESCE(((hardware.name)::text || ''::text)) || COALESCE((((' ('::text || (hardware.model)::text) || ')'::text) || ''::text))) AS hardware_hid FROM public.hardware ORDER BY (COALESCE(((hardware.name)::text || ''::text)) || COALESCE((((' ('::text || (hardware.model)::text) || ')'::text) || ''::text)));


ALTER TABLE hotwire.hardware_hid OWNER TO postgres;

--
-- Name: hardware_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hardware_type_hid AS
    SELECT hardware_type.id AS hardware_type_id, hardware_type.name AS hardware_type_hid FROM public.hardware_type;


ALTER TABLE hotwire.hardware_type_hid OWNER TO postgres;

--
-- Name: head_of_group_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW head_of_group_hid AS
    SELECT person_hid.person_id AS head_of_group_id, person_hid.person_hid AS head_of_group_hid FROM public.person_hid;


ALTER TABLE hotwire.head_of_group_hid OWNER TO dev;

--
-- Name: host_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW host_hid AS
    SELECT person_hid.person_id AS host_id, person_hid.person_hid AS host_hid FROM public.person_hid;


ALTER TABLE hotwire.host_hid OWNER TO postgres;

--
-- Name: host_system_image_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW host_system_image_hid AS
    SELECT system_image.id AS host_system_image_id, ip_address.hostname AS host_system_image_hid FROM ((public.system_image JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = system_image.id))) JOIN public.ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (ip_address.id = (SELECT ip_address.id FROM ((public.system_image s2 JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = s2.id))) JOIN public.ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (s2.id = system_image.id) LIMIT 1));


ALTER TABLE hotwire.host_system_image_hid OWNER TO postgres;

--
-- Name: include_person_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW include_person_hid AS
    SELECT person.id AS include_person_id, ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text) AS include_person_hid FROM (public.person LEFT JOIN public.title_hid USING (title_id)) ORDER BY ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text);


ALTER TABLE hotwire.include_person_hid OWNER TO postgres;

--
-- Name: infraction_type_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW infraction_type_hid AS
    SELECT infraction_type_hid.infraction_type_id, infraction_type_hid.infraction_type_hid FROM public.infraction_type_hid;


ALTER TABLE hotwire.infraction_type_hid OWNER TO dev;

--
-- Name: installer_tag_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW installer_tag_hid AS
    SELECT installer_tag_hid.installer_tag_id, installer_tag_hid.installer_tag_hid FROM public.installer_tag_hid;


ALTER TABLE hotwire.installer_tag_hid OWNER TO postgres;

--
-- Name: ip_address_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW ip_address_hid AS
    SELECT ip_address.id AS ip_address_id, COALESCE(((((ip_address.hostname)::text || ' ('::text) || ip_address.ip) || ')'::text), (ip_address.ip)::text) AS ip_address_hid FROM public.ip_address ORDER BY ip_address.hostname, ip_address.ip;


ALTER TABLE hotwire.ip_address_hid OWNER TO postgres;

--
-- Name: ip_address_qid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW ip_address_qid AS
    SELECT ip_address.id AS ip_address_id, COALESCE(((((ip_address.hostname)::text || ' ('::text) || ip_address.ip) || ')'::text), (ip_address.ip)::text) AS ip_address_hid FROM public.ip_address;


ALTER TABLE hotwire.ip_address_qid OWNER TO postgres;

--
-- Name: it_project_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW it_project_hid AS
    SELECT it_project.id AS it_project_id, it_project.name AS it_project_hid FROM public.it_project;


ALTER TABLE hotwire.it_project_hid OWNER TO cen1001;

--
-- Name: it_strategic_goal_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW it_strategic_goal_hid AS
    SELECT it_strategic_goal.id AS it_strategic_goal_id, it_strategic_goal.name AS it_strategic_goal_hid FROM public.it_strategic_goal;


ALTER TABLE hotwire.it_strategic_goal_hid OWNER TO dev;

--
-- Name: it_task_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW it_task_hid AS
    SELECT it_task.id AS it_task_id, it_task.name AS it_task_hid FROM public.it_task;


ALTER TABLE hotwire.it_task_hid OWNER TO postgres;

--
-- Name: itsales_status_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW itsales_status_hid AS
    SELECT itsales_status_hid.id AS itsales_status_id, itsales_status_hid.status AS itsales_status_hid FROM public.itsales_status_hid;


ALTER TABLE hotwire.itsales_status_hid OWNER TO dev;

--
-- Name: itsales_type_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW itsales_type_hid AS
    SELECT itsales_type_hid.id AS itsales_type_id, itsales_type_hid.type AS itsales_type_hid FROM public.itsales_type_hid;


ALTER TABLE hotwire.itsales_type_hid OWNER TO dev;

--
-- Name: itsales_unit_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW itsales_unit_hid AS
    SELECT itsales_unit_hid.id AS itsales_unit_id, itsales_unit_hid.unit AS itsales_unit_hid FROM public.itsales_unit_hid;


ALTER TABLE hotwire.itsales_unit_hid OWNER TO dev;

--
-- Name: licence_downgraded_to_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW licence_downgraded_to_hid AS
    SELECT licence_hid.licence_id AS licence_downgraded_to_id, licence_hid.licence_hid AS licence_downgraded_to_hid FROM licence_hid;


ALTER TABLE hotwire.licence_downgraded_to_hid OWNER TO postgres;

--
-- Name: linked_socket_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW linked_socket_hid AS
    SELECT socket.id AS linked_socket_id, ((patch_panel_hid.patch_panel_hid || ' - '::text) || (socket.panel_label)::text) AS linked_socket_hid FROM (public.socket JOIN public.patch_panel_hid USING (patch_panel_id));


ALTER TABLE hotwire.linked_socket_hid OWNER TO postgres;

--
-- Name: manager_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW manager_hid AS
    SELECT person_hid.person_id AS manager_id, person_hid.person_hid AS manager_hid FROM public.person_hid;


ALTER TABLE hotwire.manager_hid OWNER TO postgres;

--
-- Name: member_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW member_hid AS
    SELECT person_hid.person_id AS member_id, person_hid.person_hid AS member_hid FROM public.person_hid;


ALTER TABLE hotwire.member_hid OWNER TO postgres;

--
-- Name: mentor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW mentor_hid AS
    SELECT person_hid.person_id AS mentor_id, person_hid.person_hid AS mentor_hid FROM public.person_hid;


ALTER TABLE hotwire.mentor_hid OWNER TO postgres;

--
-- Name: nationality_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW nationality_hid AS
    SELECT nationality.id AS nationality_id, nationality.nationality AS nationality_hid FROM public.nationality ORDER BY nationality.nationality;


ALTER TABLE hotwire.nationality_hid OWNER TO postgres;

--
-- Name: netboot_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW netboot_hid AS
    SELECT netboot_options.id AS netboot_id, netboot_options."desc" AS netboot_hid FROM public.netboot_options;


ALTER TABLE hotwire.netboot_hid OWNER TO dev;

--
-- Name: network_electrical_connection_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW network_electrical_connection_hid AS
    SELECT network_electrical_connection_hid.network_electrical_connection_id, network_electrical_connection_hid.network_electrical_connection_hid FROM public.network_electrical_connection_hid;


ALTER TABLE hotwire.network_electrical_connection_hid OWNER TO dev;

--
-- Name: new_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW new_supervisor_hid AS
    SELECT person_hid.person_id AS new_supervisor_id, person_hid.person_hid AS new_supervisor_hid FROM person_hid;


ALTER TABLE hotwire.new_supervisor_hid OWNER TO dev;

--
-- Name: occupant_group_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW occupant_group_hid AS
    SELECT research_group_hid.research_group_id AS occupant_group_id, research_group_hid.research_group_hid AS occupant_group_hid FROM public.research_group_hid ORDER BY research_group_hid.research_group_hid;


ALTER TABLE hotwire.occupant_group_hid OWNER TO postgres;

--
-- Name: occupants_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW occupants_hid AS
    SELECT person_hid.person_id AS occupants_id, person_hid.person_hid AS occupants_hid FROM public.person_hid;


ALTER TABLE hotwire.occupants_hid OWNER TO postgres;

--
-- Name: old_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW old_supervisor_hid AS
    SELECT person_hid.person_id AS old_supervisor_id, person_hid.person_hid AS old_supervisor_hid FROM person_hid;


ALTER TABLE hotwire.old_supervisor_hid OWNER TO dev;

--
-- Name: operating_system_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW operating_system_hid AS
    SELECT operating_system.id AS operating_system_id, operating_system.os AS operating_system_hid FROM public.operating_system ORDER BY operating_system.os;


ALTER TABLE hotwire.operating_system_hid OWNER TO postgres;

--
-- Name: os_class_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW os_class_hid AS
    SELECT os_class_hid.os_class_id, os_class_hid.os_class_hid FROM public.os_class_hid;


ALTER TABLE hotwire.os_class_hid OWNER TO postgres;

--
-- Name: owner_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW owner_hid AS
    SELECT person.id AS owner_id, (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text) AS owner_hid FROM public.person ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER TABLE hotwire.owner_hid OWNER TO postgres;

--
-- Name: paper_licence_certificate_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW paper_licence_certificate_hid AS
    SELECT paper_licence_certificate.id AS paper_licence_certificate_id, paper_licence_certificate.asset_tag AS paper_licence_certificate_hid FROM public.paper_licence_certificate;


ALTER TABLE hotwire.paper_licence_certificate_hid OWNER TO postgres;

--
-- Name: patch_panel_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW patch_panel_hid AS
    SELECT patch_panel.id AS patch_panel_id, (((cabinet.name)::text || ' - '::text) || (patch_panel.name)::text) AS patch_panel_hid FROM (public.patch_panel LEFT JOIN public.cabinet ON ((patch_panel.cabinet_id = cabinet.id)));


ALTER TABLE hotwire.patch_panel_hid OWNER TO postgres;

--
-- Name: patch_panel_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW patch_panel_type_hid AS
    SELECT patch_panel_type_hid.patch_panel_type_id, patch_panel_type_hid.patch_panel_type_hid FROM public.patch_panel_type_hid;


ALTER TABLE hotwire.patch_panel_type_hid OWNER TO postgres;

--
-- Name: patch_strategy_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW patch_strategy_hid AS
    SELECT patch_strategy.id AS patch_strategy_id, patch_strategy.name AS patch_strategy_hid FROM public.patch_strategy;


ALTER TABLE hotwire.patch_strategy_hid OWNER TO postgres;

--
-- Name: physical_room_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW physical_room_hid AS
    SELECT room.id AS physical_room_id, (((room.name)::text || COALESCE((((' ('::text || COALESCE(((building_floor_hid.building_floor_hid)::text || ', '::text), ''::text)) || (COALESCE(building_region_hid.building_region_hid, building_hid.building_hid))::text) || ')'::text), ''::text)))::character varying(48) AS physical_room_hid FROM (((public.room LEFT JOIN public.building_floor_hid ON ((room.building_floor_id = building_floor_hid.building_floor_id))) LEFT JOIN public.building_region_hid ON ((building_region_hid.building_region_id = room.building_region_id))) LEFT JOIN public.building_hid ON ((building_hid.building_id = room.building_id))) ORDER BY (((room.name)::text || COALESCE((((' ('::text || COALESCE(((building_floor_hid.building_floor_hid)::text || ', '::text), ''::text)) || (COALESCE(building_region_hid.building_region_hid, building_hid.building_hid))::text) || ')'::text), ''::text)))::character varying(48);


ALTER TABLE hotwire.physical_room_hid OWNER TO postgres;

--
-- Name: physical_status_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW physical_status_hid AS
    SELECT status.status_id AS physical_status_id, status.status_id AS physical_status_hid FROM public.status;


ALTER TABLE hotwire.physical_status_hid OWNER TO postgres;

--
-- Name: post_category_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW post_category_hid AS
    (((SELECT 'p3-1'::text AS post_category_id, 'Part III'::text AS post_category_hid UNION SELECT ('e-'::text || (erasmus_type_hid.erasmus_type_id)::text) AS post_category_id, (erasmus_type_hid.erasmus_type_hid)::text AS post_category_hid FROM public.erasmus_type_hid) UNION SELECT ('v-'::text || (visitor_type_hid.visitor_type_id)::text) AS post_category_id, visitor_type_hid.visitor_type_hid AS post_category_hid FROM public.visitor_type_hid) UNION SELECT ('pg-'::text || (postgraduate_studentship_type_hid.postgraduate_studentship_type_id)::text) AS post_category_id, postgraduate_studentship_type_hid.postgraduate_studentship_type_hid AS post_category_hid FROM public.postgraduate_studentship_type_hid) UNION SELECT ('sc-'::text || (staff_category_hid.staff_category_id)::text) AS post_category_id, staff_category_hid.staff_category_hid AS post_category_hid FROM public.staff_category_hid;


ALTER TABLE hotwire.post_category_hid OWNER TO postgres;

--
-- Name: post_history_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW post_history_hid AS
    SELECT post_history.id AS post_history_id, ((person_hid.person_hid || COALESCE((' - '::text || (staff_category.category)::text), ''::text)) || COALESCE((' - '::text || post_history.start_date), ''::text)) AS post_history_hid FROM ((public.post_history JOIN public.staff_category ON ((post_history.staff_category_id = staff_category.id))) JOIN person_hid USING (person_id)) ORDER BY person_hid.person_hid, post_history.start_date DESC, staff_category.category;


ALTER TABLE hotwire.post_history_hid OWNER TO dev;

--
-- Name: postgraduate_studentship_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW postgraduate_studentship_hid AS
    SELECT postgraduate_studentship.id AS postgraduate_studentship_id, (person_hid.person_hid || COALESCE(((' ('::text || (postgraduate_studentship.emplid_number)::text) || ')'::text), ''::text)) AS postgraduate_studentship_hid FROM (public.postgraduate_studentship JOIN public.person_hid USING (person_id));


ALTER TABLE hotwire.postgraduate_studentship_hid OWNER TO postgres;

--
-- Name: postgraduate_studentship_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW postgraduate_studentship_type_hid AS
    SELECT postgraduate_studentship_type.id AS postgraduate_studentship_type_id, postgraduate_studentship_type.name AS postgraduate_studentship_type_hid FROM public.postgraduate_studentship_type;


ALTER TABLE hotwire.postgraduate_studentship_type_hid OWNER TO postgres;

--
-- Name: postit_owner_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW postit_owner_hid AS
    SELECT person_hid.person_id AS postit_owner_id, person_hid.person_hid AS postit_owner_hid FROM (person_hid JOIN public.mm_person_research_group USING (person_id)) WHERE (mm_person_research_group.research_group_id = 1);


ALTER TABLE hotwire.postit_owner_hid OWNER TO postgres;

--
-- Name: predecessor_task_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW predecessor_task_hid AS
    SELECT it_task.id AS predecessor_task_id, it_task.name AS predecessor_task_hid FROM public.it_task;


ALTER TABLE hotwire.predecessor_task_hid OWNER TO dev;

--
-- Name: preference_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW preference_hid AS
    SELECT "Preferences".id AS preference_id, "Preferences".hw_preference_name AS preference_hid FROM "hw_Preferences" "Preferences";


ALTER TABLE hotwire.preference_hid OWNER TO postgres;

--
-- Name: rig_member_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW rig_member_hid AS
    SELECT person.id AS rig_member_id, ((((person.surname)::text || ', '::text) || COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text)) || (COALESCE(person.known_as, person.first_names))::text) AS rig_member_hid FROM ((public.person LEFT JOIN public.title_hid USING (title_id)) LEFT JOIN public._physical_status_v2 ON ((person.id = _physical_status_v2.person_id))) WHERE ((_physical_status_v2.status_id)::text <> 'Past'::text) ORDER BY person.surname, COALESCE(person.known_as, person.first_names), COALESCE(((title_hid.title_hid)::text || ' '::text), ''::text);


ALTER TABLE hotwire.rig_member_hid OWNER TO cen1001;

--
-- Name: primary_member_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW primary_member_hid AS
    SELECT DISTINCT rig_member_hid.rig_member_id AS primary_member_id, rig_member_hid.rig_member_hid AS primary_member_hid FROM (rig_member_hid JOIN public.mm_member_research_interest_group ON ((rig_member_hid.rig_member_id = mm_member_research_interest_group.member_id)));


ALTER TABLE hotwire.primary_member_hid OWNER TO cen1001;

--
-- Name: primary_office_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW primary_office_hid AS
    SELECT room_hid.room_id AS primary_office_id, room_hid.room_hid AS primary_office_hid FROM public.room_hid ORDER BY room_hid.room_hid;


ALTER TABLE hotwire.primary_office_hid OWNER TO postgres;

--
-- Name: printer_class_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW printer_class_hid AS
    SELECT printer_class.id AS printer_class_id, printer_class.name AS printer_class_hid FROM public.printer_class ORDER BY printer_class.name;


ALTER TABLE hotwire.printer_class_hid OWNER TO postgres;

--
-- Name: reboot_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW reboot_hid AS
    SELECT reboot_hid.reboot_id, reboot_hid.reboot_hid FROM public.reboot_hid;


ALTER TABLE hotwire.reboot_hid OWNER TO postgres;

--
-- Name: recirc_or_ducted_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW recirc_or_ducted_hid AS
    SELECT recirc_or_ducted_hid.recirc_or_ducted_id, recirc_or_ducted_hid.recirc_or_ducted_hid FROM public.recirc_or_ducted_hid;


ALTER TABLE hotwire.recirc_or_ducted_hid OWNER TO postgres;

--
-- Name: research_group_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW research_group_hid AS
    SELECT research_group.id AS research_group_id, research_group.name AS research_group_hid FROM public.research_group ORDER BY research_group.name;


ALTER TABLE hotwire.research_group_hid OWNER TO postgres;

--
-- Name: research_interest_group_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW research_interest_group_hid AS
    SELECT research_interest_group.id AS research_interest_group_id, research_interest_group.name AS research_interest_group_hid FROM public.research_interest_group;


ALTER TABLE hotwire.research_interest_group_hid OWNER TO postgres;

--
-- Name: responsible_group_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW responsible_group_hid AS
    SELECT research_group_hid.research_group_id AS responsible_group_id, research_group_hid.research_group_hid AS responsible_group_hid FROM public.research_group_hid;


ALTER TABLE hotwire.responsible_group_hid OWNER TO postgres;

--
-- Name: responsible_person_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW responsible_person_hid AS
    SELECT person_hid.person_id AS responsible_person_id, person_hid.person_hid AS responsible_person_hid FROM public.person_hid;


ALTER TABLE hotwire.responsible_person_hid OWNER TO postgres;

--
-- Name: reviewer_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW reviewer_hid AS
    SELECT person_hid.person_id AS reviewer_id, person_hid.person_hid AS reviewer_hid FROM public.person_hid;


ALTER TABLE hotwire.reviewer_hid OWNER TO postgres;

--
-- Name: role_status_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW role_status_hid AS
    SELECT status.status_id AS role_status_id, status.status_id AS role_status_hid FROM public.status;


ALTER TABLE hotwire.role_status_hid OWNER TO postgres;

--
-- Name: room_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW room_hid AS
    SELECT room.id AS room_id, (((room.name)::text || COALESCE((((' ('::text || COALESCE(((building_floor_hid.building_floor_hid)::text || ', '::text), ''::text)) || (COALESCE(building_region_hid.building_region_hid, building_hid.building_hid))::text) || ')'::text), ''::text)))::character varying(48) AS room_hid FROM (((public.room LEFT JOIN public.building_floor_hid ON ((room.building_floor_id = building_floor_hid.building_floor_id))) LEFT JOIN public.building_region_hid ON ((building_region_hid.building_region_id = room.building_region_id))) LEFT JOIN public.building_hid ON ((building_hid.building_id = room.building_id))) ORDER BY (((room.name)::text || COALESCE((((' ('::text || COALESCE(((building_floor_hid.building_floor_hid)::text || ', '::text), ''::text)) || (COALESCE(building_region_hid.building_region_hid, building_hid.building_hid))::text) || ')'::text), ''::text)))::character varying(48);


ALTER TABLE hotwire.room_hid OWNER TO postgres;

--
-- Name: room_long_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW room_long_hid AS
    SELECT room.id AS room_long_id, ((room.name)::text || COALESCE((((' ('::text || COALESCE(((building_floor_hid.building_floor_hid)::text || ', '::text), ''::text)) || (COALESCE(building_region_hid.building_region_hid, building_hid.building_hid))::text) || ')'::text), ''::text)) AS room_long_hid FROM (((public.room LEFT JOIN public.building_floor_hid ON ((room.building_floor_id = building_floor_hid.building_floor_id))) LEFT JOIN public.building_region_hid ON ((building_region_hid.building_region_id = room.building_region_id))) LEFT JOIN public.building_hid ON ((building_hid.building_id = room.building_id)));


ALTER TABLE hotwire.room_long_hid OWNER TO postgres;

--
-- Name: room_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW room_type_hid AS
    SELECT room_type.id AS room_type_id, room_type.name AS room_type_hid FROM public.room_type;


ALTER TABLE hotwire.room_type_hid OWNER TO postgres;

--
-- Name: second_mentor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW second_mentor_hid AS
    SELECT person.id AS second_mentor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS second_mentor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE hotwire.second_mentor_hid OWNER TO postgres;

--
-- Name: second_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW second_supervisor_hid AS
    SELECT person.id AS second_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS second_supervisor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE hotwire.second_supervisor_hid OWNER TO postgres;

--
-- Name: sector_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW sector_hid AS
    SELECT sector.id AS sector_id, sector.name AS sector_hid FROM public.sector;


ALTER TABLE hotwire.sector_hid OWNER TO postgres;

--
-- Name: socket_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW socket_hid AS
    SELECT socket.id AS socket_id, ((patch_panel_hid.patch_panel_hid || ' - '::text) || (socket.panel_label)::text) AS socket_hid FROM (public.socket JOIN public.patch_panel_hid USING (patch_panel_id)) ORDER BY ((patch_panel_hid.patch_panel_hid || ' - '::text) || (socket.panel_label)::text);


ALTER TABLE hotwire.socket_hid OWNER TO postgres;

--
-- Name: staff_category_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW staff_category_hid AS
    SELECT staff_category.id AS staff_category_id, staff_category.category AS staff_category_hid FROM public.staff_category;


ALTER TABLE hotwire.staff_category_hid OWNER TO postgres;

--
-- Name: status_(required)_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "status_(required)_hid" AS
    SELECT user_prereg_standing_hid.user_prereg_standing_id AS "status_(required)_id", user_prereg_standing_hid.user_prereg_standing_hid AS "status_(required)_hid" FROM public.user_prereg_standing_hid;


ALTER TABLE hotwire."status_(required)_hid" OWNER TO postgres;

--
-- Name: status_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW status_hid AS
    SELECT status.status_id, status.status_id AS status_hid FROM public.status;


ALTER TABLE hotwire.status_hid OWNER TO postgres;

--
-- Name: student_name_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW student_name_hid AS
    SELECT postgraduate_studentship_hid.postgraduate_studentship_id AS student_name_id, postgraduate_studentship_hid.postgraduate_studentship_hid AS student_name_hid FROM public.postgraduate_studentship_hid;


ALTER TABLE hotwire.student_name_hid OWNER TO postgres;

--
-- Name: subnet_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW subnet_hid AS
    SELECT subnet.id AS subnet_id, (((((subnet.network_address || ' ('::text) || (subnet.notes)::text) || ', '::text) || (SELECT count(*) AS count FROM public.ip_address WHERE (((ip_address.subnet_id = subnet.id) AND (NOT ip_address.reserved)) AND ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text))))) || ' free)'::text) AS subnet_hid FROM public.subnet ORDER BY subnet.notes;


ALTER TABLE hotwire.subnet_hid OWNER TO dev;

--
-- Name: substitute_supervisor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW substitute_supervisor_hid AS
    SELECT person.id AS substitute_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS substitute_supervisor_hid FROM public.person;


ALTER TABLE hotwire.substitute_supervisor_hid OWNER TO postgres;

--
-- Name: supervisee_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW supervisee_hid AS
    SELECT person_hid.person_id AS supervisee_id, person_hid.person_hid AS supervisee_hid FROM public.person_hid;


ALTER TABLE hotwire.supervisee_hid OWNER TO postgres;

--
-- Name: supervisor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW supervisor_hid AS
    SELECT person_hid.person_id AS supervisor_id, person_hid.person_hid AS supervisor_hid FROM public.person_hid;


ALTER TABLE hotwire.supervisor_hid OWNER TO postgres;

--
-- Name: supervisor_person_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW supervisor_person_hid AS
    SELECT person.id AS supervisor_person_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS supervisor_person_hid FROM public.person;


ALTER TABLE hotwire.supervisor_person_hid OWNER TO postgres;

--
-- Name: switch_config_goal_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW switch_config_goal_hid AS
    SELECT switch_config_goal.id AS switch_config_goal_id, switch_config_goal.name AS switch_config_goal_hid FROM public.switch_config_goal ORDER BY switch_config_goal.name;


ALTER TABLE hotwire.switch_config_goal_hid OWNER TO postgres;

--
-- Name: switch_hardware_type_hid; Type: VIEW; Schema: hotwire; Owner: cen1001
--

CREATE VIEW switch_hardware_type_hid AS
    SELECT hardware_type.id AS switch_hardware_type_id, hardware_type.name AS switch_hardware_type_hid FROM public.hardware_type WHERE ((hardware_type.name)::text ~~* '%switch%'::text);


ALTER TABLE hotwire.switch_hardware_type_hid OWNER TO cen1001;

--
-- Name: switch_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW switch_hid AS
    SELECT switch.id AS switch_id, (hardware.name)::text AS switch_hid FROM (public.switch JOIN public.hardware ON ((switch.hardware_id = hardware.id)));


ALTER TABLE hotwire.switch_hid OWNER TO dev;

--
-- Name: switch_ip_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW switch_ip_hid AS
    SELECT ip_address.id AS switch_ip_id, ((((ip_address.ip)::text || ' ('::text) || COALESCE((ip_address.hostname)::text, ('--------------.'::text || (subnet.domain_name)::text))) || ')'::text) AS switch_ip_hid, ip_address.ip AS switch_ip_sort FROM (public.ip_address JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) WHERE (((subnet.domain_name)::text ~~ '%net.private.ch.cam.ac.uk'::text) AND (ip_address.reserved <> true));


ALTER TABLE hotwire.switch_ip_hid OWNER TO dev;

--
-- Name: switch_model_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW switch_model_hid AS
    SELECT switch_model.id AS switch_model_id, switch_model.description AS switch_model_hid FROM public.switch_model;


ALTER TABLE hotwire.switch_model_hid OWNER TO dev;

--
-- Name: switch_port_config_goal_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW switch_port_config_goal_hid AS
    SELECT switch_port_config_goal.id AS switch_port_config_goal_id, switch_port_config_goal.name AS switch_port_config_goal_hid FROM public.switch_port_config_goal;


ALTER TABLE hotwire.switch_port_config_goal_hid OWNER TO postgres;

--
-- Name: switch_port_goal_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW switch_port_goal_hid AS
    SELECT switch_port_config_goal.id AS switch_port_goal_id, switch_port_config_goal.name AS switch_port_goal_hid FROM public.switch_port_config_goal WHERE (switch_port_config_goal.goal_class IS NULL);


ALTER TABLE hotwire.switch_port_goal_hid OWNER TO postgres;

--
-- Name: switchport_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW switchport_hid AS
    SELECT switchport.id AS switchport_id, (((hardware.name)::text || ' '::text) || (switchport.name)::text) AS switchport_hid FROM ((public.switchport JOIN public.switch ON ((switchport.switch_id = switch.id))) JOIN public.hardware ON ((switch.hardware_id = hardware.id)));


ALTER TABLE hotwire.switchport_hid OWNER TO postgres;

--
-- Name: system_image_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW system_image_hid AS
    SELECT system_image.id AS system_image_id, ((((COALESCE(COALESCE(system_image.friendly_name, (first_ip.hostname)::character varying), ((COALESCE(((hardware.name)::text || ' '::text), ' '::text) || COALESCE((hardware.manufacturer)::text, ' '::text)))::character varying))::text || ' ('::text) || (operating_system.os)::text) || ')'::text) AS system_image_hid FROM (((public.system_image JOIN public.hardware ON ((system_image.hardware_id = hardware.id))) JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id))) LEFT JOIN (SELECT si.id, min((ip.hostname)::text) AS hostname FROM ((public.system_image si JOIN public.mm_system_image_ip_address mm ON ((si.id = mm.system_image_id))) JOIN public.ip_address ip ON ((ip.id = mm.ip_address_id))) GROUP BY si.id) first_ip ON ((system_image.id = first_ip.id))) GROUP BY system_image.id, system_image.friendly_name, first_ip.hostname, hardware.name, hardware.manufacturer, operating_system.os;


ALTER TABLE hotwire.system_image_hid OWNER TO dev;

--
-- Name: system_image_host_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW system_image_host_hid AS
    SELECT system_image.id AS system_image_host_id, ip_address.id AS ip_id, ip_address.hostname FROM ((public.system_image JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = system_image.id))) JOIN public.ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (ip_address.id = (SELECT ip_address.id FROM ((public.system_image s2 JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = s2.id))) JOIN public.ip_address ON ((ip_address.id = mm_system_image_ip_address.ip_address_id))) WHERE (s2.id = system_image.id) LIMIT 1));


ALTER TABLE hotwire.system_image_host_hid OWNER TO postgres;

--
-- Name: time_estimate_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW time_estimate_hid AS
    SELECT it_task_time_estimate.id AS time_estimate_id, it_task_time_estimate.name AS time_estimate_hid FROM public.it_task_time_estimate;


ALTER TABLE hotwire.time_estimate_hid OWNER TO dev;

--
-- Name: title_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW title_hid AS
    SELECT title_hid.title_id, title_hid.title_hid FROM public.title_hid;


ALTER TABLE hotwire.title_hid OWNER TO postgres;

--
-- Name: university_term_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW university_term_hid AS
    SELECT university_term_hid.university_term_id, university_term_hid.university_term_hid FROM public.university_term_hid;


ALTER TABLE hotwire.university_term_hid OWNER TO postgres;

--
-- Name: updated_software_package_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW updated_software_package_hid AS
    SELECT software_package_hid.software_package_id AS updated_software_package_id, software_package_hid.software_package_hid AS updated_software_package_hid FROM software_package_hid;


ALTER TABLE hotwire.updated_software_package_hid OWNER TO postgres;

--
-- Name: upgraded_licence_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW upgraded_licence_hid AS
    SELECT licence_hid.licence_id AS upgraded_licence_id, licence_hid.licence_hid AS upgraded_licence_hid FROM licence_hid;


ALTER TABLE hotwire.upgraded_licence_hid OWNER TO postgres;

--
-- Name: upgrading_licence_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW upgrading_licence_hid AS
    SELECT licence_hid.licence_id AS upgrading_licence_id, licence_hid.licence_hid AS upgrading_licence_hid FROM licence_hid;


ALTER TABLE hotwire.upgrading_licence_hid OWNER TO postgres;

--
-- Name: user_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW user_hid AS
    SELECT person.id AS user_id, (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text) AS user_hid FROM public.person ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER TABLE hotwire.user_hid OWNER TO postgres;

--
-- Name: user_prereg_standing_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW user_prereg_standing_hid AS
    SELECT user_prereg_standing_hid.user_prereg_standing_id, user_prereg_standing_hid.user_prereg_standing_hid FROM public.user_prereg_standing_hid;


ALTER TABLE hotwire.user_prereg_standing_hid OWNER TO postgres;

--
-- Name: user_prereg_title_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW user_prereg_title_hid AS
    SELECT user_prereg_title_hid.user_prereg_title_id, user_prereg_title_hid.user_prereg_title_hid FROM public.user_prereg_title_hid;


ALTER TABLE hotwire.user_prereg_title_hid OWNER TO postgres;

--
-- Name: usual_mentor_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW usual_mentor_hid AS
    SELECT person_hid.person_id AS usual_mentor_id, person_hid.person_hid AS usual_mentor_hid FROM public.person_hid;


ALTER TABLE hotwire.usual_mentor_hid OWNER TO postgres;

--
-- Name: usual_reviewer_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW usual_reviewer_hid AS
    SELECT person_hid.person_id AS usual_reviewer_id, person_hid.person_hid AS usual_reviewer_hid FROM public.person_hid;


ALTER TABLE hotwire.usual_reviewer_hid OWNER TO postgres;

--
-- Name: virus_detection_strategy_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW virus_detection_strategy_hid AS
    SELECT virus_detection_strategy.id AS virus_detection_strategy_id, virus_detection_strategy.name AS virus_detection_strategy_hid FROM public.virus_detection_strategy;


ALTER TABLE hotwire.virus_detection_strategy_hid OWNER TO postgres;

--
-- Name: visitor_type_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW visitor_type_hid AS
    SELECT visitor_type_hid.visitor_type_id, visitor_type_hid.visitor_type_hid FROM public.visitor_type_hid;


ALTER TABLE hotwire.visitor_type_hid OWNER TO postgres;

--
-- Name: vlan_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW vlan_hid AS
    SELECT vlan.id AS vlan_id, (vlan.vid || COALESCE(((' ('::text || (vlan.name)::text) || ')'::text), ''::text)) AS vlan_hid FROM public.vlan;


ALTER TABLE hotwire.vlan_hid OWNER TO dev;

--
-- Name: vlan_vid_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW vlan_vid_hid AS
    SELECT vlan.vid AS vlan_vid_id, ((((vlan.name)::text || ' ['::text) || (vlan.description)::text) || ']'::text) AS vlan_vid_hid FROM public.vlan;


ALTER TABLE hotwire.vlan_vid_hid OWNER TO postgres;

--
-- Name: website_staff_category_hid; Type: VIEW; Schema: hotwire; Owner: dev
--

CREATE VIEW website_staff_category_hid AS
    SELECT website_staff_category_hid.website_staff_category_id, website_staff_category_hid.website_staff_category_hid FROM public.website_staff_category_hid;


ALTER TABLE hotwire.website_staff_category_hid OWNER TO dev;

--
-- Name: www_person_notitles_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW www_person_notitles_hid AS
    SELECT person.id AS person_id, ((((COALESCE(person.known_as, person.first_names))::text || ' '::text) || (person.surname)::text) || COALESCE((' '::text || (person.name_suffix)::text), ''::text)) AS www_person_hid FROM (public.person LEFT JOIN public.title_hid USING (title_id));


ALTER TABLE hotwire.www_person_notitles_hid OWNER TO postgres;

--
-- Name: www_room_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW www_room_hid AS
    SELECT room.id AS room_id, ((COALESCE(((COALESCE(((building_hid.building_hid)::text || ' '::text), ''::text) || COALESCE(((building_region_hid.building_region_hid)::text || ' '::text), ''::text)) || COALESCE(((building_floor_hid.building_floor_hid)::text || ' '::text), ''::text))) || 'Room '::text) || (room.name)::text) AS room_hid FROM (((public.room LEFT JOIN public.building_hid USING (building_id)) LEFT JOIN public.building_region_hid USING (building_region_id)) LEFT JOIN public.building_floor_hid USING (building_floor_id));


ALTER TABLE hotwire.www_room_hid OWNER TO postgres;

--
-- Name: Preferences_pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "hw_Preferences"
    ADD CONSTRAINT "Preferences_pkey" PRIMARY KEY (id);


--
-- Name: hw_User Preferences.pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "hw_User Preferences"
    ADD CONSTRAINT "hw_User Preferences.pkey" PRIMARY KEY (id);


--
-- Name: hw_preference_type_pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);


--
-- Name: pk_hw_subviews; Type: CONSTRAINT; Schema: hotwire; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY _subviews
    ADD CONSTRAINT pk_hw_subviews PRIMARY KEY (view, subview);


--
-- Name: del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE del AS ON DELETE TO "10_View/Network/Switch Config Fragments" DO INSTEAD DELETE FROM public.switch_config_fragment WHERE (switch_config_fragment.id = old.id);


--
-- Name: fire_warden_area_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_area_del AS ON DELETE TO "10_View/Safety/Fire_warden_areas" DO INSTEAD DELETE FROM public.fire_warden_area WHERE (fire_warden_area.fire_warden_area_id = old.id);


--
-- Name: fire_warden_area_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_area_ins AS ON INSERT TO "10_View/Safety/Fire_warden_areas" DO INSTEAD SELECT fn_fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;


--
-- Name: fire_warden_area_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_area_upd AS ON UPDATE TO "10_View/Safety/Fire_warden_areas" DO INSTEAD SELECT fn_fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;


--
-- Name: fire_warden_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_del AS ON DELETE TO "10_View/Safety/Fire_wardens" DO INSTEAD DELETE FROM public.fire_warden WHERE (fire_warden.fire_warden_id = old.id);


--
-- Name: fire_warden_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_ins AS ON INSERT TO "10_View/Safety/Fire_wardens" DO INSTEAD SELECT fn_fire_warden_upd(new.*) AS fn_fire_warden_upd;


--
-- Name: fire_warden_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE fire_warden_upd AS ON UPDATE TO "10_View/Safety/Fire_wardens" DO INSTEAD SELECT fn_fire_warden_upd(new.*) AS fn_fire_warden_upd;


--
-- Name: firstaiders_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE firstaiders_del AS ON DELETE TO "10_View/Safety/Firstaiders" DO INSTEAD DELETE FROM public.firstaider WHERE (firstaider.id = old.id);


--
-- Name: firstaiders_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE firstaiders_ins AS ON INSERT TO "10_View/Safety/Firstaiders" DO INSTEAD INSERT INTO public.firstaider (person_id, firstaider_funding_id, qualification_date, requalify_date, hf_cn_trained, defibrillator_trained) VALUES (new.person_id, new.firstaider_funding_id, new.qualification_date, new.requalify_date, new.hf_cn_trained, new.defibrillator_trained) RETURNING firstaider.id, NULL::bigint AS int8, firstaider.person_id, firstaider.firstaider_funding_id, firstaider.qualification_date, firstaider.requalify_date, firstaider.hf_cn_trained, firstaider.defibrillator_trained, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::text AS text, NULL::character varying(20) AS "varchar", NULL::boolean AS bool, NULL::character varying(80) AS "varchar", NULL::text AS text;


--
-- Name: firstaiders_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE firstaiders_upd AS ON UPDATE TO "10_View/Safety/Firstaiders" DO INSTEAD UPDATE public.firstaider SET person_id = new.person_id, firstaider_funding_id = new.firstaider_funding_id, qualification_date = new.qualification_date, requalify_date = new.requalify_date, hf_cn_trained = new.hf_cn_trained, defibrillator_trained = new.defibrillator_trained WHERE (firstaider.id = old.id);


--
-- Name: group_computers_view_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE group_computers_view_del AS ON DELETE TO "10_View/Group_Computers" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: group_computers_view_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE group_computers_view_del AS ON DELETE TO "10_View/Group_Computers_Autoinstaller" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: group_computers_view_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE group_computers_view_upd AS ON UPDATE TO "10_View/Group_Computers" DO INSTEAD (UPDATE public.hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET user_id = new.user_id, operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments WHERE (system_image.id = old.id); );


--
-- Name: group_computers_view_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE group_computers_view_upd AS ON UPDATE TO "10_View/Group_Computers_Autoinstaller" DO INSTEAD (UPDATE public.hardware SET manufacturer = new.manufacturer, model = new.model, name = new.hardware_name, hardware_type_id = new.hardware_type_id, asset_tag = new.asset_tag, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET user_id = new.user_id, operating_system_id = new.operating_system_id, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments WHERE (system_image.id = old.id); SELECT public.fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hotwire_10_Network/Fibres/15_Fibre_Core_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Network/Fibres/15_Fibre_Core_del" AS ON DELETE TO "10_View/Network/Fibres/15_Fibre_Core" DO INSTEAD DELETE FROM public.fibre_core WHERE (fibre_core.id = old.id);


--
-- Name: hotwire_10_Network/Fibres/15_Fibre_Core_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Network/Fibres/15_Fibre_Core_ins" AS ON INSERT TO "10_View/Network/Fibres/15_Fibre_Core" DO INSTEAD INSERT INTO public.fibre_core (fibre_run_id, fibre_core_type_id, label_a, label_b, illuminate) VALUES (new.fibre_run_id, new.fibre_core_type_id, new.label_a, new.label_b, new.illuminate) RETURNING fibre_core.id, fibre_core.fibre_run_id, fibre_core.fibre_core_type_id, fibre_core.label_a, fibre_core.label_b, fibre_core.illuminate, public._to_hwsubviewb('10_View/Network/Fibres/Tests'::character varying, 'fibre_core_id'::character varying, '10_View/Network/Fibres/Tests'::character varying, NULL::character varying, NULL::character varying) AS "Tests";


--
-- Name: hotwire_10_Network/Fibres/15_Fibre_Core_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Network/Fibres/15_Fibre_Core_upd" AS ON UPDATE TO "10_View/Network/Fibres/15_Fibre_Core" DO INSTEAD UPDATE public.fibre_core SET id = new.id, fibre_run_id = new.fibre_run_id, fibre_core_type_id = new.fibre_core_type_id, label_a = new.label_a, label_b = new.label_b, illuminate = new.illuminate WHERE (fibre_core.id = old.id);


--
-- Name: hotwire_10_View/10_IT_Tasks/IT_Projects_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/10_IT_Tasks/IT_Projects_del" AS ON DELETE TO "10_View/10_IT_Tasks/IT_Projects" DO INSTEAD DELETE FROM public.it_project WHERE (it_project.id = old.id);


--
-- Name: hotwire_10_View/10_IT_Tasks/IT_Projects_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/10_IT_Tasks/IT_Projects_ins" AS ON INSERT TO "10_View/10_IT_Tasks/IT_Projects" DO INSTEAD SELECT public.hw_fn_it_projects_upd(new.*) AS id;


--
-- Name: hotwire_10_View/10_IT_Tasks/IT_Projects_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/10_IT_Tasks/IT_Projects_upd" AS ON UPDATE TO "10_View/10_IT_Tasks/IT_Projects" DO INSTEAD SELECT public.hw_fn_it_projects_upd(new.*) AS id;


--
-- Name: hotwire_10_View/10_IT_Tasks/Tasks_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/10_IT_Tasks/Tasks_del" AS ON DELETE TO "10_View/10_IT_Tasks/Tasks" DO INSTEAD UPDATE public.it_task SET deleted = now() WHERE (it_task.id = old.id);


--
-- Name: hotwire_10_View/Hobbit/Clients_Cfg_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Hobbit/Clients_Cfg_del" AS ON DELETE TO "10_View/Hobbit/Clients_Cfg" DO INSTEAD DELETE FROM public.hobbit_clients_config_stanza WHERE (hobbit_clients_config_stanza.id = old.id);


--
-- Name: hotwire_10_View/Hobbit/Clients_Cfg_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Hobbit/Clients_Cfg_ins" AS ON INSERT TO "10_View/Hobbit/Clients_Cfg" DO INSTEAD INSERT INTO public.hobbit_clients_config_stanza (rules, settings, handle, system_image_id) VALUES (new.rules, new.settings, new.handle, new.system_image_id) RETURNING hobbit_clients_config_stanza.id, hobbit_clients_config_stanza.system_image_id, hobbit_clients_config_stanza.handle, hobbit_clients_config_stanza.rules, hobbit_clients_config_stanza.settings;


--
-- Name: hotwire_10_View/Hobbit/Clients_Cfg_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Hobbit/Clients_Cfg_upd" AS ON UPDATE TO "10_View/Hobbit/Clients_Cfg" DO INSTEAD UPDATE public.hobbit_clients_config_stanza SET id = new.id, rules = new.rules, settings = new.settings, handle = new.handle, system_image_id = new.system_image_id WHERE (hobbit_clients_config_stanza.id = old.id);


--
-- Name: hotwire_10_View/IT_Sales/Sales_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/IT_Sales/Sales_del" AS ON DELETE TO "10_View/IT_Sales/Sales" DO INSTEAD DELETE FROM public.itsales_item WHERE (itsales_item.id = old.id);


--
-- Name: hotwire_10_View/IT_Sales/Sales_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/IT_Sales/Sales_ins" AS ON INSERT TO "10_View/IT_Sales/Sales" DO INSTEAD INSERT INTO public.itsales_item (itsales_type_id, research_group_id, person_id, quantity, itsales_unit_id, delivery_date, expires_date, notes, itsales_status_id, unit_cost, charge_account) VALUES (new.itsales_type_id, new.research_group_id, new.person_id, new.quantity, new.itsales_unit_id, new.delivery_date, new.expires_date, new.notes, new.itsales_status_id, new.unit_cost, new.charge_account) RETURNING itsales_item.id, itsales_item.itsales_type_id, itsales_item.research_group_id, itsales_item.person_id, itsales_item.quantity, itsales_item.itsales_unit_id, itsales_item.delivery_date, itsales_item.expires_date, itsales_item.notes, itsales_item.itsales_status_id, itsales_item.unit_cost, itsales_item.charge_account;


--
-- Name: hotwire_10_View/IT_Sales/Sales_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/IT_Sales/Sales_upd" AS ON UPDATE TO "10_View/IT_Sales/Sales" DO INSTEAD UPDATE public.itsales_item SET id = new.id, itsales_type_id = new.itsales_type_id, research_group_id = new.research_group_id, person_id = new.person_id, quantity = new.quantity, itsales_unit_id = new.itsales_unit_id, delivery_date = new.delivery_date, expires_date = new.expires_date, notes = new.notes, itsales_status_id = new.itsales_status_id, unit_cost = new.unit_cost, charge_account = new.charge_account WHERE (itsales_item.id = old.id);


--
-- Name: hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE "hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_del" AS ON DELETE TO "10_View/Mailing_Lists/Chemistry_Mail_Domain" DO INSTEAD DELETE FROM public.mail_alias WHERE (mail_alias.mail_alias_id = old.id);


--
-- Name: hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE "hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_ins" AS ON INSERT TO "10_View/Mailing_Lists/Chemistry_Mail_Domain" DO INSTEAD INSERT INTO public.mail_alias (comment, alias, addresses) VALUES (new.comment, new.alias, new.addresses) RETURNING mail_alias.mail_alias_id, mail_alias.comment, mail_alias.alias, mail_alias.addresses;


--
-- Name: hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE "hotwire_10_View/Mailing_Lists/Chemistry_Mail_Domain_upd" AS ON UPDATE TO "10_View/Mailing_Lists/Chemistry_Mail_Domain" DO INSTEAD UPDATE public.mail_alias SET mail_alias_id = new.id, comment = new.comment, alias = new.alias, addresses = new.addresses WHERE (mail_alias.mail_alias_id = old.id);


--
-- Name: hotwire_10_View/Network/Cabinet_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Cabinet_del" AS ON DELETE TO "10_View/Network/Cabinet" DO INSTEAD DELETE FROM public.cabinet WHERE (cabinet.id = old.id);


--
-- Name: hotwire_10_View/Network/Cabinet_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Cabinet_ins" AS ON INSERT TO "10_View/Network/Cabinet" DO INSTEAD INSERT INTO public.cabinet (name, email_local_part, notes, room_id, cabno, in_use, depth, fibre_u, patch_u, pots_u, other_u, height_u, ports, max_ports, sw48, sw24, cabname) VALUES (new.name, new.email_local_part, new.notes, new.room_id, new.cabno, new.in_use, new.depth, new.fibre_u, new.patch_u, new.pots_u, new.other_u, new.height_u, new.ports, new.max_ports, new.sw48, new.sw24, new.cabname) RETURNING cabinet.id, cabinet.name, cabinet.email_local_part, cabinet.notes, cabinet.room_id, cabinet.cabno, cabinet.in_use, cabinet.depth, cabinet.fibre_u, cabinet.patch_u, cabinet.pots_u, cabinet.other_u, cabinet.height_u, cabinet.ports, cabinet.max_ports, cabinet.sw48, cabinet.sw24, cabinet.cabname, public._to_hwsubviewb('10_View/Network/Fibre_Panel'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibre_Panel'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Panel", public._to_hwsubviewb('10_View/Network/Fibres/_FibreRuns_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/10_Fibre_runs'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Runs", public._to_hwsubviewb('10_View/Network/Fibres/_FibreCore_for_Cabinet_ro'::character varying, 'cabinet_id'::character varying, '10_View/Network/Fibres/15_Fibre_Core'::character varying, NULL::character varying, NULL::character varying) AS "Fibre Cores";


--
-- Name: hotwire_10_View/Network/Cabinet_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Cabinet_upd" AS ON UPDATE TO "10_View/Network/Cabinet" DO INSTEAD UPDATE public.cabinet SET id = new.id, name = new.name, email_local_part = new.email_local_part, notes = new.notes, room_id = new.room_id, cabno = new.cabno, in_use = new.in_use, depth = new.depth, fibre_u = new.fibre_u, pots_u = new.pots_u, other_u = new.other_u, height_u = new.height_u, ports = new.ports, max_ports = new.max_ports, sw48 = new.sw48, sw24 = new.sw24, patch_u = new.patch_u, cabname = new.cabname WHERE (cabinet.id = old.id);


--
-- Name: hotwire_10_View/Network/DB_Admin/Fibre Core Type_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/DB_Admin/Fibre Core Type_del" AS ON DELETE TO "10_View/Network/DB_Admin/Fibre Core Type" DO INSTEAD DELETE FROM public.fibre_core_type WHERE (fibre_core_type.id = old.id);


--
-- Name: hotwire_10_View/Network/DB_Admin/Fibre Core Type_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/DB_Admin/Fibre Core Type_ins" AS ON INSERT TO "10_View/Network/DB_Admin/Fibre Core Type" DO INSTEAD INSERT INTO public.fibre_core_type (name, fibre_type_id) VALUES (new.name, new.fibre_type_id) RETURNING fibre_core_type.id, fibre_core_type.name, fibre_core_type.fibre_type_id;


--
-- Name: hotwire_10_View/Network/DB_Admin/Fibre Core Type_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/DB_Admin/Fibre Core Type_upd" AS ON UPDATE TO "10_View/Network/DB_Admin/Fibre Core Type" DO INSTEAD UPDATE public.fibre_core_type SET id = new.id, name = new.name, fibre_type_id = new.fibre_type_id WHERE (fibre_core_type.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibre/Tests_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre/Tests_del" AS ON DELETE TO "10_View/Network/Fibres/Tests" DO INSTEAD DELETE FROM public.fibre_test WHERE (fibre_test.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibre/Tests_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre/Tests_ins" AS ON INSERT TO "10_View/Network/Fibres/Tests" DO INSTEAD INSERT INTO public.fibre_test (fibre_core_id, fibre_test_time, fibre_test_type_id, fibre_test_result, passed) VALUES (new.fibre_core_id, new.fibre_test_time, new.fibre_test_type_id, new.fibre_test_result, new.passed) RETURNING fibre_test.fibre_core_id, fibre_test.fibre_test_time, fibre_test.fibre_test_type_id, fibre_test.fibre_test_result, fibre_test.passed, fibre_test.id;


--
-- Name: hotwire_10_View/Network/Fibre/Tests_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre/Tests_upd" AS ON UPDATE TO "10_View/Network/Fibres/Tests" DO INSTEAD UPDATE public.fibre_test SET fibre_core_id = new.fibre_core_id, fibre_test_time = new.fibre_test_time, fibre_test_type_id = new.fibre_test_type_id, fibre_test_result = new.fibre_test_result, passed = new.passed, id = new.id WHERE (fibre_test.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibre_Panel_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre_Panel_del" AS ON DELETE TO "10_View/Network/Fibre_Panel" DO INSTEAD DELETE FROM public.fibre_panel WHERE (fibre_panel.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibre_Panel_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre_Panel_ins" AS ON INSERT TO "10_View/Network/Fibre_Panel" DO INSTEAD INSERT INTO public.fibre_panel (cabinet_id, name, termination_type_id) VALUES (new.cabinet_id, new.name, new.fibre_termination_id) RETURNING fibre_panel.id, fibre_panel.cabinet_id, fibre_panel.name, fibre_panel.termination_type_id;


--
-- Name: hotwire_10_View/Network/Fibre_Panel_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Fibre_Panel_upd" AS ON UPDATE TO "10_View/Network/Fibre_Panel" DO INSTEAD UPDATE public.fibre_panel SET id = new.id, cabinet_id = new.cabinet_id, name = new.name, termination_type_id = new.fibre_termination_id WHERE (fibre_panel.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibres/10_Fibre_run_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Fibres/10_Fibre_run_del" AS ON DELETE TO "10_View/Network/Fibres/10_Fibre_run" DO INSTEAD DELETE FROM public.fibre_run WHERE (fibre_run.id = old.id);


--
-- Name: hotwire_10_View/Network/Fibres/10_Fibre_run_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Fibres/10_Fibre_run_ins" AS ON INSERT TO "10_View/Network/Fibres/10_Fibre_run" DO INSTEAD INSERT INTO public.fibre_run (fibre_type, term_a_id, term_b_id, quantity, panel_a_id, panel_b_id, notes) VALUES (new.fibre_type, new.fibre_termination_a_id, new.fibre_termination_b_id, new.quantity, new.fibre_panel_a_id, new.fibre_panel_b_id, new.notes);


--
-- Name: hotwire_10_View/Network/Fibres/10_Fibre_run_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Fibres/10_Fibre_run_upd" AS ON UPDATE TO "10_View/Network/Fibres/10_Fibre_run" DO INSTEAD UPDATE public.fibre_run SET fibre_type = new.fibre_type, term_a_id = new.fibre_termination_a_id, term_b_id = new.fibre_termination_b_id, quantity = new.quantity, panel_a_id = new.fibre_panel_a_id, panel_b_id = new.fibre_panel_b_id, notes = new.notes WHERE (fibre_run.id = old.id);


--
-- Name: hotwire_10_View/Network/GBIC_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/GBIC_del" AS ON DELETE TO "10_View/Network/GBIC" DO INSTEAD DELETE FROM public.network_gbic WHERE (network_gbic.id = old.id);


--
-- Name: hotwire_10_View/Network/GBIC_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/GBIC_ins" AS ON INSERT TO "10_View/Network/GBIC" DO INSTEAD INSERT INTO public.network_gbic (name, description, network_electrical_connection_id, fibre_type_id) VALUES (new.name, new.description, new.network_electrical_connection_id, new.fibre_type_id) RETURNING network_gbic.id, network_gbic.name, network_gbic.description, network_gbic.network_electrical_connection_id, network_gbic.fibre_type_id;


--
-- Name: hotwire_10_View/Network/GBIC_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/GBIC_upd" AS ON UPDATE TO "10_View/Network/GBIC" DO INSTEAD UPDATE public.network_gbic SET id = new.id, name = new.name, description = new.description, network_electrical_connection_id = new.network_electrical_connection_id, fibre_type_id = new.fibre_type_id WHERE (network_gbic.id = old.id);


--
-- Name: hotwire_10_View/Network/MAC_to_VLAN2_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/MAC_to_VLAN2_del" AS ON DELETE TO "10_View/Network/MAC_to_VLAN" DO INSTEAD DELETE FROM public.shadow_mac_to_vlan WHERE (shadow_mac_to_vlan.id = old.id);


--
-- Name: hotwire_10_View/Network/MAC_to_VLAN2_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/MAC_to_VLAN2_ins" AS ON INSERT TO "10_View/Network/MAC_to_VLAN" DO INSTEAD INSERT INTO public.shadow_mac_to_vlan (mac, vid) VALUES (new.mac, new.vlan_vid_id) RETURNING shadow_mac_to_vlan.id, shadow_mac_to_vlan.mac, shadow_mac_to_vlan.vid, NULL::integer AS int4, NULL::integer AS int4, NULL::character varying AS "varchar", NULL::character varying AS "varchar", NULL::public._hwsubviewb AS _hwsubviewb;


--
-- Name: hotwire_10_View/Network/MAC_to_VLAN2_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/MAC_to_VLAN2_upd" AS ON UPDATE TO "10_View/Network/MAC_to_VLAN" DO INSTEAD (INSERT INTO public.network_vlan_change (shadow_mac_to_vlan_id, mac, infraction_type_id, ticket, notes, old_vid, new_vid) VALUES (old.id, new.mac, new.infraction_type_id, new.ticket, new."Infraction notes", old.vlan_vid_id, new.vlan_vid_id); UPDATE public.shadow_mac_to_vlan SET id = new.id, mac = new.mac, vid = new.vlan_vid_id WHERE (shadow_mac_to_vlan.id = old.id); );


--
-- Name: hotwire_10_View/Network/Switch Models_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Switch Models_del" AS ON DELETE TO "10_View/Network/Switch Models" DO INSTEAD DELETE FROM public.switch_model WHERE (switch_model.id = old.id);


--
-- Name: hotwire_10_View/Network/Switch Models_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Switch Models_ins" AS ON INSERT TO "10_View/Network/Switch Models" DO INSTEAD INSERT INTO public.switch_model (model, description, ports, uplink_ports) VALUES (new.model, new.description, new.ports, new.uplink_ports) RETURNING switch_model.id, switch_model.model, switch_model.description, switch_model.ports, switch_model.uplink_ports;


--
-- Name: hotwire_10_View/Network/Switch Models_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_View/Network/Switch Models_upd" AS ON UPDATE TO "10_View/Network/Switch Models" DO INSTEAD UPDATE public.switch_model SET id = new.id, model = new.model, description = new.description, ports = new.ports, uplink_ports = new.uplink_ports WHERE (switch_model.id = old.id);


--
-- Name: hotwire_10_View/Network/Switch_Port_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Switch_Port_del" AS ON DELETE TO "10_View/Network/Switch_Port" DO INSTEAD DELETE FROM public.switchport WHERE (switchport.id = old.id);


--
-- Name: hotwire_10_View/Network/Switch_Port_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Switch_Port_ins" AS ON INSERT TO "10_View/Network/Switch_Port" DO INSTEAD INSERT INTO public.switchport (name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id) VALUES (new.name, new.socket_id, new.switch_id, new.ifacename, new.disabled, new.speed_id, new.switch_auth_id) RETURNING switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY(SELECT mm_switch_config_switch_port.switch_port_config_goal_id FROM public.mm_switch_config_switch_port WHERE (mm_switch_config_switch_port.switch_port_id = switchport.id)) AS switch_port_goal_id;


--
-- Name: hotwire_10_View/Network/Switch_Port_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Network/Switch_Port_upd" AS ON UPDATE TO "10_View/Network/Switch_Port" DO INSTEAD (UPDATE public.switchport SET id = new.id, name = new.name, socket_id = new.socket_id, switch_id = new.switch_id, ifacename = new.ifacename, disabled = new.disabled, speed_id = new.speed_id, switch_auth_id = new.switch_auth_id WHERE (switchport.id = old.id); SELECT public.fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2; );


--
-- Name: hotwire_10_View/People/Photography_Registration_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/People/Photography_Registration_ins" AS ON INSERT TO "10_View/People/Photography_Registration" DO INSTEAD SELECT public.hw_fn_personnel_photography_reg_upd(new.*) AS id;


--
-- Name: hotwire_10_View/People/Photography_Registration_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/People/Photography_Registration_upd" AS ON UPDATE TO "10_View/People/Photography_Registration" DO INSTEAD SELECT public.hw_fn_personnel_photography_reg_upd(new.*) AS id;


--
-- Name: hotwire_10_View/Systems/ChemNet_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Systems/ChemNet_ins" AS ON INSERT TO "10_View/Systems/ChemNet" DO INSTEAD INSERT INTO public.machine_account (name, chemnet_token, system_image_id) VALUES (new.token_name, (SELECT public.random_string_lower(16) AS random_string_lower), new.system_image_id) RETURNING machine_account.id, machine_account.system_image_id, machine_account.name, machine_account.chemnet_token;


--
-- Name: hotwire_10_View/Systems/ChemNet_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_View/Systems/ChemNet_upd" AS ON UPDATE TO "10_View/Systems/ChemNet" DO INSTEAD UPDATE public.machine_account SET name = new.token_name, chemnet_token = new.chemnet_token, system_image_id = new.system_image_id WHERE (machine_account.id = old.id);


--
-- Name: hotwire_10_Views/10_IT_Tasks/Strategic Goals_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_Views/10_IT_Tasks/Strategic Goals_del" AS ON DELETE TO "10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD DELETE FROM public.it_strategic_goal WHERE (it_strategic_goal.id = old.id);


--
-- Name: hotwire_10_Views/10_IT_Tasks/Strategic Goals_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_Views/10_IT_Tasks/Strategic Goals_ins" AS ON INSERT TO "10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD INSERT INTO public.it_strategic_goal (name) VALUES (new.name) RETURNING it_strategic_goal.id, it_strategic_goal.name;


--
-- Name: hotwire_10_Views/10_IT_Tasks/Strategic Goals_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE "hotwire_10_Views/10_IT_Tasks/Strategic Goals_upd" AS ON UPDATE TO "10_View/10_IT_Tasks/Strategic Goals" DO INSTEAD UPDATE public.it_strategic_goal SET id = new.id, name = new.name WHERE (it_strategic_goal.id = old.id);


--
-- Name: hotwire_10_view_10_it_tasks_tasks_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_10_view_10_it_tasks_tasks_ins AS ON INSERT TO "10_View/10_IT_Tasks/Tasks" DO INSTEAD SELECT public.hw_fn_it_tasks_upd(new.*) AS id;


--
-- Name: hotwire_10_view_10_it_tasks_tasks_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_10_view_10_it_tasks_tasks_upd AS ON UPDATE TO "10_View/10_IT_Tasks/Tasks" DO INSTEAD SELECT public.hw_fn_it_tasks_upd(new.*) AS id;


--
-- Name: hotwire_view_building_floors_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_building_floors_ins AS ON INSERT TO "10_View/Building_Floors" DO INSTEAD INSERT INTO public.building_floor_hid (building_floor_hid) VALUES (new.building_floor) RETURNING building_floor_hid.building_floor_id, building_floor_hid.building_floor_hid;


--
-- Name: hotwire_view_building_floors_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_building_floors_upd AS ON UPDATE TO "10_View/Building_Floors" DO INSTEAD UPDATE public.building_floor_hid SET building_floor_hid = new.building_floor WHERE (building_floor_hid.building_floor_id = old.id);


--
-- Name: hotwire_view_building_regions_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_building_regions_ins AS ON INSERT TO "10_View/Building_Regions" DO INSTEAD INSERT INTO public.building_region_hid (building_region_hid) VALUES (new.building_region) RETURNING building_region_hid.building_region_id, building_region_hid.building_region_hid;


--
-- Name: hotwire_view_building_regions_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_building_regions_upd AS ON UPDATE TO "10_View/Building_Regions" DO INSTEAD UPDATE public.building_region_hid SET building_region_hid = new.building_region WHERE (building_region_hid.building_region_id = old.id);


--
-- Name: hotwire_view_buildings_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_buildings_ins AS ON INSERT TO "10_View/Buildings" DO INSTEAD INSERT INTO public.building_hid (building_hid) VALUES (new.building);


--
-- Name: hotwire_view_buildings_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_buildings_upd AS ON UPDATE TO "10_View/Buildings" DO INSTEAD UPDATE public.building_hid SET building_hid = new.building WHERE (building_hid.building_id = old.id);


--
-- Name: hotwire_view_cpgs_submission_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_cpgs_submission_upd AS ON UPDATE TO "10_View/CPGS_Submission" DO INSTEAD UPDATE public.postgraduate_studentship SET person_id = new.person_id, postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_submitted = new.cpgs_or_mphil_date_submitted, cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded, mphil_date_submission_due = new.mphil_date_submission_due, mphil_date_submitted = new.mphil_date_submitted, mphil_date_awarded = new.mphil_date_awarded, first_supervisor_id = new.supervisor_id WHERE (postgraduate_studentship.id = old.id);


--
-- Name: hotwire_view_dns_anames_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_anames_del AS ON DELETE TO "10_View/DNS/ANames" DO INSTEAD DELETE FROM public.dns_anames WHERE (dns_anames.id = old.id);


--
-- Name: hotwire_view_dns_anames_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_anames_ins AS ON INSERT TO "10_View/DNS/ANames" DO INSTEAD INSERT INTO public.dns_anames (name, dns_domain_id, ip_address_id) VALUES (new.name, new.dns_domain_id, new.ip_address_id) RETURNING dns_anames.id, dns_anames.name, dns_anames.dns_domain_id, dns_anames.ip_address_id;


--
-- Name: hotwire_view_dns_anames_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_anames_upd AS ON UPDATE TO "10_View/DNS/ANames" DO INSTEAD UPDATE public.dns_anames SET name = new.name, dns_domain_id = new.dns_domain_id, ip_address_id = new.ip_address_id WHERE (dns_anames.id = old.id);


--
-- Name: hotwire_view_dns_cnames_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_dns_cnames_del AS ON DELETE TO "10_View/DNS/CNames" DO INSTEAD DELETE FROM public.dns_cnames WHERE (dns_cnames.id = old.id);


--
-- Name: hotwire_view_dns_cnames_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_dns_cnames_ins AS ON INSERT TO "10_View/DNS/CNames" DO INSTEAD INSERT INTO public.dns_cnames (name, dns_domain_id, toname, fromname, expiry_date, research_group_id) VALUES (new.name, new.dns_domain_id, new.toname, (((new.name)::text || '.'::text) || ((SELECT dns_domains.name FROM public.dns_domains WHERE (dns_domains.id = new.dns_domain_id)))::text), new.expiry_date, new.research_group_id) RETURNING dns_cnames.id, dns_cnames.name, dns_cnames.dns_domain_id, dns_cnames.toname, dns_cnames.expiry_date, dns_cnames.research_group_id;


--
-- Name: hotwire_view_dns_cnames_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_dns_cnames_upd AS ON UPDATE TO "10_View/DNS/CNames" DO INSTEAD UPDATE public.dns_cnames SET name = new.name, dns_domain_id = new.dns_domain_id, research_group_id = new.research_group_id, toname = new.toname, expiry_date = new.expiry_date, fromname = (((new.name)::text || '.'::text) || ((SELECT dns_domains.name FROM public.dns_domains WHERE (dns_domains.id = new.dns_domain_id)))::text) WHERE (dns_cnames.id = old.id);


--
-- Name: hotwire_view_dns_mx_records_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_mx_records_del AS ON DELETE TO "10_View/DNS/MX_Records" DO INSTEAD DELETE FROM public.dns_mxrecords WHERE (dns_mxrecords.id = old.id);


--
-- Name: hotwire_view_dns_mx_records_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_mx_records_ins AS ON INSERT TO "10_View/DNS/MX_Records" DO INSTEAD INSERT INTO public.dns_mxrecords (id, domainname, mailhost) VALUES (nextval('public.dns_mxrecords_id_seq'::regclass), new.domainname, new.mailhost) RETURNING dns_mxrecords.id, dns_mxrecords.domainname, dns_mxrecords.mailhost;


--
-- Name: hotwire_view_dns_mx_records_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_dns_mx_records_upd AS ON UPDATE TO "10_View/DNS/MX_Records" DO INSTEAD UPDATE public.dns_mxrecords SET domainname = new.domainname, mailhost = new.mailhost WHERE (dns_mxrecords.id = old.id);


--
-- Name: hotwire_view_dom0_domu_mapping_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_dom0_domu_mapping_del AS ON DELETE TO "10_View/Dom0_DomU_Mapping" DO INSTEAD DELETE FROM public.mm_dom0_domu WHERE (mm_dom0_domu.dom0_id = old.id);


--
-- Name: hotwire_view_easy_add_machine_view_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_easy_add_machine_view_upd AS ON UPDATE TO "10_View/Easy_Add_Machine" DO INSTEAD (INSERT INTO public.hardware (manufacturer, model, room_id, hardware_type_id, owner_id, name) VALUES (new.manufacturer, new.model, new.room_id, new.hardware_type_id, new.owner_id, new.hardware_name); INSERT INTO public.system_image (wired_mac_1, wired_mac_2, hardware_id, operating_system_id, comments, host_system_image_id, user_id, research_group_id) VALUES (new.wired_mac_1, new.wired_mac_2, currval('public.hardware_id_seq'::regclass), new.operating_system_id, new.system_image_comments, new.host_system_image_id, new.user_id, new.research_group_id); INSERT INTO public.mm_system_image_ip_address (ip_address_id, system_image_id) VALUES ((SELECT ip_address.id FROM public.ip_address WHERE (((ip_address.subnet_id = new.easy_addable_subnet_id) AND (ip_address.reserved <> true)) AND ((ip_address.hostname IS NULL) OR ((ip_address.hostname)::text = ''::text))) LIMIT 1), currval('public.system_image_id_seq'::regclass)); UPDATE public.ip_address SET hostname = new.hostname WHERE (ip_address.id IN (SELECT mm_system_image_ip_address.ip_address_id FROM public.mm_system_image_ip_address WHERE (mm_system_image_ip_address.system_image_id = currval('public.system_image_id_seq'::regclass)))); );


--
-- Name: hotwire_view_easy_add_switch; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_easy_add_switch AS ON UPDATE TO "10_View/Easy_Add_Switch" DO INSTEAD (INSERT INTO public.hardware (manufacturer, model, serial_number, room_id, hardware_type_id, owner_id, name, asset_tag) VALUES (new.manufacturer, (SELECT switch_model.description FROM public.switch_model WHERE (switch_model.id = new.switch_model_id)), new.serial_number, new.room_id, new.switch_hardware_type_id, new.owner_id, new.name, new.asset_tag); INSERT INTO public.switch (hardware_id, switch_model_id) VALUES (currval('public.hardware_id_seq'::regclass), new.switch_model_id); INSERT INTO public.system_image (wired_mac_1, hardware_id, operating_system_id, user_id, research_group_id) VALUES (new.wired_mac_1, currval('public.hardware_id_seq'::regclass), new.operating_system_id, new.user_id, new.research_group_id); UPDATE public.ip_address SET hostname = new.name WHERE (ip_address.id = new.switch_ip_id); INSERT INTO public.mm_system_image_ip_address (ip_address_id, system_image_id) VALUES (new.switch_ip_id, currval('public.system_image_id_seq'::regclass)); );


--
-- Name: hotwire_view_easy_add_virtual_machine_view_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_easy_add_virtual_machine_view_upd AS ON UPDATE TO "10_View/Easy_Add_Virtual_Machine" DO INSTEAD SELECT public.upd_easy_add_virtual_machine(new.*) AS id;


--
-- Name: hotwire_view_edit_hardware_types_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_hardware_types_del AS ON DELETE TO "10_View/Edit_Hardware_Types" DO INSTEAD DELETE FROM public.hardware_type WHERE (hardware_type.id = old.id);


--
-- Name: hotwire_view_edit_hardware_types_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_hardware_types_ins AS ON INSERT TO "10_View/Edit_Hardware_Types" DO INSTEAD INSERT INTO public.hardware_type (name, subnet_id) VALUES (new.name, new.subnet_id) RETURNING hardware_type.id, hardware_type.name, hardware_type.subnet_id;


--
-- Name: hotwire_view_edit_hardware_types_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_hardware_types_upd AS ON UPDATE TO "10_View/Edit_Hardware_Types" DO INSTEAD UPDATE public.hardware_type SET name = new.name, subnet_id = new.subnet_id WHERE (hardware_type.id = old.id);


--
-- Name: hotwire_view_edit_list_of_titles_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_list_of_titles_ins AS ON INSERT TO "10_View/Edit_List_of_Titles" DO INSTEAD INSERT INTO public.title_hid (title_hid) VALUES (new.title);


--
-- Name: hotwire_view_edit_operating_systems_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_operating_systems_del AS ON DELETE TO "10_View/Edit_Operating_Systems" DO INSTEAD DELETE FROM public.operating_system WHERE (operating_system.id = old.id);


--
-- Name: hotwire_view_edit_operating_systems_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_operating_systems_ins AS ON INSERT TO "10_View/Edit_Operating_Systems" DO INSTEAD INSERT INTO public.operating_system (os, nmap_identifies_as, path_to_autoinstaller, default_key) VALUES (new.os, new.nmap_identifies_as, new.path_to_autoinstaller, new.default_key) RETURNING operating_system.id, operating_system.os, operating_system.nmap_identifies_as, operating_system.path_to_autoinstaller, operating_system.default_key;


--
-- Name: hotwire_view_edit_operating_systems_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_edit_operating_systems_upd AS ON UPDATE TO "10_View/Edit_Operating_Systems" DO INSTEAD UPDATE public.operating_system SET os = new.os, nmap_identifies_as = new.nmap_identifies_as, path_to_autoinstaller = new.path_to_autoinstaller, default_key = new.default_key WHERE (operating_system.id = old.id);


--
-- Name: hotwire_view_erasmus_students_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_erasmus_students_del AS ON DELETE TO "10_View/Roles/Erasmus_Students" DO INSTEAD DELETE FROM public.erasmus_socrates_studentship WHERE (erasmus_socrates_studentship.id = old.id);


--
-- Name: hotwire_view_erasmus_students_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_erasmus_students_ins AS ON INSERT TO "10_View/Roles/Erasmus_Students" DO INSTEAD INSERT INTO public.erasmus_socrates_studentship (person_id, erasmus_type_id, supervisor_id, intended_end_date, end_date, start_date, university, notes, force_role_status_to_past) VALUES (new.person_id, new.erasmus_type_id, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.university, new.notes, new.force_role_status_to_past) RETURNING erasmus_socrates_studentship.id, erasmus_socrates_studentship.erasmus_type_id, erasmus_socrates_studentship.person_id, erasmus_socrates_studentship.supervisor_id, NULL::character varying(48) AS "varchar", erasmus_socrates_studentship.start_date, erasmus_socrates_studentship.intended_end_date, erasmus_socrates_studentship.end_date, NULL::bigint AS int8, erasmus_socrates_studentship.university, NULL::character varying(10) AS "varchar", erasmus_socrates_studentship.notes, erasmus_socrates_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";


--
-- Name: hotwire_view_erasmus_students_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_erasmus_students_upd AS ON UPDATE TO "10_View/Roles/Erasmus_Students" DO INSTEAD (UPDATE public.erasmus_socrates_studentship SET person_id = new.person_id, erasmus_type_id = new.erasmus_type_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, university = new.university, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past WHERE (erasmus_socrates_studentship.id = old.id); UPDATE public.person SET email_address = new.email_address, cambridge_college_id = new.cambridge_college_id WHERE (person.id = old.person_id); );


--
-- Name: hotwire_view_fume_hood_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_fume_hood_ins AS ON INSERT TO "10_View/Safety/Fume_Hoods" DO INSTEAD INSERT INTO public.fume_hood (room_id, fume_hood_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, fume_hood_type_id, pdh, notes, research_group_id) VALUES (new.room_id, new.fume_hood_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.fume_hood_type_id, new.pdh, new.notes, new.research_group_id) RETURNING fume_hood.id, fume_hood.room_id, fume_hood.fume_hood_device_type_id, fume_hood.device_number, fume_hood.serial_number, fume_hood.recirc_or_ducted_id, fume_hood.manufacturer, fume_hood.firetrace_type_id, fume_hood.install_year, fume_hood.fume_hood_type_id, fume_hood.pdh, fume_hood.notes, fume_hood.research_group_id;


--
-- Name: hotwire_view_fume_hoods_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_fume_hoods_del AS ON DELETE TO "10_View/Safety/Fume_Hoods" DO INSTEAD DELETE FROM public.fume_hood WHERE (fume_hood.id = old.id);


--
-- Name: hotwire_view_fume_hoods_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_fume_hoods_upd AS ON UPDATE TO "10_View/Safety/Fume_Hoods" DO INSTEAD UPDATE public.fume_hood SET room_id = new.room_id, fume_hood_device_type_id = new.fume_hood_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, fume_hood_type_id = new.fume_hood_type_id, pdh = new.pdh, notes = new.notes, research_group_id = new.research_group_id WHERE (fume_hood.id = old.id);


--
-- Name: hotwire_view_group_fileservers_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_group_fileservers_del AS ON DELETE TO "10_View/Group_Fileservers" DO INSTEAD DELETE FROM public.group_fileserver WHERE (group_fileserver.id = old.id);


--
-- Name: hotwire_view_group_fileservers_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_group_fileservers_ins AS ON INSERT TO "10_View/Group_Fileservers" DO INSTEAD INSERT INTO public.group_fileserver (hostname_for_users, system_image_id, homepath, localhomepath, profilepath, localprofilepath) VALUES (new.hostname_for_users, new.system_image_id, new.homepath, new.localhomepath, new.profilepath, new.localprofilepath) RETURNING group_fileserver.id, group_fileserver.hostname_for_users, group_fileserver.system_image_id, group_fileserver.homepath, group_fileserver.localhomepath, group_fileserver.profilepath, group_fileserver.localprofilepath, NULL::bigint AS int8;


--
-- Name: hotwire_view_group_fileservers_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_group_fileservers_upd AS ON UPDATE TO "10_View/Group_Fileservers" DO INSTEAD UPDATE public.group_fileserver SET hostname_for_users = new.hostname_for_users, system_image_id = new.system_image_id, homepath = new.homepath, localhomepath = new.localhomepath, profilepath = new.profilepath, localprofilepath = new.localprofilepath WHERE (group_fileserver.id = old.id);


--
-- Name: hotwire_view_hardware_basic_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_basic_del AS ON DELETE TO "10_View/Hardware_Basic" DO INSTEAD DELETE FROM public.hardware WHERE (hardware.id = old.id);


--
-- Name: hotwire_view_hardware_basic_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_basic_ins AS ON INSERT TO "10_View/Hardware_Basic" DO INSTEAD INSERT INTO public.hardware (manufacturer, model, name, room_id, hardware_type_id, owner_id) VALUES (new.manufacturer, new.model, new.name, new.room_id, new.hardware_type_id, new.person_id) RETURNING hardware.id, hardware.manufacturer, hardware.model, hardware.name, hardware.room_id, hardware.hardware_type_id, hardware.owner_id;


--
-- Name: hotwire_view_hardware_basic_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_basic_upd AS ON UPDATE TO "10_View/Hardware_Basic" DO INSTEAD UPDATE public.hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, room_id = new.room_id, hardware_type_id = new.hardware_type_id, owner_id = new.person_id WHERE (hardware.id = old.id);


--
-- Name: hotwire_view_hardware_full_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_full_del AS ON DELETE TO "10_View/Hardware_Full" DO INSTEAD DELETE FROM public.hardware WHERE (hardware.id = old.id);


--
-- Name: hotwire_view_hardware_full_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_full_ins AS ON INSERT TO "10_View/Hardware_Full" DO INSTEAD INSERT INTO public.hardware (manufacturer, model, name, serial_number, monitor_serial_number, asset_tag, processor_type, processor_speed, number_of_disks, value_when_new, date_purchased, date_configured, date_decommissioned, delete_after_date, warranty_end_date, warranty_details, room_id, hardware_type_id, owner_id, rackmount, redundant_psu, power, height) VALUES (new.manufacturer, new.model, new.name, new.serial_number, new.monitor_serial_number, new.asset_tag, new.processor_type, new.processor_speed, new.number_of_disks, new.value_when_new, new.date_purchased, new.date_configured, new.date_decommissioned, new.delete_after_date, new.warranty_end_date, new.warranty_details, new.room_id, new.hardware_type_id, new.person_id, new.rackmount, new.redundant_psu, new.power, new.height) RETURNING hardware.id, hardware.manufacturer, hardware.model, hardware.name, hardware.serial_number, hardware.monitor_serial_number, hardware.asset_tag, hardware.processor_type, hardware.processor_speed, hardware.number_of_disks, hardware.value_when_new, hardware.date_purchased, hardware.date_configured, hardware.date_decommissioned, hardware.delete_after_date, hardware.warranty_end_date, hardware.warranty_details, hardware.room_id, hardware.hardware_type_id, hardware.owner_id, hardware.rackmount, hardware.redundant_psu, hardware.power, hardware.height;


--
-- Name: hotwire_view_hardware_full_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_hardware_full_upd AS ON UPDATE TO "10_View/Hardware_Full" DO INSTEAD UPDATE public.hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, processor_type = new.processor_type, processor_speed = new.processor_speed, number_of_disks = new.number_of_disks, value_when_new = new.value_when_new, date_purchased = new.date_purchased, date_configured = new.date_configured, date_decommissioned = new.date_decommissioned, delete_after_date = new.delete_after_date, warranty_end_date = new.warranty_end_date, warranty_details = new.warranty_details, room_id = new.room_id, hardware_type_id = new.hardware_type_id, owner_id = new.person_id, asset_tag = new.asset_tag, rackmount = new.rackmount, redundant_psu = new.redundant_psu, power = new.power, height = new.height WHERE (hardware.id = old.id);


--
-- Name: hotwire_view_ip_address_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_ip_address_del AS ON DELETE TO "10_View/IP_address" DO INSTEAD DELETE FROM public.ip_address WHERE (ip_address.id = old.id);


--
-- Name: hotwire_view_ip_address_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_ip_address_ins AS ON INSERT TO "10_View/IP_address" DO INSTEAD INSERT INTO public.ip_address (id, ip, hostname, subnet_id, hobbit_flags) VALUES (nextval('public.ip_address_id_seq'::regclass), new.ip, new.hostname, new.subnet_id, new.hobbit_flags) RETURNING ip_address.id, ip_address.ip, ip_address.hostname, ip_address.subnet_id, ip_address.hobbit_flags;


--
-- Name: hotwire_view_ip_address_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_ip_address_upd AS ON UPDATE TO "10_View/IP_address" DO INSTEAD UPDATE public.ip_address SET ip = new.ip, hostname = new.hostname, subnet_id = new.subnet_id, hobbit_flags = new.hobbit_flags WHERE (ip_address.id = old.id);


--
-- Name: hotwire_view_ips_without_hardware_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_ips_without_hardware_upd AS ON UPDATE TO "30_Report/IPs_without_Hardware_ro" DO INSTEAD UPDATE public.ip_address SET ip = new.ip, hostname = new.hostname WHERE (ip_address.id = old.id) RETURNING ip_address.id, ip_address.ip, ip_address.hostname;


--
-- Name: hotwire_view_ips_without_hostnames_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_ips_without_hostnames_upd AS ON UPDATE TO "30_Report/IPs_without_Hostnames_ro" DO INSTEAD UPDATE public.ip_address SET ip = new.ip, hostname = new.hostname WHERE (ip_address.id = old.id) RETURNING ip_address.id, ip_address.ip, ip_address.hostname;


--
-- Name: hotwire_view_mailing_lists_optins_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_mailing_lists_optins_upd AS ON UPDATE TO "10_View/Mailing_Lists/OptIns" DO INSTEAD (SELECT public.fn_mm_array_update(new.include_person_id, old.include_person_id, 'mm_mailinglist_include_person'::character varying, 'mailinglist_id'::character varying, 'include_person_id'::character varying, old.id) AS fn_mm_array_update; UPDATE public.mailinglist SET name = new.name, notes = new.notes, extra_addresses = new.extra_addresses, auto_upload = new.auto_upload WHERE (mailinglist.id = old.id); );


--
-- Name: hotwire_view_mailing_lists_optouts_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_mailing_lists_optouts_upd AS ON UPDATE TO "10_View/Mailing_Lists/OptOuts" DO INSTEAD (SELECT public.fn_mm_array_update(new.exclude_person_id, old.exclude_person_id, 'mm_mailinglist_exclude_person'::character varying, 'mailinglist_id'::character varying, 'exclude_person_id'::character varying, old.id) AS fn_mm_array_update; UPDATE public.mailinglist SET name = new.name, notes = new.notes, extra_addresses = new.extra_addresses, auto_upload = new.auto_upload WHERE (mailinglist.id = old.id); );


--
-- Name: hotwire_view_network_switch_config_goal_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_config_goal_del AS ON DELETE TO "10_View/Network/Switch_Config_Goals" DO INSTEAD DELETE FROM public.switch_config_goal WHERE (switch_config_goal.id = old.id);


--
-- Name: hotwire_view_network_switch_config_goal_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_config_goal_ins AS ON INSERT TO "10_View/Network/Switch_Config_Goals" DO INSTEAD INSERT INTO public.switch_config_goal (name) VALUES (new.name);


--
-- Name: hotwire_view_network_switch_config_goal_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_config_goal_upd AS ON UPDATE TO "10_View/Network/Switch_Config_Goals" DO INSTEAD (UPDATE public.switch_config_goal SET name = new.name WHERE (switch_config_goal.id = old.id); SELECT public.fn_mm_array_update((new.switch_id)::bigint[], (old.switch_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_config_goal_id'::character varying, 'switch_id'::character varying, (old.id)::bigint) AS fn_mm_array_update2; );


--
-- Name: hotwire_view_network_switch_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_del AS ON DELETE TO "10_View/Network/Switch" DO INSTEAD DELETE FROM public.switch WHERE (switch.id = old.id);


--
-- Name: hotwire_view_network_switch_port_config_fragment_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_fragment_del AS ON DELETE TO "10_View/Network/Switch_Port_Config_Fragments" DO INSTEAD DELETE FROM public.switch_port_config_fragment WHERE (switch_port_config_fragment.id = old.id);


--
-- Name: hotwire_view_network_switch_port_config_fragment_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_fragment_ins AS ON INSERT TO "10_View/Network/Switch_Port_Config_Fragments" DO INSTEAD (INSERT INTO public.switch_port_config_fragment (switch_port_config_goal_id, config) VALUES (new.switch_port_config_goal_id, new.config); SELECT public.fn_mm_array_update(new.switch_model_id, NULL::bigint[], 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, currval('public.switch_port_config_fragment_id_seq'::regclass)) AS fn_mm_array_update2; );


--
-- Name: hotwire_view_network_switch_port_config_fragment_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_fragment_upd AS ON UPDATE TO "10_View/Network/Switch_Port_Config_Fragments" DO INSTEAD (UPDATE public.switch_port_config_fragment SET switch_port_config_goal_id = new.switch_port_config_goal_id, config = new.config WHERE (switch_port_config_fragment.id = old.id); SELECT public.fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, (old.id)::bigint) AS fn_mm_array_update2; );


--
-- Name: hotwire_view_network_switch_port_config_goal_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_goal_del AS ON DELETE TO "10_View/Network/Switch_Port_Config_Goals" DO INSTEAD DELETE FROM public.switch_port_config_goal WHERE (switch_port_config_goal.id = old.id);


--
-- Name: hotwire_view_network_switch_port_config_goal_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_goal_ins AS ON INSERT TO "10_View/Network/Switch_Port_Config_Goals" DO INSTEAD INSERT INTO public.switch_port_config_goal (name, goal_class) VALUES (new.name, new.goal_class_id);


--
-- Name: hotwire_view_network_switch_port_config_goal_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_port_config_goal_upd AS ON UPDATE TO "10_View/Network/Switch_Port_Config_Goals" DO INSTEAD UPDATE public.switch_port_config_goal SET name = new.name, goal_class = new.goal_class_id WHERE (switch_port_config_goal.id = old.id);


--
-- Name: hotwire_view_network_switch_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_network_switch_upd AS ON UPDATE TO "10_View/Network/Switch" DO INSTEAD (UPDATE public.hardware SET name = new.name, manufacturer = new.manufacturer, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id, comments = new.comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1 WHERE (system_image.id = old._system_image_id); SELECT public.fn_mm_array_update((new.switch_config_goal_id)::bigint[], (old.switch_config_goal_id)::bigint[], 'mm_switch_goal_applies_to_switch'::character varying, 'switch_id'::character varying, 'switch_config_goal_id'::character varying, old.id) AS fn_mm_array_update2; SELECT public.fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old._system_image_id) AS fn_mm_array_update; UPDATE public.switch SET ignore_config = new.ignore_config, switch_model_id = new.switch_model_id, extraconfig = new.extraconfig WHERE (switch.id = old.id); );


--
-- Name: hotwire_view_part_iii_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_part_iii_del AS ON DELETE TO "10_View/Roles/Part_III_Students" DO INSTEAD DELETE FROM public.part_iii_studentship WHERE (part_iii_studentship.id = old.id);


--
-- Name: hotwire_view_part_iii_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_part_iii_ins AS ON INSERT TO "10_View/Roles/Part_III_Students" DO INSTEAD INSERT INTO public.part_iii_studentship (person_id, project_title, supervisor_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past) VALUES (new.person_id, new.project_title_html, new.supervisor_id, new.intended_end_date, new.end_date, new.start_date, new.notes, new.force_role_status_to_past) RETURNING part_iii_studentship.id, part_iii_studentship.person_id, part_iii_studentship.project_title, part_iii_studentship.supervisor_id, part_iii_studentship.intended_end_date, part_iii_studentship.end_date, part_iii_studentship.start_date, NULL::character varying(10) AS "varchar", part_iii_studentship.notes, part_iii_studentship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";


--
-- Name: hotwire_view_part_iii_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_part_iii_upd AS ON UPDATE TO "10_View/Roles/Part_III_Students" DO INSTEAD UPDATE public.part_iii_studentship SET project_title = new.project_title_html, person_id = new.person_id, supervisor_id = new.supervisor_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past WHERE (part_iii_studentship.id = old.id);


--
-- Name: hotwire_view_patch_panel_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_patch_panel_del AS ON DELETE TO "10_View/Patch_Panel" DO INSTEAD DELETE FROM public.patch_panel WHERE (patch_panel.id = old.id);


--
-- Name: hotwire_view_patch_panel_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_patch_panel_ins AS ON INSERT TO "10_View/Patch_Panel" DO INSTEAD INSERT INTO public.patch_panel (id, name, cabinet_id, patch_panel_type_id) VALUES (nextval('public.patch_panel_id_seq'::regclass), new.name, new.cabinet_id, new.patch_panel_type_id) RETURNING patch_panel.id, patch_panel.name, patch_panel.cabinet_id, patch_panel.patch_panel_type_id;


--
-- Name: hotwire_view_patch_panel_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_patch_panel_upd AS ON UPDATE TO "10_View/Patch_Panel" DO INSTEAD UPDATE public.patch_panel SET name = new.name, cabinet_id = new.cabinet_id, patch_panel_type_id = new.patch_panel_type_id WHERE (patch_panel.id = old.id);


--
-- Name: hotwire_view_personnel_basic_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_basic_del AS ON DELETE TO "10_View/People/Personnel_Basic" DO INSTEAD DELETE FROM public.person WHERE (person.id = old.id);


--
-- Name: hotwire_view_personnel_basic_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_basic_ins AS ON INSERT TO "10_View/People/Personnel_Basic" DO INSTEAD SELECT public.hw_fn_personnel_basic_upd(new.*) AS id;


--
-- Name: hotwire_view_personnel_basic_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_basic_upd AS ON UPDATE TO "10_View/People/Personnel_Basic" DO INSTEAD SELECT public.hw_fn_personnel_basic_upd(new.*) AS id;


--
-- Name: hotwire_view_personnel_data_entry_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_data_entry_del AS ON DELETE TO "10_View/People/Personnel_Data_Entry" DO INSTEAD DELETE FROM public.person WHERE (person.id = old.id);


--
-- Name: hotwire_view_personnel_data_entry_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_data_entry_ins AS ON INSERT TO "10_View/People/Personnel_Data_Entry" DO INSTEAD SELECT personnel_data_entry_upd(new.*) AS id;


--
-- Name: hotwire_view_personnel_data_entry_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_data_entry_upd AS ON UPDATE TO "10_View/People/Personnel_Data_Entry" DO INSTEAD SELECT personnel_data_entry_upd(new.*) AS id;


--
-- Name: hotwire_view_personnel_history_v2_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_personnel_history_v2_upd AS ON UPDATE TO "10_View/People/Personnel_History" DO INSTEAD UPDATE public.person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, other_information = (new.other_information)::text, notes = (new.notes)::text WHERE (person.id = old.id);


--
-- Name: hotwire_view_post_history_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_post_history_del AS ON DELETE TO "10_View/Roles/Post_History" DO INSTEAD DELETE FROM public.post_history WHERE (post_history.id = old.id);


--
-- Name: hotwire_view_post_history_update_rules; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_post_history_update_rules AS ON UPDATE TO "10_View/Roles/Post_History" DO INSTEAD (UPDATE public.person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes WHERE (person.id = new.person_id); UPDATE public.post_history SET staff_category_id = new.staff_category_id, supervisor_id = new.supervisor_id, mentor_id = new.mentor_id, external_mentor = new.external_mentor, start_date = new.start_date, intended_end_date = new.intended_end_date, end_date = new.end_date, funding_end_date = new.funding_end_date, chem = new.chem, paid_by_university = new.paid_through_payroll, hours_worked = new.hours_worked, percentage_of_fulltime_hours = new.percentage_of_fulltime_hours, probation_period = new.probation_period, date_of_first_probation_meeting = new.date_of_first_probation_meeting, date_of_second_probation_meeting = new.date_of_second_probation_meeting, date_of_third_probation_meeting = new.date_of_third_probation_meeting, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments, second_probation_meeting_comments = new.second_probation_meeting_comments, third_probation_meeting_comments = new.third_probation_meeting_comments, fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, probation_outcome = new.probation_outcome, date_probation_letters_sent = new.date_probation_letters_sent, research_grant_number = new.research_grant_number, filemaker_funding = new.sponsor, job_title = new.job_title, force_role_status_to_past = new.force_role_status_to_past WHERE (post_history.id = old.id); );


--
-- Name: hotwire_view_postgrad_mentor_meeting_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_postgrad_mentor_meeting_upd AS ON UPDATE TO "10_View/Postgrad_Mentor_Meetings" DO INSTEAD (UPDATE public.mentor_meeting SET postgraduate_studentship_id = new.student_name_id, date_of_meeting = new.date_of_meeting, mentor_id = COALESCE(new.mentor_id, new.usual_mentor_id), notes = new.notes WHERE (mentor_meeting.id = old.id); UPDATE public.postgraduate_studentship SET next_mentor_meeting_due = new.next_mentor_meeting_due, first_mentor_id = new.usual_mentor_id WHERE (postgraduate_studentship.id = old.student_name_id); );


--
-- Name: hotwire_view_postgrad_mentor_meetings_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_postgrad_mentor_meetings_ins AS ON INSERT TO "10_View/Postgrad_Mentor_Meetings" DO INSTEAD (UPDATE public.postgraduate_studentship SET next_mentor_meeting_due = new.next_mentor_meeting_due WHERE (postgraduate_studentship.id = new.student_name_id); INSERT INTO public.mentor_meeting (postgraduate_studentship_id, date_of_meeting, mentor_id, notes) VALUES (new.student_name_id, new.date_of_meeting, COALESCE(new.mentor_id, (SELECT postgraduate_studentship.first_mentor_id FROM public.postgraduate_studentship WHERE (postgraduate_studentship.id = new.student_name_id))), new.notes) RETURNING mentor_meeting.id, NULL::bigint AS int8, NULL::bigint AS int8, mentor_meeting.date_of_meeting, mentor_meeting.mentor_id, NULL::date AS date, mentor_meeting.notes; );


--
-- Name: hotwire_view_postgrad_students_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_postgrad_students_del AS ON DELETE TO "10_View/Roles/Postgrad_Students" DO INSTEAD DELETE FROM public.postgraduate_studentship WHERE (postgraduate_studentship.id = old.id);


--
-- Name: hotwire_view_postgrad_students_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_postgrad_students_ins AS ON INSERT TO "10_View/Roles/Postgrad_Students" DO INSTEAD (SELECT public.fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update; UPDATE public.person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date WHERE (person.id = new.person_id); INSERT INTO public.postgraduate_studentship (id, person_id, postgraduate_studentship_type_id, part_time, first_supervisor_id, second_supervisor_id, external_co_supervisor, substitute_supervisor_id, first_mentor_id, second_mentor_id, external_mentor, next_mentor_meeting_due, university, college_history, cpgs_title, msc_title, mphil_title, phd_thesis_title, paid_through_payroll, emplid_number, gaf_number, start_date, cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_submitted, cpgs_or_mphil_date_awarded, mphil_date_submission_due, mphil_date_submitted, mphil_date_awarded, msc_date_submission_due, msc_date_submitted, msc_date_awarded, date_registered_for_phd, phd_date_title_approved, thesis_submission_due_date, end_of_registration_date, date_phd_submission_due, phd_date_submitted, phd_date_examiners_appointed, phd_date_of_viva, phd_date_awarded, date_withdrawn_from_register, date_reinstated_on_register, intended_end_date, filemaker_funding, filemaker_fees_funding, funding_end_date, progress_notes, force_role_status_to_past, first_year_probationary_report_title, first_year_probationary_report_due, first_year_probationary_report_submitted, first_year_probationary_report_approved) VALUES (nextval('public.postgraduate_studentship_id_seq'::regclass), new.person_id, new.postgraduate_studentship_type_id, new.part_time, new.supervisor_id, new.co_supervisor_id, new.external_co_supervisor, new.substitute_supervisor_id, new.mentor_id, new.co_mentor_id, new.external_mentor, new.next_mentor_meeting_due, new.university, new.college_history, new.cpgs_title_html, new.msc_title_html, new.mphil_title_html, new.phd_thesis_title_html, new.paid_through_payroll, new.emplid_number, new.gaf_number, new.start_date, new.cpgs_or_mphil_date_submission_due, new.cpgs_or_mphil_date_submitted, new.cpgs_or_mphil_date_awarded, new.mphil_date_submission_due, new.mphil_date_submitted, new.mphil_date_awarded, new.msc_date_submission_due, new.msc_date_submitted, new.msc_date_awarded, new.date_registered_for_phd, new.phd_date_title_approved, new."PhD_three_year_submission_date", new.phd_four_year_submission_date, new."Revised_PhD_submission_date", new.phd_date_submitted, new.phd_date_examiners_appointed, new.phd_date_of_viva, new.phd_date_awarded, new.date_withdrawn_from_register, new.date_reinstated_on_register, new.intended_end_date, new.funding, new.fees_funding, new.funding_end_date, new.progress_notes, new.force_role_status_to_past, new.first_year_probationary_report_title_html, new.first_year_probationary_report_due, new.first_year_probationary_report_submitted, new.first_year_probationary_report_approved) RETURNING currval('public.postgraduate_studentship_id_seq'::regclass) AS currval, postgraduate_studentship.person_id, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar", NULL::character varying AS text, NULL::character varying(48) AS "varchar", postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.part_time, postgraduate_studentship.first_supervisor_id, postgraduate_studentship.second_supervisor_id, postgraduate_studentship.external_co_supervisor, postgraduate_studentship.substitute_supervisor_id, postgraduate_studentship.first_mentor_id, postgraduate_studentship.second_mentor_id, postgraduate_studentship.external_mentor, postgraduate_studentship.next_mentor_meeting_due, postgraduate_studentship.university, NULL::bigint AS int8, postgraduate_studentship.college_history, postgraduate_studentship.cpgs_title, postgraduate_studentship.first_year_probationary_report_title, postgraduate_studentship.msc_title, postgraduate_studentship.mphil_title, postgraduate_studentship.phd_thesis_title, postgraduate_studentship.paid_through_payroll, postgraduate_studentship.emplid_number, postgraduate_studentship.gaf_number, NULL::bigint[] AS int8, postgraduate_studentship.start_date, NULL::date AS date, postgraduate_studentship.cpgs_or_mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submitted, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.first_year_probationary_report_due, postgraduate_studentship.first_year_probationary_report_submitted, postgraduate_studentship.first_year_probationary_report_approved, postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.mphil_date_submitted, postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.msc_date_submission_due, postgraduate_studentship.msc_date_submitted, postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.phd_date_title_approved, postgraduate_studentship.thesis_submission_due_date AS "PhD_three_year_submission_date", postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date, postgraduate_studentship.date_phd_submission_due AS "Revised_PhD_submission_date", postgraduate_studentship.phd_date_submitted, postgraduate_studentship.phd_date_examiners_appointed, postgraduate_studentship.phd_date_of_viva, postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register, postgraduate_studentship.date_reinstated_on_register, postgraduate_studentship.intended_end_date, postgraduate_studentship.filemaker_funding, postgraduate_studentship.filemaker_fees_funding, postgraduate_studentship.funding_end_date, (btrim((age((postgraduate_studentship.phd_date_submitted)::timestamp with time zone, (postgraduate_studentship.date_registered_for_phd)::timestamp with time zone))::text, '@ '::text))::character varying(30) AS btrim, NULL::integer AS int4, postgraduate_studentship.progress_notes, postgraduate_studentship.force_role_status_to_past, NULL::text AS text, (((('<a href="edit.php?_view=10_View/People/Personnel_Data_Entry&_id='::text || postgraduate_studentship.person_id) || '">Personnel Data Entry</a>&nbsp;<a href="edit.php?_view=10_View/People/Personnel_History&_id='::text) || postgraduate_studentship.person_id) || '">Personnel History</a>'::text) AS ro_shortcuts_html; );


--
-- Name: hotwire_view_postgrad_students_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_postgrad_students_upd AS ON UPDATE TO "10_View/Roles/Postgrad_Students" DO INSTEAD (UPDATE public.postgraduate_studentship SET postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, part_time = new.part_time, first_supervisor_id = new.supervisor_id, second_supervisor_id = new.co_supervisor_id, external_co_supervisor = new.external_co_supervisor, substitute_supervisor_id = new.substitute_supervisor_id, first_mentor_id = new.mentor_id, second_mentor_id = new.co_mentor_id, external_mentor = new.external_mentor, next_mentor_meeting_due = new.next_mentor_meeting_due, university = new.university, college_history = new.college_history, cpgs_title = new.cpgs_title_html, msc_title = new.msc_title_html, mphil_title = new.mphil_title_html, phd_thesis_title = new.phd_thesis_title_html, paid_through_payroll = new.paid_through_payroll, emplid_number = new.emplid_number, gaf_number = new.gaf_number, start_date = new.start_date, cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_submitted = new.cpgs_or_mphil_date_submitted, cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded, mphil_date_submission_due = new.mphil_date_submission_due, mphil_date_submitted = new.mphil_date_submitted, mphil_date_awarded = new.mphil_date_awarded, msc_date_submission_due = new.msc_date_submission_due, msc_date_submitted = new.msc_date_submitted, msc_date_awarded = new.msc_date_awarded, date_registered_for_phd = new.date_registered_for_phd, phd_date_title_approved = new.phd_date_title_approved, thesis_submission_due_date = new."PhD_three_year_submission_date", end_of_registration_date = new.phd_four_year_submission_date, date_phd_submission_due = new."Revised_PhD_submission_date", phd_date_submitted = new.phd_date_submitted, phd_date_examiners_appointed = new.phd_date_examiners_appointed, phd_date_of_viva = new.phd_date_of_viva, phd_date_awarded = new.phd_date_awarded, date_withdrawn_from_register = new.date_withdrawn_from_register, date_reinstated_on_register = new.date_reinstated_on_register, intended_end_date = new.intended_end_date, filemaker_funding = new.funding, filemaker_fees_funding = new.fees_funding, funding_end_date = new.funding_end_date, progress_notes = new.progress_notes, force_role_status_to_past = new.force_role_status_to_past, first_year_probationary_report_title = new.first_year_probationary_report_title_html, first_year_probationary_report_due = new.first_year_probationary_report_due, first_year_probationary_report_submitted = new.first_year_probationary_report_submitted, first_year_probationary_report_approved = new.first_year_probationary_report_approved WHERE (postgraduate_studentship.id = old.id); UPDATE public.person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date WHERE (person.id = new.person_id); SELECT public.fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update; );


--
-- Name: hotwire_view_postgrad_training_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_postgrad_training_del AS ON DELETE TO "10_View/Postgrad_Training" DO INSTEAD DELETE FROM public.postgraduate_studentship WHERE (postgraduate_studentship.id = old.id);


--
-- Name: hotwire_view_postgrad_training_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_postgrad_training_ins AS ON INSERT TO "10_View/Postgrad_Training" DO INSTEAD INSERT INTO public.postgraduate_studentship (person_id, postgraduate_studentship_type_id, first_supervisor_id, start_date, filemaker_funding, filemaker_fees_funding, transferable_skills_days_1st_year, transferable_skills_days_2nd_year, transferable_skills_days_3rd_year, transferable_skills_days_4th_year) VALUES (new.person_id, new.postgraduate_studentship_type_id, new.supervisor_id, new.start_date, new.funding, new.fees_funding, new.transferable_skills_days_1st_year, new.transferable_skills_days_2nd_year, new.transferable_skills_days_3rd_year, new.transferable_skills_days_4th_year) RETURNING postgraduate_studentship.id, postgraduate_studentship.person_id, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar", postgraduate_studentship.postgraduate_studentship_type_id, postgraduate_studentship.first_supervisor_id, postgraduate_studentship.start_date, NULL::character varying(20) AS "varchar", postgraduate_studentship.filemaker_funding, postgraduate_studentship.filemaker_fees_funding, postgraduate_studentship.transferable_skills_days_1st_year, postgraduate_studentship.transferable_skills_days_2nd_year, postgraduate_studentship.transferable_skills_days_3rd_year, postgraduate_studentship.transferable_skills_days_4th_year, NULL::real AS float4;


--
-- Name: hotwire_view_postgrad_training_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_postgrad_training_upd AS ON UPDATE TO "10_View/Postgrad_Training" DO INSTEAD UPDATE public.postgraduate_studentship SET person_id = new.person_id, postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, first_supervisor_id = new.supervisor_id, start_date = new.start_date, filemaker_funding = new.funding, filemaker_fees_funding = new.fees_funding, transferable_skills_days_1st_year = new.transferable_skills_days_1st_year, transferable_skills_days_2nd_year = new.transferable_skills_days_2nd_year, transferable_skills_days_3rd_year = new.transferable_skills_days_3rd_year, transferable_skills_days_4th_year = new.transferable_skills_days_4th_year WHERE (postgraduate_studentship.id = old.id);


--
-- Name: hotwire_view_printers_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_printers_del AS ON DELETE TO "10_View/Printers" DO INSTEAD (DELETE FROM public.printer WHERE (printer.id = old.id); DELETE FROM public.hardware WHERE (hardware.id = old._hardware_id); );


--
-- Name: hotwire_view_printers_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_printers_upd AS ON UPDATE TO "10_View/Printers" DO INSTEAD (UPDATE public.hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, date_purchased = new.date_purchased, serial_number = new.serial_number, asset_tag = new.asset_tag, log_usage = new.log_usage, usage_url = new.usage_url, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.room_id, owner_id = new.owner_id WHERE (hardware.id = old._hardware_id); UPDATE public.printer SET printer_class_id = new.printer_class_id, cups_options = new.cups_options WHERE (printer.id = old.id); );


--
-- Name: hotwire_view_registration_and_leaving_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_registration_and_leaving_upd AS ON UPDATE TO "10_View/Registration_and_Leaving" DO INSTEAD UPDATE public.person SET arrival_date = new.date_starting, leaving_date = new.date_left, registration_completed = new.registration_completed, clearance_cert_signed = new.clearance_cert_signed WHERE (person.id = old.id);


--
-- Name: hotwire_view_research_group_editing_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_group_editing_del AS ON DELETE TO "10_View/Research_Group_Editing" DO INSTEAD DELETE FROM public.research_group WHERE (research_group.id = old.id);


--
-- Name: hotwire_view_research_group_editing_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_group_editing_ins AS ON INSERT TO "10_View/Research_Group_Editing" DO INSTEAD SELECT public.hw_fn_research_group_ins(new.*) AS id;


--
-- Name: hotwire_view_research_group_editing_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_group_editing_upd AS ON UPDATE TO "10_View/Research_Group_Editing" DO INSTEAD (UPDATE public.research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, sector_id = new.sector_id WHERE (research_group.id = old.id); SELECT public.fn_mm_array_update(new.person_id, old.person_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.person_id, old.person_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hotwire_view_research_group_test2_editing_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_research_group_test2_editing_upd AS ON UPDATE TO "10_View/Research_Group_Test2_Editing" DO INSTEAD (UPDATE public.research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, sector_id = new.sector_id WHERE (research_group.id = old.id); SELECT public.fn_mm_array_update(new.person_id, old.person_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.person_id, old.person_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hotwire_view_research_group_test_editing_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_research_group_test_editing_upd AS ON UPDATE TO "10_View/Research_Group_Test_Editing" DO INSTEAD UPDATE public.research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, sector_id = new.sector_id WHERE (research_group.id = old.id);


--
-- Name: hotwire_view_research_groups_computing_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_groups_computing_del AS ON DELETE TO "10_View/Research_Groups_Computing" DO INSTEAD DELETE FROM public.research_group WHERE (research_group.id = old.id);


--
-- Name: hotwire_view_research_groups_computing_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_groups_computing_ins AS ON INSERT TO "10_View/Research_Groups_Computing" DO INSTEAD SELECT public.hw_fn_research_groups_computing_upd(new.*) AS id;


--
-- Name: hotwire_view_research_groups_computing_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_groups_computing_upd AS ON UPDATE TO "10_View/Research_Groups_Computing" DO INSTEAD SELECT public.hw_fn_research_groups_computing_upd(new.*) AS id;


--
-- Name: hotwire_view_research_interest_group_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_interest_group_ins AS ON INSERT TO "10_View/Research_Interest_Group" DO INSTEAD SELECT public.hw_fn_research_interest_group_upd(new.*) AS id;


--
-- Name: hotwire_view_research_interest_group_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_research_interest_group_upd AS ON UPDATE TO "10_View/Research_Interest_Group" DO INSTEAD (UPDATE public.research_interest_group SET name = new.name, chair_id = new.chair_id, website = new.website, mailing_list_address = new.mailing_list_address WHERE (research_interest_group.id = old.id); SELECT public.fn_mm_array_update(new.rig_member_id, old.rig_member_id, 'mm_member_research_interest_group'::character varying, 'research_interest_group_id'::character varying, 'member_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_primary_rig_update(new.primary_member_id, old.primary_member_id, old.id) AS fn_primary_rig_update; );


--
-- Name: hotwire_view_room_attributes_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_attributes_del AS ON DELETE TO "10_View/Rooms/Room_Attributes" DO INSTEAD DELETE FROM public.room WHERE (room.id = old.id);


--
-- Name: hotwire_view_room_attributes_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_attributes_ins AS ON INSERT TO "10_View/Rooms/Room_Attributes" DO INSTEAD INSERT INTO public.room (name, building_id, building_region_id, building_floor_id, area, room_type_id, number_of_desks, number_of_benches, maximum_occupancy, last_refurbished_date, responsible_group_id, comments, power_supply, cooling_power) VALUES (new.name, new.building_id, new.building_region_id, new.building_floor_id, new.area, new.room_type_id, new.number_of_desks, new.number_of_benches, new.maximum_occupancy, new.last_refurbished_date, new.responsible_group_id, new.comments, new.power_supply, new.cooling_power) RETURNING room.id, room.name, room.building_id, room.building_region_id, room.building_floor_id, room.area, room.room_type_id, room.maximum_occupancy, room.number_of_desks, room.number_of_benches, room.last_refurbished_date, room.responsible_group_id, NULL::bigint AS int8, room.comments, room.power_supply, room.cooling_power;


--
-- Name: hotwire_view_room_attributes_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_attributes_upd AS ON UPDATE TO "10_View/Rooms/Room_Attributes" DO INSTEAD UPDATE public.room SET name = new.name, building_id = new.building_id, building_region_id = new.building_region_id, building_floor_id = new.building_floor_id, number_of_desks = new.number_of_desks, number_of_benches = new.number_of_benches, area = new.area, room_type_id = new.room_type_id, maximum_occupancy = new.maximum_occupancy, last_refurbished_date = new.last_refurbished_date, responsible_group_id = new.responsible_group_id, comments = new.comments, power_supply = new.power_supply, cooling_power = new.cooling_power WHERE (room.id = old.id);


--
-- Name: hotwire_view_room_occupancy_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_occupancy_del AS ON DELETE TO "10_View/Rooms/Room_Occupancy" DO INSTEAD DELETE FROM public.room WHERE (room.id = old.id);


--
-- Name: hotwire_view_room_occupancy_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_occupancy_ins AS ON INSERT TO "10_View/Rooms/Room_Occupancy" DO INSTEAD INSERT INTO public.room (name, room_type_id, responsible_group_id, number_of_desks, maximum_occupancy, area, last_refurbished_date, comments) VALUES (new.name, new.room_type_id, new.responsible_group_id, new.number_of_desks, new.maximum_occupancy, new.area, new.last_refurbished_date, new.comments) RETURNING room.id, room.name, room.room_type_id, room.responsible_group_id, NULL::bigint AS int8, NULL::bigint AS int8, room.number_of_desks, room.maximum_occupancy, NULL::bigint AS int8, room.area, room.last_refurbished_date, room.comments, public._to_hwsubview('room_id'::text, '10_View/People/Room_Occupants'::text, 'id'::text) AS room_occupants_subview;


--
-- Name: hotwire_view_room_occupancy_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_room_occupancy_upd AS ON UPDATE TO "10_View/Rooms/Room_Occupancy" DO INSTEAD UPDATE public.room SET name = new.name, room_type_id = new.room_type_id, responsible_group_id = new.responsible_group_id, number_of_desks = new.number_of_desks, maximum_occupancy = new.maximum_occupancy, area = new.area, last_refurbished_date = new.last_refurbished_date, comments = new.comments WHERE (room.id = old.id);


--
-- Name: hotwire_view_room_occupants_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_room_occupants_upd AS ON UPDATE TO "10_View/Rooms/Room_Occupants" DO INSTEAD (UPDATE public.room SET name = new.name, room_type_id = new.room_type_id, responsible_group_id = new.responsible_group_id, number_of_desks = new.number_of_desks, maximum_occupancy = new.maximum_occupancy, area = new.area, last_refurbished_date = new.last_refurbished_date, comments = new.comments WHERE (room.id = old.id); SELECT public.fn_mm_array_update(new.person_id, old.person_id, 'mm_person_room'::character varying, 'room_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hotwire_view_socket_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_socket_del AS ON DELETE TO "10_View/Socket" DO INSTEAD DELETE FROM public.socket WHERE (socket.id = old.id);


--
-- Name: hotwire_view_socket_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_socket_ins AS ON INSERT TO "10_View/Socket" DO INSTEAD SELECT public.hw_fn_socket_insert_or_update(new.*) AS id;


--
-- Name: hotwire_view_socket_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_socket_upd AS ON UPDATE TO "10_View/Socket" DO INSTEAD SELECT public.hw_fn_socket_insert_or_update(new.*) AS id;


--
-- Name: hotwire_view_staff_reviews_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_staff_reviews_del AS ON DELETE TO "10_View/Staff_Reviews" DO INSTEAD DELETE FROM public.staff_review_meeting WHERE (staff_review_meeting.id = old.id);


--
-- Name: hotwire_view_staff_reviews_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_staff_reviews_ins AS ON INSERT TO "10_View/Staff_Reviews" DO INSTEAD (UPDATE public.person SET next_review_due = new.next_review_due WHERE (person.id = new.person_id); INSERT INTO public.staff_review_meeting (person_id, date_of_meeting, reviewer_id, notes, last_updated) VALUES (new.person_id, new.date_of_meeting, COALESCE(new.reviewer_id, (SELECT person.usual_reviewer_id FROM public.person WHERE (person.id = new.person_id))), new.notes, now()) RETURNING staff_review_meeting.id, NULL::bigint AS int8, NULL::bigint AS int8, NULL::bigint AS int8, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, NULL::date AS date, staff_review_meeting.notes, NULL::timestamp without time zone AS "timestamp"; );


--
-- Name: hotwire_view_staff_reviews_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_staff_reviews_upd AS ON UPDATE TO "10_View/Staff_Reviews" DO INSTEAD (UPDATE public.staff_review_meeting SET date_of_meeting = new.date_of_meeting, reviewer_id = COALESCE(new.reviewer_id, new.usual_reviewer_id), notes = new.notes, last_updated = now() WHERE (staff_review_meeting.id = old.id); UPDATE public.person SET next_review_due = new.next_review_due, usual_reviewer_id = new.usual_reviewer_id WHERE (person.id = old.person_id); );


--
-- Name: hotwire_view_subnet_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_subnet_ins AS ON INSERT TO "10_View/Network/Subnet" DO INSTEAD INSERT INTO public.subnet (network_address, router, domain_name, dns_domain_id, preferred_dns1, preferred_dns2, preferred_dns3, provide_dhcp_service, vlan_id, notes) VALUES (new.network_address, new.router, new.domain_name, new.domain_for_dns_server_id, new.preferred_dns1, new.preferred_dns2, new.preferred_dns3, new.provide_dhcp_service, new.vlan_id, new.notes) RETURNING subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1, subnet.dns2, subnet.dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes;


--
-- Name: hotwire_view_subnet_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_subnet_upd AS ON UPDATE TO "10_View/Network/Subnet" DO INSTEAD UPDATE public.subnet SET network_address = new.network_address, router = new.router, domain_name = new.domain_name, dns_domain_id = new.domain_for_dns_server_id, preferred_dns1 = new.preferred_dns1, preferred_dns2 = new.preferred_dns2, preferred_dns3 = new.preferred_dns3, provide_dhcp_service = new.provide_dhcp_service, vlan_id = new.vlan_id, notes = new.notes WHERE (subnet.id = old.id) RETURNING subnet.id, subnet.network_address, subnet.router, subnet.domain_name, subnet.dns_domain_id, subnet.preferred_dns1, subnet.preferred_dns2, subnet.preferred_dns3, subnet.dns1, subnet.dns2, subnet.dns3, subnet.provide_dhcp_service, subnet.vlan_id, subnet.notes;


--
-- Name: hotwire_view_switchport_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_switchport_del AS ON DELETE TO "10_View/Switchport" DO INSTEAD DELETE FROM public.switchport WHERE (switchport.id = old.id);


--
-- Name: hotwire_view_switchport_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_switchport_ins AS ON INSERT TO "10_View/Switchport" DO INSTEAD INSERT INTO public.switchport (name, socket_id, switch_id) VALUES (new.name, new.socket_id, new.switch_id) RETURNING switchport.id, switchport.name, switchport.socket_id, switchport.switch_id;


--
-- Name: hotwire_view_switchport_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_switchport_upd AS ON UPDATE TO "10_View/Switchport" DO INSTEAD UPDATE public.switchport SET name = new.name, socket_id = new.socket_id, switch_id = new.switch_id WHERE (switchport.id = old.id);


--
-- Name: hotwire_view_system_image_ai_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_ai_upd AS ON UPDATE TO "10_View/System_Image_Autoinstaller" DO INSTEAD (UPDATE public.hardware SET name = new.hardware_name, hardware_type_id = new.hardware_type_id, room_id = new.room_id, owner_id = new.owner_id, comments = new.hardware_comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET operating_system_id = new.operating_system_id, wired_mac_1 = new.wired_mac_1, wired_mac_2 = new.wired_mac_2, wireless_mac = new.wireless_mac, architecture_id = new.architecture_id, reinstall_on_next_boot = new.reinstall_on_next_boot, host_system_image_id = new.host_system_image_id, comments = new.system_image_comments, research_group_id = new.research_group_id, user_id = new.user_id, netboot_id = new.netboot_id WHERE (system_image.id = old.id); SELECT public.fn_mm_array_update(new.ip_address_id, old.ip_address_id, 'mm_system_image_ip_address'::character varying, 'system_image_id'::character varying, 'ip_address_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.software_package_id, old.software_package_id, 'mm_system_image_software_package_jumpstart'::character varying, 'system_image_id'::character varying, 'software_package_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.installer_tag_id, old.installer_tag_id, 'mm_system_image_installer_tag'::character varying, 'system_image_id'::character varying, 'installer_tag_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hotwire_view_system_image_all_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_all_del AS ON DELETE TO "10_View/System_Image_All" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: hotwire_view_system_image_all_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_system_image_all_ins AS ON INSERT TO "10_View/Dom0_DomU_Mapping" DO INSTEAD SELECT public.hw_fn_dom0_domu_mapping_upd(new.*) AS id;


--
-- Name: hotwire_view_system_image_all_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_all_ins AS ON INSERT TO "10_View/System_Image_All" DO INSTEAD SELECT public.hw_fn_system_image_all_upd(new.*) AS id;


--
-- Name: hotwire_view_system_image_all_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_view_system_image_all_upd AS ON UPDATE TO "10_View/Dom0_DomU_Mapping" DO INSTEAD SELECT public.hw_fn_dom0_domu_mapping_upd(new.*) AS id;


--
-- Name: hotwire_view_system_image_all_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_all_upd AS ON UPDATE TO "10_View/System_Image_All" DO INSTEAD SELECT public.hw_fn_system_image_all_upd(new.*) AS id;


--
-- Name: hotwire_view_system_image_asset_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_asset_del AS ON DELETE TO "10_View/System_Image_Asset" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: hotwire_view_system_image_asset_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_asset_ins AS ON INSERT TO "10_View/System_Image_Asset" DO INSTEAD SELECT public.hw_fn_system_image_asset_ins(new.*) AS id;


--
-- Name: hotwire_view_system_image_asset_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_asset_upd AS ON UPDATE TO "10_View/System_Image_Asset" DO INSTEAD (UPDATE public.hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, owner_id = new.owner_id, value_when_new = new.value_when_new, personally_owned = new.personally_owned, comments = new.comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET operating_system_id = new.operating_system_id WHERE (system_image.id = old.id); );


--
-- Name: hotwire_view_system_image_basic_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_basic_del AS ON DELETE TO "10_View/System_Image_Basic" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: hotwire_view_system_image_basic_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_basic_ins AS ON INSERT TO "10_View/System_Image_Basic" DO INSTEAD INSERT INTO public.system_image (hardware_id, operating_system_id, comments) VALUES (new.hardware_id, new.operating_system_id, new.comments) RETURNING system_image.id, NULL::bigint AS int8, NULL::bigint AS int8, system_image.comments;


--
-- Name: hotwire_view_system_image_basic_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_basic_upd AS ON UPDATE TO "10_View/System_Image_Basic" DO INSTEAD UPDATE public.system_image SET hardware_id = new.hardware_id, operating_system_id = new.operating_system_id, comments = new.comments WHERE (system_image.id = old.id);


--
-- Name: hotwire_view_system_image_user_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_user_del AS ON DELETE TO "10_View/System_Image_User" DO INSTEAD DELETE FROM public.system_image WHERE (system_image.id = old.id);


--
-- Name: hotwire_view_system_image_user_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_user_ins AS ON INSERT TO "10_View/System_Image_User" DO INSTEAD SELECT public.hw_fn_system_image_user_upd(new.*) AS id;


--
-- Name: hotwire_view_system_image_user_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_system_image_user_upd AS ON UPDATE TO "10_View/System_Image_User" DO INSTEAD SELECT public.hw_fn_system_image_user_upd(new.*) AS id;


--
-- Name: hotwire_view_telephone_basic_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_telephone_basic_del AS ON DELETE TO "10_View/Telephone_Basic" DO INSTEAD DELETE FROM public.dept_telephone_number WHERE (dept_telephone_number.id = old.id);


--
-- Name: hotwire_view_telephone_basic_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_telephone_basic_ins AS ON INSERT TO "10_View/Telephone_Basic" DO INSTEAD SELECT public.hw_fn_telephone_basic_upd(new.*) AS id;


--
-- Name: hotwire_view_telephone_basic_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_telephone_basic_upd AS ON UPDATE TO "10_View/Telephone_Basic" DO INSTEAD SELECT public.hw_fn_telephone_basic_upd(new.*) AS id;


--
-- Name: hotwire_view_telephone_management_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_telephone_management_del AS ON DELETE TO "10_View/Telephone_Management" DO INSTEAD (DELETE FROM public.mm_person_dept_telephone_number WHERE (mm_person_dept_telephone_number.dept_telephone_number_id = old.id); DELETE FROM public.dept_telephone_number WHERE (dept_telephone_number.id = old.id); );


--
-- Name: hotwire_view_telephone_management_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_telephone_management_ins AS ON INSERT TO "10_View/Telephone_Management" DO INSTEAD SELECT public.hw_fn_telephone_mgmt_upd(new.*) AS id;


--
-- Name: hotwire_view_telephone_management_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_telephone_management_upd AS ON UPDATE TO "10_View/Telephone_Management" DO INSTEAD SELECT public.hw_fn_telephone_mgmt_upd(new.*) AS id;


--
-- Name: hotwire_view_user_account_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_user_account_del AS ON DELETE TO "10_View/User_Account" DO INSTEAD DELETE FROM public.user_account WHERE (user_account.id = old.id);


--
-- Name: hotwire_view_user_account_ins; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_user_account_ins AS ON INSERT TO "10_View/User_Account" DO INSTEAD INSERT INTO public.user_account (person_id, username, uid, description, gecos, expiry_date, is_disabled) VALUES (new.x_person_id, new.username, new.uid, new.description, new.gecos, new.expiry_date, new.is_disabled) RETURNING user_account.id, user_account.person_id AS x_person_id, user_account.username, user_account.uid, user_account.description, user_account.gecos, user_account.expiry_date, user_account.is_disabled;


--
-- Name: hotwire_view_user_account_upd; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hotwire_view_user_account_upd AS ON UPDATE TO "10_View/User_Account" DO INSTEAD UPDATE public.user_account SET username = new.username, uid = new.uid, description = new.description, gecos = new.gecos, expiry_date = new.expiry_date, is_disabled = new.is_disabled WHERE (user_account.id = old.id);


--
-- Name: hotwire_view_visitors_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_visitors_del AS ON DELETE TO "10_View/Roles/Visitors" DO INSTEAD DELETE FROM public.visitorship WHERE (visitorship.id = old.id);


--
-- Name: hotwire_view_visitors_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_visitors_ins AS ON INSERT TO "10_View/Roles/Visitors" DO INSTEAD INSERT INTO public.visitorship (home_institution, visitor_type_id, person_id, host_person_id, intended_end_date, end_date, start_date, notes, force_role_status_to_past) VALUES (new.home_institution, new.visitor_type_id, new.person_id, new.host_id, new.intended_end_date, new.end_date, new.start_date, new.notes, new.force_role_status_to_past) RETURNING visitorship.id, visitorship.person_id, NULL::character varying(10) AS "varchar", visitorship.home_institution, visitorship.visitor_type_id, visitorship.host_person_id AS host_id, visitorship.start_date, visitorship.intended_end_date, visitorship.end_date, visitorship.notes, visitorship.force_role_status_to_past, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";


--
-- Name: hotwire_view_visitors_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_visitors_upd AS ON UPDATE TO "10_View/Roles/Visitors" DO INSTEAD UPDATE public.visitorship SET home_institution = new.home_institution, visitor_type_id = new.visitor_type_id, person_id = new.person_id, host_person_id = new.host_id, intended_end_date = new.intended_end_date, end_date = new.end_date, start_date = new.start_date, notes = new.notes, force_role_status_to_past = new.force_role_status_to_past WHERE (visitorship.id = old.id);


--
-- Name: hotwire_view_vlan_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_vlan_del AS ON DELETE TO "10_View/Network/VLAN" DO INSTEAD DELETE FROM public.vlan WHERE (vlan.id = old.id);


--
-- Name: hotwire_view_vlan_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_vlan_ins AS ON INSERT TO "10_View/Network/VLAN" DO INSTEAD INSERT INTO public.vlan (vid, name, description, vrrpid) VALUES (new.vid, new.name, new.description, new.vrrpid) RETURNING vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid;


--
-- Name: hotwire_view_vlan_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hotwire_view_vlan_upd AS ON UPDATE TO "10_View/Network/VLAN" DO INSTEAD UPDATE public.vlan SET vid = new.vid, name = new.name, description = new.description, vrrpid = new.vrrpid WHERE (vlan.id = old.id) RETURNING vlan.id, vlan.vid, vlan.name, vlan.description, vlan.vrrpid;


--
-- Name: hptwire_view_postgrad_mentor_meetings_del; Type: RULE; Schema: hotwire; Owner: cen1001
--

CREATE RULE hptwire_view_postgrad_mentor_meetings_del AS ON DELETE TO "10_View/Postgrad_Mentor_Meetings" DO INSTEAD DELETE FROM public.mentor_meeting WHERE (mentor_meeting.id = old.id);


--
-- Name: hw_all_contact_details_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hw_all_contact_details_upd AS ON UPDATE TO "10_View/People/All_Contact_Details" DO INSTEAD (UPDATE public.person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, known_as = new.known_as, name_suffix = new.name_suffix, previous_surname = new.previous_surname, gender_id = new.gender_id, crsid = new.crsid, email_address = new.email_address, arrival_date = new.arrival_date, leaving_date = new.leaving_date, left_but_no_leaving_date_given = new.left_but_no_leaving_date_given, external_work_phone_numbers = new.external_work_phone_numbers, cambridge_address = new.home_address, cambridge_phone_number = new.home_phone_number, cambridge_college_id = new.cambridge_college_id, mobile_number = new.mobile_number, emergency_contact = new.emergency_contact, location = new.location, forwarding_address = new.forwarding_address, new_employer_address = new.new_employer_address WHERE (person.id = old.id); SELECT public.fn_mm_array_update(new.dept_telephone_number_id, old.dept_telephone_number_id, 'mm_person_dept_telephone_number'::character varying, 'person_id'::character varying, 'dept_telephone_number_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.room_id, old.room_id, 'mm_person_room'::character varying, 'person_id'::character varying, 'room_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.research_group_id, old.research_group_id, 'mm_person_research_group'::character varying, 'person_id'::character varying, 'research_group_id'::character varying, old.id) AS fn_mm_array_update; SELECT public.fn_mm_array_update(new.nationality_id, old.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, old.id) AS fn_mm_array_update; );


--
-- Name: hw_rg_comp_reps_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE hw_rg_comp_reps_upd AS ON UPDATE TO "10_View/Research_Groups/Computer_reps" DO INSTEAD (SELECT public.fn_mm_array_update(new.computer_rep_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, new.id) AS fn_mm_array_update; UPDATE public.research_group SET name = new.group_name, head_of_group_id = new.head_of_group_id WHERE (research_group.id = old.id); );


--
-- Name: hw_user_preferences_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_del AS ON DELETE TO "90_Action/Hotwire/User Preferences" DO INSTEAD DELETE FROM "hw_User Preferences" WHERE ("hw_User Preferences".id = old.id);


--
-- Name: hw_user_preferences_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_ins AS ON INSERT TO "90_Action/Hotwire/User Preferences" DO INSTEAD INSERT INTO "hw_User Preferences" (preference_id, preference_value, user_id) VALUES (new.preference_id, new.preference_value, new."Postgres_User_id");


--
-- Name: hw_user_preferences_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_upd AS ON UPDATE TO "90_Action/Hotwire/User Preferences" DO INSTEAD UPDATE "hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new."Postgres_User_id" WHERE ("hw_User Preferences".id = old.id);


--
-- Name: ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE ins AS ON INSERT TO "10_View/Network/Switch Config Fragments" DO INSTEAD (INSERT INTO public.switch_config_fragment (name, config, switch_config_goal_id) VALUES (new.name, new.config, new.switch_config_goal_id); SELECT public.fn_mm_array_update(new.switch_model_id, NULL::bigint[], 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, currval('public.switch_config_fragments_id_seq'::regclass)) AS fn_mm_array_update; );


--
-- Name: paper_licence_certs_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE paper_licence_certs_del AS ON DELETE TO "30_WPKG/25_paper_licence_certs" DO INSTEAD DELETE FROM public.paper_licence_certificate WHERE (paper_licence_certificate.id = old.id);


--
-- Name: paper_licence_certs_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE paper_licence_certs_ins AS ON INSERT TO "30_WPKG/25_paper_licence_certs" DO INSTEAD INSERT INTO public.paper_licence_certificate (image_oid, asset_tag) VALUES ((SELECT (y.x).val AS val FROM (SELECT new.image_oid AS x) y), new.asset_tag) RETURNING paper_licence_certificate.id, paper_licence_certificate.asset_tag, ROW('application/pdf'::character varying, paper_licence_certificate.image_oid)::public.blobtype AS "row";


--
-- Name: paper_licence_certs_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE paper_licence_certs_upd AS ON UPDATE TO "30_WPKG/25_paper_licence_certs" DO INSTEAD UPDATE public.paper_licence_certificate SET image_oid = (SELECT (y.x).val AS val FROM (SELECT new.image_oid AS x) y), asset_tag = new.asset_tag WHERE (paper_licence_certificate.id = old.id) RETURNING paper_licence_certificate.id, paper_licence_certificate.asset_tag, ROW('application/pdf'::character varying, paper_licence_certificate.image_oid)::public.blobtype AS "row";


--
-- Name: people_website_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE people_website_upd AS ON UPDATE TO "10_View/People/Website_View" DO INSTEAD (UPDATE public.person SET website_staff_category_id = new.website_staff_category_id WHERE (person.id = old.id); UPDATE public.post_history SET job_title = new.job_title WHERE (post_history.id = (SELECT ph.id FROM ((public.person p JOIN cache._latest_role lr ON ((p.id = lr.person_id))) JOIN public.post_history ph ON ((lr.role_id = ph.id))) WHERE (p.id = old.id))); );


--
-- Name: post_history_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE post_history_ins AS ON INSERT TO "10_View/Roles/Post_History" DO INSTEAD (UPDATE public.person SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes WHERE (person.id = new.person_id); INSERT INTO public.post_history (staff_category_id, person_id, supervisor_id, mentor_id, external_mentor, start_date, intended_end_date, end_date, funding_end_date, chem, paid_by_university, hours_worked, percentage_of_fulltime_hours, probation_period, date_of_first_probation_meeting, date_of_second_probation_meeting, date_of_third_probation_meeting, date_of_fourth_probation_meeting, first_probation_meeting_comments, second_probation_meeting_comments, third_probation_meeting_comments, fourth_probation_meeting_comments, probation_outcome, date_probation_letters_sent, research_grant_number, filemaker_funding, job_title, force_role_status_to_past) VALUES (new.staff_category_id, new.person_id, new.supervisor_id, new.mentor_id, new.external_mentor, new.start_date, new.intended_end_date, new.end_date, new.funding_end_date, new.chem, new.paid_through_payroll, new.hours_worked, new.percentage_of_fulltime_hours, new.probation_period, new.date_of_first_probation_meeting, new.date_of_second_probation_meeting, new.date_of_third_probation_meeting, new.date_of_fourth_probation_meeting, new.first_probation_meeting_comments, new.second_probation_meeting_comments, new.third_probation_meeting_comments, new.fourth_probation_meeting_comments, new.probation_outcome, new.date_probation_letters_sent, new.research_grant_number, new.sponsor, new.job_title, new.force_role_status_to_past) RETURNING post_history.id, post_history.staff_category_id, post_history.person_id, NULL::character varying(10) AS "varchar", post_history.supervisor_id, post_history.mentor_id, post_history.external_mentor, NULL::date AS date, post_history.start_date, post_history.intended_end_date, post_history.end_date, post_history.funding_end_date, post_history.chem, post_history.paid_by_university, post_history.hours_worked, post_history.percentage_of_fulltime_hours, post_history.research_grant_number, post_history.filemaker_funding, post_history.probation_period, post_history.date_of_first_probation_meeting, post_history.date_of_second_probation_meeting, post_history.date_of_third_probation_meeting, post_history.date_of_fourth_probation_meeting, post_history.first_probation_meeting_comments, post_history.second_probation_meeting_comments, post_history.third_probation_meeting_comments, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, post_history.date_probation_letters_sent, post_history.job_title, post_history.force_role_status_to_past, NULL::text AS text; );


--
-- Name: switch_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE switch_del AS ON DELETE TO "10_View/Network/Switch_With_Rooms" DO INSTEAD DELETE FROM public.switch WHERE (switch.id = old.id);


--
-- Name: switch_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE switch_ins AS ON INSERT TO "10_View/Network/Switch_With_Rooms" DO INSTEAD SELECT public.hw_fn_switch_upd(new.*) AS id;


--
-- Name: switch_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE switch_upd AS ON UPDATE TO "10_View/Network/Switch_With_Rooms" DO INSTEAD (UPDATE public.hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.physical_room_id, owner_id = new.owner_id, comments = new.comments WHERE (hardware.id = old._hardware_id); UPDATE public.system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1 WHERE (system_image.id = old._system_image_id); );


--
-- Name: upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE upd AS ON UPDATE TO "10_View/Network/Switch Config Fragments" DO INSTEAD (UPDATE public.switch_config_fragment SET name = new.name, config = new.config, switch_config_goal_id = new.switch_config_goal_id WHERE (switch_config_fragment.id = old.id); SELECT public.fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_config_fragment_switch_model'::character varying, 'switch_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id) AS fn_mm_array_update; UPDATE public.switch_config_fragment SET id = switch_config_fragment.id WHERE false RETURNING new.id, new.name, new.config, new.switch_config_goal_id, new.switch_model_id; );


--
-- Name: update_supervisors; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE update_supervisors AS ON UPDATE TO "10_View/Roles/Bulk_update_supervisors" DO INSTEAD SELECT update_supervisors(new.*) AS id;


--
-- Name: wpkg_licence_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_del AS ON DELETE TO "30_WPKG/40_licence" DO INSTEAD DELETE FROM public.licence WHERE (licence.id = old.id);


--
-- Name: wpkg_licence_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_ins AS ON INSERT TO "30_WPKG/40_licence" DO INSTEAD SELECT public.fn_wpkg_licence_insupd(new.*) AS id;


--
-- Name: wpkg_licence_instance_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_instance_del AS ON DELETE TO "30_WPKG/15_licence_instance" DO INSTEAD DELETE FROM public.licence_instance WHERE (licence_instance.id = old.id);


--
-- Name: wpkg_licence_instance_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_instance_ins AS ON INSERT TO "30_WPKG/15_licence_instance" DO INSTEAD SELECT public.fn_wpkg_licence_instance_insupd(new.*) AS id;


--
-- Name: wpkg_licence_instance_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_instance_upd AS ON UPDATE TO "30_WPKG/15_licence_instance" DO INSTEAD SELECT public.fn_wpkg_licence_instance_insupd(new.*) AS id;


--
-- Name: wpkg_licence_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_licence_upd AS ON UPDATE TO "30_WPKG/40_licence" DO INSTEAD SELECT public.fn_wpkg_licence_insupd(new.*) AS id;


--
-- Name: wpkg_software_del; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_software_del AS ON DELETE TO "30_WPKG/10_software" DO INSTEAD DELETE FROM public.software_package WHERE (software_package.id = old.id);


--
-- Name: wpkg_software_ins; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_software_ins AS ON INSERT TO "30_WPKG/10_software" DO INSTEAD SELECT public.fn_software_package_insupd(new.*) AS id;


--
-- Name: wpkg_software_upd; Type: RULE; Schema: hotwire; Owner: dev
--

CREATE RULE wpkg_software_upd AS ON UPDATE TO "30_WPKG/10_software" DO INSTEAD SELECT public.fn_software_package_insupd(new.*) AS id;


--
-- Name: hw_rg_software_licence_decl_trigger; Type: TRIGGER; Schema: hotwire; Owner: dev
--

CREATE TRIGGER hw_rg_software_licence_decl_trigger INSTEAD OF UPDATE ON "10_View/Research_Groups/Software_Licence_Declaration" FOR EACH ROW EXECUTE PROCEDURE public.group_software_licence_decl_trig();


--
-- Name: hw_pref_type_fkey; Type: FK CONSTRAINT; Schema: hotwire; Owner: postgres
--

ALTER TABLE ONLY "hw_Preferences"
    ADD CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id) REFERENCES hw_preference_type_hid(hw_preference_type_id);


--
-- Name: hotwire; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA hotwire FROM PUBLIC;
REVOKE ALL ON SCHEMA hotwire FROM postgres;
GRANT ALL ON SCHEMA hotwire TO postgres;
GRANT USAGE ON SCHEMA hotwire TO PUBLIC;
GRANT ALL ON SCHEMA hotwire TO dev;
GRANT USAGE ON SCHEMA hotwire TO _pgbackup;


--
-- Name: 10_View/Safety/Fire_warden_areas; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Safety/Fire_warden_areas" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Safety/Fire_warden_areas" FROM dev;
GRANT ALL ON TABLE "10_View/Safety/Fire_warden_areas" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Fire_warden_areas" TO safety_management;
GRANT SELECT ON TABLE "10_View/Safety/Fire_warden_areas" TO mgmt_ro;


--
-- Name: 10_View/Safety/Fire_wardens; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Safety/Fire_wardens" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Safety/Fire_wardens" FROM dev;
GRANT ALL ON TABLE "10_View/Safety/Fire_wardens" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Fire_wardens" TO safety_management;
GRANT SELECT ON TABLE "10_View/Safety/Fire_wardens" TO mgmt_ro;


--
-- Name: 10_View/People/Personnel_Data_Entry; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Personnel_Data_Entry" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Personnel_Data_Entry" FROM dev;
GRANT ALL ON TABLE "10_View/People/Personnel_Data_Entry" TO dev;
GRANT SELECT ON TABLE "10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT,UPDATE ON TABLE "10_View/People/Personnel_Data_Entry" TO tkd25;


--
-- Name: 10_View/Roles/Bulk_update_supervisors; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Bulk_update_supervisors" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Bulk_update_supervisors" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Bulk_update_supervisors" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/Roles/Bulk_update_supervisors" TO hr;


--
-- Name: 30_WPKG/10_software; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_WPKG/10_software" FROM PUBLIC;
REVOKE ALL ON TABLE "30_WPKG/10_software" FROM dev;
GRANT ALL ON TABLE "30_WPKG/10_software" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "30_WPKG/10_software" TO cos;


--
-- Name: 30_WPKG/15_licence_instance; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_WPKG/15_licence_instance" FROM PUBLIC;
REVOKE ALL ON TABLE "30_WPKG/15_licence_instance" FROM dev;
GRANT ALL ON TABLE "30_WPKG/15_licence_instance" TO dev;
GRANT ALL ON TABLE "30_WPKG/15_licence_instance" TO postgres;
GRANT ALL ON TABLE "30_WPKG/15_licence_instance" TO cos;


--
-- Name: 30_WPKG/40_licence; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_WPKG/40_licence" FROM PUBLIC;
REVOKE ALL ON TABLE "30_WPKG/40_licence" FROM dev;
GRANT ALL ON TABLE "30_WPKG/40_licence" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "30_WPKG/40_licence" TO cos;


--
-- Name: 10_View/Dom0_DomU_Mapping; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Dom0_DomU_Mapping" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Dom0_DomU_Mapping" FROM postgres;
GRANT ALL ON TABLE "10_View/Dom0_DomU_Mapping" TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Dom0_DomU_Mapping" TO _api_dom0domu;


--
-- Name: 10_View/10_IT_Tasks/IT_Projects; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/IT_Projects" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/IT_Projects" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/IT_Projects" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/IT_Projects" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/10_IT_Tasks/IT_Projects" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/IT_Projects" TO groupitreps;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/IT_Projects" TO it_committee;


--
-- Name: person_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE person_hid FROM PUBLIC;
REVOKE ALL ON TABLE person_hid FROM postgres;
GRANT ALL ON TABLE person_hid TO postgres;
GRANT ALL ON TABLE person_hid TO ro_hid;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE person_hid TO cos;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE person_hid TO dev;
GRANT SELECT ON TABLE person_hid TO groupitreps;
GRANT SELECT ON TABLE person_hid TO ad_accounts;


--
-- Name: computer_officer_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE computer_officer_hid FROM PUBLIC;
REVOKE ALL ON TABLE computer_officer_hid FROM dev;
GRANT ALL ON TABLE computer_officer_hid TO dev;
GRANT SELECT ON TABLE computer_officer_hid TO ro_hid;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE computer_officer_hid TO cos;
GRANT SELECT ON TABLE computer_officer_hid TO groupitreps;


--
-- Name: 10_View/10_IT_Tasks/Tasks; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Tasks" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Tasks" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Tasks" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Tasks" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Tasks" TO groupitreps;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Tasks" TO it_committee;


--
-- Name: 10_View/People/Personnel_Basic; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Personnel_Basic" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Personnel_Basic" FROM dev;
GRANT ALL ON TABLE "10_View/People/Personnel_Basic" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/People/Personnel_Basic" TO cos;
GRANT SELECT ON TABLE "10_View/People/Personnel_Basic" TO phonebook_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/People/Personnel_Basic" TO hr;


--
-- Name: 10_View/People/Photography_Registration; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Photography_Registration" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Photography_Registration" FROM dev;
GRANT ALL ON TABLE "10_View/People/Photography_Registration" TO dev;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/People/Photography_Registration" TO photography;


--
-- Name: 10_View/Research_Group_Editing; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Research_Group_Editing" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Group_Editing" FROM dev;
GRANT ALL ON TABLE "10_View/Research_Group_Editing" TO dev;
GRANT SELECT ON TABLE "10_View/Research_Group_Editing" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Research_Group_Editing" TO hr;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Research_Group_Editing" TO interviewtest;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Group_Editing" TO chematcam;


--
-- Name: 10_View/Research_Groups_Computing; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Research_Groups_Computing" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Groups_Computing" FROM dev;
GRANT ALL ON TABLE "10_View/Research_Groups_Computing" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Research_Groups_Computing" TO cos;


--
-- Name: 10_View/Research_Interest_Group; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Research_Interest_Group" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Interest_Group" FROM dev;
GRANT ALL ON TABLE "10_View/Research_Interest_Group" TO dev;
GRANT ALL ON TABLE "10_View/Research_Interest_Group" TO cen1001;
GRANT SELECT ON TABLE "10_View/Research_Interest_Group" TO mgmt_ro;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Interest_Group" TO rig_management;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Interest_Group" TO interviewtest;


--
-- Name: 10_View/Socket; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Socket" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Socket" FROM cen1001;
GRANT ALL ON TABLE "10_View/Socket" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Socket" TO cos;


--
-- Name: 10_View/Network/Switch_With_Rooms; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_With_Rooms" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_With_Rooms" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_With_Rooms" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Network/Switch_With_Rooms" TO cos;


--
-- Name: 10_View/System_Image_All; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/System_Image_All" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/System_Image_All" FROM dev;
GRANT ALL ON TABLE "10_View/System_Image_All" TO dev;
GRANT ALL ON TABLE "10_View/System_Image_All" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/System_Image_All" TO cos;


--
-- Name: 10_View/System_Image_Asset; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/System_Image_Asset" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/System_Image_Asset" FROM dev;
GRANT ALL ON TABLE "10_View/System_Image_Asset" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/System_Image_Asset" TO cos;


--
-- Name: 10_View/System_Image_User; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/System_Image_User" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/System_Image_User" FROM dev;
GRANT ALL ON TABLE "10_View/System_Image_User" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/System_Image_User" TO cos;


--
-- Name: 10_View/Telephone_Basic; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Telephone_Basic" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Telephone_Basic" FROM dev;
GRANT ALL ON TABLE "10_View/Telephone_Basic" TO dev;
GRANT SELECT ON TABLE "10_View/Telephone_Basic" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Telephone_Basic" TO cos;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Telephone_Basic" TO phones_management;


--
-- Name: 10_View/Telephone_Management; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Telephone_Management" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Telephone_Management" FROM cen1001;
GRANT ALL ON TABLE "10_View/Telephone_Management" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Telephone_Management" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Telephone_Management" TO phones_management;


--
-- Name: 10_View/Easy_Add_Virtual_Machine; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Easy_Add_Virtual_Machine" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Easy_Add_Virtual_Machine" FROM dev;
GRANT ALL ON TABLE "10_View/Easy_Add_Virtual_Machine" TO dev;
GRANT ALL ON TABLE "10_View/Easy_Add_Virtual_Machine" TO cos;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//01_header; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//01_header" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//01_header" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//01_header" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//09_hdr; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" FROM postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//09_hdr" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//10_projects; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//10_projects" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//19_hdr; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" FROM postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//19_hdr" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//20_projects; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//20_projects" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//29_hdr; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" FROM postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//29_hdr" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//30_projects; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//30_projects" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//89_hdr; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" FROM postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO postgres;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//89_hdr" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Report_for_School_IT//90_completed; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO cos;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/Report_for_School_IT//90_completed" TO it_committee;


--
-- Name: 10_View/10_IT_Tasks/Strategic Goals; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Strategic Goals" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/Strategic Goals" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/Strategic Goals" TO dev;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE "10_View/10_IT_Tasks/Strategic Goals" TO cos;


--
-- Name: 10_View/10_IT_Tasks/_audit_ro; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/10_IT_Tasks/_audit_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/10_IT_Tasks/_audit_ro" FROM dev;
GRANT ALL ON TABLE "10_View/10_IT_Tasks/_audit_ro" TO dev;
GRANT SELECT ON TABLE "10_View/10_IT_Tasks/_audit_ro" TO cos;


--
-- Name: 10_View/Building_Floors; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Building_Floors" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Building_Floors" FROM cen1001;
GRANT ALL ON TABLE "10_View/Building_Floors" TO cen1001;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Building_Floors" TO space_management;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Building_Floors" TO dev;


--
-- Name: 10_View/Building_Regions; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Building_Regions" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Building_Regions" FROM cen1001;
GRANT ALL ON TABLE "10_View/Building_Regions" TO cen1001;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Building_Regions" TO space_management;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Building_Regions" TO dev;


--
-- Name: 10_View/Buildings; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Buildings" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Buildings" FROM cen1001;
GRANT ALL ON TABLE "10_View/Buildings" TO cen1001;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Buildings" TO space_management;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Buildings" TO dev;


--
-- Name: 10_View/CPGS_Submission; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/CPGS_Submission" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/CPGS_Submission" FROM cen1001;
GRANT ALL ON TABLE "10_View/CPGS_Submission" TO cen1001;
GRANT ALL ON TABLE "10_View/CPGS_Submission" TO dev;
GRANT SELECT ON TABLE "10_View/CPGS_Submission" TO mgmt_ro;
GRANT SELECT ON TABLE "10_View/CPGS_Submission" TO accounts;
GRANT SELECT,UPDATE ON TABLE "10_View/CPGS_Submission" TO student_management;


--
-- Name: 10_View/Computers/System_Image/_Contact_Details_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Computers/System_Image/_Contact_Details_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Computers/System_Image/_Contact_Details_ro" FROM cen1001;
GRANT ALL ON TABLE "10_View/Computers/System_Image/_Contact_Details_ro" TO cen1001;
GRANT ALL ON TABLE "10_View/Computers/System_Image/_Contact_Details_ro" TO dev;
GRANT SELECT ON TABLE "10_View/Computers/System_Image/_Contact_Details_ro" TO cos;


--
-- Name: 10_View/Computers/System_Image/_Hardware_ro; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Computers/System_Image/_Hardware_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Computers/System_Image/_Hardware_ro" FROM dev;
GRANT ALL ON TABLE "10_View/Computers/System_Image/_Hardware_ro" TO dev;
GRANT ALL ON TABLE "10_View/Computers/System_Image/_Hardware_ro" TO cen1001;
GRANT SELECT ON TABLE "10_View/Computers/System_Image/_Hardware_ro" TO cos;


--
-- Name: 10_View/Computers/System_Image/_IP_address_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Computers/System_Image/_IP_address_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Computers/System_Image/_IP_address_ro" FROM cen1001;
GRANT ALL ON TABLE "10_View/Computers/System_Image/_IP_address_ro" TO cen1001;
GRANT SELECT ON TABLE "10_View/Computers/System_Image/_IP_address_ro" TO cos;


--
-- Name: 10_View/DNS/ANames; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/DNS/ANames" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/DNS/ANames" FROM cen1001;
GRANT ALL ON TABLE "10_View/DNS/ANames" TO cen1001;
GRANT ALL ON TABLE "10_View/DNS/ANames" TO dev;


--
-- Name: 10_View/DNS/CNames; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/DNS/CNames" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/DNS/CNames" FROM postgres;
GRANT ALL ON TABLE "10_View/DNS/CNames" TO postgres;
GRANT ALL ON TABLE "10_View/DNS/CNames" TO cen1001;
GRANT ALL ON TABLE "10_View/DNS/CNames" TO cos;


--
-- Name: 10_View/DNS/MX_Records; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/DNS/MX_Records" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/DNS/MX_Records" FROM cen1001;
GRANT ALL ON TABLE "10_View/DNS/MX_Records" TO cen1001;
GRANT ALL ON TABLE "10_View/DNS/MX_Records" TO dev;


--
-- Name: 10_View/Database/Access_Control_Groups; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Database/Access_Control_Groups" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Database/Access_Control_Groups" FROM dev;
GRANT ALL ON TABLE "10_View/Database/Access_Control_Groups" TO dev;
GRANT SELECT ON TABLE "10_View/Database/Access_Control_Groups" TO hr;
GRANT SELECT ON TABLE "10_View/Database/Access_Control_Groups" TO cos;


--
-- Name: 10_View/Database/Access_Control_Lists; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Database/Access_Control_Lists" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Database/Access_Control_Lists" FROM dev;
GRANT ALL ON TABLE "10_View/Database/Access_Control_Lists" TO dev;
GRANT SELECT ON TABLE "10_View/Database/Access_Control_Lists" TO hr;
GRANT SELECT ON TABLE "10_View/Database/Access_Control_Lists" TO cos;


--
-- Name: 10_View/Database/Database_Users; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Database/Database_Users" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Database/Database_Users" FROM dev;
GRANT ALL ON TABLE "10_View/Database/Database_Users" TO dev;
GRANT SELECT ON TABLE "10_View/Database/Database_Users" TO cos;
GRANT SELECT ON TABLE "10_View/Database/Database_Users" TO hr;


--
-- Name: easy_addable_subnet_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE easy_addable_subnet_hid FROM PUBLIC;
REVOKE ALL ON TABLE easy_addable_subnet_hid FROM dev;
GRANT ALL ON TABLE easy_addable_subnet_hid TO dev;
GRANT SELECT ON TABLE easy_addable_subnet_hid TO ro_hid;


--
-- Name: 10_View/Easy_Add_Machine; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Easy_Add_Machine" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Easy_Add_Machine" FROM dev;
GRANT ALL ON TABLE "10_View/Easy_Add_Machine" TO dev;
GRANT ALL ON TABLE "10_View/Easy_Add_Machine" TO cos;


--
-- Name: 10_View/Easy_Add_Switch; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Easy_Add_Switch" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Easy_Add_Switch" FROM dev;
GRANT ALL ON TABLE "10_View/Easy_Add_Switch" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/Easy_Add_Switch" TO cos;


--
-- Name: 10_View/Edit_Hardware_Types; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Edit_Hardware_Types" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Edit_Hardware_Types" FROM cen1001;
GRANT ALL ON TABLE "10_View/Edit_Hardware_Types" TO cen1001;


--
-- Name: 10_View/Edit_List_of_Titles; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Edit_List_of_Titles" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Edit_List_of_Titles" FROM cen1001;
GRANT ALL ON TABLE "10_View/Edit_List_of_Titles" TO cen1001;
GRANT SELECT,INSERT ON TABLE "10_View/Edit_List_of_Titles" TO hr;
GRANT SELECT,INSERT ON TABLE "10_View/Edit_List_of_Titles" TO student_management;
GRANT SELECT,INSERT ON TABLE "10_View/Edit_List_of_Titles" TO dev;


--
-- Name: 10_View/Edit_Operating_Systems; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Edit_Operating_Systems" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Edit_Operating_Systems" FROM cen1001;
GRANT ALL ON TABLE "10_View/Edit_Operating_Systems" TO cen1001;


--
-- Name: 10_View/Group_Computers; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Group_Computers" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Group_Computers" FROM dev;
GRANT ALL ON TABLE "10_View/Group_Computers" TO dev;
GRANT SELECT,DELETE,UPDATE ON TABLE "10_View/Group_Computers" TO groupitreps;


--
-- Name: 10_View/Group_Computers_Autoinstaller; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Group_Computers_Autoinstaller" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Group_Computers_Autoinstaller" FROM dev;
GRANT ALL ON TABLE "10_View/Group_Computers_Autoinstaller" TO dev;
GRANT SELECT,DELETE,UPDATE ON TABLE "10_View/Group_Computers_Autoinstaller" TO groupitreps;


--
-- Name: 10_View/Group_Fileservers; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Group_Fileservers" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Group_Fileservers" FROM cen1001;
GRANT ALL ON TABLE "10_View/Group_Fileservers" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Group_Fileservers" TO cos;


--
-- Name: 10_View/Hardware_Basic; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Hardware_Basic" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Hardware_Basic" FROM cen1001;
GRANT ALL ON TABLE "10_View/Hardware_Basic" TO cen1001;


--
-- Name: 10_View/Hardware_Full; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Hardware_Full" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Hardware_Full" FROM cen1001;
GRANT ALL ON TABLE "10_View/Hardware_Full" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Hardware_Full" TO cos;


--
-- Name: 10_View/Hobbit/Clients_Cfg; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Hobbit/Clients_Cfg" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Hobbit/Clients_Cfg" FROM dev;
GRANT ALL ON TABLE "10_View/Hobbit/Clients_Cfg" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Hobbit/Clients_Cfg" TO cos;


--
-- Name: 10_View/Hotwire_Test; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Hotwire_Test" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Hotwire_Test" FROM postgres;
GRANT ALL ON TABLE "10_View/Hotwire_Test" TO postgres;


--
-- Name: 10_View/IP_address; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/IP_address" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/IP_address" FROM dev;
GRANT ALL ON TABLE "10_View/IP_address" TO dev;
GRANT SELECT ON TABLE "10_View/IP_address" TO dns;
GRANT SELECT ON TABLE "10_View/IP_address" TO osbuilder;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/IP_address" TO cos;


--
-- Name: 10_View/IT_Sales/Sales; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/IT_Sales/Sales" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/IT_Sales/Sales" FROM dev;
GRANT ALL ON TABLE "10_View/IT_Sales/Sales" TO dev;
GRANT ALL ON TABLE "10_View/IT_Sales/Sales" TO cos;


--
-- Name: 10_View/Mailing_Lists/Chemistry_Mail_Domain; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Mailing_Lists/Chemistry_Mail_Domain" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Mailing_Lists/Chemistry_Mail_Domain" FROM cen1001;
GRANT ALL ON TABLE "10_View/Mailing_Lists/Chemistry_Mail_Domain" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Mailing_Lists/Chemistry_Mail_Domain" TO cos;


--
-- Name: 10_View/Mailing_Lists/OptIns; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Mailing_Lists/OptIns" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Mailing_Lists/OptIns" FROM cen1001;
GRANT ALL ON TABLE "10_View/Mailing_Lists/OptIns" TO cen1001;
GRANT SELECT,UPDATE ON TABLE "10_View/Mailing_Lists/OptIns" TO cos;


--
-- Name: 10_View/Mailing_Lists/OptOuts; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Mailing_Lists/OptOuts" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Mailing_Lists/OptOuts" FROM cen1001;
GRANT ALL ON TABLE "10_View/Mailing_Lists/OptOuts" TO cen1001;
GRANT SELECT,UPDATE ON TABLE "10_View/Mailing_Lists/OptOuts" TO cos;


--
-- Name: 10_View/Network/Cabinet; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Cabinet" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Cabinet" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Cabinet" TO dev;
GRANT ALL ON TABLE "10_View/Network/Cabinet" TO cos;


--
-- Name: 10_View/Network/Fibre_Panel; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Fibre_Panel" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Fibre_Panel" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Fibre_Panel" TO dev;
GRANT ALL ON TABLE "10_View/Network/Fibre_Panel" TO cos;


--
-- Name: 10_View/Network/Fibres/10_Fibre_run; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Network/Fibres/10_Fibre_run" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Fibres/10_Fibre_run" FROM postgres;
GRANT ALL ON TABLE "10_View/Network/Fibres/10_Fibre_run" TO postgres;
GRANT ALL ON TABLE "10_View/Network/Fibres/10_Fibre_run" TO cos;


--
-- Name: 10_View/Network/Fibres/15_Fibre_Core; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Network/Fibres/15_Fibre_Core" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Fibres/15_Fibre_Core" FROM postgres;
GRANT ALL ON TABLE "10_View/Network/Fibres/15_Fibre_Core" TO postgres;
GRANT ALL ON TABLE "10_View/Network/Fibres/15_Fibre_Core" TO cos;


--
-- Name: fibre_core_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_core_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_core_type_hid FROM postgres;
GRANT ALL ON TABLE fibre_core_type_hid TO postgres;
GRANT SELECT ON TABLE fibre_core_type_hid TO PUBLIC;


--
-- Name: fibre_panel_a_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_panel_a_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_panel_a_hid FROM postgres;
GRANT ALL ON TABLE fibre_panel_a_hid TO postgres;
GRANT SELECT ON TABLE fibre_panel_a_hid TO PUBLIC;


--
-- Name: fibre_panel_b_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_panel_b_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_panel_b_hid FROM postgres;
GRANT ALL ON TABLE fibre_panel_b_hid TO postgres;
GRANT SELECT ON TABLE fibre_panel_b_hid TO PUBLIC;


--
-- Name: fibre_core_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_core_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_core_hid FROM postgres;
GRANT ALL ON TABLE fibre_core_hid TO postgres;
GRANT SELECT ON TABLE fibre_core_hid TO PUBLIC;


--
-- Name: 10_View/Network/Fibres/_FibreRuns_ro; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Fibres/_FibreRuns_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Fibres/_FibreRuns_ro" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Fibres/_FibreRuns_ro" TO dev;
GRANT SELECT ON TABLE "10_View/Network/Fibres/_FibreRuns_ro" TO cos;


--
-- Name: 10_View/Network/MAC_to_VLAN; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/MAC_to_VLAN" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/MAC_to_VLAN" FROM dev;
GRANT ALL ON TABLE "10_View/Network/MAC_to_VLAN" TO dev;
GRANT ALL ON TABLE "10_View/Network/MAC_to_VLAN" TO cos;


--
-- Name: 10_View/Network/Subnet; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Subnet" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Subnet" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Subnet" TO dev;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Network/Subnet" TO cos;


--
-- Name: 10_View/Network/Switch; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Network/Switch" TO cos;


--
-- Name: 10_View/Network/Switch Config Fragments; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch Config Fragments" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch Config Fragments" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch Config Fragments" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch Config Fragments" TO cos;


--
-- Name: 10_View/Network/Switch Models; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Network/Switch Models" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch Models" FROM postgres;
GRANT ALL ON TABLE "10_View/Network/Switch Models" TO postgres;
GRANT ALL ON TABLE "10_View/Network/Switch Models" TO cos;


--
-- Name: 10_View/Network/Switch_Config_Goals; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_Config_Goals" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_Config_Goals" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Config_Goals" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Config_Goals" TO cos;


--
-- Name: 10_View/Network/Switch_Configs_ro; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_Configs_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_Configs_ro" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Configs_ro" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Configs_ro" TO cos;


--
-- Name: 10_View/Network/Switch_Port; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_Port" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_Port" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port" TO cos;


--
-- Name: 10_View/Network/Switch_Port_Config_Fragments; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_Port_Config_Fragments" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_Port_Config_Fragments" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port_Config_Fragments" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port_Config_Fragments" TO cos;
GRANT ALL ON TABLE "10_View/Network/Switch_Port_Config_Fragments" TO postgres;


--
-- Name: 10_View/Network/Switch_Port_Config_Goals; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/Switch_Port_Config_Goals" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/Switch_Port_Config_Goals" FROM dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port_Config_Goals" TO dev;
GRANT ALL ON TABLE "10_View/Network/Switch_Port_Config_Goals" TO cos;


--
-- Name: 10_View/Network/VLAN; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/VLAN" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/VLAN" FROM dev;
GRANT ALL ON TABLE "10_View/Network/VLAN" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Network/VLAN" TO cos;


--
-- Name: speed_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE speed_hid FROM PUBLIC;
REVOKE ALL ON TABLE speed_hid FROM dev;
GRANT ALL ON TABLE speed_hid TO dev;
GRANT SELECT ON TABLE speed_hid TO ro_hid;


--
-- Name: switch_auth_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE switch_auth_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_auth_hid FROM dev;
GRANT ALL ON TABLE switch_auth_hid TO dev;
GRANT SELECT ON TABLE switch_auth_hid TO ro_hid;


--
-- Name: 10_View/Network/_Switchport_ro; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/_Switchport_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/_Switchport_ro" FROM dev;
GRANT ALL ON TABLE "10_View/Network/_Switchport_ro" TO dev;
GRANT SELECT ON TABLE "10_View/Network/_Switchport_ro" TO cos;


--
-- Name: 10_View/Network/_switch_config_subview; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Network/_switch_config_subview" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/_switch_config_subview" FROM dev;
GRANT ALL ON TABLE "10_View/Network/_switch_config_subview" TO dev;
GRANT SELECT ON TABLE "10_View/Network/_switch_config_subview" TO cos;


--
-- Name: 10_View/Network/_vlanchange_ro; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/Network/_vlanchange_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Network/_vlanchange_ro" FROM postgres;
GRANT ALL ON TABLE "10_View/Network/_vlanchange_ro" TO postgres;
GRANT SELECT ON TABLE "10_View/Network/_vlanchange_ro" TO cos;


--
-- Name: 10_View/Patch_Panel; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Patch_Panel" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Patch_Panel" FROM dev;
GRANT ALL ON TABLE "10_View/Patch_Panel" TO dev;


--
-- Name: 10_View/People/All_Contact_Details; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/All_Contact_Details" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/All_Contact_Details" FROM dev;
GRANT ALL ON TABLE "10_View/People/All_Contact_Details" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/People/All_Contact_Details" TO reception;


--
-- Name: 10_View/People/Personnel_History; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Personnel_History" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Personnel_History" FROM dev;
GRANT ALL ON TABLE "10_View/People/Personnel_History" TO dev;
GRANT ALL ON TABLE "10_View/People/Personnel_History" TO cen1001;
GRANT SELECT ON TABLE "10_View/People/Personnel_History" TO mgmt_ro;
GRANT SELECT,UPDATE ON TABLE "10_View/People/Personnel_History" TO hr;
GRANT SELECT ON TABLE "10_View/People/Personnel_History" TO personnel_history_ro;
GRANT SELECT ON TABLE "10_View/People/Personnel_History" TO accounts;


--
-- Name: 10_View/People/Research_Group_Members; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/People/Research_Group_Members" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Research_Group_Members" FROM cen1001;
GRANT ALL ON TABLE "10_View/People/Research_Group_Members" TO cen1001;
GRANT ALL ON TABLE "10_View/People/Research_Group_Members" TO dev;
GRANT SELECT ON TABLE "10_View/People/Research_Group_Members" TO mgmt_ro;
GRANT SELECT ON TABLE "10_View/People/Research_Group_Members" TO hr;
GRANT SELECT ON TABLE "10_View/People/Research_Group_Members" TO reception;
GRANT SELECT ON TABLE "10_View/People/Research_Group_Members" TO student_management;
GRANT SELECT ON TABLE "10_View/People/Research_Group_Members" TO www_sites;


--
-- Name: 10_View/People/Room_Occupants; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/People/Room_Occupants" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Room_Occupants" FROM cen1001;
GRANT ALL ON TABLE "10_View/People/Room_Occupants" TO cen1001;
GRANT SELECT ON TABLE "10_View/People/Room_Occupants" TO dev;
GRANT SELECT ON TABLE "10_View/People/Room_Occupants" TO space_management;
GRANT SELECT ON TABLE "10_View/People/Room_Occupants" TO mgmt_ro;


--
-- Name: 10_View/People/Staff_Review_History; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Staff_Review_History" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Staff_Review_History" FROM dev;
GRANT ALL ON TABLE "10_View/People/Staff_Review_History" TO dev;
GRANT ALL ON TABLE "10_View/People/Staff_Review_History" TO cen1001;
GRANT SELECT ON TABLE "10_View/People/Staff_Review_History" TO hr;
GRANT SELECT ON TABLE "10_View/People/Staff_Review_History" TO personnel_history_ro;


--
-- Name: 10_View/People/Swipe_Cards; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "10_View/People/Swipe_Cards" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Swipe_Cards" FROM postgres;
GRANT ALL ON TABLE "10_View/People/Swipe_Cards" TO postgres;
GRANT SELECT ON TABLE "10_View/People/Swipe_Cards" TO hr;
GRANT SELECT ON TABLE "10_View/People/Swipe_Cards" TO accounts;
GRANT SELECT ON TABLE "10_View/People/Swipe_Cards" TO smb28;
GRANT SELECT ON TABLE "10_View/People/Swipe_Cards" TO dev;


--
-- Name: 10_View/People/Website_View; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/People/Website_View" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/People/Website_View" FROM dev;
GRANT ALL ON TABLE "10_View/People/Website_View" TO dev;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/People/Website_View" TO web_editors;


--
-- Name: 10_View/Postgrad_Mentor_Meetings; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Postgrad_Mentor_Meetings" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Postgrad_Mentor_Meetings" FROM cen1001;
GRANT ALL ON TABLE "10_View/Postgrad_Mentor_Meetings" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Postgrad_Mentor_Meetings" TO student_management;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Postgrad_Mentor_Meetings" TO dev;
GRANT SELECT ON TABLE "10_View/Postgrad_Mentor_Meetings" TO mgmt_ro;


--
-- Name: 10_View/Postgrad_Training; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Postgrad_Training" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Postgrad_Training" FROM cen1001;
GRANT ALL ON TABLE "10_View/Postgrad_Training" TO cen1001;
GRANT ALL ON TABLE "10_View/Postgrad_Training" TO dev;
GRANT SELECT ON TABLE "10_View/Postgrad_Training" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Postgrad_Training" TO student_management;
GRANT SELECT ON TABLE "10_View/Postgrad_Training" TO student_management_ro;


--
-- Name: 10_View/Printers; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Printers" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Printers" FROM cen1001;
GRANT ALL ON TABLE "10_View/Printers" TO cen1001;


--
-- Name: 10_View/Registration_and_Leaving; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Registration_and_Leaving" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Registration_and_Leaving" FROM cen1001;
GRANT ALL ON TABLE "10_View/Registration_and_Leaving" TO cen1001;
GRANT ALL ON TABLE "10_View/Registration_and_Leaving" TO dev;
GRANT SELECT ON TABLE "10_View/Registration_and_Leaving" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Registration_and_Leaving" TO hr;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Registration_and_Leaving" TO reception;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Registration_and_Leaving" TO student_management;


--
-- Name: 10_View/Research_Group_Test_Editing; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Research_Group_Test_Editing" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Group_Test_Editing" FROM cen1001;
GRANT ALL ON TABLE "10_View/Research_Group_Test_Editing" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Research_Group_Test_Editing" TO interviewtest;


--
-- Name: 10_View/Research_Groups/Computer_reps; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Research_Groups/Computer_reps" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Groups/Computer_reps" FROM dev;
GRANT ALL ON TABLE "10_View/Research_Groups/Computer_reps" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Groups/Computer_reps" TO groupitreps;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Groups/Computer_reps" TO headsofgroup;


--
-- Name: 10_View/Research_Groups/Software_Licence_Declaration; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Research_Groups/Software_Licence_Declaration" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Groups/Software_Licence_Declaration" FROM dev;
GRANT ALL ON TABLE "10_View/Research_Groups/Software_Licence_Declaration" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Groups/Software_Licence_Declaration" TO headsofgroup;
GRANT SELECT,UPDATE ON TABLE "10_View/Research_Groups/Software_Licence_Declaration" TO cos;


--
-- Name: 10_View/Research_Groups_Computing_Computers; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Research_Groups_Computing_Computers" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Research_Groups_Computing_Computers" FROM cen1001;
GRANT ALL ON TABLE "10_View/Research_Groups_Computing_Computers" TO cen1001;
GRANT SELECT ON TABLE "10_View/Research_Groups_Computing_Computers" TO cos;


--
-- Name: 10_View/Roles/Erasmus_Students; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Erasmus_Students" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Erasmus_Students" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Erasmus_Students" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Roles/Erasmus_Students" TO student_management;
GRANT ALL ON TABLE "10_View/Roles/Erasmus_Students" TO cen1001;


--
-- Name: 10_View/Roles/Part_III_Students; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Part_III_Students" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Part_III_Students" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Part_III_Students" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Roles/Part_III_Students" TO student_management;
GRANT SELECT ON TABLE "10_View/Roles/Part_III_Students" TO mgmt_ro;
GRANT ALL ON TABLE "10_View/Roles/Part_III_Students" TO cen1001;


--
-- Name: 10_View/Roles/Post_History; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Post_History" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Post_History" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Post_History" TO dev;
GRANT SELECT ON TABLE "10_View/Roles/Post_History" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Roles/Post_History" TO hr;


--
-- Name: 10_View/Roles/Postgrad_Students; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Postgrad_Students" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Postgrad_Students" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE "10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE "10_View/Roles/Postgrad_Students" TO student_management_ro;


--
-- Name: 10_View/Roles/Visitors; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/Visitors" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/Visitors" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/Visitors" TO dev;
GRANT SELECT ON TABLE "10_View/Roles/Visitors" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Roles/Visitors" TO hr;


--
-- Name: 10_View/Roles/_Cambridge_History_V2; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/_Cambridge_History_V2" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/_Cambridge_History_V2" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/_Cambridge_History_V2" TO dev;
GRANT ALL ON TABLE "10_View/Roles/_Cambridge_History_V2" TO cen1001;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO mgmt_ro;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO hr;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO reception;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO accounts;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO student_management;
GRANT SELECT ON TABLE "10_View/Roles/_Cambridge_History_V2" TO personnel_history_ro;


--
-- Name: 10_View/Roles/_current_people_supervised_by; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Roles/_current_people_supervised_by" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Roles/_current_people_supervised_by" FROM dev;
GRANT ALL ON TABLE "10_View/Roles/_current_people_supervised_by" TO dev;
GRANT SELECT ON TABLE "10_View/Roles/_current_people_supervised_by" TO hr;


--
-- Name: 10_View/Rooms/Room_Attributes; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Rooms/Room_Attributes" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Rooms/Room_Attributes" FROM cen1001;
GRANT ALL ON TABLE "10_View/Rooms/Room_Attributes" TO cen1001;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Attributes" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Rooms/Room_Attributes" TO space_management;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Rooms/Room_Attributes" TO dev;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Attributes" TO space_viewers;


--
-- Name: 10_View/Rooms/Room_Occupancy; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Rooms/Room_Occupancy" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Rooms/Room_Occupancy" FROM cen1001;
GRANT ALL ON TABLE "10_View/Rooms/Room_Occupancy" TO cen1001;
GRANT ALL ON TABLE "10_View/Rooms/Room_Occupancy" TO dev;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Occupancy" TO mgmt_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Rooms/Room_Occupancy" TO space_management;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Occupancy" TO space_viewers;


--
-- Name: 10_View/Rooms/Room_Occupants; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Rooms/Room_Occupants" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Rooms/Room_Occupants" FROM dev;
GRANT ALL ON TABLE "10_View/Rooms/Room_Occupants" TO dev;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Occupants" TO mgmt_ro;
GRANT SELECT,UPDATE ON TABLE "10_View/Rooms/Room_Occupants" TO space_management;
GRANT SELECT ON TABLE "10_View/Rooms/Room_Occupants" TO space_viewers;


--
-- Name: building_floor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE building_floor_hid FROM PUBLIC;
REVOKE ALL ON TABLE building_floor_hid FROM postgres;
GRANT ALL ON TABLE building_floor_hid TO postgres;
GRANT ALL ON TABLE building_floor_hid TO ro_hid;
GRANT ALL ON TABLE building_floor_hid TO dev;
GRANT SELECT ON TABLE building_floor_hid TO space_viewers;


--
-- Name: building_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE building_hid FROM PUBLIC;
REVOKE ALL ON TABLE building_hid FROM postgres;
GRANT ALL ON TABLE building_hid TO postgres;
GRANT ALL ON TABLE building_hid TO ro_hid;
GRANT ALL ON TABLE building_hid TO dev;
GRANT SELECT ON TABLE building_hid TO space_viewers;


--
-- Name: 10_View/Safety/Firstaiders; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Safety/Firstaiders" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Safety/Firstaiders" FROM dev;
GRANT ALL ON TABLE "10_View/Safety/Firstaiders" TO dev;
GRANT ALL ON TABLE "10_View/Safety/Firstaiders" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Firstaiders" TO safety_management;
GRANT SELECT ON TABLE "10_View/Safety/Firstaiders" TO www_sites;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Firstaiders" TO firstaiders;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Safety/Firstaiders" TO hr;


--
-- Name: 10_View/Safety/Fume_Hoods; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Safety/Fume_Hoods" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Safety/Fume_Hoods" FROM cen1001;
GRANT ALL ON TABLE "10_View/Safety/Fume_Hoods" TO cen1001;
GRANT SELECT ON TABLE "10_View/Safety/Fume_Hoods" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Fume_Hoods" TO safety_management;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Safety/Fume_Hoods" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/Safety/Fume_Hoods" TO space_management;


--
-- Name: 10_View/Staff_Reviews; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Staff_Reviews" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Staff_Reviews" FROM dev;
GRANT ALL ON TABLE "10_View/Staff_Reviews" TO dev;
GRANT ALL ON TABLE "10_View/Staff_Reviews" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Staff_Reviews" TO hr;
GRANT SELECT ON TABLE "10_View/Staff_Reviews" TO mgmt_ro;


--
-- Name: 10_View/Switchport; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/Switchport" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Switchport" FROM cen1001;
GRANT ALL ON TABLE "10_View/Switchport" TO cen1001;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/Switchport" TO cos;


--
-- Name: 10_View/System_Image_Autoinstaller; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/System_Image_Autoinstaller" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/System_Image_Autoinstaller" FROM dev;
GRANT ALL ON TABLE "10_View/System_Image_Autoinstaller" TO dev;
GRANT SELECT,UPDATE ON TABLE "10_View/System_Image_Autoinstaller" TO cos;


--
-- Name: 10_View/System_Image_Basic; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/System_Image_Basic" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/System_Image_Basic" FROM dev;
GRANT ALL ON TABLE "10_View/System_Image_Basic" TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "10_View/System_Image_Basic" TO cos;


--
-- Name: 10_View/Systems/ChemNet; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "10_View/Systems/ChemNet" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/Systems/ChemNet" FROM dev;
GRANT ALL ON TABLE "10_View/Systems/ChemNet" TO dev;
GRANT SELECT,INSERT,UPDATE ON TABLE "10_View/Systems/ChemNet" TO cos;


--
-- Name: 10_View/User_Account; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "10_View/User_Account" FROM PUBLIC;
REVOKE ALL ON TABLE "10_View/User_Account" FROM cen1001;
GRANT ALL ON TABLE "10_View/User_Account" TO cen1001;
GRANT ALL ON TABLE "10_View/User_Account" TO dev;
GRANT SELECT ON TABLE "10_View/User_Account" TO mgmt_ro;


--
-- Name: 30_Report/All_Group_Computers; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "30_Report/All_Group_Computers" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/All_Group_Computers" FROM postgres;
GRANT ALL ON TABLE "30_Report/All_Group_Computers" TO postgres;
GRANT ALL ON TABLE "30_Report/All_Group_Computers" TO groupitreps;


--
-- Name: 30_Report/ChemAtCam/OptIns; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/ChemAtCam/OptIns" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/ChemAtCam/OptIns" FROM dev;
GRANT ALL ON TABLE "30_Report/ChemAtCam/OptIns" TO dev;
GRANT SELECT ON TABLE "30_Report/ChemAtCam/OptIns" TO hr;
GRANT SELECT ON TABLE "30_Report/ChemAtCam/OptIns" TO chematcam;


--
-- Name: 30_Report/Current_People_by_Sector_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Current_People_by_Sector_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Current_People_by_Sector_ro" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Current_People_by_Sector_ro" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Current_People_by_Sector_ro" TO dev;
GRANT SELECT ON TABLE "30_Report/Current_People_by_Sector_ro" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Current_People_by_Sector_ro" TO hr;
GRANT SELECT ON TABLE "30_Report/Current_People_by_Sector_ro" TO reception;
GRANT SELECT ON TABLE "30_Report/Current_People_by_Sector_ro" TO phones;


--
-- Name: 30_Report/Current_Postdocs; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Current_Postdocs" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Current_Postdocs" FROM dev;
GRANT ALL ON TABLE "30_Report/Current_Postdocs" TO dev;
GRANT SELECT ON TABLE "30_Report/Current_Postdocs" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Current_Postdocs" TO hr;
GRANT ALL ON TABLE "30_Report/Current_Postdocs" TO cen1001;


--
-- Name: 30_Report/Current_Postgrad_Summary_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Current_Postgrad_Summary_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Current_Postgrad_Summary_ro" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Current_Postgrad_Summary_ro" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Current_Postgrad_Summary_ro" TO dev;
GRANT SELECT ON TABLE "30_Report/Current_Postgrad_Summary_ro" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Current_Postgrad_Summary_ro" TO student_management;


--
-- Name: 30_Report/Female_Researchers; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Female_Researchers" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Female_Researchers" FROM dev;
GRANT ALL ON TABLE "30_Report/Female_Researchers" TO dev;
GRANT ALL ON TABLE "30_Report/Female_Researchers" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Female_Researchers" TO hr;
GRANT SELECT ON TABLE "30_Report/Female_Researchers" TO mgmt_ro;


--
-- Name: 30_Report/Funding; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Funding" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Funding" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Funding" TO cen1001;
GRANT ALL ON TABLE "30_Report/Funding" TO dev;
GRANT SELECT ON TABLE "30_Report/Funding" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Funding" TO student_management;
GRANT SELECT ON TABLE "30_Report/Funding" TO hr;


--
-- Name: 30_Report/HRNJ_Postgrad_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/HRNJ_Postgrad_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/HRNJ_Postgrad_ro" FROM cen1001;
GRANT ALL ON TABLE "30_Report/HRNJ_Postgrad_ro" TO cen1001;
GRANT SELECT ON TABLE "30_Report/HRNJ_Postgrad_ro" TO dev;
GRANT SELECT ON TABLE "30_Report/HRNJ_Postgrad_ro" TO mgmt_ro;


--
-- Name: 30_Report/IPs_without_Hardware_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/IPs_without_Hardware_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/IPs_without_Hardware_ro" FROM cen1001;
GRANT ALL ON TABLE "30_Report/IPs_without_Hardware_ro" TO cen1001;


--
-- Name: 30_Report/IPs_without_Hostnames_ro; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/IPs_without_Hostnames_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/IPs_without_Hostnames_ro" FROM cen1001;
GRANT ALL ON TABLE "30_Report/IPs_without_Hostnames_ro" TO cen1001;


--
-- Name: 30_Report/Inconsistent_Status; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Inconsistent_Status" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Inconsistent_Status" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Inconsistent_Status" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Inconsistent_Status" TO dev;
GRANT SELECT ON TABLE "30_Report/Inconsistent_Status" TO hr;
GRANT SELECT ON TABLE "30_Report/Inconsistent_Status" TO student_management;
GRANT SELECT ON TABLE "30_Report/Inconsistent_Status" TO mgmt_ro;


--
-- Name: 30_Report/Leavers; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Leavers" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Leavers" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Leavers" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Leavers" TO dev;
GRANT SELECT ON TABLE "30_Report/Leavers" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Leavers" TO hr;
GRANT SELECT ON TABLE "30_Report/Leavers" TO reception;


--
-- Name: 30_Report/PDRA_Status; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/PDRA_Status" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/PDRA_Status" FROM cen1001;
GRANT ALL ON TABLE "30_Report/PDRA_Status" TO cen1001;
GRANT SELECT ON TABLE "30_Report/PDRA_Status" TO hr;


--
-- Name: 30_Report/People_Without_Roles; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/People_Without_Roles" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/People_Without_Roles" FROM dev;
GRANT ALL ON TABLE "30_Report/People_Without_Roles" TO dev;
GRANT SELECT ON TABLE "30_Report/People_Without_Roles" TO mgmt_ro;
GRANT ALL ON TABLE "30_Report/People_Without_Roles" TO cen1001;


--
-- Name: 30_Report/PhD_Submission; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/PhD_Submission" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/PhD_Submission" FROM cen1001;
GRANT ALL ON TABLE "30_Report/PhD_Submission" TO cen1001;
GRANT ALL ON TABLE "30_Report/PhD_Submission" TO dev;
GRANT SELECT ON TABLE "30_Report/PhD_Submission" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/PhD_Submission" TO student_management;
GRANT SELECT ON TABLE "30_Report/PhD_Submission" TO accounts;


--
-- Name: 30_Report/Phone_Directory; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Phone_Directory" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Phone_Directory" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Phone_Directory" TO cen1001;
GRANT ALL ON TABLE "30_Report/Phone_Directory" TO dev;
GRANT SELECT ON TABLE "30_Report/Phone_Directory" TO reception;
GRANT SELECT ON TABLE "30_Report/Phone_Directory" TO hr;
GRANT SELECT ON TABLE "30_Report/Phone_Directory" TO phones;
GRANT SELECT ON TABLE "30_Report/Phone_Directory" TO mgmt_ro;


--
-- Name: 30_Report/Postdoc_Mentors; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Postdoc_Mentors" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Postdoc_Mentors" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Postdoc_Mentors" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Postdoc_Mentors" TO hr;
GRANT SELECT ON TABLE "30_Report/Postdoc_Mentors" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Postdoc_Mentors" TO dev;


--
-- Name: 30_Report/Postgrad_Mentors; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Postgrad_Mentors" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Postgrad_Mentors" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Postgrad_Mentors" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Postgrad_Mentors" TO hr;
GRANT SELECT ON TABLE "30_Report/Postgrad_Mentors" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Postgrad_Mentors" TO dev;


--
-- Name: 30_Report/Postgrad_Personnel; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Postgrad_Personnel" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Postgrad_Personnel" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Postgrad_Personnel" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Postgrad_Personnel" TO student_management;
GRANT SELECT ON TABLE "30_Report/Postgrad_Personnel" TO dev;


--
-- Name: 30_Report/Postgrad_Students_with_DOB; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Postgrad_Students_with_DOB" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Postgrad_Students_with_DOB" FROM dev;
GRANT ALL ON TABLE "30_Report/Postgrad_Students_with_DOB" TO dev;
GRANT SELECT ON TABLE "30_Report/Postgrad_Students_with_DOB" TO student_management;


--
-- Name: 30_Report/Principal_Investigators_with_Students; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Principal_Investigators_with_Students" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Principal_Investigators_with_Students" FROM dev;
GRANT ALL ON TABLE "30_Report/Principal_Investigators_with_Students" TO dev;
GRANT ALL ON TABLE "30_Report/Principal_Investigators_with_Students" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Principal_Investigators_with_Students" TO hr;
GRANT SELECT ON TABLE "30_Report/Principal_Investigators_with_Students" TO mgmt_ro;


--
-- Name: 30_Report/REF_2021; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/REF_2021" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/REF_2021" FROM dev;
GRANT ALL ON TABLE "30_Report/REF_2021" TO dev;
GRANT SELECT ON TABLE "30_Report/REF_2021" TO ref2021;


--
-- Name: 30_Report/Retirement_Dates; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Retirement_Dates" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Retirement_Dates" FROM dev;
GRANT ALL ON TABLE "30_Report/Retirement_Dates" TO dev;
GRANT ALL ON TABLE "30_Report/Retirement_Dates" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Retirement_Dates" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Retirement_Dates" TO hr;


--
-- Name: 30_Report/Rooms_by_RIG; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Rooms_by_RIG" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Rooms_by_RIG" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Rooms_by_RIG" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Rooms_by_RIG" TO space_management;


--
-- Name: 30_Report/Rooms_by_Research_Group; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Rooms_by_Research_Group" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Rooms_by_Research_Group" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Rooms_by_Research_Group" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Rooms_by_Research_Group" TO space_management;
GRANT SELECT ON TABLE "30_Report/Rooms_by_Research_Group" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Rooms_by_Research_Group" TO dev;


--
-- Name: 30_Report/Searching; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Searching" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Searching" FROM dev;
GRANT ALL ON TABLE "30_Report/Searching" TO dev;
GRANT SELECT ON TABLE "30_Report/Searching" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Searching" TO hr;
GRANT ALL ON TABLE "30_Report/Searching" TO cen1001;


--
-- Name: 30_Report/Short_Searching; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Short_Searching" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Short_Searching" FROM dev;
GRANT ALL ON TABLE "30_Report/Short_Searching" TO dev;
GRANT ALL ON TABLE "30_Report/Short_Searching" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Short_Searching" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Short_Searching" TO hr;


--
-- Name: 30_Report/Source_of_Funds; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Source_of_Funds" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Source_of_Funds" FROM dev;
GRANT ALL ON TABLE "30_Report/Source_of_Funds" TO dev;
GRANT ALL ON TABLE "30_Report/Source_of_Funds" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Source_of_Funds" TO mgmt_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "30_Report/Source_of_Funds" TO student_management;


--
-- Name: 30_Report/Space_used_by_Research_Group; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Space_used_by_Research_Group" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Space_used_by_Research_Group" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Space_used_by_Research_Group" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Space_used_by_Research_Group" TO space_management;
GRANT SELECT ON TABLE "30_Report/Space_used_by_Research_Group" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Space_used_by_Research_Group" TO dev;


--
-- Name: 30_Report/Staff_Personnel; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Staff_Personnel" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Staff_Personnel" FROM dev;
GRANT ALL ON TABLE "30_Report/Staff_Personnel" TO dev;
GRANT ALL ON TABLE "30_Report/Staff_Personnel" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Staff_Personnel" TO hr;


--
-- Name: 30_Report/Staff_Review_and_Development; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Staff_Review_and_Development" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Staff_Review_and_Development" FROM dev;
GRANT ALL ON TABLE "30_Report/Staff_Review_and_Development" TO dev;
GRANT ALL ON TABLE "30_Report/Staff_Review_and_Development" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Staff_Review_and_Development" TO hr;


--
-- Name: 30_Report/Staff_Review_and_Development_Historical; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "30_Report/Staff_Review_and_Development_Historical" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Staff_Review_and_Development_Historical" FROM dev;
GRANT ALL ON TABLE "30_Report/Staff_Review_and_Development_Historical" TO dev;
GRANT ALL ON TABLE "30_Report/Staff_Review_and_Development_Historical" TO cen1001;
GRANT SELECT ON TABLE "30_Report/Staff_Review_and_Development_Historical" TO hr;


--
-- Name: 30_Report/Submission_Time_Taken; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE "30_Report/Submission_Time_Taken" FROM PUBLIC;
REVOKE ALL ON TABLE "30_Report/Submission_Time_Taken" FROM cen1001;
GRANT ALL ON TABLE "30_Report/Submission_Time_Taken" TO cen1001;
GRANT ALL ON TABLE "30_Report/Submission_Time_Taken" TO dev;
GRANT SELECT ON TABLE "30_Report/Submission_Time_Taken" TO mgmt_ro;
GRANT SELECT ON TABLE "30_Report/Submission_Time_Taken" TO student_management;


--
-- Name: 30_WPKG/25_paper_licence_certs; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "30_WPKG/25_paper_licence_certs" FROM PUBLIC;
REVOKE ALL ON TABLE "30_WPKG/25_paper_licence_certs" FROM postgres;
GRANT ALL ON TABLE "30_WPKG/25_paper_licence_certs" TO postgres;
GRANT ALL ON TABLE "30_WPKG/25_paper_licence_certs" TO cos;


--
-- Name: 30_WPKG/50_Host_to_package_ro; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "30_WPKG/50_Host_to_package_ro" FROM PUBLIC;
REVOKE ALL ON TABLE "30_WPKG/50_Host_to_package_ro" FROM postgres;
GRANT ALL ON TABLE "30_WPKG/50_Host_to_package_ro" TO postgres;
GRANT ALL ON TABLE "30_WPKG/50_Host_to_package_ro" TO cos;


--
-- Name: hw_user_pref_seq; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON SEQUENCE hw_user_pref_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE hw_user_pref_seq FROM postgres;
GRANT ALL ON SEQUENCE hw_user_pref_seq TO postgres;
GRANT ALL ON SEQUENCE hw_user_pref_seq TO dev;


--
-- Name: hw_User Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "hw_User Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "hw_User Preferences" FROM postgres;
GRANT ALL ON TABLE "hw_User Preferences" TO postgres;
GRANT ALL ON TABLE "hw_User Preferences" TO dev;


--
-- Name: 90_Action/Hotwire/User Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "90_Action/Hotwire/User Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "90_Action/Hotwire/User Preferences" FROM postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/User Preferences" TO postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/User Preferences" TO cos;


--
-- Name: IT_status_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "IT_status_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "IT_status_hid" FROM dev;
GRANT ALL ON TABLE "IT_status_hid" TO dev;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE "IT_status_hid" TO cos;
GRANT SELECT ON TABLE "IT_status_hid" TO groupitreps;


--
-- Name: IT_strategic_goal_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "IT_strategic_goal_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "IT_strategic_goal_hid" FROM dev;
GRANT ALL ON TABLE "IT_strategic_goal_hid" TO dev;
GRANT SELECT ON TABLE "IT_strategic_goal_hid" TO ro_hid;
GRANT SELECT ON TABLE "IT_strategic_goal_hid" TO groupitreps;


--
-- Name: IT_task_leader_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "IT_task_leader_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "IT_task_leader_hid" FROM dev;
GRANT ALL ON TABLE "IT_task_leader_hid" TO dev;
GRANT SELECT ON TABLE "IT_task_leader_hid" TO ro_hid;
GRANT SELECT ON TABLE "IT_task_leader_hid" TO groupitreps;


--
-- Name: IT_task_status_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "IT_task_status_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "IT_task_status_hid" FROM dev;
GRANT ALL ON TABLE "IT_task_status_hid" TO dev;
GRANT SELECT ON TABLE "IT_task_status_hid" TO ro_hid;
GRANT SELECT ON TABLE "IT_task_status_hid" TO groupitreps;


--
-- Name: Postgres_User_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Postgres_User_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "Postgres_User_hid" FROM postgres;
GRANT ALL ON TABLE "Postgres_User_hid" TO postgres;
GRANT ALL ON TABLE "Postgres_User_hid" TO ro_hid;


--
-- Name: Primary_strategic_goal_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE "Primary_strategic_goal_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "Primary_strategic_goal_hid" FROM dev;
GRANT ALL ON TABLE "Primary_strategic_goal_hid" TO dev;
GRANT SELECT ON TABLE "Primary_strategic_goal_hid" TO ro_hid;
GRANT SELECT ON TABLE "Primary_strategic_goal_hid" TO groupitreps;


--
-- Name: SelfService/Licences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "SelfService/Licences" FROM PUBLIC;
REVOKE ALL ON TABLE "SelfService/Licences" FROM postgres;
GRANT ALL ON TABLE "SelfService/Licences" TO postgres;
GRANT SELECT ON TABLE "SelfService/Licences" TO old_selfservice;


--
-- Name: _column_data; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE _column_data FROM PUBLIC;
REVOKE ALL ON TABLE _column_data FROM dev;
GRANT ALL ON TABLE _column_data TO dev;
GRANT SELECT ON TABLE _column_data TO PUBLIC;


--
-- Name: hw_pref_seq; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON SEQUENCE hw_pref_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE hw_pref_seq FROM dev;
GRANT ALL ON SEQUENCE hw_pref_seq TO dev;


--
-- Name: hw_Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "hw_Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "hw_Preferences" FROM postgres;
GRANT ALL ON TABLE "hw_Preferences" TO postgres;
GRANT ALL ON TABLE "hw_Preferences" TO dev;


--
-- Name: hw_preference_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hw_preference_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hw_preference_type_hid FROM postgres;
GRANT ALL ON TABLE hw_preference_type_hid TO postgres;
GRANT SELECT ON TABLE hw_preference_type_hid TO dev;


--
-- Name: _preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _preferences FROM PUBLIC;
REVOKE ALL ON TABLE _preferences FROM postgres;
GRANT ALL ON TABLE _preferences TO postgres;
GRANT SELECT ON TABLE _preferences TO PUBLIC;


--
-- Name: _role_data; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE _role_data FROM PUBLIC;
REVOKE ALL ON TABLE _role_data FROM dev;
GRANT ALL ON TABLE _role_data TO dev;
GRANT ALL ON TABLE _role_data TO PUBLIC;
GRANT ALL ON TABLE _role_data TO cos;


--
-- Name: _subviews; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE _subviews FROM PUBLIC;
REVOKE ALL ON TABLE _subviews FROM dev;
GRANT ALL ON TABLE _subviews TO dev;
GRANT SELECT ON TABLE _subviews TO PUBLIC;
GRANT SELECT ON TABLE _subviews TO mgmt_ro;
GRANT INSERT,UPDATE ON TABLE _subviews TO cen1001;


--
-- Name: _view_data; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _view_data FROM PUBLIC;
REVOKE ALL ON TABLE _view_data FROM postgres;
GRANT ALL ON TABLE _view_data TO postgres;
GRANT ALL ON TABLE _view_data TO dev;
GRANT SELECT ON TABLE _view_data TO PUBLIC;


--
-- Name: _view_data2; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _view_data2 FROM PUBLIC;
REVOKE ALL ON TABLE _view_data2 FROM postgres;
GRANT ALL ON TABLE _view_data2 TO postgres;
GRANT ALL ON TABLE _view_data2 TO dev;
GRANT SELECT ON TABLE _view_data2 TO PUBLIC;


--
-- Name: _view_data_new; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _view_data_new FROM PUBLIC;
REVOKE ALL ON TABLE _view_data_new FROM postgres;
GRANT ALL ON TABLE _view_data_new TO postgres;
GRANT ALL ON TABLE _view_data_new TO dev;
GRANT SELECT ON TABLE _view_data_new TO PUBLIC;


--
-- Name: academic_title_(optional)_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "academic_title_(optional)_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "academic_title_(optional)_hid" FROM postgres;
GRANT ALL ON TABLE "academic_title_(optional)_hid" TO postgres;
GRANT ALL ON TABLE "academic_title_(optional)_hid" TO ro_hid;


--
-- Name: all_cos_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE all_cos_hid FROM PUBLIC;
REVOKE ALL ON TABLE all_cos_hid FROM dev;
GRANT ALL ON TABLE all_cos_hid TO dev;
GRANT SELECT ON TABLE all_cos_hid TO ro_hid;


--
-- Name: licence_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE licence_hid FROM PUBLIC;
REVOKE ALL ON TABLE licence_hid FROM postgres;
GRANT ALL ON TABLE licence_hid TO postgres;
GRANT ALL ON TABLE licence_hid TO ro_hid;


--
-- Name: allowed_by_other_licence_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE allowed_by_other_licence_hid FROM PUBLIC;
REVOKE ALL ON TABLE allowed_by_other_licence_hid FROM postgres;
GRANT ALL ON TABLE allowed_by_other_licence_hid TO postgres;
GRANT SELECT ON TABLE allowed_by_other_licence_hid TO ro_hid;


--
-- Name: alternate_admin_contact_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE alternate_admin_contact_hid FROM PUBLIC;
REVOKE ALL ON TABLE alternate_admin_contact_hid FROM postgres;
GRANT ALL ON TABLE alternate_admin_contact_hid TO postgres;
GRANT ALL ON TABLE alternate_admin_contact_hid TO ro_hid;


--
-- Name: architecture_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE architecture_hid FROM PUBLIC;
REVOKE ALL ON TABLE architecture_hid FROM postgres;
GRANT ALL ON TABLE architecture_hid TO postgres;
GRANT ALL ON TABLE architecture_hid TO ro_hid;
GRANT SELECT ON TABLE architecture_hid TO groupitreps;


--
-- Name: backup_strategy_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE backup_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE backup_strategy_hid FROM postgres;
GRANT ALL ON TABLE backup_strategy_hid TO postgres;
GRANT ALL ON TABLE backup_strategy_hid TO ro_hid;


--
-- Name: building_region_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE building_region_hid FROM PUBLIC;
REVOKE ALL ON TABLE building_region_hid FROM postgres;
GRANT ALL ON TABLE building_region_hid TO postgres;
GRANT ALL ON TABLE building_region_hid TO ro_hid;
GRANT SELECT ON TABLE building_region_hid TO space_viewers;


--
-- Name: cabinet_a_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE cabinet_a_hid FROM PUBLIC;
REVOKE ALL ON TABLE cabinet_a_hid FROM postgres;
GRANT ALL ON TABLE cabinet_a_hid TO postgres;
GRANT SELECT ON TABLE cabinet_a_hid TO PUBLIC;


--
-- Name: cabinet_b_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE cabinet_b_hid FROM PUBLIC;
REVOKE ALL ON TABLE cabinet_b_hid FROM postgres;
GRANT ALL ON TABLE cabinet_b_hid TO postgres;
GRANT SELECT ON TABLE cabinet_b_hid TO PUBLIC;


--
-- Name: cabinet_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE cabinet_hid FROM PUBLIC;
REVOKE ALL ON TABLE cabinet_hid FROM postgres;
GRANT ALL ON TABLE cabinet_hid TO postgres;
GRANT ALL ON TABLE cabinet_hid TO ro_hid;


--
-- Name: cable_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE cable_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE cable_type_hid FROM postgres;
GRANT ALL ON TABLE cable_type_hid TO postgres;
GRANT ALL ON TABLE cable_type_hid TO ro_hid;


--
-- Name: cambridge_college_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE cambridge_college_hid FROM PUBLIC;
REVOKE ALL ON TABLE cambridge_college_hid FROM postgres;
GRANT ALL ON TABLE cambridge_college_hid TO postgres;
GRANT ALL ON TABLE cambridge_college_hid TO ro_hid;


--
-- Name: chair_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE chair_hid FROM PUBLIC;
REVOKE ALL ON TABLE chair_hid FROM cen1001;
GRANT ALL ON TABLE chair_hid TO cen1001;
GRANT SELECT ON TABLE chair_hid TO ro_hid;


--
-- Name: co_mentor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE co_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE co_mentor_hid FROM postgres;
GRANT ALL ON TABLE co_mentor_hid TO postgres;
GRANT ALL ON TABLE co_mentor_hid TO ro_hid;


--
-- Name: co_supervisor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE co_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE co_supervisor_hid FROM postgres;
GRANT ALL ON TABLE co_supervisor_hid TO postgres;
GRANT ALL ON TABLE co_supervisor_hid TO ro_hid;


--
-- Name: column_data; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE column_data FROM PUBLIC;
REVOKE ALL ON TABLE column_data FROM postgres;
GRANT ALL ON TABLE column_data TO postgres;
GRANT SELECT ON TABLE column_data TO PUBLIC;
GRANT ALL ON TABLE column_data TO ro_hid;


--
-- Name: computer_rep_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE computer_rep_hid FROM PUBLIC;
REVOKE ALL ON TABLE computer_rep_hid FROM dev;
GRANT ALL ON TABLE computer_rep_hid TO dev;
GRANT ALL ON TABLE computer_rep_hid TO ro_hid;
GRANT ALL ON TABLE computer_rep_hid TO postgres;
GRANT SELECT ON TABLE computer_rep_hid TO groupitreps;
GRANT SELECT ON TABLE computer_rep_hid TO headsofgroup;


--
-- Name: connector_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE connector_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE connector_type_hid FROM postgres;
GRANT ALL ON TABLE connector_type_hid TO postgres;
GRANT ALL ON TABLE connector_type_hid TO ro_hid;


--
-- Name: default_system_image_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE default_system_image_hid FROM PUBLIC;
REVOKE ALL ON TABLE default_system_image_hid FROM postgres;
GRANT ALL ON TABLE default_system_image_hid TO postgres;
GRANT SELECT ON TABLE default_system_image_hid TO ro_hid;


--
-- Name: software_package_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE software_package_hid FROM PUBLIC;
REVOKE ALL ON TABLE software_package_hid FROM postgres;
GRANT ALL ON TABLE software_package_hid TO postgres;
GRANT ALL ON TABLE software_package_hid TO ro_hid;


--
-- Name: depended_software_package_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE depended_software_package_hid FROM PUBLIC;
REVOKE ALL ON TABLE depended_software_package_hid FROM cen1001;
GRANT ALL ON TABLE depended_software_package_hid TO cen1001;
GRANT SELECT ON TABLE depended_software_package_hid TO ro_hid;


--
-- Name: dept_telephone_number_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE dept_telephone_number_hid FROM PUBLIC;
REVOKE ALL ON TABLE dept_telephone_number_hid FROM postgres;
GRANT ALL ON TABLE dept_telephone_number_hid TO postgres;
GRANT ALL ON TABLE dept_telephone_number_hid TO ro_hid;


--
-- Name: fire_warden_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE fire_warden_hid FROM PUBLIC;
REVOKE ALL ON TABLE fire_warden_hid FROM cen1001;
GRANT ALL ON TABLE fire_warden_hid TO cen1001;
GRANT SELECT ON TABLE fire_warden_hid TO PUBLIC;


--
-- Name: deputy_fire_warden_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE deputy_fire_warden_hid FROM PUBLIC;
REVOKE ALL ON TABLE deputy_fire_warden_hid FROM cen1001;
GRANT ALL ON TABLE deputy_fire_warden_hid TO cen1001;
GRANT SELECT ON TABLE deputy_fire_warden_hid TO PUBLIC;


--
-- Name: dns_domain_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE dns_domain_hid FROM PUBLIC;
REVOKE ALL ON TABLE dns_domain_hid FROM cen1001;
GRANT ALL ON TABLE dns_domain_hid TO cen1001;
GRANT SELECT ON TABLE dns_domain_hid TO ro_hid;


--
-- Name: dom0_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE dom0_hid FROM PUBLIC;
REVOKE ALL ON TABLE dom0_hid FROM postgres;
GRANT ALL ON TABLE dom0_hid TO postgres;
GRANT ALL ON TABLE dom0_hid TO ro_hid;


--
-- Name: domain_for_dns_server_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE domain_for_dns_server_hid FROM PUBLIC;
REVOKE ALL ON TABLE domain_for_dns_server_hid FROM cen1001;
GRANT ALL ON TABLE domain_for_dns_server_hid TO cen1001;
GRANT SELECT ON TABLE domain_for_dns_server_hid TO ro_hid;


--
-- Name: domu_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE domu_hid FROM PUBLIC;
REVOKE ALL ON TABLE domu_hid FROM postgres;
GRANT ALL ON TABLE domu_hid TO postgres;
GRANT ALL ON TABLE domu_hid TO ro_hid;


--
-- Name: erasmus_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE erasmus_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE erasmus_type_hid FROM postgres;
GRANT ALL ON TABLE erasmus_type_hid TO postgres;
GRANT ALL ON TABLE erasmus_type_hid TO ro_hid;


--
-- Name: exclude_person_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE exclude_person_hid FROM PUBLIC;
REVOKE ALL ON TABLE exclude_person_hid FROM postgres;
GRANT ALL ON TABLE exclude_person_hid TO postgres;
GRANT ALL ON TABLE exclude_person_hid TO ro_hid;


--
-- Name: fai_class_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fai_class_hid FROM PUBLIC;
REVOKE ALL ON TABLE fai_class_hid FROM postgres;
GRANT ALL ON TABLE fai_class_hid TO postgres;
GRANT ALL ON TABLE fai_class_hid TO ro_hid;
GRANT SELECT ON TABLE fai_class_hid TO groupitreps;


--
-- Name: fibre_termination_a_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_termination_a_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_termination_a_hid FROM postgres;
GRANT ALL ON TABLE fibre_termination_a_hid TO postgres;
GRANT SELECT ON TABLE fibre_termination_a_hid TO PUBLIC;


--
-- Name: fibre_termination_b_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_termination_b_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_termination_b_hid FROM postgres;
GRANT ALL ON TABLE fibre_termination_b_hid TO postgres;
GRANT SELECT ON TABLE fibre_termination_b_hid TO PUBLIC;


--
-- Name: fibre_run_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fibre_run_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_run_hid FROM postgres;
GRANT ALL ON TABLE fibre_run_hid TO postgres;
GRANT SELECT ON TABLE fibre_run_hid TO PUBLIC;


--
-- Name: fibre_termination_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE fibre_termination_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_termination_hid FROM dev;
GRANT ALL ON TABLE fibre_termination_hid TO dev;
GRANT ALL ON TABLE fibre_termination_hid TO cos;


--
-- Name: fibre_type_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE fibre_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE fibre_type_hid FROM dev;
GRANT ALL ON TABLE fibre_type_hid TO dev;
GRANT SELECT ON TABLE fibre_type_hid TO ro_hid;


--
-- Name: fire_warden_area_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE fire_warden_area_hid FROM PUBLIC;
REVOKE ALL ON TABLE fire_warden_area_hid FROM cen1001;
GRANT ALL ON TABLE fire_warden_area_hid TO cen1001;
GRANT SELECT ON TABLE fire_warden_area_hid TO PUBLIC;


--
-- Name: firetrace_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE firetrace_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE firetrace_type_hid FROM postgres;
GRANT ALL ON TABLE firetrace_type_hid TO postgres;
GRANT ALL ON TABLE firetrace_type_hid TO ro_hid;


--
-- Name: first_mentor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE first_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE first_mentor_hid FROM postgres;
GRANT ALL ON TABLE first_mentor_hid TO postgres;
GRANT ALL ON TABLE first_mentor_hid TO ro_hid;


--
-- Name: first_supervisor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE first_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE first_supervisor_hid FROM postgres;
GRANT ALL ON TABLE first_supervisor_hid TO postgres;
GRANT ALL ON TABLE first_supervisor_hid TO ro_hid;


--
-- Name: firstaider_funding_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE firstaider_funding_hid FROM PUBLIC;
REVOKE ALL ON TABLE firstaider_funding_hid FROM postgres;
GRANT ALL ON TABLE firstaider_funding_hid TO postgres;
GRANT SELECT ON TABLE firstaider_funding_hid TO ro_hid;


--
-- Name: fume_hood_device_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fume_hood_device_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE fume_hood_device_type_hid FROM postgres;
GRANT ALL ON TABLE fume_hood_device_type_hid TO postgres;
GRANT ALL ON TABLE fume_hood_device_type_hid TO ro_hid;


--
-- Name: fume_hood_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE fume_hood_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE fume_hood_type_hid FROM postgres;
GRANT ALL ON TABLE fume_hood_type_hid TO postgres;
GRANT ALL ON TABLE fume_hood_type_hid TO ro_hid;


--
-- Name: gender_(required)_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "gender_(required)_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "gender_(required)_hid" FROM postgres;
GRANT ALL ON TABLE "gender_(required)_hid" TO postgres;
GRANT ALL ON TABLE "gender_(required)_hid" TO ro_hid;


--
-- Name: gender_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE gender_hid FROM PUBLIC;
REVOKE ALL ON TABLE gender_hid FROM postgres;
GRANT ALL ON TABLE gender_hid TO postgres;
GRANT ALL ON TABLE gender_hid TO ro_hid;


--
-- Name: goal_class_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE goal_class_hid FROM PUBLIC;
REVOKE ALL ON TABLE goal_class_hid FROM postgres;
GRANT ALL ON TABLE goal_class_hid TO postgres;
GRANT SELECT ON TABLE goal_class_hid TO cos;


--
-- Name: group_fileserver_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE group_fileserver_hid FROM PUBLIC;
REVOKE ALL ON TABLE group_fileserver_hid FROM postgres;
GRANT ALL ON TABLE group_fileserver_hid TO postgres;
GRANT ALL ON TABLE group_fileserver_hid TO ro_hid;


--
-- Name: hardware_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hardware_hid FROM PUBLIC;
REVOKE ALL ON TABLE hardware_hid FROM postgres;
GRANT ALL ON TABLE hardware_hid TO postgres;
GRANT ALL ON TABLE hardware_hid TO ro_hid;
GRANT SELECT ON TABLE hardware_hid TO groupitreps;


--
-- Name: hardware_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hardware_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hardware_type_hid FROM postgres;
GRANT ALL ON TABLE hardware_type_hid TO postgres;
GRANT ALL ON TABLE hardware_type_hid TO ro_hid;
GRANT SELECT ON TABLE hardware_type_hid TO groupitreps;


--
-- Name: head_of_group_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE head_of_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE head_of_group_hid FROM dev;
GRANT ALL ON TABLE head_of_group_hid TO dev;
GRANT ALL ON TABLE head_of_group_hid TO ro_hid;
GRANT ALL ON TABLE head_of_group_hid TO postgres;
GRANT SELECT ON TABLE head_of_group_hid TO groupitreps;
GRANT SELECT ON TABLE head_of_group_hid TO headsofgroup;


--
-- Name: host_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE host_hid FROM PUBLIC;
REVOKE ALL ON TABLE host_hid FROM postgres;
GRANT ALL ON TABLE host_hid TO postgres;
GRANT ALL ON TABLE host_hid TO ro_hid;


--
-- Name: host_system_image_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE host_system_image_hid FROM PUBLIC;
REVOKE ALL ON TABLE host_system_image_hid FROM postgres;
GRANT ALL ON TABLE host_system_image_hid TO postgres;
GRANT ALL ON TABLE host_system_image_hid TO ro_hid;
GRANT SELECT ON TABLE host_system_image_hid TO groupitreps;


--
-- Name: include_person_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE include_person_hid FROM PUBLIC;
REVOKE ALL ON TABLE include_person_hid FROM postgres;
GRANT ALL ON TABLE include_person_hid TO postgres;
GRANT ALL ON TABLE include_person_hid TO ro_hid;


--
-- Name: infraction_type_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE infraction_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE infraction_type_hid FROM dev;
GRANT ALL ON TABLE infraction_type_hid TO dev;
GRANT SELECT ON TABLE infraction_type_hid TO ro_hid;


--
-- Name: installer_tag_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE installer_tag_hid FROM PUBLIC;
REVOKE ALL ON TABLE installer_tag_hid FROM postgres;
GRANT ALL ON TABLE installer_tag_hid TO postgres;
GRANT ALL ON TABLE installer_tag_hid TO ro_hid;
GRANT SELECT ON TABLE installer_tag_hid TO groupitreps;


--
-- Name: ip_address_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE ip_address_hid FROM PUBLIC;
REVOKE ALL ON TABLE ip_address_hid FROM postgres;
GRANT ALL ON TABLE ip_address_hid TO postgres;
GRANT ALL ON TABLE ip_address_hid TO ro_hid;
GRANT SELECT ON TABLE ip_address_hid TO groupitreps;


--
-- Name: ip_address_qid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE ip_address_qid FROM PUBLIC;
REVOKE ALL ON TABLE ip_address_qid FROM postgres;
GRANT ALL ON TABLE ip_address_qid TO postgres;
GRANT ALL ON TABLE ip_address_qid TO ro_hid;


--
-- Name: it_project_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE it_project_hid FROM PUBLIC;
REVOKE ALL ON TABLE it_project_hid FROM cen1001;
GRANT ALL ON TABLE it_project_hid TO cen1001;
GRANT SELECT ON TABLE it_project_hid TO ro_hid;


--
-- Name: it_strategic_goal_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE it_strategic_goal_hid FROM PUBLIC;
REVOKE ALL ON TABLE it_strategic_goal_hid FROM dev;
GRANT ALL ON TABLE it_strategic_goal_hid TO dev;
GRANT SELECT ON TABLE it_strategic_goal_hid TO ro_hid;
GRANT SELECT ON TABLE it_strategic_goal_hid TO groupitreps;


--
-- Name: it_task_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE it_task_hid FROM PUBLIC;
REVOKE ALL ON TABLE it_task_hid FROM postgres;
GRANT ALL ON TABLE it_task_hid TO postgres;
GRANT SELECT ON TABLE it_task_hid TO ro_hid;


--
-- Name: itsales_status_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE itsales_status_hid FROM PUBLIC;
REVOKE ALL ON TABLE itsales_status_hid FROM dev;
GRANT ALL ON TABLE itsales_status_hid TO dev;
GRANT SELECT ON TABLE itsales_status_hid TO PUBLIC;


--
-- Name: itsales_type_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE itsales_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE itsales_type_hid FROM dev;
GRANT ALL ON TABLE itsales_type_hid TO dev;
GRANT SELECT ON TABLE itsales_type_hid TO PUBLIC;


--
-- Name: itsales_unit_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE itsales_unit_hid FROM PUBLIC;
REVOKE ALL ON TABLE itsales_unit_hid FROM dev;
GRANT ALL ON TABLE itsales_unit_hid TO dev;
GRANT SELECT ON TABLE itsales_unit_hid TO PUBLIC;


--
-- Name: licence_downgraded_to_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE licence_downgraded_to_hid FROM PUBLIC;
REVOKE ALL ON TABLE licence_downgraded_to_hid FROM postgres;
GRANT ALL ON TABLE licence_downgraded_to_hid TO postgres;
GRANT SELECT ON TABLE licence_downgraded_to_hid TO ro_hid;


--
-- Name: linked_socket_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE linked_socket_hid FROM PUBLIC;
REVOKE ALL ON TABLE linked_socket_hid FROM postgres;
GRANT ALL ON TABLE linked_socket_hid TO postgres;
GRANT ALL ON TABLE linked_socket_hid TO ro_hid;


--
-- Name: manager_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE manager_hid FROM PUBLIC;
REVOKE ALL ON TABLE manager_hid FROM postgres;
GRANT ALL ON TABLE manager_hid TO postgres;
GRANT ALL ON TABLE manager_hid TO ro_hid;


--
-- Name: member_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE member_hid FROM PUBLIC;
REVOKE ALL ON TABLE member_hid FROM postgres;
GRANT ALL ON TABLE member_hid TO postgres;
GRANT ALL ON TABLE member_hid TO ro_hid;


--
-- Name: mentor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE mentor_hid FROM postgres;
GRANT ALL ON TABLE mentor_hid TO postgres;
GRANT ALL ON TABLE mentor_hid TO ro_hid;


--
-- Name: nationality_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE nationality_hid FROM PUBLIC;
REVOKE ALL ON TABLE nationality_hid FROM postgres;
GRANT ALL ON TABLE nationality_hid TO postgres;
GRANT ALL ON TABLE nationality_hid TO ro_hid;


--
-- Name: netboot_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE netboot_hid FROM PUBLIC;
REVOKE ALL ON TABLE netboot_hid FROM dev;
GRANT ALL ON TABLE netboot_hid TO dev;
GRANT ALL ON TABLE netboot_hid TO cos;
GRANT SELECT ON TABLE netboot_hid TO ro_hid;


--
-- Name: network_electrical_connection_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE network_electrical_connection_hid FROM PUBLIC;
REVOKE ALL ON TABLE network_electrical_connection_hid FROM dev;
GRANT ALL ON TABLE network_electrical_connection_hid TO dev;
GRANT SELECT ON TABLE network_electrical_connection_hid TO ro_hid;


--
-- Name: new_supervisor_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE new_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE new_supervisor_hid FROM dev;
GRANT ALL ON TABLE new_supervisor_hid TO dev;
GRANT SELECT ON TABLE new_supervisor_hid TO ro_hid;


--
-- Name: occupant_group_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE occupant_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE occupant_group_hid FROM postgres;
GRANT ALL ON TABLE occupant_group_hid TO postgres;
GRANT ALL ON TABLE occupant_group_hid TO ro_hid;


--
-- Name: occupants_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE occupants_hid FROM PUBLIC;
REVOKE ALL ON TABLE occupants_hid FROM postgres;
GRANT ALL ON TABLE occupants_hid TO postgres;
GRANT ALL ON TABLE occupants_hid TO ro_hid;


--
-- Name: old_supervisor_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE old_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE old_supervisor_hid FROM dev;
GRANT ALL ON TABLE old_supervisor_hid TO dev;
GRANT SELECT ON TABLE old_supervisor_hid TO ro_hid;


--
-- Name: operating_system_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE operating_system_hid FROM PUBLIC;
REVOKE ALL ON TABLE operating_system_hid FROM postgres;
GRANT ALL ON TABLE operating_system_hid TO postgres;
GRANT ALL ON TABLE operating_system_hid TO ro_hid;
GRANT SELECT ON TABLE operating_system_hid TO groupitreps;
GRANT ALL ON TABLE operating_system_hid TO dev;


--
-- Name: os_class_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE os_class_hid FROM PUBLIC;
REVOKE ALL ON TABLE os_class_hid FROM postgres;
GRANT ALL ON TABLE os_class_hid TO postgres;
GRANT ALL ON TABLE os_class_hid TO ro_hid;
GRANT SELECT ON TABLE os_class_hid TO osbuilder;
GRANT SELECT ON TABLE os_class_hid TO dev;


--
-- Name: owner_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE owner_hid FROM PUBLIC;
REVOKE ALL ON TABLE owner_hid FROM postgres;
GRANT ALL ON TABLE owner_hid TO postgres;
GRANT ALL ON TABLE owner_hid TO ro_hid;
GRANT SELECT ON TABLE owner_hid TO groupitreps;


--
-- Name: paper_licence_certificate_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE paper_licence_certificate_hid FROM PUBLIC;
REVOKE ALL ON TABLE paper_licence_certificate_hid FROM postgres;
GRANT ALL ON TABLE paper_licence_certificate_hid TO postgres;
GRANT ALL ON TABLE paper_licence_certificate_hid TO ro_hid;


--
-- Name: patch_panel_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE patch_panel_hid FROM PUBLIC;
REVOKE ALL ON TABLE patch_panel_hid FROM postgres;
GRANT ALL ON TABLE patch_panel_hid TO postgres;
GRANT ALL ON TABLE patch_panel_hid TO ro_hid;


--
-- Name: patch_panel_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE patch_panel_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE patch_panel_type_hid FROM postgres;
GRANT ALL ON TABLE patch_panel_type_hid TO postgres;
GRANT ALL ON TABLE patch_panel_type_hid TO ro_hid;


--
-- Name: patch_strategy_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE patch_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE patch_strategy_hid FROM postgres;
GRANT ALL ON TABLE patch_strategy_hid TO postgres;
GRANT ALL ON TABLE patch_strategy_hid TO ro_hid;


--
-- Name: physical_room_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE physical_room_hid FROM PUBLIC;
REVOKE ALL ON TABLE physical_room_hid FROM postgres;
GRANT ALL ON TABLE physical_room_hid TO postgres;
GRANT SELECT ON TABLE physical_room_hid TO ro_hid;


--
-- Name: physical_status_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE physical_status_hid FROM PUBLIC;
REVOKE ALL ON TABLE physical_status_hid FROM postgres;
GRANT ALL ON TABLE physical_status_hid TO postgres;
GRANT ALL ON TABLE physical_status_hid TO ro_hid;


--
-- Name: post_category_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE post_category_hid FROM PUBLIC;
REVOKE ALL ON TABLE post_category_hid FROM postgres;
GRANT ALL ON TABLE post_category_hid TO postgres;
GRANT ALL ON TABLE post_category_hid TO ro_hid;


--
-- Name: post_history_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE post_history_hid FROM PUBLIC;
REVOKE ALL ON TABLE post_history_hid FROM dev;
GRANT ALL ON TABLE post_history_hid TO dev;
GRANT ALL ON TABLE post_history_hid TO ro_hid;
GRANT ALL ON TABLE post_history_hid TO postgres;


--
-- Name: postgraduate_studentship_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE postgraduate_studentship_hid FROM PUBLIC;
REVOKE ALL ON TABLE postgraduate_studentship_hid FROM postgres;
GRANT ALL ON TABLE postgraduate_studentship_hid TO postgres;
GRANT ALL ON TABLE postgraduate_studentship_hid TO ro_hid;


--
-- Name: postgraduate_studentship_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE postgraduate_studentship_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE postgraduate_studentship_type_hid FROM postgres;
GRANT ALL ON TABLE postgraduate_studentship_type_hid TO postgres;
GRANT ALL ON TABLE postgraduate_studentship_type_hid TO ro_hid;


--
-- Name: postit_owner_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE postit_owner_hid FROM PUBLIC;
REVOKE ALL ON TABLE postit_owner_hid FROM postgres;
GRANT ALL ON TABLE postit_owner_hid TO postgres;
GRANT SELECT ON TABLE postit_owner_hid TO ro_hid;


--
-- Name: predecessor_task_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE predecessor_task_hid FROM PUBLIC;
REVOKE ALL ON TABLE predecessor_task_hid FROM dev;
GRANT ALL ON TABLE predecessor_task_hid TO dev;
GRANT SELECT ON TABLE predecessor_task_hid TO ro_hid;
GRANT SELECT ON TABLE predecessor_task_hid TO groupitreps;


--
-- Name: preference_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE preference_hid FROM PUBLIC;
REVOKE ALL ON TABLE preference_hid FROM postgres;
GRANT ALL ON TABLE preference_hid TO postgres;
GRANT ALL ON TABLE preference_hid TO ro_hid;


--
-- Name: rig_member_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE rig_member_hid FROM PUBLIC;
REVOKE ALL ON TABLE rig_member_hid FROM cen1001;
GRANT ALL ON TABLE rig_member_hid TO cen1001;
GRANT ALL ON TABLE rig_member_hid TO ro_hid;


--
-- Name: primary_member_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE primary_member_hid FROM PUBLIC;
REVOKE ALL ON TABLE primary_member_hid FROM cen1001;
GRANT ALL ON TABLE primary_member_hid TO cen1001;
GRANT SELECT ON TABLE primary_member_hid TO ro_hid;


--
-- Name: primary_office_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE primary_office_hid FROM PUBLIC;
REVOKE ALL ON TABLE primary_office_hid FROM postgres;
GRANT ALL ON TABLE primary_office_hid TO postgres;
GRANT ALL ON TABLE primary_office_hid TO ro_hid;


--
-- Name: printer_class_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE printer_class_hid FROM PUBLIC;
REVOKE ALL ON TABLE printer_class_hid FROM postgres;
GRANT ALL ON TABLE printer_class_hid TO postgres;
GRANT ALL ON TABLE printer_class_hid TO ro_hid;


--
-- Name: reboot_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE reboot_hid FROM PUBLIC;
REVOKE ALL ON TABLE reboot_hid FROM postgres;
GRANT ALL ON TABLE reboot_hid TO postgres;
GRANT ALL ON TABLE reboot_hid TO cos;
GRANT ALL ON TABLE reboot_hid TO ro_hid;


--
-- Name: recirc_or_ducted_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE recirc_or_ducted_hid FROM PUBLIC;
REVOKE ALL ON TABLE recirc_or_ducted_hid FROM postgres;
GRANT ALL ON TABLE recirc_or_ducted_hid TO postgres;
GRANT ALL ON TABLE recirc_or_ducted_hid TO ro_hid;


--
-- Name: research_group_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE research_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE research_group_hid FROM postgres;
GRANT ALL ON TABLE research_group_hid TO postgres;
GRANT ALL ON TABLE research_group_hid TO ro_hid;
GRANT SELECT ON TABLE research_group_hid TO groupitreps;
GRANT SELECT ON TABLE research_group_hid TO ad_accounts;


--
-- Name: research_interest_group_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE research_interest_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE research_interest_group_hid FROM postgres;
GRANT ALL ON TABLE research_interest_group_hid TO postgres;
GRANT ALL ON TABLE research_interest_group_hid TO ro_hid;


--
-- Name: responsible_group_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE responsible_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE responsible_group_hid FROM postgres;
GRANT ALL ON TABLE responsible_group_hid TO postgres;
GRANT ALL ON TABLE responsible_group_hid TO ro_hid;
GRANT SELECT ON TABLE responsible_group_hid TO space_viewers;


--
-- Name: responsible_person_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE responsible_person_hid FROM PUBLIC;
REVOKE ALL ON TABLE responsible_person_hid FROM postgres;
GRANT ALL ON TABLE responsible_person_hid TO postgres;
GRANT ALL ON TABLE responsible_person_hid TO ro_hid;
GRANT SELECT ON TABLE responsible_person_hid TO space_viewers;


--
-- Name: reviewer_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE reviewer_hid FROM PUBLIC;
REVOKE ALL ON TABLE reviewer_hid FROM postgres;
GRANT ALL ON TABLE reviewer_hid TO postgres;
GRANT ALL ON TABLE reviewer_hid TO ro_hid;


--
-- Name: role_status_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE role_status_hid FROM PUBLIC;
REVOKE ALL ON TABLE role_status_hid FROM postgres;
GRANT ALL ON TABLE role_status_hid TO postgres;
GRANT ALL ON TABLE role_status_hid TO ro_hid;


--
-- Name: room_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE room_hid FROM PUBLIC;
REVOKE ALL ON TABLE room_hid FROM postgres;
GRANT ALL ON TABLE room_hid TO postgres;
GRANT ALL ON TABLE room_hid TO ro_hid;
GRANT SELECT ON TABLE room_hid TO groupitreps;


--
-- Name: room_long_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE room_long_hid FROM PUBLIC;
REVOKE ALL ON TABLE room_long_hid FROM postgres;
GRANT ALL ON TABLE room_long_hid TO postgres;
GRANT ALL ON TABLE room_long_hid TO ro_hid;


--
-- Name: room_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE room_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE room_type_hid FROM postgres;
GRANT ALL ON TABLE room_type_hid TO postgres;
GRANT ALL ON TABLE room_type_hid TO ro_hid;
GRANT SELECT ON TABLE room_type_hid TO space_viewers;


--
-- Name: second_mentor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE second_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE second_mentor_hid FROM postgres;
GRANT ALL ON TABLE second_mentor_hid TO postgres;
GRANT ALL ON TABLE second_mentor_hid TO ro_hid;


--
-- Name: second_supervisor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE second_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE second_supervisor_hid FROM postgres;
GRANT ALL ON TABLE second_supervisor_hid TO postgres;
GRANT ALL ON TABLE second_supervisor_hid TO ro_hid;


--
-- Name: sector_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE sector_hid FROM PUBLIC;
REVOKE ALL ON TABLE sector_hid FROM postgres;
GRANT ALL ON TABLE sector_hid TO postgres;
GRANT ALL ON TABLE sector_hid TO ro_hid;


--
-- Name: socket_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE socket_hid FROM PUBLIC;
REVOKE ALL ON TABLE socket_hid FROM postgres;
GRANT ALL ON TABLE socket_hid TO postgres;
GRANT ALL ON TABLE socket_hid TO ro_hid;


--
-- Name: staff_category_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE staff_category_hid FROM PUBLIC;
REVOKE ALL ON TABLE staff_category_hid FROM postgres;
GRANT ALL ON TABLE staff_category_hid TO postgres;
GRANT ALL ON TABLE staff_category_hid TO ro_hid;


--
-- Name: status_(required)_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "status_(required)_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "status_(required)_hid" FROM postgres;
GRANT ALL ON TABLE "status_(required)_hid" TO postgres;
GRANT ALL ON TABLE "status_(required)_hid" TO ro_hid;


--
-- Name: status_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE status_hid FROM PUBLIC;
REVOKE ALL ON TABLE status_hid FROM postgres;
GRANT ALL ON TABLE status_hid TO postgres;
GRANT ALL ON TABLE status_hid TO ro_hid;
GRANT SELECT ON TABLE status_hid TO PUBLIC;


--
-- Name: student_name_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE student_name_hid FROM PUBLIC;
REVOKE ALL ON TABLE student_name_hid FROM postgres;
GRANT ALL ON TABLE student_name_hid TO postgres;
GRANT ALL ON TABLE student_name_hid TO ro_hid;


--
-- Name: subnet_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE subnet_hid FROM PUBLIC;
REVOKE ALL ON TABLE subnet_hid FROM dev;
GRANT ALL ON TABLE subnet_hid TO dev;
GRANT ALL ON TABLE subnet_hid TO ro_hid;


--
-- Name: substitute_supervisor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE substitute_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE substitute_supervisor_hid FROM postgres;
GRANT ALL ON TABLE substitute_supervisor_hid TO postgres;
GRANT ALL ON TABLE substitute_supervisor_hid TO ro_hid;


--
-- Name: supervisee_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE supervisee_hid FROM PUBLIC;
REVOKE ALL ON TABLE supervisee_hid FROM postgres;
GRANT ALL ON TABLE supervisee_hid TO postgres;
GRANT SELECT ON TABLE supervisee_hid TO ro_hid;


--
-- Name: supervisor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE supervisor_hid FROM postgres;
GRANT ALL ON TABLE supervisor_hid TO postgres;
GRANT ALL ON TABLE supervisor_hid TO ro_hid;


--
-- Name: supervisor_person_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE supervisor_person_hid FROM PUBLIC;
REVOKE ALL ON TABLE supervisor_person_hid FROM postgres;
GRANT ALL ON TABLE supervisor_person_hid TO postgres;
GRANT ALL ON TABLE supervisor_person_hid TO ro_hid;


--
-- Name: switch_config_goal_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE switch_config_goal_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_config_goal_hid FROM postgres;
GRANT ALL ON TABLE switch_config_goal_hid TO postgres;
GRANT SELECT ON TABLE switch_config_goal_hid TO ro_hid;


--
-- Name: switch_hardware_type_hid; Type: ACL; Schema: hotwire; Owner: cen1001
--

REVOKE ALL ON TABLE switch_hardware_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_hardware_type_hid FROM cen1001;
GRANT ALL ON TABLE switch_hardware_type_hid TO cen1001;
GRANT SELECT ON TABLE switch_hardware_type_hid TO ro_hid;


--
-- Name: switch_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE switch_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_hid FROM dev;
GRANT SELECT ON TABLE switch_hid TO ro_hid;


--
-- Name: switch_ip_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE switch_ip_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_ip_hid FROM dev;
GRANT ALL ON TABLE switch_ip_hid TO dev;
GRANT SELECT ON TABLE switch_ip_hid TO ro_hid;
GRANT ALL ON TABLE switch_ip_hid TO cos;


--
-- Name: switch_model_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE switch_model_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_model_hid FROM dev;
GRANT ALL ON TABLE switch_model_hid TO dev;
GRANT SELECT ON TABLE switch_model_hid TO ro_hid;
GRANT ALL ON TABLE switch_model_hid TO cos;


--
-- Name: switch_port_config_goal_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE switch_port_config_goal_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_port_config_goal_hid FROM postgres;
GRANT ALL ON TABLE switch_port_config_goal_hid TO postgres;
GRANT SELECT ON TABLE switch_port_config_goal_hid TO cos;


--
-- Name: switch_port_goal_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE switch_port_goal_hid FROM PUBLIC;
REVOKE ALL ON TABLE switch_port_goal_hid FROM postgres;
GRANT ALL ON TABLE switch_port_goal_hid TO postgres;
GRANT SELECT ON TABLE switch_port_goal_hid TO ro_hid;


--
-- Name: switchport_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE switchport_hid FROM PUBLIC;
REVOKE ALL ON TABLE switchport_hid FROM postgres;
GRANT ALL ON TABLE switchport_hid TO postgres;
GRANT ALL ON TABLE switchport_hid TO ro_hid;


--
-- Name: system_image_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE system_image_hid FROM PUBLIC;
REVOKE ALL ON TABLE system_image_hid FROM dev;
GRANT ALL ON TABLE system_image_hid TO dev;
GRANT ALL ON TABLE system_image_hid TO ro_hid;
GRANT SELECT ON TABLE system_image_hid TO wpkguser;


--
-- Name: system_image_host_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE system_image_host_hid FROM PUBLIC;
REVOKE ALL ON TABLE system_image_host_hid FROM postgres;
GRANT ALL ON TABLE system_image_host_hid TO postgres;
GRANT ALL ON TABLE system_image_host_hid TO ro_hid;


--
-- Name: time_estimate_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE time_estimate_hid FROM PUBLIC;
REVOKE ALL ON TABLE time_estimate_hid FROM dev;
GRANT ALL ON TABLE time_estimate_hid TO dev;
GRANT SELECT ON TABLE time_estimate_hid TO ro_hid;
GRANT SELECT,UPDATE ON TABLE time_estimate_hid TO groupitreps;


--
-- Name: title_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE title_hid FROM PUBLIC;
REVOKE ALL ON TABLE title_hid FROM postgres;
GRANT ALL ON TABLE title_hid TO postgres;
GRANT ALL ON TABLE title_hid TO ro_hid;
GRANT SELECT ON TABLE title_hid TO PUBLIC;


--
-- Name: university_term_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE university_term_hid FROM PUBLIC;
REVOKE ALL ON TABLE university_term_hid FROM postgres;
GRANT ALL ON TABLE university_term_hid TO postgres;
GRANT ALL ON TABLE university_term_hid TO ro_hid;


--
-- Name: updated_software_package_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE updated_software_package_hid FROM PUBLIC;
REVOKE ALL ON TABLE updated_software_package_hid FROM postgres;
GRANT ALL ON TABLE updated_software_package_hid TO postgres;
GRANT ALL ON TABLE updated_software_package_hid TO ro_hid;


--
-- Name: upgraded_licence_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE upgraded_licence_hid FROM PUBLIC;
REVOKE ALL ON TABLE upgraded_licence_hid FROM postgres;
GRANT ALL ON TABLE upgraded_licence_hid TO postgres;
GRANT ALL ON TABLE upgraded_licence_hid TO ro_hid;


--
-- Name: upgrading_licence_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE upgrading_licence_hid FROM PUBLIC;
REVOKE ALL ON TABLE upgrading_licence_hid FROM postgres;
GRANT ALL ON TABLE upgrading_licence_hid TO postgres;
GRANT ALL ON TABLE upgrading_licence_hid TO ro_hid;


--
-- Name: user_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE user_hid FROM PUBLIC;
REVOKE ALL ON TABLE user_hid FROM postgres;
GRANT ALL ON TABLE user_hid TO postgres;
GRANT ALL ON TABLE user_hid TO ro_hid;
GRANT SELECT ON TABLE user_hid TO groupitreps;


--
-- Name: user_prereg_standing_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE user_prereg_standing_hid FROM PUBLIC;
REVOKE ALL ON TABLE user_prereg_standing_hid FROM postgres;
GRANT ALL ON TABLE user_prereg_standing_hid TO postgres;
GRANT ALL ON TABLE user_prereg_standing_hid TO ro_hid;


--
-- Name: user_prereg_title_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE user_prereg_title_hid FROM PUBLIC;
REVOKE ALL ON TABLE user_prereg_title_hid FROM postgres;
GRANT ALL ON TABLE user_prereg_title_hid TO postgres;
GRANT ALL ON TABLE user_prereg_title_hid TO ro_hid;


--
-- Name: usual_mentor_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE usual_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE usual_mentor_hid FROM postgres;
GRANT ALL ON TABLE usual_mentor_hid TO postgres;
GRANT ALL ON TABLE usual_mentor_hid TO ro_hid;


--
-- Name: usual_reviewer_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE usual_reviewer_hid FROM PUBLIC;
REVOKE ALL ON TABLE usual_reviewer_hid FROM postgres;
GRANT ALL ON TABLE usual_reviewer_hid TO postgres;
GRANT ALL ON TABLE usual_reviewer_hid TO ro_hid;


--
-- Name: virus_detection_strategy_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE virus_detection_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE virus_detection_strategy_hid FROM postgres;
GRANT ALL ON TABLE virus_detection_strategy_hid TO postgres;
GRANT ALL ON TABLE virus_detection_strategy_hid TO ro_hid;


--
-- Name: visitor_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE visitor_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE visitor_type_hid FROM postgres;
GRANT ALL ON TABLE visitor_type_hid TO postgres;
GRANT ALL ON TABLE visitor_type_hid TO ro_hid;


--
-- Name: vlan_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE vlan_hid FROM PUBLIC;
REVOKE ALL ON TABLE vlan_hid FROM dev;
GRANT ALL ON TABLE vlan_hid TO dev;
GRANT SELECT ON TABLE vlan_hid TO ro_hid;


--
-- Name: vlan_vid_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE vlan_vid_hid FROM PUBLIC;
REVOKE ALL ON TABLE vlan_vid_hid FROM postgres;
GRANT ALL ON TABLE vlan_vid_hid TO postgres;
GRANT SELECT ON TABLE vlan_vid_hid TO ro_hid;


--
-- Name: website_staff_category_hid; Type: ACL; Schema: hotwire; Owner: dev
--

REVOKE ALL ON TABLE website_staff_category_hid FROM PUBLIC;
REVOKE ALL ON TABLE website_staff_category_hid FROM dev;
GRANT ALL ON TABLE website_staff_category_hid TO dev;
GRANT SELECT ON TABLE website_staff_category_hid TO ro_hid;
GRANT SELECT ON TABLE website_staff_category_hid TO PUBLIC;


--
-- Name: www_person_notitles_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE www_person_notitles_hid FROM PUBLIC;
REVOKE ALL ON TABLE www_person_notitles_hid FROM postgres;
GRANT ALL ON TABLE www_person_notitles_hid TO postgres;
GRANT SELECT ON TABLE www_person_notitles_hid TO ro_hid;


--
-- Name: www_room_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE www_room_hid FROM PUBLIC;
REVOKE ALL ON TABLE www_room_hid FROM postgres;
GRANT ALL ON TABLE www_room_hid TO postgres;
GRANT ALL ON TABLE www_room_hid TO ro_hid;

--
-- Data for Name: _subviews; Type: TABLE DATA; Schema: hotwire; Owner: dev
--

COPY _subviews (view, subview, target) FROM stdin;
probation_and_reviews_view	staff_reviews_summary_subview	staff_reviews_view
postgrad_students_view	jump_to_person_from_postgrad_subview	\N
usage_monitor_view	print_jobs_subview	\N
system_image_all_view	system_image_contact_details_subview	\N
research_group_mgmt_view	research_group_members_subview	personnel_basic_view
postgrad_funding_view	postgrad_funding_subview	postgraduate_funding_view
sick_leave_report	sick_history_subview	sick_leave_updates_view
room_occupancy_view	room_occupants_subview	personnel_basic_view
research_group_editing_view	research_group_members_subview	\N
research_groups_computing_view	cos_research_group_members_subview	\N
leave_view	misc_leave_subview	\N
leave_view	sabbatical_leave_subview	\N
leave_view	sick_history_subview	\N
part_iii_students_view	jump_to_person_from_part_iii_subview	\N
visitors_view	jump_to_person_from_visitor_subview	\N
erasmus_students_view	jump_to_person_from_erasmus_subview	\N
post_history_view	jump_to_person_from_post_history_subview	\N
switchstack_view	switchstack_cabinet_subview	\N
switchstack_view	switchstack_switches_subview	\N
cabinet_view	cabinet_switchstack_subview	\N
switch_view	switch_switchstack_subview	\N
hardware_basic_view	hardware_system_image_subview	\N
hardware_full_view	hardware_system_image_subview	\N
system_image_all_view	system_image_hardware_subview	\N
system_image_network_view	system_image_hardware_subview	\N
socket_view	socket_patch_panel_subview	\N
patch_panel_view	patch_panel_socket_subview	\N
patch_panel_view	patch_panel_cabinet_subview	\N
personnel_history_view	misc_leave_subview	\N
personnel_history_view	cambridge_history_v2_subview	post_history_view
switch_view	switch_switchport_subview	\N
switchport_view	switchport_switch_subview	\N
printers_view	printer_system_image_subview	\N
system_image_all_view	system_image_ip_address	\N
postgrad_students_view	postgrad_student_mentor_meetings_subview	\N
personnel_history_view	staff_review_history_subview	\N
research_groups_computing_view	research_groups_computing_computers_subview	\N
10_View/Cabinet	cabinet_patch_panel_subview	\N
\.


--
-- Data for Name: hw_preference_type_hid; Type: TABLE DATA; Schema: hotwire; Owner: postgres
--

COPY hw_preference_type_hid (hw_preference_type_id, hw_preference_type_hid) FROM stdin;
1	boolean
2	integer
3	string
\.


--
-- Data for Name: hw_Preferences; Type: TABLE DATA; Schema: hotwire; Owner: postgres
--

COPY "hw_Preferences" (id, hw_preference_name, hw_preference_type_id, hw_preference_const) FROM stdin;
1	Floating table headers	1	FLOATHEAD
9	Default view	3	DEFVIEW
2	Jump on a single search result	1	JUMPSEARCH
3	Jump on a single view result	1	JUMPVIEW
4	CSS path	3	CSSPATH
7	JS path	3	JSPATH
5	Warn if record data changes	1	CHANGE
10	Warn if view data changes	1	CHECKSUM
6	AJAX min dd size	2	AJAXMIN
11	Alternative views enabled	1	ALTVIEW
12	Related Records enabled	1	RELREC
13	Resizable Table columns	1	RESIZECOLS
14	Sort NULLS in numerical order	1	NUMNULL
15	Hideable Table columns	1	HIDECOLS
16	Reorderable Table columns	1	COLREORDER
17	Show old search	1	OLDSEARCH
18	Include "... show next" buttons	1	SHOWNEXT
19	Warn on missing rules	1	WARNRULES
20	Cache menu structure	1	CACHEMENU
21	Remove accents from search	1	UNACCENT
22	Date format	3	DATEFORMAT
23	Show web interface hints	1	SHOWHINTS
24	Show post-update options	1	POSTUPDATEOPT
25	Show post-insert options	1	POSTINSERTOPT
26	Offer xdebug option to developers	1	XDEBUG
27	DB Development group	3	DBDEV
28	Database internal date format	3	DBDATEFORMAT
\.


 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'as2737') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ajs1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'gm373') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hrnj1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hcn25') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'se359') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'as376') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'wjmr2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'oas23') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'nb10013') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'30_Report/ChemAtCam/OptIns',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dh473') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (18,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sb293') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'daj4') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mjd13') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jat71') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrecone') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tjy22') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dk10012') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pd260') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (5,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'wpn1000') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jd819') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jlpb2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jcd62') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'interviewtest') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sc690') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pbd2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mm2244') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sw134') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rj345') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mv245') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'smt58') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dc704') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jr748') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'smt58') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (10,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sed33') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (28,'d/m/Y');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'skrc2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'fjl1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (11,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (13,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'vdl20') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ajwt3') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ar732') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (4,'user-css');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Accounts',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sf340') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (23,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cen1001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'oj100') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrectwo') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'teha2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sre1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (22,'d/m/Y');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rl510') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'yz421') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'drs36') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mph36') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ld519') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ajp208') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'djh35') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rk529') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ap876') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (25,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cen1001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (7,'user-js');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sjp86') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (16,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (9,'10_View/People/Personnel_Phone');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'wj10') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'am966') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'djl10') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mja70') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jz366') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dshc2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dl602') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cen1001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (20,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'fcm29') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rj297') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'djp15') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'gi229') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cjk34') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'web20') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ab454') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sjj24') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mk594') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rml1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dm729') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrecone') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'arf25') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jbh42') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rlc67') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ld519') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jrn34') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ek406') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'def27') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ka204') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rmt35') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'fb419') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (27,'dev');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'srb39') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jmg11') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tf269') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ld519') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tk391') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ata27') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rlj22') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (1,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mds44') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'des26') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ncp1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'er376') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mtlc2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jwrm2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'lm554') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'amb84') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rc454') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'asm58') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dw34') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ig326') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'asa10') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pdw12') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jl802') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pjg48') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrectwo') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Data_Entry',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rj297') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jc730') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'gs564') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'qaep2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'gk422') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cen1001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tgp27') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sb293') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ptw22') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (24,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cen1001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mjg32') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrecone') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'smt58') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rjp71') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jbom1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cl469') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ej311') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hs536') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sej13') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrectwo') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rf10001') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'so230') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sv319') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sb293') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'js924') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cgb36') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mw529') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tkd25') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'smb28') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pkc31') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'mat64') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jhk10') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (3,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Photography_Registration',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'photography') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pd260') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sh811') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ejg49') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'nla27') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (3,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'lm554') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ejg49') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (15,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'thl32') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (4,'sb293-css',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sb293') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'kr366') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rmp53') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Data_Entry',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hrnj1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'dgr30') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (26,'TRUE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'js24') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'lm554') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tpjk2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'djp15') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jk18') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'lm554') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sm2029') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (21,'FALSE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'es401') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'symm2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrecone') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ps589') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tl442') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (14,'FALSE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'od265') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jpm206') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ld519') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rj297') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'lka26') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/System_Image_All',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'alt36') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (17,'TRUE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sb293') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'zy251') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cp605') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'rl403') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (6,'25');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'oamp2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'aehw2') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sl591') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'jrk38') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'adb29') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'acf50') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (17,'FALSE');
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'amp96') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'pdb30') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'sm2210') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'tr352') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'ip100') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Phone',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'chrectwo') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/People/Personnel_Basic',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'cdh30') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (9,'10_View/Group_Computers',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hab60') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value, user_id ) values (2,'FALSE',( select pg_roles.oid from pg_roles where pg_roles.rolname = 'hrnj1') ) ;
 insert into hotwire."hw_User Preferences" (preference_id, preference_value ) values (12,'TRUE');

