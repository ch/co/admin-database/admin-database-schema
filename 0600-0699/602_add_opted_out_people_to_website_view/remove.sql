DROP VIEW hotwire3."10_View/People/Website_View";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Website_View" AS 
 SELECT person.id, person.crsid AS ro_crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS ro_email_address, person.image_lo AS image, www_person_hid_v2.www_person_hid AS ro_display_name, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS ro_name_for_sorting, cambridge_college.name AS ro_college_name, cambridge_college.website AS ro_college_website, telephone_numbers.telephone_numbers AS ro_telephone_numbers, 
        CASE
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS ro_post_category, person.website_staff_category_id, person.counts_as_academic AS ro_counts_as_academic, s.status_id AS ro_physical_status, r.rigs AS ro_rigs, pr.primary_rig AS ro_primary_rig, rgs.research_groups AS ro_research_groups, rgs.website_addresses AS ro_website_addresses, post_history.job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE AND _latest_role.role_tablename = 'post_history'::text
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE hotwire3."10_View/People/Website_View"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Website_View" TO dev;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire3."10_View/People/Website_View" TO web_editors;

CREATE OR REPLACE RULE people_website_upd AS
    ON UPDATE TO hotwire3."10_View/People/Website_View" DO INSTEAD ( UPDATE person SET website_staff_category_id = new.website_staff_category_id
  WHERE person.id = old.id;
 UPDATE post_history SET job_title = new.job_title
  WHERE post_history.id = (( SELECT ph.id
           FROM person p
      JOIN cache._latest_role lr ON p.id = lr.person_id
   JOIN post_history ph ON lr.role_id = ph.id
  WHERE p.id = old.id));
);


