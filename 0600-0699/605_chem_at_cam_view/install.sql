CREATE VIEW hotwire3."10_View/People/Chem_at_Cam" AS
WITH optins AS (
    SELECT
        id,
        surname,
        first_names,
        title_hid,
        post_category,
        email_address,
        forwarding_address::text, -- looks like the admin team populate this for leavers
        leaving_date, -- the next two are to flush out those who have come back
        status_id as presence
    FROM person
    LEFT JOIN cache._latest_role_v12 _latest_role ON _latest_role.person_id = person.id
    LEFT JOIN public.title_hid USING (title_id)
    LEFT JOIN _physical_status_v3 USING (person_id)
    WHERE chem_at_cam = 't'
    ORDER BY leaving_date DESC
)
SELECT * FROM optins; -- the 'with' clause is needed to preserve the sort order, which Hotwire is too clever about

ALTER VIEW hotwire3."10_View/People/Chem_at_Cam" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/People/Chem_at_Cam" TO chematcam;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/People/Chem_at_Cam', 'person' );
