CREATE OR REPLACE VIEW _postgrad_end_dates_v5 AS 
 SELECT postgraduate_studentship.id, best_end_dates.end_date, postgraduate_studentship.funding_end_date AS intended_end_date, best_end_dates.intended_end_date AS estimated_leaving_date, 
        CASE
            WHEN postgraduate_studentship.force_role_status_to_past = true THEN 'Past'::character varying(20)
            WHEN best_end_dates.end_date < now() THEN 'Past'::character varying(20)
            WHEN postgraduate_studentship.start_date < now() AND best_end_dates.end_date > now() THEN 'Current'::character varying(20)
            WHEN postgraduate_studentship.start_date < now() AND best_end_dates.end_date IS NULL THEN 'Current'::character varying(20)
            WHEN postgraduate_studentship.start_date > now() THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS status_id, btrim(age(COALESCE(postgraduate_studentship.phd_date_submitted, 'now'::text::date)::timestamp with time zone, COALESCE(postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.start_date)::timestamp with time zone)::text, '@ '::text)::character varying(30) AS phd_duration, COALESCE(postgraduate_studentship.phd_date_submitted, 'now'::text::date) - COALESCE(postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.start_date) AS phd_duration_days
   FROM postgraduate_studentship
   JOIN ( SELECT postgraduate_studentship.id, 
                CASE
                    WHEN postgraduate_studentship.force_role_status_to_past = true THEN COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.phd_date_submitted, postgraduate_studentship.date_withdrawn_from_register, 'now'::text::date - 1)
                    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN COALESCE(postgraduate_studentship.phd_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(postgraduate_studentship.mphil_date_awarded, postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN COALESCE(postgraduate_studentship.cpgs_or_mphil_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN COALESCE(postgraduate_studentship.msc_date_awarded, postgraduate_studentship.date_withdrawn_from_register)
                    ELSE NULL::date
                END AS end_date, 
                CASE
                    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN 
                    CASE
                        WHEN postgraduate_studentship.end_of_registration_date IS NULL THEN (postgraduate_studentship.start_date + '4 years'::interval + '3 mons'::interval)::date
                        ELSE (postgraduate_studentship.end_of_registration_date + '3 mons'::interval)::date
                    END
                    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(postgraduate_studentship.mphil_date_submission_due, postgraduate_studentship.cpgs_or_mphil_date_submission_due)
                    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN postgraduate_studentship.cpgs_or_mphil_date_submission_due
                    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN postgraduate_studentship.msc_date_submission_due
                    ELSE NULL::date
                END AS intended_end_date
           FROM postgraduate_studentship
      JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = postgraduate_studentship.postgraduate_studentship_type_id
   JOIN person ON person.id = postgraduate_studentship.person_id) best_end_dates USING (id)
   JOIN person ON person.id = postgraduate_studentship.person_id;

ALTER TABLE _postgrad_end_dates_v5
  OWNER TO cen1001;
GRANT ALL ON TABLE _postgrad_end_dates_v5 TO cen1001;
GRANT SELECT ON TABLE _postgrad_end_dates_v5 TO mgmt_ro;
GRANT SELECT ON TABLE _postgrad_end_dates_v5 TO dev;
GRANT SELECT ON TABLE _postgrad_end_dates_v5 TO student_management;

