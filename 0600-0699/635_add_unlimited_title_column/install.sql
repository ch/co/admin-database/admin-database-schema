ALTER TABLE public.title_hid ADD COLUMN abbrev_title varchar;
ALTER TABLE public.title_hid ADD COLUMN long_title varchar;
ALTER TABLE public.title_hid ADD COLUMN website_title varchar;
ALTER TABLE public.title_hid ADD COLUMN salutation_title varchar;

UPDATE public.title_hid set abbrev_title = title_hid::varchar;
UPDATE public.title_hid set long_title = long_title_hid::varchar;
UPDATE public.title_hid set website_title = website_title_hid::varchar;
UPDATE public.title_hid set salutation_title = salutation_title_hid::varchar;

ALTER TABLE public.title_hid ALTER COLUMN abbrev_title SET NOT NULL;
ALTER TABLE public.title_hid ALTER COLUMN long_title SET NOT NULL;
ALTER TABLE public.title_hid ALTER COLUMN salutation_title SET NOT NULL;

CREATE UNIQUE INDEX title_abbrev_idx ON public.title_hid(abbrev_title);
