DROP INDEX title_abbrev_idx ;

ALTER TABLE public.title_hid DROP COLUMN abbrev_title;
ALTER TABLE public.title_hid DROP COLUMN long_title;
ALTER TABLE public.title_hid DROP COLUMN website_title;
ALTER TABLE public.title_hid DROP COLUMN salutation_title;
