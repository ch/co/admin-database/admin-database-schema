DROP VIEW hotwire3."10_View/People/Chem_at_Cam";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Chem_at_Cam" AS 
 WITH optins AS (
         SELECT person.id, person.surname, person.first_names, title_hid.title_hid, _latest_role.post_category, person.email_address, person.forwarding_address::text AS forwarding_address, person.leaving_date, _physical_status_v3.status_id AS presence
           FROM person
      LEFT JOIN cache._latest_role_v12 _latest_role ON _latest_role.person_id = person.id
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN _physical_status_v3 USING (person_id)
  WHERE person.chem_at_cam = true
  ORDER BY person.leaving_date DESC
        )
 SELECT optins.id, optins.surname, optins.first_names, optins.title_hid, optins.post_category, optins.email_address, optins.forwarding_address, optins.leaving_date, optins.presence
   FROM optins;

ALTER TABLE hotwire3."10_View/People/Chem_at_Cam"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Chem_at_Cam" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Chem_at_Cam" TO chematcam;

