DROP VIEW hotwire3."10_View/People/Titles";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Titles" AS 
 SELECT title_hid.title_id AS id, title_hid.abbrev_title AS title, title_hid.long_title AS long_title, title_hid.website_title AS title_shown_on_websites, title_hid.salutation_title AS title_for_written_salutation
   FROM title_hid;

ALTER TABLE hotwire3."10_View/People/Titles"
  OWNER TO dev;

-- Rule: hotwire3_people_titles_ins ON hotwire3."10_View/People/Titles"

-- DROP RULE hotwire3_people_titles_ins ON hotwire3."10_View/People/Titles";

CREATE OR REPLACE RULE hotwire3_people_titles_ins AS
    ON INSERT TO hotwire3."10_View/People/Titles" DO INSTEAD  INSERT INTO title_hid (title_hid, long_title_hid, website_title_hid, salutation_title_hid, abbrev_title, long_title, website_title, salutation_title) 
  VALUES (new.title::varchar(10), new.long_title::varchar(40), new.title_shown_on_websites::varchar(40), new.title_for_written_salutation, new.title, new.long_title, new.title_shown_on_websites, new.title_for_written_salutation)
  RETURNING title_hid.title_id, title_hid.abbrev_title, title_hid.long_title, title_hid.website_title, title_hid.salutation_title;

-- Rule: hotwire3_people_titles_upd ON hotwire3."10_View/People/Titles"

-- DROP RULE hotwire3_people_titles_upd ON hotwire3."10_View/People/Titles";

CREATE OR REPLACE RULE hotwire3_people_titles_upd AS
    ON UPDATE TO hotwire3."10_View/People/Titles" DO INSTEAD  UPDATE title_hid SET title_hid = new.title::varchar(10), long_title_hid = new.long_title::varchar(40), website_title_hid = new.title_shown_on_websites::varchar(40), salutation_title_hid = new.title_for_written_salutation, abbrev_title = new.title, long_title = new.long_title, website_title = new.title_shown_on_websites, salutation_title = new.title_for_written_salutation
  WHERE title_hid.title_id = old.id;
