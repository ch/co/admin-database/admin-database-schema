DROP VIEW hotwire3."10_View/Safety/Fire_wardens";

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Fire_wardens" AS 
 SELECT fire_warden.fire_warden_id AS id, fire_warden.person_id, fire_warden.qualification_date, fire_warden_area.fire_warden_area_id, fire_warden_area.building_id AS ro_building_id, fire_warden_area.building_region_id AS ro_building_region_id, fire_warden_area.building_floor_id AS ro_building_floor_id, fire_warden.is_primary AS main_firewarden_for_area, ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE fire_warden.person_id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id, _physical_status_v3.status_id AS ro_physical_status, _fire_warden_status.requalification_date::date AS ro_requalification_date, _fire_warden_status.in_date AS ro_qualification_in_date, _latest_role_v12.post_category AS ro_post_category, 
        CASE
            WHEN _physical_status_v3.status_id::text <> 'Current'::text THEN 'orange'::text
            WHEN _fire_warden_status.in_date = false THEN 'red'::text
            WHEN _fire_warden_status.in_date IS NULL THEN 'blue'::text
            ELSE NULL::text
        END AS _cssclass
   FROM fire_warden
   LEFT JOIN _latest_role_v12 USING (person_id)
   LEFT JOIN fire_warden_area USING (fire_warden_area_id)
   LEFT JOIN _physical_status_v3 USING (person_id)
   LEFT JOIN _fire_warden_status USING (fire_warden_id);

ALTER TABLE hotwire3."10_View/Safety/Fire_wardens"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO safety_management;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO mgmt_ro;

-- Rule: fire_warden_del ON hotwire3."10_View/Safety/Fire_wardens"

-- DROP RULE fire_warden_del ON hotwire3."10_View/Safety/Fire_wardens";

CREATE OR REPLACE RULE fire_warden_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  DELETE FROM fire_warden
  WHERE fire_warden.fire_warden_id = old.id;


-- Trigger: fire_warden_upd on hotwire3."10_View/Safety/Fire_wardens"

-- DROP TRIGGER fire_warden_upd ON hotwire3."10_View/Safety/Fire_wardens";

CREATE TRIGGER fire_warden_upd
  INSTEAD OF INSERT OR UPDATE
  ON hotwire3."10_View/Safety/Fire_wardens"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.fire_wardens_trig();


