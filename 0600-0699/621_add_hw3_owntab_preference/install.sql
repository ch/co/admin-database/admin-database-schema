CREATE UNIQUE INDEX hw_pref_const_uniq ON hotwire3."hw_Preferences" (hw_preference_const);
INSERT INTO hotwire3."hw_Preferences" ( hw_preference_name, hw_preference_type_id, hw_preference_const ) VALUES ( 'Open record in own tab', 1, 'OWNTAB');
INSERT INTO hotwire3."hw_User Preferences" ( preference_id, preference_value ) VALUES ( (SELECT id FROM hotwire3."hw_Preferences" WHERE hw_preference_const = 'OWNTAB' ), 'FALSE' );
