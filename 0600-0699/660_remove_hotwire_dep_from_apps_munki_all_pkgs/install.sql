CREATE OR REPLACE VIEW apps.munki_all_pkgs AS 
 SELECT software_package.munki_package_name AS name, ip_address.ip
   FROM software_package
   JOIN mm_software_package_installs_on_architecture ON software_package.id = mm_software_package_installs_on_architecture.software_package_id
   JOIN mm_software_package_installs_on_os_class USING (software_package_id)
   JOIN system_image USING (architecture_id)
   JOIN operating_system ON operating_system.id = system_image.operating_system_id
   JOIN public.os_class_hid ON os_class_hid.os_class_id = operating_system.os_class_id AND os_class_hid.os_class_id = mm_software_package_installs_on_os_class.os_class_id
   JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   JOIN ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id;

ALTER TABLE apps.munki_all_pkgs
  OWNER TO dev;
GRANT ALL ON TABLE apps.munki_all_pkgs TO dev;
GRANT SELECT ON TABLE apps.munki_all_pkgs TO osbuilder;

