CREATE FUNCTION public.copy_table_to_cache(schemaname text, tablename text) RETURNS VOID AS
$$
DECLARE
    source text := quote_ident(schemaname)||'.'||quote_ident(tablename);
BEGIN
    IF EXISTS (SELECT * FROM information_schema.tables WHERE table_schema='cache' AND table_name=tablename)
    THEN
	EXECUTE 'TRUNCATE TABLE cache.'||quote_ident(tablename);
        EXECUTE 'INSERT INTO cache.'||quote_ident(tablename)||' ( SELECT * FROM '||source||' )';
    ELSE
        EXECUTE 'CREATE TABLE cache.'||quote_ident(tablename)|| ' AS SELECT * FROM '||source;
        EXECUTE 'ALTER TABLE cache.'||quote_ident(tablename)|| ' OWNER TO dev';
    END IF;
    RETURN;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION public.copy_table_to_cache(text,text) OWNER TO dev;
