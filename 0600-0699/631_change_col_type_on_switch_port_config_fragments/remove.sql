DROP VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" AS 
 SELECT switch_port_config_fragment.id, switch_port_config_fragment.switch_port_config_goal_id, switch_port_config_fragment.config, ARRAY( SELECT mm_switch_port_config_fragment_switch_model.switch_model_id
           FROM mm_switch_port_config_fragment_switch_model
          WHERE mm_switch_port_config_fragment_switch_model.switch_port_config_fragment_id = switch_port_config_fragment.id) AS switch_model_id
   FROM switch_port_config_fragment;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" TO cos;

CREATE OR REPLACE FUNCTION hotwire3.network_switch_port_config_fragment_ins()
  RETURNS trigger AS
$BODY$
begin
  new.id := nextval('switch_port_config_fragment_id_seq');
  INSERT INTO switch_port_config_fragment ( id, config, switch_port_config_goal_id ) VALUES ( new.id, new.config, new.switch_port_config_goal_id );
  PERFORM fn_mm_array_update(new.switch_model_id, array[]::bigint[], 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, new.id::bigint);
  RETURN new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.network_switch_port_config_fragment_ins()
  OWNER TO dev;

-- Rule: hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments"

-- DROP RULE hotwire3_view_network_switch_port_config_fragment_del ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments";

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_fragment_del AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" DO INSTEAD  DELETE FROM switch_port_config_fragment
  WHERE switch_port_config_fragment.id = old.id;

-- Rule: hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments"

-- DROP RULE hotwire3_view_network_switch_port_config_fragment_upd ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments";

CREATE OR REPLACE RULE hotwire3_view_network_switch_port_config_fragment_upd AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments" DO INSTEAD ( UPDATE switch_port_config_fragment SET switch_port_config_goal_id = new.switch_port_config_goal_id, config = new.config
  WHERE switch_port_config_fragment.id = old.id;
 SELECT fn_mm_array_update(new.switch_model_id, old.switch_model_id, 'mm_switch_port_config_fragment_switch_model'::character varying, 'switch_port_config_fragment_id'::character varying, 'switch_model_id'::character varying, old.id::bigint) AS fn_mm_array_update2;
);


-- Trigger: hotwire3_view_network_switch_port_config_fragment_ins on hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments"

-- DROP TRIGGER hotwire3_view_network_switch_port_config_fragment_ins ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments";

CREATE TRIGGER hotwire3_view_network_switch_port_config_fragment_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Network/Switches/Switch_Port_Config_Fragments"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.network_switch_port_config_fragment_ins();


