CREATE VIEW apps.dom0_domu_mapping AS
SELECT DISTINCT
    dom0_ip.hostname as dom0_hostname,
    array_agg( domu_ip.hostname ) AS domu_hostnames
FROM mm_dom0_domu
JOIN mm_system_image_ip_address mm_dom0_ip ON mm_dom0_ip.system_image_id = mm_dom0_domu.dom0_id
JOIN ip_address dom0_ip ON mm_dom0_ip.ip_address_id = dom0_ip.id
JOIN mm_system_image_ip_address mm_domu_ip ON mm_domu_ip.system_image_id = mm_dom0_domu.domu_id
JOIN ip_address domu_ip ON mm_domu_ip.ip_address_id = domu_ip.id
GROUP BY dom0_ip.hostname;
COMMENT ON VIEW apps.dom0_domu_mapping IS 'Changeset 629 to replace old dom0-domu update mechanism';


ALTER TABLE apps.dom0_domu_mapping OWNER TO dev;

GRANT SELECT,INSERT ON apps.dom0_domu_mapping TO _api_dom0domu;
GRANT SELECT,UPDATE (id, hardware_id) ON system_image TO _api_dom0domu;
GRANT SELECT,INSERT ON mm_dom0_domu TO _api_dom0domu;
GRANT SELECT (id,banned_for_lack_of_software_compliance) ON research_group TO _api_dom0domu;
GRANT SELECT ON mm_person_research_group TO _api_dom0domu;
GRANT SELECT (id,owner_id) ON hardware TO _api_dom0domu;

CREATE FUNCTION apps.dom0_domu_mapping_upd() RETURNS TRIGGER AS
$$
DECLARE
    dom0_id bigint;
    domu_id bigint;
    domu_hostname varchar(63);
BEGIN
    -- Dom0 id (what if it's null?)
    SELECT system_image.id INTO dom0_id
    FROM system_image
    JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
    JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
    WHERE ip_address.hostname = NEW.dom0_hostname;
    IF NOT FOUND THEN
        RAISE NOTICE 'System image with hostname % not found', NEW.dom0_hostname;
        RETURN NULL;
    END IF;

    -- loop over domu hostnames
    FOREACH domu_hostname IN ARRAY NEW.domu_hostnames LOOP
        -- Find id
        SELECT system_image.id INTO domu_id
        FROM system_image
        JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
        JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
        WHERE ip_address.hostname = domu_hostname;
        If NOT FOUND THEN
            RAISE NOTICE 'System image with hostname % not found', domu_hostname;
            CONTINUE;
        END IF;
        -- Update hardware in domu system image
        UPDATE system_image SET hardware_id = (SELECT hardware_id FROM system_image WHERE id = dom0_id ) WHERE id = domu_id;
        -- Update many-many relation between system images. We don't remove out of date ones.
        BEGIN
            INSERT INTO mm_dom0_domu (dom0_id,domu_id) VALUES (dom0_id,domu_id);
        EXCEPTION WHEN unique_violation THEN
          -- skip if already there
        END;
    END LOOP;
    RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;
COMMENT ON FUNCTION apps.dom0_domu_mapping_upd() IS 'Changeset 629 to replace old dom0-domu update mechanism';
ALTER FUNCTION apps.dom0_domu_mapping_upd() OWNER TO dev;

CREATE TRIGGER update_mappings INSTEAD OF INSERT ON apps.dom0_domu_mapping FOR EACH ROW EXECUTE PROCEDURE apps.dom0_domu_mapping_upd();
