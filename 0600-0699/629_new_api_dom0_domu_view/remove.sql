DROP VIEW apps.dom0_domu_mapping;
DROP FUNCTION apps.dom0_domu_mapping_upd();

REVOKE SELECT,INSERT (id, hardware_id) ON system_image FROM _api_dom0domu;
REVOKE SELECT,INSERT ON mm_dom0_domu FROM _api_dom0domu;
REVOKE SELECT (id,banned_for_lack_of_software_compliance) ON research_group FROM _api_dom0domu;
REVOKE SELECT ON mm_person_research_group FROM _api_dom0domu;
REVOKE SELECT (id,owner_id) ON hardware FROM _api_dom0domu;
