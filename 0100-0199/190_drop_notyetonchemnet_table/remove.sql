
SET search_path = public, pg_catalog;

--
-- Name: notyetonchemnet; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE notyetonchemnet (
    crsid character varying
);


ALTER TABLE public.notyetonchemnet OWNER TO rl201;

--
-- Data for Name: notyetonchemnet; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY notyetonchemnet (crsid) FROM stdin;
wj10@ch.cam.ac.uk
srb39@ch.cam.ac.uk
as2604@ch.cam.ac.uk
at741@ch.cam.ac.uk
bjg45@ch.cam.ac.uk
cb775@ch.cam.ac.uk
chga2@ch.cam.ac.uk
cl469@ch.cam.ac.uk
cmd44@ch.cam.ac.uk
dh471@ch.cam.ac.uk
dmgfd2@ch.cam.ac.uk
er376@ch.cam.ac.uk
gamh2@ch.cam.ac.uk
gm487@ch.cam.ac.uk
jb878@ch.cam.ac.uk
jcf40@ch.cam.ac.uk
jjl54@ch.cam.ac.uk
jw570@ch.cam.ac.uk
jw750@ch.cam.ac.uk
jz366@ch.cam.ac.uk
mjc248@ch.cam.ac.uk
mk594@ch.cam.ac.uk
mo374@ch.cam.ac.uk
nmb43@ch.cam.ac.uk
pmb57@ch.cam.ac.uk
qa205@ch.cam.ac.uk
rlj1001@ch.cam.ac.uk
rmm32@ch.cam.ac.uk
rrk27@ch.cam.ac.uk
rs749@ch.cam.ac.uk
spw19@ch.cam.ac.uk
svl1000@ch.cam.ac.uk
tpcr2@ch.cam.ac.uk
xf225@ch.cam.ac.uk
zl302@ch.cam.ac.uk
\.


--
-- PostgreSQL database dump complete
--

