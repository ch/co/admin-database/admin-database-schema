CREATE OR REPLACE FUNCTION www.latest_role_for_supervisor(
    IN _person text,
    IN _supervisor text,
    OUT post_category character varying,
    OUT post_category_weight integer)
  RETURNS record AS
$BODY$
declare
  _person_id int;
  _supervisor_id int;
  _category_id varchar;
begin

  _person_id := (select id from person where crsid=_person);
  _supervisor_id := (select id from person where crsid=_supervisor);
  _category_id := (select www._all_roles_v1.post_category_id from www._all_roles_v1 left join www.post_category_weights on post_category_weights.post_category_id=_all_roles_v1.post_category_id where person_id=_person_id and supervisor_id=_supervisor_id
                 order by coalesce(nullif(end_date,NULL), nullif(funding_end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date) desc, post_category_weights.weight desc limit 1);
                 
  post_category := (select post_category_hid from www.post_category_hid where post_category_id=_category_id);
  post_category_weight := (select weight from www.post_category_weights where post_category_id=_category_id);

  -- What if _person = _supervisor? A person does not supervise themselves...
  -- Or, indeed, if e.g. we have a person on a website not supervised by one of the group leaders for some reason
  -- Fall back to whatever their research_group_post_category is if we get here

  IF post_category is null then
    post_category := (select research_group_post_category from www.person_info_v8 where crsid=_person);
  END IF;

  IF post_category_weight is null then
    post_category_weight := 0;
  END IF;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION www.latest_role_for_supervisor(text, text)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO dev;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO public;
GRANT EXECUTE ON FUNCTION www.latest_role_for_supervisor(text, text) TO www_sites;

