CREATE OR REPLACE VIEW hotwire."10_View/RFL/IP_Address" AS 
 SELECT ip_address.ip, ip_address.hostname
   FROM ip_address
  WHERE (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text) AND NOT ip_address.reserved
  ORDER BY ip_address.ip;

ALTER TABLE hotwire."10_View/RFL/IP_Address"
  OWNER TO rl201;

