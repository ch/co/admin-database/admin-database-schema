ALTER TABLE switch_model OWNER TO rl201;
GRANT ALL ON TABLE switch_model TO rl201;
GRANT SELECT ON TABLE switch_model TO osbuilder;
GRANT ALL ON TABLE switch_model TO cos;
GRANT SELECT ON TABLE switch_model TO public;

ALTER TABLE mm_switch_config_fragment_switch_model OWNER TO rl201;
GRANT ALL ON TABLE mm_switch_config_fragment_switch_model TO rl201;
GRANT ALL ON TABLE mm_switch_config_fragment_switch_model TO osbuilder;
GRANT ALL ON TABLE mm_switch_config_fragment_switch_model TO cos;

ALTER TABLE switch_config_fragment OWNER TO rl201;
GRANT ALL ON TABLE switch_config_fragment TO rl201;
GRANT ALL ON TABLE switch_config_fragment TO osbuilder;
GRANT ALL ON TABLE switch_config_fragment TO cos;

ALTER TABLE mm_switch_config_switch_port OWNER TO rl201;
GRANT ALL ON TABLE mm_switch_config_switch_port TO rl201;
GRANT ALL ON TABLE mm_switch_config_switch_port TO osbuilder;
GRANT ALL ON TABLE mm_switch_config_switch_port TO cos;

ALTER TABLE mm_switch_model_has_default_config_goal OWNER TO rl201;
GRANT ALL ON TABLE mm_switch_model_has_default_config_goal TO rl201;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE mm_switch_model_has_default_config_goal TO cos;

ALTER TABLE static_switch_config OWNER TO rl201;
GRANT ALL ON TABLE static_switch_config TO rl201;
GRANT SELECT ON TABLE static_switch_config TO osbuilder;
GRANT ALL ON TABLE static_switch_config TO cos;
