ALTER TABLE switch_model OWNER TO dev;
REVOKE ALL ON TABLE switch_model FROM rl201;
GRANT SELECT ON TABLE switch_model TO osbuilder;
GRANT ALL ON TABLE switch_model TO cos,dev;
GRANT SELECT ON TABLE switch_model TO public;

ALTER TABLE mm_switch_config_fragment_switch_model OWNER TO dev;
REVOKE ALL ON TABLE mm_switch_config_fragment_switch_model FROM rl201;
GRANT ALL ON TABLE mm_switch_config_fragment_switch_model TO osbuilder;
GRANT ALL ON TABLE mm_switch_config_fragment_switch_model TO cos,dev;

ALTER TABLE switch_config_fragment OWNER TO dev;
REVOKE ALL ON TABLE switch_config_fragment FROM rl201;
GRANT ALL ON TABLE switch_config_fragment TO osbuilder;
GRANT ALL ON TABLE switch_config_fragment TO cos,dev;

ALTER TABLE mm_switch_config_switch_port OWNER TO dev;
REVOKE ALL ON TABLE mm_switch_config_switch_port FROM rl201;
GRANT ALL ON TABLE mm_switch_config_switch_port TO osbuilder;
GRANT ALL ON TABLE mm_switch_config_switch_port TO cos,dev;

ALTER TABLE mm_switch_model_has_default_config_goal OWNER TO dev;
REVOKE ALL ON TABLE mm_switch_model_has_default_config_goal FROM rl201;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE mm_switch_model_has_default_config_goal TO cos,dev;

ALTER TABLE static_switch_config OWNER TO dev;
REVOKE ALL ON TABLE static_switch_config FROM rl201;
GRANT SELECT ON TABLE static_switch_config TO osbuilder;
GRANT ALL ON TABLE static_switch_config TO cos,dev;
