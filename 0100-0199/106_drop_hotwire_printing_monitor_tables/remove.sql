--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: print_job_id_seq; Type: SEQUENCE; Schema: public; Owner: sjt71
--

CREATE SEQUENCE print_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.print_job_id_seq OWNER TO sjt71;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: print_job; Type: TABLE; Schema: public; Owner: sjt71; Tablespace: 
--

CREATE TABLE print_job (
    id bigint DEFAULT nextval('print_job_id_seq'::regclass) NOT NULL,
    hardware_id bigint NOT NULL,
    date date NOT NULL,
    owner character varying(20) NOT NULL,
    title character varying(20) NOT NULL,
    application character varying(20) NOT NULL,
    mono_sides integer NOT NULL,
    colour_sides integer NOT NULL,
    sheets integer NOT NULL
);


ALTER TABLE public.print_job OWNER TO sjt71;

--
-- Name: idx_print_job; Type: CONSTRAINT; Schema: public; Owner: sjt71; Tablespace: 
--

ALTER TABLE ONLY print_job
    ADD CONSTRAINT idx_print_job UNIQUE (hardware_id, date, owner, title, application);


--
-- Name: idx_print_job_id; Type: CONSTRAINT; Schema: public; Owner: sjt71; Tablespace: 
--

ALTER TABLE ONLY print_job
    ADD CONSTRAINT idx_print_job_id UNIQUE (id);


--
-- Name: pk_print_job; Type: CONSTRAINT; Schema: public; Owner: sjt71; Tablespace: 
--

ALTER TABLE ONLY print_job
    ADD CONSTRAINT pk_print_job PRIMARY KEY (id);


--
-- Name: print_jobs_hardware_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sjt71
--

ALTER TABLE ONLY print_job
    ADD CONSTRAINT print_jobs_hardware_id_fkey FOREIGN KEY (hardware_id) REFERENCES hardware(id);


--
-- Name: print_job_id_seq; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON SEQUENCE print_job_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE print_job_id_seq FROM sjt71;
GRANT ALL ON SEQUENCE print_job_id_seq TO sjt71;
GRANT USAGE ON SEQUENCE print_job_id_seq TO cosuser;


--
-- Name: print_job; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE print_job FROM PUBLIC;
REVOKE ALL ON TABLE print_job FROM sjt71;
GRANT ALL ON TABLE print_job TO sjt71;
GRANT INSERT ON TABLE print_job TO cosuser;


--
-- PostgreSQL database dump complete
--

