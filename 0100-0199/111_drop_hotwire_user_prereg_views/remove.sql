CREATE OR REPLACE VIEW visitor_preregistration_view AS 
 SELECT user_prereg.id, user_prereg.user_prereg_title_id, user_prereg.forename AS first_names, user_prereg.surname, user_prereg.email, user_prereg.research_group_id, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.previous_surname AS previous_surname_if_any, user_prereg.old_crsid AS previous_crsid_if_any
   FROM user_prereg
  WHERE user_prereg.done = false AND user_prereg.mailed_admin_team <> true;

ALTER TABLE visitor_preregistration_view
  OWNER TO postgres;
GRANT ALL ON TABLE visitor_preregistration_view TO postgres;
GRANT SELECT, INSERT ON TABLE visitor_preregistration_view TO secretariat;
GRANT SELECT, INSERT ON TABLE visitor_preregistration_view TO cos;

CREATE OR REPLACE RULE visitor_preregistration_ins AS
    ON INSERT TO visitor_preregistration_view DO INSTEAD  INSERT INTO user_prereg (user_prereg_standing_id, user_prereg_title_id, forename, surname, research_group_id, gender_id, birth_date, start_date, leaving_date, previous_surname, old_crsid, email, requesting_person_crsid, make_ad_accounts_in_advance) 
  VALUES (( SELECT user_prereg_standing_hid.user_prereg_standing_id
           FROM user_prereg_standing_hid
          WHERE user_prereg_standing_hid.user_prereg_standing_hid::text = 'Visitor'::text), new.user_prereg_title_id, new.first_names, new.surname, new.research_group_id, new.gender_id, new.birth_date, new.start_date, new.leaving_date, new.previous_surname_if_any, new.previous_crsid_if_any, new.email, "current_user"(), true)
  RETURNING user_prereg.id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.email, user_prereg.research_group_id, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.previous_surname, user_prereg.old_crsid;


CREATE OR REPLACE VIEW hotwire."10_View/Visitor_Preregistration" AS 
 SELECT user_prereg.id, user_prereg.user_prereg_title_id, user_prereg.forename AS first_names, user_prereg.surname, user_prereg.email, user_prereg.research_group_id, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.previous_surname AS previous_surname_if_any, user_prereg.old_crsid AS previous_crsid_if_any
   FROM user_prereg
  WHERE user_prereg.done = false AND user_prereg.mailed_admin_team <> true;

ALTER TABLE hotwire."10_View/Visitor_Preregistration"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Visitor_Preregistration" TO postgres;
GRANT SELECT, INSERT ON TABLE hotwire."10_View/Visitor_Preregistration" TO secretariat;
GRANT SELECT, INSERT ON TABLE hotwire."10_View/Visitor_Preregistration" TO cos;

CREATE OR REPLACE RULE hotwire_view_visitor_preregistration_ins AS
    ON INSERT TO hotwire."10_View/Visitor_Preregistration" DO INSTEAD  INSERT INTO user_prereg (user_prereg_standing_id, user_prereg_title_id, forename, surname, research_group_id, gender_id, birth_date, start_date, leaving_date, previous_surname, old_crsid, email, requesting_person_crsid, make_ad_accounts_in_advance) 
  VALUES (( SELECT user_prereg_standing_hid.user_prereg_standing_id
           FROM user_prereg_standing_hid
          WHERE user_prereg_standing_hid.user_prereg_standing_hid::text = 'Visitor'::text), new.user_prereg_title_id, new.first_names, new.surname, new.research_group_id, new.gender_id, new.birth_date, new.start_date, new.leaving_date, new.previous_surname_if_any, new.previous_crsid_if_any, new.email, "current_user"(), true)
  RETURNING user_prereg.id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.email, user_prereg.research_group_id, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.previous_surname, user_prereg.old_crsid;


