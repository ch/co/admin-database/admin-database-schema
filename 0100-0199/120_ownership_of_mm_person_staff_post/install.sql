ALTER TABLE mm_person_staff_post OWNER TO dev;
REVOKE ALL ON TABLE mm_person_staff_post FROM sjt71;
REVOKE SELECT ON TABLE mm_person_staff_post FROM old_cos;
GRANT ALL ON TABLE mm_person_staff_post TO dev;
GRANT SELECT ON TABLE mm_person_staff_post TO mgmt_ro;
