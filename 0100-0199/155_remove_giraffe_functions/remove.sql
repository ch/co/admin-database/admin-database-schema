CREATE OR REPLACE FUNCTION giraffe_neck_manager()
  RETURNS trigger AS
$BODY$
begin
 perform * from postit where neck_position=NEW.neck_position and id!=NEW.id;
 if found then
  update postit set neck_position=neck_position+1 where id!=NEW.id and neck_position>=NEW.neck_position;
  update postit set neck_position=NULL, giraffe_id=2 where neck_position>10;
 end if;
 RETURN new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION giraffe_neck_manager()
  OWNER TO dev;

