ALTER TABLE _role_data OWNER TO sjt71;
GRANT ALL ON TABLE _role_data TO sjt71;
GRANT ALL ON TABLE _role_data TO dev;
GRANT SELECT ON TABLE _role_data TO public;

ALTER TABLE _subviews OWNER TO sjt71;
GRANT ALL ON TABLE _subviews TO sjt71;
GRANT SELECT ON TABLE _subviews TO public;
GRANT SELECT ON TABLE _subviews TO mgmt_ro;
GRANT UPDATE, INSERT ON TABLE _subviews TO cen1001;
GRANT DELETE ON TABLE _subviews TO dev;

ALTER TABLE hotwire._subviews OWNER TO sjt71;
GRANT ALL ON TABLE hotwire._subviews TO sjt71;
GRANT SELECT ON TABLE hotwire._subviews TO public;
GRANT SELECT ON TABLE hotwire._subviews TO mgmt_ro;
GRANT UPDATE, INSERT ON TABLE hotwire._subviews TO cen1001;
GRANT DELETE ON TABLE hotwire._subviews TO dev;

ALTER TABLE _view_data OWNER TO sjt71;
GRANT ALL ON TABLE _view_data TO sjt71;
GRANT SELECT ON TABLE _view_data TO public;

ALTER TABLE column_data OWNER TO sjt71;
GRANT ALL ON TABLE column_data TO sjt71;
GRANT SELECT ON TABLE column_data TO old_cos;
GRANT SELECT ON TABLE column_data TO mgmt_ro;
GRANT SELECT ON TABLE column_data TO space_management;
GRANT SELECT ON TABLE column_data TO selfservice;
GRANT SELECT ON TABLE column_data TO public;
