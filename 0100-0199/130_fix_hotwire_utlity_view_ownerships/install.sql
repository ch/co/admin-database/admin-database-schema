ALTER VIEW _role_data OWNER TO dev;
GRANT ALL ON TABLE _role_data TO dev;
REVOKE ALL ON TABLE _role_data from sjt71;
GRANT SELECT on _role_data to PUBLIC;

ALTER TABLE public._subviews OWNER TO dev;
GRANT ALL ON TABLE _subviews TO dev;
REVOKE ALL ON TABLE _subviews FROM sjt71;
GRANT SELECT ON TABLE _subviews TO public;

ALTER TABLE hotwire._subviews OWNER TO dev;
GRANT ALL ON TABLE hotwire._subviews TO dev;
REVOKE ALL ON TABLE hotwire._subviews FROM sjt71;
GRANT SELECT ON TABLE hotwire._subviews TO public;

ALTER TABLE _view_data OWNER TO dev;
GRANT ALL ON TABLE _view_data TO dev;
REVOKE ALL ON TABLE _view_data FROM sjt71;
GRANT SELECT ON TABLE _view_data TO public;

ALTER TABLE column_data OWNER TO dev;
GRANT ALL ON TABLE column_data TO dev;
REVOKE ALL ON TABLE column_data FROM sjt71;
REVOKE SELECT ON TABLE column_data FROM old_cos;
REVOKE SELECT ON TABLE column_data FROM mgmt_ro;
REVOKE SELECT ON TABLE column_data FROM space_management;
REVOKE SELECT ON TABLE column_data FROM selfservice;
GRANT SELECT ON TABLE column_data TO public;
