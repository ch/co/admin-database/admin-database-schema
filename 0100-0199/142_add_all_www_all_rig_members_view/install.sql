create or replace view www.all_rig_members_v1 as 
select 	crsid, research_interest_group.name as rig_name , 't'::boolean as is_primary
  from www.person_info_v8 
   join  mm_member_research_interest_group on www.person_info_v8.id=mm_member_research_interest_group.member_id
   join research_interest_group on research_interest_group.id=mm_member_research_interest_group.research_interest_group_id
   where mm_member_research_interest_group.is_primary='t'
UNION
select 	crsid, research_interest_group.name , 'f'::boolean as is_primary from www.person_info_v8 
   join  mm_member_research_interest_group on www.person_info_v8.id=mm_member_research_interest_group.member_id
   join research_interest_group on research_interest_group.id=mm_member_research_interest_group.research_interest_group_id
   where mm_member_research_interest_group.is_primary='f'
   order by crsid;

alter view www.all_rig_members_v1 owner to dev;
GRANT select ON TABLE www.all_rig_members_v1 TO www_sites;

