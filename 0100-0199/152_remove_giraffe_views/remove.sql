-- View: hotwire."10_View/_Giraffe_Body"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Body" AS 
 SELECT a.id, a.id AS postit_id
   FROM ( SELECT postit.id
           FROM postit
          WHERE postit.giraffe_id = 2
          ORDER BY postit.description) a;

ALTER TABLE hotwire."10_View/_Giraffe_Body"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Body" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Body" TO cos;

-- View: hotwire."10_View/_Giraffe_Done"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Done" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 5::bigint;

ALTER TABLE hotwire."10_View/_Giraffe_Done"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Done" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Done" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_AF"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_AF" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 6415;

ALTER TABLE hotwire."10_View/_Giraffe_Head_AF"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_AF" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_AF" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_ALT"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_ALT" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 5552;

ALTER TABLE hotwire."10_View/_Giraffe_Head_ALT"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_ALT" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_ALT" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_CEN"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_CEN" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 1;

ALTER TABLE hotwire."10_View/_Giraffe_Head_CEN"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_CEN" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_CEN" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_RC"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_RC" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 282;

ALTER TABLE hotwire."10_View/_Giraffe_Head_RC"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_RC" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_RC" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_RFL"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_RFL" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 247;

ALTER TABLE hotwire."10_View/_Giraffe_Head_RFL"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_RFL" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_RFL" TO cos;

-- View: hotwire."10_View/_Giraffe_Head_TKD"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Head_TKD" AS 
 SELECT postit.id, postit.id AS postit_id
   FROM postit
  WHERE postit.giraffe_id = 4::bigint AND postit.owner_id = 222;

ALTER TABLE hotwire."10_View/_Giraffe_Head_TKD"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Head_TKD" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Head_TKD" TO cos;

-- View: hotwire."10_View/_Giraffe_Neck"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_Neck" AS 
 SELECT a.id, a.id AS postit_id
   FROM ( SELECT postit.id
           FROM postit
          WHERE postit.giraffe_id = 3
          ORDER BY postit.neck_position, postit.coordinates[1]) a;

ALTER TABLE hotwire."10_View/_Giraffe_Neck"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_Neck" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_Neck" TO cos;

-- View: hotwire."10_View/_Giraffe_SEP"
CREATE OR REPLACE VIEW hotwire."10_View/_Giraffe_SEP" AS 
 SELECT a.id, a.id AS postit_id
   FROM ( SELECT postit.id
           FROM postit
          WHERE postit.giraffe_id = 1
          ORDER BY postit.description) a;

ALTER TABLE hotwire."10_View/_Giraffe_SEP"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/_Giraffe_SEP" TO postgres;
GRANT SELECT ON TABLE hotwire."10_View/_Giraffe_SEP" TO cos;

-- View: hotwire."10_View/Giraffe"
CREATE OR REPLACE VIEW hotwire."10_View/Giraffe" AS 
 SELECT 1::bigint AS id, ( SELECT 'Projects in flight: '::text || count(*)
           FROM postit
          WHERE postit.giraffe_id <> 5) AS "In flight", _to_hwsubviewb('10_View/_Giraffe_Head_RFL'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "RFL", _to_hwsubviewb('10_View/_Giraffe_Head_ALT'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "ALT", _to_hwsubviewb('10_View/_Giraffe_Head_CEN'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "CEN", _to_hwsubviewb('10_View/_Giraffe_Head_RC'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "RC", _to_hwsubviewb('10_View/_Giraffe_Head_AF'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "AF", _to_hwsubviewb('10_View/_Giraffe_Head_TKD'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "TKD", _to_hwsubviewb('10_View/_Giraffe_Neck'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "Neck", _to_hwsubviewb('10_View/_Giraffe_Body'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "Body", _to_hwsubviewb('10_View/_Giraffe_SEP'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "SEP", _to_hwsubviewb('10_View/_Giraffe_Done'::character varying, NULL::character varying, '10_View/Postits'::character varying, NULL::character varying, NULL::character varying) AS "Done";

ALTER TABLE hotwire."10_View/Giraffe"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Giraffe" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Giraffe" TO cos;

-- View: hotwire."10_View/_postit_drop_over"
CREATE OR REPLACE VIEW hotwire."10_View/_postit_drop_over" AS 
 SELECT 1::bigint AS id, 'Stub'::character varying AS result;

ALTER TABLE hotwire."10_View/_postit_drop_over"
  OWNER TO postgres;

CREATE OR REPLACE VIEW hotwire."10_View/Postits" AS 
 SELECT postit.id, postit.description, postit.coordinates, postit.owner_id AS postit_owner_id, postit.date_created, postit.target_end_date, postit.actual_end_date, postit.giraffe_id, postit.project_duration_id, postit.notes, postit.neck_position
   FROM postit;

ALTER TABLE hotwire."10_View/Postits"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Postits" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Postits" TO cos;

CREATE OR REPLACE RULE "hotwire_10_View/Postits_del" AS
    ON DELETE TO hotwire."10_View/Postits" DO INSTEAD  DELETE FROM postit
  WHERE postit.id = old.id;

CREATE OR REPLACE RULE "hotwire_10_View/Postits_ins" AS
    ON INSERT TO hotwire."10_View/Postits" DO INSTEAD  INSERT INTO postit (description, coordinates, owner_id, date_created, target_end_date, actual_end_date, giraffe_id, project_duration_id, notes, neck_position) 
  VALUES (new.description, new.coordinates, new.postit_owner_id, COALESCE(new.date_created::timestamp with time zone, now()), new.target_end_date, new.actual_end_date, new.giraffe_id, new.project_duration_id, new.notes, new.neck_position)
  RETURNING postit.id, postit.description, postit.coordinates, postit.owner_id, postit.date_created, postit.target_end_date, postit.actual_end_date, postit.giraffe_id, postit.project_duration_id, postit.notes, postit.neck_position;

CREATE OR REPLACE RULE "hotwire_10_View/Postits_upd" AS
    ON UPDATE TO hotwire."10_View/Postits" DO INSTEAD  UPDATE postit SET id = new.id, description = new.description, coordinates = new.coordinates, owner_id = new.postit_owner_id, date_created = COALESCE(new.date_created::timestamp with time zone, now()), target_end_date = new.target_end_date, actual_end_date = new.actual_end_date, giraffe_id = new.giraffe_id, project_duration_id = new.project_duration_id, notes = new.notes, neck_position = new.neck_position
  WHERE postit.id = old.id;

CREATE OR REPLACE VIEW hotwire.giraffe_hid AS 
 SELECT giraffe_hid.giraffe_id, giraffe_hid.giraffe_hid
   FROM giraffe_hid;

ALTER TABLE hotwire.giraffe_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.giraffe_hid TO postgres;
GRANT ALL ON TABLE hotwire.giraffe_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.project_duration_hid AS 
 SELECT project_duration_hid.id AS project_duration_id, project_duration_hid.description AS project_duration_hid
   FROM project_duration_hid;

ALTER TABLE hotwire.project_duration_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.project_duration_hid TO postgres;
GRANT SELECT ON TABLE hotwire.project_duration_hid TO ro_hid;
