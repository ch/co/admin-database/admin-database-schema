ALTER TABLE ip_address_view OWNER TO dev;
REVOKE ALL ON TABLE ip_address_view FROM sjt71;
REVOKE SELECT, UPDATE, INSERT ON TABLE ip_address_view FROM old_cos;
GRANT ALL ON TABLE ip_address_view TO dev;
GRANT SELECT ON TABLE ip_address_view TO dns;
GRANT SELECT ON TABLE ip_address_view TO osbuilder;
