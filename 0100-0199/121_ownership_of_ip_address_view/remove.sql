ALTER TABLE ip_address_view OWNER TO sjt71;
GRANT ALL ON TABLE ip_address_view TO sjt71;
GRANT SELECT, UPDATE, INSERT ON TABLE ip_address_view TO old_cos;
GRANT ALL ON TABLE ip_address_view TO dev;
GRANT SELECT ON TABLE ip_address_view TO dns;
GRANT SELECT ON TABLE ip_address_view TO osbuilder;
