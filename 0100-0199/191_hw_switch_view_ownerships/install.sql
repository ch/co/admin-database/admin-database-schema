ALTER TABLE hotwire."10_View/Network/Switch Config Fragments" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" FROM rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" TO dev;

ALTER TABLE hotwire."10_View/Network/_switch_config_subview" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/_switch_config_subview" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_View/Network/_switch_config_subview" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/_switch_config_subview" TO dev;

ALTER TABLE hotwire."10_View/Network/_Switchport_ro" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/_Switchport_ro" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_View/Network/_Switchport_ro" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/_Switchport_ro" TO dev;

ALTER TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" FROM rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" TO dev;

ALTER TABLE hotwire."10_View/Network/Switch_Port" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Port" FROM rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port" TO dev;

ALTER TABLE hotwire."10_View/Network/Switch_Configs_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" FROM rl201;

ALTER TABLE hotwire."10_View/Network/Switch_Config_Goals" OWNER TO dev;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" FROM rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" TO cos;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" TO dev;
