ALTER TABLE hotwire."10_View/Network/Switch Config Fragments" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch Config Fragments" FROM dev;

ALTER TABLE hotwire."10_View/Network/_switch_config_subview" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/_switch_config_subview" TO rl201;
GRANT SELECT ON TABLE hotwire."10_View/Network/_switch_config_subview" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/_switch_config_subview" FROM dev;

ALTER TABLE hotwire."10_View/Network/_Switchport_ro" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/_Switchport_ro" TO rl201;
GRANT SELECT ON TABLE hotwire."10_View/Network/_Switchport_ro" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/_Switchport_ro" FROM dev;;

ALTER TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Port_Config_Goals" FROM dev;

ALTER TABLE hotwire."10_View/Network/Switch_Port" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Port" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Port" FROM dev;

ALTER TABLE hotwire."10_View/Network/Switch_Configs_ro" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" FROM rl201;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Configs_ro" FROM dev;

ALTER TABLE hotwire."10_View/Network/Switch_Config_Goals" OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/Network/Switch_Config_Goals" FROM dev;
