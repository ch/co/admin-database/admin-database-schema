CREATE OR REPLACE FUNCTION fn_createuser(_crsid character varying)
  RETURNS void AS
$BODY$
declare

begin
 -- create postgres user account
  perform usename from pg_user where usename=_crsid;
  if not found then
   execute 'create user '||_crsid||' with in group _autoadded'; 
  end if;
 -- Create entry in user_account table
  if _crsid is not null then
   perform id from user_account where username=_crsid;
   if not found then
    insert into user_account (person_id,username,chemnet_token) values ((select id from person where crsid=_crsid),_crsid,random_string_lower(16));
   end if;
  end if; 
 end
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER
  COST 100;
ALTER FUNCTION fn_createuser(character varying)
  OWNER TO rl201;
COMMENT ON FUNCTION fn_createuser(character varying) IS 'A Privileged function which ensures that a postgres account exists for the user.';

REVOKE INSERT,SELECT ON public.user_account FROM useradder;
REVOKE SELECT ON public.person FROM useradder;
REVOKE USAGE ON public.user_account_id_seq FROM useradder;
DROP ROLE useradder;
