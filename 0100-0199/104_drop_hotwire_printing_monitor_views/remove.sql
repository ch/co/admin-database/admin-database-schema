CREATE OR REPLACE VIEW hotwire."10_View/Usage_Monitor" AS 
 SELECT hardware.id, hardware.name, hardware.hardware_type_id, hardware.room_id, hardware.log_usage, hardware.usage_url
   FROM hardware
  WHERE hardware.log_usage = true;

ALTER TABLE hotwire."10_View/Usage_Monitor"
  OWNER TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/Usage_Monitor" TO sjt71;
GRANT SELECT ON TABLE hotwire."10_View/Usage_Monitor" TO old_cos;
GRANT SELECT ON TABLE hotwire."10_View/Usage_Monitor" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Usage_Monitor" TO secretariat;

CREATE OR REPLACE VIEW hotwire."10_View/Print_Jobs" AS 
 SELECT print_job.id, print_job.hardware_id, person.id AS person_id, print_job.owner, print_job.date, print_job.title, print_job.application, print_job.mono_sides, print_job.colour_sides, print_job.sheets, NULL::text AS _view
   FROM print_job
   JOIN hardware ON print_job.hardware_id = hardware.id
   LEFT JOIN person ON print_job.owner::text = person.crsid::text
  ORDER BY print_job.owner, print_job.date;

ALTER TABLE hotwire."10_View/Print_Jobs"
  OWNER TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/Print_Jobs" TO sjt71;
GRANT SELECT ON TABLE hotwire."10_View/Print_Jobs" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Print_Jobs" TO secretariat;

