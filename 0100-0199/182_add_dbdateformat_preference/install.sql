INSERT INTO hotwire."hw_Preferences" (hw_preference_name,hw_preference_type_id,hw_preference_const) VALUES ('Database internal date format', 3, 'DBDATEFORMAT');

INSERT INTO hotwire."hw_User Preferences" (preference_id,preference_value) VALUES ( (SELECT id FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DBDATEFORMAT'), 'd/m/Y');
INSERT INTO hotwire."hw_User Preferences" (preference_id,preference_value) VALUES ( (SELECT id FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DATEFORMAT'), 'd/m/Y');
