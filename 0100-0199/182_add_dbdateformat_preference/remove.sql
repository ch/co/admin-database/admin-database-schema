DELETE FROM hotwire."hw_User Preferences" WHERE preference_id = (SELECT id FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DBDATEFORMAT') AND user_id IS NULL;
DELETE FROM hotwire."hw_User Preferences" WHERE preference_id = (SELECT id FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DATEFORMAT') AND user_id IS NULL;
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DBDATEFORMAT';
