insert into dns_domains ( id, name, type ) values ( 122, 'admin.private.cam.ac.uk', 'NATIVE');
insert into subnet ( id, network_address, router, domain_name, provide_dhcp_service, dns_domain_id, notes, vlan_id, monitor_with_hobbit, is_pool_subnet, subnetmask )
 values ( 126,'172.23.55.128/27', '172.23.55.158', 'admin.private.cam.ac.uk', 'f', 122, 'BMS Management', 5, 't', 'f', '255.255.255.224')
;

 update ip_address set hostname = $$gw-3460.route-south.net.private.cam.ac.uk$$, allow_free_choice_of_hostname ='t', hobbit_flags = 'dialup'  where ip = $$172.23.55.157/32$$;
 update ip_address set hostname = $$gw-3460.route-oadd.net.private.cam.ac.uk$$, allow_free_choice_of_hostname ='t', hobbit_flags = 'dialup'  where ip = $$172.23.55.156/32$$;
 update ip_address set hostname = $$gw-3460.net.private.cam.ac.uk$$, allow_free_choice_of_hostname ='t'  where ip = $$172.23.55.158/32$$;
 update ip_address set hostname = $$chem22-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.151/32$$;
 update ip_address set hostname = $$chem23-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.152/32$$;
 update ip_address set hostname = $$chem24-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.153/32$$;
 update ip_address set hostname = $$chem25-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.154/32$$;
 update ip_address set hostname = $$chem26-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.155/32$$;
 update ip_address set hostname = $$chemistry1-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.129/32$$;
 update ip_address set hostname = $$chemistry2-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.130/32$$;
 update ip_address set hostname = $$chemistry3-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.131/32$$;
 update ip_address set hostname = $$chemistry4-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.132/32$$;
 update ip_address set hostname = $$chemistry5-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.133/32$$;
 update ip_address set hostname = $$chemistry6-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.134/32$$;
 update ip_address set hostname = $$chemistry7-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.135/32$$;
 update ip_address set hostname = $$chemistry8-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.136/32$$;
 update ip_address set hostname = $$chemistry9-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.137/32$$;
 update ip_address set hostname = $$chemistry10-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.138/32$$;
 update ip_address set hostname = $$chemistry11-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.139/32$$;
 update ip_address set hostname = $$chemistry12-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.140/32$$;
 update ip_address set hostname = $$chemistry13-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.141/32$$;
 update ip_address set hostname = $$chemistry14-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.142/32$$;
 update ip_address set hostname = $$chemistry16-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.144/32$$;
 update ip_address set hostname = $$chemistry17-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.145/32$$;
 update ip_address set hostname = $$chem-emic1-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.146/32$$;
 update ip_address set hostname = $$chem18-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.147/32$$;
 update ip_address set hostname = $$chem19-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.148/32$$;
 update ip_address set hostname = $$chem20-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.149/32$$;
 update ip_address set hostname = $$chem21-bms.admin.private.cam.ac.uk$$ where ip = $$172.23.55.150/32$$;

