CREATE OR REPLACE VIEW print_jobs_report AS 
 SELECT print_job.id, print_job.hardware_id, person.id AS person_id, print_job.owner, print_job.date, print_job.title, print_job.application, print_job.mono_sides, print_job.colour_sides, print_job.sheets, NULL::text AS _view
   FROM print_job
   JOIN hardware ON print_job.hardware_id = hardware.id
   LEFT JOIN person ON print_job.owner::text = person.crsid::text
  ORDER BY print_job.owner, print_job.date;

ALTER TABLE print_jobs_report
  OWNER TO sjt71;
GRANT ALL ON TABLE print_jobs_report TO sjt71;
GRANT SELECT ON TABLE print_jobs_report TO dev;
GRANT SELECT ON TABLE print_jobs_report TO secretariat;

CREATE OR REPLACE VIEW print_jobs_subview AS 
 SELECT print_job.id, print_job.hardware_id, person.id AS person_id, research_group.head_of_group_id, print_job.owner, print_job.date, print_job.title, print_job.application, print_job.mono_sides, print_job.colour_sides, print_job.sheets, NULL::text AS _targetview
   FROM print_job
   JOIN hardware ON print_job.hardware_id = hardware.id
   LEFT JOIN person ON print_job.owner::text = person.crsid::text
   LEFT JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
  ORDER BY print_job.date DESC, person.id;

ALTER TABLE print_jobs_subview
  OWNER TO sjt71;
GRANT ALL ON TABLE print_jobs_subview TO sjt71;
GRANT SELECT ON TABLE print_jobs_subview TO secretariat;
GRANT SELECT ON TABLE print_jobs_subview TO mgmt_ro;
GRANT ALL ON TABLE print_jobs_subview TO dev;
