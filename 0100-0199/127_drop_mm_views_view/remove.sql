CREATE OR REPLACE VIEW _mm_views AS 
 SELECT pg_class.relname AS view
   FROM pg_class
  WHERE pg_class.relname ~~ 'mm_%'::text AND (pg_class.relkind = 'r'::"char" OR pg_class.relkind = 'v'::"char");

ALTER TABLE _mm_views OWNER TO sjt71;
GRANT ALL ON TABLE _mm_views TO sjt71;
GRANT SELECT ON TABLE _mm_views TO public;

