CREATE VIEW public.cambridge_college_hid AS
SELECT cambridge_college.id AS cambridge_college_id, cambridge_college.name AS cambridge_college_hid FROM public.cambridge_college;


ALTER TABLE public.cambridge_college_hid OWNER TO dev;

grant select on public.cambridge_college_hid to selfservice;
