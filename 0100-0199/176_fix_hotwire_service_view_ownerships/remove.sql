ALTER TABLE hotwire._column_data OWNER TO rl201;
GRANT ALL ON TABLE hotwire._column_data TO rl201;
GRANT ALL ON TABLE hotwire._column_data TO dev;
GRANT SELECT ON TABLE hotwire._column_data TO public;

ALTER TABLE hotwire._role_data OWNER TO rl201;
GRANT ALL ON TABLE hotwire._role_data TO rl201;
GRANT ALL ON TABLE hotwire._role_data TO public;
GRANT ALL ON TABLE hotwire._role_data TO cos;
