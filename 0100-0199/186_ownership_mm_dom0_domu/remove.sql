ALTER TABLE mm_dom0_domu OWNER TO rl201;
GRANT ALL ON TABLE mm_dom0_domu TO rl201;
GRANT ALL ON TABLE mm_dom0_domu TO cos;
GRANT ALL ON TABLE mm_dom0_domu TO _api_dom0domu;
REVOKE ALL ON TABLE mm_dom0_domu FROM dev;
GRANT SELECT ON TABLE mm_dom0_domu TO dev;
