CREATE OR REPLACE VIEW hotwire.desk_hid AS 
 SELECT desk.id AS desk_id, (COALESCE(desk.description, 'Desk in'::text) || ' '::text) || room_hid.room_hid::text AS desk_hid
   FROM desk
   JOIN hotwire.room_hid USING (room_id);

ALTER TABLE hotwire.desk_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.desk_hid TO postgres;
GRANT SELECT ON TABLE hotwire.desk_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.future_person_hid AS 
 SELECT future_person.id AS future_person_id, future_person.name::text || COALESCE((((' ['::text || future_person.start_date) || ' to '::text) || future_person.end_date) || ']'::text, ''::text) AS future_person_hid
   FROM future_person;

ALTER TABLE hotwire.future_person_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.future_person_hid TO postgres;
GRANT ALL ON TABLE hotwire.future_person_hid TO ro_hid;

