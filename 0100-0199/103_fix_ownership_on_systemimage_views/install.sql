-- Make owner dev not sjt71
-- Give rights to COs, remove from old_cos
ALTER TABLE hotwire."10_View/System_Image_Asset" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Asset" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_Asset" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_Asset" from old_cos;

ALTER TABLE hotwire."10_View/System_Image_Basic" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Basic" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_Basic" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_Basic" from old_cos;

ALTER TABLE hotwire."10_View/System_Image_User" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/System_Image_User" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_User" TO cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_User" from old_cos;
