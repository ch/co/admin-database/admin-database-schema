ALTER TABLE hotwire."10_View/System_Image_Asset" OWNER TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Asset" TO sjt71;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_Asset" TO old_cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_Asset"  FROM cos;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Asset" TO dev;

ALTER TABLE hotwire."10_View/System_Image_Basic" OWNER TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Basic" TO sjt71;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_Basic" TO old_cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_Basic"  FROM cos;
GRANT ALL ON TABLE hotwire."10_View/System_Image_Basic" TO dev;

ALTER TABLE hotwire."10_View/System_Image_User" OWNER TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/System_Image_User" TO sjt71;
GRANT ALL ON TABLE hotwire."10_View/System_Image_User" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/System_Image_User" TO old_cos;
REVOKE ALL ON TABLE hotwire."10_View/System_Image_User"  FROM cos;

