CREATE OR REPLACE VIEW easy_add_machine_view AS 
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id, ''::character varying(30) AS hostname, NULL::integer AS subnet_id, NULL::macaddr AS wired_mac_1, ( SELECT operating_system_hid.operating_system_id
           FROM operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text ~~* '%debian%'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id, 'Virtual'::character varying(20) AS manufacturer, 'Xen'::character varying(20) AS model, ''::character varying(20) AS hardware_name, ( SELECT hardware_type_hid.hardware_type_id
           FROM hardware_type_hid
          WHERE hardware_type_hid.hardware_type_hid::text ~~* '%server%'::text
         LIMIT 1) AS hardware_type_id, ''::character varying(80) AS system_image_comments, NULL::integer AS host_system_image_id, ( SELECT mm_person_room.room_id
           FROM person
      JOIN mm_person_room ON person.id = mm_person_room.person_id
     WHERE person.crsid::name = "current_user"()
    LIMIT 1) AS room_id, NULL::macaddr AS wired_mac_2, ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id, ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS owner_id, ( SELECT mm_person_research_group.research_group_id
           FROM person
      JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
     WHERE person.crsid::name = "current_user"()
    LIMIT 1) AS research_group_id;

ALTER TABLE easy_add_machine_view
  OWNER TO rl201;
GRANT ALL ON TABLE easy_add_machine_view TO rl201;
GRANT ALL ON TABLE easy_add_machine_view TO cos;

-- Rule: easy_add_machine_view_upd ON easy_add_machine_view

-- DROP RULE easy_add_machine_view_upd ON easy_add_machine_view;

CREATE OR REPLACE RULE easy_add_machine_view_upd AS
    ON UPDATE TO easy_add_machine_view DO INSTEAD ( INSERT INTO hardware (manufacturer, model, room_id, hardware_type_id, owner_id, name) 
  VALUES (new.manufacturer, new.model, new.room_id, new.hardware_type_id, new.owner_id, new.hardware_name);
 INSERT INTO system_image (wired_mac_1, wired_mac_2, hardware_id, operating_system_id, comments, host_system_image_id, user_id, research_group_id) 
  VALUES (new.wired_mac_1, new.wired_mac_2, currval('hardware_id_seq'::regclass), new.operating_system_id, new.system_image_comments, new.host_system_image_id, new.user_id, new.research_group_id);
 UPDATE ip_address SET hostname = new.hostname
  WHERE (ip_address.id IN ( SELECT ip_address.id
           FROM ip_address
          WHERE ip_address.subnet_id = new.subnet_id AND ip_address.reserved <> true AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
         LIMIT 1));
 INSERT INTO mm_system_image_ip_address (ip_address_id, system_image_id) 
  VALUES (( SELECT ip_address.id
           FROM ip_address
          WHERE ip_address.subnet_id = new.subnet_id AND ip_address.reserved <> true AND ip_address.hostname::text = ((new.hostname::text || '.'::text) || ((( SELECT subnet.domain_name
                   FROM subnet
                  WHERE subnet.id = new.subnet_id))::text))
         LIMIT 1), currval('system_image_id_seq'::regclass));
);


