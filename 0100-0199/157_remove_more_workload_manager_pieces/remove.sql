CREATE OR REPLACE VIEW hotwire."10_Teaching/Units" AS 
 SELECT todb_units.id, todb_units.name, todb_units.todb_course_id, todb_units.sort_order, todb_units.description, todb_units.note
   FROM todb_units;

ALTER TABLE hotwire."10_Teaching/Units"
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_Teaching/Units" TO rl201;
GRANT ALL ON TABLE hotwire."10_Teaching/Units" TO todb;

-- Rule: "hotwire_10_Teaching/Units_del" ON hotwire."10_Teaching/Units"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Units_del" AS
    ON DELETE TO hotwire."10_Teaching/Units" DO INSTEAD  DELETE FROM todb_units
  WHERE todb_units.id = old.id;

-- Rule: "hotwire_10_Teaching/Units_ins" ON hotwire."10_Teaching/Units"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Units_ins" AS
    ON INSERT TO hotwire."10_Teaching/Units" DO INSTEAD  INSERT INTO todb_units (name, todb_course_id, sort_order, description, note) 
  VALUES (new.name, new.todb_course_id, new.sort_order, new.description, new.note)
  RETURNING todb_units.id, todb_units.name, todb_units.todb_course_id, todb_units.sort_order, todb_units.description, todb_units.note;

-- Rule: "hotwire_10_Teaching/Units_upd" ON hotwire."10_Teaching/Units"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Units_upd" AS
    ON UPDATE TO hotwire."10_Teaching/Units" DO INSTEAD  UPDATE todb_units SET id = new.id, name = new.name, todb_course_id = new.todb_course_id, sort_order = new.sort_order, description = new.description, note = new.note
  WHERE todb_units.id = old.id;

-- View: hotwire."10_Teaching/Point_Formulae"

CREATE OR REPLACE VIEW hotwire."10_Teaching/Point_Formulae" AS 
 SELECT todb_point_formulae.id, todb_point_formulae.name, todb_point_formulae.description, todb_point_formulae.multiplier, todb_point_formulae."offset", todb_point_formulae.year
   FROM todb_point_formulae;

ALTER TABLE hotwire."10_Teaching/Point_Formulae"
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_Teaching/Point_Formulae" TO rl201;
GRANT ALL ON TABLE hotwire."10_Teaching/Point_Formulae" TO todb;

-- Rule: "hotwire_10_Teaching/Point_Formulae_del" ON hotwire."10_Teaching/Point_Formulae"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Point_Formulae_del" AS
    ON DELETE TO hotwire."10_Teaching/Point_Formulae" DO INSTEAD  DELETE FROM todb_point_formulae
  WHERE todb_point_formulae.id = old.id;

-- Rule: "hotwire_10_Teaching/Point_Formulae_ins" ON hotwire."10_Teaching/Point_Formulae"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Point_Formulae_ins" AS
    ON INSERT TO hotwire."10_Teaching/Point_Formulae" DO INSTEAD  INSERT INTO todb_point_formulae (name, description, multiplier, "offset", year) 
  VALUES (new.name, new.description, new.multiplier, new."offset", new.year)
  RETURNING todb_point_formulae.id, todb_point_formulae.name, todb_point_formulae.description, todb_point_formulae.multiplier, todb_point_formulae."offset", todb_point_formulae.year;

-- Rule: "hotwire_10_Teaching/Point_Formulae_upd" ON hotwire."10_Teaching/Point_Formulae"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Point_Formulae_upd" AS
    ON UPDATE TO hotwire."10_Teaching/Point_Formulae" DO INSTEAD  UPDATE todb_point_formulae SET id = new.id, name = new.name, description = new.description, multiplier = new.multiplier, "offset" = new."offset", year = new.year
  WHERE todb_point_formulae.id = old.id;

-- View: hotwire."10_Teaching/Jobs"

CREATE OR REPLACE VIEW hotwire."10_Teaching/Jobs" AS 
 SELECT todb_jobs.id, todb_jobs.todb_course_id, todb_jobs.todb_course_year, todb_jobs.todb_paper_id, todb_jobs.name, todb_jobs.todb_jobtype_id, todb_jobs.hours, todb_jobs.todb_term_id, todb_jobs.person_id, todb_jobs.points, todb_jobs.note, todb_jobs.deleted, todb_jobs.todb_formula_id, todb_jobs.academic_year
   FROM todb_jobs;

ALTER TABLE hotwire."10_Teaching/Jobs"
  OWNER TO rl201;

-- Rule: "hotwire_10_Teaching/Jobs_del" ON hotwire."10_Teaching/Jobs"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Jobs_del" AS
    ON DELETE TO hotwire."10_Teaching/Jobs" DO INSTEAD  DELETE FROM todb_jobs
  WHERE todb_jobs.id = old.id;

-- Rule: "hotwire_10_Teaching/Jobs_ins" ON hotwire."10_Teaching/Jobs"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Jobs_ins" AS
    ON INSERT TO hotwire."10_Teaching/Jobs" DO INSTEAD  INSERT INTO todb_jobs (todb_course_id, todb_course_year, todb_paper_id, name, todb_jobtype_id, hours, todb_term_id, person_id, points, note, deleted, todb_formula_id, academic_year) 
  VALUES (new.todb_course_id, new.todb_course_year, new.todb_paper_id, new.name, new.todb_jobtype_id, new.hours, new.todb_term_id, new.person_id, new.points, new.note, new.deleted, new.todb_formula_id, new.academic_year)
  RETURNING todb_jobs.id, todb_jobs.todb_course_id, todb_jobs.todb_course_year, todb_jobs.todb_paper_id, todb_jobs.name, todb_jobs.todb_jobtype_id, todb_jobs.hours, todb_jobs.todb_term_id, todb_jobs.person_id, todb_jobs.points, todb_jobs.note, todb_jobs.deleted, todb_jobs.todb_formula_id, todb_jobs.academic_year;

-- Rule: "hotwire_10_Teaching/Jobs_upd" ON hotwire."10_Teaching/Jobs"

CREATE OR REPLACE RULE "hotwire_10_Teaching/Jobs_upd" AS
    ON UPDATE TO hotwire."10_Teaching/Jobs" DO INSTEAD  UPDATE todb_jobs SET id = new.id, todb_course_id = new.todb_course_id, todb_course_year = new.todb_course_year, todb_paper_id = new.todb_paper_id, name = new.name, todb_jobtype_id = new.todb_jobtype_id, hours = new.hours, todb_term_id = new.todb_term_id, person_id = new.person_id, points = new.points, note = new.note, deleted = new.deleted, todb_formula_id = new.todb_formula_id, academic_year = new.academic_year
  WHERE todb_jobs.id = old.id;


