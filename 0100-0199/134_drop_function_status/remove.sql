CREATE OR REPLACE FUNCTION status(v_person_id bigint)
  RETURNS text AS
$BODY$
declare v_status text; begin select max_history.status into v_status from (select status, max(start_date) from (select person_id as id, visitor_type as status, start_date from visitorship union select person_id as id, postgraduate_studentship_type_hid as status, start_date from postgraduate_studentship join postgraduate_studentship_type_hid using (postgraduate_studentship_type_id) union select person_id as id, 'Erasmus/Socrates' as status, start_date from erasmus_socrates_studentship union select person_id as id, staff_category_hid.staff_category_hid as status, post_history.start_date from post_history join staff_post on staff_post_id=staff_post.id join staff_category_hid using (staff_category_id) union select person_id as id, 'Part III Undergrad' as status, start_date from part_iii_studentship) history where history.id = v_person_id group by status) max_history;
IF v_status is not null then return v_status; else return 'External'; end if;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION status(bigint)
  OWNER TO sjt71;

