CREATE OR REPLACE VIEW post_funding_subview AS 
 SELECT post_funded.id, post_funded.post_history_id, post_funded.funding_source_id, post_funded.percentage, post_funded.start_date, post_funded.end_date
   FROM post_funded
  ORDER BY post_funded.start_date;

ALTER TABLE post_funding_subview
  OWNER TO sjt71;
GRANT ALL ON TABLE post_funding_subview TO sjt71;
GRANT ALL ON TABLE post_funding_subview TO dev;
