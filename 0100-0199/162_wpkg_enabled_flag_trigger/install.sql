-- create function
CREATE FUNCTION system_image_reset_wpkg_enabled() RETURNS TRIGGER AS
$BODY$
    BEGIN
    IF new.operating_system_id <> old.operating_system_id THEN
        new.wpkg_enabled = 'f';
    END IF;
    RETURN new;
    END;
$BODY$
LANGUAGE plpgsql;

ALTER FUNCTION system_image_reset_wpkg_enabled() OWNER TO dev;

-- attach function to system_image
CREATE TRIGGER system_image_wpkg_enabled_trig BEFORE UPDATE ON system_image FOR EACH ROW EXECUTE PROCEDURE system_image_reset_wpkg_enabled();
