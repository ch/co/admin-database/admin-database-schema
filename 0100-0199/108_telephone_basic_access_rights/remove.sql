GRANT SELECT ON TABLE hotwire."10_View/Telephone_Basic" TO building_security;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO admin_team;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO student_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO space_management;
