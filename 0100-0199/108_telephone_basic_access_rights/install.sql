REVOKE SELECT ON TABLE hotwire."10_View/Telephone_Basic" FROM building_security;
REVOKE SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" FROM admin_team;
REVOKE SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" FROM student_management;
REVOKE SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" FROM space_management;
