--
-- Name: giraffe_hid; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE giraffe_hid (
    giraffe_id bigint NOT NULL,
    giraffe_hid character varying
);


ALTER TABLE public.giraffe_hid OWNER TO postgres;

--
-- Name: giraffe_hid_giraffe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE giraffe_hid_giraffe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.giraffe_hid_giraffe_id_seq OWNER TO postgres;

--
-- Name: giraffe_hid_giraffe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE giraffe_hid_giraffe_id_seq OWNED BY giraffe_hid.giraffe_id;


--
-- Name: giraffe_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY giraffe_hid ALTER COLUMN giraffe_id SET DEFAULT nextval('giraffe_hid_giraffe_id_seq'::regclass);


--
-- Data for Name: giraffe_hid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY giraffe_hid (giraffe_id, giraffe_hid) FROM stdin;
1	SEP
2	Body
3	Neck
4	Head
5	Done
\.


--
-- Name: giraffe_hid_giraffe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('giraffe_hid_giraffe_id_seq', 5, true);


--
-- Name: giraffe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY giraffe_hid
    ADD CONSTRAINT giraffe_pkey PRIMARY KEY (giraffe_id);


--
-- Name: giraffe_hid; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE giraffe_hid FROM PUBLIC;
REVOKE ALL ON TABLE giraffe_hid FROM postgres;
GRANT ALL ON TABLE giraffe_hid TO postgres;
GRANT SELECT ON TABLE giraffe_hid TO ro_hid;


--
-- PostgreSQL database dump complete
--


--
-- Name: project_duration_hid; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE project_duration_hid (
    id bigint NOT NULL,
    description character varying
);


ALTER TABLE public.project_duration_hid OWNER TO postgres;

--
-- Name: project_duration_hid_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE project_duration_hid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_duration_hid_id_seq OWNER TO postgres;

--
-- Name: project_duration_hid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE project_duration_hid_id_seq OWNED BY project_duration_hid.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_duration_hid ALTER COLUMN id SET DEFAULT nextval('project_duration_hid_id_seq'::regclass);


--
-- Data for Name: project_duration_hid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY project_duration_hid (id, description) FROM stdin;
1	minutes
2	hours
3	days
4	months
5	weeks
\.


--
-- Name: project_duration_hid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('project_duration_hid_id_seq', 5, true);


--
-- Name: project_duration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY project_duration_hid
    ADD CONSTRAINT project_duration_pkey PRIMARY KEY (id);


--
-- Name: project_duration_hid; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE project_duration_hid FROM PUBLIC;
REVOKE ALL ON TABLE project_duration_hid FROM postgres;
GRANT ALL ON TABLE project_duration_hid TO postgres;
GRANT SELECT ON TABLE project_duration_hid TO ro_hid;


--
-- PostgreSQL database dump complete
--


CREATE TABLE postit (
    id bigint NOT NULL,
    description character varying,
    coordinates point,
    owner_id bigint,
    date_created date,
    target_end_date date,
    actual_end_date date,
    giraffe_id bigint,
    project_duration_id bigint,
    notes character varying,
    neck_position integer
);


ALTER TABLE public.postit OWNER TO dev;

--
-- Name: postit_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE postit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.postit_id_seq OWNER TO dev;

--
-- Name: postit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE postit_id_seq OWNED BY postit.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY postit ALTER COLUMN id SET DEFAULT nextval('postit_id_seq'::regclass);


--
-- Name: postit_neckpos_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY postit
    ADD CONSTRAINT postit_neckpos_uniq UNIQUE (neck_position);


--
-- Name: postit_pk; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY postit
    ADD CONSTRAINT postit_pk PRIMARY KEY (id);


--
-- Name: fki_postit_duration_fkey; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX fki_postit_duration_fkey ON postit USING btree (project_duration_id);


--
-- Name: fki_postit_giraffe_fky; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX fki_postit_giraffe_fky ON postit USING btree (giraffe_id);


--
-- Name: fki_postit_person_fkey; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX fki_postit_person_fkey ON postit USING btree (owner_id);


--
-- Name: giraffe_neck_manager_trig; Type: TRIGGER; Schema: public; Owner: dev
--

CREATE TRIGGER giraffe_neck_manager_trig BEFORE INSERT OR UPDATE ON postit FOR EACH ROW EXECUTE PROCEDURE giraffe_neck_manager();


--
-- Name: postit_duration_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY postit
    ADD CONSTRAINT postit_duration_fkey FOREIGN KEY (project_duration_id) REFERENCES project_duration_hid(id);


--
-- Name: postit_giraffe_fky; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY postit
    ADD CONSTRAINT postit_giraffe_fky FOREIGN KEY (giraffe_id) REFERENCES giraffe_hid(giraffe_id);


--
-- Name: postit_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY postit
    ADD CONSTRAINT postit_person_fkey FOREIGN KEY (owner_id) REFERENCES person(id);


--
-- Name: postit; Type: ACL; Schema: public; Owner: dev
--

REVOKE ALL ON TABLE postit FROM PUBLIC;
REVOKE ALL ON TABLE postit FROM dev;
GRANT ALL ON TABLE postit TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE postit TO cos;

