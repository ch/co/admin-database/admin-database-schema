CREATE OR REPLACE VIEW hw4_viewlist AS 
 SELECT tables.table_name, tables.is_insertable_into::character varying::text = 'YES'::text AND has_table_privilege(('hotwire."'::text || tables.table_name::text) || '"'::text, 'insert'::text) AS can_ins, has_table_privilege(('hotwire."'::text || tables.table_name::text) || '"'::text, 'delete'::text) AS can_del, has_table_privilege(('hotwire."'::text || tables.table_name::text) || '"'::text, 'update'::text) AS can_upd
   FROM information_schema.tables
  WHERE tables.table_schema::text = 'hotwire'::text AND tables.table_name::text ~~ '%/%'::text AND tables.table_type::text = 'VIEW'::text AND has_table_privilege(('hotwire."'::text || tables.table_name::text) || '"'::text, 'select'::text);

ALTER TABLE hw4_viewlist
  OWNER TO rl201;

CREATE OR REPLACE VIEW hw4_views AS 
 SELECT regexp_replace(regexp_replace(views.table_name::text, '//.*'::text, ''::text), '/[^/]*$'::text, ''::text) AS path, regexp_replace(regexp_replace(views.table_name::text, '//.*'::text, ''::text), '.*/'::text, ''::text) AS vname, regexp_replace(views.table_name::text, '//.*'::text, '/*'::text) AS mname, views.table_name
   FROM information_schema.views
  WHERE views.table_schema::text = 'hotwire'::text AND views.table_name::text ~~ '%/%'::text;

ALTER TABLE hw4_views
  OWNER TO rl201;

CREATE OR REPLACE VIEW hw4_menu AS 
 SELECT DISTINCT regexp_replace(regexp_replace(hw4_views.path, '[0-9]+_'::text, ''::text, 'g'::text), '_'::text, ' '::text, 'g'::text) AS displaypath, hw4_views.path
   FROM hw4_views
  ORDER BY hw4_views.path;

ALTER TABLE hw4_menu
  OWNER TO rl201;


