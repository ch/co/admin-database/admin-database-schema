CREATE TABLE _preferences (
    rolname name NOT NULL,
    view name
);

ALTER TABLE public._preferences OWNER TO sjt71;


ALTER TABLE ONLY _preferences
    ADD CONSTRAINT pk_preferences PRIMARY KEY (rolname);

REVOKE ALL ON TABLE _preferences FROM PUBLIC;
REVOKE ALL ON TABLE _preferences FROM sjt71;
GRANT ALL ON TABLE _preferences TO sjt71;
GRANT SELECT ON TABLE _preferences TO PUBLIC;

