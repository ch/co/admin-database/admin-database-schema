DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'UNACCENT';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DATEFORMAT';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'SHOWHINTS';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'POSTUPDATEOPT';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'POSTINSERTOPT';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'XDEBUG';
DELETE FROM hotwire."hw_Preferences" WHERE hw_preference_const = 'DBDEV';

DELETE FROM hotwire."hw_User Preferences" where hw_preference_const = 'UNACCENT' and user_id is null;
