INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Remove accents from search', 1, 'UNACCENT');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Date format', 3, 'DATEFORMAT');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Show web interface hints', 1, 'SHOWHINTS');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Show post-update options', 1, 'POSTUPDATEOPT');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Show post-insert options', 1, 'POSTINSERTOPT');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'Offer xdebug option to developers', 1, 'XDEBUG');
INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id, hw_preference_const) VALUES ( 'DB Development group', 3, 'DBDEV');

-- if we don't have this set it whinges
INSERT INTO hotwire."hw_User Preferences" ( preference_id, preference_value, user_id ) VALUES ( (select id from hotwire."hw_Preferences" where hw_preference_const = 'UNACCENT'), 'FALSE', null );
