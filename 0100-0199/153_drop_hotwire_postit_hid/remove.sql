CREATE OR REPLACE VIEW hotwire.postit_hid AS 
 SELECT postit.id AS postit_id, postit.description::text || COALESCE((' ['::text || person.crsid::text) || ']'::text, ''::text) AS postit_hid
   FROM postit
   LEFT JOIN person ON postit.owner_id = person.id;

ALTER TABLE hotwire.postit_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.postit_hid TO postgres;
GRANT SELECT ON TABLE hotwire.postit_hid TO ro_hid;

