DROP RULE telephone_basic_upd ON telephone_basic_view;
DROP RULE telephone_basic_ins ON telephone_basic_view;
DROP RULE telephone_basic_del ON telephone_basic_view;
DROP FUNCTION fn_telephone_basic_upd(telephone_basic_view);
DROP VIEW telephone_basic_view;
