CREATE OR REPLACE VIEW telephone_basic_view AS 
 SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, person_hid.person_hid AS mm_person, mm_person_dept_telephone_number.person_id AS mm_person_id, dept_telephone_number.fax
   FROM dept_telephone_number
   LEFT JOIN mm_person_dept_telephone_number ON dept_telephone_number.id = mm_person_dept_telephone_number.dept_telephone_number_id
   LEFT JOIN person_hid USING (person_id);

ALTER TABLE telephone_basic_view
  OWNER TO sjt71;
GRANT ALL ON TABLE telephone_basic_view TO sjt71;
GRANT ALL ON TABLE telephone_basic_view TO dev;
GRANT SELECT ON TABLE telephone_basic_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE telephone_basic_view TO admin_team;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE telephone_basic_view TO old_cos;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE telephone_basic_view TO space_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE telephone_basic_view TO student_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE telephone_basic_view TO phones_management;

CREATE OR REPLACE FUNCTION fn_telephone_basic_upd(telephone_basic_view)
  RETURNS bigint AS
$BODY$

declare
        v alias for $1;
        v_dept_telephone_number_id BIGINT;
begin

IF v.id IS NOT NULL THEN

  v_dept_telephone_number_id := v.id;

  UPDATE dept_telephone_number SET
        extension_number=v.extension_number, room_id=v.room_id, fax=v.fax
  WHERE dept_telephone_number.id = v.id;

ELSE

  v_dept_telephone_number_id := nextval('dept_telephone_number_id_seq');

  INSERT INTO dept_telephone_number
        (id, extension_number, room_id, fax)
  VALUES (v_dept_telephone_number_id, v.extension_number, v.room_id, v.fax);

END IF;

PERFORM fn_upd_many_many (v_dept_telephone_number_id, v.mm_person, 
                        'dept_telephone_number', 'person');

return v_dept_telephone_number_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_telephone_basic_upd(telephone_basic_view)
  OWNER TO sjt71;


-- Rule: telephone_basic_del ON telephone_basic_view

CREATE OR REPLACE RULE telephone_basic_del AS
    ON DELETE TO telephone_basic_view DO INSTEAD  DELETE FROM dept_telephone_number
  WHERE dept_telephone_number.id = old.id;

-- Rule: telephone_basic_ins ON telephone_basic_view

CREATE OR REPLACE RULE telephone_basic_ins AS
    ON INSERT TO telephone_basic_view DO INSTEAD  SELECT fn_telephone_basic_upd(new.*) AS id;

-- Rule: telephone_basic_upd ON telephone_basic_view

CREATE OR REPLACE RULE telephone_basic_upd AS
    ON UPDATE TO telephone_basic_view DO INSTEAD  SELECT fn_telephone_basic_upd(new.*) AS id;


