CREATE OR REPLACE VIEW _pg_hist AS 
 SELECT pg.id AS _pg_id, pg.person_id AS _person_id, pg.start_date, COALESCE(pg.phd_date_awarded, pg.date_withdrawn_from_register) AS end_date, postgraduate_studentship_type.name AS "position", pg.first_supervisor_id AS supervisor_person_id, pg.filemaker_funding AS funding
   FROM postgraduate_studentship pg
   JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = pg.postgraduate_studentship_type_id;

ALTER TABLE _pg_hist
  OWNER TO sjt71;

CREATE OR REPLACE VIEW _sp_hist AS 
 SELECT ph.id AS _ph_id, ph.person_id AS _person_id, ph.start_date, ph.end_date, sp.name AS "position", ph.supervisor_id AS supervisor_person_id, ph.filemaker_funding AS funding
   FROM post_history ph
   JOIN staff_post sp ON sp.id = ph.staff_post_id;

ALTER TABLE _sp_hist
  OWNER TO sjt71;


CREATE OR REPLACE VIEW sv_personnel_history AS 
         SELECT _sp_hist._ph_id, _sp_hist._person_id, _sp_hist.start_date, _sp_hist.end_date, _sp_hist."position", _sp_hist.supervisor_person_id, _sp_hist.funding
           FROM _sp_hist
UNION 
         SELECT _pg_hist._pg_id AS _ph_id, _pg_hist._person_id, _pg_hist.start_date, _pg_hist.end_date, _pg_hist."position", _pg_hist.supervisor_person_id, _pg_hist.funding
           FROM _pg_hist;

ALTER TABLE sv_personnel_history
  OWNER TO sjt71;
