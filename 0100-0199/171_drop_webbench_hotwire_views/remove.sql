CREATE OR REPLACE VIEW hotwire."10_View/WebBench/Desk" AS 
 SELECT desk.id, desk.description, desk.room_id
   FROM desk;

ALTER TABLE hotwire."10_View/WebBench/Desk"
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Desk" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Desk" TO webbench;
GRANT SELECT ON TABLE hotwire."10_View/WebBench/Desk" TO webbench_ro;

-- Rule: "hotwire_10_View/WebBench/Desk_del" ON hotwire."10_View/WebBench/Desk"

-- DROP RULE "hotwire_10_View/WebBench/Desk_del" ON hotwire."10_View/WebBench/Desk";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_del" AS
    ON DELETE TO hotwire."10_View/WebBench/Desk" DO INSTEAD  DELETE FROM desk
  WHERE desk.id = old.id;

-- Rule: "hotwire_10_View/WebBench/Desk_ins" ON hotwire."10_View/WebBench/Desk"

-- DROP RULE "hotwire_10_View/WebBench/Desk_ins" ON hotwire."10_View/WebBench/Desk";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_ins" AS
    ON INSERT TO hotwire."10_View/WebBench/Desk" DO INSTEAD  INSERT INTO desk (description, room_id) 
  VALUES (new.description, new.room_id)
  RETURNING desk.id, desk.description, desk.room_id;

-- Rule: "hotwire_10_View/WebBench/Desk_upd" ON hotwire."10_View/WebBench/Desk"

-- DROP RULE "hotwire_10_View/WebBench/Desk_upd" ON hotwire."10_View/WebBench/Desk";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_upd" AS
    ON UPDATE TO hotwire."10_View/WebBench/Desk" DO INSTEAD  UPDATE desk SET id = new.id, description = new.description, room_id = new.room_id
  WHERE desk.id = old.id;

CREATE OR REPLACE VIEW hotwire."10_View/WebBench/Desk_Assignment" AS 
 SELECT desk_assignment.id, desk_assignment.start_date, desk_assignment.end_date, desk_assignment.notes, desk_assignment.person_id, desk_assignment.desk_id, desk_assignment.future_person_id
   FROM desk_assignment;

ALTER TABLE hotwire."10_View/WebBench/Desk_Assignment"
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Desk_Assignment" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Desk_Assignment" TO webbench;
GRANT SELECT ON TABLE hotwire."10_View/WebBench/Desk_Assignment" TO webbench_ro;

-- Rule: "hotwire_10_View/WebBench/Desk_Assignment_del" ON hotwire."10_View/WebBench/Desk_Assignment"

-- DROP RULE "hotwire_10_View/WebBench/Desk_Assignment_del" ON hotwire."10_View/WebBench/Desk_Assignment";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_Assignment_del" AS
    ON DELETE TO hotwire."10_View/WebBench/Desk_Assignment" DO INSTEAD  DELETE FROM desk_assignment
  WHERE desk_assignment.id = old.id;

-- Rule: "hotwire_10_View/WebBench/Desk_Assignment_ins" ON hotwire."10_View/WebBench/Desk_Assignment"

-- DROP RULE "hotwire_10_View/WebBench/Desk_Assignment_ins" ON hotwire."10_View/WebBench/Desk_Assignment";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_Assignment_ins" AS
    ON INSERT TO hotwire."10_View/WebBench/Desk_Assignment" DO INSTEAD  INSERT INTO desk_assignment (start_date, end_date, notes, person_id, desk_id, future_person_id) 
  VALUES (new.start_date, new.end_date, new.notes, new.person_id, new.desk_id, new.future_person_id)
  RETURNING desk_assignment.id, desk_assignment.start_date, desk_assignment.end_date, desk_assignment.notes, desk_assignment.person_id, desk_assignment.desk_id, desk_assignment.future_person_id;

-- Rule: "hotwire_10_View/WebBench/Desk_Assignment_upd" ON hotwire."10_View/WebBench/Desk_Assignment"

-- DROP RULE "hotwire_10_View/WebBench/Desk_Assignment_upd" ON hotwire."10_View/WebBench/Desk_Assignment";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Desk_Assignment_upd" AS
    ON UPDATE TO hotwire."10_View/WebBench/Desk_Assignment" DO INSTEAD  UPDATE desk_assignment SET id = new.id, start_date = new.start_date, end_date = new.end_date, person_id = new.person_id, future_person_id = new.future_person_id, notes = new.notes, desk_id = new.desk_id
  WHERE desk_assignment.id = old.id;

CREATE OR REPLACE VIEW hotwire."10_View/WebBench/Future_People" AS 
 SELECT future_person.id, future_person.name, future_person.supervisor_id, future_person.start_date, future_person.end_date, future_person.notes
   FROM future_person;

ALTER TABLE hotwire."10_View/WebBench/Future_People"
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Future_People" TO rl201;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Future_People" TO webbench;
GRANT SELECT ON TABLE hotwire."10_View/WebBench/Future_People" TO webbench_ro;

-- Rule: "hotwire_10_View/WebBench/Future_People_del" ON hotwire."10_View/WebBench/Future_People"

-- DROP RULE "hotwire_10_View/WebBench/Future_People_del" ON hotwire."10_View/WebBench/Future_People";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Future_People_del" AS
    ON DELETE TO hotwire."10_View/WebBench/Future_People" DO INSTEAD  DELETE FROM future_person
  WHERE future_person.id = old.id;

-- Rule: "hotwire_10_View/WebBench/Future_People_ins" ON hotwire."10_View/WebBench/Future_People"

-- DROP RULE "hotwire_10_View/WebBench/Future_People_ins" ON hotwire."10_View/WebBench/Future_People";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Future_People_ins" AS
    ON INSERT TO hotwire."10_View/WebBench/Future_People" DO INSTEAD  INSERT INTO future_person (name, supervisor_id, start_date, end_date, notes) 
  VALUES (new.name, new.supervisor_id, new.start_date, new.end_date, new.notes)
  RETURNING future_person.id, future_person.name, future_person.supervisor_id, future_person.start_date, future_person.end_date, future_person.notes;

-- Rule: "hotwire_10_View/WebBench/Future_People_upd" ON hotwire."10_View/WebBench/Future_People"

-- DROP RULE "hotwire_10_View/WebBench/Future_People_upd" ON hotwire."10_View/WebBench/Future_People";

CREATE OR REPLACE RULE "hotwire_10_View/WebBench/Future_People_upd" AS
    ON UPDATE TO hotwire."10_View/WebBench/Future_People" DO INSTEAD  UPDATE future_person SET id = new.id, name = new.name, supervisor_id = new.supervisor_id, start_date = new.start_date, end_date = new.end_date, notes = new.notes
  WHERE future_person.id = old.id;

CREATE OR REPLACE VIEW hotwire."10_View/WebBench/Summary" AS 
 SELECT d.room_id AS id, '</td><td class=''wbheader''></div></a><span class=''wbheader''>Room/Telephone Number</span><td class=''wbheader''>Present Occupant</td><td class=''wbheader''>Leaving Date</td><td class=''wbheader''>Future Arrivals/Comments</td><td class=''wbheader''>Arrival date</td><a href=''#''><div>' AS header, ('</td></tr><tr>'::text || array_to_string(ARRAY( SELECT desk_info.info
           FROM desk_info
          WHERE desk_info.id = d.room_id), '</tr><tr>'::text)) || '</tr><tr><td colspan=5 class=''hideme''>'::text AS detail
   FROM desk d
   JOIN room_hid USING (room_id);

ALTER TABLE hotwire."10_View/WebBench/Summary"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Summary" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/WebBench/Summary" TO webbench;
GRANT SELECT ON TABLE hotwire."10_View/WebBench/Summary" TO webbench_ro;

