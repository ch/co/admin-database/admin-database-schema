--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mentor; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE mentor (
    mentor_id bigint NOT NULL,
    person_id bigint NOT NULL,
    profile_url character varying
);


ALTER TABLE public.mentor OWNER TO dev;

--
-- Name: TABLE mentor; Type: COMMENT; Schema: public; Owner: dev
--

COMMENT ON TABLE mentor IS 'Mentoring scheme see ticket 85061. Will likely be extended.';


--
-- Name: mentor_mentor_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE mentor_mentor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.mentor_mentor_id_seq OWNER TO dev;

--
-- Name: mentor_mentor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE mentor_mentor_id_seq OWNED BY mentor.mentor_id;


--
-- Name: mentor_id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY mentor ALTER COLUMN mentor_id SET DEFAULT nextval('mentor_mentor_id_seq'::regclass);


--
-- Name: mentor_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY mentor
    ADD CONSTRAINT mentor_pkey PRIMARY KEY (mentor_id);


--
-- Name: mentor_fk_person; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY mentor
    ADD CONSTRAINT mentor_fk_person FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--



CREATE OR REPLACE VIEW www.mentors AS 
 SELECT person.id, title_hid.title_hid, person.surname, person.crsid, COALESCE(person.known_as, person.first_names) AS first_names, person.image_oid, (((COALESCE(title_hid.title_hid, ''::character varying)::text || ' '::text) || COALESCE(person.known_as, person.first_names)::text) || ' '::text) || person.surname::text AS friendly_name, mentor.profile_url
   FROM mentor
   JOIN person ON mentor.person_id = person.id
   LEFT JOIN title_hid ON person.title_id = title_hid.title_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
  WHERE s.status_id::text = 'Current'::text;

ALTER TABLE www.mentors
  OWNER TO dev;
GRANT ALL ON TABLE www.mentors TO dev;
GRANT ALL ON TABLE www.mentors TO cen1001;
GRANT SELECT ON TABLE www.mentors TO www_sites;


