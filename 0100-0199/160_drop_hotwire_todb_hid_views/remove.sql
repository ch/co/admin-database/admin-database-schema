CREATE OR REPLACE VIEW hotwire.todb_course_hid AS 
 SELECT todb_courses.id AS todb_course_id, todb_courses.course AS todb_course_hid
   FROM todb_courses;

ALTER TABLE hotwire.todb_course_hid
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire.todb_course_hid TO rl201;
GRANT SELECT ON TABLE hotwire.todb_course_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.todb_formula_hid AS 
 SELECT todb_point_formulae.id AS todb_formula_id, todb_point_formulae.name AS todb_formula_hid
   FROM todb_point_formulae;

ALTER TABLE hotwire.todb_formula_hid
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire.todb_formula_hid TO rl201;
GRANT SELECT ON TABLE hotwire.todb_formula_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.todb_jobtype_hid AS 
 SELECT todb_jobtype.id AS todb_jobtype_id, todb_jobtype.jobtype AS todb_jobtype_hid
   FROM todb_jobtype;

ALTER TABLE hotwire.todb_jobtype_hid
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire.todb_jobtype_hid TO rl201;
GRANT SELECT ON TABLE hotwire.todb_jobtype_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.todb_paper_hid AS 
 SELECT todb_papers.id AS todb_paper_id, todb_papers.paper AS todb_paper_hid
   FROM todb_papers;

ALTER TABLE hotwire.todb_paper_hid
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire.todb_paper_hid TO rl201;
GRANT SELECT ON TABLE hotwire.todb_paper_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.todb_term_hid AS 
 SELECT todb_terms.id AS todb_term_id, todb_terms.term AS todb_term_hid
   FROM todb_terms;

ALTER TABLE hotwire.todb_term_hid
  OWNER TO rl201;
GRANT ALL ON TABLE hotwire.todb_term_hid TO rl201;
GRANT SELECT ON TABLE hotwire.todb_term_hid TO ro_hid;

