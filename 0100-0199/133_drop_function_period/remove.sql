CREATE OR REPLACE FUNCTION period(v_person_id bigint)
  RETURNS text AS
$BODY$

declare v_period text;

begin

select
 case
  when end_date < now() then 'Past'
  when start_date < now() and end_date > now() then 'Current'
  when start_date > now() then 'Future'
  else 'Unknown'
 end
into v_period from physical_stay
where person_id = v_person_id
order by start_date desc
limit 1;

return v_period;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION period(bigint)
  OWNER TO sjt71;

