CREATE OR REPLACE FUNCTION no_past_desk_changes()
  RETURNS trigger AS
$BODY$
begin
if ((new.start_date is null)
  or (new.start_date != new.start_date)
 and (old.start_date < now())) then
  raise exception 'May not change start date once it has passed';
 end if;
 if ((new.future_person_id is not null)
  and old.future_person_id != new.future_person_id
  and old.start_date <now()) then
   raise exception 'May not change person once assignment has started';
 end if;
 if ((new.person_id is not null) 
  and old.person_id != new.person_id
  and old.start_date <now()) then
   raise exception 'May not change person once assignment has started';
 end if;
return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION no_past_desk_changes()
  OWNER TO dev;

CREATE TABLE desk
(
  id serial NOT NULL,
  description text,
  room_id bigint NOT NULL,
  CONSTRAINT desk_pkey PRIMARY KEY (id),
  CONSTRAINT desk_description_uniq UNIQUE (description, room_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE desk
  OWNER TO dev;

CREATE TABLE future_person
(
  id serial NOT NULL,
  name character varying,
  supervisor_id integer,
  start_date date,
  end_date date,
  notes text,
  CONSTRAINT future_person_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE future_person
  OWNER TO dev;


CREATE TABLE desk_assignment
(
  id bigserial NOT NULL,
  start_date date,
  end_date date,
  notes text,
  person_id integer,
  desk_id integer,
  future_person_id integer,
  CONSTRAINT desk_assignment_pkey PRIMARY KEY (id),
  CONSTRAINT desk_assignment_desk_fkey FOREIGN KEY (desk_id)
      REFERENCES desk (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT desk_assignment_future_person_fkey FOREIGN KEY (future_person_id)
      REFERENCES future_person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT desk_assignment_person_fkey FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT desk_assignment_not_future_and_real CHECK (future_person_id IS NULL OR person_id IS NULL) -- Do not permit desk assignments to be for both future people and real people
)
WITH (
  OIDS=FALSE
);
ALTER TABLE desk_assignment
  OWNER TO dev;
COMMENT ON CONSTRAINT desk_assignment_not_future_and_real ON desk_assignment IS 'Do not permit desk assignments to be for both future people and real people';


-- Index: fki_desk_assignment_desk_fkey

-- DROP INDEX fki_desk_assignment_desk_fkey;

CREATE INDEX fki_desk_assignment_desk_fkey
  ON desk_assignment
  USING btree
  (desk_id);

-- Index: fki_desk_assignment_future_person_fkey

-- DROP INDEX fki_desk_assignment_future_person_fkey;

CREATE INDEX fki_desk_assignment_future_person_fkey
  ON desk_assignment
  USING btree
  (future_person_id);

-- Index: fki_desk_assignment_person_fkey

-- DROP INDEX fki_desk_assignment_person_fkey;

CREATE INDEX fki_desk_assignment_person_fkey
  ON desk_assignment
  USING btree
  (person_id);


-- Trigger: may_not_change_start_date_if_passed on desk_assignment

-- DROP TRIGGER may_not_change_start_date_if_passed ON desk_assignment;

CREATE TRIGGER may_not_change_start_date_if_passed
  BEFORE UPDATE
  ON desk_assignment
  FOR EACH ROW
  EXECUTE PROCEDURE no_past_desk_changes();


