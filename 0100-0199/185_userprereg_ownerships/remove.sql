ALTER TABLE user_prereg_title_hid OWNER TO rl201;
GRANT ALL ON TABLE user_prereg_title_hid TO rl201;
REVOKE ALL ON TABLE user_prereg_title_hid FROM dev;
GRANT SELECT ON TABLE user_prereg_title_hid TO newusersignup;
GRANT SELECT ON TABLE user_prereg_title_hid TO cos;
GRANT SELECT ON TABLE user_prereg_title_hid TO ro_hid;

ALTER TABLE user_prereg_standing_hid OWNER TO rl201;
GRANT ALL ON TABLE user_prereg_standing_hid TO rl201;
REVOKE ALL ON TABLE user_prereg_standing_hid FROM dev;
GRANT SELECT ON TABLE user_prereg_standing_hid TO newusersignup;
GRANT ALL ON TABLE user_prereg_standing_hid TO cos;
GRANT SELECT ON TABLE user_prereg_standing_hid TO ro_hid;
