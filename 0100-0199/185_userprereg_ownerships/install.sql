ALTER TABLE user_prereg_title_hid OWNER TO dev;
GRANT ALL ON TABLE user_prereg_title_hid TO dev;
REVOKE ALL ON TABLE user_prereg_title_hid FROM rl201;
GRANT SELECT ON TABLE user_prereg_title_hid TO newusersignup;
GRANT SELECT ON TABLE user_prereg_title_hid TO cos;
GRANT SELECT ON TABLE user_prereg_title_hid TO ro_hid;

ALTER TABLE user_prereg_standing_hid OWNER TO dev;
GRANT ALL ON TABLE user_prereg_standing_hid TO dev;
REVOKE ALL ON TABLE user_prereg_standing_hid FROM rl201;
GRANT SELECT ON TABLE user_prereg_standing_hid TO newusersignup;
GRANT SELECT ON TABLE user_prereg_standing_hid TO cos;
GRANT SELECT ON TABLE user_prereg_standing_hid TO ro_hid;
