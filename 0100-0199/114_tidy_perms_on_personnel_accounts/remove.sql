ALTER TABLE hotwire."10_View/People/Personnel_Accounts" OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Accounts" TO cen1001;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO accounts;
