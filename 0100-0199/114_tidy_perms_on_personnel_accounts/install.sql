ALTER TABLE hotwire."10_View/People/Personnel_Accounts" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_Accounts" TO dev;
REVOKE SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" FROM mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/People/Personnel_Accounts" TO accounts;
