CREATE OR REPLACE VIEW webbench_future_assignments AS 
 SELECT desk_assignment.desk_id, (((((((('<a href=''edit.php?_view=10_View/WebBench/Desk_Assignment&_id[]='::text || desk_assignment.id) || '''>Assigned '::text) || COALESCE(desk_assignment.start_date::character varying, '?'::character varying)::text) || ' - '::text) || COALESCE(desk_assignment.end_date::character varying, '?'::character varying)::text) || ' '::text) || COALESCE(person_hid.person_hid, future_person_hid.future_person_hid, 'Unknown person'::text)) || COALESCE(('('::text || desk_assignment.notes) || ')'::text, ''::text)) || '</a>'::text AS info
   FROM desk_assignment
   LEFT JOIN hotwire.person_hid USING (person_id)
   LEFT JOIN hotwire.future_person_hid USING (future_person_id)
  WHERE desk_assignment.start_date > now();

ALTER TABLE webbench_future_assignments
  OWNER TO rl201;

CREATE OR REPLACE VIEW desk_info AS 
 SELECT room.id, (((((((((((((((('<td class=''wb_room''>Room '::text || room.name::text) || ' - '::text) || COALESCE(desk.description, 'desk'::text)) || COALESCE(' / '::text || ((( SELECT t.extension_number
           FROM dept_telephone_number t
          WHERE t.room_id = room.id AND NOT t.fax AND NOT t.personal_line))::text), ''::text)) || '</td>'::text) || '<td class=''wb_occupier''>'::text) || COALESCE(((('<a href=''edit.php?_view=10_View/WebBench/Desk_Assignment&_id[]='::text || da.id) || '''>'::text) || person_hid.person_hid) || '</a>'::text, ((('<a href=''edit.php?_view=10_View/WebBench/Desk_Assignment&_id[]='::text || da.id) || '''>'::text) || future_person_hid.future_person_hid) || '</a>'::text, ' '::text)) || '</td>'::text) || '<td class=''wb_leaving''>'::text) || COALESCE(da.end_date::character varying, ' '::character varying)::text) || '</td>'::text) || '<td class=''wb_future''>'::text) || COALESCE(array_to_string(ARRAY( SELECT webbench_future_assignments.info
           FROM webbench_future_assignments
          WHERE webbench_future_assignments.desk_id = desk.id), ', '::text), 'No future plans'::text)) || '</td>'::text) || '<td class=''wb_arrival''>'::text) || COALESCE(da.end_date::character varying, ' '::character varying)::text) || '</td>'::text AS info
   FROM desk
   JOIN hotwire.desk_hid ON desk.id = desk_hid.desk_id
   JOIN room ON desk.room_id = room.id
   LEFT JOIN ( SELECT desk_assignment.id, desk_assignment.start_date, desk_assignment.end_date, desk_assignment.notes, desk_assignment.person_id, desk_assignment.desk_id, desk_assignment.future_person_id
   FROM desk_assignment
  WHERE desk_assignment.start_date < now() AND desk_assignment.end_date > now()) da USING (desk_id)
   LEFT JOIN hotwire.person_hid USING (person_id)
   LEFT JOIN hotwire.future_person_hid USING (future_person_id)
  ORDER BY desk.description;

ALTER TABLE desk_info
  OWNER TO rl201;

