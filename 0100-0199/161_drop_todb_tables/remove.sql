CREATE TABLE todb_courses
(
  id serial NOT NULL,
  course character varying(32),
  CONSTRAINT todb_courses_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_courses OWNER TO rl201;

CREATE TABLE todb_jobtype
(
  id serial NOT NULL,
  jobtype character varying(32),
  CONSTRAINT todb_jobtype_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_jobtype OWNER TO rl201;

CREATE TABLE todb_papers
(
  id serial NOT NULL,
  paper character varying(32),
  CONSTRAINT todb_papers_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_papers OWNER TO rl201;

CREATE TABLE todb_point_formulae
(
  id serial NOT NULL,
  name character varying(255),
  description character varying,
  multiplier numeric DEFAULT 0,
  "offset" numeric DEFAULT 0,
  year date,
  CONSTRAINT todb_point_formulae_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_point_formulae OWNER TO rl201;

CREATE TABLE todb_terms
(
  id serial NOT NULL,
  term character varying(32),
  CONSTRAINT todb_terms_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_terms OWNER TO rl201;

CREATE TABLE todb_units
(
  id serial NOT NULL,
  name character varying(32),
  todb_course_id bigint,
  sort_order numeric,
  description text,
  note text,
  CONSTRAINT todb_units_pkey PRIMARY KEY (id),
  CONSTRAINT fkey_todb_course_id FOREIGN KEY (todb_course_id)
      REFERENCES todb_courses (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT todb_units_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_units OWNER TO rl201;

CREATE TABLE todb_units_years
(
  id serial NOT NULL,
  unit_id integer,
  year date,
  CONSTRAINT todb_units_years_pkey PRIMARY KEY (id),
  CONSTRAINT fkey_todb_units_years FOREIGN KEY (unit_id)
      REFERENCES todb_units (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_units_years OWNER TO rl201;

CREATE INDEX fki_fkey_todb_units_years
  ON todb_units_years
  USING btree
  (unit_id);

CREATE TRIGGER todb_units_years_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON todb_units_years
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();


CREATE TABLE todb_jobs
(
  id serial NOT NULL,
  todb_course_id integer,
  todb_course_year integer,
  todb_paper_id integer,
  name text,
  todb_jobtype_id integer,
  hours numeric,
  todb_term_id integer,
  person_id integer,
  points integer,
  note text,
  deleted boolean,
  todb_formula_id integer,
  academic_year date,
  CONSTRAINT todb_jobs_pkey PRIMARY KEY (id),
  CONSTRAINT fkey_todb_jobs_course_id FOREIGN KEY (todb_course_id)
      REFERENCES todb_courses (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkey_todb_jobs_paper_id FOREIGN KEY (todb_paper_id)
      REFERENCES todb_papers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todb_jobs OWNER TO rl201;

CREATE INDEX fki_fkey_todb_jobs_course_id
  ON todb_jobs
  USING btree
  (todb_course_id);

CREATE INDEX fki_fkey_todb_jobs_paper_id
  ON todb_jobs
  USING btree
  (todb_paper_id);

CREATE TRIGGER todb_jobs_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON todb_jobs
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();

