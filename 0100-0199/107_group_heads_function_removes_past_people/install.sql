CREATE OR REPLACE FUNCTION fn_update_group_heads()
  RETURNS void AS
$BODY$
 declare
   headofgp varchar;

 begin
  -- people without DB roles: create them
    for headofgp in 
        select distinct crsid 
        from person 
        inner join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        left join pg_roles on person.crsid=rolname 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        where crsid is not null and rolname is null and _physical_status_v2.status_id <> 'Past'
    loop
        execute 'create role '||headofgp||' with login in role headsofgroup';
   end loop;

  -- people who don't have head role
    for headofgp in 
        select distinct crsid 
        from person 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        inner join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        where (
                select count(*) 
                from pg_auth_members 
                inner join pg_roles u on member=u.oid 
                inner join pg_roles g on roleid=g.oid 
                where u.rolname=crsid and g.rolname='groupheadofgps'
            )=0 
        and 
            crsid is not null
        and 
            _physical_status_v2.status_id <> 'Past'
    loop
        execute 'grant headsofgroup to '||headofgp;
    end loop;

  -- people with the head of group role who shouldn't have
    for headofgp in 
        select u.rolname 
        from pg_auth_members 
        inner join pg_authid u on u.oid=pg_auth_members.member 
        inner join pg_authid g on pg_auth_members.roleid=g.oid 
        left join person on person.crsid=u.rolname 
        left join _physical_status_v2 on person.id = _physical_status_v2.person_id
        left join research_group on research_group.head_of_group_id=person.id or research_group.deputy_head_of_group_id=person.id 
        where g.rolname='headsofgroup' and ( research_group.id is null or _physical_status_v2.status_id = 'Past' )
    loop
        execute 'revoke headsofgroup from '||headofgp;
    end loop;


 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_update_group_heads()
  OWNER TO dev;
