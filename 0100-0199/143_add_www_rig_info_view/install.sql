create or replace view www.rig_info_v1 as
select research_interest_group.id, name, crsid as chair, website from research_interest_group
join www.person_info_v8 on www.person_info_v8.id=research_interest_group.chair_id;

alter view www.rig_info_v1 owner to dev;
grant select on www.rig_info_v1 to www_sites;
