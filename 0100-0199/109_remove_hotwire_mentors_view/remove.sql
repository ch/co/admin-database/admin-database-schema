CREATE OR REPLACE VIEW hotwire."10_View/Mentors" AS 
 SELECT mentor.mentor_id AS id, mentor.person_id, mentor.profile_url
   FROM mentor;

ALTER TABLE hotwire."10_View/Mentors"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Mentors" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Mentors" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Mentors" TO hr;

CREATE OR REPLACE RULE "hotwire_10_View/Mentors_del" AS
    ON DELETE TO hotwire."10_View/Mentors" DO INSTEAD  DELETE FROM mentor
      WHERE mentor.mentor_id = old.id;

CREATE OR REPLACE RULE "hotwire_10_View/Mentors_ins" AS
    ON INSERT TO hotwire."10_View/Mentors" DO INSTEAD  INSERT INTO mentor (person_id, profile_url) 
      VALUES (new.person_id, new.profile_url)
      RETURNING mentor.mentor_id, mentor.person_id, mentor.profile_url;

CREATE OR REPLACE RULE "hotwire_10_View/Mentors_upd" AS
    ON UPDATE TO hotwire."10_View/Mentors" DO INSTEAD  UPDATE mentor SET mentor_id = new.id, person_id = new.person_id, profile_url = new.profile_url
      WHERE mentor.mentor_id = old.id;
