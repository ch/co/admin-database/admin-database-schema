DROP VIEW public.skills_authoriser_view;
DROP VIEW public.skills_compulsory_view;
DROP VIEW public.skills_contacts_view;
DROP VIEW public.skills_course_sessions_view;
DROP VIEW public.skills_location_view;
DROP VIEW public.skills_presenters_view;
DROP VIEW public.skills_providers_view;
DROP VIEW public.skills_course_v2;
DROP VIEW public.skills_courses_v3;
DROP VIEW public.skills_courses_v4;
DROP VIEW public.skills_courses_by_session_v1;

DROP VIEW public.provider_hid;
DROP VIEW public.location_hid;
DROP VIEW public.presenter_hid;
DROP VIEW public.authoriser_hid;
DROP VIEW public.compulsion_hid;
DROP VIEW public.contact_hid;
