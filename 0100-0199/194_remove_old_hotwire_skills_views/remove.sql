CREATE OR REPLACE VIEW skills_authoriser_view AS 
 SELECT skills_authoriser.id, skills_authoriser.authoriser
   FROM skills_authoriser;

ALTER TABLE skills_authoriser_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_authoriser_view TO postgres;
GRANT ALL ON TABLE skills_authoriser_view TO dev;
GRANT ALL ON TABLE skills_authoriser_view TO skills;

CREATE OR REPLACE VIEW skills_compulsory_view AS 
 SELECT skills_compulsory.id, skills_compulsory.compulsion
   FROM skills_compulsory;

ALTER TABLE skills_compulsory_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_compulsory_view TO postgres;
GRANT ALL ON TABLE skills_compulsory_view TO dev;
GRANT ALL ON TABLE skills_compulsory_view TO skills;


CREATE OR REPLACE VIEW skills_contacts_view AS 
 SELECT skills_contacts.id, skills_contacts.contact
   FROM skills_contacts;

ALTER TABLE skills_contacts_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_contacts_view TO postgres;
GRANT ALL ON TABLE skills_contacts_view TO dev;
GRANT ALL ON TABLE skills_contacts_view TO skills;

CREATE OR REPLACE VIEW skills_course_sessions_view AS 
 SELECT skills_sessions.id, skills_sessions.start_date, skills_sessions.start_text, skills_sessions.end_date, skills_sessions.end_text, skills_sessions.narrative_time, skills_sessions.venue, skills_sessions.provider
   FROM skills_sessions;

ALTER TABLE skills_course_sessions_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_course_sessions_view TO postgres;
GRANT ALL ON TABLE skills_course_sessions_view TO dev;
GRANT ALL ON TABLE skills_course_sessions_view TO old_cos;
GRANT ALL ON TABLE skills_course_sessions_view TO skills;

CREATE OR REPLACE VIEW skills_location_view AS 
 SELECT skills_location.id, skills_location.location
   FROM skills_location;

ALTER TABLE skills_location_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_location_view TO postgres;
GRANT ALL ON TABLE skills_location_view TO dev;
GRANT ALL ON TABLE skills_location_view TO skills;

CREATE OR REPLACE VIEW skills_presenters_view AS 
 SELECT skills_presenters.id, skills_presenters.presenter
   FROM skills_presenters;

ALTER TABLE skills_presenters_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_presenters_view TO postgres;
GRANT ALL ON TABLE skills_presenters_view TO dev;
GRANT ALL ON TABLE skills_presenters_view TO skills;

CREATE OR REPLACE VIEW skills_providers_view AS 
 SELECT skills_providers.id, skills_providers.provider
   FROM skills_providers;

ALTER TABLE skills_providers_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_providers_view TO postgres;
GRANT ALL ON TABLE skills_providers_view TO dev;
GRANT ALL ON TABLE skills_providers_view TO skills;

CREATE OR REPLACE VIEW skills_course_v2 AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.information_url AS information, skills_courses.expired, skills_authoriser.authoriser, skills_compulsory.compulsion, skills_contacts.contact, skills_location.location, skills_courses.presenter_id, skills_courses.provider_id, ARRAY( SELECT mm_skills_courses_rcskills.skills_rcskills_id
           FROM mm_skills_courses_skills_rcskills mm_skills_courses_rcskills
      JOIN skills_rcskills_hid USING (skills_rcskills_id)
     WHERE mm_skills_courses_rcskills.skills_courses_id = skills_courses.id) AS "skills_rcskills[]", ARRAY( SELECT mm_skills_courses_bskills.skills_bskills_id
           FROM mm_skills_courses_skills_bskills mm_skills_courses_bskills
      JOIN skills_bskills_hid USING (skills_bskills_id)
     WHERE mm_skills_courses_bskills.skills_courses_id = skills_courses.id) AS "skills_bskills[]", ARRAY( SELECT (mm_skills_courses_year.skills_courses_years_id || ':'::text) || skills_courses_years_hid.skills_courses_years_hid::text
           FROM mm_skills_courses_skills_courses_years mm_skills_courses_year
      JOIN skills_courses_years_hid USING (skills_courses_years_id)
     WHERE mm_skills_courses_year.skills_courses_id = skills_courses.id) AS "skills_years[]"
   FROM skills_courses
   LEFT JOIN skills_authoriser ON skills_courses.authoriser_id = skills_authoriser.id
   LEFT JOIN skills_compulsory ON skills_courses.compulsion_id = skills_compulsory.id
   LEFT JOIN skills_location ON skills_courses.location_id = skills_location.id
   LEFT JOIN skills_contacts ON skills_courses.contact_id = skills_contacts.id;

ALTER TABLE skills_course_v2
  OWNER TO postgres;
GRANT ALL ON TABLE skills_course_v2 TO postgres;
GRANT ALL ON TABLE skills_course_v2 TO old_cos;
GRANT ALL ON TABLE skills_course_v2 TO www_sites;
GRANT ALL ON TABLE skills_course_v2 TO skills;
GRANT SELECT ON TABLE skills_course_v2 TO cen1001;


CREATE OR REPLACE VIEW skills_courses_v3 AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.information_url AS information, skills_courses.expired, skills_authoriser.authoriser, skills_compulsory.compulsion, skills_contacts.contact, skills_location.location, skills_courses.presenter_id, skills_courses.provider_id, skills_courses.skills_credit, ARRAY( SELECT mm_skills_courses_rcskills.skills_rcskills_id
           FROM mm_skills_courses_skills_rcskills mm_skills_courses_rcskills
      JOIN skills_rcskills_hid USING (skills_rcskills_id)
     WHERE mm_skills_courses_rcskills.skills_courses_id = skills_courses.id) AS "skills_rcskills[]", ARRAY( SELECT mm_skills_courses_bskills.skills_bskills_id
           FROM mm_skills_courses_skills_bskills mm_skills_courses_bskills
      JOIN skills_bskills_hid USING (skills_bskills_id)
     WHERE mm_skills_courses_bskills.skills_courses_id = skills_courses.id) AS "skills_bskills[]", ARRAY( SELECT (mm_skills_courses_year.skills_courses_years_id || ':'::text) || skills_courses_years_hid.skills_courses_years_hid::text
           FROM mm_skills_courses_skills_courses_years mm_skills_courses_year
      JOIN skills_courses_years_hid USING (skills_courses_years_id)
     WHERE mm_skills_courses_year.skills_courses_id = skills_courses.id) AS "skills_years[]"
   FROM skills_courses
   LEFT JOIN skills_authoriser ON skills_courses.authoriser_id = skills_authoriser.id
   LEFT JOIN skills_compulsory ON skills_courses.compulsion_id = skills_compulsory.id
   LEFT JOIN skills_location ON skills_courses.location_id = skills_location.id
   LEFT JOIN skills_contacts ON skills_courses.contact_id = skills_contacts.id;

ALTER TABLE skills_courses_v3
  OWNER TO postgres;
GRANT ALL ON TABLE skills_courses_v3 TO postgres;
GRANT SELECT ON TABLE skills_courses_v3 TO www_sites;

CREATE OR REPLACE VIEW skills_courses_v4 AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.information_url, skills_courses.information, skills_courses.expired, skills_authoriser.authoriser, skills_compulsory.compulsion, skills_contacts.contact, skills_location.location, skills_courses.presenter_id, skills_courses.provider_id, skills_courses.skills_credit, ARRAY( SELECT mm_skills_courses_rcskills.skills_rcskills_id
           FROM mm_skills_courses_skills_rcskills mm_skills_courses_rcskills
      JOIN skills_rcskills_hid USING (skills_rcskills_id)
     WHERE mm_skills_courses_rcskills.skills_courses_id = skills_courses.id) AS "skills_rcskills[]", ARRAY( SELECT mm_skills_courses_bskills.skills_bskills_id
           FROM mm_skills_courses_skills_bskills mm_skills_courses_bskills
      JOIN skills_bskills_hid USING (skills_bskills_id)
     WHERE mm_skills_courses_bskills.skills_courses_id = skills_courses.id) AS "skills_bskills[]", ARRAY( SELECT (mm_skills_courses_year.skills_courses_years_id || ':'::text) || skills_courses_years_hid.skills_courses_years_hid::text
           FROM mm_skills_courses_skills_courses_years mm_skills_courses_year
      JOIN skills_courses_years_hid USING (skills_courses_years_id)
     WHERE mm_skills_courses_year.skills_courses_id = skills_courses.id) AS "skills_years[]", ARRAY( SELECT mm_skills_courses_skills_course_sessions.skills_course_sessions_id
           FROM mm_skills_courses_skills_course_sessions
          WHERE mm_skills_courses_skills_course_sessions.skills_courses_id = skills_courses.id) AS "skills_courses_sessions[]"
   FROM skills_courses
   LEFT JOIN skills_authoriser ON skills_courses.authoriser_id = skills_authoriser.id
   LEFT JOIN skills_compulsory ON skills_courses.compulsion_id = skills_compulsory.id
   LEFT JOIN skills_location ON skills_courses.location_id = skills_location.id
   LEFT JOIN skills_contacts ON skills_courses.contact_id = skills_contacts.id;

ALTER TABLE skills_courses_v4
  OWNER TO postgres;
GRANT ALL ON TABLE skills_courses_v4 TO postgres;
GRANT SELECT ON TABLE skills_courses_v4 TO www_sites;

CREATE OR REPLACE VIEW skills_courses_by_session_v1 AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.information_url, skills_courses.information, skills_courses.expired, skills_authoriser.authoriser, skills_compulsory.compulsion, skills_contacts.contact, skills_location.location, skills_courses.presenter_id, skills_courses.provider_id, skills_courses.skills_credit, ARRAY( SELECT mm_skills_courses_rcskills.skills_rcskills_id
           FROM mm_skills_courses_skills_rcskills mm_skills_courses_rcskills
      JOIN skills_rcskills_hid USING (skills_rcskills_id)
     WHERE mm_skills_courses_rcskills.skills_courses_id = skills_courses.id) AS "skills_rcskills[]", ARRAY( SELECT mm_skills_courses_bskills.skills_bskills_id
           FROM mm_skills_courses_skills_bskills mm_skills_courses_bskills
      JOIN skills_bskills_hid USING (skills_bskills_id)
     WHERE mm_skills_courses_bskills.skills_courses_id = skills_courses.id) AS "skills_bskills[]", ARRAY( SELECT (mm_skills_courses_year.skills_courses_years_id || ':'::text) || skills_courses_years_hid.skills_courses_years_hid::text
           FROM mm_skills_courses_skills_courses_years mm_skills_courses_year
      JOIN skills_courses_years_hid USING (skills_courses_years_id)
     WHERE mm_skills_courses_year.skills_courses_id = skills_courses.id) AS "skills_years[]", skills_sessions.id AS skills_sessions_id, skills_sessions.start_date
   FROM skills_courses
   LEFT JOIN skills_authoriser ON skills_courses.authoriser_id = skills_authoriser.id
   LEFT JOIN skills_compulsory ON skills_courses.compulsion_id = skills_compulsory.id
   LEFT JOIN skills_location ON skills_courses.location_id = skills_location.id
   LEFT JOIN skills_contacts ON skills_courses.contact_id = skills_contacts.id
   LEFT JOIN mm_skills_courses_skills_course_sessions ON mm_skills_courses_skills_course_sessions.skills_courses_id = skills_courses.id
   LEFT JOIN skills_sessions ON mm_skills_courses_skills_course_sessions.skills_course_sessions_id = skills_sessions.id;

ALTER TABLE skills_courses_by_session_v1
  OWNER TO cen1001;
GRANT ALL ON TABLE skills_courses_by_session_v1 TO cen1001;
GRANT SELECT ON TABLE skills_courses_by_session_v1 TO www_sites;

CREATE OR REPLACE VIEW provider_hid AS 
 SELECT skills_providers.id AS provider_id, skills_providers.provider AS provider_hid
   FROM skills_providers;

ALTER TABLE provider_hid
  OWNER TO postgres;
GRANT ALL ON TABLE provider_hid TO postgres;
GRANT SELECT ON TABLE provider_hid TO old_cos;
GRANT SELECT ON TABLE provider_hid TO ro_hid;

CREATE OR REPLACE VIEW location_hid AS 
 SELECT skills_location.id AS location_id, skills_location.location AS location_hid
   FROM skills_location;

ALTER TABLE location_hid
  OWNER TO postgres;
GRANT ALL ON TABLE location_hid TO postgres;
GRANT SELECT ON TABLE location_hid TO old_cos;
GRANT SELECT ON TABLE location_hid TO ro_hid;


CREATE OR REPLACE VIEW presenter_hid AS 
 SELECT skills_presenters.id AS presenter_id, skills_presenters.presenter AS presenter_hid
   FROM skills_presenters;

ALTER TABLE presenter_hid
  OWNER TO postgres;
GRANT ALL ON TABLE presenter_hid TO postgres;
GRANT SELECT ON TABLE presenter_hid TO old_cos;
GRANT SELECT ON TABLE presenter_hid TO ro_hid;



CREATE OR REPLACE VIEW authoriser_hid AS 
 SELECT skills_authoriser.id AS authoriser_id, skills_authoriser.authoriser AS authoriser_hid
   FROM skills_authoriser;

ALTER TABLE authoriser_hid
  OWNER TO postgres;
GRANT ALL ON TABLE authoriser_hid TO postgres;
GRANT SELECT ON TABLE authoriser_hid TO old_cos;
GRANT SELECT ON TABLE authoriser_hid TO ro_hid;


CREATE OR REPLACE VIEW compulsion_hid AS 
 SELECT skills_compulsory.id AS compulsion_id, skills_compulsory.compulsion AS compulsion_hid
   FROM skills_compulsory;

ALTER TABLE compulsion_hid
  OWNER TO postgres;
GRANT ALL ON TABLE compulsion_hid TO postgres;
GRANT SELECT ON TABLE compulsion_hid TO old_cos;
GRANT SELECT ON TABLE compulsion_hid TO ro_hid;


CREATE OR REPLACE VIEW contact_hid AS 
 SELECT skills_contacts.id AS contact_id, skills_contacts.contact AS contact_hid
   FROM skills_contacts;

ALTER TABLE contact_hid
  OWNER TO postgres;
GRANT ALL ON TABLE contact_hid TO postgres;
GRANT SELECT ON TABLE contact_hid TO old_cos;
GRANT SELECT ON TABLE contact_hid TO ro_hid;

