CREATE OR REPLACE FUNCTION fn_get_many_many(
    v_home_id bigint,
    v_home text,
    v_foreign text)
  RETURNS text AS
$BODY$

declare
  v_table text;
  v_foreign_id bigint;
  v_query text;
  v_array bigint[];

begin

  v_array := '{}';
  v_table := 'mm_' || v_home || '_' || v_foreign;
  v_query := 'SELECT ' || v_foreign || '_id FROM ' || v_table || ' WHERE '
                       || v_home || '_id = ' || v_home_id;

  FOR v_foreign_id IN EXECUTE v_query LOOP
    v_array := array_append(v_array, v_foreign_id);
  END LOOP;

  return array_to_string(v_array, ',');

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_get_many_many(bigint, text, text)
  OWNER TO sjt71;
