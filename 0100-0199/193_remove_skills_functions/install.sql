DROP RULE hotwire_view_skills_course_ins ON hotwire."10_View/Skills_Course";
DROP RULE hotwire_view_skills_course_upd ON hotwire."10_View/Skills_Course";
DROP FUNCTION hw_fn_skills_course_ins(hotwire."10_View/Skills_Course");
DROP FUNCTION hw_fn_skills_course_upd(hotwire."10_View/Skills_Course");
DROP VIEW hotwire."10_View/Skills_Course";
