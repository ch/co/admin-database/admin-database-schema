CREATE OR REPLACE VIEW hotwire."10_View/Skills_Course" AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid AS ro_rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.authoriser_id, skills_courses.compulsion_id, skills_courses.contact_id, skills_courses.location_id, skills_courses.presenter_id, skills_courses.provider_id, skills_courses.skills_credit::character varying(10) AS skills_credit, ARRAY( SELECT mm_skills_courses_skills_rcskills.skills_rcskills_id
           FROM mm_skills_courses_skills_rcskills
          WHERE mm_skills_courses_skills_rcskills.skills_courses_id = skills_courses.id) AS skills_rcskills_id, ARRAY( SELECT mm_skills_courses_skills_bskills.skills_bskills_id
           FROM mm_skills_courses_skills_bskills
          WHERE mm_skills_courses_skills_bskills.skills_courses_id = skills_courses.id) AS skills_bskills_id, skills_courses.information_url::character varying(250) AS information_url, skills_courses.information, ARRAY( SELECT mm_skills_courses_skills_courses_years.skills_courses_years_id
           FROM mm_skills_courses_skills_courses_years
          WHERE mm_skills_courses_skills_courses_years.skills_courses_id = skills_courses.id) AS skills_courses_years_id, ARRAY( SELECT mm_skills_courses_skills_course_sessions.skills_course_sessions_id
           FROM mm_skills_courses_skills_course_sessions
          WHERE mm_skills_courses_skills_course_sessions.skills_course_sessions_id = skills_courses.id) AS skills_course_sessions_id
   FROM skills_courses;

ALTER TABLE hotwire."10_View/Skills_Course"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course" TO skills;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course" TO old_cos;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course" TO dev;


CREATE OR REPLACE FUNCTION hw_fn_skills_course_ins(hotwire."10_View/Skills_Course")
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         new_skills_course BIGINT;
 begin

 insert into skills_courses
         (description, lastevent,information,information_url,title,skills_credit,authoriser_id,compulsion_id, contact_id,location_id,presenter_id,provider_id)
         values
         (
 v.description, v.lastevent,v.information,v.information_url,v.title,v.skills_credit::numeric,
 v.authoriser_id,v.compulsion_id,
         v.contact_id,v.location_id,v.presenter_id,v.provider_id
         ) returning id into new_skills_course;

 perform fn_mm_array_update(v.skills_rcskills_id,
                            'mm_skills_courses_skills_rcskills'::varchar,
                            'skills_rcskills_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_bskills_id,
                            'mm_skills_courses_skills_bskills'::varchar,
                            'skills_bskills_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_courses_years_id,
                            'mm_skills_courses_skills_courses_years'::varchar,
                            'skills_courses_years_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_courses_years_id,
                            'mm_skills_courses_skills_course_sessions'::varchar,
                            'skills_course_sessions_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 return new_skills_course;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_skills_course_ins(hotwire."10_View/Skills_Course")
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION hw_fn_skills_course_upd(hotwire."10_View/Skills_Course")
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         new_skills_course BIGINT;
 begin


 update skills_courses set
  description=v.description,
  lastevent=v.lastevent,
  information=v.information,
  information_url=v.information_url,
  title=v.title,
  skills_credit=v.skills_credit::numeric,
  authoriser_id=v.authoriser_id,
  compulsion_id=v.compulsion_id,
  contact_id=v.contact_id,
  location_id=v.location_id,
  presenter_id=v.presenter_id,
  provider_id=v.provider_id
 WHERE id=v.id;

 perform fn_mm_array_update(v.skills_rcskills_id,
                            'mm_skills_courses_skills_rcskills'::varchar,
                            'skills_rcskills_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_bskills_id,
                            'mm_skills_courses_skills_bskills'::varchar,
                            'skills_bskills_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_courses_years_id,
                            'mm_skills_courses_skills_courses_years'::varchar,
                            'skills_courses_years_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);

 perform fn_mm_array_update(v.skills_courses_years_id,
                            'mm_skills_courses_skills_course_sessions'::varchar,
                            'skills_course_sessions_id'::varchar,
                            'skills_courses_id'::varchar,
                            new_skills_course);
 return v.id;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_skills_course_upd(hotwire."10_View/Skills_Course")
  OWNER TO postgres;

CREATE OR REPLACE RULE hotwire_view_skills_course_ins AS
    ON INSERT TO hotwire."10_View/Skills_Course" DO INSTEAD  SELECT hw_fn_skills_course_ins(new.*) AS id;
CREATE OR REPLACE RULE hotwire_view_skills_course_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Course" DO INSTEAD  SELECT hw_fn_skills_course_upd(new.*) AS id;

