ALTER TABLE it_task_audit OWNER TO dev;
GRANT ALL ON TABLE it_task_audit TO dev;
REVOKE SELECT, INSERT ON TABLE it_task_audit FROM old_cos;
GRANT INSERT,SELECT ON TABLE it_task_audit TO cos;
GRANT INSERT ON TABLE it_task_audit TO public;

ALTER TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" TO cos;
