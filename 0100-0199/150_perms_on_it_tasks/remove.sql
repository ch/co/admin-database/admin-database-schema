ALTER TABLE it_task_audit OWNER TO postgres;
GRANT ALL ON TABLE it_task_audit TO postgres;
GRANT SELECT, INSERT ON TABLE it_task_audit TO old_cos;
GRANT ALL ON TABLE it_task_audit TO dev;
REVOKE ALL ON TABLE it_task_audit FROM cos;
GRANT INSERT ON TABLE it_task_audit TO cos;
GRANT INSERT ON TABLE it_task_audit TO public;

ALTER TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" TO postgres;
REVOKE ALL ON TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" FROM cos;
REVOKE ALL ON TABLE hotwire."10_View/10_IT_Tasks/_audit_ro" FROM dev;
