CREATE OR REPLACE VIEW hotwire."10_View/Skills_Authoriser" AS 
 SELECT skills_authoriser.id, skills_authoriser.authoriser
   FROM skills_authoriser;

ALTER TABLE hotwire."10_View/Skills_Authoriser"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Authoriser" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Authoriser" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Authoriser" TO skills;

-- Rule: hotwire_view_skills_authoriser_ins ON hotwire."10_View/Skills_Authoriser"

-- DROP RULE hotwire_view_skills_authoriser_ins ON hotwire."10_View/Skills_Authoriser";

CREATE OR REPLACE RULE hotwire_view_skills_authoriser_ins AS
    ON INSERT TO hotwire."10_View/Skills_Authoriser" DO INSTEAD  INSERT INTO skills_authoriser (authoriser) 
  VALUES (new.authoriser)
  RETURNING skills_authoriser.id, skills_authoriser.authoriser;

-- Rule: hotwire_view_skills_authoriser_view_del ON hotwire."10_View/Skills_Authoriser"

-- DROP RULE hotwire_view_skills_authoriser_view_del ON hotwire."10_View/Skills_Authoriser";

CREATE OR REPLACE RULE hotwire_view_skills_authoriser_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Authoriser" DO INSTEAD  DELETE FROM skills_authoriser
  WHERE skills_authoriser.id = old.id;

-- Rule: hotwire_view_skills_authoriser_view_upd ON hotwire."10_View/Skills_Authoriser"

-- DROP RULE hotwire_view_skills_authoriser_view_upd ON hotwire."10_View/Skills_Authoriser";

CREATE OR REPLACE RULE hotwire_view_skills_authoriser_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Authoriser" DO INSTEAD  UPDATE skills_authoriser SET authoriser = new.authoriser
  WHERE skills_authoriser.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Skills_Compulsory" AS 
 SELECT skills_compulsory.id, skills_compulsory.compulsion
   FROM skills_compulsory;

ALTER TABLE hotwire."10_View/Skills_Compulsory"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Compulsory" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Compulsory" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Compulsory" TO skills;

-- Rule: hotwire_view_skills_compulsory_ins ON hotwire."10_View/Skills_Compulsory"

-- DROP RULE hotwire_view_skills_compulsory_ins ON hotwire."10_View/Skills_Compulsory";

CREATE OR REPLACE RULE hotwire_view_skills_compulsory_ins AS
    ON INSERT TO hotwire."10_View/Skills_Compulsory" DO INSTEAD  INSERT INTO skills_compulsory (compulsion) 
  VALUES (new.compulsion)
  RETURNING skills_compulsory.id, skills_compulsory.compulsion;

-- Rule: hotwire_view_skills_compulsory_view_del ON hotwire."10_View/Skills_Compulsory"

-- DROP RULE hotwire_view_skills_compulsory_view_del ON hotwire."10_View/Skills_Compulsory";

CREATE OR REPLACE RULE hotwire_view_skills_compulsory_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Compulsory" DO INSTEAD  DELETE FROM skills_compulsory
  WHERE skills_compulsory.id = old.id;

-- Rule: hotwire_view_skills_compulsory_view_upd ON hotwire."10_View/Skills_Compulsory"

-- DROP RULE hotwire_view_skills_compulsory_view_upd ON hotwire."10_View/Skills_Compulsory";

CREATE OR REPLACE RULE hotwire_view_skills_compulsory_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Compulsory" DO INSTEAD  UPDATE skills_compulsory SET compulsion = new.compulsion
  WHERE skills_compulsory.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Skills_Contacts" AS 
 SELECT skills_contacts.id, skills_contacts.contact
   FROM skills_contacts;

ALTER TABLE hotwire."10_View/Skills_Contacts"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Contacts" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Contacts" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Contacts" TO skills;

-- Rule: hotwire_view_skills_contacts_ins ON hotwire."10_View/Skills_Contacts"

-- DROP RULE hotwire_view_skills_contacts_ins ON hotwire."10_View/Skills_Contacts";

CREATE OR REPLACE RULE hotwire_view_skills_contacts_ins AS
    ON INSERT TO hotwire."10_View/Skills_Contacts" DO INSTEAD  INSERT INTO skills_contacts (contact) 
  VALUES (new.contact)
  RETURNING skills_contacts.id, skills_contacts.contact;

-- Rule: hotwire_view_skills_contacts_view_del ON hotwire."10_View/Skills_Contacts"

-- DROP RULE hotwire_view_skills_contacts_view_del ON hotwire."10_View/Skills_Contacts";

CREATE OR REPLACE RULE hotwire_view_skills_contacts_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Contacts" DO INSTEAD  DELETE FROM skills_contacts
  WHERE skills_contacts.id = old.id;

-- Rule: hotwire_view_skills_contacts_view_upd ON hotwire."10_View/Skills_Contacts"

-- DROP RULE hotwire_view_skills_contacts_view_upd ON hotwire."10_View/Skills_Contacts";

CREATE OR REPLACE RULE hotwire_view_skills_contacts_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Contacts" DO INSTEAD  UPDATE skills_contacts SET contact = new.contact
  WHERE skills_contacts.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Skills_Course_Sessions" AS 
 SELECT skills_sessions.id, skills_sessions.start_date, skills_sessions.start_text, skills_sessions.end_date, skills_sessions.end_text, skills_sessions.narrative_time, skills_sessions.venue, skills_sessions.provider
   FROM skills_sessions;

ALTER TABLE hotwire."10_View/Skills_Course_Sessions"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course_Sessions" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course_Sessions" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course_Sessions" TO old_cos;
GRANT ALL ON TABLE hotwire."10_View/Skills_Course_Sessions" TO skills;

-- Rule: hotwire_view_skills_course_sessions_del ON hotwire."10_View/Skills_Course_Sessions"

-- DROP RULE hotwire_view_skills_course_sessions_del ON hotwire."10_View/Skills_Course_Sessions";

CREATE OR REPLACE RULE hotwire_view_skills_course_sessions_del AS
    ON DELETE TO hotwire."10_View/Skills_Course_Sessions" DO INSTEAD  DELETE FROM skills_sessions
  WHERE skills_sessions.id = old.id;

-- Rule: hotwire_view_skills_course_sessions_ins ON hotwire."10_View/Skills_Course_Sessions"

-- DROP RULE hotwire_view_skills_course_sessions_ins ON hotwire."10_View/Skills_Course_Sessions";

CREATE OR REPLACE RULE hotwire_view_skills_course_sessions_ins AS
    ON INSERT TO hotwire."10_View/Skills_Course_Sessions" DO INSTEAD  INSERT INTO skills_sessions (start_date, start_text, end_date, end_text, narrative_time, venue, provider) 
  VALUES (new.start_date, new.start_text, new.end_date, new.end_text, new.narrative_time, new.venue, new.provider)
  RETURNING skills_sessions.id, skills_sessions.start_date, skills_sessions.start_text, skills_sessions.end_date, skills_sessions.end_text, skills_sessions.narrative_time, skills_sessions.venue, skills_sessions.provider;

-- Rule: hotwire_view_skills_course_sessions_upd ON hotwire."10_View/Skills_Course_Sessions"

-- DROP RULE hotwire_view_skills_course_sessions_upd ON hotwire."10_View/Skills_Course_Sessions";

CREATE OR REPLACE RULE hotwire_view_skills_course_sessions_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Course_Sessions" DO INSTEAD  UPDATE skills_sessions SET start_date = new.start_date, start_text = new.start_text, end_date = new.end_date, end_text = new.end_text, narrative_time = new.narrative_time, venue = new.venue, provider = new.provider
  WHERE skills_sessions.id = old.id;



CREATE OR REPLACE VIEW hotwire."10_View/Skills_Location" AS 
 SELECT skills_location.id, skills_location.location
   FROM skills_location;

ALTER TABLE hotwire."10_View/Skills_Location"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Location" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Location" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Location" TO skills;

-- Rule: hotwire_view_skills_location_ins ON hotwire."10_View/Skills_Location"

-- DROP RULE hotwire_view_skills_location_ins ON hotwire."10_View/Skills_Location";

CREATE OR REPLACE RULE hotwire_view_skills_location_ins AS
    ON INSERT TO hotwire."10_View/Skills_Location" DO INSTEAD  INSERT INTO skills_location (location) 
  VALUES (new.location)
  RETURNING skills_location.id, skills_location.location;

-- Rule: hotwire_view_skills_location_view_del ON hotwire."10_View/Skills_Location"

-- DROP RULE hotwire_view_skills_location_view_del ON hotwire."10_View/Skills_Location";

CREATE OR REPLACE RULE hotwire_view_skills_location_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Location" DO INSTEAD  DELETE FROM skills_location
  WHERE skills_location.id = old.id;

-- Rule: hotwire_view_skills_location_view_upd ON hotwire."10_View/Skills_Location"

-- DROP RULE hotwire_view_skills_location_view_upd ON hotwire."10_View/Skills_Location";

CREATE OR REPLACE RULE hotwire_view_skills_location_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Location" DO INSTEAD  UPDATE skills_location SET location = new.location
  WHERE skills_location.id = old.id;



CREATE OR REPLACE VIEW hotwire."10_View/Skills_Presenters" AS 
 SELECT skills_presenters.id, skills_presenters.presenter
   FROM skills_presenters;

ALTER TABLE hotwire."10_View/Skills_Presenters"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Presenters" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Presenters" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Presenters" TO skills;

-- Rule: hotwire_view_skills_presenters_ins ON hotwire."10_View/Skills_Presenters"

-- DROP RULE hotwire_view_skills_presenters_ins ON hotwire."10_View/Skills_Presenters";

CREATE OR REPLACE RULE hotwire_view_skills_presenters_ins AS
    ON INSERT TO hotwire."10_View/Skills_Presenters" DO INSTEAD  INSERT INTO skills_presenters (presenter) 
  VALUES (new.presenter)
  RETURNING skills_presenters.id, skills_presenters.presenter;

-- Rule: hotwire_view_skills_presenters_view_del ON hotwire."10_View/Skills_Presenters"

-- DROP RULE hotwire_view_skills_presenters_view_del ON hotwire."10_View/Skills_Presenters";

CREATE OR REPLACE RULE hotwire_view_skills_presenters_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Presenters" DO INSTEAD  DELETE FROM skills_presenters
  WHERE skills_presenters.id = old.id;

-- Rule: hotwire_view_skills_presenters_view_upd ON hotwire."10_View/Skills_Presenters"

-- DROP RULE hotwire_view_skills_presenters_view_upd ON hotwire."10_View/Skills_Presenters";

CREATE OR REPLACE RULE hotwire_view_skills_presenters_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Presenters" DO INSTEAD  UPDATE skills_presenters SET presenter = new.presenter
  WHERE skills_presenters.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Skills_Providers" AS 
 SELECT skills_providers.id, skills_providers.provider
   FROM skills_providers;

ALTER TABLE hotwire."10_View/Skills_Providers"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Providers" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Skills_Providers" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Skills_Providers" TO skills;

-- Rule: hotwire_view_skills_providers_ins ON hotwire."10_View/Skills_Providers"

-- DROP RULE hotwire_view_skills_providers_ins ON hotwire."10_View/Skills_Providers";

CREATE OR REPLACE RULE hotwire_view_skills_providers_ins AS
    ON INSERT TO hotwire."10_View/Skills_Providers" DO INSTEAD  INSERT INTO skills_providers (provider) 
  VALUES (new.provider)
  RETURNING skills_providers.id, skills_providers.provider;

-- Rule: hotwire_view_skills_providers_view_del ON hotwire."10_View/Skills_Providers"

-- DROP RULE hotwire_view_skills_providers_view_del ON hotwire."10_View/Skills_Providers";

CREATE OR REPLACE RULE hotwire_view_skills_providers_view_del AS
    ON DELETE TO hotwire."10_View/Skills_Providers" DO INSTEAD  DELETE FROM skills_providers
  WHERE skills_providers.id = old.id;

-- Rule: hotwire_view_skills_providers_view_upd ON hotwire."10_View/Skills_Providers"

-- DROP RULE hotwire_view_skills_providers_view_upd ON hotwire."10_View/Skills_Providers";

CREATE OR REPLACE RULE hotwire_view_skills_providers_view_upd AS
    ON UPDATE TO hotwire."10_View/Skills_Providers" DO INSTEAD  UPDATE skills_providers SET provider = new.provider
  WHERE skills_providers.id = old.id;

CREATE OR REPLACE VIEW hotwire.skills_bskills_hid AS 
 SELECT skills_bskills.id AS skills_bskills_id, skills_bskills.description::text AS skills_bskills_hid
   FROM skills_bskills;

ALTER TABLE hotwire.skills_bskills_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.skills_bskills_hid TO postgres;
GRANT ALL ON TABLE hotwire.skills_bskills_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.skills_course_sessions_hid AS 
 SELECT skills_sessions.id AS skills_course_sessions_id, ((COALESCE(to_char(skills_sessions.start_date::timestamp with time zone, 'DD Mon YY '::text) || skills_sessions.start_text::text, ''::character varying::text) || ' - '::text) || COALESCE(to_char(skills_sessions.start_date::timestamp with time zone, 'DD Mon YY '::text) || skills_sessions.end_text::text, ''::character varying::text)) || COALESCE(skills_sessions.narrative_time, ''::character varying)::text AS skills_course_sessions_hid
   FROM skills_sessions;

ALTER TABLE hotwire.skills_course_sessions_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.skills_course_sessions_hid TO postgres;
GRANT ALL ON TABLE hotwire.skills_course_sessions_hid TO ro_hid;



CREATE OR REPLACE VIEW hotwire.skills_courses_years_hid AS 
 SELECT skills_courses_years.id AS skills_courses_years_id, skills_courses_years.year AS skills_courses_years_hid
   FROM skills_courses_years;

ALTER TABLE hotwire.skills_courses_years_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.skills_courses_years_hid TO postgres;
GRANT ALL ON TABLE hotwire.skills_courses_years_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.skills_rcskills_hid AS 
 SELECT skills_rcskills.id AS skills_rcskills_id, ((skills_rcskills.class::text || skills_rcskills.subclass) || ': '::text) || skills_rcskills.description::text AS skills_rcskills_hid
   FROM skills_rcskills;

ALTER TABLE hotwire.skills_rcskills_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.skills_rcskills_hid TO postgres;
GRANT ALL ON TABLE hotwire.skills_rcskills_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.provider_hid AS 
 SELECT skills_providers.id AS provider_id, skills_providers.provider AS provider_hid
   FROM skills_providers;

ALTER TABLE hotwire.provider_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.provider_hid TO postgres;
GRANT ALL ON TABLE hotwire.provider_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.location_hid AS 
 SELECT skills_location.id AS location_id, skills_location.location AS location_hid
   FROM skills_location;

ALTER TABLE hotwire.location_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.location_hid TO postgres;
GRANT ALL ON TABLE hotwire.location_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.presenter_hid AS 
 SELECT skills_presenters.id AS presenter_id, skills_presenters.presenter AS presenter_hid
   FROM skills_presenters;

ALTER TABLE hotwire.presenter_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.presenter_hid TO postgres;
GRANT ALL ON TABLE hotwire.presenter_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.authoriser_hid AS 
 SELECT skills_authoriser.id AS authoriser_id, skills_authoriser.authoriser AS authoriser_hid
   FROM skills_authoriser;

ALTER TABLE hotwire.authoriser_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.authoriser_hid TO postgres;
GRANT ALL ON TABLE hotwire.authoriser_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.compulsion_hid AS 
 SELECT skills_compulsory.id AS compulsion_id, skills_compulsory.compulsion AS compulsion_hid
   FROM skills_compulsory;

ALTER TABLE hotwire.compulsion_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.compulsion_hid TO postgres;
GRANT ALL ON TABLE hotwire.compulsion_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.contact_hid AS 
 SELECT skills_contacts.id AS contact_id, skills_contacts.contact AS contact_hid
   FROM skills_contacts;

ALTER TABLE hotwire.contact_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.contact_hid TO postgres;
GRANT ALL ON TABLE hotwire.contact_hid TO ro_hid;

