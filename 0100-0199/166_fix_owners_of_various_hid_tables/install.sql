 ALTER TABLE hotwire."IT_strategic_goal_hid" OWNER TO dev;
 ALTER TABLE hotwire."IT_task_leader_hid" OWNER TO dev;
 ALTER TABLE hotwire."IT_task_status_hid" OWNER TO dev;
 ALTER TABLE hotwire."Primary_strategic_goal_hid" OWNER TO dev;
 ALTER TABLE hotwire."all_cos_hid" OWNER TO dev;
 ALTER TABLE hotwire."it_strategic_goal_hid" OWNER TO dev;
 ALTER TABLE hotwire."predecessor_task_hid" OWNER TO dev;
 ALTER TABLE hotwire."switch_model_hid" OWNER TO dev;
 ALTER TABLE hotwire."time_estimate_hid" OWNER TO dev;

 GRANT ALL ON TABLE hotwire."IT_strategic_goal_hid" TO dev;
 GRANT ALL ON TABLE hotwire."IT_task_leader_hid" TO dev;
 GRANT ALL ON TABLE hotwire."IT_task_status_hid" TO dev;
 GRANT ALL ON TABLE hotwire."Primary_strategic_goal_hid" TO dev;
 GRANT ALL ON TABLE hotwire."all_cos_hid" TO dev;
 GRANT ALL ON TABLE hotwire."it_strategic_goal_hid" TO dev;
 GRANT ALL ON TABLE hotwire."predecessor_task_hid" TO dev;
 GRANT ALL ON TABLE hotwire."switch_model_hid" TO dev;
 GRANT ALL ON TABLE hotwire."time_estimate_hid" TO dev;

 GRANT SELECT ON TABLE hotwire."IT_strategic_goal_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."IT_task_leader_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."IT_task_status_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."Primary_strategic_goal_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."all_cos_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."it_strategic_goal_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."predecessor_task_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."switch_model_hid" TO ro_hid;
 GRANT SELECT ON TABLE hotwire."time_estimate_hid" TO ro_hid;
