create view _retired_staff_mailinglist as
select
    person.email_address,
    person.crsid
FROM mm_mailinglist_include_person
left join person ON mm_mailinglist_include_person.include_person_id = person.id
where mm_mailinglist_include_person.mailinglist_id = ( select mailinglist.id FROM mailinglist WHERE mailinglist.name::text = 'chem-retiredstaff' );

alter view _retired_staff_mailinglist owner to dev;
grant select on _retired_staff_mailinglist to leavers_trigger, mailinglists, www_sites, cos;
