CREATE OR REPLACE FUNCTION update_mac_software(
    _mac macaddr,
    _name character varying,
    _info character varying,
    _signatory character varying,
    _version character varying,
    _path character varying,
    _from character varying)
  RETURNS character varying AS
$BODY$
declare
 _swid integer;
 _swvid integer;
 _instid integer;
 _system_image_id integer;
begin
 select id from system_image where _mac IN (wired_mac_1,wired_mac_2,wired_mac_3,wired_mac_4,wireless_mac) limit 1 into _system_image_id;
 if not found then
  return 'No such machine';
 end if;
 select id from mac_software where name=_name into _swid;
 if not found then
  insert into mac_software(name) values (_name) returning id into _swid;
 end if;
 select id from mac_software_version where software_id=_swid and version=_version and signatory=_signatory and infostring=_info into _swvid;
 if not found then
  insert into mac_software_version(software_id, version, signatory, infostring) values (_swid,_version,_signatory,_info) returning id into _swvid;
 end if;
 update mac_software_instance set lastseen=now() where system_image_id=_system_image_id and software_version_id=_swvid and location=_path and obtainedfrom=_from;
 if not found then
  insert into mac_software_instance (software_version_id,system_image_id,location,obtainedfrom,lastseen,firstseen) values (_swvid,_system_image_id,_path,_from,now(),now());
 end if;
 return 'OK';
end
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER
  COST 100;
ALTER FUNCTION update_mac_software(macaddr, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO rl201;

REVOKE ALL ON FUNCTION update_mac_software(macaddr, character varying, character varying, character varying, character varying, character varying, character varying) FROM osbuilder;
REVOKE ALL ON FUNCTION update_mac_software(macaddr, character varying, character varying, character varying, character varying, character varying, character varying) FROM dev;
GRANT EXECUTE ON FUNCTION update_mac_software(macaddr, character varying, character varying, character varying, character varying, character varying, character varying) TO PUBLIC;
