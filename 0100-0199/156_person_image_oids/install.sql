-- add new column
ALTER TABLE person ADD COLUMN image_lo oid;

-- populate it
ALTER TABLE person DISABLE TRIGGER crsid_to_user;
ALTER TABLE person DISABLE TRIGGER crsid_trigger;
ALTER TABLE person DISABLE TRIGGER person_trig;
ALTER TABLE person DISABLE TRIGGER physical_status_trig;
UPDATE person SET image_lo = image_oid::oid;
ALTER TABLE person ENABLE TRIGGER crsid_to_user;
ALTER TABLE person ENABLE TRIGGER crsid_trigger;
ALTER TABLE person ENABLE TRIGGER person_trig;
ALTER TABLE person ENABLE TRIGGER physical_status_trig;

-- add trigger to keep image_lo up to date
-- create function
CREATE OR REPLACE FUNCTION person_update_image_lo() RETURNS trigger AS
$BODY$
BEGIN
        NEW.image_lo := NEW.image_oid::oid;
RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;
ALTER FUNCTION person_update_image_lo() OWNER TO dev;
-- attach function to person
CREATE TRIGGER image_lo_from_image_oid BEFORE UPDATE OR INSERT ON person FOR EACH ROW EXECUTE PROCEDURE person_update_image_lo();

-- The next step is to update all the views that use the image_oid column to use image_lo instead
-- First they all need to be updated to read from image_lo and not image_oid
-- Then when they are all reading from it, they need to start updating image_lo directly and not image_oid
-- and after that drop the old image_oid column like this
-- ALTER TABLE person ALTER COLUMN image_lo SET DEFAULT 3977791;
-- ALTER TABLE person DROP TRIGGER image_lo_from_image_oid;
-- DROP FUNCTION person_update_image_lo();
-- ALTER TABLE person DROP COLUMN image_oid;
