ALTER TABLE netboot_options OWNER TO dev;
REVOKE ALL ON TABLE netboot_options FROM rl201;
GRANT ALL ON TABLE netboot_options TO dev;

ALTER VIEW hotwire.netboot_hid OWNER TO dev;
REVOKE ALL ON TABLE hotwire.netboot_hid FROM rl201;
GRANT ALL ON hotwire.netboot_hid TO dev;
GRANT SELECT ON hotwire.netboot_hid TO cos;
GRANT SELECT ON hotwire.netboot_hid TO ro_hid;
