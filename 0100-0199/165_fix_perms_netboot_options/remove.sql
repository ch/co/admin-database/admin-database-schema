ALTER TABLE netboot_options OWNER TO rl201;
GRANT ALL ON TABLE netboot_options TO rl201;
GRANT ALL ON TABLE netboot_options TO dev;

ALTER TABLE hotwire.netboot_hid OWNER TO rl201;
GRANT ALL ON TABLE hotwire.netboot_hid TO rl201;
GRANT ALL ON TABLE hotwire.netboot_hid TO cos;
GRANT SELECT ON TABLE hotwire.netboot_hid TO ro_hid;
