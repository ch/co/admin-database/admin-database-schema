CREATE OR REPLACE VIEW skills_rcskills_hid AS 
 SELECT skills_rcskills.id AS skills_rcskills_id, ((skills_rcskills.class::text || skills_rcskills.subclass) || ': '::text) || skills_rcskills.description::text AS skills_rcskills_hid
   FROM skills_rcskills;

ALTER TABLE skills_rcskills_hid
  OWNER TO postgres;
GRANT ALL ON TABLE skills_rcskills_hid TO postgres;
GRANT ALL ON TABLE skills_rcskills_hid TO dev;
GRANT ALL ON TABLE skills_rcskills_hid TO www_sites;
GRANT ALL ON TABLE skills_rcskills_hid TO old_cos;
GRANT ALL ON TABLE skills_rcskills_hid TO skills;
GRANT SELECT ON TABLE skills_rcskills_hid TO ro_hid;

CREATE OR REPLACE VIEW skills_courses_years_hid AS 
 SELECT skills_courses_years.id AS skills_courses_years_id, skills_courses_years.year AS skills_courses_years_hid
   FROM skills_courses_years;

ALTER TABLE skills_courses_years_hid
  OWNER TO postgres;
GRANT ALL ON TABLE skills_courses_years_hid TO postgres;
GRANT ALL ON TABLE skills_courses_years_hid TO old_cos;
GRANT ALL ON TABLE skills_courses_years_hid TO skills;
GRANT SELECT ON TABLE skills_courses_years_hid TO ro_hid;

CREATE OR REPLACE VIEW skills_course_sessions_hid AS 
 SELECT skills_sessions.id AS skills_course_sessions_id, ((COALESCE(to_char(skills_sessions.start_date::timestamp with time zone, 'DD Mon YY '::text) || skills_sessions.start_text::text, ''::character varying::text) || ' - '::text) || COALESCE(to_char(skills_sessions.start_date::timestamp with time zone, 'DD Mon YY '::text) || skills_sessions.end_text::text, ''::character varying::text)) || COALESCE(skills_sessions.narrative_time, ''::character varying)::text AS skills_course_sessions_hid
   FROM skills_sessions;

ALTER TABLE skills_course_sessions_hid
  OWNER TO postgres;
GRANT ALL ON TABLE skills_course_sessions_hid TO postgres;
GRANT SELECT ON TABLE skills_course_sessions_hid TO old_cos;
GRANT ALL ON TABLE skills_course_sessions_hid TO skills;
GRANT SELECT ON TABLE skills_course_sessions_hid TO ro_hid;

CREATE OR REPLACE VIEW skills_bskills_hid AS 
 SELECT skills_bskills.id AS skills_bskills_id, skills_bskills.description::text AS skills_bskills_hid
   FROM skills_bskills;

ALTER TABLE skills_bskills_hid
  OWNER TO postgres;
GRANT ALL ON TABLE skills_bskills_hid TO postgres;
GRANT ALL ON TABLE skills_bskills_hid TO dev;
GRANT ALL ON TABLE skills_bskills_hid TO www_sites;
GRANT ALL ON TABLE skills_bskills_hid TO old_cos;
GRANT ALL ON TABLE skills_bskills_hid TO skills;
GRANT SELECT ON TABLE skills_bskills_hid TO ro_hid;

CREATE OR REPLACE VIEW skills_course_view AS 
 SELECT skills_courses.id, skills_courses.rskillxmlid AS ro_rskillxmlid, skills_courses.title, skills_courses.description, skills_courses.lastevent, skills_courses.authoriser_id, skills_courses.compulsion_id, skills_courses.contact_id, skills_courses.location_id, skills_courses.presenter_id, skills_courses.provider_id, skills_courses.skills_credit::character varying(10) AS skills_credit, skills_rcskills_hid.skills_rcskills_hid AS mm_skills_rcskills, mm_skills_courses_skills_rcskills.skills_rcskills_id AS mm_skills_rcskills_id, skills_bskills_hid.skills_bskills_hid AS mm_skills_bskills, mm_skills_courses_bskills.skills_bskills_id AS mm_skills_bskills_id, skills_courses.information_url::character varying(250) AS information_url, skills_courses.information, skills_courses_years_hid.skills_courses_years_hid AS mm_skills_courses_years, mm_skills_courses_year.skills_courses_years_id AS mm_skills_courses_years_id, skills_course_sessions_hid.skills_course_sessions_hid AS mm_skills_course_sessions, mm_skills_course_sessions.skills_course_sessions_id AS mm_skills_course_sessions_id
   FROM skills_courses
   LEFT JOIN mm_skills_courses_skills_courses_years mm_skills_courses_year ON skills_courses.id = mm_skills_courses_year.skills_courses_id
   LEFT JOIN skills_courses_years_hid USING (skills_courses_years_id)
   LEFT JOIN mm_skills_courses_skills_rcskills ON skills_courses.id = mm_skills_courses_skills_rcskills.skills_courses_id
   LEFT JOIN skills_rcskills_hid USING (skills_rcskills_id)
   LEFT JOIN mm_skills_courses_skills_course_sessions mm_skills_course_sessions ON skills_courses.id = mm_skills_course_sessions.skills_courses_id
   LEFT JOIN skills_course_sessions_hid ON mm_skills_course_sessions.skills_course_sessions_id = skills_course_sessions_hid.skills_course_sessions_id
   LEFT JOIN mm_skills_courses_skills_bskills mm_skills_courses_bskills ON skills_courses.id = mm_skills_courses_bskills.skills_courses_id
   LEFT JOIN skills_bskills_hid USING (skills_bskills_id);

ALTER TABLE skills_course_view
  OWNER TO postgres;
GRANT ALL ON TABLE skills_course_view TO postgres;
GRANT ALL ON TABLE skills_course_view TO skills;
GRANT ALL ON TABLE skills_course_view TO old_cos;
GRANT ALL ON TABLE skills_course_view TO dev;

CREATE OR REPLACE FUNCTION fn_skills_course_ins(skills_course_view)
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         new_skills_course BIGINT;
 begin

 new_skills_course := nextval('skills_course_seq');

 insert into skills_courses
         (id, description, lastevent,information,information_url,title,skills_credit,authoriser_id,compulsion_id, contact_id,location_id,presenter_id,provider_id)
         values
         (
 new_skills_course, v.description, v.lastevent,v.information,v.information_url,v.title,v.skills_credit::numeric,
 v.authoriser_id,v.compulsion_id,
         v.contact_id,v.location_id,v.presenter_id,v.provider_id
         );

 perform fn_upd_many_many(new_skills_course, v.mm_skills_rcskills::text, 'skills_courses'::text, 'skills_rcskills'::text);
 perform fn_upd_many_many(new_skills_course, v.mm_skills_bskills::text, 'skills_courses'::text, 'skills_bskills'::text);
 perform fn_upd_many_many(new_skills_course, v.mm_skills_courses_years::text, 'skills_courses'::text, 'skills_courses_years'::text);
 perform fn_upd_many_many(new_skills_course, v.mm_skills_course_sessions::text, 'skills_courses'::text, 'skills_course_sessions'::text);

 return new_skills_course;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_skills_course_ins(skills_course_view)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION fn_skills_course_upd(skills_course_view)
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         new_skills_course BIGINT;
 begin


 update skills_courses set
  description=v.description,
  lastevent=v.lastevent,
  information=v.information,
  information_url=v.information_url,
  title=v.title,
  skills_credit=v.skills_credit::numeric,
  authoriser_id=v.authoriser_id,
  compulsion_id=v.compulsion_id,
  contact_id=v.contact_id,
  location_id=v.location_id,
  presenter_id=v.presenter_id,
  provider_id=v.provider_id
 WHERE id=v.id;

 perform fn_upd_many_many(v.id, v.mm_skills_rcskills::text, 'skills_courses'::text, 'skills_rcskills'::text);
 perform fn_upd_many_many(v.id, v.mm_skills_bskills::text, 'skills_courses'::text, 'skills_bskills'::text);
 perform fn_upd_many_many(v.id, v.mm_skills_courses_years::text, 'skills_courses'::text, 'skills_courses_years'::text);
 perform fn_upd_many_many(v.id, v.mm_skills_course_sessions::text, 'skills_courses'::text, 'skills_course_sessions'::text);
 return v.id;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_skills_course_upd(skills_course_view)
  OWNER TO postgres;

CREATE OR REPLACE RULE skills_course_ins AS
    ON INSERT TO skills_course_view DO INSTEAD  SELECT fn_skills_course_ins(new.*) AS id;

CREATE OR REPLACE RULE skills_course_upd AS
    ON UPDATE TO skills_course_view DO INSTEAD  SELECT fn_skills_course_upd(new.*) AS id;

