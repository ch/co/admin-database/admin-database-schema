DROP RULE skills_course_ins ON skills_course_view;
DROP RULE skills_course_upd ON skills_course_view;

DROP FUNCTION fn_skills_course_ins(skills_course_view);
DROP FUNCTION fn_skills_course_upd(skills_course_view);

DROP VIEW public.skills_course_view;

DROP VIEW public.skills_bskills_hid;
DROP VIEW public.skills_course_sessions_hid;
DROP VIEW public.skills_courses_years_hid;
DROP VIEW public.skills_rcskills_hid;

