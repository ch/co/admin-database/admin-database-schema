--
-- Name: person_collection; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE person_collection (
    id integer NOT NULL,
    name character varying,
    notes character varying,
    ad_group character varying,
    db_group name,
    view name
);


ALTER TABLE public.person_collection OWNER TO postgres;

--
-- Name: TABLE person_collection; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE person_collection IS 'Groups are formed by taking
 (base_view UNION add_view) left join del_view where del_view.crsid is null

i.e. 
 all of base_view and add_view but /none/ of del_view.';


--
-- Name: person_collection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_collection_id_seq OWNER TO postgres;

--
-- Name: person_collection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE person_collection_id_seq OWNED BY person_collection.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person_collection ALTER COLUMN id SET DEFAULT nextval('person_collection_id_seq'::regclass);


--
-- Data for Name: person_collection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY person_collection (id, name, notes, ad_group, db_group, view) FROM stdin;
1	COs-test	A test group - COs without TKD	CosRoot	costest	_collview_cosroot
\.


--
-- Name: person_collection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('person_collection_id_seq', 1, true);


--
-- Name: person_collection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY person_collection
    ADD CONSTRAINT person_collection_pkey PRIMARY KEY (id);

CREATE OR REPLACE VIEW _collview_cosroot_sub AS 
 SELECT 'tkd25'::character varying AS crsid;

ALTER TABLE _collview_cosroot_sub
  OWNER TO postgres;

CREATE OR REPLACE VIEW _collview_cosroot_base AS 
 SELECT person.crsid
   FROM person
   JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
   JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
  WHERE research_group.name::text = 'COs'::text;

ALTER TABLE _collview_cosroot_base
  OWNER TO postgres;

CREATE OR REPLACE VIEW _collview_cosroot AS 
 SELECT a.crsid
   FROM _collview_cosroot_base a
   LEFT JOIN _collview_cosroot_sub x ON a.crsid::text = x.crsid::text
  WHERE x.crsid IS NULL;

ALTER TABLE _collview_cosroot
  OWNER TO postgres;


CREATE FUNCTION fn_synccolltodb(_collection character varying)
  RETURNS SETOF character varying AS
$BODY$
declare
 _viewname name;
 _dbgroup name;
 _currentusers varchar[];
 _targetusers varchar[];
 _addusers varchar[];
 _delusers varchar[];
 _crsid varchar;
begin
 return next 'Searching for collection name = '||_collection;
 select "view","db_group" from person_collection where name=_collection and db_group is not null into _viewname,_dbgroup;
 return next 'Viewname '||coalesce(_viewname,'NULL');
 return next 'DB group '||coalesce(_dbgroup,'NULL');
 -- Ignore if not found.
 if found then
  -- Does the group exist?
  return next 'Found collection';
  perform rolname from pg_authid g where not g.rolcanlogin and rolname=_dbgroup;
  if not found then
   return next 'Group '||_dbgroup||' found';
   -- Create role
   execute 'create group '||_dbgroup||' nologin';
  end if;
  -- Sync role
  execute 'select array(select crsid from '||_viewname||')' into _targetusers;
  select array(select u.rolname from pg_authid g inner join pg_auth_members on roleid=g.oid inner join pg_authid u on u.oid=member where not g.rolcanlogin and g.rolname=_dbgroup) into _currentusers;
  return next 'Target users'||array_to_string(_targetusers,',');
  return next 'Current users'||array_to_string(_currentusers,',');
  _addusers=fn_array_diff_left(_targetusers,_currentusers);
  _delusers=fn_array_diff_right(_targetusers,_currentusers);
  for _crsid in select unnest(_addusers) LOOP
   execute 'grant '||_dbgroup||' to '||_crsid;
   return next 'Add user '||_crsid;
  end loop;
  for _crsid in select unnest(_delusers) LOOP
   execute 'revoke '||_dbgroup||' from '||_crsid;
   return next 'Del user '||_crsid;
  end loop;
  
  
 end if;
 return next 'Ends';
end
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION fn_synccolltodb(character varying)
  OWNER TO rl201;
COMMENT ON FUNCTION fn_synccolltodb(character varying) IS 'A Privileged function synchronizes a postgres group with a view in the DB.';


