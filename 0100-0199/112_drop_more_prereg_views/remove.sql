
CREATE OR REPLACE VIEW hotwire."10_View/Jackdaw_Signup" AS 
 SELECT user_prereg.id, user_prereg.user_prereg_standing_id AS "status_(required)_id", user_prereg.user_prereg_title_id AS "academic_title_(optional)_id", user_prereg.forename AS "given_or_first_name_(required)", user_prereg.surname AS "family/last_name_(required)", user_prereg.gender_id AS "gender_(required)_id", user_prereg.birth_date AS "date_of_birth_(required)", user_prereg.start_date AS "starting_date_(optional)", user_prereg.leaving_date AS "leaving_date_(optional)", user_prereg.cambridge_college_id, 'If you have ever been in the University before, in any capacity, please tell us the CRSID allocated to you then, if you remember it. If you had a previous surname during your time in the University, that would also be useful.'::text AS ro_note, user_prereg.previous_surname AS "previous_surname_(if_appropriate)", user_prereg.old_crsid AS "previous_crsid_(if_any)", user_prereg.email AS "email_address_(required)"
   FROM user_prereg;

ALTER TABLE hotwire."10_View/Jackdaw_Signup"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Jackdaw_Signup" TO postgres;
GRANT SELECT, INSERT ON TABLE hotwire."10_View/Jackdaw_Signup" TO newusersignup;

CREATE OR REPLACE RULE hotwire_view_jackdaw_signup_view_ins AS
    ON INSERT TO hotwire."10_View/Jackdaw_Signup" DO INSTEAD  INSERT INTO user_prereg (user_prereg_standing_id, user_prereg_title_id, forename, surname, gender_id, birth_date, start_date, leaving_date, cambridge_college_id, previous_surname, old_crsid, email) 
      VALUES (new."status_(required)_id", new."academic_title_(optional)_id", new."given_or_first_name_(required)", new."family/last_name_(required)", new."gender_(required)_id", new."date_of_birth_(required)", new."starting_date_(optional)", new."leaving_date_(optional)", new.cambridge_college_id, new."previous_surname_(if_appropriate)", new."previous_crsid_(if_any)", new."email_address_(required)")
      RETURNING user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, 'If you have ever been in the University before, in any capacity, please tell us the CRSID allocated to you then, if you remember it. If you had a previous surname during your time in the University, that would also be useful.'::text AS text, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.email;


-- DROP VIEW hotwire."10_View/User_Prereg";

CREATE OR REPLACE VIEW hotwire."10_View/User_Prereg" AS 
 SELECT user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, user_prereg.done, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.ucam_previous, user_prereg.email
   FROM user_prereg
  ORDER BY user_prereg.id DESC;

ALTER TABLE hotwire."10_View/User_Prereg"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/User_Prereg" TO postgres;
GRANT SELECT, UPDATE, INSERT ON TABLE hotwire."10_View/User_Prereg" TO newusersignup;
GRANT ALL ON TABLE hotwire."10_View/User_Prereg" TO cos;
ALTER TABLE hotwire."10_View/User_Prereg" ALTER COLUMN id SET DEFAULT nextval('user_prereg_seq'::regclass);
ALTER TABLE hotwire."10_View/User_Prereg" ALTER COLUMN done SET DEFAULT false;
ALTER TABLE hotwire."10_View/User_Prereg" ALTER COLUMN ucam_previous SET DEFAULT false;

CREATE OR REPLACE RULE hotwire_view_user_prereg_del AS
    ON DELETE TO hotwire."10_View/User_Prereg" DO INSTEAD  DELETE FROM user_prereg
      WHERE user_prereg.id = old.id;

CREATE OR REPLACE RULE hotwire_view_user_prereg_ins AS
    ON INSERT TO hotwire."10_View/User_Prereg" DO INSTEAD  INSERT INTO user_prereg (id, user_prereg_standing_id, user_prereg_title_id, forename, surname, gender_id, birth_date, start_date, leaving_date, cambridge_college_id, done, previous_surname, old_crsid, ucam_previous, email) 
      VALUES (new.id, new.user_prereg_standing_id, new.user_prereg_title_id, new.forename, new.surname, new.gender_id, new.birth_date, new.start_date, new.leaving_date, new.cambridge_college_id, new.done, new.previous_surname, new.old_crsid, new.ucam_previous, new.email)
      RETURNING user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, user_prereg.done, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.ucam_previous, user_prereg.email;

CREATE OR REPLACE RULE hotwire_view_user_prereg_upd AS
    ON UPDATE TO hotwire."10_View/User_Prereg" DO INSTEAD  UPDATE user_prereg SET id = new.id, user_prereg_standing_id = new.user_prereg_standing_id, user_prereg_title_id = new.user_prereg_title_id, forename = new.forename, surname = new.surname, gender_id = new.gender_id, birth_date = new.birth_date, start_date = new.start_date, leaving_date = new.leaving_date, cambridge_college_id = new.cambridge_college_id, done = new.done, previous_surname = new.previous_surname, old_crsid = new.old_crsid, ucam_previous = new.ucam_previous, email = new.email
      WHERE user_prereg.id = old.id;


CREATE OR REPLACE VIEW _user_prereg_crsids_to_collect AS 
 SELECT user_prereg.id, user_prereg.email, user_prereg_standing_hid.user_prereg_standing_hid AS standing, user_prereg_title_hid.user_prereg_title_hid AS title, user_prereg.forename, user_prereg.surname, gender_hid.gender_hid AS gender, user_prereg.start_date, user_prereg.leaving_date, cambridge_college.name AS college, user_prereg.previous_surname
   FROM user_prereg
   LEFT JOIN user_prereg_title_hid USING (user_prereg_title_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN cambridge_college ON user_prereg.cambridge_college_id = cambridge_college.id
   JOIN user_prereg_standing_hid USING (user_prereg_standing_id)
  WHERE user_prereg.make_ad_accounts_in_advance = true AND user_prereg.assigned_crsid IS NULL AND user_prereg.mailed_admin_team <> true;

ALTER TABLE _user_prereg_crsids_to_collect
  OWNER TO postgres;
GRANT ALL ON TABLE _user_prereg_crsids_to_collect TO postgres;
GRANT SELECT ON TABLE _user_prereg_crsids_to_collect TO _jackdaw_prereg;

CREATE OR REPLACE VIEW _user_prereg_jackdaw AS 
 SELECT user_prereg.id, user_prereg.email, user_prereg_standing_hid.user_prereg_standing_hid AS standing, user_prereg_title_hid.user_prereg_title_hid AS title, user_prereg.forename, user_prereg.surname, "substring"(gender_hid.gender_hid::text, 1, 1) AS sex, 
        CASE user_prereg.start_date > now()
            WHEN true THEN to_char(user_prereg.start_date::timestamp with time zone, 'DD'::text)
            WHEN false THEN ''::text
            ELSE NULL::text
        END AS starting_day, 
        CASE user_prereg.start_date > now()
            WHEN true THEN to_char(user_prereg.start_date::timestamp with time zone, 'Mon'::text)
            WHEN false THEN ''::text
            ELSE NULL::text
        END AS starting_month, 
        CASE user_prereg.start_date > now()
            WHEN true THEN to_char(user_prereg.start_date::timestamp with time zone, 'YYYY'::text)
            WHEN false THEN ''::text
            ELSE NULL::text
        END AS starting_year, to_char(user_prereg.birth_date::timestamp with time zone, 'DD'::text) AS birth_day, to_char(user_prereg.birth_date::timestamp with time zone, 'Mon'::text) AS birth_month, to_char(user_prereg.birth_date::timestamp with time zone, 'YYYY'::text) AS birth_year, to_char(user_prereg.leaving_date::timestamp with time zone, 'DD'::text) AS leaving_day, to_char(user_prereg.leaving_date::timestamp with time zone, 'Mon'::text) AS leaving_month, to_char(user_prereg.leaving_date::timestamp with time zone, 'YYYY'::text) AS leaving_year, cambridge_college.cs_abbrev AS coll, user_prereg.ucam_previous AS here_before, user_prereg.previous_surname AS oldname, user_prereg.old_crsid AS oldid, 'CHEM'::text AS inst
   FROM user_prereg
   JOIN user_prereg_standing_hid USING (user_prereg_standing_id)
   LEFT JOIN user_prereg_title_hid USING (user_prereg_title_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN cambridge_college ON user_prereg.cambridge_college_id = cambridge_college.id
  WHERE NOT user_prereg.done;

ALTER TABLE _user_prereg_jackdaw
  OWNER TO postgres;
GRANT ALL ON TABLE _user_prereg_jackdaw TO postgres;
GRANT SELECT ON TABLE _user_prereg_jackdaw TO _jackdaw_prereg;

CREATE OR REPLACE VIEW jackdaw_signup_view AS 
 SELECT user_prereg.id, user_prereg.user_prereg_standing_id AS "status_(required)_id", user_prereg.user_prereg_title_id AS "academic_title_(optional)_id", user_prereg.forename AS "given_or_first_name_(required)", user_prereg.surname AS "family/last_name_(required)", user_prereg.gender_id AS "gender_(required)_id", user_prereg.birth_date AS "date_of_birth_(required)", user_prereg.start_date AS "starting_date_(optional)", user_prereg.leaving_date AS "leaving_date_(required)", user_prereg.cambridge_college_id, 'If you have ever been in the University before, in any capacity, please tell us the CRSID allocated to you then, if you remember it. If you had a previous surname during your time in the University, that would also be useful.'::text AS ro_note, user_prereg.previous_surname AS "previous_surname_(if_appropriate)", user_prereg.old_crsid AS "previous_crsid_(if_any)", user_prereg.email AS "email_address_(required)"
   FROM user_prereg;

ALTER TABLE jackdaw_signup_view
  OWNER TO cen1001;
GRANT ALL ON TABLE jackdaw_signup_view TO cen1001;
GRANT SELECT, INSERT ON TABLE jackdaw_signup_view TO newusersignup;

CREATE OR REPLACE RULE jackdaw_signup_view_ins AS
    ON INSERT TO jackdaw_signup_view DO INSTEAD  INSERT INTO user_prereg (user_prereg_standing_id, user_prereg_title_id, forename, surname, gender_id, birth_date, start_date, leaving_date, cambridge_college_id, previous_surname, old_crsid, email) 
  VALUES (new."status_(required)_id", new."academic_title_(optional)_id", new."given_or_first_name_(required)", new."family/last_name_(required)", new."gender_(required)_id", new."date_of_birth_(required)", new."starting_date_(optional)", new."leaving_date_(required)", new.cambridge_college_id, new."previous_surname_(if_appropriate)", new."previous_crsid_(if_any)", new."email_address_(required)")
  RETURNING user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, 'If you have ever been in the University before, in any capacity, please tell us the CRSID allocated to you then, if you remember it. If you had a previous surname during your time in the University, that would also be useful.'::text AS text, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.email;

CREATE OR REPLACE VIEW user_prereg_view AS 
 SELECT user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, user_prereg.done, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.ucam_previous, user_prereg.email
   FROM user_prereg
  ORDER BY user_prereg.id DESC;

ALTER TABLE user_prereg_view
  OWNER TO postgres;
GRANT ALL ON TABLE user_prereg_view TO postgres;
GRANT SELECT, UPDATE, INSERT ON TABLE user_prereg_view TO newusersignup;
GRANT ALL ON TABLE user_prereg_view TO cos;
ALTER TABLE user_prereg_view ALTER COLUMN id SET DEFAULT nextval('user_prereg_seq'::regclass);
ALTER TABLE user_prereg_view ALTER COLUMN done SET DEFAULT false;
ALTER TABLE user_prereg_view ALTER COLUMN ucam_previous SET DEFAULT false;

CREATE OR REPLACE RULE user_prereg_del AS
    ON DELETE TO user_prereg_view DO INSTEAD  DELETE FROM user_prereg
  WHERE user_prereg.id = old.id;

CREATE OR REPLACE RULE user_prereg_ins AS
    ON INSERT TO user_prereg_view DO INSTEAD  INSERT INTO user_prereg (id, user_prereg_standing_id, user_prereg_title_id, forename, surname, gender_id, birth_date, start_date, leaving_date, cambridge_college_id, done, previous_surname, old_crsid, ucam_previous, email) 
  VALUES (new.id, new.user_prereg_standing_id, new.user_prereg_title_id, new.forename, new.surname, new.gender_id, new.birth_date, new.start_date, new.leaving_date, new.cambridge_college_id, new.done, new.previous_surname, new.old_crsid, new.ucam_previous, new.email)
  RETURNING user_prereg.id, user_prereg.user_prereg_standing_id, user_prereg.user_prereg_title_id, user_prereg.forename, user_prereg.surname, user_prereg.gender_id, user_prereg.birth_date, user_prereg.start_date, user_prereg.leaving_date, user_prereg.cambridge_college_id, user_prereg.done, user_prereg.previous_surname, user_prereg.old_crsid, user_prereg.ucam_previous, user_prereg.email;

CREATE OR REPLACE RULE user_prereg_upd AS
    ON UPDATE TO user_prereg_view DO INSTEAD  UPDATE user_prereg SET id = new.id, user_prereg_standing_id = new.user_prereg_standing_id, user_prereg_title_id = new.user_prereg_title_id, forename = new.forename, surname = new.surname, gender_id = new.gender_id, birth_date = new.birth_date, start_date = new.start_date, leaving_date = new.leaving_date, cambridge_college_id = new.cambridge_college_id, done = new.done, previous_surname = new.previous_surname, old_crsid = new.old_crsid, ucam_previous = new.ucam_previous, email = new.email
  WHERE user_prereg.id = old.id;
