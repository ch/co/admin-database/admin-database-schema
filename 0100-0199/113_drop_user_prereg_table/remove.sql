CREATE OR REPLACE FUNCTION user_prereg_must_end()
  RETURNS trigger AS
$BODY$BEGIN
IF (Now()+'2 weeks'::interval > NEW.leaving_date) THEN
     RAISE EXCEPTION 'Must not be leaving in the next fortnight';
END IF;
if (NEW.leaving_date IS NULL) THEN
     RAISE EXCEPTION 'Must be planning to leave at some stage';
END IF;
RETURN NEW;
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION user_prereg_must_end()
  OWNER TO rl201;
COMMENT ON FUNCTION user_prereg_must_end() IS 'To ensure that leaving dates are set and at least a fortnight away. See #33308';

CREATE OR REPLACE FUNCTION user_prereg_no_kids()
  RETURNS trigger AS
$BODY$BEGIN
IF (Now() < NEW.birth_date + '16 years'::interval) THEN
     RAISE EXCEPTION 'Must be over 16 to work here. Please check date of birth';
END IF;
RETURN NEW;
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION user_prereg_no_kids()
  OWNER TO rl201;
COMMENT ON FUNCTION user_prereg_no_kids() IS 'To ensure that we don''t try to register child prodigies!';

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: user_prereg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_prereg_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.user_prereg_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: user_prereg; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE user_prereg (
    id integer DEFAULT nextval('user_prereg_seq'::regclass) NOT NULL,
    user_prereg_standing_id integer NOT NULL,
    user_prereg_title_id integer,
    forename character varying(100),
    surname character varying(100) NOT NULL,
    gender_id integer DEFAULT 1 NOT NULL,
    birth_date date NOT NULL,
    start_date date,
    leaving_date date,
    cambridge_college_id integer,
    done boolean DEFAULT false,
    previous_surname character varying(100),
    old_crsid character varying(12),
    ucam_previous boolean DEFAULT false NOT NULL,
    email character varying(120) NOT NULL,
    requesting_person_crsid character varying(8),
    make_ad_accounts_in_advance boolean DEFAULT false,
    assigned_crsid character varying(8),
    mailed_admin_team boolean DEFAULT false,
    research_group_id bigint
);


ALTER TABLE public.user_prereg OWNER TO rl201;

--
-- Name: user_prereg_key; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY user_prereg
    ADD CONSTRAINT user_prereg_key PRIMARY KEY (id);


--
-- Name: user_prereg_id; Type: INDEX; Schema: public; Owner: rl201; Tablespace: 
--

CREATE UNIQUE INDEX user_prereg_id ON user_prereg USING btree (id);


--
-- Name: user_prereg_must_end; Type: TRIGGER; Schema: public; Owner: rl201
--

CREATE TRIGGER user_prereg_must_end
    BEFORE INSERT ON user_prereg
    FOR EACH ROW
    EXECUTE PROCEDURE user_prereg_must_end();


--
-- Name: user_prereg_no_kids; Type: TRIGGER; Schema: public; Owner: rl201
--

CREATE TRIGGER user_prereg_no_kids
    BEFORE INSERT ON user_prereg
    FOR EACH ROW
    EXECUTE PROCEDURE user_prereg_no_kids();


--
-- Name: TRIGGER user_prereg_no_kids ON user_prereg; Type: COMMENT; Schema: public; Owner: rl201
--

COMMENT ON TRIGGER user_prereg_no_kids ON user_prereg IS 'Ensure no child prodigies';


--
-- Name: user_prereg_fk_research_group; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY user_prereg
    ADD CONSTRAINT user_prereg_fk_research_group FOREIGN KEY (research_group_id) REFERENCES research_group(id);


--
-- Name: user_prereg_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE user_prereg_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_prereg_seq FROM postgres;
GRANT ALL ON SEQUENCE user_prereg_seq TO postgres;
GRANT ALL ON SEQUENCE user_prereg_seq TO newusersignup;


--
-- Name: user_prereg; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON TABLE user_prereg FROM PUBLIC;
REVOKE ALL ON TABLE user_prereg FROM rl201;
GRANT ALL ON TABLE user_prereg TO rl201;
GRANT ALL ON TABLE user_prereg TO dev;
GRANT SELECT,UPDATE ON TABLE user_prereg TO _jackdaw_prereg;
GRANT ALL ON TABLE user_prereg TO cos;


--
-- PostgreSQL database dump complete
--

