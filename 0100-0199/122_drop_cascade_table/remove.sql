CREATE TABLE _cascade (
    source name NOT NULL,
    target name NOT NULL
);

ALTER TABLE public._cascade OWNER TO sjt71;

ALTER TABLE ONLY _cascade ADD CONSTRAINT pk_cascade PRIMARY KEY (source, target);
