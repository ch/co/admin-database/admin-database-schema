CREATE OR REPLACE VIEW _latest_role_v9 AS 
 SELECT l.person_id, l.start_date, l.intended_end_date, l.estimated_leaving_date, l.funding_end_date, l.end_date, l.supervisor_id, l.co_supervisor_id, l.mentor_id, l.post_category_id, l.post_category, l.status, l.funding, l.fees_funding, l.research_grant_number, l.paid_by_university, l.hesa_leaving_code, l.chem, l.role_id, l.role_tablename, l.role_target_viewname
   FROM _all_roles_v11 l
   JOIN ( SELECT lengths.person_id, lengths.fixed_start_date, max(lengths.length_of_role) AS max_length
           FROM _all_roles_v11 lengths
      JOIN ( SELECT starts.person_id, max(starts.fixed_start_date) AS max_start
                   FROM _all_roles_v11 starts
                  WHERE starts.fixed_start_date <= now()
                  GROUP BY starts.person_id) newest_start ON newest_start.person_id = lengths.person_id AND newest_start.max_start = lengths.fixed_start_date
     GROUP BY lengths.person_id, lengths.fixed_start_date) latest ON latest.person_id = l.person_id AND latest.max_length = l.length_of_role AND latest.fixed_start_date = l.fixed_start_date
  ORDER BY l.person_id;

ALTER TABLE _latest_role_v9
  OWNER TO rl201;
GRANT ALL ON TABLE _latest_role_v9 TO rl201;
GRANT SELECT ON TABLE _latest_role_v9 TO adreconcile;
GRANT SELECT ON TABLE _latest_role_v9 TO dev;
GRANT SELECT ON TABLE _latest_role_v9 TO public;
COMMENT ON VIEW _latest_role_v9
  IS 'Slightly faster, I think. RFL 2012-06-15';

CREATE OR REPLACE VIEW personnel_accounts_view AS 
 SELECT person.id, person_hid.person_hid AS ro_person, person.image_oid AS _image_oid, person.surname AS ro_surname, person.first_names AS ro_first_names, person.title_id AS ro_title_id, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, person.crsid AS ro_crsid, person.email_address AS ro_email_address, _latest_role_v9.supervisor_id AS ro_supervisor_id, person.ni_number, person.payroll_personal_reference_number, _latest_role_v9.post_category_id AS ro_post_category_id, postgraduate_studentship.emplid_number AS ro_emplid_number, postgraduate_studentship.gaf_number AS ro_gaf_number, postgraduate_studentship.date_registered_for_phd AS ro_date_registered_for_phd, postgraduate_studentship.cpgs_or_mphil_date_submission_due AS ro_cpgs_or_mphil_date_submission_due, postgraduate_studentship.mphil_date_submission_due AS ro_mphil_date_submission_due, postgraduate_studentship.date_phd_submission_due AS ro_date_phd_submission_due, postgraduate_studentship.end_of_registration_date AS ro_end_of_registration_date, _latest_role_v9.funding_end_date AS ro_funding_end_date, _physical_status_v3.status_id AS ro_status_id, 
        CASE
            WHEN _physical_status_v3.status_id::text = 'Past'::text THEN 'orange'::text
            WHEN _physical_status_v3.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
            ELSE NULL::text
        END AS _hl_status
   FROM person
   LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
   LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)
   LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
   LEFT JOIN room_hid USING (room_id)
   LEFT JOIN _latest_role_v9 ON _latest_role_v9.person_id = person.id
   LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
   LEFT JOIN person_hid ON person.id = person_hid.person_id
   LEFT JOIN postgraduate_studentship ON postgraduate_studentship.person_id = person.id
  ORDER BY person.surname, person.first_names;

ALTER TABLE personnel_accounts_view OWNER TO dev;
GRANT ALL ON TABLE personnel_accounts_view TO dev;
GRANT ALL ON TABLE personnel_accounts_view TO cen1001;
GRANT SELECT, UPDATE ON TABLE personnel_accounts_view TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE personnel_accounts_view TO accounts;

CREATE OR REPLACE RULE personnel_accounts_upd AS
    ON UPDATE TO personnel_accounts_view DO INSTEAD  UPDATE person SET payroll_personal_reference_number = new.payroll_personal_reference_number, ni_number = new.ni_number
  WHERE person.id = old.id;

CREATE OR REPLACE VIEW apps.jmg11_ticket89275_people AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address, person.surname, person.first_names, person.name_suffix, title_hid.title_hid AS title, college.name, college.website AS college_website, _latest_role.post_category, s.status_id AS physical_status, ( SELECT research_interest_group.name
           FROM research_interest_group
      LEFT JOIN mm_member_research_interest_group mmpr ON research_interest_group.id = mmpr.research_interest_group_id
     WHERE mmpr.is_primary = true AND mmpr.member_id = person.id) AS primary_rig, ARRAY( SELECT research_interest_group.name
           FROM research_interest_group
      LEFT JOIN mm_member_research_interest_group mm ON mm.research_interest_group_id = research_interest_group.id
     WHERE (mm.is_primary = false OR mm.is_primary = NULL::boolean) AND mm.member_id = person.id) AS secondary_rigs, ARRAY( SELECT research_group.name
           FROM research_group
      LEFT JOIN mm_person_research_group mmr ON research_group.id = mmr.research_group_id
     WHERE mmr.person_id = person.id) AS in_research_groups, ARRAY( SELECT research_group.name
           FROM research_group
          WHERE research_group.head_of_group_id = person.id) AS heads_research_groups, ARRAY( SELECT dept_telephone_number.extension_number
           FROM dept_telephone_number
      LEFT JOIN mm_person_dept_telephone_number mmt ON mmt.dept_telephone_number_id = dept_telephone_number.id
     WHERE mmt.person_id = person.id) AS telephone_numbers
   FROM person
   LEFT JOIN cambridge_college college ON person.cambridge_college_id = college.id
   LEFT JOIN title_hid ON person.title_id = title_hid.title_id
   JOIN _latest_role_v9 _latest_role ON person.id = _latest_role.person_id
   JOIN _physical_status_v3 s ON s.person_id = person.id
  WHERE person.do_not_show_on_website IS FALSE AND person.crsid IS NOT NULL AND (_latest_role.post_category::text = 'Academic staff'::text OR _latest_role.post_category::text = 'Academic-related staff'::text OR _latest_role.post_category::text = 'Teaching Fellow'::text OR _latest_role.post_category::text = 'Retired'::text OR person.counts_as_academic = true);

ALTER TABLE apps.jmg11_ticket89275_people OWNER TO dev;
GRANT ALL ON TABLE apps.jmg11_ticket89275_people TO dev;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_people TO jmg11;
GRANT SELECT ON TABLE apps.jmg11_ticket89275_people TO jmg11_89275;

