CREATE OR REPLACE VIEW switchstack_list AS 
 SELECT switchstack.id, switchstack.name, switchstack.description, cabinet.name AS "Cabinet"
   FROM switchstack
   JOIN cabinet ON switchstack.cabinet_id = cabinet.id;

ALTER TABLE switchstack_list
  OWNER TO sjt71;
