CREATE OR REPLACE FUNCTION fn_test(v_this text)
  RETURNS text AS
$BODY$

declare
  v_table text;

begin
  EXECUTE 'SELECT view FROM _mm_views WHERE view LIKE '
        || quote_literal('%' || v_this || '%') INTO v_table;

  return v_table;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_test(text)
  OWNER TO sjt71;

