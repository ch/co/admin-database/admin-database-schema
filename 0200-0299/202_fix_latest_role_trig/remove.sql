CREATE OR REPLACE FUNCTION update_latest_role_trig()
  RETURNS trigger AS
$BODY$BEGIN

update cache._latest_role set
start_date=l.start_date,
intended_end_date=l.intended_end_date,
estimated_leaving_date=l.estimated_leaving_date,
funding_end_date=l.funding_end_date,
end_date=l.end_date,
supervisor_id=l.supervisor_id,
co_supervisor_id=l.co_supervisor_id,
mentor_id=l.mentor_id,
post_category_id=l.post_category_id,
post_category=l.post_category,
status=l.status,
funding=l.funding,
fees_funding=l.fees_funding,
research_grant_number=l.research_grant_number,
paid_by_university=l.paid_by_university,
hesa_leaving_code=l.hesa_leaving_code,
chem=l.chem,
role_id=l.role_id,
role_tablename=l.role_tablename,
role_target_viewname=l.role_target_viewname
from _latest_role_v9 l where _latest_role.person_id=l.person_id and l.person_id=new.person_id;
if not found then
 insert into cache._latest_role select * from _latest_role_v9 where person_id=new.person_id;
end if;
return new;
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION update_latest_role_trig()
  OWNER TO dev;

