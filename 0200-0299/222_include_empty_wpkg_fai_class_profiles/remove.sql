CREATE OR REPLACE VIEW apps.wpkg_fai_class_profiles as
 SELECT fai_class_hid.fai_class_hid AS profile, array_to_string(array_agg(software_package.wpkg_package_name), ','::text) AS packages
   FROM software_package
   JOIN mm_fai_class_software_package ON software_package.id = mm_fai_class_software_package.software_package_id
   JOIN fai_class_hid ON fai_class_hid.fai_class_id = mm_fai_class_software_package.fai_class_id
  GROUP BY fai_class_hid.fai_class_hid;

