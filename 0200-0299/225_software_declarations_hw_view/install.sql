-- Heads of group see their own groups
-- COs see everything
CREATE VIEW hotwire."10_View/Research_Groups/Software_Licence_Declaration" AS
SELECT
    research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    CASE
        WHEN latest_software_declaration.declaration_date IS NULL THEN 'f'::boolean
        WHEN latest_software_declaration.declaration_date + interval '1 year' > current_date THEN 't'::boolean
        ELSE 'f'::boolean
    END AS all_software_used_by_this_group_is_correctly_licenced,
    latest_software_declaration.declaration_date AS ro_date_of_last_declaration,
    latest_software_declaration.username_of_signer AS ro_declared_by
FROM research_group
LEFT JOIN (
    SELECT 
        research_group_id,
        max(declaration_date) as declaration_date
    FROM group_software_licence_declarations
    GROUP BY research_group_id 
    ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id
LEFT JOIN group_software_licence_declarations latest_software_declaration ON (latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id)
JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id
WHERE current_user = head_of_group.crsid OR pg_has_role(current_user,'cos','member')
ORDER BY research_group.name
;

ALTER TABLE hotwire."10_View/Research_Groups/Software_Licence_Declaration" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire."10_View/Research_Groups/Software_Licence_Declaration" to headsofgroup,cos;
GRANT SELECT ON hotwire."10_View/Research_Groups/Software_Licence_Declaration" to cos;

CREATE FUNCTION group_software_licence_decl_trig() RETURNS TRIGGER AS
$$
    BEGIN
        IF (NEW.all_software_used_by_this_group_is_correctly_licenced = 't') 
            AND
           ((current_date <> OLD.ro_date_of_last_declaration) OR OLD.ro_date_of_last_declaration IS NULL)
        THEN
            INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( NEW.id, current_date, current_user );
        END IF;
        RETURN NEW;
    END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER hw_rg_software_licence_decl_trigger INSTEAD OF UPDATE ON hotwire."10_View/Research_Groups/Software_Licence_Declaration" FOR EACH ROW EXECUTE PROCEDURE group_software_licence_decl_trig();
