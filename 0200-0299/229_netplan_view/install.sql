CREATE VIEW apps.virtual_machine_netplan AS
SELECT
    ip.hostname,
    set_masklen(ip.ip,masklen(subnet.network_address)) as ip,
    subnet.router
FROM ip_address ip
JOIN subnet ON ip.subnet_id = subnet.id
WHERE ip.hostname IS NOT NULL AND ip.hostname <> '';

ALTER VIEW apps.virtual_machine_netplan OWNER TO dev;
GRANT SELECT ON apps.virtual_machine_netplan TO osbuilder;
