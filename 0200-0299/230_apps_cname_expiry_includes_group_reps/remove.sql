DROP VIEW apps.cname_expiry;

CREATE OR REPLACE VIEW apps.cname_expiry AS 
 SELECT cname.fromname, cname.expiry_date, rg.name AS research_group, rg.active_directory_container
   FROM dns_cnames cname
   LEFT JOIN research_group rg ON cname.research_group_id = rg.id;

ALTER TABLE apps.cname_expiry OWNER TO dev;
GRANT ALL ON TABLE apps.cname_expiry TO dev;
GRANT SELECT ON TABLE apps.cname_expiry TO cos;
GRANT SELECT ON TABLE apps.cname_expiry TO ad_accounts;

