﻿CREATE OR REPLACE FUNCTION audit_generic()
  RETURNS trigger AS
$BODY$

    my $sql = <<EOSQL;

      insert into _audit ( operation, stamp, username, tablename, oldcols, newcols, id ) values ( 
        '$_TD->{event}',
                now(),
        current_user,
        '$_TD->{relname}',

EOSQL

    $old="";
    $new="";
    $quotestring='$audit$'; # safe string to quote with
    $newline ="\n"; # this insanity is needed because you can't pass posgres '\n' inside 
                    # a dollar-quoted string, so we use Perl's escapes (?) to prduce a 
                    # literal newline.

    # fill in oldcols
    if ($_TD->{event} eq 'INSERT')  
        {
          $sql .= "NULL, ";
        }
    else
        {
            while(($col, $val)=each(%{$_TD->{old}})){
                if ($val ne $_TD->{new}{$col}) {
                    $old .= $col . ':' . $val . $newline ;
                }
            }
        $sql .= $quotestring . $old . $quotestring . ', ';
    }

    # fill in newcols
    if ($_TD->{event} eq 'DELETE')  
        {
          $sql .= "NULL, ";
        }
        else
        {
            while(($col, $val)=each(%{$_TD->{new}})){
                if ($val ne $_TD->{old}{$col}) {
                    $new .=  $col . ':' . $val . $newline;
                }
            }
            $sql .= $quotestring . $new . $quotestring . ', ';
    }


    
    # fill in primary key

    my $long_pk = $_TD->{table_name} . '_id';
    my $pk_col;
    # look for a primary key field in either of our standard forms
    # Have to do it like this because when we're updating views the user may not have
    # sufficient rights to look at the information schema on the underlying table. 
    foreach (keys(%{$_TD->{new}}), keys(%{$_TD->{old}})) {
        if ($_ eq $long_pk ) {
            $pk_col = $long_pk;
	} elsif ($_ eq 'id')  {
		$pk_col='id';
	}

    }    
    if (defined $pk_col )  {

        if ($_TD->{event} eq 'DELETE') {
        
            $sql .= $quotestring . $_TD->{old}{$pk_col} . $quotestring . " )";
        } else {
            $sql .= $quotestring . $_TD->{new}{$pk_col} . $quotestring . " )";
        }
    } else {
        $sql .= $quotestring . '0' . $quotestring . ")";
    }

    my $rv = spi_exec_query($sql);

    unless ($rv->{status} eq 'SPI_OK_INSERT')
    {
        elog(NOTICE,"trigger status: $rv->{status}");
        return 'SKIP'
    }


    return undef; 
    
$BODY$
  LANGUAGE plperl VOLATILE
  COST 100;
ALTER FUNCTION audit_generic()
  OWNER TO cen1001;
