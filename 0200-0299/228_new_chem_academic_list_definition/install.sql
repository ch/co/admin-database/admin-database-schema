CREATE VIEW _chem_academic_mailinglist_2018 AS
    SELECT
        person.email_address,
        person.crsid,
        person_hid.person_hid
    FROM person
    LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
    LEFT JOIN _physical_status_v2 ON _physical_status_v2.person_id = person.id
    JOIN person_hid ON person.id = person_hid.person_id
    WHERE
            person.email_address IS NOT NULL
        AND
        (
                _latest_role_v12.post_category::text = 'Academic staff'::text
            OR
                _latest_role_v12.post_category::text = 'Teaching Fellow'
            OR
                person.counts_as_academic = true
        ) 
        AND 
            _physical_status_v2.status_id::text = 'Current'::text 
        AND NOT 
            (
                person.id IN 
                    ( 
                        SELECT mm_mailinglist_exclude_person.exclude_person_id
                        FROM mm_mailinglist_exclude_person
                        WHERE mm_mailinglist_exclude_person.mailinglist_id = ( SELECT mailinglist.id FROM mailinglist WHERE mailinglist.name::text = 'chem-academic-2018'::text)
                    )
            )     

UNION

    SELECT 
        person.email_address, 
        person.crsid, 
        NULL::character varying(80) AS person_hid
    FROM mm_mailinglist_include_person
    LEFT JOIN person ON mm_mailinglist_include_person.include_person_id = person.id
    LEFT JOIN _physical_status_v2 ON _physical_status_v2.person_id = person.id
    WHERE
            _physical_status_v2.status_id::text = 'Current'::text
        AND
            person.email_address IS NOT NULL
        AND
            mm_mailinglist_include_person.mailinglist_id = ( SELECT mailinglist.id FROM mailinglist WHERE mailinglist.name::text = 'chem-academic-2018'::text)

;

ALTER VIEW _chem_academic_mailinglist_2018 OWNER TO dev;
GRANT SELECT ON _chem_academic_mailinglist_2018 TO mailinglists, leavers_trigger, www_sites;
