drop view apps.wpkg_host_profiles;
DROP VIEW apps.wpkg_hosts;

CREATE OR REPLACE VIEW apps.wpkg_hosts AS 
 SELECT system_image_id, ip_address.hostname, ip_address.short_hostname, fai_class_hid.fai_class_hid AS extra_profiles
   FROM ip_address
   JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
   LEFT JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
   LEFT JOIN research_group ON system_image.research_group_id = research_group.id
   LEFT JOIN fai_class_hid ON research_group.fai_class_id = fai_class_hid.fai_class_id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN os_class_hid ON os_class_hid.os_class_id = operating_system.os_class_id
  WHERE COALESCE(os_class_hid.os_class_hid::text, ''::text) = 'Windows'::text;

ALTER TABLE apps.wpkg_hosts
  OWNER TO dev;
GRANT SELECT ON TABLE apps.wpkg_hosts TO wpkguser;

COMMENT ON VIEW apps.wpkg_hosts is 'Used by hosts_xml.php on wpkg-xml';

create or replace view apps.wpkg_host_profiles as 
 SELECT short_hostname, array_to_string(array_agg(software_package.wpkg_package_name), ','::text) AS packages
 from apps.wpkg_hosts
    LEFT JOIN mm_system_image_software_package_jumpstart USING (system_image_id)
 LEFT JOIN software_package ON mm_system_image_software_package_jumpstart.software_package_id = software_package.id	  
   GROUP BY short_hostname;

ALTER TABLE apps.wpkg_host_profiles
  OWNER TO dev;
GRANT SELECT ON TABLE apps.wpkg_host_profiles TO wpkguser;

COMMENT ON VIEW apps.wpkg_host_profiles is 'Used by profiles_xml.php on wpkg-xml';
