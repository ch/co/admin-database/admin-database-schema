drop view apps.wpkg_host_profiles;
DROP VIEW apps.wpkg_hosts;

CREATE OR REPLACE VIEW apps.wpkg_hosts AS 
SELECT ip_address.hostname, ip_address.short_hostname, fai_class_hid as extra_profiles
   FROM ip_address
   JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
   LEFT JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
   LEFT JOIN research_group ON system_image.research_group_id = research_group.id
   LEFT JOIN fai_class_hid ON research_group.fai_class_id = fai_class_hid.fai_class_id
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN os_class_hid ON os_class_hid.os_class_id = operating_system.os_class_id
  WHERE COALESCE(os_class_hid.os_class_hid::text, ''::text) = 'Windows'::text;

ALTER TABLE apps.wpkg_hosts
  OWNER TO dev;
GRANT SELECT ON TABLE apps.wpkg_hosts TO wpkguser;

COMMENT ON VIEW apps.wpkg_hosts is 'Used by hosts_xml.php on wpkg-xml';



create or replace view apps.wpkg_host_profiles as 
select ip_address.short_hostname, array_to_string(array_agg(wpkg_package_name),',') as packages from mm_system_image_ip_address 
inner join mm_system_image_software_package_jumpstart using (system_image_id) 
inner join software_package on software_package_id=software_package.id
inner join ip_address on ip_address.id=ip_address_id
inner join apps.wpkg_hosts on wpkg_hosts.short_hostname=ip_address.short_hostname
group by ip_address.short_hostname;

ALTER TABLE apps.wpkg_host_profiles
  OWNER TO dev;
GRANT SELECT ON TABLE apps.wpkg_host_profiles TO wpkguser;

COMMENT ON VIEW apps.wpkg_host_profiles is 'Used by profiles_xml.php on wpkg-xml';
