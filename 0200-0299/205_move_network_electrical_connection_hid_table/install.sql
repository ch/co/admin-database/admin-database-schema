ALTER TABLE hotwire.network_electrical_connection_hid SET SCHEMA public;
ALTER TABLE public.network_electrical_connection_hid OWNER TO dev;
CREATE VIEW hotwire.network_electrical_connection_hid AS
SELECT
	network_electrical_connection_id,
	network_electrical_connection_hid
FROM public.network_electrical_connection_hid;
ALTER VIEW hotwire.network_electrical_connection_hid OWNER TO dev;
GRANT SELECT ON hotwire.network_electrical_connection_hid TO ro_hid;
