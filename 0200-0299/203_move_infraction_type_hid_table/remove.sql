DROP VIEW hotwire.infraction_type_hid;
ALTER TABLE public.infraction_type_hid SET SCHEMA hotwire;
ALTER TABLE hotwire.infraction_type_hid OWNER TO postgres;
GRANT ALL ON TABLE hotwire.infraction_type_hid TO dev;
