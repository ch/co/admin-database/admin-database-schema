ALTER TABLE hotwire.infraction_type_hid SET SCHEMA public;
ALTER TABLE public.infraction_type_hid OWNER TO dev;
CREATE VIEW hotwire.infraction_type_hid AS
SELECT
	infraction_type_id,
	infraction_type_hid
FROM public.infraction_type_hid;
ALTER VIEW hotwire.infraction_type_hid OWNER TO dev;
GRANT SELECT ON hotwire.infraction_type_hid TO ro_hid;
