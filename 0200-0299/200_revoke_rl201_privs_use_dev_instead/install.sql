REVOKE SELECT ON TABLE hotwire._view_data_new FROM rl201;
REVOKE SELECT ON TABLE hotwire._view_data2 FROM rl201;
REVOKE SELECT ON TABLE hotwire._view_data FROM rl201;
REVOKE ALL ON TABLE hotwire."90_Action/Hotwire/User Preferences" FROM rl201;
REVOKE ALL ON TABLE hotwire."10_View/Hotwire_Test" FROM rl201;
REVOKE ALL ON TABLE hotwire."10_View/Easy_Add_Virtual_Machine" FROM rl201;
REVOKE EXECUTE ON FUNCTION fn_very_easy_add_switch(character varying,macaddr,character varying,integer) FROM rl201;
