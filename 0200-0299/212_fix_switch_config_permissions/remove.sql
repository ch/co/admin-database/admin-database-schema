ALTER TABLE switch_config_goal
  OWNER TO postgres;
GRANT ALL ON TABLE switch_config_goal TO postgres;
revoke all on table switch_config_goal from cos;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE switch_config_goal TO cos;

ALTER TABLE mm_switch_goal_applies_to_switch
  OWNER TO postgres;
GRANT ALL ON TABLE mm_switch_goal_applies_to_switch TO postgres;
GRANT ALL ON TABLE mm_switch_goal_applies_to_switch TO cos;
GRANT ALL ON TABLE mm_switch_goal_applies_to_switch TO osbuilder;
