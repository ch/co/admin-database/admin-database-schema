ALTER TABLE hotwire.fibre_type_hid SET SCHEMA public;
ALTER TABLE public.fibre_type_hid OWNER TO dev;
CREATE VIEW hotwire.fibre_type_hid AS
SELECT
	fibre_type_id,
	fibre_type_hid,
	fibre_type_abbrev
FROM public.fibre_type_hid;
ALTER VIEW hotwire.fibre_type_hid OWNER TO dev;
GRANT SELECT ON hotwire.fibre_type_hid TO ro_hid;
