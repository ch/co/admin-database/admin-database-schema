ALTER TABLE hotwire.website_staff_category_hid SET SCHEMA public;
CREATE VIEW hotwire.website_staff_category_hid AS
SELECT
	website_staff_category_id,
	website_staff_category_hid
FROM public.website_staff_category_hid;
ALTER VIEW hotwire.website_staff_category_hid OWNER TO dev;
GRANT SELECT ON hotwire.website_staff_category_hid TO ro_hid;
GRANT SELECT ON hotwire.website_staff_category_hid TO PUBLIC;
