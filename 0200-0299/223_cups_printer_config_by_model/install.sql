ALTER TABLE ppds_by_os ADD COLUMN model_uri varchar;
ALTER TABLE printer_class ADD COLUMN model_uri varchar;

CREATE VIEW apps.closest_printer AS
SELECT 
        ip_address.ip AS computer_ip,
	ip_address.hostname AS computer_hostname,
	room.name AS room,
	possible_printer.name AS printer,
	possible_printer.manufacturer,
	possible_printer.model,
	printer_ip.ip AS printer_ip,
	printer_ip.hostname AS printer_hostname,
	COALESCE(ppds_by_os.ppd, printer_class.ppd_name) AS ppd_name,
        COALESCE(ppds_by_os.model_uri, printer_class.model_uri) as model_uri,
	printer.cups_options
FROM ip_address
JOIN mm_system_image_ip_address ON ip_address.id = mm_system_image_ip_address.ip_address_id
JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
LEFT JOIN ppds_by_os ON ppds_by_os.operating_system_id = system_image.operating_system_id
JOIN hardware computer ON system_image.hardware_id = computer.id
JOIN hardware possible_printer ON possible_printer.room_id = computer.room_id
JOIN hardware_type ON possible_printer.hardware_type_id = hardware_type.id
JOIN printer ON possible_printer.id = printer.hardware_id
JOIN printer_class ON printer.printer_class_id = printer_class.id
JOIN system_image printer_image ON possible_printer.id = printer_image.hardware_id
JOIN mm_system_image_ip_address mm_printer_ip ON printer_image.id = mm_printer_ip.system_image_id
JOIN ip_address printer_ip ON mm_printer_ip.ip_address_id = printer_ip.id
JOIN room ON computer.room_id = room.id
WHERE hardware_type.name::text = 'Network printer'::text AND room.name::text <> 'None'::text;

ALTER VIEW apps.closest_printer OWNER TO dev;
GRANT SELECT ON apps.closest_printer to cos,osbuilder;
