alter table mm_software_package_can_install_on_os drop constraint mm_sw_pkg_fk_pkg;
alter table mm_software_package_can_install_on_os add constraint mm_sw_pkg_fk_pkg foreign key (software_package_id) references software_package(id) on delete cascade;
