create or replace view www.intranet_staff_list as 
select person.image_oid, person.crsid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name,
        CASE
            WHEN _latest_role_v12.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title
from person
LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
LEFT JOIN _latest_role_v12 on _latest_role_v12.person_id = person.id
LEFT JOIN post_history ON post_history.id = _latest_role_v12.role_id
WHERE _latest_role_v12.status = 'Current' 
AND _latest_role_v12.post_category_id in ('sc-3', 'sc-2');

ALTER TABLE www.intranet_staff_list
  OWNER TO dev;
GRANT ALL ON TABLE www.intranet_staff_list TO dev;
GRANT SELECT ON TABLE www.intranet_staff_list TO www_intranet;
GRANT ALL ON TABLE www.intranet_staff_list TO postgres;

COMMENT ON VIEW www.intranet_staff_list IS 'This view deliberately does not respect the do_not_show_on_website field. In turn, only www_intranet has SELECT permissions, and this user is specifically used on the intranet in tandem with the chemistry_intranet_staff module to produce a View that has Dept-only access controls in place.'
