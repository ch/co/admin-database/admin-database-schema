CREATE OR REPLACE VIEW hotwire_test_view AS 
 SELECT 1::bigint AS id, 1::bigint AS "BigInt", 0::bit(1) AS "Bit", false AS "Boolean", 'c'::character(1) AS "Character", 'hello'::character varying(25) AS "Varchar(25)", now()::date AS "Date", now()::time without time zone AS "Time", '127.0.0.1/24'::inet AS "IP Address", '00:01:02:03:04:05'::macaddr AS "MAC Address", 1745795::oid AS image_oid;

ALTER TABLE hotwire_test_view
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire_test_view TO postgres;
GRANT ALL ON TABLE hotwire_test_view TO rl201;
