CREATE TABLE group_software_licence_declarations (
    id bigserial primary key,
    research_group_id bigint not null,
    declaration_date date not null,
    username_of_signer varchar not null
);
--
-- I have chosen to use a varchar for the 'signature' rather than a foreign key on person
-- in case we have to start deleting people from the database for GDPR. If we want to join
-- the column against person.crsid then we can deal with failed matches ourselves
--
ALTER TABLE group_software_licence_declarations ADD CONSTRAINT software_decl_fk_research_group FOREIGN KEY (research_group_id) REFERENCES research_group (id);

ALTER TABLE group_software_licence_declarations OWNER TO dev;
-- These permissions needed to make the update trigger on the hotwire view work
GRANT INSERT ON group_software_licence_declarations TO headsofgroup;
GRANT USAGE ON group_software_licence_declarations_id_seq TO headsofgroup;
