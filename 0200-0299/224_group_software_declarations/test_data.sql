INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( (select id from research_group where name = 'Wales'), '2018-06-06', 'dw34');
INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( (select id from research_group where name = 'Goodman'), '2016-01-06', 'jmg11' );
INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( (select id from research_group where name = 'Goodman'), '2017-01-10', 'jmg11' );
INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( (select id from research_group where name = 'CIL'), '2017-01-10', 'jmg11');
INSERT INTO group_software_licence_declarations ( research_group_id, declaration_date, username_of_signer ) VALUES ( (select id from research_group where name = 'Frenkel'), '2017-02-28', 'df246');
