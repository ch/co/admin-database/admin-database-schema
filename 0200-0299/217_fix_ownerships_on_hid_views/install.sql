ALTER VIEW hotwire.head_of_group_hid OWNER TO dev;
ALTER VIEW hotwire.computer_rep_hid OWNER TO dev;
GRANT ALL ON hotwire.head_of_group_hid TO postgres;
GRANT ALL ON hotwire.computer_rep_hid TO postgres;
