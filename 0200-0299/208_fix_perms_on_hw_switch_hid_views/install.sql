ALTER VIEW hotwire.switch_auth_hid OWNER TO dev;
GRANT SELECT ON hotwire.switch_auth_hid TO ro_hid;
ALTER VIEW hotwire.speed_hid OWNER TO dev;
GRANT SELECT ON hotwire.speed_hid TO ro_hid;
ALTER TABLE switch_port_config_goal OWNER TO dev;
GRANT ALL ON switch_port_config_goal TO cos;
GRANT SELECT ON switch_port_config_goal TO osbuilder;
