ALTER TABLE mm_system_image_installer_tag DROP CONSTRAINT mm_system_image_installer_tag_fk_installer_tag_id;

ALTER TABLE mm_system_image_installer_tag
  ADD CONSTRAINT mm_system_image_installer_tag_fk_installer_tag_id FOREIGN KEY (installer_tag_id)
      REFERENCES installer_tag_hid (installer_tag_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

