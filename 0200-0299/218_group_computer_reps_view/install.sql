GRANT SELECT ON hotwire.head_of_group_hid,hotwire.computer_rep_hid to groupitreps,headsofgroup;

-- This should be a with (secueity_barrier) view but not supported in 9.1
CREATE VIEW hotwire."10_View/Research_Groups/Computer_reps"  AS
SELECT
    rg.id,
    rg.name as group_name,
    rg.head_of_group_id,
    ARRAY(
        SELECT computer_rep_id
        from mm_research_group_computer_rep mm
        where mm.research_group_id = rg.id
    ) as computer_rep_id
FROM research_group rg
JOIN person head_of_group on rg.head_of_group_id = head_of_group.id
LEFT JOIN person deputy_head_of_group on rg.deputy_head_of_group_id = deputy_head_of_group.id
LEFT JOIN person administrator on rg.administrator_id = administrator.id
WHERE 
    head_of_group.crsid = current_user 
    OR
    current_user = ANY (
            ARRAY(
                SELECT rep.crsid
                from mm_research_group_computer_rep mm
                join person rep on mm.computer_rep_id = rep.id
                where mm.research_group_id = rg.id
                )
            )
;

ALTER VIEW hotwire."10_View/Research_Groups/Computer_reps" OWNER TO dev;
-- The view will only contain rows that people are allowed to see by virtue of their
-- being a computer rep or head of group. 
GRANT SELECT,UPDATE ON hotwire."10_View/Research_Groups/Computer_reps" TO groupitreps,headsofgroup;

CREATE RULE hw_rg_comp_reps_upd AS ON UPDATE TO hotwire."10_View/Research_Groups/Computer_reps" DO INSTEAD
(
SELECT fn_mm_array_update(NEW.computer_rep_id,'mm_research_group_computer_rep','research_group_id','computer_rep_id',NEW.id);
UPDATE research_group SET
        name = NEW.group_name,
        head_of_group_id = NEW.head_of_group_id
    WHERE id = OLD.id;
);
