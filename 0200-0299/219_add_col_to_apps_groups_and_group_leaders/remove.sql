DROP VIEW apps.groups_and_group_leaders;
CREATE OR REPLACE VIEW apps.groups_and_group_leaders AS 
 SELECT person.crsid, person_hid.person_id, person_hid.person_hid, research_group.id AS research_group_id, research_group.name AS "group"
   FROM research_group
   JOIN person ON research_group.head_of_group_id = person.id
   JOIN person_hid ON person_hid.person_id = person.id;

ALTER TABLE apps.groups_and_group_leaders OWNER TO cen1001;
GRANT ALL ON TABLE apps.groups_and_group_leaders TO cen1001;
GRANT SELECT ON TABLE apps.groups_and_group_leaders TO space_report;
