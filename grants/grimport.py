#!/usr/bin/python3
import csv
import chemaccmgmt
import os
import itertools
import functools
import psycopg2
from psycopg2 import sql
import locale

awards_csv = "awards.csv"
applications_csv = "applications.csv"

encoding = "utf-8"
os.environ["PGPASSFILE"] = "/scratch/cen1001/.dbdev"


def truncate_tables(db):
    # truncate existing tables
    db.cursor.execute(
        "truncate grants.award, grants.application, grants.research_area, grants.funder, grants.funder_type, grants.holder"
    )
    db.cursor.execute(
        """DELETE FROM grants.application_status WHERE status <> 'Pending' and status <> 'Unknown' """
    )
    # reset sequence for application status and ensure 'Pending' is present and has id 1
    db.conn.commit()


def import_application_status(db, applications_csv):
    with open(applications_csv, "r", newline="", encoding=encoding) as applications:
        applications_data = csv.DictReader(applications)
        for row in applications_data:
            if row["Status"]:
                status = row["Status"].strip()
                db.cursor.execute(
                    "INSERT INTO grants.application_status ( status ) VALUES ( %s ) ON CONFLICT (status) DO NOTHING",
                    [status],
                )
        db.conn.commit()


def import_research_areas(db, awards_csv):
    # make list of research_area s
    with open(awards_csv, "r", newline="", encoding=encoding) as awards:
        awards_data = csv.DictReader(awards)
        for row in awards_data:
            research_area = row["Research Area"].strip()
            research_code = row["Research Code"].strip()
            if research_code or research_area:
                db.cursor.execute(
                    "INSERT INTO grants.research_area ( research_area, research_code ) VALUES ( %s, %s ) ON CONFLICT DO NOTHING",
                    [research_area, research_code],
                )
        db.conn.commit()


def import_sponsors(db, awards_csv, applications_csv):
    # make list of sponsor types and enter data
    with open(awards_csv, "r", newline="", encoding=encoding) as awards:
        awards_data = csv.DictReader(awards)
        for row in awards_data:
            sponsor_type = row["Sponsor Type"].strip()
            if sponsor_type:
                db.cursor.execute(
                    "INSERT INTO grants.funder_type ( funder_type ) VALUES ( %s ) ON CONFLICT DO NOTHING",
                    [sponsor_type],
                )
        db.conn.commit()

    # make list of sponsors and enter data
    with open(awards_csv, "r", newline="", encoding=encoding) as awards, open(
        applications_csv, "r", newline="", encoding=encoding
    ) as applications:
        awards_data = csv.DictReader(awards)
        applications_data = csv.DictReader(applications)
        # awards have sponsor types, applications don't so do awards first
        for row in itertools.chain(awards_data, applications_data):
            sponsor = row.get("Sponsor", "").strip()
            sponsor_type = row.get("Sponsor Type", "").strip()
            code = row.get("Sponsor Code", "").strip()
            if sponsor_type and sponsor and code:
                db.cursor.execute(
                    "INSERT INTO grants.funder ( funder, funder_type_id, code ) VALUES (%s, (SELECT id FROM grants.funder_type WHERE funder_type = %s), %s) ON CONFLICT DO NOTHING",
                    [sponsor, sponsor_type, code],
                )
            elif (code and not sponsor_type) or (sponsor_type and not code):
                print(
                    f"problem here: sponsor: {sponsor} code: {code} type: {sponsor_type}"
                )
            elif sponsor:
                db.cursor.execute(
                    "INSERT INTO grants.funder ( funder ) VALUES ( %s ) ON CONFLICT (funder) DO NOTHING",
                    [sponsor],
                )
        db.conn.commit()


def find_pi(db, pi):
    person_id = None
    query = """
SELECT DISTINCT person.id FROM person
JOIN post_history ON person.id = post_history.person_id
WHERE post_history.staff_category_id IN ( SELECT id FROM public.staff_category WHERE category <> 'Assistant staff' AND category <> 'Unknown' )
      AND person.surname ilike %s
    """
    db.cursor.execute(query, [pi])
    if db.cursor.rowcount == 1:
        person_id = db.cursor.fetchone()[0]
    return person_id


def import_pis(db, awards_csv, applications_csv):
    with open(awards_csv, "r", newline="", encoding=encoding) as awards, open(
        applications_csv, "r", newline="", encoding=encoding
    ) as applications:
        awards_data = csv.DictReader(awards)
        applications_data = csv.DictReader(applications)
        for row in itertools.chain(awards_data, applications_data):
            pi = row["Grant Holder"].strip()
            if pi:
                person_id = find_pi(db, pi)
                if person_id:
                    db.cursor.execute(
                        "INSERT INTO grants.holder ( holder, person_id ) VALUES ( %s, %s ) ON CONFLICT (holder) DO NOTHING",
                        [pi, person_id],
                    )
                else:
                    db.cursor.execute(
                        "INSERT INTO grants.holder ( holder ) VALUES ( %s ) ON CONFLICT (holder) DO NOTHING",
                        [pi],
                    )
        db.conn.commit()


def normalize_column_name(name):
    column = (
        " ".join(name.replace(".", " ").replace("-", "_").strip().split())
        .replace(" ", "_")
        .lower()
        .replace("_no", "_number")
        .replace(")", "")
        .replace("(", "")
    )
    return column


def import_applications(db, applications_csv):
    cols_to_import_directly = [
        "Project Title",
        "Start Date",
        "End Date",
        "Application Date",
        "Co-investigators",
        "Consumables",
        "Contingency",
        "Equipment",
        "Estates Costs",
        "Exceptional Items",
        "Facilities",
        "Grant Awarded",
        "Indirect Costs",
        "Infrastructure Tech",
        "Notes",
        "Overheads",
        "PI Costs",
        "Pooled Labour",
        "Staff",
        "Travel",
        "Univ.Contribution",
        "X5 No.",
    ]

    cols_to_import_and_rename = [
        ( "Account No", "project_number"),
        ( "RG No.", "award_number")
    ]

    numeric_columns = [
        "Consumables",
        "Contingency",
        "Equipment",
        "Estates Costs",
        "Exceptional Items",
        "Facilities",
        "Indirect Costs",
        "Infrastructure Tech",
        "Overheads",
        "PI Costs",
        "Pooled Labour",
        "Staff",
        "Travel",
        "Univ.Contribution",
        "Grant Sought",
    ]

    cols_to_calculate = ["Grant Sought"]

    with open(applications_csv, "r", newline="", encoding=encoding) as applications:
        applications_data = csv.DictReader(applications)
        for original_row in applications_data:
            row = {x: original_row[x].strip() for x in original_row}
            # We /should/ be doing this with locale, but the input data is
            # so inconsistent that the best I can do is strip commas where
            # they exist
            for column in numeric_columns:
                if column in row:
                    row[column] = row[column].replace(",", "")
            if not functools.reduce(lambda x, y: x + y, row.values()):
                print(f"Not importing empty row: {row}")
                continue
            record = {}
            for column in cols_to_import_directly:
                if column in row:
                    if row[column]:
                        record[normalize_column_name(column)] = row[column]
            for column in cols_to_import_and_rename:
                if column[0] in row:
                    if row[column[0]]:
                        record[column[1]] = row[column[0]]
            for column in cols_to_calculate:
                if column in row:
                    if row[column]:
                        record["_" + normalize_column_name(column)] = row[column]
            # find sponsor and holder foreign keys
            if "Sponsor" in row:
                record["funder_id"] = find_fk(
                    db, row["Sponsor"], table="funder", column="funder"
                )
            if "Grant Holder" in row:
                record["grant_holder_id"] = find_fk(
                    db, row["Grant Holder"], table="holder", column="holder"
                )
            if "Status" in row:
                record["status_id"] = find_fk(
                    db, row["Status"], table="application_status", column="status"
                )
            # FIXME this shouldn't be nedded
            else:
                # If the record hasn't a status at all, set None and let the later code to handle null
                # status deal with it
                record["status_id"] = None

            # There are some null statuses in the data. Rather than allow nulls in this column we invent
            # an Unknown status, which allows us to import these but should improve the data quality
            # going forward because the column is not nullable, forcing the user to pick one.
            if not record["status_id"]:
                record["status_id"] = find_fk(
                    db, "Unknown", table="application_status", column="status"
                )
            keys_sorted = sorted(record.keys())
            query = sql.SQL("INSERT INTO grants.application ({}) VALUES ({})").format(
                sql.SQL(", ").join(map(sql.Identifier, keys_sorted)),
                sql.SQL(", ").join(sql.Placeholder() * len(keys_sorted)),
            )
            try:
                db.cursor.execute(query, [record[x] for x in keys_sorted])
            except psycopg2.errors.UniqueViolation:
                print(f"Cannot insert application with duplicate unique column")
                print(record)
                db.conn.rollback()
            except psycopg2.errors.InvalidTextRepresentation:
                print(f"Cannot insert application with invalid data")
                print(record)
                db.conn.rollback()
            except psycopg2.errors.NotNullViolation:
                print(f"Cannot insert row with null data in not-null column")
                print(record)
                db.conn.rollback()
            db.conn.commit()


def find_fk(db, foreign_key, table="grants.funder", column="funder"):
    # return None if not found
    id = None
    query = sql.SQL('SELECT id FROM "grants".{table} WHERE {column} = %s').format(
        table=sql.Identifier(table), column=sql.Identifier(column)
    )
    db.cursor.execute(query, [foreign_key])
    if db.cursor.rowcount == 1:
        id = db.cursor.fetchone()[0]
    return id


def import_awards(db, awards_csv):
    cols_to_import_directly = [
        "Project Title",
        "End Date",
        "Co-investigators",
        "Consumables",
        "Contingency",
        "Equipment",
        "Estates Costs",
        "Exceptional",
        "Facilities",
        "Indirect Costs",
        "Infrastructure Tech",
        "Notes",
        "Overheads",
        "PI Costs",
        "Pooled Labour",
        "Staff",
        "Start Date",
        "Travel",
        "Univ. Contribution",
    ]

    cols_to_import_and_rename = [
        ( "Account No.", "project_number" ),
        ( "RG No.", "award_number" ),
        ( "Sponsor Ref.", "funder_ref" ),
    ]

    numeric_columns = [
        "Consumables",
        "Contingency",
        "Equipment",
        "Estates Costs",
        "Exceptional",
        "Facilities",
        "Indirect Costs",
        "Infrastructure Tech",
        "Overheads",
        "PI Costs",
        "Pooled Labour",
        "Project Status",
        "Staff",
        "Travel",
        "Univ. Contribution",
    ]

    cols_to_calculate = [
        "Grant Value",
        "Duration (mths)",
        "Months Remaining",
        "Project Status",
        "Research Code",
        "Sponsor Code",
        "Sponsor Type",
        "Research Code",
    ]

    with open(awards_csv, "r", newline="", encoding=encoding) as applications:
        applications_data = csv.DictReader(applications)
        for original_row in applications_data:
            row = {x: original_row[x].strip() for x in original_row}
            # We /should/ be doing this with locale, but the input data is
            # so inconsistent that the best I can do is strip commas where
            # they exist
            for column in numeric_columns:
                if column in row:
                    row[column] = row[column].replace(",", "")
            if not functools.reduce(lambda x, y: x + y, row.values()):
                print(f"Not importing empty row: {row}")
                continue
            record = {}
            for column in cols_to_import_directly:
                if column in row:
                    if row[column]:
                        record[normalize_column_name(column)] = row[column]
            for column in cols_to_import_and_rename:
                if column[0] in row:
                    if row[column[0]]:
                        record[column[1]] = row[column[0]]
            for column in cols_to_calculate:
                if column in row:
                    if row[column]:
                        record["_" + normalize_column_name(column)] = row[column]
            # find sponsor and holder foreign keys
            if "Sponsor" in row:
                record["funder_id"] = find_fk(
                    db, row["Sponsor"], table="funder", column="funder"
                )
            if "Grant Holder" in row:
                record["grant_holder_id"] = find_fk(
                    db, row["Grant Holder"], table="holder", column="holder"
                )
            if "Research Area" in row:
                record["research_area_id"] = find_fk(
                    db,
                    row["Research Area"],
                    table="research_area",
                    column="research_area",
                )

            keys_sorted = sorted(record.keys())
            query = sql.SQL("INSERT INTO grants.award ({}) VALUES ({})").format(
                sql.SQL(", ").join(map(sql.Identifier, keys_sorted)),
                sql.SQL(", ").join(sql.Placeholder() * len(keys_sorted)),
            )
            try:
                db.cursor.execute(query, [record[x] for x in keys_sorted])
            except psycopg2.errors.UniqueViolation:
                print(f"Cannot insert award with duplicate unique column")
                print(record)
                db.conn.rollback()
            except psycopg2.errors.InvalidTextRepresentation:
                print(f"Cannot insert award with invalid data")
                print(record)
                db.conn.rollback()
            db.conn.commit()


if __name__ == "__main__":
    # connect to database
    db = chemaccmgmt.db.ChemDB(
        dsn="dbname=chemistry host=dbdev.ch.private.cam.ac.uk user=cen1001",
        use_pgpass=True,
    )

    truncate_tables(db)
    import_research_areas(db, awards_csv)
    import_sponsors(db, awards_csv, applications_csv)
    import_application_status(db, applications_csv)
    import_pis(db, awards_csv, applications_csv)
    import_applications(db, applications_csv)
    import_awards(db, awards_csv)

    db.conn.close()
