SET search_path = radius, pg_catalog;

CREATE OR REPLACE FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Places those authenticating via personal ChemNet tokens into the ChemNet VLAN

begin
 perform * from public.user_account where username=_crsid and not is_disabled and not owner_banned and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into ChemNet's VLAN (as if wireless)
  return query select * from radius.radcheck_vlan(_crsid||'@'||_realm,618);
end if;
end;
$$;

ALTER FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) OWNER TO postgres;

CREATE OR REPLACE VIEW radius._radius_machine_account AS
SELECT
    machine_account.name,
    CASE
        WHEN system_image.reinstall_on_next_boot = 't' THEN vlan.vid
        WHEN system_image.was_managed_machine_on > (current_date - interval '3 months') THEN vlan.vid
        WHEN system_image.is_managed_mac = 't' THEN vlan.vid
        WHEN system_image.auto_banned_from_network AND NOT system_image.override_auto_ban THEN 191
        ELSE vlan.vid
    END AS vid,
    machine_account.tagged_vlans
FROM public.machine_account 
JOIN public.system_image ON machine_account.system_image_id = system_image.id 
JOIN public.mm_system_image_ip_address USING (system_image_id) 
JOIN public.ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id 
JOIN public.subnet ON ip_address.subnet_id = subnet.id 
JOIN public.vlan ON subnet.vlan_id = vlan.id;


ALTER TABLE radius._radius_machine_account OWNER TO postgres;

CREATE OR REPLACE VIEW radius._radius_mac_address AS
SELECT
    CASE
        WHEN system_image.reinstall_on_next_boot = 't' THEN vlan.vid
        WHEN system_image.was_managed_machine_on > (current_date - interval '3 months') THEN vlan.vid
        WHEN system_image.is_managed_mac = 't' THEN vlan.vid
        WHEN system_image.auto_banned_from_network AND NOT system_image.override_auto_ban THEN 191
        ELSE vlan.vid
    END AS vid,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac
FROM public.system_image
JOIN public.mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = system_image.id
JOIN public.ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
JOIN public.subnet ON ip_address.subnet_id = subnet.id
JOIN public.vlan ON subnet.vlan_id = vlan.id
WHERE NOT subnet.ignore_vlan;


ALTER TABLE radius._radius_mac_address OWNER TO postgres;


CREATE OR REPLACE FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT user_account.id, user_account.username::text ||'@'|| _realm AS username, 
        'Cleartext-Password'::character varying AS attribute, user_account.chemnet_token AS value, 
        ':='::character varying AS op, user_account.username AS raw
        FROM public.user_account where username=_localpart and not is_disabled and not owner_banned and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;

ALTER FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;


CREATE OR REPLACE FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
begin
 -- TODO - machines may not log in to specific ports - server ports etc.
 return query select machine_account.id, machine_account.name || _realm as username, 
        'Cleartext-Password'::varchar as attribute, machine_account.chemnet_token as value,
        ':='::varchar as op, machine_account.name as raw
        from public.machine_account join public.system_image on machine_account.system_image_id = system_image.id where name=_localpart and (not system_image.auto_banned_from_network or system_image.override_auto_ban or system_image.reinstall_on_next_boot or (system_image.was_managed_machine_on > current_date - interval '3 months') or system_image.is_managed_mac);
end;
$$;


ALTER FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

SET search_path = "$user",public;
