-- view _radius_mac_address used by radpasswd_mac() and radcheck_mac()

CREATE OR REPLACE VIEW radius._radius_mac_address AS
SELECT
    CASE
        WHEN system_image.reinstall_on_next_boot = 't' THEN vlan.vid
        WHEN operating_system.has_security_support = 'f' THEN 1106 -- legacy-os VLAN
        WHEN system_image.was_managed_machine_on > (current_date - interval '3 months') THEN vlan.vid
        WHEN system_image.is_managed_mac = 't' THEN vlan.vid
        WHEN system_image.auto_banned_from_network AND NOT system_image.override_auto_ban THEN 191
        ELSE vlan.vid
    END AS vid,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac
FROM public.system_image
JOIN public.operating_system ON system_image.operating_system_id = operating_system.id
JOIN public.mm_system_image_ip_address ON mm_system_image_ip_address.system_image_id = system_image.id
JOIN public.ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
JOIN public.subnet ON ip_address.subnet_id = subnet.id
JOIN public.vlan ON subnet.vlan_id = vlan.id
WHERE NOT subnet.ignore_vlan;

ALTER TABLE radius._radius_mac_address OWNER TO postgres;

-- view radius._radius_machine_account used by radcheck_machine()

CREATE OR REPLACE VIEW radius._radius_machine_account AS
SELECT
    machine_account.name,
    CASE
        WHEN system_image.reinstall_on_next_boot = 't' THEN vlan.vid
        WHEN operating_system.has_security_support = 'f' THEN 1106 -- legacy-os VLAN
        WHEN system_image.was_managed_machine_on > (current_date - interval '3 months') THEN vlan.vid
        WHEN system_image.is_managed_mac = 't' THEN vlan.vid
        WHEN system_image.auto_banned_from_network AND NOT system_image.override_auto_ban THEN 191
        ELSE vlan.vid
    END AS vid,
    machine_account.tagged_vlans
FROM public.machine_account
JOIN public.system_image ON machine_account.system_image_id = system_image.id
JOIN public.operating_system ON system_image.operating_system_id = operating_system.id
JOIN public.mm_system_image_ip_address USING (system_image_id)
JOIN public.ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
JOIN public.subnet ON ip_address.subnet_id = subnet.id
JOIN public.vlan ON subnet.vlan_id = vlan.id;

ALTER TABLE radius._radius_machine_account OWNER TO postgres;

