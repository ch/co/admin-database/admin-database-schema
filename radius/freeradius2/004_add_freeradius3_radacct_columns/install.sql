ALTER TABLE radius.radacct ADD COLUMN "acctupdatetime" timestamp with time zone;
ALTER TABLE radius.radacct ADD COLUMN "framedipv6address" inet;
ALTER TABLE radius.radacct ADD COLUMN "framedipv6prefix" inet;
ALTER TABLE radius.radacct ADD COLUMN "framedinterfaceid" text;
ALTER TABLE radius.radacct ADD COLUMN "delegatedipv6prefix" inet;
ALTER TABLE radius.radacct ADD COLUMN "acctinterval" bigint;
