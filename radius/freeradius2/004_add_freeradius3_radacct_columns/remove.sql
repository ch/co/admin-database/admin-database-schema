ALTER TABLE radius.radacct DROP COLUMN "acctupdatetime";
ALTER TABLE radius.radacct DROP COLUMN "framedipv6address";
ALTER TABLE radius.radacct DROP COLUMN "framedipv6prefix";
ALTER TABLE radius.radacct DROP COLUMN "framedinterfaceid";
ALTER TABLE radius.radacct DROP COLUMN "delegatedipv6prefix";
ALTER TABLE radius.radacct DROP COLUMN "acctinterval";
