--
-- RADIUS database schema
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: radius; Type: SCHEMA; Schema: -; Owner: radius
--

CREATE SCHEMA radius;


ALTER SCHEMA radius OWNER TO radius;

SET search_path = radius, pg_catalog;

--
-- Name: dummy_radreply; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW dummy_radreply AS
    SELECT NULL::bigint AS id, NULL::character varying AS username, NULL::character varying AS attribute, NULL::character varying AS value, NULL::character varying AS op;


ALTER TABLE radius.dummy_radreply OWNER TO postgres;

--
-- Name: radcheck_mac(character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

-- FIXME
CREATE FUNCTION radcheck_mac(_mac character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _vlanid integer;
 -- Performs the authentication bit for local users or mac-based users
begin
 -- Use vlan determined by shadow_mac_to_vlan first
 -- FIXME this might cause problems for our banning attempts
 -- need to review the content of shadow_mac_to_vlan
select vid from shadow_mac_to_vlan where mac=_mac::macaddr into _vlanid;
if found then
 return query select * from radcheck_vlan(_mac,_vlanid);
else
 select vid from _radius_mac_address where _mac::macaddr in (wired_mac_1,wired_mac_2,wired_mac_3,wired_mac_4,wireless_mac) into _vlanid;
 if found then 
  return query select * from radcheck_vlan(_mac,_vlanid);
 else
  return query select * from radcheck_vlan(_mac,192);
 end if;
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_mac(_mac character varying) OWNER TO postgres;

--
-- Name: radcheck_machine(character varying, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheck_machine(_machine character varying, _realm character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
declare
 _vlanid integer;
 _vlans integer[];
begin
select vid,tagged_vlans into _vlanid,_vlans from _radius_machine_account where name=_machine;
if found then
   return query select * from radcheck_vlans(_machine||'@'||_realm,_vlanid, _vlans);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_machine(_machine character varying, _realm character varying) OWNER TO postgres;

--
-- Name: radcheck_personal(character varying, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

-- FIXME
CREATE FUNCTION radcheck_personal(_crsid character varying, _realm character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 perform * from user_account where username=_crsid and not is_disabled and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into ChemNet's VLAN (as if wireless)
  return query select * from radcheck_vlan(_crsid||'@'||_realm,618);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) OWNER TO postgres;

CREATE FUNCTION radcheck_non_chemnet(_crsid character varying, _realm character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 perform * from non_chemnet_user_account where username=_crsid and realm=_realm and not is_disabled and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into non-acad VLAN
  return query select * from radcheck_vlan(_crsid||'@'||_realm,635);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_non_chemnet(_crsid character varying, _realm character varying) OWNER TO postgres;
--
-- Name: radcheck_phone(character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheck_phone(_localpart character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
begin

return query select * from radcheck_vlans(_localpart,1,'{2682}');
end;
$$;


ALTER FUNCTION radius.radcheck_phone(_localpart character varying) OWNER TO postgres;

--
-- Name: radcheck_ucsap(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheck_ucsap(_localpart character varying, _nasip inet, _nasport character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
-- TODO Check nasip,nasport is set to UCSAP - must be done in radius_password
return query select * from radius.radcheck_vlans(_localpart,3116,'{618, 635}');
end;
$$;


ALTER FUNCTION radius.radcheck_ucsap(_localpart character varying, _nasip inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radcheck_vlan(character varying, integer); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheck_vlan(_user character varying, _vlanid integer) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 return query select * from radcheck_vlans(_user,_vlanid,'{}');
end;
$$;


ALTER FUNCTION radius.radcheck_vlan(_user character varying, _vlanid integer) OWNER TO postgres;

--
-- Name: radcheck_vlans(character varying, integer, integer[]); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheck_vlans(_user character varying, _vlanid integer, _taggedvlanids integer[]) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
declare
 s radius.dummy_radreply;
 _vlan integer;
 _firstvlan boolean='t';
begin
select 1::integer as id, _user as username, 'Tunnel-Type'::varchar as attribute, '13'::varchar as "value", ':='::varchar as op into s; 
return next s;
s.attribute='Tunnel-Medium-Type';
s.value='6';
s.id=s.id+1;
return next s;
s.attribute='Tunnel-Private-Group-ID';
s.value=_vlanid::varchar;
s.id=s.id+1;
return next s; 
if _taggedvlanids is not null then
 foreach _vlan in ARRAY _taggedvlanids LOOP
  if (not _firstvlan) then
   s.op='+=';
  end if;
  s.id=s.id+1;
  s.attribute='Egress-VLANID';
  s.value=('x'||'31000'||to_hex(_vlan))::bit(32)::int::varchar;
  return next s;
  _firstvlan='f';
 end loop;
end if;
s.op=':=';
s.id=s.id+1;
s.attribute='Ingress-Filters';
s.value='1';
return next s;
end;
$$;


ALTER FUNCTION radius.radcheck_vlans(_user character varying, _vlanid integer, _taggedvlanids integer[]) OWNER TO postgres;

--
-- Name: radcheckv2(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radcheckv2(v_user_id character varying, _nas inet, _nasport character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $_$
declare 
 _realm varchar;
 _localpart varchar;
 -- Performs the authentication bit for local users or mac-based users
begin
_realm=split_part(v_user_id,'@',2);
_localpart=split_part(v_user_id,'@',1);
case _realm 
 when 'ch.cam.ac.uk', 'ch.2018.cam.ac.uk' then
  return query select * from radcheck_personal(_localpart,_realm);
 when 'chemnet','chemnet.ch.cam.ac.uk','chemnet.2018.ch.cam.ac.uk' then
  return query select * from radcheck_machine(_localpart,_realm);
 when 'nonacad.ch.cam.ac.uk' then
  return query select * from radcheck_non_chemnet(_localpart,_realm);
 when '' then
  -- Many options 
  case when _localpart='UCSAP8023' then
   return query select * from radcheck_ucsap(_localpart,_nas,_nasport);
  when _localpart ~ '^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$' then
   return query select * from radcheck_mac(_localpart);
  when _localpart ~ '^CP-[0-9]+G?-' then
   return query select * from radcheck_phone(_localpart);
  else
  end case;
  else
  -- Unknown realm which we don't handle here
end case;
end;
$_$;


ALTER FUNCTION radius.radcheckv2(v_user_id character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radcheck; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius.radcheck AS
    SELECT user_account.id, ((user_account.username)::text || '@ch.cam.ac.uk'::text) AS username, 'Cleartext-Password'::character varying AS attribute, user_account.chemnet_token AS value, ':='::character varying AS op, user_account.username AS raw FROM public.user_account UNION ALL SELECT shadow_mac_to_vlan.id, (shadow_mac_to_vlan.mac)::character varying AS username, 'Cleartext-Password'::character varying AS attribute, (shadow_mac_to_vlan.mac)::character varying AS value, ':='::character varying AS op, (shadow_mac_to_vlan.mac)::character varying AS raw FROM public.shadow_mac_to_vlan;


ALTER TABLE radius.radcheck OWNER TO postgres;

--
-- Name: radpasswd_mac(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

-- FIXME check what this does, do we need to prune it? 
CREATE FUNCTION radpasswd_mac(_mac character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 perform * from _radius_mac_address where _mac::macaddr in (wired_mac_1,wired_mac_2,wired_mac_3,wired_mac_4,wireless_mac);
 if found then
  -- This is a mac address we know about; permit the connection
  return query select 1::bigint, _mac::text as username,
         'Cleartext-Password'::varchar as attribute, _mac::varchar as value,
         ':='::varchar as op, _mac::varchar as raw;
 else
  -- Unknown MAC: permit the connection and log
  update additional_mac_addresses set last_seen=now() where mac=_mac::macaddr;
  if not found then
   insert into additional_mac_addresses (mac, first_seen, last_seen) values (_mac::macaddr,now(),now());
  end if;
  return query select 1::bigint, _mac::text as username,
         'Cleartext-Password'::varchar as attribute, _mac::varchar as value,
         ':='::varchar as op, _mac::varchar as raw;
 end if;
end;
$$;


ALTER FUNCTION radius.radpasswd_mac(_mac character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_machine(character varying, character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

-- FIXME
CREATE FUNCTION radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
begin
 -- TODO - machines may not log in to specific ports - server ports etc.
 return query select machine_account.id, machine_account.name || _realm as username, 
        'Cleartext-Password'::varchar as attribute, machine_account.chemnet_token as value,
        ':='::varchar as op, machine_account.name as raw
        from machine_account where name=_localpart;
end;
$$;


ALTER FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_personal(character varying, character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

-- FIXME
CREATE FUNCTION radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT user_account.id, user_account.username::text ||'@'|| _realm AS username, 
        'Cleartext-Password'::character varying AS attribute, user_account.chemnet_token AS value, 
        ':='::character varying AS op, user_account.username AS raw
        FROM user_account where username=_localpart and not is_disabled and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;


ALTER FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

CREATE FUNCTION radpasswd_non_chemnet(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT non_chemnet_user_account.id, non_chemnet_user_account.username::text ||'@'|| _realm AS username,
        'Cleartext-Password'::character varying AS attribute, non_chemnet_user_account.token AS value,
        ':='::character varying AS op, non_chemnet_user_account.username AS raw
        FROM non_chemnet_user_account where username=_localpart and realm=_realm and not is_disabled and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;


ALTER FUNCTION radius.radpasswd_non_chemnet(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;
--
-- Name: radpasswd_phone(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radpasswd_phone(_user character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
  -- TODO - phones should not appear on UCSAP ports etc
  if (position('CP-7937G-SEP' in _user)>0) then -- Conference phones cannot have a null password ; use digit '1' by convention
   return query select 0::bigint as id, _user::text as username, 'Cleartext-Password'::varchar as attribute,
    '1'::varchar as value, ':='::varchar as op, _user as raw;
  else 
   return query select 0::bigint as id, _user::text as username, 'Cleartext-Password'::varchar as attribute,
    ''::varchar as value, ':='::varchar as op, _user as raw ;
  end if;
end;
$$;


ALTER FUNCTION radius.radpasswd_phone(_user character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_ucsap(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radpasswd_ucsap(_localpart character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - UCSAPS may not log in to specific ports - server ports etc.
 _r.id=1;
 _r.username=_localpart;
 _r.attribute='Cleartext-Password';
 _r.value='27vtFNU6';
 _r.op=':=';
 _r.raw=_localpart;
 if _nas is not null and coalesce(_nasport,'') !=''  then
 perform * from switch_port_config_goal inner join switchport on switch_auth_id= switch_port_config_goal.id inner join switch on switch.id=switch_id inner join system_image using (hardware_id) inner join mm_system_image_ip_address on system_image_id=system_image.id inner join ip_address on ip_address.id=ip_address_id where switch_port_config_goal.id in (24,2) and ip=_nas and switchport.name=_nasport;
 if not found then
  RAISE WARNING 'Access point attempting to connect to port % of switch % which is not configured for it', _nasport, _nas;
  _r.value='LOGINDENIED';
 end if;
 end if;
 return next _r;
end;
$$;


ALTER FUNCTION radius.radpasswd_ucsap(_localpart character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswdv2(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radpasswdv2(v_user_id character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $_$
declare 
 _realm varchar;
 _localpart varchar;
 -- Performs the authentication bit for local users or mac-based users
begin
_realm=split_part(v_user_id,'@',2);
_localpart=split_part(v_user_id,'@',1);
 -- Check to see whether this port is reserved for UCS-AP?
 if _nas is not null and coalesce(_nasport,'') !=''  then
 perform * from switch_port_config_goal inner join switchport on switch_auth_id= switch_port_config_goal.id inner join switch on switch.id=switch_id inner join system_image using (hardware_id) inner join mm_system_image_ip_address on system_image_id=system_image.id inner join ip_address on ip_address.id=ip_address_id where switch_port_config_goal.id in (24,2) and ip=_nas and switchport.name=_nasport;
 if (_localpart !='UCSAP8023' and found)  then
  RAISE WARNING 'Attempt by % to connect to port % of switch % reserved for UCS-AP', v_user_id, _nasport, _nas;
  return;
 end if;
 end if;
 
case _realm 
 when 'ch.cam.ac.uk', 'ch.2018.cam.ac.uk' then
  return query select * from radpasswd_personal(_localpart,_realm,_nas,_nasport);
 when 'chemnet','chemnet.ch.cam.ac.uk','chemnet.2018.ch.cam.ac.uk' then
  return query select * from radpasswd_machine(_localpart,_realm,_nas,_nasport);
 when 'nonacad.ch.cam.ac.uk' then
  return query select * from radpasswd_non_chemnet(_localpart,_realm,_nas,_nasport);
 when '' then
  -- Many options 
  case when _localpart='UCSAP8023' then
   return query select * from radpasswd_ucsap(_localpart,_nas,_nasport);
  when _localpart ~ '^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$' then
   return query select * from radpasswd_mac(_localpart,_nas,_nasport);
  when _localpart ~ '^CP-[0-9]+G?-' then
   return query select * from radpasswd_phone(_localpart,_nas,_nasport);
  else
   -- Unknown Local Part
   RAISE WARNING 'Unknown Local Part in empty realm (%)', _localpart;
  end case;
  else
   RAISE WARNING 'Unknown Realm (%)', _realm;
  -- Unknown realm which we don't handle here
end case;
end;
$_$;


ALTER FUNCTION radius.radpasswdv2(v_user_id character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: _radius_mac_address; Type: VIEW; Schema: radius; Owner: postgres
--

-- FIXME exclude banned systems here
CREATE VIEW _radius_mac_address AS
    SELECT vlan.vid, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wired_mac_3, system_image.wired_mac_4, system_image.wireless_mac FROM ((((public.system_image JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = system_image.id))) JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) JOIN public.vlan ON ((subnet.vlan_id = vlan.id))) WHERE (NOT subnet.ignore_vlan);


ALTER TABLE radius._radius_mac_address OWNER TO postgres;

--
-- Name: _radius_machine_account; Type: VIEW; Schema: radius; Owner: postgres
--

-- FIXME exclude banned system images
CREATE VIEW _radius_machine_account AS
    SELECT machine_account.name, vlan.vid, machine_account.tagged_vlans FROM (((((public.machine_account JOIN public.system_image ON ((machine_account.system_image_id = system_image.id))) JOIN public.mm_system_image_ip_address USING (system_image_id)) JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) JOIN public.vlan ON ((subnet.vlan_id = vlan.id)));


ALTER TABLE radius._radius_machine_account OWNER TO postgres;

--
-- Name: _radius_switch_port_auth; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW _radius_switch_port_auth AS
    SELECT ip_address.ip, switchport.name, switchport.switch_auth_id FROM (((public.ip_address JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) JOIN (public.system_image JOIN public.switch USING (hardware_id)) ON ((mm_system_image_ip_address.system_image_id = system_image.id))) JOIN public.switchport ON ((switchport.switch_id = switch.id)));


ALTER TABLE radius._radius_switch_port_auth OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: additional_mac_addresses; Type: TABLE; Schema: radius; Owner: radius; Tablespace: 
--

CREATE TABLE additional_mac_addresses (
    id bigint NOT NULL,
    mac macaddr,
    first_seen timestamp without time zone,
    last_seen timestamp without time zone
);


ALTER TABLE radius.additional_mac_addresses OWNER TO radius;

--
-- Name: additional_mac_addresses_id_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE additional_mac_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.additional_mac_addresses_id_seq OWNER TO radius;

--
-- Name: additional_mac_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE additional_mac_addresses_id_seq OWNED BY additional_mac_addresses.id;


--
-- Name: radacct; Type: TABLE; Schema: radius; Owner: radius; Tablespace: 
--

CREATE TABLE radacct (
    radacctid bigint NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer
);


ALTER TABLE radius.radacct OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE radacct_radacctid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.radacct_radacctid_seq OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE radacct_radacctid_seq OWNED BY radacct.radacctid;


--
-- Name: radpostauth; Type: TABLE; Schema: radius; Owner: radius; Tablespace: 
--

CREATE TABLE radpostauth (
    id bigint NOT NULL,
    username character varying(253) NOT NULL,
    pass character varying(128),
    reply character varying(32),
    authdate timestamp with time zone DEFAULT '2015-12-02 13:30:33.807109+00'::timestamp with time zone NOT NULL,
    callingstationid character varying(50),
    nasipaddress inet
);


ALTER TABLE radius.radpostauth OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE radpostauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.radpostauth_id_seq OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE radpostauth_id_seq OWNED BY radpostauth.id;


--
-- Name: radusergroup; Type: TABLE; Schema: radius; Owner: radius; Tablespace: 
--

CREATE TABLE radusergroup (
    username character varying(64),
    groupname character varying(64),
    priority integer
);


ALTER TABLE radius.radusergroup OWNER TO radius;

--
-- Name: id; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY additional_mac_addresses ALTER COLUMN id SET DEFAULT nextval('additional_mac_addresses_id_seq'::regclass);


--
-- Name: radacctid; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radacct ALTER COLUMN radacctid SET DEFAULT nextval('radacct_radacctid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radpostauth ALTER COLUMN id SET DEFAULT nextval('radpostauth_id_seq'::regclass);


--
-- Name: radacct_acctuniqueid_key; Type: CONSTRAINT; Schema: radius; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radacct
    ADD CONSTRAINT radacct_acctuniqueid_key UNIQUE (acctuniqueid);


--
-- Name: radacct_pkey; Type: CONSTRAINT; Schema: radius; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radacct
    ADD CONSTRAINT radacct_pkey PRIMARY KEY (radacctid);


--
-- Name: radpostauth_pkey; Type: CONSTRAINT; Schema: radius; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radpostauth
    ADD CONSTRAINT radpostauth_pkey PRIMARY KEY (id);


--
-- Name: radacct_active_user_idx; Type: INDEX; Schema: radius; Owner: radius; Tablespace: 
--

CREATE INDEX radacct_active_user_idx ON radacct USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_start_user_idx; Type: INDEX; Schema: radius; Owner: radius; Tablespace: 
--

CREATE INDEX radacct_start_user_idx ON radacct USING btree (acctstarttime, username);

--
-- Found on radius1 and radius0 but not on radius2 
--

CREATE INDEX radacct_id_tail ON radacct USING btree (lower("substring"((acctsessionid)::text, 11, 2)));


--
-- Name: radcheck; Type: ACL; Schema: radius; Owner: postgres
--

REVOKE ALL ON TABLE radcheck FROM PUBLIC;
REVOKE ALL ON TABLE radcheck FROM postgres;
GRANT ALL ON TABLE radcheck TO postgres;
GRANT SELECT ON TABLE radcheck TO radius;


--
-- Name: _radius_mac_address; Type: ACL; Schema: radius; Owner: postgres
--

REVOKE ALL ON TABLE _radius_mac_address FROM PUBLIC;
REVOKE ALL ON TABLE _radius_mac_address FROM postgres;
GRANT ALL ON TABLE _radius_mac_address TO postgres;
GRANT SELECT ON TABLE _radius_mac_address TO radius;


--
-- Name: _radius_machine_account; Type: ACL; Schema: radius; Owner: postgres
--

REVOKE ALL ON TABLE _radius_machine_account FROM PUBLIC;
REVOKE ALL ON TABLE _radius_machine_account FROM postgres;
GRANT ALL ON TABLE _radius_machine_account TO postgres;
GRANT SELECT ON TABLE _radius_machine_account TO radius;


--
-- Name: _radius_switch_port_auth; Type: ACL; Schema: radius; Owner: postgres
--

REVOKE ALL ON TABLE _radius_switch_port_auth FROM PUBLIC;
REVOKE ALL ON TABLE _radius_switch_port_auth FROM postgres;
GRANT ALL ON TABLE _radius_switch_port_auth TO postgres;
GRANT SELECT ON TABLE _radius_switch_port_auth TO radius;


--
-- Name: additional_mac_addresses; Type: ACL; Schema: radius; Owner: radius
--

REVOKE ALL ON TABLE additional_mac_addresses FROM PUBLIC;
REVOKE ALL ON TABLE additional_mac_addresses FROM radius;
GRANT ALL ON TABLE additional_mac_addresses TO radius;
GRANT ALL ON TABLE additional_mac_addresses TO postgres;


--
-- Name: radpostauth; Type: ACL; Schema: radius; Owner: radius
--

REVOKE ALL ON TABLE radpostauth FROM PUBLIC;
REVOKE ALL ON TABLE radpostauth FROM radius;
GRANT ALL ON TABLE radpostauth TO radius;
GRANT ALL ON TABLE radpostauth TO postgres;


--
-- Name: radusergroup; Type: ACL; Schema: radius; Owner: radius
--

REVOKE ALL ON TABLE radusergroup FROM PUBLIC;
REVOKE ALL ON TABLE radusergroup FROM radius;
GRANT ALL ON TABLE radusergroup TO radius;
GRANT ALL ON TABLE radusergroup TO postgres;

--
-- Additional permissions on objects from main chemistry database
--

GRANT SELECT ON TABLE public.machine_account, public.user_account, public.non_chemnet_user_account, public.shadow_mac_to_vlan, public.switchport, public.switch, public.system_image, public.mm_system_image_ip_address, public.ip_address, public.switch_port_config_goal TO radius;
