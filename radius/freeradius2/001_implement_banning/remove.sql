SET search_path = radius, pg_catalog;

CREATE OR REPLACE FUNCTION radcheck_personal(_crsid character varying, _realm character varying) RETURNS SETOF dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 perform * from user_account where username=_crsid and not is_disabled and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into ChemNet's VLAN (as if wireless)
  return query select * from radcheck_vlan(_crsid||'@'||_realm,618);
end if;
end;
$$;

ALTER FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) OWNER TO postgres;

CREATE OR REPLACE VIEW _radius_machine_account AS
    SELECT machine_account.name, vlan.vid, machine_account.tagged_vlans FROM (((((public.machine_account JOIN public.system_image ON ((machine_account.system_image_id = system_image.id))) JOIN public.mm_system_image_ip_address USING (system_image_id)) JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) JOIN public.vlan ON ((subnet.vlan_id = vlan.id)));


ALTER TABLE radius._radius_machine_account OWNER TO postgres;

CREATE OR REPLACE VIEW _radius_mac_address AS
    SELECT vlan.vid, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wired_mac_3, system_image.wired_mac_4, system_image.wireless_mac FROM ((((public.system_image JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = system_image.id))) JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id))) JOIN public.subnet ON ((ip_address.subnet_id = subnet.id))) JOIN public.vlan ON ((subnet.vlan_id = vlan.id))) WHERE (NOT subnet.ignore_vlan);


ALTER TABLE radius._radius_mac_address OWNER TO postgres;


CREATE OR REPLACE FUNCTION radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT user_account.id, user_account.username::text ||'@'|| _realm AS username, 
        'Cleartext-Password'::character varying AS attribute, user_account.chemnet_token AS value, 
        ':='::character varying AS op, user_account.username AS raw
        FROM user_account where username=_localpart and not is_disabled and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;

ALTER FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;


CREATE OR REPLACE FUNCTION radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
begin
 -- TODO - machines may not log in to specific ports - server ports etc.
 return query select machine_account.id, machine_account.name || _realm as username, 
        'Cleartext-Password'::varchar as attribute, machine_account.chemnet_token as value,
        ':='::varchar as op, machine_account.name as raw
        from machine_account where name=_localpart;
end;
$$;


ALTER FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

SET search_path = "$user",public;
