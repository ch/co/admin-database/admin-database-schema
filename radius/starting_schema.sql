--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Ubuntu 13.1-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.1 (Ubuntu 13.1-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: radius; Type: SCHEMA; Schema: -; Owner: radius
--

CREATE SCHEMA radius;


ALTER SCHEMA radius OWNER TO radius;

--
-- Name: radacct_insert_trigger(); Type: FUNCTION; Schema: radius; Owner: radius
--

CREATE FUNCTION radius.radacct_insert_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
    hash varchar;
    tablename varchar;
BEGIN
    hash=lower(substring(new.acctsessionid from 11 for 2));
    tablename='radius.radacct_'||hash;
    -- perform * from information_schema.tables where table_name=tablename;
    -- if not found then
    --   execute 'create table '||tablename||
    --           ' (CHECK ( lower(substring(acctsessionid from 11 for 2))='''||hash
    --           ||''')) INHERITS (radacct);';
    -- end if;
    execute 'insert into '||tablename||' SELECT $1.* ' using new;
    RETURN NULL;
END;
$_$;


ALTER FUNCTION radius.radacct_insert_trigger() OWNER TO radius;

--
-- Name: dummy_radreply; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius.dummy_radreply AS
 SELECT NULL::bigint AS id,
    NULL::character varying AS username,
    NULL::character varying AS attribute,
    NULL::character varying AS value,
    NULL::character varying AS op;


ALTER TABLE radius.dummy_radreply OWNER TO postgres;

--
-- Name: radcheck_mac(character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_mac(_mac character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _vlanid integer;
 -- Performs the authentication bit for local users or mac-based users
begin
 -- Use vlan determined by shadow_mac_to_vlan first
select vid from shadow_mac_to_vlan where mac=_mac::macaddr into _vlanid;
if found then
 return query select * from radcheck_vlan(_mac,_vlanid);
else
 select vid from _radius_mac_address where _mac::macaddr in (wired_mac_1,wired_mac_2,wired_mac_3,wired_mac_4,wireless_mac) into _vlanid;
 if found then 
  return query select * from radcheck_vlan(_mac,_vlanid);
 else
  return query select * from radcheck_vlan(_mac,192);
 end if;
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_mac(_mac character varying) OWNER TO postgres;

--
-- Name: radcheck_machine(character varying, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_machine(_machine character varying, _realm character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
declare
 _vlanid integer;
 _vlans integer[];
begin
select vid,tagged_vlans into _vlanid,_vlans from _radius_machine_account where name=_machine;
if found then
   return query select * from radcheck_vlans(_machine||'@'||_realm,_vlanid, _vlans);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_machine(_machine character varying, _realm character varying) OWNER TO postgres;

--
-- Name: radcheck_non_chemnet(character varying, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_non_chemnet(_crsid character varying, _realm character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 perform * from non_chemnet_user_account where username=_crsid and realm=_realm and not is_disabled and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into non-acad VLAN
  return query select * from radcheck_vlan(_crsid||'@'||_realm,635);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_non_chemnet(_crsid character varying, _realm character varying) OWNER TO postgres;

--
-- Name: radcheck_personal(character varying, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Places those authenticating via personal ChemNet tokens into the ChemNet VLAN

begin
 perform * from public.user_account where username=_crsid and not is_disabled and not owner_banned and (expiry_date is null or expiry_date>now());
 if found then
  -- Place machine directly into ChemNet's VLAN (as if wireless)
  return query select * from radius.radcheck_vlan(_crsid||'@'||_realm,618);
end if;
end;
$$;


ALTER FUNCTION radius.radcheck_personal(_crsid character varying, _realm character varying) OWNER TO postgres;

--
-- Name: radcheck_phone(character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_phone(_localpart character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
begin

return query select * from radcheck_vlans(_localpart,1,'{2682}');
end;
$$;


ALTER FUNCTION radius.radcheck_phone(_localpart character varying) OWNER TO postgres;

--
-- Name: radcheck_ucsap(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_ucsap(_localpart character varying, _nasip inet, _nasport character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
-- TODO Check nasip,nasport is set to UCSAP - must be done in radius_password
return query select * from radius.radcheck_vlans(_localpart,3116,'{618, 635}');
end;
$$;


ALTER FUNCTION radius.radcheck_ucsap(_localpart character varying, _nasip inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radcheck_vlan(character varying, integer); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_vlan(_user character varying, _vlanid integer) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users

begin
 return query select * from radcheck_vlans(_user,_vlanid,'{}');
end;
$$;


ALTER FUNCTION radius.radcheck_vlan(_user character varying, _vlanid integer) OWNER TO postgres;

--
-- Name: radcheck_vlans(character varying, integer, integer[]); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheck_vlans(_user character varying, _vlanid integer, _taggedvlanids integer[]) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $$
 -- Performs the authentication bit for local users or mac-based users
declare
 s radius.dummy_radreply;
 _vlan integer;
 _firstvlan boolean='t';
begin
select 1::integer as id, _user as username, 'Tunnel-Type'::varchar as attribute, '13'::varchar as "value", ':='::varchar as op into s; 
return next s;
s.attribute='Tunnel-Medium-Type';
s.value='6';
s.id=s.id+1;
return next s;
s.attribute='Tunnel-Private-Group-ID';
s.value=_vlanid::varchar;
s.id=s.id+1;
return next s; 
if _taggedvlanids is not null then
 foreach _vlan in ARRAY _taggedvlanids LOOP
  if (not _firstvlan) then
   s.op='+=';
  end if;
  s.id=s.id+1;
  s.attribute='Egress-VLANID';
  s.value=('x'||'31000'||to_hex(_vlan))::bit(32)::int::varchar;
  return next s;
  _firstvlan='f';
 end loop;
end if;
s.op=':=';
s.id=s.id+1;
s.attribute='Ingress-Filters';
s.value='1';
return next s;
end;
$$;


ALTER FUNCTION radius.radcheck_vlans(_user character varying, _vlanid integer, _taggedvlanids integer[]) OWNER TO postgres;

--
-- Name: radcheckv2(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radcheckv2(v_user_id character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.dummy_radreply
    LANGUAGE plpgsql ROWS 5
    AS $_$
declare 
 _realm varchar;
 _localpart varchar;
 -- Performs the authentication bit for local users or mac-based users
begin
_realm=split_part(v_user_id,'@',2);
_localpart=split_part(v_user_id,'@',1);
case _realm 
 when 'ch.cam.ac.uk', 'ch.2018.cam.ac.uk' then
  return query select * from radcheck_personal(_localpart,_realm);
 when 'chemnet','chemnet.ch.cam.ac.uk','chemnet.2018.ch.cam.ac.uk' then
  return query select * from radcheck_machine(_localpart,_realm);
 when 'nonacad.ch.cam.ac.uk' then
  return query select * from radcheck_non_chemnet(_localpart,_realm);
 when '' then
  -- Many options 
  case when _localpart='UCSAP8023' then
   return query select * from radcheck_ucsap(_localpart,_nas,_nasport);
  when _localpart ~ '^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$' then
   return query select * from radcheck_mac(_localpart);
  when _localpart ~ '^CP-[0-9]+G?-' then
   return query select * from radcheck_phone(_localpart);
  else
  end case;
  else
  -- Unknown realm which we don't handle here
end case;
end;
$_$;


ALTER FUNCTION radius.radcheckv2(v_user_id character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radcheck; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius.radcheck AS
 SELECT user_account.id,
    ((user_account.username)::text || '@ch.cam.ac.uk'::text) AS username,
    'Cleartext-Password'::character varying AS attribute,
    user_account.chemnet_token AS value,
    ':='::character varying AS op,
    user_account.username AS raw
   FROM public.user_account
UNION ALL
 SELECT shadow_mac_to_vlan.id,
    (shadow_mac_to_vlan.mac)::character varying AS username,
    'Cleartext-Password'::character varying AS attribute,
    (shadow_mac_to_vlan.mac)::character varying AS value,
    ':='::character varying AS op,
    (shadow_mac_to_vlan.mac)::character varying AS raw
   FROM public.shadow_mac_to_vlan;


ALTER TABLE radius.radcheck OWNER TO postgres;

--
-- Name: radpasswd_mac(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_mac(_mac character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 perform * from _radius_mac_address where _mac::macaddr in (wired_mac_1,wired_mac_2,wired_mac_3,wired_mac_4,wireless_mac);
 if found then
  -- This is a mac address we know about; permit the connection
  return query select 1::bigint, _mac::text as username,
         'Cleartext-Password'::varchar as attribute, _mac::varchar as value,
         ':='::varchar as op, _mac::varchar as raw;
 else
  -- Unknown MAC: permit the connection and log
  update additional_mac_addresses set last_seen=now() where mac=_mac::macaddr;
  if not found then
   insert into additional_mac_addresses (mac, first_seen, last_seen) values (_mac::macaddr,now(),now());
  end if;
  return query select 1::bigint, _mac::text as username,
         'Cleartext-Password'::varchar as attribute, _mac::varchar as value,
         ':='::varchar as op, _mac::varchar as raw;
 end if;
end;
$$;


ALTER FUNCTION radius.radpasswd_mac(_mac character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_machine(character varying, character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
begin
 -- TODO - machines may not log in to specific ports - server ports etc.
 return query select machine_account.id, machine_account.name || _realm as username, 
        'Cleartext-Password'::varchar as attribute, machine_account.chemnet_token as value,
        ':='::varchar as op, machine_account.name as raw
        from public.machine_account join public.system_image on machine_account.system_image_id = system_image.id where name=_localpart and (not system_image.auto_banned_from_network or system_image.override_auto_ban or system_image.reinstall_on_next_boot or (system_image.was_managed_machine_on > current_date - interval '3 months') or system_image.is_managed_mac);
end;
$$;


ALTER FUNCTION radius.radpasswd_machine(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_non_chemnet(character varying, character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_non_chemnet(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT non_chemnet_user_account.id, non_chemnet_user_account.username::text ||'@'|| _realm AS username,
        'Cleartext-Password'::character varying AS attribute, non_chemnet_user_account.token AS value,
        ':='::character varying AS op, non_chemnet_user_account.username AS raw
        FROM non_chemnet_user_account where username=_localpart and realm=_realm and not is_disabled and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;


ALTER FUNCTION radius.radpasswd_non_chemnet(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_personal(character varying, character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - users may not log in to specific ports - server ports etc.
 SELECT user_account.id, user_account.username::text ||'@'|| _realm AS username, 
        'Cleartext-Password'::character varying AS attribute, user_account.chemnet_token AS value, 
        ':='::character varying AS op, user_account.username AS raw
        FROM public.user_account where username=_localpart and not is_disabled and not owner_banned and (expiry_date is null or expiry_date>now()) into _r;
 if found then
  return next _r;
 end if;

end;
$$;


ALTER FUNCTION radius.radpasswd_personal(_localpart character varying, _realm character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_phone(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_phone(_user character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
  -- TODO - phones should not appear on UCSAP ports etc
  if (position('CP-7937G-SEP' in _user)>0) then -- Conference phones cannot have a null password ; use digit '1' by convention
   return query select 0::bigint as id, _user::text as username, 'Cleartext-Password'::varchar as attribute,
    '1'::varchar as value, ':='::varchar as op, _user as raw;
  else 
   return query select 0::bigint as id, _user::text as username, 'Cleartext-Password'::varchar as attribute,
    ''::varchar as value, ':='::varchar as op, _user as raw ;
  end if;
end;
$$;


ALTER FUNCTION radius.radpasswd_phone(_user character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswd_ucsap(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswd_ucsap(_localpart character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $$
declare 
 _r radius.radcheck%ROWTYPE;
begin
 -- TODO - UCSAPS may not log in to specific ports - server ports etc.
 _r.id=1;
 _r.username=_localpart;
 _r.attribute='Cleartext-Password';
 _r.value='27vtFNU6';
 _r.op=':=';
 _r.raw=_localpart;
 if _nas is not null and coalesce(_nasport,'') !=''  then
 perform * from switch_port_config_goal inner join switchport on switch_auth_id= switch_port_config_goal.id inner join switch on switch.id=switch_id inner join system_image using (hardware_id) inner join mm_system_image_ip_address on system_image_id=system_image.id inner join ip_address on ip_address.id=ip_address_id where switch_port_config_goal.id in (24,2) and ip=_nas and switchport.name=_nasport;
 if not found then
  RAISE WARNING 'Access point attempting to connect to port % of switch % which is not configured for it', _nasport, _nas;
  _r.value='LOGINDENIED';
 end if;
 end if;
 return next _r;
end;
$$;


ALTER FUNCTION radius.radpasswd_ucsap(_localpart character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: radpasswdv2(character varying, inet, character varying); Type: FUNCTION; Schema: radius; Owner: postgres
--

CREATE FUNCTION radius.radpasswdv2(v_user_id character varying, _nas inet, _nasport character varying) RETURNS SETOF radius.radcheck
    LANGUAGE plpgsql ROWS 5
    AS $_$
declare 
 _realm varchar;
 _localpart varchar;
 -- Performs the authentication bit for local users or mac-based users
begin
_realm=split_part(v_user_id,'@',2);
_localpart=split_part(v_user_id,'@',1);
 -- Check to see whether this port is reserved for UCS-AP?
 if _nas is not null and coalesce(_nasport,'') !=''  then
 perform * from switch_port_config_goal inner join switchport on switch_auth_id= switch_port_config_goal.id inner join switch on switch.id=switch_id inner join system_image using (hardware_id) inner join mm_system_image_ip_address on system_image_id=system_image.id inner join ip_address on ip_address.id=ip_address_id where switch_port_config_goal.id in (24,2) and ip=_nas and switchport.name=_nasport;
 if (_localpart !='UCSAP8023' and found)  then
  RAISE WARNING 'Attempt by % to connect to port % of switch % reserved for UCS-AP', v_user_id, _nasport, _nas;
  return;
 end if;
 end if;
 
case _realm 
 when 'ch.cam.ac.uk', 'ch.2018.cam.ac.uk' then
  return query select * from radpasswd_personal(_localpart,_realm,_nas,_nasport);
 when 'chemnet','chemnet.ch.cam.ac.uk','chemnet.2018.ch.cam.ac.uk' then
  return query select * from radpasswd_machine(_localpart,_realm,_nas,_nasport);
 when 'nonacad.ch.cam.ac.uk' then
  return query select * from radpasswd_non_chemnet(_localpart,_realm,_nas,_nasport);
 when '' then
  -- Many options 
  case when _localpart='UCSAP8023' then
   return query select * from radpasswd_ucsap(_localpart,_nas,_nasport);
  when _localpart ~ '^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$' then
   return query select * from radpasswd_mac(_localpart,_nas,_nasport);
  when _localpart ~ '^CP-[0-9]+G?-' then
   return query select * from radpasswd_phone(_localpart,_nas,_nasport);
  else
   -- Unknown Local Part
   RAISE WARNING 'Unknown Local Part in empty realm (%)', _localpart;
  end case;
  else
   RAISE WARNING 'Unknown Realm (%)', _realm;
  -- Unknown realm which we don't handle here
end case;
end;
$_$;


ALTER FUNCTION radius.radpasswdv2(v_user_id character varying, _nas inet, _nasport character varying) OWNER TO postgres;

--
-- Name: _radius_mac_address; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius._radius_mac_address AS
 SELECT
        CASE
            WHEN (system_image.reinstall_on_next_boot = true) THEN vlan.vid
            WHEN (operating_system.has_security_support = false) THEN 1106
            WHEN (system_image.was_managed_machine_on > (CURRENT_DATE - '3 mons'::interval)) THEN vlan.vid
            WHEN (system_image.is_managed_mac = true) THEN vlan.vid
            WHEN (system_image.auto_banned_from_network AND (NOT system_image.override_auto_ban)) THEN 191
            ELSE vlan.vid
        END AS vid,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac
   FROM (((((public.system_image
     JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id)))
     JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.system_image_id = system_image.id)))
     JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id)))
     JOIN public.subnet ON ((ip_address.subnet_id = subnet.id)))
     JOIN public.vlan ON ((subnet.vlan_id = vlan.id)))
  WHERE (NOT subnet.ignore_vlan);


ALTER TABLE radius._radius_mac_address OWNER TO postgres;

--
-- Name: _radius_machine_account; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius._radius_machine_account AS
 SELECT machine_account.name,
        CASE
            WHEN (system_image.reinstall_on_next_boot = true) THEN vlan.vid
            WHEN (operating_system.has_security_support = false) THEN 1106
            WHEN (system_image.was_managed_machine_on > (CURRENT_DATE - '3 mons'::interval)) THEN vlan.vid
            WHEN (system_image.is_managed_mac = true) THEN vlan.vid
            WHEN (system_image.auto_banned_from_network AND (NOT system_image.override_auto_ban)) THEN 191
            ELSE vlan.vid
        END AS vid,
    machine_account.tagged_vlans
   FROM ((((((public.machine_account
     JOIN public.system_image ON ((machine_account.system_image_id = system_image.id)))
     JOIN public.operating_system ON ((system_image.operating_system_id = operating_system.id)))
     JOIN public.mm_system_image_ip_address USING (system_image_id))
     JOIN public.ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id)))
     JOIN public.subnet ON ((ip_address.subnet_id = subnet.id)))
     JOIN public.vlan ON ((subnet.vlan_id = vlan.id)));


ALTER TABLE radius._radius_machine_account OWNER TO postgres;

--
-- Name: _radius_switch_port_auth; Type: VIEW; Schema: radius; Owner: postgres
--

CREATE VIEW radius._radius_switch_port_auth AS
 SELECT ip_address.ip,
    switchport.name,
    switchport.switch_auth_id
   FROM (((public.ip_address
     JOIN public.mm_system_image_ip_address ON ((mm_system_image_ip_address.ip_address_id = ip_address.id)))
     JOIN (public.system_image
     JOIN public.switch USING (hardware_id)) ON ((mm_system_image_ip_address.system_image_id = system_image.id)))
     JOIN public.switchport ON ((switchport.switch_id = switch.id)));


ALTER TABLE radius._radius_switch_port_auth OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: additional_mac_addresses; Type: TABLE; Schema: radius; Owner: radius
--

CREATE TABLE radius.additional_mac_addresses (
    id bigint NOT NULL,
    mac macaddr,
    first_seen timestamp without time zone,
    last_seen timestamp without time zone
);


ALTER TABLE radius.additional_mac_addresses OWNER TO radius;

--
-- Name: additional_mac_addresses_id_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE radius.additional_mac_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.additional_mac_addresses_id_seq OWNER TO radius;

--
-- Name: additional_mac_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE radius.additional_mac_addresses_id_seq OWNED BY radius.additional_mac_addresses.id;


--
-- Name: radacct; Type: TABLE; Schema: radius; Owner: radius
--

CREATE TABLE radius.radacct (
    radacctid bigint NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
)
PARTITION BY HASH (acctuniqueid);


ALTER TABLE radius.radacct OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE radius.radacct_radacctid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.radacct_radacctid_seq OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE radius.radacct_radacctid_seq OWNED BY radius.radacct.radacctid;


--
-- Name: radacct_0; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_0 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_0 FOR VALUES WITH (modulus 256, remainder 0);


ALTER TABLE radius.radacct_0 OWNER TO postgres;

--
-- Name: radacct_1; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_1 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_1 FOR VALUES WITH (modulus 256, remainder 1);


ALTER TABLE radius.radacct_1 OWNER TO postgres;

--
-- Name: radacct_10; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_10 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_10 FOR VALUES WITH (modulus 256, remainder 10);


ALTER TABLE radius.radacct_10 OWNER TO postgres;

--
-- Name: radacct_100; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_100 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_100 FOR VALUES WITH (modulus 256, remainder 100);


ALTER TABLE radius.radacct_100 OWNER TO postgres;

--
-- Name: radacct_101; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_101 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_101 FOR VALUES WITH (modulus 256, remainder 101);


ALTER TABLE radius.radacct_101 OWNER TO postgres;

--
-- Name: radacct_102; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_102 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_102 FOR VALUES WITH (modulus 256, remainder 102);


ALTER TABLE radius.radacct_102 OWNER TO postgres;

--
-- Name: radacct_103; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_103 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_103 FOR VALUES WITH (modulus 256, remainder 103);


ALTER TABLE radius.radacct_103 OWNER TO postgres;

--
-- Name: radacct_104; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_104 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_104 FOR VALUES WITH (modulus 256, remainder 104);


ALTER TABLE radius.radacct_104 OWNER TO postgres;

--
-- Name: radacct_105; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_105 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_105 FOR VALUES WITH (modulus 256, remainder 105);


ALTER TABLE radius.radacct_105 OWNER TO postgres;

--
-- Name: radacct_106; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_106 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_106 FOR VALUES WITH (modulus 256, remainder 106);


ALTER TABLE radius.radacct_106 OWNER TO postgres;

--
-- Name: radacct_107; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_107 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_107 FOR VALUES WITH (modulus 256, remainder 107);


ALTER TABLE radius.radacct_107 OWNER TO postgres;

--
-- Name: radacct_108; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_108 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_108 FOR VALUES WITH (modulus 256, remainder 108);


ALTER TABLE radius.radacct_108 OWNER TO postgres;

--
-- Name: radacct_109; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_109 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_109 FOR VALUES WITH (modulus 256, remainder 109);


ALTER TABLE radius.radacct_109 OWNER TO postgres;

--
-- Name: radacct_11; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_11 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_11 FOR VALUES WITH (modulus 256, remainder 11);


ALTER TABLE radius.radacct_11 OWNER TO postgres;

--
-- Name: radacct_110; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_110 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_110 FOR VALUES WITH (modulus 256, remainder 110);


ALTER TABLE radius.radacct_110 OWNER TO postgres;

--
-- Name: radacct_111; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_111 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_111 FOR VALUES WITH (modulus 256, remainder 111);


ALTER TABLE radius.radacct_111 OWNER TO postgres;

--
-- Name: radacct_112; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_112 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_112 FOR VALUES WITH (modulus 256, remainder 112);


ALTER TABLE radius.radacct_112 OWNER TO postgres;

--
-- Name: radacct_113; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_113 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_113 FOR VALUES WITH (modulus 256, remainder 113);


ALTER TABLE radius.radacct_113 OWNER TO postgres;

--
-- Name: radacct_114; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_114 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_114 FOR VALUES WITH (modulus 256, remainder 114);


ALTER TABLE radius.radacct_114 OWNER TO postgres;

--
-- Name: radacct_115; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_115 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_115 FOR VALUES WITH (modulus 256, remainder 115);


ALTER TABLE radius.radacct_115 OWNER TO postgres;

--
-- Name: radacct_116; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_116 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_116 FOR VALUES WITH (modulus 256, remainder 116);


ALTER TABLE radius.radacct_116 OWNER TO postgres;

--
-- Name: radacct_117; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_117 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_117 FOR VALUES WITH (modulus 256, remainder 117);


ALTER TABLE radius.radacct_117 OWNER TO postgres;

--
-- Name: radacct_118; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_118 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_118 FOR VALUES WITH (modulus 256, remainder 118);


ALTER TABLE radius.radacct_118 OWNER TO postgres;

--
-- Name: radacct_119; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_119 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_119 FOR VALUES WITH (modulus 256, remainder 119);


ALTER TABLE radius.radacct_119 OWNER TO postgres;

--
-- Name: radacct_12; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_12 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_12 FOR VALUES WITH (modulus 256, remainder 12);


ALTER TABLE radius.radacct_12 OWNER TO postgres;

--
-- Name: radacct_120; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_120 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_120 FOR VALUES WITH (modulus 256, remainder 120);


ALTER TABLE radius.radacct_120 OWNER TO postgres;

--
-- Name: radacct_121; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_121 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_121 FOR VALUES WITH (modulus 256, remainder 121);


ALTER TABLE radius.radacct_121 OWNER TO postgres;

--
-- Name: radacct_122; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_122 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_122 FOR VALUES WITH (modulus 256, remainder 122);


ALTER TABLE radius.radacct_122 OWNER TO postgres;

--
-- Name: radacct_123; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_123 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_123 FOR VALUES WITH (modulus 256, remainder 123);


ALTER TABLE radius.radacct_123 OWNER TO postgres;

--
-- Name: radacct_124; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_124 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_124 FOR VALUES WITH (modulus 256, remainder 124);


ALTER TABLE radius.radacct_124 OWNER TO postgres;

--
-- Name: radacct_125; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_125 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_125 FOR VALUES WITH (modulus 256, remainder 125);


ALTER TABLE radius.radacct_125 OWNER TO postgres;

--
-- Name: radacct_126; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_126 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_126 FOR VALUES WITH (modulus 256, remainder 126);


ALTER TABLE radius.radacct_126 OWNER TO postgres;

--
-- Name: radacct_127; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_127 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_127 FOR VALUES WITH (modulus 256, remainder 127);


ALTER TABLE radius.radacct_127 OWNER TO postgres;

--
-- Name: radacct_128; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_128 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_128 FOR VALUES WITH (modulus 256, remainder 128);


ALTER TABLE radius.radacct_128 OWNER TO postgres;

--
-- Name: radacct_129; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_129 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_129 FOR VALUES WITH (modulus 256, remainder 129);


ALTER TABLE radius.radacct_129 OWNER TO postgres;

--
-- Name: radacct_13; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_13 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_13 FOR VALUES WITH (modulus 256, remainder 13);


ALTER TABLE radius.radacct_13 OWNER TO postgres;

--
-- Name: radacct_130; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_130 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_130 FOR VALUES WITH (modulus 256, remainder 130);


ALTER TABLE radius.radacct_130 OWNER TO postgres;

--
-- Name: radacct_131; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_131 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_131 FOR VALUES WITH (modulus 256, remainder 131);


ALTER TABLE radius.radacct_131 OWNER TO postgres;

--
-- Name: radacct_132; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_132 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_132 FOR VALUES WITH (modulus 256, remainder 132);


ALTER TABLE radius.radacct_132 OWNER TO postgres;

--
-- Name: radacct_133; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_133 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_133 FOR VALUES WITH (modulus 256, remainder 133);


ALTER TABLE radius.radacct_133 OWNER TO postgres;

--
-- Name: radacct_134; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_134 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_134 FOR VALUES WITH (modulus 256, remainder 134);


ALTER TABLE radius.radacct_134 OWNER TO postgres;

--
-- Name: radacct_135; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_135 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_135 FOR VALUES WITH (modulus 256, remainder 135);


ALTER TABLE radius.radacct_135 OWNER TO postgres;

--
-- Name: radacct_136; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_136 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_136 FOR VALUES WITH (modulus 256, remainder 136);


ALTER TABLE radius.radacct_136 OWNER TO postgres;

--
-- Name: radacct_137; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_137 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_137 FOR VALUES WITH (modulus 256, remainder 137);


ALTER TABLE radius.radacct_137 OWNER TO postgres;

--
-- Name: radacct_138; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_138 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_138 FOR VALUES WITH (modulus 256, remainder 138);


ALTER TABLE radius.radacct_138 OWNER TO postgres;

--
-- Name: radacct_139; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_139 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_139 FOR VALUES WITH (modulus 256, remainder 139);


ALTER TABLE radius.radacct_139 OWNER TO postgres;

--
-- Name: radacct_14; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_14 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_14 FOR VALUES WITH (modulus 256, remainder 14);


ALTER TABLE radius.radacct_14 OWNER TO postgres;

--
-- Name: radacct_140; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_140 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_140 FOR VALUES WITH (modulus 256, remainder 140);


ALTER TABLE radius.radacct_140 OWNER TO postgres;

--
-- Name: radacct_141; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_141 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_141 FOR VALUES WITH (modulus 256, remainder 141);


ALTER TABLE radius.radacct_141 OWNER TO postgres;

--
-- Name: radacct_142; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_142 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_142 FOR VALUES WITH (modulus 256, remainder 142);


ALTER TABLE radius.radacct_142 OWNER TO postgres;

--
-- Name: radacct_143; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_143 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_143 FOR VALUES WITH (modulus 256, remainder 143);


ALTER TABLE radius.radacct_143 OWNER TO postgres;

--
-- Name: radacct_144; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_144 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_144 FOR VALUES WITH (modulus 256, remainder 144);


ALTER TABLE radius.radacct_144 OWNER TO postgres;

--
-- Name: radacct_145; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_145 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_145 FOR VALUES WITH (modulus 256, remainder 145);


ALTER TABLE radius.radacct_145 OWNER TO postgres;

--
-- Name: radacct_146; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_146 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_146 FOR VALUES WITH (modulus 256, remainder 146);


ALTER TABLE radius.radacct_146 OWNER TO postgres;

--
-- Name: radacct_147; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_147 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_147 FOR VALUES WITH (modulus 256, remainder 147);


ALTER TABLE radius.radacct_147 OWNER TO postgres;

--
-- Name: radacct_148; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_148 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_148 FOR VALUES WITH (modulus 256, remainder 148);


ALTER TABLE radius.radacct_148 OWNER TO postgres;

--
-- Name: radacct_149; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_149 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_149 FOR VALUES WITH (modulus 256, remainder 149);


ALTER TABLE radius.radacct_149 OWNER TO postgres;

--
-- Name: radacct_15; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_15 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_15 FOR VALUES WITH (modulus 256, remainder 15);


ALTER TABLE radius.radacct_15 OWNER TO postgres;

--
-- Name: radacct_150; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_150 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_150 FOR VALUES WITH (modulus 256, remainder 150);


ALTER TABLE radius.radacct_150 OWNER TO postgres;

--
-- Name: radacct_151; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_151 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_151 FOR VALUES WITH (modulus 256, remainder 151);


ALTER TABLE radius.radacct_151 OWNER TO postgres;

--
-- Name: radacct_152; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_152 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_152 FOR VALUES WITH (modulus 256, remainder 152);


ALTER TABLE radius.radacct_152 OWNER TO postgres;

--
-- Name: radacct_153; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_153 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_153 FOR VALUES WITH (modulus 256, remainder 153);


ALTER TABLE radius.radacct_153 OWNER TO postgres;

--
-- Name: radacct_154; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_154 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_154 FOR VALUES WITH (modulus 256, remainder 154);


ALTER TABLE radius.radacct_154 OWNER TO postgres;

--
-- Name: radacct_155; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_155 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_155 FOR VALUES WITH (modulus 256, remainder 155);


ALTER TABLE radius.radacct_155 OWNER TO postgres;

--
-- Name: radacct_156; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_156 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_156 FOR VALUES WITH (modulus 256, remainder 156);


ALTER TABLE radius.radacct_156 OWNER TO postgres;

--
-- Name: radacct_157; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_157 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_157 FOR VALUES WITH (modulus 256, remainder 157);


ALTER TABLE radius.radacct_157 OWNER TO postgres;

--
-- Name: radacct_158; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_158 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_158 FOR VALUES WITH (modulus 256, remainder 158);


ALTER TABLE radius.radacct_158 OWNER TO postgres;

--
-- Name: radacct_159; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_159 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_159 FOR VALUES WITH (modulus 256, remainder 159);


ALTER TABLE radius.radacct_159 OWNER TO postgres;

--
-- Name: radacct_16; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_16 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_16 FOR VALUES WITH (modulus 256, remainder 16);


ALTER TABLE radius.radacct_16 OWNER TO postgres;

--
-- Name: radacct_160; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_160 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_160 FOR VALUES WITH (modulus 256, remainder 160);


ALTER TABLE radius.radacct_160 OWNER TO postgres;

--
-- Name: radacct_161; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_161 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_161 FOR VALUES WITH (modulus 256, remainder 161);


ALTER TABLE radius.radacct_161 OWNER TO postgres;

--
-- Name: radacct_162; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_162 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_162 FOR VALUES WITH (modulus 256, remainder 162);


ALTER TABLE radius.radacct_162 OWNER TO postgres;

--
-- Name: radacct_163; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_163 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_163 FOR VALUES WITH (modulus 256, remainder 163);


ALTER TABLE radius.radacct_163 OWNER TO postgres;

--
-- Name: radacct_164; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_164 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_164 FOR VALUES WITH (modulus 256, remainder 164);


ALTER TABLE radius.radacct_164 OWNER TO postgres;

--
-- Name: radacct_165; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_165 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_165 FOR VALUES WITH (modulus 256, remainder 165);


ALTER TABLE radius.radacct_165 OWNER TO postgres;

--
-- Name: radacct_166; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_166 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_166 FOR VALUES WITH (modulus 256, remainder 166);


ALTER TABLE radius.radacct_166 OWNER TO postgres;

--
-- Name: radacct_167; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_167 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_167 FOR VALUES WITH (modulus 256, remainder 167);


ALTER TABLE radius.radacct_167 OWNER TO postgres;

--
-- Name: radacct_168; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_168 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_168 FOR VALUES WITH (modulus 256, remainder 168);


ALTER TABLE radius.radacct_168 OWNER TO postgres;

--
-- Name: radacct_169; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_169 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_169 FOR VALUES WITH (modulus 256, remainder 169);


ALTER TABLE radius.radacct_169 OWNER TO postgres;

--
-- Name: radacct_17; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_17 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_17 FOR VALUES WITH (modulus 256, remainder 17);


ALTER TABLE radius.radacct_17 OWNER TO postgres;

--
-- Name: radacct_170; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_170 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_170 FOR VALUES WITH (modulus 256, remainder 170);


ALTER TABLE radius.radacct_170 OWNER TO postgres;

--
-- Name: radacct_171; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_171 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_171 FOR VALUES WITH (modulus 256, remainder 171);


ALTER TABLE radius.radacct_171 OWNER TO postgres;

--
-- Name: radacct_172; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_172 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_172 FOR VALUES WITH (modulus 256, remainder 172);


ALTER TABLE radius.radacct_172 OWNER TO postgres;

--
-- Name: radacct_173; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_173 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_173 FOR VALUES WITH (modulus 256, remainder 173);


ALTER TABLE radius.radacct_173 OWNER TO postgres;

--
-- Name: radacct_174; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_174 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_174 FOR VALUES WITH (modulus 256, remainder 174);


ALTER TABLE radius.radacct_174 OWNER TO postgres;

--
-- Name: radacct_175; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_175 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_175 FOR VALUES WITH (modulus 256, remainder 175);


ALTER TABLE radius.radacct_175 OWNER TO postgres;

--
-- Name: radacct_176; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_176 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_176 FOR VALUES WITH (modulus 256, remainder 176);


ALTER TABLE radius.radacct_176 OWNER TO postgres;

--
-- Name: radacct_177; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_177 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_177 FOR VALUES WITH (modulus 256, remainder 177);


ALTER TABLE radius.radacct_177 OWNER TO postgres;

--
-- Name: radacct_178; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_178 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_178 FOR VALUES WITH (modulus 256, remainder 178);


ALTER TABLE radius.radacct_178 OWNER TO postgres;

--
-- Name: radacct_179; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_179 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_179 FOR VALUES WITH (modulus 256, remainder 179);


ALTER TABLE radius.radacct_179 OWNER TO postgres;

--
-- Name: radacct_18; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_18 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_18 FOR VALUES WITH (modulus 256, remainder 18);


ALTER TABLE radius.radacct_18 OWNER TO postgres;

--
-- Name: radacct_180; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_180 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_180 FOR VALUES WITH (modulus 256, remainder 180);


ALTER TABLE radius.radacct_180 OWNER TO postgres;

--
-- Name: radacct_181; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_181 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_181 FOR VALUES WITH (modulus 256, remainder 181);


ALTER TABLE radius.radacct_181 OWNER TO postgres;

--
-- Name: radacct_182; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_182 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_182 FOR VALUES WITH (modulus 256, remainder 182);


ALTER TABLE radius.radacct_182 OWNER TO postgres;

--
-- Name: radacct_183; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_183 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_183 FOR VALUES WITH (modulus 256, remainder 183);


ALTER TABLE radius.radacct_183 OWNER TO postgres;

--
-- Name: radacct_184; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_184 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_184 FOR VALUES WITH (modulus 256, remainder 184);


ALTER TABLE radius.radacct_184 OWNER TO postgres;

--
-- Name: radacct_185; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_185 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_185 FOR VALUES WITH (modulus 256, remainder 185);


ALTER TABLE radius.radacct_185 OWNER TO postgres;

--
-- Name: radacct_186; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_186 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_186 FOR VALUES WITH (modulus 256, remainder 186);


ALTER TABLE radius.radacct_186 OWNER TO postgres;

--
-- Name: radacct_187; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_187 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_187 FOR VALUES WITH (modulus 256, remainder 187);


ALTER TABLE radius.radacct_187 OWNER TO postgres;

--
-- Name: radacct_188; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_188 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_188 FOR VALUES WITH (modulus 256, remainder 188);


ALTER TABLE radius.radacct_188 OWNER TO postgres;

--
-- Name: radacct_189; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_189 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_189 FOR VALUES WITH (modulus 256, remainder 189);


ALTER TABLE radius.radacct_189 OWNER TO postgres;

--
-- Name: radacct_19; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_19 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_19 FOR VALUES WITH (modulus 256, remainder 19);


ALTER TABLE radius.radacct_19 OWNER TO postgres;

--
-- Name: radacct_190; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_190 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_190 FOR VALUES WITH (modulus 256, remainder 190);


ALTER TABLE radius.radacct_190 OWNER TO postgres;

--
-- Name: radacct_191; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_191 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_191 FOR VALUES WITH (modulus 256, remainder 191);


ALTER TABLE radius.radacct_191 OWNER TO postgres;

--
-- Name: radacct_192; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_192 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_192 FOR VALUES WITH (modulus 256, remainder 192);


ALTER TABLE radius.radacct_192 OWNER TO postgres;

--
-- Name: radacct_193; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_193 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_193 FOR VALUES WITH (modulus 256, remainder 193);


ALTER TABLE radius.radacct_193 OWNER TO postgres;

--
-- Name: radacct_194; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_194 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_194 FOR VALUES WITH (modulus 256, remainder 194);


ALTER TABLE radius.radacct_194 OWNER TO postgres;

--
-- Name: radacct_195; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_195 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_195 FOR VALUES WITH (modulus 256, remainder 195);


ALTER TABLE radius.radacct_195 OWNER TO postgres;

--
-- Name: radacct_196; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_196 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_196 FOR VALUES WITH (modulus 256, remainder 196);


ALTER TABLE radius.radacct_196 OWNER TO postgres;

--
-- Name: radacct_197; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_197 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_197 FOR VALUES WITH (modulus 256, remainder 197);


ALTER TABLE radius.radacct_197 OWNER TO postgres;

--
-- Name: radacct_198; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_198 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_198 FOR VALUES WITH (modulus 256, remainder 198);


ALTER TABLE radius.radacct_198 OWNER TO postgres;

--
-- Name: radacct_199; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_199 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_199 FOR VALUES WITH (modulus 256, remainder 199);


ALTER TABLE radius.radacct_199 OWNER TO postgres;

--
-- Name: radacct_2; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_2 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_2 FOR VALUES WITH (modulus 256, remainder 2);


ALTER TABLE radius.radacct_2 OWNER TO postgres;

--
-- Name: radacct_20; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_20 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_20 FOR VALUES WITH (modulus 256, remainder 20);


ALTER TABLE radius.radacct_20 OWNER TO postgres;

--
-- Name: radacct_200; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_200 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_200 FOR VALUES WITH (modulus 256, remainder 200);


ALTER TABLE radius.radacct_200 OWNER TO postgres;

--
-- Name: radacct_201; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_201 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_201 FOR VALUES WITH (modulus 256, remainder 201);


ALTER TABLE radius.radacct_201 OWNER TO postgres;

--
-- Name: radacct_202; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_202 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_202 FOR VALUES WITH (modulus 256, remainder 202);


ALTER TABLE radius.radacct_202 OWNER TO postgres;

--
-- Name: radacct_203; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_203 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_203 FOR VALUES WITH (modulus 256, remainder 203);


ALTER TABLE radius.radacct_203 OWNER TO postgres;

--
-- Name: radacct_204; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_204 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_204 FOR VALUES WITH (modulus 256, remainder 204);


ALTER TABLE radius.radacct_204 OWNER TO postgres;

--
-- Name: radacct_205; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_205 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_205 FOR VALUES WITH (modulus 256, remainder 205);


ALTER TABLE radius.radacct_205 OWNER TO postgres;

--
-- Name: radacct_206; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_206 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_206 FOR VALUES WITH (modulus 256, remainder 206);


ALTER TABLE radius.radacct_206 OWNER TO postgres;

--
-- Name: radacct_207; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_207 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_207 FOR VALUES WITH (modulus 256, remainder 207);


ALTER TABLE radius.radacct_207 OWNER TO postgres;

--
-- Name: radacct_208; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_208 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_208 FOR VALUES WITH (modulus 256, remainder 208);


ALTER TABLE radius.radacct_208 OWNER TO postgres;

--
-- Name: radacct_209; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_209 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_209 FOR VALUES WITH (modulus 256, remainder 209);


ALTER TABLE radius.radacct_209 OWNER TO postgres;

--
-- Name: radacct_21; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_21 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_21 FOR VALUES WITH (modulus 256, remainder 21);


ALTER TABLE radius.radacct_21 OWNER TO postgres;

--
-- Name: radacct_210; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_210 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_210 FOR VALUES WITH (modulus 256, remainder 210);


ALTER TABLE radius.radacct_210 OWNER TO postgres;

--
-- Name: radacct_211; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_211 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_211 FOR VALUES WITH (modulus 256, remainder 211);


ALTER TABLE radius.radacct_211 OWNER TO postgres;

--
-- Name: radacct_212; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_212 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_212 FOR VALUES WITH (modulus 256, remainder 212);


ALTER TABLE radius.radacct_212 OWNER TO postgres;

--
-- Name: radacct_213; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_213 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_213 FOR VALUES WITH (modulus 256, remainder 213);


ALTER TABLE radius.radacct_213 OWNER TO postgres;

--
-- Name: radacct_214; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_214 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_214 FOR VALUES WITH (modulus 256, remainder 214);


ALTER TABLE radius.radacct_214 OWNER TO postgres;

--
-- Name: radacct_215; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_215 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_215 FOR VALUES WITH (modulus 256, remainder 215);


ALTER TABLE radius.radacct_215 OWNER TO postgres;

--
-- Name: radacct_216; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_216 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_216 FOR VALUES WITH (modulus 256, remainder 216);


ALTER TABLE radius.radacct_216 OWNER TO postgres;

--
-- Name: radacct_217; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_217 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_217 FOR VALUES WITH (modulus 256, remainder 217);


ALTER TABLE radius.radacct_217 OWNER TO postgres;

--
-- Name: radacct_218; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_218 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_218 FOR VALUES WITH (modulus 256, remainder 218);


ALTER TABLE radius.radacct_218 OWNER TO postgres;

--
-- Name: radacct_219; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_219 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_219 FOR VALUES WITH (modulus 256, remainder 219);


ALTER TABLE radius.radacct_219 OWNER TO postgres;

--
-- Name: radacct_22; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_22 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_22 FOR VALUES WITH (modulus 256, remainder 22);


ALTER TABLE radius.radacct_22 OWNER TO postgres;

--
-- Name: radacct_220; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_220 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_220 FOR VALUES WITH (modulus 256, remainder 220);


ALTER TABLE radius.radacct_220 OWNER TO postgres;

--
-- Name: radacct_221; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_221 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_221 FOR VALUES WITH (modulus 256, remainder 221);


ALTER TABLE radius.radacct_221 OWNER TO postgres;

--
-- Name: radacct_222; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_222 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_222 FOR VALUES WITH (modulus 256, remainder 222);


ALTER TABLE radius.radacct_222 OWNER TO postgres;

--
-- Name: radacct_223; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_223 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_223 FOR VALUES WITH (modulus 256, remainder 223);


ALTER TABLE radius.radacct_223 OWNER TO postgres;

--
-- Name: radacct_224; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_224 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_224 FOR VALUES WITH (modulus 256, remainder 224);


ALTER TABLE radius.radacct_224 OWNER TO postgres;

--
-- Name: radacct_225; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_225 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_225 FOR VALUES WITH (modulus 256, remainder 225);


ALTER TABLE radius.radacct_225 OWNER TO postgres;

--
-- Name: radacct_226; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_226 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_226 FOR VALUES WITH (modulus 256, remainder 226);


ALTER TABLE radius.radacct_226 OWNER TO postgres;

--
-- Name: radacct_227; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_227 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_227 FOR VALUES WITH (modulus 256, remainder 227);


ALTER TABLE radius.radacct_227 OWNER TO postgres;

--
-- Name: radacct_228; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_228 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_228 FOR VALUES WITH (modulus 256, remainder 228);


ALTER TABLE radius.radacct_228 OWNER TO postgres;

--
-- Name: radacct_229; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_229 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_229 FOR VALUES WITH (modulus 256, remainder 229);


ALTER TABLE radius.radacct_229 OWNER TO postgres;

--
-- Name: radacct_23; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_23 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_23 FOR VALUES WITH (modulus 256, remainder 23);


ALTER TABLE radius.radacct_23 OWNER TO postgres;

--
-- Name: radacct_230; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_230 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_230 FOR VALUES WITH (modulus 256, remainder 230);


ALTER TABLE radius.radacct_230 OWNER TO postgres;

--
-- Name: radacct_231; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_231 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_231 FOR VALUES WITH (modulus 256, remainder 231);


ALTER TABLE radius.radacct_231 OWNER TO postgres;

--
-- Name: radacct_232; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_232 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_232 FOR VALUES WITH (modulus 256, remainder 232);


ALTER TABLE radius.radacct_232 OWNER TO postgres;

--
-- Name: radacct_233; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_233 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_233 FOR VALUES WITH (modulus 256, remainder 233);


ALTER TABLE radius.radacct_233 OWNER TO postgres;

--
-- Name: radacct_234; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_234 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_234 FOR VALUES WITH (modulus 256, remainder 234);


ALTER TABLE radius.radacct_234 OWNER TO postgres;

--
-- Name: radacct_235; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_235 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_235 FOR VALUES WITH (modulus 256, remainder 235);


ALTER TABLE radius.radacct_235 OWNER TO postgres;

--
-- Name: radacct_236; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_236 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_236 FOR VALUES WITH (modulus 256, remainder 236);


ALTER TABLE radius.radacct_236 OWNER TO postgres;

--
-- Name: radacct_237; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_237 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_237 FOR VALUES WITH (modulus 256, remainder 237);


ALTER TABLE radius.radacct_237 OWNER TO postgres;

--
-- Name: radacct_238; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_238 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_238 FOR VALUES WITH (modulus 256, remainder 238);


ALTER TABLE radius.radacct_238 OWNER TO postgres;

--
-- Name: radacct_239; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_239 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_239 FOR VALUES WITH (modulus 256, remainder 239);


ALTER TABLE radius.radacct_239 OWNER TO postgres;

--
-- Name: radacct_24; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_24 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_24 FOR VALUES WITH (modulus 256, remainder 24);


ALTER TABLE radius.radacct_24 OWNER TO postgres;

--
-- Name: radacct_240; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_240 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_240 FOR VALUES WITH (modulus 256, remainder 240);


ALTER TABLE radius.radacct_240 OWNER TO postgres;

--
-- Name: radacct_241; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_241 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_241 FOR VALUES WITH (modulus 256, remainder 241);


ALTER TABLE radius.radacct_241 OWNER TO postgres;

--
-- Name: radacct_242; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_242 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_242 FOR VALUES WITH (modulus 256, remainder 242);


ALTER TABLE radius.radacct_242 OWNER TO postgres;

--
-- Name: radacct_243; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_243 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_243 FOR VALUES WITH (modulus 256, remainder 243);


ALTER TABLE radius.radacct_243 OWNER TO postgres;

--
-- Name: radacct_244; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_244 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_244 FOR VALUES WITH (modulus 256, remainder 244);


ALTER TABLE radius.radacct_244 OWNER TO postgres;

--
-- Name: radacct_245; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_245 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_245 FOR VALUES WITH (modulus 256, remainder 245);


ALTER TABLE radius.radacct_245 OWNER TO postgres;

--
-- Name: radacct_246; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_246 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_246 FOR VALUES WITH (modulus 256, remainder 246);


ALTER TABLE radius.radacct_246 OWNER TO postgres;

--
-- Name: radacct_247; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_247 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_247 FOR VALUES WITH (modulus 256, remainder 247);


ALTER TABLE radius.radacct_247 OWNER TO postgres;

--
-- Name: radacct_248; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_248 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_248 FOR VALUES WITH (modulus 256, remainder 248);


ALTER TABLE radius.radacct_248 OWNER TO postgres;

--
-- Name: radacct_249; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_249 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_249 FOR VALUES WITH (modulus 256, remainder 249);


ALTER TABLE radius.radacct_249 OWNER TO postgres;

--
-- Name: radacct_25; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_25 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_25 FOR VALUES WITH (modulus 256, remainder 25);


ALTER TABLE radius.radacct_25 OWNER TO postgres;

--
-- Name: radacct_250; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_250 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_250 FOR VALUES WITH (modulus 256, remainder 250);


ALTER TABLE radius.radacct_250 OWNER TO postgres;

--
-- Name: radacct_251; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_251 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_251 FOR VALUES WITH (modulus 256, remainder 251);


ALTER TABLE radius.radacct_251 OWNER TO postgres;

--
-- Name: radacct_252; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_252 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_252 FOR VALUES WITH (modulus 256, remainder 252);


ALTER TABLE radius.radacct_252 OWNER TO postgres;

--
-- Name: radacct_253; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_253 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_253 FOR VALUES WITH (modulus 256, remainder 253);


ALTER TABLE radius.radacct_253 OWNER TO postgres;

--
-- Name: radacct_254; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_254 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_254 FOR VALUES WITH (modulus 256, remainder 254);


ALTER TABLE radius.radacct_254 OWNER TO postgres;

--
-- Name: radacct_255; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_255 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_255 FOR VALUES WITH (modulus 256, remainder 255);


ALTER TABLE radius.radacct_255 OWNER TO postgres;

--
-- Name: radacct_26; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_26 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_26 FOR VALUES WITH (modulus 256, remainder 26);


ALTER TABLE radius.radacct_26 OWNER TO postgres;

--
-- Name: radacct_27; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_27 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_27 FOR VALUES WITH (modulus 256, remainder 27);


ALTER TABLE radius.radacct_27 OWNER TO postgres;

--
-- Name: radacct_28; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_28 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_28 FOR VALUES WITH (modulus 256, remainder 28);


ALTER TABLE radius.radacct_28 OWNER TO postgres;

--
-- Name: radacct_29; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_29 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_29 FOR VALUES WITH (modulus 256, remainder 29);


ALTER TABLE radius.radacct_29 OWNER TO postgres;

--
-- Name: radacct_3; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_3 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_3 FOR VALUES WITH (modulus 256, remainder 3);


ALTER TABLE radius.radacct_3 OWNER TO postgres;

--
-- Name: radacct_30; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_30 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_30 FOR VALUES WITH (modulus 256, remainder 30);


ALTER TABLE radius.radacct_30 OWNER TO postgres;

--
-- Name: radacct_31; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_31 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_31 FOR VALUES WITH (modulus 256, remainder 31);


ALTER TABLE radius.radacct_31 OWNER TO postgres;

--
-- Name: radacct_32; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_32 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_32 FOR VALUES WITH (modulus 256, remainder 32);


ALTER TABLE radius.radacct_32 OWNER TO postgres;

--
-- Name: radacct_33; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_33 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_33 FOR VALUES WITH (modulus 256, remainder 33);


ALTER TABLE radius.radacct_33 OWNER TO postgres;

--
-- Name: radacct_34; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_34 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_34 FOR VALUES WITH (modulus 256, remainder 34);


ALTER TABLE radius.radacct_34 OWNER TO postgres;

--
-- Name: radacct_35; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_35 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_35 FOR VALUES WITH (modulus 256, remainder 35);


ALTER TABLE radius.radacct_35 OWNER TO postgres;

--
-- Name: radacct_36; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_36 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_36 FOR VALUES WITH (modulus 256, remainder 36);


ALTER TABLE radius.radacct_36 OWNER TO postgres;

--
-- Name: radacct_37; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_37 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_37 FOR VALUES WITH (modulus 256, remainder 37);


ALTER TABLE radius.radacct_37 OWNER TO postgres;

--
-- Name: radacct_38; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_38 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_38 FOR VALUES WITH (modulus 256, remainder 38);


ALTER TABLE radius.radacct_38 OWNER TO postgres;

--
-- Name: radacct_39; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_39 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_39 FOR VALUES WITH (modulus 256, remainder 39);


ALTER TABLE radius.radacct_39 OWNER TO postgres;

--
-- Name: radacct_4; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_4 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_4 FOR VALUES WITH (modulus 256, remainder 4);


ALTER TABLE radius.radacct_4 OWNER TO postgres;

--
-- Name: radacct_40; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_40 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_40 FOR VALUES WITH (modulus 256, remainder 40);


ALTER TABLE radius.radacct_40 OWNER TO postgres;

--
-- Name: radacct_41; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_41 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_41 FOR VALUES WITH (modulus 256, remainder 41);


ALTER TABLE radius.radacct_41 OWNER TO postgres;

--
-- Name: radacct_42; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_42 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_42 FOR VALUES WITH (modulus 256, remainder 42);


ALTER TABLE radius.radacct_42 OWNER TO postgres;

--
-- Name: radacct_43; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_43 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_43 FOR VALUES WITH (modulus 256, remainder 43);


ALTER TABLE radius.radacct_43 OWNER TO postgres;

--
-- Name: radacct_44; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_44 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_44 FOR VALUES WITH (modulus 256, remainder 44);


ALTER TABLE radius.radacct_44 OWNER TO postgres;

--
-- Name: radacct_45; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_45 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_45 FOR VALUES WITH (modulus 256, remainder 45);


ALTER TABLE radius.radacct_45 OWNER TO postgres;

--
-- Name: radacct_46; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_46 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_46 FOR VALUES WITH (modulus 256, remainder 46);


ALTER TABLE radius.radacct_46 OWNER TO postgres;

--
-- Name: radacct_47; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_47 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_47 FOR VALUES WITH (modulus 256, remainder 47);


ALTER TABLE radius.radacct_47 OWNER TO postgres;

--
-- Name: radacct_48; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_48 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_48 FOR VALUES WITH (modulus 256, remainder 48);


ALTER TABLE radius.radacct_48 OWNER TO postgres;

--
-- Name: radacct_49; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_49 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_49 FOR VALUES WITH (modulus 256, remainder 49);


ALTER TABLE radius.radacct_49 OWNER TO postgres;

--
-- Name: radacct_5; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_5 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_5 FOR VALUES WITH (modulus 256, remainder 5);


ALTER TABLE radius.radacct_5 OWNER TO postgres;

--
-- Name: radacct_50; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_50 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_50 FOR VALUES WITH (modulus 256, remainder 50);


ALTER TABLE radius.radacct_50 OWNER TO postgres;

--
-- Name: radacct_51; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_51 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_51 FOR VALUES WITH (modulus 256, remainder 51);


ALTER TABLE radius.radacct_51 OWNER TO postgres;

--
-- Name: radacct_52; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_52 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_52 FOR VALUES WITH (modulus 256, remainder 52);


ALTER TABLE radius.radacct_52 OWNER TO postgres;

--
-- Name: radacct_53; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_53 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_53 FOR VALUES WITH (modulus 256, remainder 53);


ALTER TABLE radius.radacct_53 OWNER TO postgres;

--
-- Name: radacct_54; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_54 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_54 FOR VALUES WITH (modulus 256, remainder 54);


ALTER TABLE radius.radacct_54 OWNER TO postgres;

--
-- Name: radacct_55; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_55 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_55 FOR VALUES WITH (modulus 256, remainder 55);


ALTER TABLE radius.radacct_55 OWNER TO postgres;

--
-- Name: radacct_56; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_56 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_56 FOR VALUES WITH (modulus 256, remainder 56);


ALTER TABLE radius.radacct_56 OWNER TO postgres;

--
-- Name: radacct_57; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_57 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_57 FOR VALUES WITH (modulus 256, remainder 57);


ALTER TABLE radius.radacct_57 OWNER TO postgres;

--
-- Name: radacct_58; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_58 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_58 FOR VALUES WITH (modulus 256, remainder 58);


ALTER TABLE radius.radacct_58 OWNER TO postgres;

--
-- Name: radacct_59; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_59 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_59 FOR VALUES WITH (modulus 256, remainder 59);


ALTER TABLE radius.radacct_59 OWNER TO postgres;

--
-- Name: radacct_6; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_6 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_6 FOR VALUES WITH (modulus 256, remainder 6);


ALTER TABLE radius.radacct_6 OWNER TO postgres;

--
-- Name: radacct_60; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_60 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_60 FOR VALUES WITH (modulus 256, remainder 60);


ALTER TABLE radius.radacct_60 OWNER TO postgres;

--
-- Name: radacct_61; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_61 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_61 FOR VALUES WITH (modulus 256, remainder 61);


ALTER TABLE radius.radacct_61 OWNER TO postgres;

--
-- Name: radacct_62; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_62 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_62 FOR VALUES WITH (modulus 256, remainder 62);


ALTER TABLE radius.radacct_62 OWNER TO postgres;

--
-- Name: radacct_63; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_63 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_63 FOR VALUES WITH (modulus 256, remainder 63);


ALTER TABLE radius.radacct_63 OWNER TO postgres;

--
-- Name: radacct_64; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_64 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_64 FOR VALUES WITH (modulus 256, remainder 64);


ALTER TABLE radius.radacct_64 OWNER TO postgres;

--
-- Name: radacct_65; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_65 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_65 FOR VALUES WITH (modulus 256, remainder 65);


ALTER TABLE radius.radacct_65 OWNER TO postgres;

--
-- Name: radacct_66; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_66 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_66 FOR VALUES WITH (modulus 256, remainder 66);


ALTER TABLE radius.radacct_66 OWNER TO postgres;

--
-- Name: radacct_67; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_67 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_67 FOR VALUES WITH (modulus 256, remainder 67);


ALTER TABLE radius.radacct_67 OWNER TO postgres;

--
-- Name: radacct_68; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_68 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_68 FOR VALUES WITH (modulus 256, remainder 68);


ALTER TABLE radius.radacct_68 OWNER TO postgres;

--
-- Name: radacct_69; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_69 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_69 FOR VALUES WITH (modulus 256, remainder 69);


ALTER TABLE radius.radacct_69 OWNER TO postgres;

--
-- Name: radacct_7; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_7 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_7 FOR VALUES WITH (modulus 256, remainder 7);


ALTER TABLE radius.radacct_7 OWNER TO postgres;

--
-- Name: radacct_70; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_70 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_70 FOR VALUES WITH (modulus 256, remainder 70);


ALTER TABLE radius.radacct_70 OWNER TO postgres;

--
-- Name: radacct_71; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_71 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_71 FOR VALUES WITH (modulus 256, remainder 71);


ALTER TABLE radius.radacct_71 OWNER TO postgres;

--
-- Name: radacct_72; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_72 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_72 FOR VALUES WITH (modulus 256, remainder 72);


ALTER TABLE radius.radacct_72 OWNER TO postgres;

--
-- Name: radacct_73; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_73 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_73 FOR VALUES WITH (modulus 256, remainder 73);


ALTER TABLE radius.radacct_73 OWNER TO postgres;

--
-- Name: radacct_74; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_74 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_74 FOR VALUES WITH (modulus 256, remainder 74);


ALTER TABLE radius.radacct_74 OWNER TO postgres;

--
-- Name: radacct_75; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_75 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_75 FOR VALUES WITH (modulus 256, remainder 75);


ALTER TABLE radius.radacct_75 OWNER TO postgres;

--
-- Name: radacct_76; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_76 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_76 FOR VALUES WITH (modulus 256, remainder 76);


ALTER TABLE radius.radacct_76 OWNER TO postgres;

--
-- Name: radacct_77; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_77 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_77 FOR VALUES WITH (modulus 256, remainder 77);


ALTER TABLE radius.radacct_77 OWNER TO postgres;

--
-- Name: radacct_78; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_78 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_78 FOR VALUES WITH (modulus 256, remainder 78);


ALTER TABLE radius.radacct_78 OWNER TO postgres;

--
-- Name: radacct_79; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_79 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_79 FOR VALUES WITH (modulus 256, remainder 79);


ALTER TABLE radius.radacct_79 OWNER TO postgres;

--
-- Name: radacct_8; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_8 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_8 FOR VALUES WITH (modulus 256, remainder 8);


ALTER TABLE radius.radacct_8 OWNER TO postgres;

--
-- Name: radacct_80; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_80 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_80 FOR VALUES WITH (modulus 256, remainder 80);


ALTER TABLE radius.radacct_80 OWNER TO postgres;

--
-- Name: radacct_81; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_81 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_81 FOR VALUES WITH (modulus 256, remainder 81);


ALTER TABLE radius.radacct_81 OWNER TO postgres;

--
-- Name: radacct_82; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_82 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_82 FOR VALUES WITH (modulus 256, remainder 82);


ALTER TABLE radius.radacct_82 OWNER TO postgres;

--
-- Name: radacct_83; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_83 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_83 FOR VALUES WITH (modulus 256, remainder 83);


ALTER TABLE radius.radacct_83 OWNER TO postgres;

--
-- Name: radacct_84; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_84 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_84 FOR VALUES WITH (modulus 256, remainder 84);


ALTER TABLE radius.radacct_84 OWNER TO postgres;

--
-- Name: radacct_85; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_85 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_85 FOR VALUES WITH (modulus 256, remainder 85);


ALTER TABLE radius.radacct_85 OWNER TO postgres;

--
-- Name: radacct_86; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_86 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_86 FOR VALUES WITH (modulus 256, remainder 86);


ALTER TABLE radius.radacct_86 OWNER TO postgres;

--
-- Name: radacct_87; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_87 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_87 FOR VALUES WITH (modulus 256, remainder 87);


ALTER TABLE radius.radacct_87 OWNER TO postgres;

--
-- Name: radacct_88; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_88 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_88 FOR VALUES WITH (modulus 256, remainder 88);


ALTER TABLE radius.radacct_88 OWNER TO postgres;

--
-- Name: radacct_89; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_89 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_89 FOR VALUES WITH (modulus 256, remainder 89);


ALTER TABLE radius.radacct_89 OWNER TO postgres;

--
-- Name: radacct_9; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_9 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_9 FOR VALUES WITH (modulus 256, remainder 9);


ALTER TABLE radius.radacct_9 OWNER TO postgres;

--
-- Name: radacct_90; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_90 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_90 FOR VALUES WITH (modulus 256, remainder 90);


ALTER TABLE radius.radacct_90 OWNER TO postgres;

--
-- Name: radacct_91; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_91 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_91 FOR VALUES WITH (modulus 256, remainder 91);


ALTER TABLE radius.radacct_91 OWNER TO postgres;

--
-- Name: radacct_92; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_92 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_92 FOR VALUES WITH (modulus 256, remainder 92);


ALTER TABLE radius.radacct_92 OWNER TO postgres;

--
-- Name: radacct_93; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_93 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_93 FOR VALUES WITH (modulus 256, remainder 93);


ALTER TABLE radius.radacct_93 OWNER TO postgres;

--
-- Name: radacct_94; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_94 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_94 FOR VALUES WITH (modulus 256, remainder 94);


ALTER TABLE radius.radacct_94 OWNER TO postgres;

--
-- Name: radacct_95; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_95 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_95 FOR VALUES WITH (modulus 256, remainder 95);


ALTER TABLE radius.radacct_95 OWNER TO postgres;

--
-- Name: radacct_96; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_96 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_96 FOR VALUES WITH (modulus 256, remainder 96);


ALTER TABLE radius.radacct_96 OWNER TO postgres;

--
-- Name: radacct_97; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_97 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_97 FOR VALUES WITH (modulus 256, remainder 97);


ALTER TABLE radius.radacct_97 OWNER TO postgres;

--
-- Name: radacct_98; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_98 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_98 FOR VALUES WITH (modulus 256, remainder 98);


ALTER TABLE radius.radacct_98 OWNER TO postgres;

--
-- Name: radacct_99; Type: TABLE; Schema: radius; Owner: postgres
--

CREATE TABLE radius.radacct_99 (
    radacctid bigint DEFAULT nextval('radius.radacct_radacctid_seq'::regclass) NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer,
    acctupdatetime timestamp with time zone,
    framedipv6address inet,
    framedipv6prefix inet,
    framedinterfaceid text,
    delegatedipv6prefix inet,
    acctinterval bigint
);
ALTER TABLE ONLY radius.radacct ATTACH PARTITION radius.radacct_99 FOR VALUES WITH (modulus 256, remainder 99);


ALTER TABLE radius.radacct_99 OWNER TO postgres;

--
-- Name: radpostauth; Type: TABLE; Schema: radius; Owner: radius
--

CREATE TABLE radius.radpostauth (
    id bigint NOT NULL,
    username character varying(253) NOT NULL,
    pass character varying(128),
    reply character varying(32),
    authdate timestamp with time zone DEFAULT '2015-12-02 13:30:33.807109+00'::timestamp with time zone NOT NULL,
    callingstationid character varying(50),
    nasipaddress inet
);


ALTER TABLE radius.radpostauth OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE; Schema: radius; Owner: radius
--

CREATE SEQUENCE radius.radpostauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radius.radpostauth_id_seq OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE OWNED BY; Schema: radius; Owner: radius
--

ALTER SEQUENCE radius.radpostauth_id_seq OWNED BY radius.radpostauth.id;


--
-- Name: radusergroup; Type: TABLE; Schema: radius; Owner: radius
--

CREATE TABLE radius.radusergroup (
    username character varying(64),
    groupname character varying(64),
    priority integer
);


ALTER TABLE radius.radusergroup OWNER TO radius;

--
-- Name: additional_mac_addresses id; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radius.additional_mac_addresses ALTER COLUMN id SET DEFAULT nextval('radius.additional_mac_addresses_id_seq'::regclass);


--
-- Name: radacct radacctid; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radius.radacct ALTER COLUMN radacctid SET DEFAULT nextval('radius.radacct_radacctid_seq'::regclass);


--
-- Name: radpostauth id; Type: DEFAULT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radius.radpostauth ALTER COLUMN id SET DEFAULT nextval('radius.radpostauth_id_seq'::regclass);


--
-- Name: radacct radacct_pkey; Type: CONSTRAINT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radius.radacct
    ADD CONSTRAINT radacct_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_0 radacct_0_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_0
    ADD CONSTRAINT radacct_0_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_100 radacct_100_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_100
    ADD CONSTRAINT radacct_100_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_101 radacct_101_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_101
    ADD CONSTRAINT radacct_101_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_102 radacct_102_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_102
    ADD CONSTRAINT radacct_102_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_103 radacct_103_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_103
    ADD CONSTRAINT radacct_103_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_104 radacct_104_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_104
    ADD CONSTRAINT radacct_104_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_105 radacct_105_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_105
    ADD CONSTRAINT radacct_105_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_106 radacct_106_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_106
    ADD CONSTRAINT radacct_106_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_107 radacct_107_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_107
    ADD CONSTRAINT radacct_107_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_108 radacct_108_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_108
    ADD CONSTRAINT radacct_108_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_109 radacct_109_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_109
    ADD CONSTRAINT radacct_109_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_10 radacct_10_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_10
    ADD CONSTRAINT radacct_10_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_110 radacct_110_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_110
    ADD CONSTRAINT radacct_110_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_111 radacct_111_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_111
    ADD CONSTRAINT radacct_111_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_112 radacct_112_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_112
    ADD CONSTRAINT radacct_112_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_113 radacct_113_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_113
    ADD CONSTRAINT radacct_113_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_114 radacct_114_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_114
    ADD CONSTRAINT radacct_114_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_115 radacct_115_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_115
    ADD CONSTRAINT radacct_115_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_116 radacct_116_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_116
    ADD CONSTRAINT radacct_116_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_117 radacct_117_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_117
    ADD CONSTRAINT radacct_117_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_118 radacct_118_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_118
    ADD CONSTRAINT radacct_118_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_119 radacct_119_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_119
    ADD CONSTRAINT radacct_119_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_11 radacct_11_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_11
    ADD CONSTRAINT radacct_11_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_120 radacct_120_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_120
    ADD CONSTRAINT radacct_120_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_121 radacct_121_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_121
    ADD CONSTRAINT radacct_121_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_122 radacct_122_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_122
    ADD CONSTRAINT radacct_122_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_123 radacct_123_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_123
    ADD CONSTRAINT radacct_123_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_124 radacct_124_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_124
    ADD CONSTRAINT radacct_124_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_125 radacct_125_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_125
    ADD CONSTRAINT radacct_125_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_126 radacct_126_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_126
    ADD CONSTRAINT radacct_126_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_127 radacct_127_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_127
    ADD CONSTRAINT radacct_127_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_128 radacct_128_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_128
    ADD CONSTRAINT radacct_128_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_129 radacct_129_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_129
    ADD CONSTRAINT radacct_129_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_12 radacct_12_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_12
    ADD CONSTRAINT radacct_12_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_130 radacct_130_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_130
    ADD CONSTRAINT radacct_130_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_131 radacct_131_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_131
    ADD CONSTRAINT radacct_131_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_132 radacct_132_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_132
    ADD CONSTRAINT radacct_132_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_133 radacct_133_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_133
    ADD CONSTRAINT radacct_133_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_134 radacct_134_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_134
    ADD CONSTRAINT radacct_134_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_135 radacct_135_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_135
    ADD CONSTRAINT radacct_135_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_136 radacct_136_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_136
    ADD CONSTRAINT radacct_136_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_137 radacct_137_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_137
    ADD CONSTRAINT radacct_137_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_138 radacct_138_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_138
    ADD CONSTRAINT radacct_138_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_139 radacct_139_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_139
    ADD CONSTRAINT radacct_139_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_13 radacct_13_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_13
    ADD CONSTRAINT radacct_13_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_140 radacct_140_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_140
    ADD CONSTRAINT radacct_140_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_141 radacct_141_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_141
    ADD CONSTRAINT radacct_141_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_142 radacct_142_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_142
    ADD CONSTRAINT radacct_142_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_143 radacct_143_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_143
    ADD CONSTRAINT radacct_143_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_144 radacct_144_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_144
    ADD CONSTRAINT radacct_144_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_145 radacct_145_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_145
    ADD CONSTRAINT radacct_145_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_146 radacct_146_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_146
    ADD CONSTRAINT radacct_146_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_147 radacct_147_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_147
    ADD CONSTRAINT radacct_147_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_148 radacct_148_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_148
    ADD CONSTRAINT radacct_148_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_149 radacct_149_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_149
    ADD CONSTRAINT radacct_149_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_14 radacct_14_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_14
    ADD CONSTRAINT radacct_14_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_150 radacct_150_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_150
    ADD CONSTRAINT radacct_150_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_151 radacct_151_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_151
    ADD CONSTRAINT radacct_151_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_152 radacct_152_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_152
    ADD CONSTRAINT radacct_152_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_153 radacct_153_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_153
    ADD CONSTRAINT radacct_153_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_154 radacct_154_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_154
    ADD CONSTRAINT radacct_154_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_155 radacct_155_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_155
    ADD CONSTRAINT radacct_155_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_156 radacct_156_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_156
    ADD CONSTRAINT radacct_156_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_157 radacct_157_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_157
    ADD CONSTRAINT radacct_157_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_158 radacct_158_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_158
    ADD CONSTRAINT radacct_158_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_159 radacct_159_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_159
    ADD CONSTRAINT radacct_159_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_15 radacct_15_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_15
    ADD CONSTRAINT radacct_15_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_160 radacct_160_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_160
    ADD CONSTRAINT radacct_160_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_161 radacct_161_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_161
    ADD CONSTRAINT radacct_161_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_162 radacct_162_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_162
    ADD CONSTRAINT radacct_162_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_163 radacct_163_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_163
    ADD CONSTRAINT radacct_163_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_164 radacct_164_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_164
    ADD CONSTRAINT radacct_164_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_165 radacct_165_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_165
    ADD CONSTRAINT radacct_165_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_166 radacct_166_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_166
    ADD CONSTRAINT radacct_166_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_167 radacct_167_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_167
    ADD CONSTRAINT radacct_167_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_168 radacct_168_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_168
    ADD CONSTRAINT radacct_168_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_169 radacct_169_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_169
    ADD CONSTRAINT radacct_169_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_16 radacct_16_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_16
    ADD CONSTRAINT radacct_16_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_170 radacct_170_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_170
    ADD CONSTRAINT radacct_170_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_171 radacct_171_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_171
    ADD CONSTRAINT radacct_171_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_172 radacct_172_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_172
    ADD CONSTRAINT radacct_172_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_173 radacct_173_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_173
    ADD CONSTRAINT radacct_173_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_174 radacct_174_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_174
    ADD CONSTRAINT radacct_174_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_175 radacct_175_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_175
    ADD CONSTRAINT radacct_175_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_176 radacct_176_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_176
    ADD CONSTRAINT radacct_176_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_177 radacct_177_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_177
    ADD CONSTRAINT radacct_177_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_178 radacct_178_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_178
    ADD CONSTRAINT radacct_178_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_179 radacct_179_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_179
    ADD CONSTRAINT radacct_179_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_17 radacct_17_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_17
    ADD CONSTRAINT radacct_17_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_180 radacct_180_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_180
    ADD CONSTRAINT radacct_180_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_181 radacct_181_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_181
    ADD CONSTRAINT radacct_181_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_182 radacct_182_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_182
    ADD CONSTRAINT radacct_182_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_183 radacct_183_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_183
    ADD CONSTRAINT radacct_183_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_184 radacct_184_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_184
    ADD CONSTRAINT radacct_184_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_185 radacct_185_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_185
    ADD CONSTRAINT radacct_185_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_186 radacct_186_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_186
    ADD CONSTRAINT radacct_186_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_187 radacct_187_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_187
    ADD CONSTRAINT radacct_187_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_188 radacct_188_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_188
    ADD CONSTRAINT radacct_188_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_189 radacct_189_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_189
    ADD CONSTRAINT radacct_189_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_18 radacct_18_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_18
    ADD CONSTRAINT radacct_18_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_190 radacct_190_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_190
    ADD CONSTRAINT radacct_190_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_191 radacct_191_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_191
    ADD CONSTRAINT radacct_191_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_192 radacct_192_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_192
    ADD CONSTRAINT radacct_192_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_193 radacct_193_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_193
    ADD CONSTRAINT radacct_193_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_194 radacct_194_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_194
    ADD CONSTRAINT radacct_194_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_195 radacct_195_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_195
    ADD CONSTRAINT radacct_195_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_196 radacct_196_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_196
    ADD CONSTRAINT radacct_196_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_197 radacct_197_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_197
    ADD CONSTRAINT radacct_197_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_198 radacct_198_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_198
    ADD CONSTRAINT radacct_198_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_199 radacct_199_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_199
    ADD CONSTRAINT radacct_199_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_19 radacct_19_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_19
    ADD CONSTRAINT radacct_19_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_1 radacct_1_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_1
    ADD CONSTRAINT radacct_1_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_200 radacct_200_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_200
    ADD CONSTRAINT radacct_200_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_201 radacct_201_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_201
    ADD CONSTRAINT radacct_201_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_202 radacct_202_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_202
    ADD CONSTRAINT radacct_202_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_203 radacct_203_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_203
    ADD CONSTRAINT radacct_203_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_204 radacct_204_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_204
    ADD CONSTRAINT radacct_204_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_205 radacct_205_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_205
    ADD CONSTRAINT radacct_205_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_206 radacct_206_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_206
    ADD CONSTRAINT radacct_206_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_207 radacct_207_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_207
    ADD CONSTRAINT radacct_207_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_208 radacct_208_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_208
    ADD CONSTRAINT radacct_208_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_209 radacct_209_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_209
    ADD CONSTRAINT radacct_209_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_20 radacct_20_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_20
    ADD CONSTRAINT radacct_20_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_210 radacct_210_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_210
    ADD CONSTRAINT radacct_210_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_211 radacct_211_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_211
    ADD CONSTRAINT radacct_211_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_212 radacct_212_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_212
    ADD CONSTRAINT radacct_212_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_213 radacct_213_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_213
    ADD CONSTRAINT radacct_213_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_214 radacct_214_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_214
    ADD CONSTRAINT radacct_214_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_215 radacct_215_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_215
    ADD CONSTRAINT radacct_215_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_216 radacct_216_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_216
    ADD CONSTRAINT radacct_216_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_217 radacct_217_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_217
    ADD CONSTRAINT radacct_217_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_218 radacct_218_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_218
    ADD CONSTRAINT radacct_218_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_219 radacct_219_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_219
    ADD CONSTRAINT radacct_219_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_21 radacct_21_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_21
    ADD CONSTRAINT radacct_21_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_220 radacct_220_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_220
    ADD CONSTRAINT radacct_220_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_221 radacct_221_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_221
    ADD CONSTRAINT radacct_221_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_222 radacct_222_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_222
    ADD CONSTRAINT radacct_222_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_223 radacct_223_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_223
    ADD CONSTRAINT radacct_223_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_224 radacct_224_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_224
    ADD CONSTRAINT radacct_224_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_225 radacct_225_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_225
    ADD CONSTRAINT radacct_225_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_226 radacct_226_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_226
    ADD CONSTRAINT radacct_226_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_227 radacct_227_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_227
    ADD CONSTRAINT radacct_227_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_228 radacct_228_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_228
    ADD CONSTRAINT radacct_228_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_229 radacct_229_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_229
    ADD CONSTRAINT radacct_229_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_22 radacct_22_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_22
    ADD CONSTRAINT radacct_22_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_230 radacct_230_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_230
    ADD CONSTRAINT radacct_230_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_231 radacct_231_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_231
    ADD CONSTRAINT radacct_231_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_232 radacct_232_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_232
    ADD CONSTRAINT radacct_232_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_233 radacct_233_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_233
    ADD CONSTRAINT radacct_233_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_234 radacct_234_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_234
    ADD CONSTRAINT radacct_234_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_235 radacct_235_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_235
    ADD CONSTRAINT radacct_235_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_236 radacct_236_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_236
    ADD CONSTRAINT radacct_236_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_237 radacct_237_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_237
    ADD CONSTRAINT radacct_237_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_238 radacct_238_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_238
    ADD CONSTRAINT radacct_238_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_239 radacct_239_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_239
    ADD CONSTRAINT radacct_239_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_23 radacct_23_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_23
    ADD CONSTRAINT radacct_23_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_240 radacct_240_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_240
    ADD CONSTRAINT radacct_240_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_241 radacct_241_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_241
    ADD CONSTRAINT radacct_241_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_242 radacct_242_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_242
    ADD CONSTRAINT radacct_242_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_243 radacct_243_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_243
    ADD CONSTRAINT radacct_243_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_244 radacct_244_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_244
    ADD CONSTRAINT radacct_244_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_245 radacct_245_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_245
    ADD CONSTRAINT radacct_245_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_246 radacct_246_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_246
    ADD CONSTRAINT radacct_246_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_247 radacct_247_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_247
    ADD CONSTRAINT radacct_247_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_248 radacct_248_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_248
    ADD CONSTRAINT radacct_248_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_249 radacct_249_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_249
    ADD CONSTRAINT radacct_249_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_24 radacct_24_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_24
    ADD CONSTRAINT radacct_24_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_250 radacct_250_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_250
    ADD CONSTRAINT radacct_250_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_251 radacct_251_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_251
    ADD CONSTRAINT radacct_251_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_252 radacct_252_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_252
    ADD CONSTRAINT radacct_252_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_253 radacct_253_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_253
    ADD CONSTRAINT radacct_253_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_254 radacct_254_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_254
    ADD CONSTRAINT radacct_254_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_255 radacct_255_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_255
    ADD CONSTRAINT radacct_255_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_25 radacct_25_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_25
    ADD CONSTRAINT radacct_25_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_26 radacct_26_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_26
    ADD CONSTRAINT radacct_26_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_27 radacct_27_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_27
    ADD CONSTRAINT radacct_27_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_28 radacct_28_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_28
    ADD CONSTRAINT radacct_28_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_29 radacct_29_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_29
    ADD CONSTRAINT radacct_29_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_2 radacct_2_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_2
    ADD CONSTRAINT radacct_2_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_30 radacct_30_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_30
    ADD CONSTRAINT radacct_30_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_31 radacct_31_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_31
    ADD CONSTRAINT radacct_31_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_32 radacct_32_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_32
    ADD CONSTRAINT radacct_32_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_33 radacct_33_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_33
    ADD CONSTRAINT radacct_33_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_34 radacct_34_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_34
    ADD CONSTRAINT radacct_34_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_35 radacct_35_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_35
    ADD CONSTRAINT radacct_35_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_36 radacct_36_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_36
    ADD CONSTRAINT radacct_36_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_37 radacct_37_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_37
    ADD CONSTRAINT radacct_37_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_38 radacct_38_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_38
    ADD CONSTRAINT radacct_38_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_39 radacct_39_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_39
    ADD CONSTRAINT radacct_39_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_3 radacct_3_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_3
    ADD CONSTRAINT radacct_3_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_40 radacct_40_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_40
    ADD CONSTRAINT radacct_40_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_41 radacct_41_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_41
    ADD CONSTRAINT radacct_41_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_42 radacct_42_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_42
    ADD CONSTRAINT radacct_42_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_43 radacct_43_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_43
    ADD CONSTRAINT radacct_43_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_44 radacct_44_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_44
    ADD CONSTRAINT radacct_44_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_45 radacct_45_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_45
    ADD CONSTRAINT radacct_45_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_46 radacct_46_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_46
    ADD CONSTRAINT radacct_46_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_47 radacct_47_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_47
    ADD CONSTRAINT radacct_47_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_48 radacct_48_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_48
    ADD CONSTRAINT radacct_48_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_49 radacct_49_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_49
    ADD CONSTRAINT radacct_49_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_4 radacct_4_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_4
    ADD CONSTRAINT radacct_4_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_50 radacct_50_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_50
    ADD CONSTRAINT radacct_50_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_51 radacct_51_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_51
    ADD CONSTRAINT radacct_51_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_52 radacct_52_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_52
    ADD CONSTRAINT radacct_52_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_53 radacct_53_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_53
    ADD CONSTRAINT radacct_53_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_54 radacct_54_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_54
    ADD CONSTRAINT radacct_54_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_55 radacct_55_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_55
    ADD CONSTRAINT radacct_55_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_56 radacct_56_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_56
    ADD CONSTRAINT radacct_56_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_57 radacct_57_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_57
    ADD CONSTRAINT radacct_57_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_58 radacct_58_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_58
    ADD CONSTRAINT radacct_58_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_59 radacct_59_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_59
    ADD CONSTRAINT radacct_59_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_5 radacct_5_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_5
    ADD CONSTRAINT radacct_5_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_60 radacct_60_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_60
    ADD CONSTRAINT radacct_60_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_61 radacct_61_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_61
    ADD CONSTRAINT radacct_61_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_62 radacct_62_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_62
    ADD CONSTRAINT radacct_62_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_63 radacct_63_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_63
    ADD CONSTRAINT radacct_63_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_64 radacct_64_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_64
    ADD CONSTRAINT radacct_64_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_65 radacct_65_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_65
    ADD CONSTRAINT radacct_65_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_66 radacct_66_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_66
    ADD CONSTRAINT radacct_66_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_67 radacct_67_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_67
    ADD CONSTRAINT radacct_67_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_68 radacct_68_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_68
    ADD CONSTRAINT radacct_68_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_69 radacct_69_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_69
    ADD CONSTRAINT radacct_69_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_6 radacct_6_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_6
    ADD CONSTRAINT radacct_6_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_70 radacct_70_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_70
    ADD CONSTRAINT radacct_70_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_71 radacct_71_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_71
    ADD CONSTRAINT radacct_71_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_72 radacct_72_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_72
    ADD CONSTRAINT radacct_72_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_73 radacct_73_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_73
    ADD CONSTRAINT radacct_73_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_74 radacct_74_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_74
    ADD CONSTRAINT radacct_74_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_75 radacct_75_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_75
    ADD CONSTRAINT radacct_75_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_76 radacct_76_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_76
    ADD CONSTRAINT radacct_76_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_77 radacct_77_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_77
    ADD CONSTRAINT radacct_77_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_78 radacct_78_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_78
    ADD CONSTRAINT radacct_78_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_79 radacct_79_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_79
    ADD CONSTRAINT radacct_79_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_7 radacct_7_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_7
    ADD CONSTRAINT radacct_7_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_80 radacct_80_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_80
    ADD CONSTRAINT radacct_80_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_81 radacct_81_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_81
    ADD CONSTRAINT radacct_81_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_82 radacct_82_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_82
    ADD CONSTRAINT radacct_82_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_83 radacct_83_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_83
    ADD CONSTRAINT radacct_83_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_84 radacct_84_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_84
    ADD CONSTRAINT radacct_84_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_85 radacct_85_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_85
    ADD CONSTRAINT radacct_85_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_86 radacct_86_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_86
    ADD CONSTRAINT radacct_86_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_87 radacct_87_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_87
    ADD CONSTRAINT radacct_87_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_88 radacct_88_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_88
    ADD CONSTRAINT radacct_88_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_89 radacct_89_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_89
    ADD CONSTRAINT radacct_89_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_8 radacct_8_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_8
    ADD CONSTRAINT radacct_8_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_90 radacct_90_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_90
    ADD CONSTRAINT radacct_90_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_91 radacct_91_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_91
    ADD CONSTRAINT radacct_91_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_92 radacct_92_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_92
    ADD CONSTRAINT radacct_92_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_93 radacct_93_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_93
    ADD CONSTRAINT radacct_93_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_94 radacct_94_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_94
    ADD CONSTRAINT radacct_94_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_95 radacct_95_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_95
    ADD CONSTRAINT radacct_95_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_96 radacct_96_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_96
    ADD CONSTRAINT radacct_96_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_97 radacct_97_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_97
    ADD CONSTRAINT radacct_97_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_98 radacct_98_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_98
    ADD CONSTRAINT radacct_98_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_99 radacct_99_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_99
    ADD CONSTRAINT radacct_99_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radacct_9 radacct_9_pkey; Type: CONSTRAINT; Schema: radius; Owner: postgres
--

ALTER TABLE ONLY radius.radacct_9
    ADD CONSTRAINT radacct_9_pkey PRIMARY KEY (acctuniqueid);


--
-- Name: radpostauth radpostauth_pkey; Type: CONSTRAINT; Schema: radius; Owner: radius
--

ALTER TABLE ONLY radius.radpostauth
    ADD CONSTRAINT radpostauth_pkey PRIMARY KEY (id);


--
-- Name: radacct_start_user_idx; Type: INDEX; Schema: radius; Owner: radius
--

CREATE INDEX radacct_start_user_idx ON ONLY radius.radacct USING btree (acctstarttime, username);


--
-- Name: radacct_0_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_0_acctstarttime_username_idx ON radius.radacct_0 USING btree (acctstarttime, username);


--
-- Name: radacct_active_user_idx; Type: INDEX; Schema: radius; Owner: radius
--

CREATE INDEX radacct_active_user_idx ON ONLY radius.radacct USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_0_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_0_username_nasipaddress_acctsessionid_idx ON radius.radacct_0 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_100_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_100_acctstarttime_username_idx ON radius.radacct_100 USING btree (acctstarttime, username);


--
-- Name: radacct_100_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_100_username_nasipaddress_acctsessionid_idx ON radius.radacct_100 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_101_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_101_acctstarttime_username_idx ON radius.radacct_101 USING btree (acctstarttime, username);


--
-- Name: radacct_101_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_101_username_nasipaddress_acctsessionid_idx ON radius.radacct_101 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_102_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_102_acctstarttime_username_idx ON radius.radacct_102 USING btree (acctstarttime, username);


--
-- Name: radacct_102_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_102_username_nasipaddress_acctsessionid_idx ON radius.radacct_102 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_103_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_103_acctstarttime_username_idx ON radius.radacct_103 USING btree (acctstarttime, username);


--
-- Name: radacct_103_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_103_username_nasipaddress_acctsessionid_idx ON radius.radacct_103 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_104_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_104_acctstarttime_username_idx ON radius.radacct_104 USING btree (acctstarttime, username);


--
-- Name: radacct_104_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_104_username_nasipaddress_acctsessionid_idx ON radius.radacct_104 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_105_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_105_acctstarttime_username_idx ON radius.radacct_105 USING btree (acctstarttime, username);


--
-- Name: radacct_105_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_105_username_nasipaddress_acctsessionid_idx ON radius.radacct_105 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_106_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_106_acctstarttime_username_idx ON radius.radacct_106 USING btree (acctstarttime, username);


--
-- Name: radacct_106_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_106_username_nasipaddress_acctsessionid_idx ON radius.radacct_106 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_107_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_107_acctstarttime_username_idx ON radius.radacct_107 USING btree (acctstarttime, username);


--
-- Name: radacct_107_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_107_username_nasipaddress_acctsessionid_idx ON radius.radacct_107 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_108_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_108_acctstarttime_username_idx ON radius.radacct_108 USING btree (acctstarttime, username);


--
-- Name: radacct_108_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_108_username_nasipaddress_acctsessionid_idx ON radius.radacct_108 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_109_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_109_acctstarttime_username_idx ON radius.radacct_109 USING btree (acctstarttime, username);


--
-- Name: radacct_109_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_109_username_nasipaddress_acctsessionid_idx ON radius.radacct_109 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_10_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_10_acctstarttime_username_idx ON radius.radacct_10 USING btree (acctstarttime, username);


--
-- Name: radacct_10_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_10_username_nasipaddress_acctsessionid_idx ON radius.radacct_10 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_110_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_110_acctstarttime_username_idx ON radius.radacct_110 USING btree (acctstarttime, username);


--
-- Name: radacct_110_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_110_username_nasipaddress_acctsessionid_idx ON radius.radacct_110 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_111_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_111_acctstarttime_username_idx ON radius.radacct_111 USING btree (acctstarttime, username);


--
-- Name: radacct_111_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_111_username_nasipaddress_acctsessionid_idx ON radius.radacct_111 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_112_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_112_acctstarttime_username_idx ON radius.radacct_112 USING btree (acctstarttime, username);


--
-- Name: radacct_112_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_112_username_nasipaddress_acctsessionid_idx ON radius.radacct_112 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_113_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_113_acctstarttime_username_idx ON radius.radacct_113 USING btree (acctstarttime, username);


--
-- Name: radacct_113_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_113_username_nasipaddress_acctsessionid_idx ON radius.radacct_113 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_114_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_114_acctstarttime_username_idx ON radius.radacct_114 USING btree (acctstarttime, username);


--
-- Name: radacct_114_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_114_username_nasipaddress_acctsessionid_idx ON radius.radacct_114 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_115_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_115_acctstarttime_username_idx ON radius.radacct_115 USING btree (acctstarttime, username);


--
-- Name: radacct_115_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_115_username_nasipaddress_acctsessionid_idx ON radius.radacct_115 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_116_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_116_acctstarttime_username_idx ON radius.radacct_116 USING btree (acctstarttime, username);


--
-- Name: radacct_116_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_116_username_nasipaddress_acctsessionid_idx ON radius.radacct_116 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_117_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_117_acctstarttime_username_idx ON radius.radacct_117 USING btree (acctstarttime, username);


--
-- Name: radacct_117_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_117_username_nasipaddress_acctsessionid_idx ON radius.radacct_117 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_118_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_118_acctstarttime_username_idx ON radius.radacct_118 USING btree (acctstarttime, username);


--
-- Name: radacct_118_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_118_username_nasipaddress_acctsessionid_idx ON radius.radacct_118 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_119_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_119_acctstarttime_username_idx ON radius.radacct_119 USING btree (acctstarttime, username);


--
-- Name: radacct_119_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_119_username_nasipaddress_acctsessionid_idx ON radius.radacct_119 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_11_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_11_acctstarttime_username_idx ON radius.radacct_11 USING btree (acctstarttime, username);


--
-- Name: radacct_11_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_11_username_nasipaddress_acctsessionid_idx ON radius.radacct_11 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_120_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_120_acctstarttime_username_idx ON radius.radacct_120 USING btree (acctstarttime, username);


--
-- Name: radacct_120_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_120_username_nasipaddress_acctsessionid_idx ON radius.radacct_120 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_121_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_121_acctstarttime_username_idx ON radius.radacct_121 USING btree (acctstarttime, username);


--
-- Name: radacct_121_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_121_username_nasipaddress_acctsessionid_idx ON radius.radacct_121 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_122_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_122_acctstarttime_username_idx ON radius.radacct_122 USING btree (acctstarttime, username);


--
-- Name: radacct_122_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_122_username_nasipaddress_acctsessionid_idx ON radius.radacct_122 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_123_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_123_acctstarttime_username_idx ON radius.radacct_123 USING btree (acctstarttime, username);


--
-- Name: radacct_123_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_123_username_nasipaddress_acctsessionid_idx ON radius.radacct_123 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_124_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_124_acctstarttime_username_idx ON radius.radacct_124 USING btree (acctstarttime, username);


--
-- Name: radacct_124_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_124_username_nasipaddress_acctsessionid_idx ON radius.radacct_124 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_125_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_125_acctstarttime_username_idx ON radius.radacct_125 USING btree (acctstarttime, username);


--
-- Name: radacct_125_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_125_username_nasipaddress_acctsessionid_idx ON radius.radacct_125 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_126_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_126_acctstarttime_username_idx ON radius.radacct_126 USING btree (acctstarttime, username);


--
-- Name: radacct_126_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_126_username_nasipaddress_acctsessionid_idx ON radius.radacct_126 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_127_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_127_acctstarttime_username_idx ON radius.radacct_127 USING btree (acctstarttime, username);


--
-- Name: radacct_127_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_127_username_nasipaddress_acctsessionid_idx ON radius.radacct_127 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_128_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_128_acctstarttime_username_idx ON radius.radacct_128 USING btree (acctstarttime, username);


--
-- Name: radacct_128_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_128_username_nasipaddress_acctsessionid_idx ON radius.radacct_128 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_129_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_129_acctstarttime_username_idx ON radius.radacct_129 USING btree (acctstarttime, username);


--
-- Name: radacct_129_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_129_username_nasipaddress_acctsessionid_idx ON radius.radacct_129 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_12_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_12_acctstarttime_username_idx ON radius.radacct_12 USING btree (acctstarttime, username);


--
-- Name: radacct_12_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_12_username_nasipaddress_acctsessionid_idx ON radius.radacct_12 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_130_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_130_acctstarttime_username_idx ON radius.radacct_130 USING btree (acctstarttime, username);


--
-- Name: radacct_130_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_130_username_nasipaddress_acctsessionid_idx ON radius.radacct_130 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_131_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_131_acctstarttime_username_idx ON radius.radacct_131 USING btree (acctstarttime, username);


--
-- Name: radacct_131_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_131_username_nasipaddress_acctsessionid_idx ON radius.radacct_131 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_132_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_132_acctstarttime_username_idx ON radius.radacct_132 USING btree (acctstarttime, username);


--
-- Name: radacct_132_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_132_username_nasipaddress_acctsessionid_idx ON radius.radacct_132 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_133_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_133_acctstarttime_username_idx ON radius.radacct_133 USING btree (acctstarttime, username);


--
-- Name: radacct_133_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_133_username_nasipaddress_acctsessionid_idx ON radius.radacct_133 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_134_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_134_acctstarttime_username_idx ON radius.radacct_134 USING btree (acctstarttime, username);


--
-- Name: radacct_134_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_134_username_nasipaddress_acctsessionid_idx ON radius.radacct_134 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_135_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_135_acctstarttime_username_idx ON radius.radacct_135 USING btree (acctstarttime, username);


--
-- Name: radacct_135_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_135_username_nasipaddress_acctsessionid_idx ON radius.radacct_135 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_136_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_136_acctstarttime_username_idx ON radius.radacct_136 USING btree (acctstarttime, username);


--
-- Name: radacct_136_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_136_username_nasipaddress_acctsessionid_idx ON radius.radacct_136 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_137_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_137_acctstarttime_username_idx ON radius.radacct_137 USING btree (acctstarttime, username);


--
-- Name: radacct_137_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_137_username_nasipaddress_acctsessionid_idx ON radius.radacct_137 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_138_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_138_acctstarttime_username_idx ON radius.radacct_138 USING btree (acctstarttime, username);


--
-- Name: radacct_138_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_138_username_nasipaddress_acctsessionid_idx ON radius.radacct_138 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_139_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_139_acctstarttime_username_idx ON radius.radacct_139 USING btree (acctstarttime, username);


--
-- Name: radacct_139_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_139_username_nasipaddress_acctsessionid_idx ON radius.radacct_139 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_13_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_13_acctstarttime_username_idx ON radius.radacct_13 USING btree (acctstarttime, username);


--
-- Name: radacct_13_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_13_username_nasipaddress_acctsessionid_idx ON radius.radacct_13 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_140_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_140_acctstarttime_username_idx ON radius.radacct_140 USING btree (acctstarttime, username);


--
-- Name: radacct_140_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_140_username_nasipaddress_acctsessionid_idx ON radius.radacct_140 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_141_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_141_acctstarttime_username_idx ON radius.radacct_141 USING btree (acctstarttime, username);


--
-- Name: radacct_141_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_141_username_nasipaddress_acctsessionid_idx ON radius.radacct_141 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_142_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_142_acctstarttime_username_idx ON radius.radacct_142 USING btree (acctstarttime, username);


--
-- Name: radacct_142_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_142_username_nasipaddress_acctsessionid_idx ON radius.radacct_142 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_143_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_143_acctstarttime_username_idx ON radius.radacct_143 USING btree (acctstarttime, username);


--
-- Name: radacct_143_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_143_username_nasipaddress_acctsessionid_idx ON radius.radacct_143 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_144_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_144_acctstarttime_username_idx ON radius.radacct_144 USING btree (acctstarttime, username);


--
-- Name: radacct_144_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_144_username_nasipaddress_acctsessionid_idx ON radius.radacct_144 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_145_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_145_acctstarttime_username_idx ON radius.radacct_145 USING btree (acctstarttime, username);


--
-- Name: radacct_145_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_145_username_nasipaddress_acctsessionid_idx ON radius.radacct_145 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_146_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_146_acctstarttime_username_idx ON radius.radacct_146 USING btree (acctstarttime, username);


--
-- Name: radacct_146_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_146_username_nasipaddress_acctsessionid_idx ON radius.radacct_146 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_147_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_147_acctstarttime_username_idx ON radius.radacct_147 USING btree (acctstarttime, username);


--
-- Name: radacct_147_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_147_username_nasipaddress_acctsessionid_idx ON radius.radacct_147 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_148_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_148_acctstarttime_username_idx ON radius.radacct_148 USING btree (acctstarttime, username);


--
-- Name: radacct_148_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_148_username_nasipaddress_acctsessionid_idx ON radius.radacct_148 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_149_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_149_acctstarttime_username_idx ON radius.radacct_149 USING btree (acctstarttime, username);


--
-- Name: radacct_149_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_149_username_nasipaddress_acctsessionid_idx ON radius.radacct_149 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_14_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_14_acctstarttime_username_idx ON radius.radacct_14 USING btree (acctstarttime, username);


--
-- Name: radacct_14_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_14_username_nasipaddress_acctsessionid_idx ON radius.radacct_14 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_150_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_150_acctstarttime_username_idx ON radius.radacct_150 USING btree (acctstarttime, username);


--
-- Name: radacct_150_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_150_username_nasipaddress_acctsessionid_idx ON radius.radacct_150 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_151_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_151_acctstarttime_username_idx ON radius.radacct_151 USING btree (acctstarttime, username);


--
-- Name: radacct_151_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_151_username_nasipaddress_acctsessionid_idx ON radius.radacct_151 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_152_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_152_acctstarttime_username_idx ON radius.radacct_152 USING btree (acctstarttime, username);


--
-- Name: radacct_152_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_152_username_nasipaddress_acctsessionid_idx ON radius.radacct_152 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_153_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_153_acctstarttime_username_idx ON radius.radacct_153 USING btree (acctstarttime, username);


--
-- Name: radacct_153_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_153_username_nasipaddress_acctsessionid_idx ON radius.radacct_153 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_154_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_154_acctstarttime_username_idx ON radius.radacct_154 USING btree (acctstarttime, username);


--
-- Name: radacct_154_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_154_username_nasipaddress_acctsessionid_idx ON radius.radacct_154 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_155_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_155_acctstarttime_username_idx ON radius.radacct_155 USING btree (acctstarttime, username);


--
-- Name: radacct_155_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_155_username_nasipaddress_acctsessionid_idx ON radius.radacct_155 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_156_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_156_acctstarttime_username_idx ON radius.radacct_156 USING btree (acctstarttime, username);


--
-- Name: radacct_156_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_156_username_nasipaddress_acctsessionid_idx ON radius.radacct_156 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_157_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_157_acctstarttime_username_idx ON radius.radacct_157 USING btree (acctstarttime, username);


--
-- Name: radacct_157_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_157_username_nasipaddress_acctsessionid_idx ON radius.radacct_157 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_158_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_158_acctstarttime_username_idx ON radius.radacct_158 USING btree (acctstarttime, username);


--
-- Name: radacct_158_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_158_username_nasipaddress_acctsessionid_idx ON radius.radacct_158 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_159_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_159_acctstarttime_username_idx ON radius.radacct_159 USING btree (acctstarttime, username);


--
-- Name: radacct_159_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_159_username_nasipaddress_acctsessionid_idx ON radius.radacct_159 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_15_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_15_acctstarttime_username_idx ON radius.radacct_15 USING btree (acctstarttime, username);


--
-- Name: radacct_15_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_15_username_nasipaddress_acctsessionid_idx ON radius.radacct_15 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_160_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_160_acctstarttime_username_idx ON radius.radacct_160 USING btree (acctstarttime, username);


--
-- Name: radacct_160_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_160_username_nasipaddress_acctsessionid_idx ON radius.radacct_160 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_161_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_161_acctstarttime_username_idx ON radius.radacct_161 USING btree (acctstarttime, username);


--
-- Name: radacct_161_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_161_username_nasipaddress_acctsessionid_idx ON radius.radacct_161 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_162_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_162_acctstarttime_username_idx ON radius.radacct_162 USING btree (acctstarttime, username);


--
-- Name: radacct_162_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_162_username_nasipaddress_acctsessionid_idx ON radius.radacct_162 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_163_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_163_acctstarttime_username_idx ON radius.radacct_163 USING btree (acctstarttime, username);


--
-- Name: radacct_163_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_163_username_nasipaddress_acctsessionid_idx ON radius.radacct_163 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_164_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_164_acctstarttime_username_idx ON radius.radacct_164 USING btree (acctstarttime, username);


--
-- Name: radacct_164_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_164_username_nasipaddress_acctsessionid_idx ON radius.radacct_164 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_165_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_165_acctstarttime_username_idx ON radius.radacct_165 USING btree (acctstarttime, username);


--
-- Name: radacct_165_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_165_username_nasipaddress_acctsessionid_idx ON radius.radacct_165 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_166_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_166_acctstarttime_username_idx ON radius.radacct_166 USING btree (acctstarttime, username);


--
-- Name: radacct_166_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_166_username_nasipaddress_acctsessionid_idx ON radius.radacct_166 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_167_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_167_acctstarttime_username_idx ON radius.radacct_167 USING btree (acctstarttime, username);


--
-- Name: radacct_167_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_167_username_nasipaddress_acctsessionid_idx ON radius.radacct_167 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_168_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_168_acctstarttime_username_idx ON radius.radacct_168 USING btree (acctstarttime, username);


--
-- Name: radacct_168_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_168_username_nasipaddress_acctsessionid_idx ON radius.radacct_168 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_169_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_169_acctstarttime_username_idx ON radius.radacct_169 USING btree (acctstarttime, username);


--
-- Name: radacct_169_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_169_username_nasipaddress_acctsessionid_idx ON radius.radacct_169 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_16_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_16_acctstarttime_username_idx ON radius.radacct_16 USING btree (acctstarttime, username);


--
-- Name: radacct_16_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_16_username_nasipaddress_acctsessionid_idx ON radius.radacct_16 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_170_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_170_acctstarttime_username_idx ON radius.radacct_170 USING btree (acctstarttime, username);


--
-- Name: radacct_170_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_170_username_nasipaddress_acctsessionid_idx ON radius.radacct_170 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_171_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_171_acctstarttime_username_idx ON radius.radacct_171 USING btree (acctstarttime, username);


--
-- Name: radacct_171_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_171_username_nasipaddress_acctsessionid_idx ON radius.radacct_171 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_172_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_172_acctstarttime_username_idx ON radius.radacct_172 USING btree (acctstarttime, username);


--
-- Name: radacct_172_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_172_username_nasipaddress_acctsessionid_idx ON radius.radacct_172 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_173_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_173_acctstarttime_username_idx ON radius.radacct_173 USING btree (acctstarttime, username);


--
-- Name: radacct_173_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_173_username_nasipaddress_acctsessionid_idx ON radius.radacct_173 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_174_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_174_acctstarttime_username_idx ON radius.radacct_174 USING btree (acctstarttime, username);


--
-- Name: radacct_174_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_174_username_nasipaddress_acctsessionid_idx ON radius.radacct_174 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_175_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_175_acctstarttime_username_idx ON radius.radacct_175 USING btree (acctstarttime, username);


--
-- Name: radacct_175_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_175_username_nasipaddress_acctsessionid_idx ON radius.radacct_175 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_176_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_176_acctstarttime_username_idx ON radius.radacct_176 USING btree (acctstarttime, username);


--
-- Name: radacct_176_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_176_username_nasipaddress_acctsessionid_idx ON radius.radacct_176 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_177_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_177_acctstarttime_username_idx ON radius.radacct_177 USING btree (acctstarttime, username);


--
-- Name: radacct_177_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_177_username_nasipaddress_acctsessionid_idx ON radius.radacct_177 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_178_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_178_acctstarttime_username_idx ON radius.radacct_178 USING btree (acctstarttime, username);


--
-- Name: radacct_178_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_178_username_nasipaddress_acctsessionid_idx ON radius.radacct_178 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_179_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_179_acctstarttime_username_idx ON radius.radacct_179 USING btree (acctstarttime, username);


--
-- Name: radacct_179_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_179_username_nasipaddress_acctsessionid_idx ON radius.radacct_179 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_17_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_17_acctstarttime_username_idx ON radius.radacct_17 USING btree (acctstarttime, username);


--
-- Name: radacct_17_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_17_username_nasipaddress_acctsessionid_idx ON radius.radacct_17 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_180_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_180_acctstarttime_username_idx ON radius.radacct_180 USING btree (acctstarttime, username);


--
-- Name: radacct_180_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_180_username_nasipaddress_acctsessionid_idx ON radius.radacct_180 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_181_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_181_acctstarttime_username_idx ON radius.radacct_181 USING btree (acctstarttime, username);


--
-- Name: radacct_181_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_181_username_nasipaddress_acctsessionid_idx ON radius.radacct_181 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_182_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_182_acctstarttime_username_idx ON radius.radacct_182 USING btree (acctstarttime, username);


--
-- Name: radacct_182_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_182_username_nasipaddress_acctsessionid_idx ON radius.radacct_182 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_183_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_183_acctstarttime_username_idx ON radius.radacct_183 USING btree (acctstarttime, username);


--
-- Name: radacct_183_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_183_username_nasipaddress_acctsessionid_idx ON radius.radacct_183 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_184_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_184_acctstarttime_username_idx ON radius.radacct_184 USING btree (acctstarttime, username);


--
-- Name: radacct_184_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_184_username_nasipaddress_acctsessionid_idx ON radius.radacct_184 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_185_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_185_acctstarttime_username_idx ON radius.radacct_185 USING btree (acctstarttime, username);


--
-- Name: radacct_185_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_185_username_nasipaddress_acctsessionid_idx ON radius.radacct_185 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_186_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_186_acctstarttime_username_idx ON radius.radacct_186 USING btree (acctstarttime, username);


--
-- Name: radacct_186_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_186_username_nasipaddress_acctsessionid_idx ON radius.radacct_186 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_187_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_187_acctstarttime_username_idx ON radius.radacct_187 USING btree (acctstarttime, username);


--
-- Name: radacct_187_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_187_username_nasipaddress_acctsessionid_idx ON radius.radacct_187 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_188_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_188_acctstarttime_username_idx ON radius.radacct_188 USING btree (acctstarttime, username);


--
-- Name: radacct_188_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_188_username_nasipaddress_acctsessionid_idx ON radius.radacct_188 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_189_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_189_acctstarttime_username_idx ON radius.radacct_189 USING btree (acctstarttime, username);


--
-- Name: radacct_189_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_189_username_nasipaddress_acctsessionid_idx ON radius.radacct_189 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_18_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_18_acctstarttime_username_idx ON radius.radacct_18 USING btree (acctstarttime, username);


--
-- Name: radacct_18_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_18_username_nasipaddress_acctsessionid_idx ON radius.radacct_18 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_190_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_190_acctstarttime_username_idx ON radius.radacct_190 USING btree (acctstarttime, username);


--
-- Name: radacct_190_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_190_username_nasipaddress_acctsessionid_idx ON radius.radacct_190 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_191_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_191_acctstarttime_username_idx ON radius.radacct_191 USING btree (acctstarttime, username);


--
-- Name: radacct_191_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_191_username_nasipaddress_acctsessionid_idx ON radius.radacct_191 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_192_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_192_acctstarttime_username_idx ON radius.radacct_192 USING btree (acctstarttime, username);


--
-- Name: radacct_192_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_192_username_nasipaddress_acctsessionid_idx ON radius.radacct_192 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_193_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_193_acctstarttime_username_idx ON radius.radacct_193 USING btree (acctstarttime, username);


--
-- Name: radacct_193_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_193_username_nasipaddress_acctsessionid_idx ON radius.radacct_193 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_194_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_194_acctstarttime_username_idx ON radius.radacct_194 USING btree (acctstarttime, username);


--
-- Name: radacct_194_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_194_username_nasipaddress_acctsessionid_idx ON radius.radacct_194 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_195_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_195_acctstarttime_username_idx ON radius.radacct_195 USING btree (acctstarttime, username);


--
-- Name: radacct_195_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_195_username_nasipaddress_acctsessionid_idx ON radius.radacct_195 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_196_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_196_acctstarttime_username_idx ON radius.radacct_196 USING btree (acctstarttime, username);


--
-- Name: radacct_196_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_196_username_nasipaddress_acctsessionid_idx ON radius.radacct_196 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_197_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_197_acctstarttime_username_idx ON radius.radacct_197 USING btree (acctstarttime, username);


--
-- Name: radacct_197_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_197_username_nasipaddress_acctsessionid_idx ON radius.radacct_197 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_198_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_198_acctstarttime_username_idx ON radius.radacct_198 USING btree (acctstarttime, username);


--
-- Name: radacct_198_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_198_username_nasipaddress_acctsessionid_idx ON radius.radacct_198 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_199_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_199_acctstarttime_username_idx ON radius.radacct_199 USING btree (acctstarttime, username);


--
-- Name: radacct_199_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_199_username_nasipaddress_acctsessionid_idx ON radius.radacct_199 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_19_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_19_acctstarttime_username_idx ON radius.radacct_19 USING btree (acctstarttime, username);


--
-- Name: radacct_19_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_19_username_nasipaddress_acctsessionid_idx ON radius.radacct_19 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_1_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_1_acctstarttime_username_idx ON radius.radacct_1 USING btree (acctstarttime, username);


--
-- Name: radacct_1_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_1_username_nasipaddress_acctsessionid_idx ON radius.radacct_1 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_200_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_200_acctstarttime_username_idx ON radius.radacct_200 USING btree (acctstarttime, username);


--
-- Name: radacct_200_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_200_username_nasipaddress_acctsessionid_idx ON radius.radacct_200 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_201_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_201_acctstarttime_username_idx ON radius.radacct_201 USING btree (acctstarttime, username);


--
-- Name: radacct_201_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_201_username_nasipaddress_acctsessionid_idx ON radius.radacct_201 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_202_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_202_acctstarttime_username_idx ON radius.radacct_202 USING btree (acctstarttime, username);


--
-- Name: radacct_202_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_202_username_nasipaddress_acctsessionid_idx ON radius.radacct_202 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_203_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_203_acctstarttime_username_idx ON radius.radacct_203 USING btree (acctstarttime, username);


--
-- Name: radacct_203_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_203_username_nasipaddress_acctsessionid_idx ON radius.radacct_203 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_204_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_204_acctstarttime_username_idx ON radius.radacct_204 USING btree (acctstarttime, username);


--
-- Name: radacct_204_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_204_username_nasipaddress_acctsessionid_idx ON radius.radacct_204 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_205_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_205_acctstarttime_username_idx ON radius.radacct_205 USING btree (acctstarttime, username);


--
-- Name: radacct_205_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_205_username_nasipaddress_acctsessionid_idx ON radius.radacct_205 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_206_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_206_acctstarttime_username_idx ON radius.radacct_206 USING btree (acctstarttime, username);


--
-- Name: radacct_206_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_206_username_nasipaddress_acctsessionid_idx ON radius.radacct_206 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_207_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_207_acctstarttime_username_idx ON radius.radacct_207 USING btree (acctstarttime, username);


--
-- Name: radacct_207_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_207_username_nasipaddress_acctsessionid_idx ON radius.radacct_207 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_208_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_208_acctstarttime_username_idx ON radius.radacct_208 USING btree (acctstarttime, username);


--
-- Name: radacct_208_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_208_username_nasipaddress_acctsessionid_idx ON radius.radacct_208 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_209_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_209_acctstarttime_username_idx ON radius.radacct_209 USING btree (acctstarttime, username);


--
-- Name: radacct_209_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_209_username_nasipaddress_acctsessionid_idx ON radius.radacct_209 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_20_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_20_acctstarttime_username_idx ON radius.radacct_20 USING btree (acctstarttime, username);


--
-- Name: radacct_20_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_20_username_nasipaddress_acctsessionid_idx ON radius.radacct_20 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_210_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_210_acctstarttime_username_idx ON radius.radacct_210 USING btree (acctstarttime, username);


--
-- Name: radacct_210_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_210_username_nasipaddress_acctsessionid_idx ON radius.radacct_210 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_211_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_211_acctstarttime_username_idx ON radius.radacct_211 USING btree (acctstarttime, username);


--
-- Name: radacct_211_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_211_username_nasipaddress_acctsessionid_idx ON radius.radacct_211 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_212_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_212_acctstarttime_username_idx ON radius.radacct_212 USING btree (acctstarttime, username);


--
-- Name: radacct_212_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_212_username_nasipaddress_acctsessionid_idx ON radius.radacct_212 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_213_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_213_acctstarttime_username_idx ON radius.radacct_213 USING btree (acctstarttime, username);


--
-- Name: radacct_213_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_213_username_nasipaddress_acctsessionid_idx ON radius.radacct_213 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_214_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_214_acctstarttime_username_idx ON radius.radacct_214 USING btree (acctstarttime, username);


--
-- Name: radacct_214_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_214_username_nasipaddress_acctsessionid_idx ON radius.radacct_214 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_215_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_215_acctstarttime_username_idx ON radius.radacct_215 USING btree (acctstarttime, username);


--
-- Name: radacct_215_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_215_username_nasipaddress_acctsessionid_idx ON radius.radacct_215 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_216_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_216_acctstarttime_username_idx ON radius.radacct_216 USING btree (acctstarttime, username);


--
-- Name: radacct_216_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_216_username_nasipaddress_acctsessionid_idx ON radius.radacct_216 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_217_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_217_acctstarttime_username_idx ON radius.radacct_217 USING btree (acctstarttime, username);


--
-- Name: radacct_217_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_217_username_nasipaddress_acctsessionid_idx ON radius.radacct_217 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_218_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_218_acctstarttime_username_idx ON radius.radacct_218 USING btree (acctstarttime, username);


--
-- Name: radacct_218_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_218_username_nasipaddress_acctsessionid_idx ON radius.radacct_218 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_219_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_219_acctstarttime_username_idx ON radius.radacct_219 USING btree (acctstarttime, username);


--
-- Name: radacct_219_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_219_username_nasipaddress_acctsessionid_idx ON radius.radacct_219 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_21_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_21_acctstarttime_username_idx ON radius.radacct_21 USING btree (acctstarttime, username);


--
-- Name: radacct_21_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_21_username_nasipaddress_acctsessionid_idx ON radius.radacct_21 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_220_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_220_acctstarttime_username_idx ON radius.radacct_220 USING btree (acctstarttime, username);


--
-- Name: radacct_220_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_220_username_nasipaddress_acctsessionid_idx ON radius.radacct_220 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_221_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_221_acctstarttime_username_idx ON radius.radacct_221 USING btree (acctstarttime, username);


--
-- Name: radacct_221_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_221_username_nasipaddress_acctsessionid_idx ON radius.radacct_221 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_222_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_222_acctstarttime_username_idx ON radius.radacct_222 USING btree (acctstarttime, username);


--
-- Name: radacct_222_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_222_username_nasipaddress_acctsessionid_idx ON radius.radacct_222 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_223_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_223_acctstarttime_username_idx ON radius.radacct_223 USING btree (acctstarttime, username);


--
-- Name: radacct_223_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_223_username_nasipaddress_acctsessionid_idx ON radius.radacct_223 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_224_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_224_acctstarttime_username_idx ON radius.radacct_224 USING btree (acctstarttime, username);


--
-- Name: radacct_224_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_224_username_nasipaddress_acctsessionid_idx ON radius.radacct_224 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_225_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_225_acctstarttime_username_idx ON radius.radacct_225 USING btree (acctstarttime, username);


--
-- Name: radacct_225_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_225_username_nasipaddress_acctsessionid_idx ON radius.radacct_225 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_226_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_226_acctstarttime_username_idx ON radius.radacct_226 USING btree (acctstarttime, username);


--
-- Name: radacct_226_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_226_username_nasipaddress_acctsessionid_idx ON radius.radacct_226 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_227_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_227_acctstarttime_username_idx ON radius.radacct_227 USING btree (acctstarttime, username);


--
-- Name: radacct_227_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_227_username_nasipaddress_acctsessionid_idx ON radius.radacct_227 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_228_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_228_acctstarttime_username_idx ON radius.radacct_228 USING btree (acctstarttime, username);


--
-- Name: radacct_228_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_228_username_nasipaddress_acctsessionid_idx ON radius.radacct_228 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_229_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_229_acctstarttime_username_idx ON radius.radacct_229 USING btree (acctstarttime, username);


--
-- Name: radacct_229_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_229_username_nasipaddress_acctsessionid_idx ON radius.radacct_229 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_22_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_22_acctstarttime_username_idx ON radius.radacct_22 USING btree (acctstarttime, username);


--
-- Name: radacct_22_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_22_username_nasipaddress_acctsessionid_idx ON radius.radacct_22 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_230_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_230_acctstarttime_username_idx ON radius.radacct_230 USING btree (acctstarttime, username);


--
-- Name: radacct_230_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_230_username_nasipaddress_acctsessionid_idx ON radius.radacct_230 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_231_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_231_acctstarttime_username_idx ON radius.radacct_231 USING btree (acctstarttime, username);


--
-- Name: radacct_231_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_231_username_nasipaddress_acctsessionid_idx ON radius.radacct_231 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_232_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_232_acctstarttime_username_idx ON radius.radacct_232 USING btree (acctstarttime, username);


--
-- Name: radacct_232_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_232_username_nasipaddress_acctsessionid_idx ON radius.radacct_232 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_233_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_233_acctstarttime_username_idx ON radius.radacct_233 USING btree (acctstarttime, username);


--
-- Name: radacct_233_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_233_username_nasipaddress_acctsessionid_idx ON radius.radacct_233 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_234_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_234_acctstarttime_username_idx ON radius.radacct_234 USING btree (acctstarttime, username);


--
-- Name: radacct_234_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_234_username_nasipaddress_acctsessionid_idx ON radius.radacct_234 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_235_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_235_acctstarttime_username_idx ON radius.radacct_235 USING btree (acctstarttime, username);


--
-- Name: radacct_235_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_235_username_nasipaddress_acctsessionid_idx ON radius.radacct_235 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_236_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_236_acctstarttime_username_idx ON radius.radacct_236 USING btree (acctstarttime, username);


--
-- Name: radacct_236_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_236_username_nasipaddress_acctsessionid_idx ON radius.radacct_236 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_237_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_237_acctstarttime_username_idx ON radius.radacct_237 USING btree (acctstarttime, username);


--
-- Name: radacct_237_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_237_username_nasipaddress_acctsessionid_idx ON radius.radacct_237 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_238_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_238_acctstarttime_username_idx ON radius.radacct_238 USING btree (acctstarttime, username);


--
-- Name: radacct_238_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_238_username_nasipaddress_acctsessionid_idx ON radius.radacct_238 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_239_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_239_acctstarttime_username_idx ON radius.radacct_239 USING btree (acctstarttime, username);


--
-- Name: radacct_239_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_239_username_nasipaddress_acctsessionid_idx ON radius.radacct_239 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_23_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_23_acctstarttime_username_idx ON radius.radacct_23 USING btree (acctstarttime, username);


--
-- Name: radacct_23_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_23_username_nasipaddress_acctsessionid_idx ON radius.radacct_23 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_240_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_240_acctstarttime_username_idx ON radius.radacct_240 USING btree (acctstarttime, username);


--
-- Name: radacct_240_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_240_username_nasipaddress_acctsessionid_idx ON radius.radacct_240 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_241_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_241_acctstarttime_username_idx ON radius.radacct_241 USING btree (acctstarttime, username);


--
-- Name: radacct_241_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_241_username_nasipaddress_acctsessionid_idx ON radius.radacct_241 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_242_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_242_acctstarttime_username_idx ON radius.radacct_242 USING btree (acctstarttime, username);


--
-- Name: radacct_242_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_242_username_nasipaddress_acctsessionid_idx ON radius.radacct_242 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_243_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_243_acctstarttime_username_idx ON radius.radacct_243 USING btree (acctstarttime, username);


--
-- Name: radacct_243_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_243_username_nasipaddress_acctsessionid_idx ON radius.radacct_243 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_244_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_244_acctstarttime_username_idx ON radius.radacct_244 USING btree (acctstarttime, username);


--
-- Name: radacct_244_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_244_username_nasipaddress_acctsessionid_idx ON radius.radacct_244 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_245_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_245_acctstarttime_username_idx ON radius.radacct_245 USING btree (acctstarttime, username);


--
-- Name: radacct_245_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_245_username_nasipaddress_acctsessionid_idx ON radius.radacct_245 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_246_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_246_acctstarttime_username_idx ON radius.radacct_246 USING btree (acctstarttime, username);


--
-- Name: radacct_246_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_246_username_nasipaddress_acctsessionid_idx ON radius.radacct_246 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_247_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_247_acctstarttime_username_idx ON radius.radacct_247 USING btree (acctstarttime, username);


--
-- Name: radacct_247_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_247_username_nasipaddress_acctsessionid_idx ON radius.radacct_247 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_248_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_248_acctstarttime_username_idx ON radius.radacct_248 USING btree (acctstarttime, username);


--
-- Name: radacct_248_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_248_username_nasipaddress_acctsessionid_idx ON radius.radacct_248 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_249_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_249_acctstarttime_username_idx ON radius.radacct_249 USING btree (acctstarttime, username);


--
-- Name: radacct_249_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_249_username_nasipaddress_acctsessionid_idx ON radius.radacct_249 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_24_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_24_acctstarttime_username_idx ON radius.radacct_24 USING btree (acctstarttime, username);


--
-- Name: radacct_24_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_24_username_nasipaddress_acctsessionid_idx ON radius.radacct_24 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_250_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_250_acctstarttime_username_idx ON radius.radacct_250 USING btree (acctstarttime, username);


--
-- Name: radacct_250_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_250_username_nasipaddress_acctsessionid_idx ON radius.radacct_250 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_251_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_251_acctstarttime_username_idx ON radius.radacct_251 USING btree (acctstarttime, username);


--
-- Name: radacct_251_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_251_username_nasipaddress_acctsessionid_idx ON radius.radacct_251 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_252_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_252_acctstarttime_username_idx ON radius.radacct_252 USING btree (acctstarttime, username);


--
-- Name: radacct_252_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_252_username_nasipaddress_acctsessionid_idx ON radius.radacct_252 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_253_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_253_acctstarttime_username_idx ON radius.radacct_253 USING btree (acctstarttime, username);


--
-- Name: radacct_253_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_253_username_nasipaddress_acctsessionid_idx ON radius.radacct_253 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_254_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_254_acctstarttime_username_idx ON radius.radacct_254 USING btree (acctstarttime, username);


--
-- Name: radacct_254_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_254_username_nasipaddress_acctsessionid_idx ON radius.radacct_254 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_255_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_255_acctstarttime_username_idx ON radius.radacct_255 USING btree (acctstarttime, username);


--
-- Name: radacct_255_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_255_username_nasipaddress_acctsessionid_idx ON radius.radacct_255 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_25_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_25_acctstarttime_username_idx ON radius.radacct_25 USING btree (acctstarttime, username);


--
-- Name: radacct_25_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_25_username_nasipaddress_acctsessionid_idx ON radius.radacct_25 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_26_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_26_acctstarttime_username_idx ON radius.radacct_26 USING btree (acctstarttime, username);


--
-- Name: radacct_26_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_26_username_nasipaddress_acctsessionid_idx ON radius.radacct_26 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_27_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_27_acctstarttime_username_idx ON radius.radacct_27 USING btree (acctstarttime, username);


--
-- Name: radacct_27_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_27_username_nasipaddress_acctsessionid_idx ON radius.radacct_27 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_28_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_28_acctstarttime_username_idx ON radius.radacct_28 USING btree (acctstarttime, username);


--
-- Name: radacct_28_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_28_username_nasipaddress_acctsessionid_idx ON radius.radacct_28 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_29_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_29_acctstarttime_username_idx ON radius.radacct_29 USING btree (acctstarttime, username);


--
-- Name: radacct_29_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_29_username_nasipaddress_acctsessionid_idx ON radius.radacct_29 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_2_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_2_acctstarttime_username_idx ON radius.radacct_2 USING btree (acctstarttime, username);


--
-- Name: radacct_2_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_2_username_nasipaddress_acctsessionid_idx ON radius.radacct_2 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_30_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_30_acctstarttime_username_idx ON radius.radacct_30 USING btree (acctstarttime, username);


--
-- Name: radacct_30_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_30_username_nasipaddress_acctsessionid_idx ON radius.radacct_30 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_31_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_31_acctstarttime_username_idx ON radius.radacct_31 USING btree (acctstarttime, username);


--
-- Name: radacct_31_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_31_username_nasipaddress_acctsessionid_idx ON radius.radacct_31 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_32_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_32_acctstarttime_username_idx ON radius.radacct_32 USING btree (acctstarttime, username);


--
-- Name: radacct_32_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_32_username_nasipaddress_acctsessionid_idx ON radius.radacct_32 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_33_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_33_acctstarttime_username_idx ON radius.radacct_33 USING btree (acctstarttime, username);


--
-- Name: radacct_33_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_33_username_nasipaddress_acctsessionid_idx ON radius.radacct_33 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_34_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_34_acctstarttime_username_idx ON radius.radacct_34 USING btree (acctstarttime, username);


--
-- Name: radacct_34_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_34_username_nasipaddress_acctsessionid_idx ON radius.radacct_34 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_35_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_35_acctstarttime_username_idx ON radius.radacct_35 USING btree (acctstarttime, username);


--
-- Name: radacct_35_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_35_username_nasipaddress_acctsessionid_idx ON radius.radacct_35 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_36_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_36_acctstarttime_username_idx ON radius.radacct_36 USING btree (acctstarttime, username);


--
-- Name: radacct_36_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_36_username_nasipaddress_acctsessionid_idx ON radius.radacct_36 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_37_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_37_acctstarttime_username_idx ON radius.radacct_37 USING btree (acctstarttime, username);


--
-- Name: radacct_37_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_37_username_nasipaddress_acctsessionid_idx ON radius.radacct_37 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_38_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_38_acctstarttime_username_idx ON radius.radacct_38 USING btree (acctstarttime, username);


--
-- Name: radacct_38_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_38_username_nasipaddress_acctsessionid_idx ON radius.radacct_38 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_39_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_39_acctstarttime_username_idx ON radius.radacct_39 USING btree (acctstarttime, username);


--
-- Name: radacct_39_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_39_username_nasipaddress_acctsessionid_idx ON radius.radacct_39 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_3_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_3_acctstarttime_username_idx ON radius.radacct_3 USING btree (acctstarttime, username);


--
-- Name: radacct_3_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_3_username_nasipaddress_acctsessionid_idx ON radius.radacct_3 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_40_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_40_acctstarttime_username_idx ON radius.radacct_40 USING btree (acctstarttime, username);


--
-- Name: radacct_40_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_40_username_nasipaddress_acctsessionid_idx ON radius.radacct_40 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_41_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_41_acctstarttime_username_idx ON radius.radacct_41 USING btree (acctstarttime, username);


--
-- Name: radacct_41_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_41_username_nasipaddress_acctsessionid_idx ON radius.radacct_41 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_42_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_42_acctstarttime_username_idx ON radius.radacct_42 USING btree (acctstarttime, username);


--
-- Name: radacct_42_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_42_username_nasipaddress_acctsessionid_idx ON radius.radacct_42 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_43_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_43_acctstarttime_username_idx ON radius.radacct_43 USING btree (acctstarttime, username);


--
-- Name: radacct_43_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_43_username_nasipaddress_acctsessionid_idx ON radius.radacct_43 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_44_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_44_acctstarttime_username_idx ON radius.radacct_44 USING btree (acctstarttime, username);


--
-- Name: radacct_44_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_44_username_nasipaddress_acctsessionid_idx ON radius.radacct_44 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_45_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_45_acctstarttime_username_idx ON radius.radacct_45 USING btree (acctstarttime, username);


--
-- Name: radacct_45_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_45_username_nasipaddress_acctsessionid_idx ON radius.radacct_45 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_46_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_46_acctstarttime_username_idx ON radius.radacct_46 USING btree (acctstarttime, username);


--
-- Name: radacct_46_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_46_username_nasipaddress_acctsessionid_idx ON radius.radacct_46 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_47_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_47_acctstarttime_username_idx ON radius.radacct_47 USING btree (acctstarttime, username);


--
-- Name: radacct_47_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_47_username_nasipaddress_acctsessionid_idx ON radius.radacct_47 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_48_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_48_acctstarttime_username_idx ON radius.radacct_48 USING btree (acctstarttime, username);


--
-- Name: radacct_48_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_48_username_nasipaddress_acctsessionid_idx ON radius.radacct_48 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_49_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_49_acctstarttime_username_idx ON radius.radacct_49 USING btree (acctstarttime, username);


--
-- Name: radacct_49_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_49_username_nasipaddress_acctsessionid_idx ON radius.radacct_49 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_4_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_4_acctstarttime_username_idx ON radius.radacct_4 USING btree (acctstarttime, username);


--
-- Name: radacct_4_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_4_username_nasipaddress_acctsessionid_idx ON radius.radacct_4 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_50_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_50_acctstarttime_username_idx ON radius.radacct_50 USING btree (acctstarttime, username);


--
-- Name: radacct_50_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_50_username_nasipaddress_acctsessionid_idx ON radius.radacct_50 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_51_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_51_acctstarttime_username_idx ON radius.radacct_51 USING btree (acctstarttime, username);


--
-- Name: radacct_51_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_51_username_nasipaddress_acctsessionid_idx ON radius.radacct_51 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_52_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_52_acctstarttime_username_idx ON radius.radacct_52 USING btree (acctstarttime, username);


--
-- Name: radacct_52_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_52_username_nasipaddress_acctsessionid_idx ON radius.radacct_52 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_53_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_53_acctstarttime_username_idx ON radius.radacct_53 USING btree (acctstarttime, username);


--
-- Name: radacct_53_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_53_username_nasipaddress_acctsessionid_idx ON radius.radacct_53 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_54_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_54_acctstarttime_username_idx ON radius.radacct_54 USING btree (acctstarttime, username);


--
-- Name: radacct_54_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_54_username_nasipaddress_acctsessionid_idx ON radius.radacct_54 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_55_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_55_acctstarttime_username_idx ON radius.radacct_55 USING btree (acctstarttime, username);


--
-- Name: radacct_55_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_55_username_nasipaddress_acctsessionid_idx ON radius.radacct_55 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_56_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_56_acctstarttime_username_idx ON radius.radacct_56 USING btree (acctstarttime, username);


--
-- Name: radacct_56_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_56_username_nasipaddress_acctsessionid_idx ON radius.radacct_56 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_57_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_57_acctstarttime_username_idx ON radius.radacct_57 USING btree (acctstarttime, username);


--
-- Name: radacct_57_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_57_username_nasipaddress_acctsessionid_idx ON radius.radacct_57 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_58_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_58_acctstarttime_username_idx ON radius.radacct_58 USING btree (acctstarttime, username);


--
-- Name: radacct_58_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_58_username_nasipaddress_acctsessionid_idx ON radius.radacct_58 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_59_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_59_acctstarttime_username_idx ON radius.radacct_59 USING btree (acctstarttime, username);


--
-- Name: radacct_59_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_59_username_nasipaddress_acctsessionid_idx ON radius.radacct_59 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_5_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_5_acctstarttime_username_idx ON radius.radacct_5 USING btree (acctstarttime, username);


--
-- Name: radacct_5_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_5_username_nasipaddress_acctsessionid_idx ON radius.radacct_5 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_60_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_60_acctstarttime_username_idx ON radius.radacct_60 USING btree (acctstarttime, username);


--
-- Name: radacct_60_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_60_username_nasipaddress_acctsessionid_idx ON radius.radacct_60 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_61_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_61_acctstarttime_username_idx ON radius.radacct_61 USING btree (acctstarttime, username);


--
-- Name: radacct_61_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_61_username_nasipaddress_acctsessionid_idx ON radius.radacct_61 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_62_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_62_acctstarttime_username_idx ON radius.radacct_62 USING btree (acctstarttime, username);


--
-- Name: radacct_62_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_62_username_nasipaddress_acctsessionid_idx ON radius.radacct_62 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_63_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_63_acctstarttime_username_idx ON radius.radacct_63 USING btree (acctstarttime, username);


--
-- Name: radacct_63_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_63_username_nasipaddress_acctsessionid_idx ON radius.radacct_63 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_64_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_64_acctstarttime_username_idx ON radius.radacct_64 USING btree (acctstarttime, username);


--
-- Name: radacct_64_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_64_username_nasipaddress_acctsessionid_idx ON radius.radacct_64 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_65_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_65_acctstarttime_username_idx ON radius.radacct_65 USING btree (acctstarttime, username);


--
-- Name: radacct_65_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_65_username_nasipaddress_acctsessionid_idx ON radius.radacct_65 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_66_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_66_acctstarttime_username_idx ON radius.radacct_66 USING btree (acctstarttime, username);


--
-- Name: radacct_66_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_66_username_nasipaddress_acctsessionid_idx ON radius.radacct_66 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_67_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_67_acctstarttime_username_idx ON radius.radacct_67 USING btree (acctstarttime, username);


--
-- Name: radacct_67_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_67_username_nasipaddress_acctsessionid_idx ON radius.radacct_67 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_68_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_68_acctstarttime_username_idx ON radius.radacct_68 USING btree (acctstarttime, username);


--
-- Name: radacct_68_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_68_username_nasipaddress_acctsessionid_idx ON radius.radacct_68 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_69_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_69_acctstarttime_username_idx ON radius.radacct_69 USING btree (acctstarttime, username);


--
-- Name: radacct_69_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_69_username_nasipaddress_acctsessionid_idx ON radius.radacct_69 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_6_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_6_acctstarttime_username_idx ON radius.radacct_6 USING btree (acctstarttime, username);


--
-- Name: radacct_6_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_6_username_nasipaddress_acctsessionid_idx ON radius.radacct_6 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_70_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_70_acctstarttime_username_idx ON radius.radacct_70 USING btree (acctstarttime, username);


--
-- Name: radacct_70_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_70_username_nasipaddress_acctsessionid_idx ON radius.radacct_70 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_71_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_71_acctstarttime_username_idx ON radius.radacct_71 USING btree (acctstarttime, username);


--
-- Name: radacct_71_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_71_username_nasipaddress_acctsessionid_idx ON radius.radacct_71 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_72_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_72_acctstarttime_username_idx ON radius.radacct_72 USING btree (acctstarttime, username);


--
-- Name: radacct_72_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_72_username_nasipaddress_acctsessionid_idx ON radius.radacct_72 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_73_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_73_acctstarttime_username_idx ON radius.radacct_73 USING btree (acctstarttime, username);


--
-- Name: radacct_73_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_73_username_nasipaddress_acctsessionid_idx ON radius.radacct_73 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_74_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_74_acctstarttime_username_idx ON radius.radacct_74 USING btree (acctstarttime, username);


--
-- Name: radacct_74_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_74_username_nasipaddress_acctsessionid_idx ON radius.radacct_74 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_75_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_75_acctstarttime_username_idx ON radius.radacct_75 USING btree (acctstarttime, username);


--
-- Name: radacct_75_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_75_username_nasipaddress_acctsessionid_idx ON radius.radacct_75 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_76_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_76_acctstarttime_username_idx ON radius.radacct_76 USING btree (acctstarttime, username);


--
-- Name: radacct_76_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_76_username_nasipaddress_acctsessionid_idx ON radius.radacct_76 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_77_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_77_acctstarttime_username_idx ON radius.radacct_77 USING btree (acctstarttime, username);


--
-- Name: radacct_77_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_77_username_nasipaddress_acctsessionid_idx ON radius.radacct_77 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_78_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_78_acctstarttime_username_idx ON radius.radacct_78 USING btree (acctstarttime, username);


--
-- Name: radacct_78_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_78_username_nasipaddress_acctsessionid_idx ON radius.radacct_78 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_79_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_79_acctstarttime_username_idx ON radius.radacct_79 USING btree (acctstarttime, username);


--
-- Name: radacct_79_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_79_username_nasipaddress_acctsessionid_idx ON radius.radacct_79 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_7_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_7_acctstarttime_username_idx ON radius.radacct_7 USING btree (acctstarttime, username);


--
-- Name: radacct_7_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_7_username_nasipaddress_acctsessionid_idx ON radius.radacct_7 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_80_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_80_acctstarttime_username_idx ON radius.radacct_80 USING btree (acctstarttime, username);


--
-- Name: radacct_80_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_80_username_nasipaddress_acctsessionid_idx ON radius.radacct_80 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_81_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_81_acctstarttime_username_idx ON radius.radacct_81 USING btree (acctstarttime, username);


--
-- Name: radacct_81_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_81_username_nasipaddress_acctsessionid_idx ON radius.radacct_81 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_82_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_82_acctstarttime_username_idx ON radius.radacct_82 USING btree (acctstarttime, username);


--
-- Name: radacct_82_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_82_username_nasipaddress_acctsessionid_idx ON radius.radacct_82 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_83_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_83_acctstarttime_username_idx ON radius.radacct_83 USING btree (acctstarttime, username);


--
-- Name: radacct_83_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_83_username_nasipaddress_acctsessionid_idx ON radius.radacct_83 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_84_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_84_acctstarttime_username_idx ON radius.radacct_84 USING btree (acctstarttime, username);


--
-- Name: radacct_84_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_84_username_nasipaddress_acctsessionid_idx ON radius.radacct_84 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_85_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_85_acctstarttime_username_idx ON radius.radacct_85 USING btree (acctstarttime, username);


--
-- Name: radacct_85_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_85_username_nasipaddress_acctsessionid_idx ON radius.radacct_85 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_86_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_86_acctstarttime_username_idx ON radius.radacct_86 USING btree (acctstarttime, username);


--
-- Name: radacct_86_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_86_username_nasipaddress_acctsessionid_idx ON radius.radacct_86 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_87_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_87_acctstarttime_username_idx ON radius.radacct_87 USING btree (acctstarttime, username);


--
-- Name: radacct_87_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_87_username_nasipaddress_acctsessionid_idx ON radius.radacct_87 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_88_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_88_acctstarttime_username_idx ON radius.radacct_88 USING btree (acctstarttime, username);


--
-- Name: radacct_88_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_88_username_nasipaddress_acctsessionid_idx ON radius.radacct_88 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_89_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_89_acctstarttime_username_idx ON radius.radacct_89 USING btree (acctstarttime, username);


--
-- Name: radacct_89_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_89_username_nasipaddress_acctsessionid_idx ON radius.radacct_89 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_8_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_8_acctstarttime_username_idx ON radius.radacct_8 USING btree (acctstarttime, username);


--
-- Name: radacct_8_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_8_username_nasipaddress_acctsessionid_idx ON radius.radacct_8 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_90_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_90_acctstarttime_username_idx ON radius.radacct_90 USING btree (acctstarttime, username);


--
-- Name: radacct_90_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_90_username_nasipaddress_acctsessionid_idx ON radius.radacct_90 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_91_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_91_acctstarttime_username_idx ON radius.radacct_91 USING btree (acctstarttime, username);


--
-- Name: radacct_91_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_91_username_nasipaddress_acctsessionid_idx ON radius.radacct_91 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_92_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_92_acctstarttime_username_idx ON radius.radacct_92 USING btree (acctstarttime, username);


--
-- Name: radacct_92_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_92_username_nasipaddress_acctsessionid_idx ON radius.radacct_92 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_93_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_93_acctstarttime_username_idx ON radius.radacct_93 USING btree (acctstarttime, username);


--
-- Name: radacct_93_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_93_username_nasipaddress_acctsessionid_idx ON radius.radacct_93 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_94_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_94_acctstarttime_username_idx ON radius.radacct_94 USING btree (acctstarttime, username);


--
-- Name: radacct_94_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_94_username_nasipaddress_acctsessionid_idx ON radius.radacct_94 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_95_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_95_acctstarttime_username_idx ON radius.radacct_95 USING btree (acctstarttime, username);


--
-- Name: radacct_95_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_95_username_nasipaddress_acctsessionid_idx ON radius.radacct_95 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_96_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_96_acctstarttime_username_idx ON radius.radacct_96 USING btree (acctstarttime, username);


--
-- Name: radacct_96_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_96_username_nasipaddress_acctsessionid_idx ON radius.radacct_96 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_97_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_97_acctstarttime_username_idx ON radius.radacct_97 USING btree (acctstarttime, username);


--
-- Name: radacct_97_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_97_username_nasipaddress_acctsessionid_idx ON radius.radacct_97 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_98_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_98_acctstarttime_username_idx ON radius.radacct_98 USING btree (acctstarttime, username);


--
-- Name: radacct_98_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_98_username_nasipaddress_acctsessionid_idx ON radius.radacct_98 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_99_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_99_acctstarttime_username_idx ON radius.radacct_99 USING btree (acctstarttime, username);


--
-- Name: radacct_99_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_99_username_nasipaddress_acctsessionid_idx ON radius.radacct_99 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_9_acctstarttime_username_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_9_acctstarttime_username_idx ON radius.radacct_9 USING btree (acctstarttime, username);


--
-- Name: radacct_9_username_nasipaddress_acctsessionid_idx; Type: INDEX; Schema: radius; Owner: postgres
--

CREATE INDEX radacct_9_username_nasipaddress_acctsessionid_idx ON radius.radacct_9 USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_0_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_0_acctstarttime_username_idx;


--
-- Name: radacct_0_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_0_pkey;


--
-- Name: radacct_0_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_0_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_100_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_100_acctstarttime_username_idx;


--
-- Name: radacct_100_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_100_pkey;


--
-- Name: radacct_100_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_100_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_101_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_101_acctstarttime_username_idx;


--
-- Name: radacct_101_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_101_pkey;


--
-- Name: radacct_101_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_101_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_102_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_102_acctstarttime_username_idx;


--
-- Name: radacct_102_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_102_pkey;


--
-- Name: radacct_102_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_102_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_103_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_103_acctstarttime_username_idx;


--
-- Name: radacct_103_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_103_pkey;


--
-- Name: radacct_103_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_103_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_104_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_104_acctstarttime_username_idx;


--
-- Name: radacct_104_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_104_pkey;


--
-- Name: radacct_104_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_104_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_105_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_105_acctstarttime_username_idx;


--
-- Name: radacct_105_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_105_pkey;


--
-- Name: radacct_105_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_105_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_106_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_106_acctstarttime_username_idx;


--
-- Name: radacct_106_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_106_pkey;


--
-- Name: radacct_106_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_106_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_107_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_107_acctstarttime_username_idx;


--
-- Name: radacct_107_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_107_pkey;


--
-- Name: radacct_107_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_107_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_108_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_108_acctstarttime_username_idx;


--
-- Name: radacct_108_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_108_pkey;


--
-- Name: radacct_108_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_108_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_109_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_109_acctstarttime_username_idx;


--
-- Name: radacct_109_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_109_pkey;


--
-- Name: radacct_109_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_109_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_10_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_10_acctstarttime_username_idx;


--
-- Name: radacct_10_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_10_pkey;


--
-- Name: radacct_10_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_10_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_110_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_110_acctstarttime_username_idx;


--
-- Name: radacct_110_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_110_pkey;


--
-- Name: radacct_110_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_110_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_111_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_111_acctstarttime_username_idx;


--
-- Name: radacct_111_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_111_pkey;


--
-- Name: radacct_111_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_111_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_112_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_112_acctstarttime_username_idx;


--
-- Name: radacct_112_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_112_pkey;


--
-- Name: radacct_112_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_112_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_113_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_113_acctstarttime_username_idx;


--
-- Name: radacct_113_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_113_pkey;


--
-- Name: radacct_113_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_113_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_114_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_114_acctstarttime_username_idx;


--
-- Name: radacct_114_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_114_pkey;


--
-- Name: radacct_114_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_114_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_115_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_115_acctstarttime_username_idx;


--
-- Name: radacct_115_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_115_pkey;


--
-- Name: radacct_115_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_115_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_116_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_116_acctstarttime_username_idx;


--
-- Name: radacct_116_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_116_pkey;


--
-- Name: radacct_116_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_116_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_117_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_117_acctstarttime_username_idx;


--
-- Name: radacct_117_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_117_pkey;


--
-- Name: radacct_117_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_117_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_118_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_118_acctstarttime_username_idx;


--
-- Name: radacct_118_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_118_pkey;


--
-- Name: radacct_118_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_118_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_119_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_119_acctstarttime_username_idx;


--
-- Name: radacct_119_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_119_pkey;


--
-- Name: radacct_119_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_119_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_11_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_11_acctstarttime_username_idx;


--
-- Name: radacct_11_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_11_pkey;


--
-- Name: radacct_11_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_11_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_120_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_120_acctstarttime_username_idx;


--
-- Name: radacct_120_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_120_pkey;


--
-- Name: radacct_120_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_120_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_121_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_121_acctstarttime_username_idx;


--
-- Name: radacct_121_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_121_pkey;


--
-- Name: radacct_121_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_121_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_122_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_122_acctstarttime_username_idx;


--
-- Name: radacct_122_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_122_pkey;


--
-- Name: radacct_122_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_122_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_123_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_123_acctstarttime_username_idx;


--
-- Name: radacct_123_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_123_pkey;


--
-- Name: radacct_123_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_123_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_124_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_124_acctstarttime_username_idx;


--
-- Name: radacct_124_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_124_pkey;


--
-- Name: radacct_124_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_124_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_125_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_125_acctstarttime_username_idx;


--
-- Name: radacct_125_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_125_pkey;


--
-- Name: radacct_125_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_125_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_126_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_126_acctstarttime_username_idx;


--
-- Name: radacct_126_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_126_pkey;


--
-- Name: radacct_126_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_126_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_127_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_127_acctstarttime_username_idx;


--
-- Name: radacct_127_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_127_pkey;


--
-- Name: radacct_127_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_127_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_128_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_128_acctstarttime_username_idx;


--
-- Name: radacct_128_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_128_pkey;


--
-- Name: radacct_128_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_128_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_129_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_129_acctstarttime_username_idx;


--
-- Name: radacct_129_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_129_pkey;


--
-- Name: radacct_129_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_129_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_12_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_12_acctstarttime_username_idx;


--
-- Name: radacct_12_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_12_pkey;


--
-- Name: radacct_12_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_12_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_130_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_130_acctstarttime_username_idx;


--
-- Name: radacct_130_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_130_pkey;


--
-- Name: radacct_130_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_130_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_131_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_131_acctstarttime_username_idx;


--
-- Name: radacct_131_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_131_pkey;


--
-- Name: radacct_131_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_131_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_132_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_132_acctstarttime_username_idx;


--
-- Name: radacct_132_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_132_pkey;


--
-- Name: radacct_132_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_132_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_133_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_133_acctstarttime_username_idx;


--
-- Name: radacct_133_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_133_pkey;


--
-- Name: radacct_133_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_133_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_134_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_134_acctstarttime_username_idx;


--
-- Name: radacct_134_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_134_pkey;


--
-- Name: radacct_134_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_134_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_135_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_135_acctstarttime_username_idx;


--
-- Name: radacct_135_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_135_pkey;


--
-- Name: radacct_135_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_135_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_136_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_136_acctstarttime_username_idx;


--
-- Name: radacct_136_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_136_pkey;


--
-- Name: radacct_136_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_136_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_137_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_137_acctstarttime_username_idx;


--
-- Name: radacct_137_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_137_pkey;


--
-- Name: radacct_137_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_137_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_138_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_138_acctstarttime_username_idx;


--
-- Name: radacct_138_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_138_pkey;


--
-- Name: radacct_138_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_138_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_139_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_139_acctstarttime_username_idx;


--
-- Name: radacct_139_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_139_pkey;


--
-- Name: radacct_139_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_139_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_13_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_13_acctstarttime_username_idx;


--
-- Name: radacct_13_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_13_pkey;


--
-- Name: radacct_13_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_13_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_140_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_140_acctstarttime_username_idx;


--
-- Name: radacct_140_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_140_pkey;


--
-- Name: radacct_140_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_140_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_141_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_141_acctstarttime_username_idx;


--
-- Name: radacct_141_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_141_pkey;


--
-- Name: radacct_141_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_141_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_142_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_142_acctstarttime_username_idx;


--
-- Name: radacct_142_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_142_pkey;


--
-- Name: radacct_142_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_142_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_143_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_143_acctstarttime_username_idx;


--
-- Name: radacct_143_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_143_pkey;


--
-- Name: radacct_143_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_143_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_144_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_144_acctstarttime_username_idx;


--
-- Name: radacct_144_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_144_pkey;


--
-- Name: radacct_144_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_144_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_145_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_145_acctstarttime_username_idx;


--
-- Name: radacct_145_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_145_pkey;


--
-- Name: radacct_145_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_145_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_146_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_146_acctstarttime_username_idx;


--
-- Name: radacct_146_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_146_pkey;


--
-- Name: radacct_146_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_146_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_147_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_147_acctstarttime_username_idx;


--
-- Name: radacct_147_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_147_pkey;


--
-- Name: radacct_147_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_147_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_148_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_148_acctstarttime_username_idx;


--
-- Name: radacct_148_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_148_pkey;


--
-- Name: radacct_148_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_148_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_149_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_149_acctstarttime_username_idx;


--
-- Name: radacct_149_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_149_pkey;


--
-- Name: radacct_149_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_149_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_14_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_14_acctstarttime_username_idx;


--
-- Name: radacct_14_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_14_pkey;


--
-- Name: radacct_14_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_14_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_150_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_150_acctstarttime_username_idx;


--
-- Name: radacct_150_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_150_pkey;


--
-- Name: radacct_150_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_150_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_151_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_151_acctstarttime_username_idx;


--
-- Name: radacct_151_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_151_pkey;


--
-- Name: radacct_151_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_151_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_152_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_152_acctstarttime_username_idx;


--
-- Name: radacct_152_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_152_pkey;


--
-- Name: radacct_152_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_152_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_153_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_153_acctstarttime_username_idx;


--
-- Name: radacct_153_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_153_pkey;


--
-- Name: radacct_153_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_153_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_154_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_154_acctstarttime_username_idx;


--
-- Name: radacct_154_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_154_pkey;


--
-- Name: radacct_154_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_154_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_155_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_155_acctstarttime_username_idx;


--
-- Name: radacct_155_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_155_pkey;


--
-- Name: radacct_155_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_155_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_156_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_156_acctstarttime_username_idx;


--
-- Name: radacct_156_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_156_pkey;


--
-- Name: radacct_156_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_156_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_157_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_157_acctstarttime_username_idx;


--
-- Name: radacct_157_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_157_pkey;


--
-- Name: radacct_157_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_157_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_158_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_158_acctstarttime_username_idx;


--
-- Name: radacct_158_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_158_pkey;


--
-- Name: radacct_158_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_158_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_159_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_159_acctstarttime_username_idx;


--
-- Name: radacct_159_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_159_pkey;


--
-- Name: radacct_159_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_159_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_15_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_15_acctstarttime_username_idx;


--
-- Name: radacct_15_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_15_pkey;


--
-- Name: radacct_15_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_15_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_160_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_160_acctstarttime_username_idx;


--
-- Name: radacct_160_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_160_pkey;


--
-- Name: radacct_160_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_160_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_161_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_161_acctstarttime_username_idx;


--
-- Name: radacct_161_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_161_pkey;


--
-- Name: radacct_161_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_161_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_162_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_162_acctstarttime_username_idx;


--
-- Name: radacct_162_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_162_pkey;


--
-- Name: radacct_162_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_162_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_163_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_163_acctstarttime_username_idx;


--
-- Name: radacct_163_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_163_pkey;


--
-- Name: radacct_163_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_163_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_164_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_164_acctstarttime_username_idx;


--
-- Name: radacct_164_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_164_pkey;


--
-- Name: radacct_164_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_164_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_165_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_165_acctstarttime_username_idx;


--
-- Name: radacct_165_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_165_pkey;


--
-- Name: radacct_165_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_165_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_166_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_166_acctstarttime_username_idx;


--
-- Name: radacct_166_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_166_pkey;


--
-- Name: radacct_166_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_166_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_167_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_167_acctstarttime_username_idx;


--
-- Name: radacct_167_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_167_pkey;


--
-- Name: radacct_167_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_167_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_168_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_168_acctstarttime_username_idx;


--
-- Name: radacct_168_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_168_pkey;


--
-- Name: radacct_168_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_168_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_169_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_169_acctstarttime_username_idx;


--
-- Name: radacct_169_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_169_pkey;


--
-- Name: radacct_169_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_169_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_16_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_16_acctstarttime_username_idx;


--
-- Name: radacct_16_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_16_pkey;


--
-- Name: radacct_16_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_16_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_170_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_170_acctstarttime_username_idx;


--
-- Name: radacct_170_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_170_pkey;


--
-- Name: radacct_170_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_170_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_171_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_171_acctstarttime_username_idx;


--
-- Name: radacct_171_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_171_pkey;


--
-- Name: radacct_171_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_171_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_172_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_172_acctstarttime_username_idx;


--
-- Name: radacct_172_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_172_pkey;


--
-- Name: radacct_172_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_172_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_173_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_173_acctstarttime_username_idx;


--
-- Name: radacct_173_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_173_pkey;


--
-- Name: radacct_173_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_173_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_174_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_174_acctstarttime_username_idx;


--
-- Name: radacct_174_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_174_pkey;


--
-- Name: radacct_174_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_174_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_175_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_175_acctstarttime_username_idx;


--
-- Name: radacct_175_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_175_pkey;


--
-- Name: radacct_175_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_175_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_176_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_176_acctstarttime_username_idx;


--
-- Name: radacct_176_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_176_pkey;


--
-- Name: radacct_176_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_176_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_177_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_177_acctstarttime_username_idx;


--
-- Name: radacct_177_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_177_pkey;


--
-- Name: radacct_177_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_177_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_178_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_178_acctstarttime_username_idx;


--
-- Name: radacct_178_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_178_pkey;


--
-- Name: radacct_178_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_178_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_179_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_179_acctstarttime_username_idx;


--
-- Name: radacct_179_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_179_pkey;


--
-- Name: radacct_179_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_179_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_17_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_17_acctstarttime_username_idx;


--
-- Name: radacct_17_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_17_pkey;


--
-- Name: radacct_17_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_17_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_180_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_180_acctstarttime_username_idx;


--
-- Name: radacct_180_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_180_pkey;


--
-- Name: radacct_180_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_180_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_181_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_181_acctstarttime_username_idx;


--
-- Name: radacct_181_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_181_pkey;


--
-- Name: radacct_181_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_181_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_182_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_182_acctstarttime_username_idx;


--
-- Name: radacct_182_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_182_pkey;


--
-- Name: radacct_182_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_182_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_183_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_183_acctstarttime_username_idx;


--
-- Name: radacct_183_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_183_pkey;


--
-- Name: radacct_183_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_183_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_184_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_184_acctstarttime_username_idx;


--
-- Name: radacct_184_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_184_pkey;


--
-- Name: radacct_184_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_184_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_185_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_185_acctstarttime_username_idx;


--
-- Name: radacct_185_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_185_pkey;


--
-- Name: radacct_185_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_185_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_186_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_186_acctstarttime_username_idx;


--
-- Name: radacct_186_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_186_pkey;


--
-- Name: radacct_186_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_186_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_187_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_187_acctstarttime_username_idx;


--
-- Name: radacct_187_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_187_pkey;


--
-- Name: radacct_187_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_187_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_188_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_188_acctstarttime_username_idx;


--
-- Name: radacct_188_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_188_pkey;


--
-- Name: radacct_188_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_188_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_189_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_189_acctstarttime_username_idx;


--
-- Name: radacct_189_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_189_pkey;


--
-- Name: radacct_189_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_189_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_18_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_18_acctstarttime_username_idx;


--
-- Name: radacct_18_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_18_pkey;


--
-- Name: radacct_18_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_18_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_190_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_190_acctstarttime_username_idx;


--
-- Name: radacct_190_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_190_pkey;


--
-- Name: radacct_190_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_190_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_191_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_191_acctstarttime_username_idx;


--
-- Name: radacct_191_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_191_pkey;


--
-- Name: radacct_191_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_191_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_192_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_192_acctstarttime_username_idx;


--
-- Name: radacct_192_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_192_pkey;


--
-- Name: radacct_192_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_192_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_193_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_193_acctstarttime_username_idx;


--
-- Name: radacct_193_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_193_pkey;


--
-- Name: radacct_193_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_193_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_194_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_194_acctstarttime_username_idx;


--
-- Name: radacct_194_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_194_pkey;


--
-- Name: radacct_194_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_194_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_195_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_195_acctstarttime_username_idx;


--
-- Name: radacct_195_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_195_pkey;


--
-- Name: radacct_195_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_195_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_196_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_196_acctstarttime_username_idx;


--
-- Name: radacct_196_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_196_pkey;


--
-- Name: radacct_196_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_196_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_197_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_197_acctstarttime_username_idx;


--
-- Name: radacct_197_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_197_pkey;


--
-- Name: radacct_197_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_197_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_198_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_198_acctstarttime_username_idx;


--
-- Name: radacct_198_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_198_pkey;


--
-- Name: radacct_198_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_198_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_199_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_199_acctstarttime_username_idx;


--
-- Name: radacct_199_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_199_pkey;


--
-- Name: radacct_199_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_199_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_19_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_19_acctstarttime_username_idx;


--
-- Name: radacct_19_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_19_pkey;


--
-- Name: radacct_19_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_19_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_1_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_1_acctstarttime_username_idx;


--
-- Name: radacct_1_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_1_pkey;


--
-- Name: radacct_1_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_1_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_200_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_200_acctstarttime_username_idx;


--
-- Name: radacct_200_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_200_pkey;


--
-- Name: radacct_200_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_200_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_201_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_201_acctstarttime_username_idx;


--
-- Name: radacct_201_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_201_pkey;


--
-- Name: radacct_201_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_201_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_202_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_202_acctstarttime_username_idx;


--
-- Name: radacct_202_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_202_pkey;


--
-- Name: radacct_202_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_202_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_203_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_203_acctstarttime_username_idx;


--
-- Name: radacct_203_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_203_pkey;


--
-- Name: radacct_203_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_203_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_204_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_204_acctstarttime_username_idx;


--
-- Name: radacct_204_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_204_pkey;


--
-- Name: radacct_204_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_204_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_205_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_205_acctstarttime_username_idx;


--
-- Name: radacct_205_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_205_pkey;


--
-- Name: radacct_205_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_205_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_206_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_206_acctstarttime_username_idx;


--
-- Name: radacct_206_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_206_pkey;


--
-- Name: radacct_206_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_206_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_207_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_207_acctstarttime_username_idx;


--
-- Name: radacct_207_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_207_pkey;


--
-- Name: radacct_207_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_207_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_208_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_208_acctstarttime_username_idx;


--
-- Name: radacct_208_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_208_pkey;


--
-- Name: radacct_208_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_208_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_209_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_209_acctstarttime_username_idx;


--
-- Name: radacct_209_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_209_pkey;


--
-- Name: radacct_209_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_209_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_20_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_20_acctstarttime_username_idx;


--
-- Name: radacct_20_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_20_pkey;


--
-- Name: radacct_20_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_20_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_210_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_210_acctstarttime_username_idx;


--
-- Name: radacct_210_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_210_pkey;


--
-- Name: radacct_210_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_210_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_211_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_211_acctstarttime_username_idx;


--
-- Name: radacct_211_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_211_pkey;


--
-- Name: radacct_211_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_211_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_212_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_212_acctstarttime_username_idx;


--
-- Name: radacct_212_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_212_pkey;


--
-- Name: radacct_212_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_212_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_213_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_213_acctstarttime_username_idx;


--
-- Name: radacct_213_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_213_pkey;


--
-- Name: radacct_213_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_213_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_214_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_214_acctstarttime_username_idx;


--
-- Name: radacct_214_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_214_pkey;


--
-- Name: radacct_214_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_214_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_215_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_215_acctstarttime_username_idx;


--
-- Name: radacct_215_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_215_pkey;


--
-- Name: radacct_215_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_215_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_216_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_216_acctstarttime_username_idx;


--
-- Name: radacct_216_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_216_pkey;


--
-- Name: radacct_216_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_216_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_217_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_217_acctstarttime_username_idx;


--
-- Name: radacct_217_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_217_pkey;


--
-- Name: radacct_217_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_217_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_218_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_218_acctstarttime_username_idx;


--
-- Name: radacct_218_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_218_pkey;


--
-- Name: radacct_218_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_218_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_219_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_219_acctstarttime_username_idx;


--
-- Name: radacct_219_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_219_pkey;


--
-- Name: radacct_219_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_219_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_21_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_21_acctstarttime_username_idx;


--
-- Name: radacct_21_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_21_pkey;


--
-- Name: radacct_21_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_21_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_220_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_220_acctstarttime_username_idx;


--
-- Name: radacct_220_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_220_pkey;


--
-- Name: radacct_220_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_220_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_221_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_221_acctstarttime_username_idx;


--
-- Name: radacct_221_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_221_pkey;


--
-- Name: radacct_221_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_221_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_222_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_222_acctstarttime_username_idx;


--
-- Name: radacct_222_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_222_pkey;


--
-- Name: radacct_222_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_222_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_223_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_223_acctstarttime_username_idx;


--
-- Name: radacct_223_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_223_pkey;


--
-- Name: radacct_223_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_223_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_224_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_224_acctstarttime_username_idx;


--
-- Name: radacct_224_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_224_pkey;


--
-- Name: radacct_224_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_224_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_225_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_225_acctstarttime_username_idx;


--
-- Name: radacct_225_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_225_pkey;


--
-- Name: radacct_225_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_225_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_226_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_226_acctstarttime_username_idx;


--
-- Name: radacct_226_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_226_pkey;


--
-- Name: radacct_226_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_226_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_227_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_227_acctstarttime_username_idx;


--
-- Name: radacct_227_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_227_pkey;


--
-- Name: radacct_227_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_227_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_228_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_228_acctstarttime_username_idx;


--
-- Name: radacct_228_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_228_pkey;


--
-- Name: radacct_228_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_228_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_229_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_229_acctstarttime_username_idx;


--
-- Name: radacct_229_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_229_pkey;


--
-- Name: radacct_229_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_229_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_22_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_22_acctstarttime_username_idx;


--
-- Name: radacct_22_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_22_pkey;


--
-- Name: radacct_22_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_22_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_230_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_230_acctstarttime_username_idx;


--
-- Name: radacct_230_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_230_pkey;


--
-- Name: radacct_230_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_230_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_231_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_231_acctstarttime_username_idx;


--
-- Name: radacct_231_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_231_pkey;


--
-- Name: radacct_231_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_231_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_232_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_232_acctstarttime_username_idx;


--
-- Name: radacct_232_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_232_pkey;


--
-- Name: radacct_232_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_232_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_233_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_233_acctstarttime_username_idx;


--
-- Name: radacct_233_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_233_pkey;


--
-- Name: radacct_233_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_233_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_234_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_234_acctstarttime_username_idx;


--
-- Name: radacct_234_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_234_pkey;


--
-- Name: radacct_234_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_234_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_235_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_235_acctstarttime_username_idx;


--
-- Name: radacct_235_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_235_pkey;


--
-- Name: radacct_235_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_235_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_236_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_236_acctstarttime_username_idx;


--
-- Name: radacct_236_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_236_pkey;


--
-- Name: radacct_236_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_236_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_237_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_237_acctstarttime_username_idx;


--
-- Name: radacct_237_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_237_pkey;


--
-- Name: radacct_237_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_237_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_238_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_238_acctstarttime_username_idx;


--
-- Name: radacct_238_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_238_pkey;


--
-- Name: radacct_238_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_238_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_239_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_239_acctstarttime_username_idx;


--
-- Name: radacct_239_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_239_pkey;


--
-- Name: radacct_239_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_239_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_23_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_23_acctstarttime_username_idx;


--
-- Name: radacct_23_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_23_pkey;


--
-- Name: radacct_23_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_23_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_240_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_240_acctstarttime_username_idx;


--
-- Name: radacct_240_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_240_pkey;


--
-- Name: radacct_240_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_240_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_241_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_241_acctstarttime_username_idx;


--
-- Name: radacct_241_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_241_pkey;


--
-- Name: radacct_241_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_241_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_242_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_242_acctstarttime_username_idx;


--
-- Name: radacct_242_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_242_pkey;


--
-- Name: radacct_242_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_242_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_243_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_243_acctstarttime_username_idx;


--
-- Name: radacct_243_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_243_pkey;


--
-- Name: radacct_243_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_243_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_244_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_244_acctstarttime_username_idx;


--
-- Name: radacct_244_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_244_pkey;


--
-- Name: radacct_244_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_244_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_245_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_245_acctstarttime_username_idx;


--
-- Name: radacct_245_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_245_pkey;


--
-- Name: radacct_245_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_245_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_246_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_246_acctstarttime_username_idx;


--
-- Name: radacct_246_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_246_pkey;


--
-- Name: radacct_246_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_246_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_247_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_247_acctstarttime_username_idx;


--
-- Name: radacct_247_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_247_pkey;


--
-- Name: radacct_247_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_247_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_248_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_248_acctstarttime_username_idx;


--
-- Name: radacct_248_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_248_pkey;


--
-- Name: radacct_248_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_248_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_249_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_249_acctstarttime_username_idx;


--
-- Name: radacct_249_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_249_pkey;


--
-- Name: radacct_249_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_249_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_24_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_24_acctstarttime_username_idx;


--
-- Name: radacct_24_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_24_pkey;


--
-- Name: radacct_24_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_24_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_250_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_250_acctstarttime_username_idx;


--
-- Name: radacct_250_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_250_pkey;


--
-- Name: radacct_250_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_250_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_251_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_251_acctstarttime_username_idx;


--
-- Name: radacct_251_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_251_pkey;


--
-- Name: radacct_251_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_251_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_252_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_252_acctstarttime_username_idx;


--
-- Name: radacct_252_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_252_pkey;


--
-- Name: radacct_252_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_252_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_253_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_253_acctstarttime_username_idx;


--
-- Name: radacct_253_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_253_pkey;


--
-- Name: radacct_253_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_253_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_254_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_254_acctstarttime_username_idx;


--
-- Name: radacct_254_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_254_pkey;


--
-- Name: radacct_254_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_254_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_255_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_255_acctstarttime_username_idx;


--
-- Name: radacct_255_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_255_pkey;


--
-- Name: radacct_255_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_255_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_25_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_25_acctstarttime_username_idx;


--
-- Name: radacct_25_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_25_pkey;


--
-- Name: radacct_25_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_25_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_26_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_26_acctstarttime_username_idx;


--
-- Name: radacct_26_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_26_pkey;


--
-- Name: radacct_26_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_26_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_27_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_27_acctstarttime_username_idx;


--
-- Name: radacct_27_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_27_pkey;


--
-- Name: radacct_27_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_27_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_28_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_28_acctstarttime_username_idx;


--
-- Name: radacct_28_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_28_pkey;


--
-- Name: radacct_28_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_28_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_29_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_29_acctstarttime_username_idx;


--
-- Name: radacct_29_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_29_pkey;


--
-- Name: radacct_29_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_29_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_2_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_2_acctstarttime_username_idx;


--
-- Name: radacct_2_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_2_pkey;


--
-- Name: radacct_2_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_2_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_30_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_30_acctstarttime_username_idx;


--
-- Name: radacct_30_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_30_pkey;


--
-- Name: radacct_30_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_30_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_31_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_31_acctstarttime_username_idx;


--
-- Name: radacct_31_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_31_pkey;


--
-- Name: radacct_31_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_31_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_32_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_32_acctstarttime_username_idx;


--
-- Name: radacct_32_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_32_pkey;


--
-- Name: radacct_32_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_32_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_33_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_33_acctstarttime_username_idx;


--
-- Name: radacct_33_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_33_pkey;


--
-- Name: radacct_33_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_33_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_34_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_34_acctstarttime_username_idx;


--
-- Name: radacct_34_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_34_pkey;


--
-- Name: radacct_34_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_34_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_35_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_35_acctstarttime_username_idx;


--
-- Name: radacct_35_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_35_pkey;


--
-- Name: radacct_35_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_35_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_36_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_36_acctstarttime_username_idx;


--
-- Name: radacct_36_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_36_pkey;


--
-- Name: radacct_36_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_36_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_37_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_37_acctstarttime_username_idx;


--
-- Name: radacct_37_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_37_pkey;


--
-- Name: radacct_37_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_37_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_38_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_38_acctstarttime_username_idx;


--
-- Name: radacct_38_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_38_pkey;


--
-- Name: radacct_38_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_38_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_39_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_39_acctstarttime_username_idx;


--
-- Name: radacct_39_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_39_pkey;


--
-- Name: radacct_39_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_39_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_3_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_3_acctstarttime_username_idx;


--
-- Name: radacct_3_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_3_pkey;


--
-- Name: radacct_3_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_3_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_40_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_40_acctstarttime_username_idx;


--
-- Name: radacct_40_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_40_pkey;


--
-- Name: radacct_40_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_40_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_41_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_41_acctstarttime_username_idx;


--
-- Name: radacct_41_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_41_pkey;


--
-- Name: radacct_41_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_41_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_42_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_42_acctstarttime_username_idx;


--
-- Name: radacct_42_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_42_pkey;


--
-- Name: radacct_42_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_42_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_43_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_43_acctstarttime_username_idx;


--
-- Name: radacct_43_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_43_pkey;


--
-- Name: radacct_43_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_43_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_44_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_44_acctstarttime_username_idx;


--
-- Name: radacct_44_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_44_pkey;


--
-- Name: radacct_44_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_44_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_45_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_45_acctstarttime_username_idx;


--
-- Name: radacct_45_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_45_pkey;


--
-- Name: radacct_45_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_45_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_46_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_46_acctstarttime_username_idx;


--
-- Name: radacct_46_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_46_pkey;


--
-- Name: radacct_46_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_46_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_47_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_47_acctstarttime_username_idx;


--
-- Name: radacct_47_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_47_pkey;


--
-- Name: radacct_47_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_47_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_48_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_48_acctstarttime_username_idx;


--
-- Name: radacct_48_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_48_pkey;


--
-- Name: radacct_48_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_48_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_49_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_49_acctstarttime_username_idx;


--
-- Name: radacct_49_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_49_pkey;


--
-- Name: radacct_49_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_49_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_4_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_4_acctstarttime_username_idx;


--
-- Name: radacct_4_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_4_pkey;


--
-- Name: radacct_4_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_4_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_50_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_50_acctstarttime_username_idx;


--
-- Name: radacct_50_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_50_pkey;


--
-- Name: radacct_50_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_50_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_51_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_51_acctstarttime_username_idx;


--
-- Name: radacct_51_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_51_pkey;


--
-- Name: radacct_51_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_51_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_52_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_52_acctstarttime_username_idx;


--
-- Name: radacct_52_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_52_pkey;


--
-- Name: radacct_52_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_52_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_53_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_53_acctstarttime_username_idx;


--
-- Name: radacct_53_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_53_pkey;


--
-- Name: radacct_53_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_53_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_54_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_54_acctstarttime_username_idx;


--
-- Name: radacct_54_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_54_pkey;


--
-- Name: radacct_54_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_54_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_55_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_55_acctstarttime_username_idx;


--
-- Name: radacct_55_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_55_pkey;


--
-- Name: radacct_55_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_55_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_56_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_56_acctstarttime_username_idx;


--
-- Name: radacct_56_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_56_pkey;


--
-- Name: radacct_56_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_56_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_57_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_57_acctstarttime_username_idx;


--
-- Name: radacct_57_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_57_pkey;


--
-- Name: radacct_57_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_57_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_58_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_58_acctstarttime_username_idx;


--
-- Name: radacct_58_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_58_pkey;


--
-- Name: radacct_58_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_58_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_59_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_59_acctstarttime_username_idx;


--
-- Name: radacct_59_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_59_pkey;


--
-- Name: radacct_59_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_59_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_5_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_5_acctstarttime_username_idx;


--
-- Name: radacct_5_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_5_pkey;


--
-- Name: radacct_5_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_5_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_60_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_60_acctstarttime_username_idx;


--
-- Name: radacct_60_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_60_pkey;


--
-- Name: radacct_60_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_60_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_61_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_61_acctstarttime_username_idx;


--
-- Name: radacct_61_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_61_pkey;


--
-- Name: radacct_61_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_61_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_62_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_62_acctstarttime_username_idx;


--
-- Name: radacct_62_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_62_pkey;


--
-- Name: radacct_62_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_62_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_63_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_63_acctstarttime_username_idx;


--
-- Name: radacct_63_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_63_pkey;


--
-- Name: radacct_63_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_63_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_64_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_64_acctstarttime_username_idx;


--
-- Name: radacct_64_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_64_pkey;


--
-- Name: radacct_64_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_64_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_65_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_65_acctstarttime_username_idx;


--
-- Name: radacct_65_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_65_pkey;


--
-- Name: radacct_65_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_65_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_66_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_66_acctstarttime_username_idx;


--
-- Name: radacct_66_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_66_pkey;


--
-- Name: radacct_66_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_66_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_67_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_67_acctstarttime_username_idx;


--
-- Name: radacct_67_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_67_pkey;


--
-- Name: radacct_67_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_67_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_68_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_68_acctstarttime_username_idx;


--
-- Name: radacct_68_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_68_pkey;


--
-- Name: radacct_68_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_68_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_69_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_69_acctstarttime_username_idx;


--
-- Name: radacct_69_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_69_pkey;


--
-- Name: radacct_69_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_69_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_6_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_6_acctstarttime_username_idx;


--
-- Name: radacct_6_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_6_pkey;


--
-- Name: radacct_6_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_6_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_70_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_70_acctstarttime_username_idx;


--
-- Name: radacct_70_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_70_pkey;


--
-- Name: radacct_70_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_70_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_71_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_71_acctstarttime_username_idx;


--
-- Name: radacct_71_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_71_pkey;


--
-- Name: radacct_71_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_71_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_72_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_72_acctstarttime_username_idx;


--
-- Name: radacct_72_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_72_pkey;


--
-- Name: radacct_72_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_72_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_73_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_73_acctstarttime_username_idx;


--
-- Name: radacct_73_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_73_pkey;


--
-- Name: radacct_73_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_73_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_74_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_74_acctstarttime_username_idx;


--
-- Name: radacct_74_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_74_pkey;


--
-- Name: radacct_74_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_74_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_75_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_75_acctstarttime_username_idx;


--
-- Name: radacct_75_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_75_pkey;


--
-- Name: radacct_75_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_75_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_76_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_76_acctstarttime_username_idx;


--
-- Name: radacct_76_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_76_pkey;


--
-- Name: radacct_76_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_76_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_77_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_77_acctstarttime_username_idx;


--
-- Name: radacct_77_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_77_pkey;


--
-- Name: radacct_77_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_77_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_78_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_78_acctstarttime_username_idx;


--
-- Name: radacct_78_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_78_pkey;


--
-- Name: radacct_78_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_78_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_79_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_79_acctstarttime_username_idx;


--
-- Name: radacct_79_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_79_pkey;


--
-- Name: radacct_79_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_79_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_7_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_7_acctstarttime_username_idx;


--
-- Name: radacct_7_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_7_pkey;


--
-- Name: radacct_7_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_7_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_80_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_80_acctstarttime_username_idx;


--
-- Name: radacct_80_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_80_pkey;


--
-- Name: radacct_80_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_80_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_81_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_81_acctstarttime_username_idx;


--
-- Name: radacct_81_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_81_pkey;


--
-- Name: radacct_81_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_81_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_82_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_82_acctstarttime_username_idx;


--
-- Name: radacct_82_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_82_pkey;


--
-- Name: radacct_82_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_82_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_83_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_83_acctstarttime_username_idx;


--
-- Name: radacct_83_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_83_pkey;


--
-- Name: radacct_83_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_83_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_84_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_84_acctstarttime_username_idx;


--
-- Name: radacct_84_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_84_pkey;


--
-- Name: radacct_84_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_84_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_85_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_85_acctstarttime_username_idx;


--
-- Name: radacct_85_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_85_pkey;


--
-- Name: radacct_85_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_85_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_86_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_86_acctstarttime_username_idx;


--
-- Name: radacct_86_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_86_pkey;


--
-- Name: radacct_86_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_86_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_87_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_87_acctstarttime_username_idx;


--
-- Name: radacct_87_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_87_pkey;


--
-- Name: radacct_87_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_87_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_88_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_88_acctstarttime_username_idx;


--
-- Name: radacct_88_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_88_pkey;


--
-- Name: radacct_88_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_88_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_89_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_89_acctstarttime_username_idx;


--
-- Name: radacct_89_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_89_pkey;


--
-- Name: radacct_89_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_89_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_8_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_8_acctstarttime_username_idx;


--
-- Name: radacct_8_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_8_pkey;


--
-- Name: radacct_8_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_8_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_90_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_90_acctstarttime_username_idx;


--
-- Name: radacct_90_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_90_pkey;


--
-- Name: radacct_90_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_90_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_91_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_91_acctstarttime_username_idx;


--
-- Name: radacct_91_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_91_pkey;


--
-- Name: radacct_91_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_91_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_92_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_92_acctstarttime_username_idx;


--
-- Name: radacct_92_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_92_pkey;


--
-- Name: radacct_92_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_92_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_93_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_93_acctstarttime_username_idx;


--
-- Name: radacct_93_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_93_pkey;


--
-- Name: radacct_93_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_93_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_94_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_94_acctstarttime_username_idx;


--
-- Name: radacct_94_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_94_pkey;


--
-- Name: radacct_94_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_94_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_95_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_95_acctstarttime_username_idx;


--
-- Name: radacct_95_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_95_pkey;


--
-- Name: radacct_95_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_95_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_96_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_96_acctstarttime_username_idx;


--
-- Name: radacct_96_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_96_pkey;


--
-- Name: radacct_96_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_96_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_97_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_97_acctstarttime_username_idx;


--
-- Name: radacct_97_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_97_pkey;


--
-- Name: radacct_97_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_97_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_98_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_98_acctstarttime_username_idx;


--
-- Name: radacct_98_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_98_pkey;


--
-- Name: radacct_98_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_98_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_99_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_99_acctstarttime_username_idx;


--
-- Name: radacct_99_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_99_pkey;


--
-- Name: radacct_99_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_99_username_nasipaddress_acctsessionid_idx;


--
-- Name: radacct_9_acctstarttime_username_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_start_user_idx ATTACH PARTITION radius.radacct_9_acctstarttime_username_idx;


--
-- Name: radacct_9_pkey; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_pkey ATTACH PARTITION radius.radacct_9_pkey;


--
-- Name: radacct_9_username_nasipaddress_acctsessionid_idx; Type: INDEX ATTACH; Schema: radius; Owner: -
--

ALTER INDEX radius.radacct_active_user_idx ATTACH PARTITION radius.radacct_9_username_nasipaddress_acctsessionid_idx;


--
-- Name: TABLE radcheck; Type: ACL; Schema: radius; Owner: postgres
--

GRANT SELECT ON TABLE radius.radcheck TO radius;


--
-- Name: TABLE _radius_mac_address; Type: ACL; Schema: radius; Owner: postgres
--

GRANT SELECT ON TABLE radius._radius_mac_address TO radius;


--
-- Name: TABLE _radius_machine_account; Type: ACL; Schema: radius; Owner: postgres
--

GRANT SELECT ON TABLE radius._radius_machine_account TO radius;


--
-- Name: TABLE _radius_switch_port_auth; Type: ACL; Schema: radius; Owner: postgres
--

GRANT SELECT ON TABLE radius._radius_switch_port_auth TO radius;


--
-- Name: TABLE additional_mac_addresses; Type: ACL; Schema: radius; Owner: radius
--

GRANT ALL ON TABLE radius.additional_mac_addresses TO postgres;


--
-- Name: TABLE radpostauth; Type: ACL; Schema: radius; Owner: radius
--

GRANT ALL ON TABLE radius.radpostauth TO postgres;


--
-- Name: TABLE radusergroup; Type: ACL; Schema: radius; Owner: radius
--

GRANT ALL ON TABLE radius.radusergroup TO postgres;


--
-- PostgreSQL database dump complete
--
