CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent AS
 SELECT person.id AS person_id,
    person.crsid AS person_crsid,
    person.email_address AS person_email,
    (((COALESCE(person.known_as, person.first_names))::text || ' '::text) || (person.surname)::text) AS person_name,
    person.surname AS person_surname,
    leaving_date_estimate.leaving_date,
    leavers_form_overview_.id AS leavers_form_id,
    leavers_form_overview_.form_exists AS leavers_form_exists,
    leavers_form_overview_.last_updated AS leavers_form_last_updated
   FROM ((public.person
     LEFT JOIN ( SELECT leavers_form_overview.id,
            true AS form_exists,
            leavers_form_overview.last_updated,
            leavers_form_overview.person
           FROM leavers_form.leavers_form_overview
          WHERE (leavers_form_overview.deactivated IS NULL)) leavers_form_overview_ ON ((leavers_form_overview_.person = person.id)))
     JOIN leavers_form.leaving_date_estimate ON ((leaving_date_estimate.person_id = person.id)))
  WHERE ((person.leaving_date > (CURRENT_DATE - (concat(31, ' DAYS'))::interval)) AND (person.leaving_date < (CURRENT_DATE + (concat(31, ' DAYS'))::interval)))
  ORDER BY person.surname, (((COALESCE(person.known_as, person.first_names))::text || ' '::text) || (person.surname)::text);


ALTER TABLE leavers_form.leaver_soon_or_recent OWNER TO dev;

GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent TO onlineleaversform;
