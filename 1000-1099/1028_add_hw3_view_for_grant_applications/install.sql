SET ROLE dev;

CREATE VIEW hotwire3."10_View/Grants/_application_award_subview" AS
WITH sorted_awards AS (
SELECT
    award.id,
    award.application_id AS _application_id,
    holder.holder,
    funder.funder,
    CASE
        WHEN start_date > current_date THEN 'Future'::varchar
        WHEN end_date >= current_date THEN 'Current'::varchar
        WHEN end_date < current_date THEN 'Past'::varchar
        ELSE 'Unknown'::varchar
    END AS status,
    award.project_title,
    award.start_date,
    award.end_date,
    award.project_number,
    award.award_number
FROM grants.award
LEFT JOIN grants.holder ON award.grant_holder_id = holder.id
LEFT JOIN grants.funder ON award.funder_id = funder.id
ORDER BY award_number DESC NULLS LAST
)
SELECT * FROM sorted_awards
;

ALTER VIEW hotwire3."10_View/Grants/_application_award_subview" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/_application_award_subview" TO grants_administration,grants_ro;

CREATE VIEW hotwire3."10_View/Grants/Applications" AS
WITH applications_sorted AS (
SELECT
    id,
    application_date,
    x5_number,
    award_number,
    grant_holder_id,
    co_investigators,
    funder_id,
    project_title,
    start_date,
    end_date,
--    _grant_sought as ro_fm_grant_sought, -- checked and all match the ro_grant_sought column
    status_id as application_status_id,
    project_number,
    grant_awarded,
    notes,
    (consumables + contingency + equipment + estates_costs + exceptional_items + facilities + indirect_costs + infrastructure_tech + overheads + pi_costs + pooled_labour + staff + travel + univ_contribution ) as ro_grant_sought,
    staff,
    pooled_labour,
    travel,
    equipment,
    exceptional_items AS exceptional,
    consumables AS other_costs,
    facilities,
    overheads,
    estates_costs,
    indirect_costs,
    pi_costs AS "PI_costs",
    infrastructure_tech,
    univ_contribution,
    contingency,
    _to_hwsubviewb('10_View/Grants/_application_award_subview','_application_id','10_View/Grants/Awards','id','id') AS linked_award
FROM grants.application
ORDER BY application_date DESC NULLS LAST
)
SELECT * FROM applications_sorted
;

ALTER TABLE hotwire3."10_View/Grants/Applications" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Grants/Applications" TO grants_administration;
GRANT SELECT ON hotwire3."10_View/Grants/Applications" TO grants_administration,grants_ro;

CREATE FUNCTION hotwire3.grants_application_upd() RETURNS TRIGGER AS $$
DECLARE
    new_id BIGINT;
    pending_id BIGINT;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        SELECT id FROM grants.application_status WHERE status = 'Pending' INTO pending_id;
        INSERT INTO grants.application (
            application_date,
            grant_holder_id,
            funder_id,
            project_title,
            start_date,
            end_date,
            status_id,
            project_number,
            grant_awarded,
            notes,
            x5_number,
            award_number,
            co_investigators,
            staff,
            pooled_labour,
            travel,
            equipment,
            exceptional_items,
            consumables,
            facilities,
            overheads,
            estates_costs,
            indirect_costs,
            pi_costs,
            infrastructure_tech,
            univ_contribution,
            contingency
        ) VALUES (
            NEW.application_date,
            NEW.grant_holder_id,
            NEW.funder_id,
            NEW.project_title,
            NEW.start_date,
            NEW.end_date,
            COALESCE(NEW.application_status_id,pending_id),
            NEW.project_number,
            NEW.grant_awarded,
            NEW.notes,
            NEW.x5_number,
            NEW.award_number,
            NEW.co_investigators,
            COALESCE(NEW.staff,0),
            COALESCE(NEW.pooled_labour,0),
            COALESCE(NEW.travel,0),
            COALESCE(NEW.equipment,0),
            COALESCE(NEW.exceptional,0),
            COALESCE(NEW.other_costs,0),
            COALESCE(NEW.facilities,0),
            COALESCE(NEW.overheads,0),
            COALESCE(NEW.estates_costs,0),
            COALESCE(NEW.indirect_costs,0),
            COALESCE(NEW."PI_costs",0),
            COALESCE(NEW.infrastructure_tech,0),
            COALESCE(NEW.univ_contribution,0),
            COALESCE(NEW.contingency,0)
            ) RETURNING id INTO new_id;
        NEW.id := new_id;
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN
        UPDATE grants.application SET
            application_date = NEW.application_date,
            grant_holder_id = NEW.grant_holder_id,
            funder_id = NEW.funder_id,
            project_title = NEW.project_title,
            start_date = NEW.start_date,
            end_date = NEW.end_date,
            status_id = NEW.application_status_id,
            project_number = NEW.project_number,
            grant_awarded = NEW.grant_awarded,
            notes = NEW.notes,
            x5_number = NEW.x5_number,
            award_number = NEW.award_number,
            co_investigators = NEW.co_investigators,
            staff = NEW.staff,
            pooled_labour = NEW.pooled_labour,
            travel = NEW.travel,
            equipment = NEW.equipment,
            exceptional_items = NEW.exceptional,
            consumables = NEW.other_costs,
            facilities = NEW.facilities,
            overheads = NEW.overheads,
            estates_costs = NEW.estates_costs,
            indirect_costs = NEW.indirect_costs,
            pi_costs = NEW."PI_costs",
            infrastructure_tech = NEW.infrastructure_tech,
            univ_contribution = NEW.univ_contribution,
            contingency = NEW.contingency
        WHERE id = OLD.id;
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        DELETE FROM grants.application WHERE id = OLD.id;
        RETURN NULL;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.grants_application_upd() OWNER TO dev;
CREATE TRIGGER grants_application_trig INSTEAD OF INSERT OR UPDATE OR DELETE ON hotwire3."10_View/Grants/Applications" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_application_upd();
