INSERT INTO it_access.role_type(role_type, description, weight, show_on_registration, time_limit)
    VALUES ('Database-Admitto reconciliation', 'For reconciling historical Admitto expiry dates with database dates', 10, 'f', NULL);

INSERT INTO it_access.mm_role_type_account_type ( role_type_id, account_type_id)
SELECT role_type_id, account_type_id FROM it_access.role_type, it_access.account_type where role_type = 'Database-Admitto reconciliation';
