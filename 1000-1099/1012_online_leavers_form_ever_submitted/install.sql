DROP VIEW leavers_form.leaver_soon_or_recent_submitted;

CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent_submitted AS
  SELECT
    leaver_soon_or_recent.person_id,
    leaver_soon_or_recent.person_crsid,
    leaver_soon_or_recent.person_email,
    leaver_soon_or_recent.person_name,
    leaver_soon_or_recent.person_surname,
    leaver_soon_or_recent.leaving_date,
    leaver_soon_or_recent.leavers_form_id,
    COALESCE(leaver_soon_or_recent.leavers_form_exists, FALSE) AS leavers_form_exists,
    leaver_soon_or_recent.leavers_form_last_updated,
    EXISTS (SELECT 1 FROM leavers_form.leavers_form_submission WHERE leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id) AS leavers_form_ever_submitted,
    EXISTS (SELECT 1 FROM leavers_form.leavers_form_submission WHERE leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id AND leavers_form_submission.submitted_when >= leaver_soon_or_recent.leavers_form_last_updated) AS leavers_form_latest_updates_submitted
  FROM leavers_form.leaver_soon_or_recent
  ORDER BY person_surname, person_name ASC;

ALTER TABLE leavers_form.leaver_soon_or_recent_submitted
  OWNER TO dev;
GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent_submitted TO dev;
GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;
