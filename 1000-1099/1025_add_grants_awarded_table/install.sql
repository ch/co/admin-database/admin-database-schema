SET ROLE dev;

-- similar to but not the same as a RIG
CREATE TABLE grants.research_area (
    id bigint primary key generated always as identity,
    research_area varchar unique not null,
    research_code varchar unique not null
);
GRANT SELECT,INSERT,UPDATE,DELETE ON grants.research_area TO grants_administration;
GRANT SELECT ON grants.research_area TO grants_ro;
CREATE TRIGGER grants_research_area_audit AFTER INSERT OR DELETE OR UPDATE ON grants.research_area FOR EACH ROW EXECUTE FUNCTION audit_generic();



CREATE TABLE grants.award (
    id bigint primary key generated always as identity,
    application_id bigint unique references grants.application (id) on delete set null, -- seems nullable, eg grant MBAG/771 not in applications
    grant_holder_id bigint references grants.holder (id), -- points to whatever we got from filemaker
    person_id bigint references public.person (id), -- the real person, which may not be identifiable from Filemaker alone, hence currently nullable
    funder_id bigint references grants.funder (id),
    project_title text,
    award_number bigint,
    project_number varchar, -- not null? but what happens on import
    end_date date,
    co_investigators varchar,
    consumables numeric(20,2) not null default 0,
    contingency numeric(20,2) not null default 0,
    _duration_mths numeric, -- for checking the import, should be calculated from end_date - start_date
    equipment numeric(20,2) not null default 0,
    estates_costs numeric(20,2) not null default 0,
    exceptional numeric(20,2) not null default 0,
    facilities numeric(20,2) not null default 0,
    _grant_value numeric(20,2), -- for checking
    indirect_costs numeric(20,2) not null default 0,
    infrastructure_tech numeric(20,2) not null default 0,
    _months_remaining numeric, -- for checking import
    notes text,
    overheads numeric(20,2) not null default 0,
    pi_costs numeric(20,2) not null default 0,
    pooled_labour numeric(20,2) not null default 0,
    _project_status varchar, -- for checking import from Filemaker, should be calculated from start/end date
    research_area_id bigint references grants.research_area (id), -- this should refer to RIG but there's also 'student training'
    _research_code varchar, -- this column is on grants.research_area, used for checking
    _sponsor_code varchar, -- this column is on grants.funder, used for checking
    funder_ref varchar,
    _sponsor_type varchar, -- see grants.funder, only here for import checks
    staff numeric(20,2) not null default 0,
    start_date date,
    travel numeric(20,2) not null default 0,
    univ_contribution numeric(20,2) not null default 0
);

GRANT SELECT,INSERT,UPDATE,DELETE ON grants.award TO grants_administration;
GRANT SELECT ON grants.award TO grants_ro;
CREATE TRIGGER grants_award_audit AFTER INSERT OR DELETE OR UPDATE ON grants.award FOR EACH ROW EXECUTE FUNCTION audit_generic();
