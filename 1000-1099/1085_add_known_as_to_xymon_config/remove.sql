DROP VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg";

DROP FUNCTION hotwire3.xymon_clients_cfg_trig();

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg"
 AS
 SELECT hobbit_clients_config_stanza.id,
    hobbit_clients_config_stanza.system_image_id,
    hobbit_clients_config_stanza.handle,
    hobbit_clients_config_stanza.rules::text AS rules,
    hobbit_clients_config_stanza.settings::text AS settings
   FROM hobbit_clients_config_stanza;

ALTER TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO dev;

-- Rule: "hotwire3_10_View/Computers/Xymon_Clients_Cfg_del" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg"

-- DROP Rule "hotwire3_10_View/Computers/Xymon_Clients_Cfg_del" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg";

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_del" AS
    ON DELETE TO hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    DO INSTEAD
(DELETE FROM hobbit_clients_config_stanza
  WHERE (hobbit_clients_config_stanza.id = old.id));

-- Rule: "hotwire3_10_View/Computers/Xymon_Clients_Cfg_ins" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg"

-- DROP Rule "hotwire3_10_View/Computers/Xymon_Clients_Cfg_ins" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg";

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_ins" AS
    ON INSERT TO hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    DO INSTEAD
(INSERT INTO hobbit_clients_config_stanza (rules, settings, handle, system_image_id)
  VALUES ((new.rules)::character varying, (new.settings)::character varying, new.handle, new.system_image_id)
  RETURNING hobbit_clients_config_stanza.id,
    hobbit_clients_config_stanza.system_image_id,
    hobbit_clients_config_stanza.handle,
    (hobbit_clients_config_stanza.rules)::text AS rules,
    (hobbit_clients_config_stanza.settings)::text AS settings);

-- Rule: "hotwire3_10_View/Computers/Xymon_Clients_Cfg_upd" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg"

-- DROP Rule "hotwire3_10_View/Computers/Xymon_Clients_Cfg_upd" ON hotwire3."10_View/Computers/Xymon_Clients_Cfg";

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_upd" AS
    ON UPDATE TO hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    DO INSTEAD
(UPDATE hobbit_clients_config_stanza SET rules = (new.rules)::character varying, settings = (new.settings)::character varying, handle = new.handle, system_image_id = new.system_image_id
  WHERE (hobbit_clients_config_stanza.id = old.id));

