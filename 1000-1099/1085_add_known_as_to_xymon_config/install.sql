-- View: hotwire3."10_View/Computers/Xymon_Clients_Cfg"

DROP VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Xymon_Clients_Cfg"
 AS
 SELECT hobbit_clients_config_stanza.id,
    hobbit_clients_config_stanza.system_image_id,
    system_image.friendly_name AS known_to_xymon_as,
    hobbit_clients_config_stanza.handle,
    hobbit_clients_config_stanza.rules::text AS rules,
    hobbit_clients_config_stanza.settings::text AS settings
   FROM hobbit_clients_config_stanza
   LEFT JOIN system_image ON system_image_id=system_image.id;

ALTER TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    OWNER TO dev;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Xymon_Clients_Cfg" TO dev;

CREATE OR REPLACE RULE "hotwire3_10_View/Computers/Xymon_Clients_Cfg_del" AS
    ON DELETE TO hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    DO INSTEAD
(DELETE FROM hobbit_clients_config_stanza
  WHERE (hobbit_clients_config_stanza.id = old.id));

CREATE FUNCTION hotwire3.xymon_clients_cfg_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$
begin
    if NEW.id is not null then
        UPDATE hobbit_clients_config_stanza SET
            system_image_id=NEW.system_image_id,
            handle=NEW.handle,
            rules=NEW.rules,
            settings=NEW.settings
        WHERE id=NEW.id;

    else
	NEW.id:=nextval('hobbit_clients_config_stanza_id_seq');
        INSERT INTO hobbit_clients_config_stanza (
	    id,
            system_image_id,
            handle,
            rules,
            settings
        ) VALUES (
	    NEW.id,
            NEW.system_image_id,
            NEW.handle,
            NEW.rules,
            NEW.settings
        );
    end if;

    if NEW.system_image_id is not null then
        UPDATE system_image SET
        friendly_name=NEW.known_to_xymon_as
        WHERE system_image.id=NEW.system_image_id;
    end if;

    RETURN NEW;
end;
$BODY$;

ALTER FUNCTION hotwire3.xymon_clients_cfg_trig()
    OWNER TO dev;

CREATE TRIGGER "hotwire3_10_View/Computers_Xymon_Clients_cfg_trig" 
    INSTEAD OF INSERT OR UPDATE
    ON hotwire3."10_View/Computers/Xymon_Clients_Cfg"
    FOR EACH ROW
    EXECUTE PROCEDURE hotwire3.xymon_clients_cfg_trig();

