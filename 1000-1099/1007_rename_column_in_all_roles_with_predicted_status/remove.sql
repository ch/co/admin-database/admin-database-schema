CREATE OR REPLACE VIEW public._all_roles_with_predicted_status
 AS
 SELECT role_data.person_id,
    role_data.start_date,
    role_data.intended_end_date,
    role_data.estimated_leaving_date,
    role_data.end_date,
    role_data.funding_end_date,
    role_data.supervisor_id,
    role_data.co_supervisor_id,
    role_data.mentor_id,
    role_data.post_category_id,
    role_data.post_category,
    role_predicted_status(role_data.start_date, role_data.end_date, role_data.intended_end_date, role_data.funding_end_date, role_data.force_role_status_to_past, role_data.role_tablename)::character varying(10) AS status,
    role_data.funding,
    role_data.fees_funding,
    role_data.research_grant_number,
    role_data.paid_by_university,
    role_data.job_title,
    role_data.role_id,
    role_data.role_tablename,
    role_data.role_target_viewname,
    COALESCE((role_data.start_date - '1900-01-01'::date) * 10000 + COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, 'now'::text::date) - role_data.start_date, 0), 0) AS weight,
    COALESCE(role_data.start_date, '1900-01-01'::date) AS fixed_start_date,
    COALESCE(COALESCE(COALESCE(role_data.end_date, role_data.intended_end_date, now()::date) - role_data.start_date, 0), 0) AS length_of_role
   FROM ( SELECT visitorship.person_id,
            visitorship.start_date,
            visitorship.intended_end_date,
            visitorship.intended_end_date AS estimated_leaving_date,
            visitorship.end_date,
            NULL::date AS funding_end_date,
            visitorship.host_person_id AS supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'v-'::text || visitorship.visitor_type_id::text AS post_category_id,
            visitor_type_hid.visitor_type_hid AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            NULL::character varying(120) AS job_title,
            visitorship.id AS role_id,
            visitorship.force_role_status_to_past,
            'visitorship'::text AS role_tablename,
            '10_View/Roles/Visitors'::text AS role_target_viewname
           FROM visitorship
             LEFT JOIN visitor_type_hid USING (visitor_type_id)
        UNION
         SELECT part_iii_studentship.person_id,
            part_iii_studentship.start_date,
            part_iii_studentship.intended_end_date,
            part_iii_studentship.intended_end_date AS estimated_leaving_date,
            part_iii_studentship.end_date,
            NULL::date AS funding_end_date,
            part_iii_studentship.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'p3-1'::text AS post_category_id,
            'Part III'::character varying(80) AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            NULL::character varying(120) AS job_title,
            part_iii_studentship.id AS role_id,
            part_iii_studentship.force_role_status_to_past,
            'part_iii_studentship'::text AS role_tablename,
            '10_View/Roles/Part_III_Students'::text AS role_target_viewname
           FROM part_iii_studentship
        UNION
         SELECT erasmus_socrates_studentship.person_id,
            erasmus_socrates_studentship.start_date,
            erasmus_socrates_studentship.intended_end_date,
            erasmus_socrates_studentship.intended_end_date AS estimated_leaving_date,
            erasmus_socrates_studentship.end_date,
            NULL::date AS funding_end_date,
            erasmus_socrates_studentship.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            NULL::bigint AS mentor_id,
            'e-'::text || erasmus_socrates_studentship.erasmus_type_id AS post_category_id,
            erasmus_type_hid.erasmus_type_hid AS post_category,
            NULL::character varying(500) AS funding,
            NULL::character varying(500) AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            NULL::boolean AS paid_by_university,
            NULL::character varying(120) AS job_title,
            erasmus_socrates_studentship.id AS role_id,
            erasmus_socrates_studentship.force_role_status_to_past,
            'erasmus_socrates_studentship'::text AS role_tablename,
            '10_View/Roles/Erasmus_Students'::text AS role_target_viewname
           FROM erasmus_socrates_studentship
             JOIN erasmus_type_hid USING (erasmus_type_id)
        UNION
         SELECT post_history.person_id,
            post_history.start_date,
            post_history.intended_end_date,
            LEAST(post_history.end_date, post_history.intended_end_date, post_history.funding_end_date) AS estimated_leaving_date,
            post_history.end_date,
            post_history.funding_end_date,
            post_history.supervisor_id,
            NULL::bigint AS co_supervisor_id,
            post_history.mentor_id,
            'sc-'::text || post_history.staff_category_id::text AS post_category_id,
            staff_category.category AS post_category,
            post_history.filemaker_funding AS funding,
            NULL::character varying(500) AS fees_funding,
            post_history.research_grant_number,
            post_history.paid_by_university,
            post_history.job_title,
            post_history.id AS role_id,
            post_history.force_role_status_to_past,
            'post_history'::text AS role_tablename,
            '10_View/Roles/Post_History'::text AS role_target_viewname
           FROM post_history
             LEFT JOIN staff_category ON post_history.staff_category_id = staff_category.id
        UNION
         SELECT postgraduate_studentship.person_id,
            postgraduate_studentship.start_date,
            postgraduate_studentship.intended_end_date,
            COALESCE(postgraduate_studentship.end_date, postgraduate_studentship.intended_end_date) AS estimated_leaving_date,
            postgraduate_studentship.end_date,
            postgraduate_studentship.funding_end_date,
            postgraduate_studentship.first_supervisor_id AS supervisor_id,
            postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
            postgraduate_studentship.first_mentor_id AS mentor_id,
            'pg-'::text || postgraduate_studentship.postgraduate_studentship_type_id::text AS post_category_id,
            postgraduate_studentship_type.name AS post_category,
            postgraduate_studentship.filemaker_funding AS funding,
            postgraduate_studentship.filemaker_fees_funding AS fees_funding,
            NULL::character varying(500) AS research_grant_number,
            postgraduate_studentship.paid_through_payroll AS paid_by_university,
            NULL::character varying(120) AS job_title,
            postgraduate_studentship.id AS role_id,
            postgraduate_studentship.force_role_status_to_past,
            'postgraduate_studentship'::text AS role_tablename,
            '10_View/Roles/Postgrad_Students'::text AS role_target_viewname
           FROM postgraduate_studentship
             LEFT JOIN postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
             LEFT JOIN person ON person.id = postgraduate_studentship.person_id) role_data;

ALTER TABLE public._all_roles_with_predicted_status
    OWNER TO dev;

GRANT ALL ON TABLE public._all_roles_with_predicted_status TO dev;

CREATE OR REPLACE VIEW apps.user_accounts_to_create_in_ad_v2
 AS
 SELECT person.id,
    person.crsid,
    btrim(COALESCE(person.known_as, person.first_names)::text)::character varying(32) AS first_names,
    btrim(person.surname::text)::character varying(32) AS surname,
    btrim(person.email_address::text)::character varying(48) AS email_address,
    max(COALESCE(research_group.active_directory_container, research_group.name)::text) AS rg
   FROM person
     LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     JOIN _physical_status_v3 physical_status ON person.id = physical_status.person_id
     LEFT JOIN _all_roles_with_predicted_status all_roles ON person.id = all_roles.person_id
  WHERE physical_status.status_id::text = 'Current'::text AND all_roles.post_category::text <> 'Visitor (Commercial)'::text AND person.crsid IS NOT NULL AND person.email_address::text ~~ '%@%'::text AND all_roles.status::text = 'Current'::text
  GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address;

CREATE OR REPLACE VIEW apps.bouncing_admitto_accounts
 AS
 SELECT pt.long_title AS ptitle,
    p.surname AS psurname,
    COALESCE(p.known_as, p.first_names) AS pfirst,
    p.email_address AS pemail,
    p.crsid,
    ps.status_id AS pstatus,
        CASE
            WHEN eligible_for_account.crsid IS NOT NULL THEN true
            ELSE false
        END AS automatically_eligible_for_account,
    ts.salutation_title AS stitle,
    s.email_address AS semail,
    s.surname AS ssurname
   FROM person p
     LEFT JOIN LATERAL ( SELECT user_accounts_to_create_in_ad_v2.id,
            user_accounts_to_create_in_ad_v2.crsid,
            user_accounts_to_create_in_ad_v2.first_names,
            user_accounts_to_create_in_ad_v2.surname,
            user_accounts_to_create_in_ad_v2.email_address,
            user_accounts_to_create_in_ad_v2.rg
           FROM apps.user_accounts_to_create_in_ad_v2
          WHERE p.crsid::text = user_accounts_to_create_in_ad_v2.crsid::text) eligible_for_account ON true
     LEFT JOIN title_hid pt ON p.title_id = pt.title_id
     JOIN _latest_role_v12 r ON r.person_id = p.id
     JOIN _physical_status_v3 ps ON p.id = ps.person_id
     JOIN person s ON r.supervisor_id = s.id
     LEFT JOIN title_hid ts ON ts.title_id = s.title_id;

CREATE OR REPLACE VIEW apps.user_accounts_to_disable_v2
 AS
 SELECT person.crsid,
    person.surname,
    person.first_names,
    latest_post_category.post_category,
    COALESCE(person.leaving_date, past_roles.leaving_date, CURRENT_DATE - 42) AS leaving_date,
    ( SELECT min(all_future_start_dates.start_date) AS min_start
           FROM ( SELECT all_roles.start_date
                   FROM _all_roles_with_predicted_status all_roles
                  WHERE all_roles.person_id = person.id AND all_roles.post_category_id <> 'v-17'::text AND all_roles.start_date > ('now'::text::date - '1 mon'::interval)) all_future_start_dates) AS next_role_start_date
   FROM person
     JOIN _physical_status_v3 ps ON person.id = ps.person_id
     LEFT JOIN ( SELECT role.person_id,
            role.role_id,
            role.status
           FROM _all_roles_with_predicted_status role
          WHERE role.status::text = 'Current'::text AND role.post_category_id <> 'v-17'::text) current_roles ON person.id = current_roles.person_id
     LEFT JOIN ( SELECT role.person_id,
            max(LEAST(role.end_date, role.estimated_leaving_date)) AS leaving_date
           FROM _all_roles_with_predicted_status role
          WHERE role.status::text = 'Past'::text AND role.post_category_id <> 'v-17'::text
          GROUP BY role.person_id) past_roles ON person.id = past_roles.person_id
     LEFT JOIN LATERAL ( SELECT all_roles.post_category
           FROM _all_roles_with_predicted_status all_roles
          WHERE all_roles.person_id = person.id
          ORDER BY all_roles.start_date
         LIMIT 1) latest_post_category ON true
  WHERE person.is_spri IS NOT TRUE AND (current_roles.role_id IS NULL OR ps.status_id::text = 'Past'::text);

CREATE OR REPLACE VIEW leavers_form.role
 AS
 SELECT all_roles.role_id,
    all_roles.role_tablename AS role_type,
    person.person_id,
    person.person_crsid,
    person.person_email,
    person.person_name,
    person.person_surname,
    all_roles.start_date,
    all_roles.end_date,
    all_roles.intended_end_date,
    all_roles.estimated_leaving_date,
    btrim(all_roles.job_title::text) AS job_title,
    all_roles.post_category_id,
    all_roles.post_category,
    all_roles.status,
    line_manager.line_manager_id,
    line_manager.line_manager_crsid,
    line_manager.line_manager_email,
    line_manager.line_manager_name,
    post_history_.is_floor_technician,
    post_history_.is_online_leaving_form_superuser
   FROM _all_roles_with_predicted_status all_roles
     LEFT JOIN ( SELECT person_1.id AS line_manager_id,
            person_1.crsid AS line_manager_crsid,
            person_1.email_address AS line_manager_email,
            (COALESCE(person_1.known_as, person_1.first_names)::text || ' '::text) || person_1.surname::text AS line_manager_name
           FROM person person_1) line_manager ON line_manager.line_manager_id = all_roles.supervisor_id
     JOIN ( SELECT person_1.id AS person_id,
            person_1.crsid AS person_crsid,
            person_1.email_address AS person_email,
            (COALESCE(person_1.known_as, person_1.first_names)::text || ' '::text) || person_1.surname::text AS person_name,
            person_1.surname AS person_surname
           FROM person person_1) person ON person.person_id = all_roles.person_id
     LEFT JOIN ( SELECT post_history.id,
            'post_history'::text AS table_name,
            post_history.is_floor_technician,
            post_history.is_online_leaving_form_superuser
           FROM post_history) post_history_ ON post_history_.id = all_roles.role_id AND post_history_.table_name = all_roles.role_tablename
  ORDER BY person.person_surname, person.person_name, all_roles.status;


DROP VIEW public._all_roles_with_predicted_status_v2;
