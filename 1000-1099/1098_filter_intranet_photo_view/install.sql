CREATE OR REPLACE VIEW www.intranet_person_photo AS
  SELECT person_photo.person_id, person.crsid, photo, photo_version
  FROM person_photo
  JOIN person ON person.id=person_photo.person_id
  LEFT JOIN _latest_role_v12 ON _latest_role_v12.person_id = person.id
  LEFT JOIN post_history ON post_history.id = _latest_role_v12.role_id
  WHERE _latest_role_v12.status::text = 'Current'::text AND (_latest_role_v12.post_category_id = ANY (ARRAY['sc-3'::text, 'sc-2'::text]))
        AND hide_photo_from_website IS false;

