CREATE OR REPLACE VIEW www.intranet_person_photo AS
  SELECT person_id, person.crsid, photo, photo_version
  FROM person_photo
  JOIN person ON person.id=person_id
  WHERE hide_photo_from_website IS false;
