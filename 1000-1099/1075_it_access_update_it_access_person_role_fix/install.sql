CREATE OR REPLACE FUNCTION it_access.update_it_access_person_role()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE

    affected_person_id BIGINT;
    new_it_role_start_date DATE;
    new_it_role_end_date DATE;
    original_notes TEXT;
    original_it_role_start_date DATE;
    original_it_role_end_date DATE;

    current_chem_role_type_id BIGINT = 2;

BEGIN

    -- check what table and operation called us, and set the person_id to
    -- operate on accordingly, or exit without changes.
    IF TG_TABLE_NAME = 'person' AND TG_OP = 'UPDATE' THEN
        -- If we're been triggered by an update on person we also check if any
        -- column changed that could affect it_access.person_role. If not we
        -- return straight away. Doing it this way means we don't have to grant as many
        -- permissions, because lots of roles can update a few columns in public.person
        -- and so will fire this trigger, but very few can update the columns
        -- that cause the trigger to make any changes to it_access.person_role.
        -- We also install the trigger as ON UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given
        -- which doesn't achieve quite the same thing as checking if a column changed, as one can update a column
        -- to the same value as before.
        IF (
            (NEW.arrival_date IS DISTINCT FROM OLD.arrival_date)
            OR
            (NEW.leaving_date IS DISTINCT FROM OLD.leaving_date)
            OR
            (NEW.left_but_no_leaving_date_given IS DISTINCT FROM OLD.left_but_no_leaving_date_given)
        ) THEN
            affected_person_id = NEW.id;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF TG_TABLE_NAME = '_hr_role' THEN
        -- Only student_management, role_end_date_setter, gdpr_purge, and hr
        -- can make changes to the _hr_role table, and the changes they could make are
        -- very likely to cause a change in it_access.person_role. So for simplicity we
        -- don't do a detailed check of OLD vs NEW before running the rest of
        -- the trigger if we're fired on a change to _hr_role.
        IF ( TG_OP = 'DELETE' ) THEN
            affected_person_id = OLD.person_id;
        ELSIF ( TG_OP = 'INSERT' ) THEN
            affected_person_id = NEW.person_id;
        ELSIF ( TG_OP = 'UPDATE' ) THEN
            affected_person_id = NEW.person_id;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        -- We weren't fired on the right table or operation, so return unchanged
        RETURN NEW;
    END IF;

    -- get new dates for Current Chemist IT access
    -- If they are null, use '-infinity', as we want to 
    -- record that the person was thought to be entitled to IT access at some
    -- point as we might well have given them an account in the past. We
    -- do not permit null dates in it_access.person_role so we have to put something.
    SELECT
        COALESCE(current_chemist_role_start_date,'-infinity'::date),
        COALESCE(current_chemist_role_end_date,'-infinity'::date)
    INTO
        new_it_role_start_date,
        new_it_role_end_date
    FROM it_access.get_current_chemist_it_role_dates_for_person(affected_person_id) ;

    -- get current dates for Current Chemist IT access
    SELECT
        start_date,
        end_date
    INTO
        original_it_role_start_date,
        original_it_role_end_date
    FROM it_access.person_role
    WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

    -- Did anything actually change?
    IF (
            (new_it_role_start_date IS DISTINCT FROM original_it_role_start_date)
            OR
            (new_it_role_end_date IS DISTINCT FROM original_it_role_end_date)
    ) THEN
        -- get any existing notes about the person's Current Chemist role
        SELECT COALESCE(notes,'') INTO original_notes
        FROM it_access.person_role
        WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

        -- Changes to _hr_role can insert a new Current Chemist role so do an upsert
        IF TG_TABLE_NAME = '_hr_role' THEN
            INSERT INTO it_access.person_role
                (
                    person_id,
                    role_type_id,
                    start_date,
                    end_date,
                    notes
                )
                VALUES
                (
                    affected_person_id,
                    current_chem_role_type_id,
                    new_it_role_start_date,
                    new_it_role_end_date,
                    FORMAT(E'%s: Insert new Current Chemist IT role from changes in _hr_role by %s\n',now(), current_user)
                )
            ON CONFLICT (person_id, role_type_id) WHERE role_type_id = 2
            DO UPDATE SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in _hr_role by %s\n',now(), current_user)||original_notes;

        -- changes to person can only update an existing current chemist role, not create one
        ELSIF TG_TABLE_NAME = 'person' THEN

            UPDATE it_access.person_role SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in person by %s\n',now(), current_user)||original_notes
            WHERE person_id = affected_person_id AND role_type_id = current_chem_role_type_id;

       END IF;
    END IF;

    RETURN NEW;
END;
$BODY$;
