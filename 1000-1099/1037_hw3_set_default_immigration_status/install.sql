-- FUNCTION: hotwire3.personnel_data_entry()

-- DROP FUNCTION hotwire3.personnel_data_entry();

CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare 
	d_do_not_show_on_website boolean := 'f';
	d_hide_phone_no_from_website boolean := 'f';
	d_immigration_status_id bigint := 1 ;
	on_retired_staff_list bigint;
	retired_staff_list_id bigint;

begin
	if NEW.id is not null 
	then
		update person set
			surname = NEW.surname,
			image_lo = NEW.image_oid,
			first_names = NEW.first_names, 
			title_id = NEW.title_id, 
			known_as = NEW.known_as,
			name_suffix = NEW.name_suffix,
			previous_surname = NEW.previous_surname,
			date_of_birth = NEW.date_of_birth, 
			gender_id = NEW.gender_id, 
			crsid = NEW.crsid,
			email_address = NEW.email_address, 
			hide_email = NEW.hide_email, 
			do_not_show_on_website = COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
			hide_phone_no_from_website = COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
			arrival_date = NEW.arrival_date,
			leaving_date = NEW.leaving_date,
			left_but_no_leaving_date_given = NEW.left_but_no_leaving_date_given,
			cambridge_address = NEW.home_address::varchar(500),
			cambridge_phone_number = NEW.home_phone_number,
			cambridge_college_id = NEW.cambridge_college_id,
			mobile_number = NEW.mobile_number,
			emergency_contact = NEW.emergency_contact::varchar(500), 
			other_information = NEW.other_information,
			notes = NEW.notes,
			forwarding_address = NEW.forwarding_address,
			new_employer_address = NEW.new_employer_address, 
			chem_at_cam = NEW."chem_@_cam",
			continuous_employment_start_date = NEW.continuous_employment_start_date,
			paper_file_details = NEW.paper_file_status, 
			registration_completed = NEW.registration_completed,
			clearance_cert_signed = NEW.clearance_cert_signed,
			counts_as_academic=NEW.treat_as_academic_staff,
                        passport_issuing_country_id=NEW.passport_issuing_country_id,
                        passport_number=NEW.passport_number,
                        passport_expiry_date=NEW.passport_expiry_date,
                        immigration_status_id = COALESCE(NEW.immigration_status_id, d_immigration_status_id),
                        immigration_status_expiry_date=NEW.immigration_status_expiry_date,
                        brp_number=NEW."BRP_number"
		where person.id = OLD.id;
	else
                NEW.id := nextval('person_id_seq');
		insert into person (
                                id,
				surname,
				image_lo, 
				first_names, 
				title_id, 
				known_as,
				name_suffix,
				previous_surname, 
				date_of_birth, 
				gender_id, 
				crsid,
				email_address, 
				hide_email, 
				do_not_show_on_website,
				arrival_date,
				leaving_date,
				cambridge_address,
				cambridge_phone_number, 
				cambridge_college_id,
				mobile_number,
				emergency_contact, 
				other_information,
				notes,
				forwarding_address,
				new_employer_address, 
				chem_at_cam,
				continuous_employment_start_date,
				paper_file_details, 
				registration_completed,
				clearance_cert_signed,
				counts_as_academic,
				hide_phone_no_from_website,
                                passport_issuing_country_id,
                                passport_number,
                                passport_expiry_date,
                                immigration_status_id,
                                immigration_status_expiry_date,
                                brp_number
			) values (
                                NEW.id,
				NEW.surname, 
				NEW.image_oid,
				NEW.first_names, 
				NEW.title_id, 
				NEW.known_as,
				NEW.name_suffix,
				NEW.previous_surname,
				NEW.date_of_birth, 
				NEW.gender_id, 
				NEW.crsid,
				NEW.email_address, 
				NEW.hide_email, 
				COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
				NEW.arrival_date,
				NEW.leaving_date,
				NEW.home_address::varchar(500),
				NEW.home_phone_number, 
				NEW.cambridge_college_id,
				NEW.mobile_number,
				NEW.emergency_contact::varchar(500), 
				NEW.other_information,
				NEW.notes,
				NEW.forwarding_address,
				NEW.new_employer_address, 
				NEW."chem_@_cam",
				NEW.continuous_employment_start_date,
				NEW.paper_file_status, 
				NEW.registration_completed,
				NEW.clearance_cert_signed,
				NEW.treat_as_academic_staff,
				COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
                                NEW.passport_issuing_country_id,
                                NEW.passport_number,
                                NEW.passport_expiry_date,
                                COALESCE(NEW.immigration_status_id,d_immigration_status_id),
                                NEW.immigration_status_expiry_date,
                                NEW."BRP_number"
			) ;
	end if;

	-- do retired staff list update
	select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
	select include_person_id from mm_mailinglist_include_person where include_person_id = NEW.id and mailinglist_id = retired_staff_list_id into on_retired_staff_list;
	if NEW.retired_staff_mailing_list = 't' and on_retired_staff_list is null
	then
	   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, NEW.id); 
	end if;
	if NEW.retired_staff_mailing_list = 'f' and on_retired_staff_list is not null
	then
	   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = NEW.id; 
	end if;

	-- many-many updates here
	perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, NEW.id);

	
	return NEW;
end;
$BODY$;

ALTER FUNCTION hotwire3.personnel_data_entry()
    OWNER TO dev;

