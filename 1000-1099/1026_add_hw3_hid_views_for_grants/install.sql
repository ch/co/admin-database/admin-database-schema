CREATE VIEW hotwire3.grant_holder_hid AS
SELECT id AS grant_holder_id,
       holder AS grant_holder_hid
FROM grants.holder
ORDER BY holder;

ALTER TABLE hotwire3.grant_holder_hid OWNER TO dev;
GRANT SELECT ON hotwire3.grant_holder_hid TO grants_administration,grants_ro;

CREATE VIEW hotwire3.research_area_hid AS
SELECT id AS research_area_id,
       research_area AS research_area_hid
FROM grants.research_area
ORDER BY research_area;

ALTER TABLE hotwire3.research_area_hid OWNER TO dev;
GRANT SELECT ON hotwire3.research_area_hid TO grants_administration,grants_ro;

CREATE VIEW hotwire3.funder_type_hid AS
SELECT id AS funder_type_id,
       funder_type AS funder_type_hid
FROM grants.funder_type
ORDER BY funder_type;

ALTER TABLE hotwire3.funder_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.funder_type_hid TO grants_administration,grants_ro;

CREATE VIEW hotwire3.funder_hid AS
SELECT id AS funder_id,
       funder AS funder_hid
FROM grants.funder
ORDER BY funder;

ALTER TABLE hotwire3.funder_hid OWNER TO dev;
GRANT SELECT ON hotwire3.funder_hid TO grants_administration,grants_ro;

CREATE VIEW hotwire3.application_status_hid AS
SELECT id AS application_status_id,
       status AS application_status_hid
FROM grants.application_status
ORDER BY status;

ALTER TABLE hotwire3.application_status_hid OWNER TO dev;
GRANT SELECT ON hotwire3.application_status_hid TO grants_administration,grants_ro;
