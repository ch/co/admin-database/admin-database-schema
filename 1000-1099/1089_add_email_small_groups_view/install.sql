CREATE VIEW apps.email_small_groups
 AS
 WITH all_objects AS (
         SELECT person.crsid AS object,
            ARRAY[person.crsid]::text[] AS names,
            person.email_address::text AS address,
            'user'::character varying AS class
           FROM person
          WHERE person.crsid IS NOT NULL
        UNION
           SELECT room.email_local_part AS object,
                  ARRAY[room.email_local_part,room.name,lower(room.name),substring(room.email_local_part from 4)]::text[] as names,
                  btrim(person.email_address::text) AS address,
                  'room' AS class
          FROM room
          JOIN mm_person_room mm ON room.id = mm.room_id
          JOIN person ON mm.person_id = person.id
          LEFT JOIN _physical_status_v3 USING (person_id)
          WHERE _physical_status_v3.status_id::text = 'Current'::text
        UNION
         SELECT cabinet.email_local_part AS object,
            ARRAY[cabinet.email_local_part,'cab'||cabinet.cabno,cabinet.cabname,cabinet.name]::text[] as names,
            person.email_address::text AS address,
            'cabinet'::text AS class
         FROM cabinet
         JOIN mm_cabinet_serves_room ON cabinet.id = mm_cabinet_serves_room.cabinet_id
         JOIN room ON mm_cabinet_serves_room.room_id = room.id
         JOIN mm_person_room ON mm_person_room.room_id = room.id
         JOIN person ON mm_person_room.person_id = person.id
         JOIN _physical_status_v3 ps ON person.id = ps.person_id
         WHERE ps.status_id::text = 'Current'::text
        UNION
         SELECT 'comp-rep-' ||regexp_replace(lower(research_group.name::text), '[^a-z]+'::text, ''::text, 'g'::text) AS object,
                ARRAY[research_group.name,regexp_replace(lower(research_group.name::text), '[^a-z]+'::text, ''::text, 'g'::text)]::text[] as names,
                person.email_address::text AS address,
               'computer-rep' AS class
           FROM research_group
            JOIN mm_research_group_computer_rep mm ON research_group.id = mm.research_group_id
            JOIN person ON mm.computer_rep_id = person.id
            JOIN _physical_status_v3 ps ON ps.person_id = person.id
            WHERE ps.status_id::text = 'Current'::text
        )
 SELECT all_objects.object,
    all_objects.names,
    all_objects.address,
    all_objects.class
   FROM all_objects
   WHERE address ~~ '%@%'::text
;

ALTER TABLE apps.email_small_groups
    OWNER TO dev;

GRANT SELECT ON TABLE apps.email_small_groups TO ad_accounts;
GRANT SELECT ON TABLE apps.email_small_groups TO mailinglists;
GRANT SELECT ON TABLE apps.email_small_groups TO cos;
