CREATE OR REPLACE VIEW apps.managed_mail_domain_v3
 AS
 WITH all_aliases AS (
         SELECT mail_alias.alias,
            mail_alias.addresses AS target,
            mail_alias.comment,
            1 AS priority
           FROM mail_alias
        UNION
         SELECT person.crsid AS alias,
            COALESCE(person.managed_mail_domain_target, person.email_address::text) AS addresses,
            'autogenerated from db'::character varying AS comment,
            2 AS priority
           FROM person
             JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
          WHERE (_physical_status_v3.status_id::text = 'Current'::text OR _physical_status_v3.status_id::text = 'Past'::text) AND person.crsid IS NOT NULL AND person.email_address::text ~~ '%@%'::text AND btrim(COALESCE(person.managed_mail_domain_target, person.email_address::text)) <> (person.crsid::text || '@ch.cam.ac.uk'::text) AND NOT person.managed_mail_domain_optout
        UNION
         SELECT a.email_local_part AS alias,
            array_to_string(a.array_addresses, ','::text) AS addresses,
            'room alias from db'::character varying AS comment,
            3 AS priority
           FROM ( SELECT room.email_local_part,
                    ARRAY( SELECT btrim(p.email_address::text) AS btrim
                           FROM person p
                             JOIN mm_person_room mm ON p.id = mm.person_id
                             JOIN room r2 ON mm.room_id = r2.id
                             LEFT JOIN _physical_status_v3 USING (person_id)
                          WHERE r2.id = room.id AND p.email_address IS NOT NULL AND p.email_address::text ~~ '%@%'::text AND _physical_status_v3.status_id::text = 'Current'::text) AS array_addresses
                   FROM room) a
          WHERE a.array_addresses <> '{}'::text[]
        UNION
         SELECT cabinet_addresses.email_local_part AS alias,
            string_agg(btrim(cabinet_addresses.email_address::text), ','::text) AS target,
            'Cabinet alias from database'::text AS comment,
            4 AS priority
           FROM ( SELECT DISTINCT cabinet.email_local_part,
                    person.email_address
                   FROM cabinet
                     JOIN mm_cabinet_serves_room ON cabinet.id = mm_cabinet_serves_room.cabinet_id
                     JOIN room ON mm_cabinet_serves_room.room_id = room.id
                     JOIN mm_person_room ON mm_person_room.room_id = room.id
                     JOIN person ON mm_person_room.person_id = person.id
                     JOIN _physical_status_v3 ps ON person.id = ps.person_id
                  WHERE ps.status_id::text = 'Current'::text AND person.email_address::text ~~ '%@%'::text) cabinet_addresses
          GROUP BY cabinet_addresses.email_local_part
        UNION
         SELECT a.alias,
            array_to_string(a.array_addresses, ','::text) AS target,
            'Group computer rep alias'::character varying AS comment,
            5 AS priority
           FROM ( SELECT research_group.id,
                    research_group.name,
                    'comp-rep-'::text || regexp_replace(lower(research_group.name::text), '[^a-z]+'::text, ''::text, 'g'::text) AS alias,
                    ARRAY( SELECT btrim(p.email_address::text) AS btrim
                           FROM person p
                             LEFT JOIN _physical_status_v3 ps ON p.id = ps.person_id
                             JOIN mm_research_group_computer_rep mm ON p.id = mm.computer_rep_id
                             JOIN research_group r2 ON mm.research_group_id = r2.id
                          WHERE ps.status_id::text = 'Current'::text AND p.email_address::text ~~ '%@%'::text AND r2.id = research_group.id) AS array_addresses
                   FROM research_group) a
          WHERE a.array_addresses <> '{}'::text[]
        )
 SELECT DISTINCT ON (all_aliases.alias) all_aliases.alias,
    all_aliases.target,
    all_aliases.comment
   FROM all_aliases
  ORDER BY all_aliases.alias, all_aliases.priority;
