DROP VIEW registration.crsids_allowed_to_generate_forms;

CREATE TABLE registration.form_type (
  form_type_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  form_type TEXT UNIQUE NOT NULL
);

ALTER TABLE registration.form_type OWNER TO dev;

INSERT INTO registration.form_type (form_type) VALUES ('visitor'::text), ('staff'::text), ('research'::text), ('postgrad'::text), ('erasmus'::text);

CREATE TABLE registration.mm_person_form_type (
  person_id BIGINT NOT NULL REFERENCES public.person(id),
  form_type_id BIGINT NOT NULL REFERENCES registration.form_type(form_type_id),
  PRIMARY KEY (person_id,form_type_id)
);

ALTER TABLE registration.mm_person_form_type OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON registration.mm_person_form_type TO hr;

CREATE VIEW registration.crsids_allowed_to_generate_forms
 AS
 SELECT DISTINCT person.crsid,
    form_type.form_type
   FROM person
     JOIN public._all_roles_with_predicted_status_v2 ar ON person.id = ar.person_id
     JOIN _physical_status_v3 ps ON person.id = ps.person_id
     CROSS JOIN registration.form_type
  WHERE ar.status::text = 'Current'::text AND ps.status_id::text = 'Current'::text AND (ar.post_category::text = 'Assistant staff'::text OR ar.post_category::text = 'Academic-related staff'::text OR ar.post_category::text = 'Academic staff'::text)
  UNION
  SELECT person.crsid,
    form_type.form_type
  FROM registration.mm_person_form_type
  JOIN public.person ON person.id = mm_person_form_type.person_id
  JOIN registration.form_type USING(form_type_id)
  ;

ALTER TABLE registration.crsids_allowed_to_generate_forms
    OWNER TO dev;

GRANT ALL ON TABLE registration.crsids_allowed_to_generate_forms TO dev;
GRANT SELECT ON TABLE registration.crsids_allowed_to_generate_forms TO starters_registration;

CREATE VIEW hotwire3.registration_form_type_hid AS
SELECT form_type_id AS registration_form_type_id,
    form_type AS registration_form_type_hid
    FROM registration.form_type;

ALTER VIEW hotwire3.registration_form_type_hid OWNER TO dev;
GRANT SELECT ON hotwire3.registration_form_type_hid TO PUBLIC;

-- The select within a select is because Hotwire gets confused trying to decode the
-- inner select and throws an error if it's not wrapped
CREATE VIEW hotwire3."10_View/Registration/Additional_Form_Generators" AS
WITH a AS (
SELECT person_id AS id,
  person_id,
  array_agg(form_type_id) AS registration_form_type_id
FROM public.person
JOIN registration.mm_person_form_type ON person.id = mm_person_form_type.person_id
GROUP BY id, person_id)
SELECT * from a;

ALTER VIEW hotwire3."10_View/Registration/Additional_Form_Generators" OWNER TO dev;
GRANT SELECT,INSERT,DELETE,UPDATE ON hotwire3."10_View/Registration/Additional_Form_Generators" TO hr;

CREATE FUNCTION hotwire3.registration_form_generators() RETURNS TRIGGER AS
$body$
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE'
    THEN
      NEW.id = NEW.person_id;
      DELETE FROM registration.mm_person_form_type WHERE person_id = NEW.person_id;
      INSERT INTO registration.mm_person_form_type (person_id, form_type_id) SELECT NEW.person_id, UNNEST(NEW.registration_form_type_id) ;
      RETURN NEW;
    END IF;
    IF TG_OP = 'DELETE'
    THEN
      DELETE FROM registration.mm_person_form_type WHERE person_id = OLD.id;
      RETURN OLD;
    END IF;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.registration_form_generators() OWNER TO dev;

CREATE TRIGGER hw3_update_additional_reg INSTEAD OF INSERT OR UPDATE OR DELETE ON hotwire3."10_View/Registration/Additional_Form_Generators" FOR EACH ROW EXECUTE FUNCTION hotwire3.registration_form_generators();

INSERT INTO hotwire3._primary_table (view_name, primary_table) VALUES ('10_View/Registration/Additional_Form_Generators','person');