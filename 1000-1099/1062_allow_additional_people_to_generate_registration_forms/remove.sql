DROP VIEW registration.crsids_allowed_to_generate_forms;

CREATE VIEW registration.crsids_allowed_to_generate_forms
 AS
 SELECT person.crsid,
    form_types.form_type
   FROM person
     JOIN cache._latest_role_v12 lr ON person.id = lr.person_id
     JOIN _physical_status_v3 ps ON person.id = ps.person_id
     CROSS JOIN ( VALUES ('visitor'::text), ('staff'::text), ('research'::text), ('postgrad'::text), ('erasmus'::text)) form_types(form_type)
  WHERE lr.status::text = 'Current'::text AND ps.status_id::text = 'Current'::text AND (lr.post_category::text = 'Assistant staff'::text OR lr.post_category::text = 'Academic-related staff'::text OR lr.post_category::text = 'Academic staff'::text);

ALTER TABLE registration.crsids_allowed_to_generate_forms
    OWNER TO dev;

GRANT ALL ON TABLE registration.crsids_allowed_to_generate_forms TO dev;
GRANT SELECT ON TABLE registration.crsids_allowed_to_generate_forms TO starters_registration;

DROP VIEW hotwire3."10_View/Registration/Additional_Form_Generators";

DROP VIEW hotwire3.registration_form_type_hid;

DROP TABLE registration.mm_person_form_type;

DROP TABLE registration.form_type;

DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Registration/Additional_Form_Generators';

DROP FUNCTION hotwire3.registration_form_generators();
