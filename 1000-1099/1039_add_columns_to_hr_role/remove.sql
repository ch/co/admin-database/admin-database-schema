CREATE OR REPLACE FUNCTION public.update_hr_role()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    my_estimated_role_end_date DATE;
    my_post_category_id TEXT;
    my_post_category VARCHAR(80);
    my_status_id VARCHAR(10);
    my_supervisor_id BIGINT;
    my_json_new JSONB;

BEGIN

    -- if it's a delete, just delete it
    IF TG_OP = 'DELETE' THEN
        DELETE FROM public._hr_role WHERE person_id = OLD.person_id AND role_id = OLD.id and role_tablename = TG_TABLE_NAME;
        RETURN NULL;
    END IF;

    -- get data set up
    -- convert row to json so we can check for columns that
    -- don't exist in all rows
    my_json_new = row_to_json(NEW);

    -- status
    SELECT public.role_predicted_status(NEW.start_date,NEW.end_date,NEW.intended_end_date,(my_json_new->>'funding_end_date')::DATE,NEW.force_role_status_to_past,TG_TABLE_NAME) INTO my_status_id;

    -- estimated_role_end_date is calculated like this for most role types, and overridden below as necessary
    SELECT COALESCE(NEW.end_date,NEW.intended_end_date) into my_estimated_role_end_date;

    -- post category, supervisor, and sometimes estimated_role_end_date depend on role type
    CASE TG_TABLE_NAME
        WHEN 'part_iii_studentship' THEN
            my_post_category := 'Part III'::varchar(80);
            my_post_category_id := 'p3-1'::text;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'erasmus_socrates_studentship' THEN
            SELECT erasmus_type_hid INTO my_post_category FROM erasmus_type_hid WHERE erasmus_type_hid.erasmus_type_id = NEW.erasmus_type_id;
            SELECT 'e-' || NEW.erasmus_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'postgraduate_studentship' THEN
            SELECT name INTO my_post_category FROM postgraduate_studentship_type WHERE postgraduate_studentship_type.id = NEW.postgraduate_studentship_type_id;
            SELECT 'pg-' || NEW.postgraduate_studentship_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.first_supervisor_id;
        WHEN 'post_history' THEN
            SELECT category INTO my_post_category FROM staff_category WHERE id = NEW.staff_category_id;
            SELECT 'sc-' || NEW.staff_category_id::text INTO my_post_category_id;
            -- update the estimated role end date as the definition is different for staff
            SELECT COALESCE(NEW.end_date,LEAST(NEW.intended_end_date,NEW.funding_end_date)) into my_estimated_role_end_date;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'visitorship' THEN
            SELECT visitor_type_hid INTO my_post_category FROM visitor_type_hid WHERE visitor_type_hid.visitor_type_id = NEW.visitor_type_id;
            SELECT 'v-' || NEW.visitor_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.host_person_id;
    END CASE;

    -- if it's an insert, insert, or update, update
    IF TG_OP = 'INSERT' THEN
        INSERT INTO public._hr_role (
            person_id,
            role_id,
            role_tablename,
            start_date,
            intended_end_date,
            estimated_role_end_date,
            end_date,
            funding_end_date,
            supervisor_id,
            post_category_id,
            post_category,
            predicted_role_status_id,
            funding,
            fees_funding,
            research_grant_number,
            paid_by_university,
            job_title,
            left_but_no_leaving_date_given
        ) VALUES (
            NEW.person_id,
            NEW.id,
            TG_TABLE_NAME,
            NEW.start_date,
            NEW.intended_end_date,
            my_estimated_role_end_date,
            NEW.end_date,
            (my_json_new->>'funding_end_date')::DATE,
            my_supervisor_id,
            my_post_category_id,
            my_post_category,
            my_status_id,
            (my_json_new->>'filemaker_funding')::VARCHAR(500),
            (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            (my_json_new->>'research_grant_number')::VARCHAR(500),
            (my_json_new->>'paid_by_university')::BOOLEAN,
            (my_json_new->>'job_title')::VARCHAR(120),
            NEW.force_role_status_to_past
        );

    ELSIF TG_OP = 'UPDATE' THEN
        UPDATE _hr_role SET
            person_id = NEW.person_id,
            start_date = NEW.start_date,
            intended_end_date = NEW.intended_end_date,
            estimated_role_end_date = my_estimated_role_end_date,
            end_date = NEW.end_date,
            funding_end_date = (my_json_new->>'funding_end_date')::DATE,
            supervisor_id = my_supervisor_id,
            post_category_id = my_post_category_id,
            post_category = my_post_category,
            predicted_role_status_id = my_status_id,
            funding = (my_json_new->>'filemaker_funding')::VARCHAR(500),
            fees_funding = (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            research_grant_number = (my_json_new->>'research_grant_number')::VARCHAR(500),
            paid_by_university = (my_json_new->>'paid_by_university')::BOOLEAN,
            job_title = (my_json_new->>'job_title')::VARCHAR(120),
            left_but_no_leaving_date_given = NEW.force_role_status_to_past
        WHERE role_id = NEW.id AND role_tablename = TG_TABLE_NAME;
    END IF;

    RETURN NEW;
END;

$BODY$;

ALTER TABLE public._hr_role DROP COLUMN chem;
ALTER TABLE public._hr_role DROP COLUMN mentor_id;
ALTER TABLE public._hr_role DROP COLUMN co_supervisor_id;

CREATE OR REPLACE FUNCTION public.update_hr_role()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    my_estimated_role_end_date DATE;
    my_post_category_id TEXT;
    my_post_category VARCHAR(80);
    my_status_id VARCHAR(10);
    my_supervisor_id BIGINT;
    my_mentor_id BIGINT;
    my_json_new JSONB;

BEGIN

    -- if it's a delete, just delete it
    IF TG_OP = 'DELETE' THEN
        DELETE FROM public._hr_role WHERE person_id = OLD.person_id AND role_id = OLD.id and role_tablename = TG_TABLE_NAME;
        RETURN NULL;
    END IF;

    -- get data set up
    -- convert row to json so we can check for columns that
    -- don't exist in all rows
    my_json_new = row_to_json(NEW);

    -- status
    SELECT public.role_predicted_status(NEW.start_date,NEW.end_date,NEW.intended_end_date,(my_json_new->>'funding_end_date')::DATE,NEW.force_role_status_to_past,TG_TABLE_NAME) INTO my_status_id;

    -- estimated_role_end_date is calculated like this for most role types, and overridden below as necessary
    SELECT COALESCE(NEW.end_date,NEW.intended_end_date) into my_estimated_role_end_date;

    -- post category, supervisor, and sometimes estimated_role_end_date depend on role type
    CASE TG_TABLE_NAME
        WHEN 'part_iii_studentship' THEN
            my_post_category := 'Part III'::varchar(80);
            my_post_category_id := 'p3-1'::text;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'erasmus_socrates_studentship' THEN
            SELECT erasmus_type_hid INTO my_post_category FROM erasmus_type_hid WHERE erasmus_type_hid.erasmus_type_id = NEW.erasmus_type_id;
            SELECT 'e-' || NEW.erasmus_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'postgraduate_studentship' THEN
            SELECT name INTO my_post_category FROM postgraduate_studentship_type WHERE postgraduate_studentship_type.id = NEW.postgraduate_studentship_type_id;
            SELECT 'pg-' || NEW.postgraduate_studentship_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.first_supervisor_id;
        WHEN 'post_history' THEN
            SELECT category INTO my_post_category FROM staff_category WHERE id = NEW.staff_category_id;
            SELECT 'sc-' || NEW.staff_category_id::text INTO my_post_category_id;
            -- update the estimated role end date as the definition is different for staff
            SELECT COALESCE(NEW.end_date,LEAST(NEW.intended_end_date,NEW.funding_end_date)) into my_estimated_role_end_date;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'visitorship' THEN
            SELECT visitor_type_hid INTO my_post_category FROM visitor_type_hid WHERE visitor_type_hid.visitor_type_id = NEW.visitor_type_id;
            SELECT 'v-' || NEW.visitor_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.host_person_id;
    END CASE;

    -- if it's an insert, insert, or update, update
    IF TG_OP = 'INSERT' THEN
        INSERT INTO public._hr_role (
            person_id,
            role_id,
            role_tablename,
            start_date,
            intended_end_date,
            estimated_role_end_date,
            end_date,
            funding_end_date,
            supervisor_id,
            post_category_id,
            post_category,
            predicted_role_status_id,
            funding,
            fees_funding,
            research_grant_number,
            paid_by_university,
            job_title,
            left_but_no_leaving_date_given
        ) VALUES (
            NEW.person_id,
            NEW.id,
            TG_TABLE_NAME,
            NEW.start_date,
            NEW.intended_end_date,
            my_estimated_role_end_date,
            NEW.end_date,
            (my_json_new->>'funding_end_date')::DATE,
            my_supervisor_id,
            my_post_category_id,
            my_post_category,
            my_status_id,
            (my_json_new->>'filemaker_funding')::VARCHAR(500),
            (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            (my_json_new->>'research_grant_number')::VARCHAR(500),
            (my_json_new->>'paid_by_university')::BOOLEAN,
            (my_json_new->>'job_title')::VARCHAR(120),
            NEW.force_role_status_to_past
        );

    ELSIF TG_OP = 'UPDATE' THEN
        UPDATE _hr_role SET
            person_id = NEW.person_id,
            start_date = NEW.start_date,
            intended_end_date = NEW.intended_end_date,
            estimated_role_end_date = my_estimated_role_end_date,
            end_date = NEW.end_date,
            funding_end_date = (my_json_new->>'funding_end_date')::DATE,
            supervisor_id = my_supervisor_id,
            post_category_id = my_post_category_id,
            post_category = my_post_category,
            predicted_role_status_id = my_status_id,
            funding = (my_json_new->>'filemaker_funding')::VARCHAR(500),
            fees_funding = (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            research_grant_number = (my_json_new->>'research_grant_number')::VARCHAR(500),
            paid_by_university = (my_json_new->>'paid_by_university')::BOOLEAN,
            job_title = (my_json_new->>'job_title')::VARCHAR(120),
            left_but_no_leaving_date_given = NEW.force_role_status_to_past
        WHERE role_id = NEW.id AND role_tablename = TG_TABLE_NAME;
    END IF;

    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION public.update_hr_role()
    OWNER TO dev;
