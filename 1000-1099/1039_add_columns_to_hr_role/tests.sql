BEGIN;

SELECT plan(8);

-- check co-supervisors match for roles with those
SELECT results_eq(format('SELECT co_supervisor_id FROM _hr_role WHERE role_tablename = %L ORDER BY role_id',tabname),
                  format('SELECT second_supervisor_id FROM %I ORDER BY id',tabname),
                  format('Second supervisor matches in _hr_role %s',tabname))
FROM (values ('postgraduate_studentship')) as t (tabname);


-- check chem field
PREPARE get_chem_for_table AS SELECT chem FROM _hr_role WHERE role_tablename = $1 ORDER BY role_id;
-- always true for postgrads
SELECT results_eq('execute get_chem_for_table($$postgraduate_studentship$$)',
                  'select TRUE from postgraduate_studentship',
                  'chem for postgrads in _hr_role always true');

-- match chem role value for staff
SELECT results_eq('execute get_chem_for_table($$post_history$$)',
                  'select chem from post_history order by id',
                  'chem in _hr_role matches value in post_history for staff');
-- always false for other tables
SELECT results_eq(format('get_chem_for_table(%L)',tabname),
                  format('select FALSE from %I',tabname),
                  format('chem for %I in _hr_role always false',tabname))
FROM (values ('part_iii_studentship'), ('erasmus_socrates_studentship'), ('visitorship')) as t (tabname);;


-- mentors
PREPARE get_mentor_for_table AS SELECT mentor_id FROM _hr_role WHERE role_tablename = $1 ORDER BY role_id;
SELECT results_eq('execute get_mentor_for_table($$postgraduate_studentship$$)',
                  'select first_mentor_id FROM postgraduate_studentship order by id',
                  '_hr_role mentors match postgrad mentors');
SELECT results_eq('execute get_mentor_for_table($$post_history$$)',
                  'select mentor_id FROM post_history order by id',
                  '_hr_role mentors match staff mentors');

SELECT * FROM finish();

ROLLBACK;
