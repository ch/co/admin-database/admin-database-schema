INSERT INTO hotwire3._primary_table
    ( view_name, primary_table )
    VALUES
    ( '10_View/Grants/Grant_holders', 'grants.holder'),
    ( '10_View/Grants/Funders', 'grants.funder'),
    ( '10_View/Grants/Merge_Funders', 'grants.funder'),
    ( '10_View/Grants/Research_Areas', 'grants.research_area'),
    ( '10_View/Grants/Application_Statuses', 'grants.application_status'),
    ( '10_View/Grants/Applications', 'grants.application'),
    ( '10_View/Grants/Awards', 'grants.award')
;
