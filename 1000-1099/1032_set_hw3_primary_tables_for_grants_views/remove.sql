DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Grant_holders';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Funders';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Merge_Funders';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Research_Areas';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Application_Statuses';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Applications';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Awards';
