REVOKE SELECT, UPDATE(end_date) ON TABLE public.post_history FROM onlineleaversform;
REVOKE SELECT, UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship FROM onlineleaversform;
REVOKE SELECT, UPDATE(end_date) ON TABLE public.part_iii_studentship FROM onlineleaversform;
REVOKE SELECT, UPDATE(end_date) ON TABLE public.visitorship FROM onlineleaversform;

REVOKE SELECT ON TABLE public.post_history FROM role_end_date_setter;
REVOKE SELECT ON TABLE public.erasmus_socrates_studentship FROM role_end_date_setter;
REVOKE SELECT ON TABLE public.part_iii_studentship FROM role_end_date_setter;
REVOKE SELECT ON TABLE public.visitorship FROM role_end_date_setter;
