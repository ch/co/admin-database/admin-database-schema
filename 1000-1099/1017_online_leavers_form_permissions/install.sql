GRANT SELECT, UPDATE(end_date) ON TABLE public.post_history TO onlineleaversform;
GRANT SELECT, UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship TO onlineleaversform;
GRANT SELECT, UPDATE(end_date) ON TABLE public.part_iii_studentship TO onlineleaversform;
GRANT SELECT, UPDATE(end_date) ON TABLE public.visitorship TO onlineleaversform;

GRANT SELECT, UPDATE(end_date) ON TABLE public.post_history TO role_end_date_setter;
GRANT SELECT, UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship TO role_end_date_setter;
GRANT SELECT, UPDATE(end_date) ON TABLE public.part_iii_studentship TO role_end_date_setter;
GRANT SELECT, UPDATE(end_date) ON TABLE public.visitorship TO role_end_date_setter;
