CREATE VIEW hotwire3."10_View/Grants/Awards" AS
WITH ordered_grants AS (
    SELECT
        award.id,
        project_number,
        award_number,
        grant_holder_id,
        co_investigators,
        funder_id,
        funder_ref,
        funder_type.funder_type as ro_funder_type,
        funder.code as ro_funder_code,
        project_title,
        start_date,
        end_date,
        (extract('year' from age((end_date+1),start_date))*12) + extract('month' from age((end_date+1),start_date)) + ((extract('day' from age((end_date+1),start_date)))/30.43)::numeric(6,2) as ro_duration_months,
        -- _duration_mths::numeric(6,2) as ro_fm_duration_mths,
        CASE 
            WHEN start_date > current_date THEN (extract('year' from age((end_date+1),start_date))*12) + extract('month' from age((end_date+1),start_date)) + ((extract('day' from age((end_date+1),start_date)))/30.43)::numeric(6,2)
            WHEN end_date < current_date THEN 0::numeric(6,2)
            ELSE (extract('year' from age((end_date+1),current_date))*12) + extract('month' from age((end_date+1),current_date)) + ((extract('day' from age((end_date+1),current_date)))/30.43)::numeric(6,2)
        END AS ro_months_remaining,
        CASE
           WHEN start_date > current_date THEN 'Future'::varchar
           WHEN end_date >= current_date THEN 'Current'::varchar
           WHEN end_date < current_date THEN 'Past'::varchar
           ELSE 'Unknown'::varchar
        END AS status,
        notes,
        research_area_id,
        research_area.research_code as ro_research_code,
        (staff + pooled_labour + travel + equipment + exceptional + consumables + facilities + overheads + estates_costs + indirect_costs + pi_costs + infrastructure_tech + univ_contribution + contingency ) AS ro_project_value,
        staff,
        pooled_labour,
        travel,
        equipment,
        exceptional,
        consumables AS other_costs,
        facilities,
        overheads,
        estates_costs,
        indirect_costs,
        pi_costs AS "PI_costs",
        infrastructure_tech,
        univ_contribution,
        contingency,
        CASE
          WHEN end_date < current_date THEN 'orange'::text
          ELSE NULL::text
        END AS _cssclass
    FROM grants.award
    LEFT JOIN grants.research_area ON award.research_area_id = research_area.id
    LEFT JOIN grants.funder ON award.funder_id = funder.id
    LEFT JOIN grants.funder_type ON funder_type.id = funder.funder_type_id
    ORDER BY award_number::bigint DESC NULLS LAST
) SELECT * FROM ordered_grants;

ALTER TABLE hotwire3."10_View/Grants/Awards" OWNER TO dev;
GRANT SELECT,UPDATE,INSERT,DELETE ON hotwire3."10_View/Grants/Awards" TO grants_administration;
GRANT SELECT ON hotwire3."10_View/Grants/Awards" TO grants_ro;

CREATE FUNCTION hotwire3.grants_awards_upd() RETURNS TRIGGER AS $$
DECLARE
    new_id BIGINT := NULL;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO grants.award ( 
            project_number,
            grant_holder_id,
            funder_id,
            funder_ref,
            project_title,
            start_date,
            end_date,
            notes,
            award_number,
            co_investigators,
            research_area_id,
            staff,
            pooled_labour,
            travel,
            equipment,
            exceptional,
            consumables,
            facilities,
            overheads,
            estates_costs,
            indirect_costs,
            pi_costs,
            infrastructure_tech,
            univ_contribution,
            contingency
        ) VALUES (
            NEW.project_number,
            NEW.grant_holder_id,
            NEW.funder_id,
            NEW.funder_ref,
            NEW.project_title,
            NEW.start_date,
            NEW.end_date,
            NEW.notes,
            NEW.award_number,
            NEW.co_investigators,
            NEW.research_area_id,
            COALESCE(NEW.staff,0),
            COALESCE(NEW.pooled_labour,0),
            COALESCE(NEW.travel,0),
            COALESCE(NEW.equipment,0),
            COALESCE(NEW.exceptional,0),
            COALESCE(NEW.other_costs,0),
            COALESCE(NEW.facilities,0),
            COALESCE(NEW.overheads,0),
            COALESCE(NEW.estates_costs,0),
            COALESCE(NEW.indirect_costs,0),
            COALESCE(NEW."PI_costs",0),
            COALESCE(NEW.infrastructure_tech,0),
            COALESCE(NEW.univ_contribution,0),
            COALESCE(NEW.contingency,0)) RETURNING id INTO new_id;
        NEW.id := new_id;
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN UPDATE grants.award SET 
            project_number = NEW.project_number,
            grant_holder_id = NEW.grant_holder_id,
            funder_id = NEW.funder_id,
            funder_ref = NEW.funder_ref,
            project_title = NEW.project_title,
            start_date = NEW.start_date,
            end_date = NEW.end_date,
            notes = NEW.notes,
            award_number = NEW.award_number,
            co_investigators = NEW.co_investigators,
            research_area_id = NEW.research_area_id,
            staff = NEW.staff,
            pooled_labour = NEW.pooled_labour,
            travel = NEW.travel,
            equipment = NEW.equipment,
            exceptional = NEW.exceptional,
            consumables = NEW.other_costs,
            facilities = NEW.facilities,
            overheads = NEW.overheads,
            estates_costs = NEW.estates_costs,
            indirect_costs = NEW.indirect_costs,
            pi_costs = NEW."PI_costs",
            infrastructure_tech = NEW.infrastructure_tech,
            univ_contribution = NEW.univ_contribution,
            contingency = NEW.contingency
        WHERE id = OLD.id;
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        DELETE FROM grants.award WHERE id = OLD.id;
        RETURN NULL;
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.grants_awards_upd() OWNER TO dev;
CREATE TRIGGER grants_award_trig INSTEAD OF UPDATE OR INSERT OR DELETE ON hotwire3."10_View/Grants/Awards" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_awards_upd();
