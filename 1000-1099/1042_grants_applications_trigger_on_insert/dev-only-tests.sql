BEGIN;
SELECT plan(4);

\set testpi '\'Test PI\''
\set testfunder '\'Test Funder\''
 -- award_number is varchar in application but bigint in award because of bad data from Filemaker
\set award '123456'
\set project '\'MAAG/ABCD\''
\set x5 '\'54321\''
\set title '\'Test project title\''
\set co_investigators '\'Maculloch and Wallis\''
\set notes '\'Test note\''

\set consumables '100'
\set contingency '200'
\set equipment '300'
\set estates_costs '400'
\set exceptional '500'
\set facilities '600'
\set indirect_costs '700'
\set infrastructure_tech '800'
\set overheads '900'
\set pi_costs '1000'
\set pooled_labour '1100'
\set staff '1200'
\set travel '1300'
\set univ_contribution '1400'

-- ensure we have exactly one grantholder called :testpi
DELETE FROM grants.holder WHERE holder = :testpi;
INSERT INTO grants.holder ( holder ) VALUES ( :testpi ); 
-- ensure we have a funder called :testfunder
DELETE FROM grants.funder WHERE funder = :testfunder;
INSERT INTO grants.funder ( funder ) VALUES ( :testfunder ); 

PREPARE delete_award AS
    DELETE FROM grants.award WHERE award_number = (:award);

PREPARE delete_application AS
    DELETE FROM grants.application WHERE award_number = (:award)::varchar;

PREPARE insert_application(varchar) AS
    INSERT INTO grants.application
     (
        grant_holder_id,
        status_id,
        start_date,
        end_date,
        application_date,
        project_number,
        x5_number,
        award_number,
        co_investigators,
        notes,
        consumables,
        contingency,
        equipment,
        estates_costs,
        exceptional_items,
        facilities,
        indirect_costs,
        infrastructure_tech,
        overheads,
        pi_costs,
        pooled_labour,
        staff,
        travel,
        univ_contribution
     )
    VALUES
     (
        (SELECT id FROM grants.holder WHERE holder = :testpi),
        (SELECT id FROM grants.application_status WHERE status = $1 ),
        current_date + interval '3 months',
        current_date + interval '15 months',
        current_date,
        :project,
        :x5,
        :award::varchar,
        :co_investigators,
        :notes,
        :consumables,
        :contingency,
        :equipment,
        :estates_costs,
        :exceptional,
        :facilities,
        :indirect_costs,
        :infrastructure_tech,
        :overheads,
        :pi_costs,
        :pooled_labour,
        :staff,
        :travel,
        :univ_contribution
    );

PREPARE find_award AS
    SELECT
           application_id,
           grant_holder_id,
           funder_id,
           project_title,
           award_number,
           project_number,
           end_date,
           co_investigators,
           consumables,
           contingency,
           equipment,
           estates_costs,
           exceptional,
           facilities,
           indirect_costs,
           infrastructure_tech,
           notes,
           overheads,
           pi_costs,
           pooled_labour,
           staff,
           start_date,
           travel,
           univ_contribution
    FROM grants.award
    WHERE award_number = :award;

PREPARE find_application AS
    SELECT
           id,
           grant_holder_id,
           funder_id,
           project_title,
           award_number::bigint,
           project_number,
           end_date,
           co_investigators,
           consumables,
           contingency,
           equipment,
           estates_costs,
           exceptional_items,
           facilities,
           indirect_costs,
           infrastructure_tech,
           notes,
           overheads,
           pi_costs,
           pooled_labour,
           staff,
           start_date,
           travel,
           univ_contribution
    FROM grants.application
    WHERE award_number = (:award)::varchar;

PREPARE update_application(varchar) AS UPDATE grants.application SET status_id = (SELECT id FROM grants.application_status WHERE status = $1) WHERE award_number = (:award)::varchar;

-- ensure no pre-existing award or application that matches the test values
EXECUTE delete_award;
EXECUTE delete_application;

-- insert into application with funded
EXECUTE insert_application('Funded');
-- check that we have an award with that application
SELECT results_eq('find_award',
                  'find_application',
                  'Insert of Funded application generates Award');

EXECUTE delete_award;
EXECUTE delete_application;
-- insert into application with pending
EXECUTE insert_application('Pending');
-- no award with that application
SELECT results_ne('find_award','find_application','Insert of Pending application does not create award');

-- update to funded
EXECUTE update_application('Funded');
-- check we now have an application
SELECT results_eq('find_award',
                  'find_application',
                  'Update of Pending application to Funded generates Award');
-- check we can't do this twice
EXECUTE update_application('Pending');
SELECT throws_ok('EXECUTE update_application(''Funded'')','duplicate key value violates unique constraint "award_application_id_key"','We should only be able to generate an application once per award');

SELECT finish();
ROLLBACK;
