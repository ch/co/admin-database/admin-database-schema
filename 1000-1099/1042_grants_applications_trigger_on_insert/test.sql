BEGIN;

SELECT plan(3);

SELECT has_trigger('grants','application','insert_award_on_application_funded_trig','applications table has trigger to insert award');
SELECT has_function('grants','insert_award_on_application_funded','{}'::name[],'Has function grants.insert_award_on_application_funded');
SELECT function_owner_is('grants','insert_award_on_application_funded','{}'::name[],'dev','function grants.insert_award_on_application_funded owned by dev');

SELECT finish();

ROLLBACK;
