DROP TRIGGER insert_award_on_application_funded_trig ON grants.application;

CREATE OR REPLACE FUNCTION grants.insert_award_on_application_funded()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
   funded_id BIGINT;
   pending_id BIGINT;
BEGIN
   SELECT id FROM grants.application_status WHERE status = 'Funded' INTO funded_id;
   SELECT id FROM grants.application_status WHERE status = 'Pending' INTO pending_id;
   IF (
        (TG_OP = 'UPDATE' AND OLD.status_id = pending_id AND NEW.status_id = funded_id)
        OR
        (TG_OP = 'INSERT' AND NEW.status_id = funded_id)
      ) THEN
       INSERT INTO grants.award (
           application_id,
           grant_holder_id,
           funder_id,
           project_title,
           award_number,
           project_number,
           end_date,
           co_investigators,
           consumables,
           contingency,
           equipment,
           estates_costs,
           exceptional,
           facilities,
           indirect_costs,
           infrastructure_tech,
           notes,
           overheads,
           pi_costs,
           pooled_labour,
           staff,
           start_date,
           travel,
           univ_contribution 
        ) VALUES ( 
           NEW.id,
           NEW.grant_holder_id,
           NEW.funder_id,
           NEW.project_title,
           NEW.award_number::bigint,
           NEW.project_number,
           NEW.end_date,
           NEW.co_investigators,
           NEW.consumables,
           NEW.contingency,
           NEW.equipment,
           NEW.estates_costs,
           NEW.exceptional_items, -- this field has different names in the 2 Filemaker databases
           NEW.facilities,
           NEW.indirect_costs,
           NEW.infrastructure_tech,
           NEW.notes,
           NEW.overheads,
           NEW.pi_costs,
           NEW.pooled_labour,
           NEW.staff,
           NEW.start_date,
           NEW.travel,
           NEW.univ_contribution 
           );
   END IF;
   RETURN NULL;
END;
$BODY$;

ALTER FUNCTION grants.insert_award_on_application_funded()
    OWNER TO dev;

CREATE TRIGGER insert_award_on_application_funded_trig
    AFTER UPDATE OR INSERT
    ON grants.application
    FOR EACH ROW
    EXECUTE FUNCTION grants.insert_award_on_application_funded();
