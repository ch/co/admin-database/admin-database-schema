SET ROLE dev;

DROP TABLE grants.application;
DROP TABLE grants.application_status;
DROP TABLE grants.funder;
DROP TABLE grants.funder_type;
DROP TABLE grants.holder;
