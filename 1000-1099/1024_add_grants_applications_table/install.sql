SET ROLE dev;

CREATE TABLE grants.application_status (
    id bigint primary key generated always as identity,
    status varchar unique not null
);

GRANT SELECT,INSERT,UPDATE,DELETE ON grants.application_status TO grants_administration;
GRANT SELECT ON grants.application_status TO grants_ro;
CREATE TRIGGER grants_application_status_audit AFTER INSERT OR DELETE OR UPDATE ON grants.application_status FOR EACH ROW EXECUTE FUNCTION audit_generic();

INSERT INTO grants.application_status ( status ) values ( 'Pending' ), ( 'Unknown' ); -- There are others but this ensures Pending has id 1 for use as a default later. Unknown we add to take care of historic nulls.

CREATE TABLE grants.funder_type (
    id bigint primary key generated always as identity,
    funder_type varchar unique not null
);
GRANT SELECT,INSERT,UPDATE,DELETE ON grants.funder_type TO grants_administration;
GRANT SELECT ON grants.funder_type TO grants_ro;
CREATE TRIGGER grants_funder_type_audit AFTER INSERT OR DELETE OR UPDATE ON grants.funder_type FOR EACH ROW EXECUTE FUNCTION audit_generic();

CREATE TABLE grants.funder (
    id bigint primary key generated always as identity,
    funder varchar unique not null,
    funder_type_id bigint references grants.funder_type (id),
    code varchar  -- import from Filemaker but this could also be modelled as many-many as they use hybrid codes 'RX' to combine 'research' and 'x research council'
);
GRANT SELECT,INSERT,UPDATE,DELETE ON grants.funder TO grants_administration;
GRANT SELECT ON grants.funder TO grants_ro;
CREATE TRIGGER grants_funder_audit AFTER INSERT OR DELETE OR UPDATE ON grants.funder FOR EACH ROW EXECUTE FUNCTION audit_generic();

-- this is temporary (we hope!) while we resolve the ambiguous names in Filemaker
CREATE TABLE grants.holder (
    id bigint primary key generated always as identity,
    holder varchar unique not null,
    person_id bigint references public.person (id) -- when we can unambiguously identify a person for a 'holder' string, fill this in
);
GRANT SELECT,INSERT,UPDATE,DELETE ON grants.holder TO grants_administration;
GRANT SELECT ON grants.holder TO grants_ro;
CREATE TRIGGER grants_holder_audit AFTER INSERT OR DELETE OR UPDATE ON grants.holder FOR EACH ROW EXECUTE FUNCTION audit_generic();

CREATE TABLE grants.application (
    id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    grant_holder_id bigint not null references grants.holder (id), -- points to whatever we got from Filemaker
    person_id bigint references public.person (id), -- when we know for sure who the holder of this specific grant is, can fill this in. NB can't always be deduced from grant.holder as some are unknowable - eg the 3 or more Joneses who hold grants
    award_number varchar, -- nullable, and clearly not unique
    funder_id bigint references grants.funder (id), -- need a way to easily add to this, appears nullable
    project_title text,
    start_date DATE,
    end_date DATE,
    _grant_sought numeric(20,2), -- for checking import from Filemaker
    project_number varchar, -- also clearly not unique
    application_date date,
    co_investigators varchar, -- FIXME what type should this be? It can contain people from outside the department
    consumables numeric(20,2) not null default 0,
    contingency numeric(20,2) not null default 0,
    equipment numeric(20,2) not null default 0,
    estates_costs numeric(20,2) not null default 0,
    exceptional_items numeric(20,2) not null default 0,
    facilities numeric(20,2) not null default 0,
    grant_awarded varchar, -- nullable because not known until awarded, and also not numeric! Doesn't matter because we don't import this to awards table
    indirect_costs numeric(20,2) not null default 0,
    infrastructure_tech numeric(20,2) not null default 0,
    notes text,
    overheads numeric(20,2) not null default 0,
    pi_costs numeric(20,2) not null default 0,
    pooled_labour numeric(20,2) not null default 0,
    staff numeric(20,2) not null default 0,
    status_id bigint not null references grants.application_status (id) default 1, -- default to pending?
    travel numeric(20,2) not null default 0,
    univ_contribution numeric(20,2) not null default 0,
    x5_number varchar -- also clearly not unique....
)
;
GRANT SELECT,INSERT,UPDATE,DELETE ON grants.application TO grants_administration;
GRANT SELECT ON grants.application TO grants_ro;
CREATE TRIGGER grants_application_audit AFTER INSERT OR DELETE OR UPDATE ON grants.application FOR EACH ROW EXECUTE FUNCTION audit_generic();
