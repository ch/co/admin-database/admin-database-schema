DROP VIEW leavers_form.leaver_soon_or_recent_submitted;

DROP VIEW leavers_form.leaver_soon_or_recent;

CREATE VIEW leavers_form.leaver_soon_or_recent AS
SELECT
  person.id AS person_id,
  person.crsid AS person_crsid,
  person.email_address AS person_email,
(COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text AS person_name,
  person.surname AS person_surname,
  leaving_date_estimate.leaving_date AS estimated_leaving_date,
  person.leaving_date AS confirmed_leaving_date,
  leavers_form_overview_.id AS leavers_form_id,
  leavers_form_overview_.form_exists AS leavers_form_exists,
  leavers_form_overview_.last_updated AS leavers_form_last_updated
FROM
  person
  LEFT JOIN (
    SELECT
      leavers_form_overview.id,
      TRUE AS form_exists,
      leavers_form_overview.last_updated,
      leavers_form_overview.person
    FROM
      leavers_form.leavers_form_overview
    WHERE
      leavers_form_overview.deactivated IS NULL) leavers_form_overview_ ON leavers_form_overview_.person = person.id
  JOIN leavers_form.leaving_date_estimate ON leaving_date_estimate.person_id = person.id
ORDER BY
  person.surname,
((COALESCE(person.known_as, person.first_names)::text || ' '::text) || person.surname::text);

ALTER TABLE leavers_form.leaver_soon_or_recent OWNER TO dev;

GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent TO dev;

GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent TO onlineleaversform;

CREATE VIEW leavers_form.leaver_soon_or_recent_submitted AS
SELECT
  x.person_id,
  x.person_crsid,
  x.person_email,
  x.person_name,
  x.person_surname,
  x.estimated_leaving_date,
  x.confirmed_leaving_date,
  x.leavers_form_id,
  x.leavers_form_exists,
  x.leavers_form_last_updated,
  x.leavers_form_last_submitted,
  x.leavers_form_last_submitted IS NOT NULL AS leavers_form_ever_submitted,
  COALESCE(x.leavers_form_last_submitted, '-infinity'::timestamp with time zone) >= x.leavers_form_last_updated AS leavers_form_latest_updates_submitted,
  x.firstaider_qualification_up_to_date,
  x.fire_warden_in_date
FROM (
  SELECT
    leaver_soon_or_recent.person_id,
    leaver_soon_or_recent.person_crsid,
    leaver_soon_or_recent.person_email,
    leaver_soon_or_recent.person_name,
    leaver_soon_or_recent.person_surname,
    leaver_soon_or_recent.estimated_leaving_date,
    leaver_soon_or_recent.confirmed_leaving_date,
    leaver_soon_or_recent.leavers_form_id,
    COALESCE(leaver_soon_or_recent.leavers_form_exists, FALSE) AS leavers_form_exists,
    leaver_soon_or_recent.leavers_form_last_updated,
(
      SELECT
        max(leavers_form_submission.submitted_when) AS max
      FROM
        leavers_form.leavers_form_submission
      WHERE
        leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id) AS leavers_form_last_submitted,
      safety_status.firstaider_qualification_up_to_date,
      safety_status.fire_warden_in_date
    FROM
      leavers_form.leaver_soon_or_recent
    LEFT JOIN leavers_form.safety_status USING (person_id)) x
ORDER BY
  x.person_surname,
  x.person_name;

ALTER TABLE leavers_form.leaver_soon_or_recent_submitted OWNER TO dev;

GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent_submitted TO dev;

GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;

