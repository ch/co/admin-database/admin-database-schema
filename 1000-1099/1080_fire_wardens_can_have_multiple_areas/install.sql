-- add fire warden qualification date column to person
ALTER TABLE person ADD column fire_warden_qualification_date DATE;

-- create many-many join table between person and fire_warden_area to replace fire_warden table
CREATE TABLE public.mm_person_fire_warden_area (
    mm_person_fire_warden_area_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    person_id BIGINT NOT NULL REFERENCES public.person(id),
    fire_warden_area_id BIGINT NOT NULL REFERENCES public.fire_warden_area(fire_warden_area_id),
    is_primary BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE UNIQUE INDEX mm_person_fire_warden_area_index ON public.mm_person_fire_warden_area( person_id, fire_warden_area_id );
CREATE UNIQUE INDEX one_primary_firewarden_per_area ON public.mm_person_fire_warden_area ( fire_warden_area_id ) WHERE is_primary IS TRUE;

ALTER TABLE public.mm_person_fire_warden_area OWNER TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.mm_person_fire_warden_area TO safety_management;
GRANT SELECT,UPDATE (id,fire_warden_qualification_date) ON person TO safety_management;

-- insert records from fire_warden table to new records
INSERT INTO public.mm_person_fire_warden_area ( person_id, fire_warden_area_id, is_primary )
    SELECT person_id, fire_warden_area_id, is_primary
    FROM public.fire_warden;


-- recreate views that need fire warden.fire_warden_area_id
CREATE OR REPLACE VIEW www.fire_wardens_v7
 AS
 WITH pfw AS (
    SELECT
        fire_warden_area.fire_warden_area_id,
        string_agg(person_hid.person_hid, '; ') AS primary_firewarden
    FROM public.fire_warden_area
    JOIN public.mm_person_fire_warden_area USING (fire_warden_area_id)
    JOIN public.person_hid USING (person_id)
    JOIN _physical_status_v3 ps USING (person_id)
    WHERE mm_person_fire_warden_area.is_primary IS TRUE AND ps.status_id = 'Current'
    GROUP BY fire_warden_area.fire_warden_area_id
 ),
 sfw AS (
    SELECT
        fire_warden_area.fire_warden_area_id,
        string_agg(person_hid.person_hid, '; ') AS people
    FROM public.fire_warden_area
    JOIN public.mm_person_fire_warden_area USING (fire_warden_area_id)
    JOIN public.person_hid USING (person_id)
    JOIN _physical_status_v3 ps USING (person_id)
    WHERE mm_person_fire_warden_area.is_primary IS FALSE AND ps.status_id = 'Current'
    GROUP BY fire_warden_area.fire_warden_area_id
 )
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.primary_firewarden,
    sfw.people AS secondary_firewardens,
    row_number() OVER (ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END)) AS floor_sort
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN pfw USING (fire_warden_area_id)
     LEFT JOIN sfw USING (fire_warden_area_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END);

-- hotwire views and their deps
-- hotwire3."10_View/Safety/Fire_wardens"

DROP VIEW hotwire3."10_View/Safety/Fire_wardens";

DROP VIEW leavers_form.leaver_soon_or_recent_submitted;

DROP VIEW leavers_form.safety_status;

DROP VIEW public._fire_warden_status;

CREATE VIEW public._fire_warden_status
 AS
 SELECT mm_person_fire_warden_area.person_id AS person_id,
    person.fire_warden_qualification_date + '2 years'::interval AS requalification_date,
    age(person.fire_warden_qualification_date::timestamp with time zone) < '2 years'::interval AS in_date
   FROM mm_person_fire_warden_area
   JOIN person on mm_person_fire_warden_area.person_id = person.id;

ALTER TABLE public._fire_warden_status
    OWNER TO dev;

CREATE VIEW leavers_form.safety_status
 AS
 SELECT person.id AS person_id,
    firstaider.requalify_date >= 'now'::text::date AS firstaider_qualification_up_to_date,
    _fire_warden_status.in_date AS fire_warden_in_date
   FROM person
     LEFT JOIN firstaider ON firstaider.person_id = person.id
     LEFT JOIN _fire_warden_status ON person.id = _fire_warden_status.person_id
;

ALTER TABLE leavers_form.safety_status
    OWNER TO dev;

GRANT ALL ON TABLE leavers_form.safety_status TO dev;
GRANT SELECT ON TABLE leavers_form.safety_status TO onlineleaversform;

CREATE VIEW leavers_form.leaver_soon_or_recent_submitted
 AS
 SELECT x.person_id,
    x.person_crsid,
    x.person_email,
    x.person_name,
    x.person_surname,
    x.post_category,
    x.estimated_leaving_date,
    x.confirmed_leaving_date,
    x.leavers_form_id,
    x.leavers_form_exists,
    x.leavers_form_last_updated,
    x.leavers_form_last_submitted,
    x.leavers_form_last_submitted IS NOT NULL AS leavers_form_ever_submitted,
    COALESCE(x.leavers_form_last_submitted, '-infinity'::timestamp with time zone) >= x.leavers_form_last_updated AS leavers_form_latest_updates_submitted,
    x.firstaider_qualification_up_to_date,
    x.fire_warden_in_date
   FROM ( SELECT leaver_soon_or_recent.person_id,
            leaver_soon_or_recent.person_crsid,
            leaver_soon_or_recent.person_email,
            leaver_soon_or_recent.person_name,
            leaver_soon_or_recent.person_surname,
            leaver_soon_or_recent.post_category,
            leaver_soon_or_recent.estimated_leaving_date,
            leaver_soon_or_recent.confirmed_leaving_date,
            leaver_soon_or_recent.leavers_form_id,
            COALESCE(leaver_soon_or_recent.leavers_form_exists, false) AS leavers_form_exists,
            leaver_soon_or_recent.leavers_form_last_updated,
            ( SELECT max(leavers_form_submission.submitted_when) AS max
                   FROM leavers_form.leavers_form_submission
                  WHERE leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id) AS leavers_form_last_submitted,
            safety_status.firstaider_qualification_up_to_date,
            safety_status.fire_warden_in_date
           FROM leavers_form.leaver_soon_or_recent
             LEFT JOIN leavers_form.safety_status USING (person_id)) x
  ORDER BY x.person_surname, x.person_name;

ALTER TABLE leavers_form.leaver_soon_or_recent_submitted
    OWNER TO dev;

GRANT ALL ON TABLE leavers_form.leaver_soon_or_recent_submitted TO dev;
GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;

-- this requires https://gitlab.developers.cam.ac.uk/ch/co/admin-database/hotwire3/-/commit/dc3e2b1d751cd04e07d77028b37d2c1954dc6804
CREATE VIEW hotwire3."10_View/Safety/Fire_wardens"
 AS
 SELECT mm_person_fire_warden_area_id AS id,
    person_id,
    person.fire_warden_qualification_date AS qualification_date,
    fire_warden_area_id,
    fire_warden_area.building_id AS ro_building_id,
    fire_warden_area.building_region_id AS ro_building_region_id,
    fire_warden_area.building_floor_id AS ro_building_floor_id,
    mm_person_fire_warden_area.is_primary AS main_firewarden_for_area,
    ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE mm_person_fire_warden_area.person_id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
    person.email_address AS ro_email_address,
    _physical_status.status_id AS ro_physical_status,
    _fire_warden_status.requalification_date::date AS ro_requalification_date,
    _fire_warden_status.in_date AS ro_qualification_in_date,
    _latest_role.post_category AS ro_post_category,
    _latest_role.estimated_leaving_date AS ro_estimated_leaving_date,
        CASE
            WHEN _physical_status.status_id::text <> 'Current'::text THEN 'orange'::text
            WHEN _fire_warden_status.in_date = false THEN 'red'::text
            WHEN _fire_warden_status.in_date IS NULL THEN 'blue'::text
            ELSE NULL::text
        END AS _cssclass
   FROM mm_person_fire_warden_area
     LEFT JOIN _latest_role_v12 _latest_role USING (person_id)
     LEFT JOIN fire_warden_area USING (fire_warden_area_id)
     LEFT JOIN _physical_status_v3 _physical_status USING (person_id)
     LEFT JOIN _fire_warden_status USING (person_id)
     LEFT JOIN person ON person.id = mm_person_fire_warden_area.person_id;

CREATE OR REPLACE FUNCTION hotwire3.fire_wardens_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    IF NEW.id IS NOT NULL 
    THEN
        UPDATE mm_person_fire_warden_area SET
            is_primary = NEW.main_firewarden_for_area,
            fire_warden_area_id = NEW.fire_warden_area_id
        WHERE mm_person_fire_warden_area_id = NEW.id;
        UPDATE person SET
            fire_warden_qualification_date = NEW.qualification_date
        WHERE id = NEW.person_id;
    ELSE
        INSERT INTO mm_person_fire_warden_area ( 
            person_id,
            fire_warden_area_id ,
            is_primary
        ) VALUES (
            NEW.person_id,
            NEW.fire_warden_area_id,
            NEW.main_firewarden_for_area
        ) RETURNING mm_person_fire_warden_area_id INTO NEW.id;
    END IF;
    RETURN NEW;     
END;
$BODY$;

ALTER TABLE hotwire3."10_View/Safety/Fire_wardens"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Safety/Fire_wardens" TO safety_management;

CREATE OR REPLACE RULE fire_warden_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_wardens"
    DO INSTEAD
(DELETE FROM fire_warden
  WHERE (fire_warden.fire_warden_id = old.id));

CREATE TRIGGER fire_warden_upd
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Safety/Fire_wardens"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.fire_wardens_trig();


CREATE OR REPLACE VIEW hotwire3.deputy_fire_warden_hid
 AS
 SELECT person_id AS deputy_fire_warden_id,
    person_hid.person_hid AS deputy_fire_warden_hid
   FROM hotwire3.person_hid;

DROP VIEW hotwire3."10_View/Safety/Fire_warden_areas";
CREATE VIEW hotwire3."10_View/Safety/Fire_warden_areas"
 AS
 SELECT fire_warden_area.fire_warden_area_id AS id,
    fire_warden_area.name,
    fire_warden_area.location,
    fire_warden_area.building_id,
    fire_warden_area.building_region_id,
    fire_warden_area.building_floor_id,
    primary_fw.person_id,
    ARRAY( SELECT fw3.person_id
           FROM mm_person_fire_warden_area fw3
          WHERE fw3.fire_warden_area_id = fire_warden_area.fire_warden_area_id AND fw3.is_primary = false) AS deputy_fire_warden_id
   FROM fire_warden_area
     LEFT JOIN ( SELECT a2.fire_warden_area_id,
            fw2.person_id
           FROM fire_warden_area a2
             JOIN mm_person_fire_warden_area fw2 USING (fire_warden_area_id)
          WHERE fw2.is_primary = true) primary_fw USING (fire_warden_area_id);

ALTER TABLE hotwire3."10_View/Safety/Fire_warden_areas"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Fire_warden_areas" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Fire_warden_areas" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Safety/Fire_warden_areas" TO safety_management;

CREATE OR REPLACE FUNCTION hotwire3.fire_warden_area_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    IF new.id IS NOT NULL
    THEN
        UPDATE fire_warden_area SET
            name = NEW.name,
            location = NEW.location,
            building_id = NEW.building_id,
            building_region_id = NEW.building_region_id,
            building_floor_id = NEW.building_floor_id
            WHERE fire_warden_area_id = NEW.id;
    ELSE
        INSERT INTO fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) VALUES (
            NEW.name,
            NEW.location,
            NEW.building_id,
            NEW.building_region_id,
            NEW.building_floor_id
        ) RETURNING fire_warden_area_id INTO NEW.id;
    END IF;
    -- remove all fire wardens for this area
    DELETE FROM mm_person_fire_warden_area WHERE fire_warden_area_id = NEW.id;
    -- update the primary fire warden
    INSERT INTO mm_person_fire_warden_area ( person_id, fire_warden_area_id, is_primary ) VALUES ( NEW.person_id, NEW.id, TRUE );
    -- update secondary fire wardens FIXME
    INSERT INTO mm_person_fire_warden_area ( person_id, fire_warden_area_id, is_primary ) SELECT UNNEST(NEW.deputy_fire_warden_id), NEW.id, FALSE;
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.fire_warden_area_trig()
    OWNER TO dev;

CREATE OR REPLACE RULE fire_warden_area_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_warden_areas"
    DO INSTEAD
(DELETE FROM fire_warden_area
  WHERE (fire_warden_area.fire_warden_area_id = old.id));

CREATE TRIGGER fire_warden_area_upd
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Safety/Fire_warden_areas"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.fire_warden_area_trig();


UPDATE person SET fire_warden_qualification_date = fire_warden.qualification_date
FROM fire_warden
JOIN person p2 ON p2.id = fire_warden.person_id
WHERE person.id = p2.id;

CREATE TRIGGER audit_firewarden AFTER INSERT OR UPDATE OR DELETE ON public.mm_person_fire_warden_area FOR EACH ROW EXECUTE FUNCTION audit_generic();
