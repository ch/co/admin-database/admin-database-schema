-- The intranet staff list webpage is dept-only. We thus don't respect
-- the do_not_show_on_website flag here. With hindsight perhaps that should
-- have been do_not_show_on_public_websites ...
-- We still provide the facility to hide photos though; it's not a common
-- request but we have agreed to provide the capability.

CREATE OR REPLACE VIEW www.intranet_person_photo AS
  SELECT person_id, person.crsid, photo, photo_version
  FROM person_photo
  JOIN person ON person.id=person_id
  WHERE hide_photo_from_website IS false;

ALTER TABLE www.intranet_person_photo
  OWNER TO dev;

-- www_sites must *not* have access given this view includes people
-- who have opted out of the public websites.
GRANT SELECT ON TABLE www.intranet_person_photo TO www_intranet;
