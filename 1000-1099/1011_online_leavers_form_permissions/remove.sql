REVOKE UPDATE(end_date) ON TABLE public.post_history FROM onlineleaversform;
REVOKE UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship FROM onlineleaversform;
REVOKE UPDATE(end_date) ON TABLE public.part_iii_studentship FROM onlineleaversform;
REVOKE UPDATE(end_date) ON TABLE public.visitorship FROM onlineleaversform;
