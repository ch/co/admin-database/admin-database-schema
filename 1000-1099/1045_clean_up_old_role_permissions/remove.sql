SET ROLE dev;
SET ROLE slony;

GRANT SELECT ON public.person TO guacamoleuser;
GRANT SELECT ON public.system_image TO guacamoleuser;
GRANT SELECT ON public.operating_system TO guacamoleuser;
GRANT SELECT ON public.ip_address TO guacamoleuser;
GRANT SELECT ON public.research_group TO guacamoleuser;
GRANT SELECT ON public.mm_person_research_group TO guacamoleuser;
GRANT SELECT ON public.mm_system_image_ip_address TO guacamoleuser;

GRANT SELECT ON public._latest_role_v12 TO adreconcile;
GRANT SELECT ON public.research_group_hid TO adreconcile;
GRANT SELECT ON cache._latest_role_v12 TO adreconcile;
GRANT SELECT ON public.person TO adreconcile;
GRANT SELECT ON public.research_group TO adreconcile;
GRANT SELECT ON public.mm_person_research_group TO adreconcile;
