SET ROLE dev;
SET ROLE slony; -- we have to be superuser for this changeset because of bad ownerships

REVOKE SELECT ON public.person FROM guacamoleuser;
REVOKE SELECT ON public.system_image FROM guacamoleuser;
REVOKE SELECT ON public.operating_system FROM guacamoleuser;
REVOKE SELECT ON public.ip_address FROM guacamoleuser;
REVOKE SELECT ON public.research_group FROM guacamoleuser;
REVOKE SELECT ON public.mm_person_research_group FROM guacamoleuser;
REVOKE SELECT ON public.mm_system_image_ip_address FROM guacamoleuser;

REVOKE SELECT ON public._latest_role_v12 FROM adreconcile;
REVOKE SELECT ON public.research_group_hid FROM adreconcile;
REVOKE SELECT ON cache._latest_role_v12 FROM adreconcile;
REVOKE SELECT ON public.person FROM adreconcile;
REVOKE SELECT ON public.research_group FROM adreconcile;
REVOKE SELECT ON public.mm_person_research_group FROM adreconcile;
