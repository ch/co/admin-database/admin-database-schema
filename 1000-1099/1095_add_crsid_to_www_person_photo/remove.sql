DROP VIEW www.person_photo;

CREATE OR REPLACE VIEW www.person_photo AS
  SELECT person_id, photo, photo_version
  FROM person_photo
  JOIN person ON person.id=person_id
  WHERE do_not_show_on_website IS false AND
        hide_photo_from_website IS false;

ALTER TABLE www.person_photo
  OWNER TO dev;

GRANT SELECT ON TABLE www.person_photo TO www_sites;
