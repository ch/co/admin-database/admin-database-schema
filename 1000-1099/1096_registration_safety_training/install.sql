ALTER TABLE public.postgraduate_studentship_type ADD COLUMN safety_training_html text;
ALTER TABLE public.visitor_type_hid ADD COLUMN safety_training_html text;
ALTER TABLE public.erasmus_type_hid ADD COLUMN safety_training_html text;
ALTER TABLE public.staff_category ADD COLUMN safety_training_html text;

DROP VIEW registration.safety_training;

CREATE VIEW registration.safety_training
 AS
 SELECT 'e-'::text || erasmus_type_hid.erasmus_type_id::text AS id,
    erasmus_type_hid.safety_training_url,
    erasmus_type_hid.check_safety_training,
    COALESCE(safety_training_html, ('<a target="_blank" rel="noopener noreferrer" href="' || safety_training_url || '">' || safety_training_url || '</a>')
    ) AS safety_training_html
   FROM erasmus_type_hid
UNION
 SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS id,
    visitor_type_hid.safety_training_url,
    visitor_type_hid.check_safety_training,
    COALESCE(safety_training_html, ('<a target="_blank" rel="noopener noreferrer" href="' || safety_training_url || '">' || safety_training_url || '</a>')
    ) AS safety_training_html

   FROM visitor_type_hid
UNION
 SELECT 'pg-'::text || postgraduate_studentship_type.id::text AS id,
    postgraduate_studentship_type.safety_training_url,
    postgraduate_studentship_type.check_safety_training,
    COALESCE(safety_training_html, ('<a target="_blank" rel="noopener noreferrer" href="' || safety_training_url || '">' || safety_training_url || '</a>')
    ) AS safety_training_html

   FROM postgraduate_studentship_type
UNION
 SELECT 'sc-'::text || staff_category.id::text AS id,
    staff_category.safety_training_url,
    staff_category.check_safety_training,
    COALESCE(safety_training_html, ('<a target="_blank" rel="noopener noreferrer" href="' || safety_training_url || '">' || safety_training_url || '</a>')
    ) AS safety_training_html

   FROM staff_category;

ALTER TABLE registration.safety_training
    OWNER TO dev;

GRANT ALL ON TABLE registration.safety_training TO dev;
GRANT SELECT ON TABLE registration.safety_training TO starters_registration;

