DROP VIEW registration.safety_training;

CREATE VIEW registration.safety_training
 AS
 SELECT 'e-'::text || erasmus_type_hid.erasmus_type_id::text AS id,
    erasmus_type_hid.safety_training_url,
    erasmus_type_hid.check_safety_training
   FROM erasmus_type_hid
UNION
 SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS id,
    visitor_type_hid.safety_training_url,
    visitor_type_hid.check_safety_training
   FROM visitor_type_hid
UNION
 SELECT 'pg-'::text || postgraduate_studentship_type.id::text AS id,
    postgraduate_studentship_type.safety_training_url,
    postgraduate_studentship_type.check_safety_training
   FROM postgraduate_studentship_type
UNION
 SELECT 'sc-'::text || staff_category.id::text AS id,
    staff_category.safety_training_url,
    staff_category.check_safety_training
   FROM staff_category;

ALTER TABLE registration.safety_training
    OWNER TO dev;

GRANT ALL ON TABLE registration.safety_training TO dev;
GRANT SELECT ON TABLE registration.safety_training TO starters_registration;


ALTER TABLE public.postgraduate_studentship_type DROP COLUMN safety_training_html;
ALTER TABLE public.visitor_type_hid DROP COLUMN safety_training_html;
ALTER TABLE public.erasmus_type_hid DROP COLUMN safety_training_html;
ALTER TABLE public.staff_category DROP COLUMN safety_training_html;

