DROP TRIGGER cap_role_dates ON it_access.person_role;

DROP FUNCTION it_access.cap_role_dates();

CREATE OR REPLACE FUNCTION it_access.cap_role_end_dates()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
  role_time_limit interval;
  previous_role_end_date date;
BEGIN

  -- get end date of any enabling role, which becomes the effective start date of this one.
  -- if the role is one with an end_date of +infinity, we use the current date instead.
  -- A negative infinite date is left unchanged, which will lead to the extension
  -- having an end date of -infinity too, which is fine because that's a case where
  -- we should not be granting an extension until the person has sorted their records
  -- out.
  SELECT
    CASE WHEN end_date = 'infinity'::date THEN current_date
    ELSE end_date
    END AS end_date INTO previous_role_end_date
  FROM
    it_access.person_role
  WHERE
    person_id = NEW.person_id AND role_type_id = (SELECT role_type_id FROM it_access.role_type WHERE role_type = 'Current Chemist');

  -- If this is an extension role and we didn't find a previous role end date, set it to -infinity so the
  -- extension cannot go live
  IF NEW.role_type_id IN (SELECT role_type_id FROM it_access.role_type WHERE role_type like '%leaver extension')
  THEN
      previous_role_end_date = COALESCE(previous_role_end_date,'-infinity'::date);
  END IF;

  -- time limit of new role, which is given by its role_type
  SELECT
    time_limit INTO role_time_limit
  FROM
    it_access.role_type
  WHERE
    role_type.role_type_id = NEW.role_type_id;

  -- cap any end dates that go past the limit for this role
  IF NEW.end_date > COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit THEN
    NEW.end_date = COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit;
  END IF;

  RETURN NEW;
END;
$BODY$;

ALTER FUNCTION it_access.cap_role_end_dates()
    OWNER TO dev;


CREATE TRIGGER cap_end_dates
  BEFORE INSERT OR UPDATE ON it_access.person_role
  FOR EACH ROW
  EXECUTE FUNCTION it_access.cap_role_end_dates ();
