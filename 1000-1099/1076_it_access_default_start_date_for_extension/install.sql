DROP TRIGGER cap_end_dates ON it_access.person_role;

DROP FUNCTION it_access.cap_role_end_dates();

CREATE OR REPLACE FUNCTION it_access.cap_role_dates()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
  role_time_limit interval;
  previous_role_end_date date;
BEGIN

  -- for leaver extensions
  IF NEW.role_type_id IN (SELECT role_type_id FROM it_access.role_type WHERE role_type like '%leaver extension')
  THEN
      -- get end date of any enabling role, which becomes the effective start date of this one.
      -- if the role is one with an end_date of +infinity, we use the current date instead.
      -- A negative infinite date is left unchanged, which will lead to the extension
      -- having an end date of -infinity too, which is fine because that's a case where
      -- we should not be granting an extension until the person has sorted their records
      -- out.
      SELECT
          CASE WHEN end_date = 'infinity'::date THEN current_date
          ELSE end_date
      END AS end_date INTO previous_role_end_date
      FROM
          it_access.person_role
      WHERE
        person_id = NEW.person_id AND role_type_id = (SELECT role_type_id FROM it_access.role_type WHERE role_type = 'Current Chemist');

      -- If no previous end date is set use -infinity so the
      -- extension cannot go live. 
      previous_role_end_date = COALESCE(previous_role_end_date,'-infinity'::date);
      -- Override the extension start date set by the user to the previous end date.
      NEW.start_date = previous_role_end_date;
  END IF;

  -- time limit of new role, which is given by its role_type
  SELECT
    time_limit INTO role_time_limit
  FROM
    it_access.role_type
  WHERE
    role_type.role_type_id = NEW.role_type_id;

  -- if no end date was given use the maximum allowed
  IF NEW.end_date IS NULL
  THEN
    NEW.end_date = current_date + role_time_limit;
  END IF;

  -- cap any end dates that go past the limit for this role
  IF NEW.end_date > COALESCE(previous_role_end_date, NEW.start_date, current_date) + role_time_limit THEN
    NEW.end_date = COALESCE(previous_role_end_date, NEW.start_date, current_date) + role_time_limit;
  END IF;
  -- Set null start dates to current date
  IF NEW.start_date IS NULL
  THEN
      NEW.start_date = current_date;
  END IF;

  RETURN NEW;
END;
$BODY$;

ALTER FUNCTION it_access.cap_role_dates()
    OWNER TO dev;


CREATE TRIGGER cap_role_dates
  BEFORE INSERT OR UPDATE ON it_access.person_role
  FOR EACH ROW
  EXECUTE FUNCTION it_access.cap_role_dates ();
