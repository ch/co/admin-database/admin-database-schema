SET ROLE dev;

CREATE SCHEMA grants;
GRANT USAGE on SCHEMA grants to PUBLIC;
GRANT USAGE on SCHEMA grants to _pgbackup;
