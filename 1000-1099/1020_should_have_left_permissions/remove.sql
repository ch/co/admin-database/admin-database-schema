REVOKE SELECT ON TABLE apps.process_leavers_with_id_v3 FROM hr;
GRANT SELECT ON TABLE apps.process_leavers_with_id_v3 TO leavers_management;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/All"
  OWNER TO ajh221;
REVOKE ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/All" FROM dev;
REVOKE SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/All" FROM hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Students"
  OWNER TO ajh221;
REVOKE ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Students" FROM dev;
REVOKE SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Students" FROM hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Visitors"
  OWNER TO ajh221;
REVOKE ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Visitors" FROM dev;
REVOKE SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Visitors" FROM hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Staff"
  OWNER TO ajh221;
REVOKE ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Staff" FROM dev;
REVOKE SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Staff" FROM hr;
