REVOKE SELECT ON TABLE apps.process_leavers_with_id_v3 FROM leavers_management;
GRANT SELECT ON TABLE apps.process_leavers_with_id_v3 TO hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/All"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/All" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/All" TO hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Students"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Students" TO hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Visitors"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Visitors" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Visitors" TO hr;


ALTER TABLE hotwire3."10_View/People/Should_Have_Left/Staff"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Should_Have_Left/Staff" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Should_Have_Left/Staff" TO hr;
