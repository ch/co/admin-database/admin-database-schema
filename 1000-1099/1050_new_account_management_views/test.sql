BEGIN;

\set schema '\'apps\''

SELECT plan(9);


-- make lists of things to check
CREATE TEMPORARY TABLE test_views ( viewname text NOT NULL );
INSERT INTO test_views ( viewname ) 
VALUES 
        ('it_access_current_v1'),
        ('it_access_all_v1'),
        ('user_accounts_to_create_in_ad_v3')
;

-- views exist
SELECT has_view(:schema,
                 viewname,
                 format('Has %I.%I', :schema, viewname)
       )
FROM test_views;

-- ownership correct
SELECT view_owner_is(:schema, viewname, 'dev', format('Owner of %I.%I is dev', :schema, viewname))
FROM test_views;

-- perms for additional users present
SELECT table_privs_are(:schema, 'user_accounts_to_create_in_ad_v3', username, ARRAY['SELECT'], format('%I has select on %I.user_accounts_to_create_in_ad_v3', :schema, username)) FROM ( values ('ad_accounts'), ('useradder') ) as x (username) ;
-- compare with user_accounts_to_create_in_ad_v2

-- compare contents of Admitto account creation view to current version
SELECT results_eq('SELECT * FROM apps.user_accounts_to_create_in_ad_v2 ORDER BY id', 'SELECT * from apps.user_accounts_to_create_in_ad_v3 ORDER BY id','Old Admitto account creation view matches new');

SELECT * FROM finish();

ROLLBACK;
