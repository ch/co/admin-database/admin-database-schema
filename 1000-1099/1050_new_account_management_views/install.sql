SET ROLE dev;

-- A view to use for managing accounts. Similar to apps.user_accounts_to_create_in_ad_v2
-- but covers all account types and includes dates.

CREATE VIEW apps.it_access_current_v1 AS
SELECT DISTINCT
    person.id,
    person.crsid,
    btrim(COALESCE(person.known_as, person.first_names)::text)::character varying(32) AS first_names,
    btrim(person.surname::text)::character varying(32) AS surname,
    btrim(person.email_address::text)::character varying(48) AS email_address,
    account_type.account_type,
    min(person_role.start_date) AS start_date, -- this wasn't in apps.user_accounts_to_create_in_ad_v2
    max(person_role.end_date) AS end_date, -- this wasn't in apps.user_accounts_to_create_in_ad_v2 but is needed to set the expiry date as we will keep it set at all times
    string_agg(DISTINCT role_type.role_type,',') AS qualifying_roles -- all role types that grant access at the current date, for debugging
FROM it_access.person_role
    JOIN person ON person.id = person_role.person_id
    JOIN it_access.mm_role_type_account_type USING (role_type_id)
    JOIN it_access.account_type USING (account_type_id)
    JOIN it_access.role_type USING (role_type_id)
    WHERE person_role.start_date <= current_date AND person_role.end_date >= current_date AND person.email_address like '%@%' AND person.crsid IS NOT NULL
    GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address, account_type.account_type;

CREATE VIEW apps.it_access_all_v1 AS
SELECT DISTINCT
    person.id,
    person.crsid,
    btrim(COALESCE(person.known_as, person.first_names)::text)::character varying(32) AS first_names,
    btrim(person.surname::text)::character varying(32) AS surname,
    btrim(person.email_address::text)::character varying(48) AS email_address,
    account_type.account_type,
    min(person_role.start_date) AS start_date, -- this wasn't in apps.user_accounts_to_create_in_ad_v2
    max(person_role.end_date) AS end_date, -- this wasn't in apps.user_accounts_to_create_in_ad_v2 but is needed to set the expiry date as we will keep it set at all times
    string_agg(DISTINCT role_type.role_type,',') AS qualifying_roles -- all role types that grant access at the current date, for debugging
FROM it_access.person_role
    JOIN person ON person.id = person_role.person_id
    JOIN it_access.mm_role_type_account_type USING (role_type_id)
    JOIN it_access.account_type USING (account_type_id)
    JOIN it_access.role_type USING (role_type_id)
    WHERE person.email_address like '%@%' AND person.crsid IS NOT NULL
    GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address, account_type.account_type;

-- Don't replace the apps.user_accounts_to_disable_v2 view. Once we are
-- handling account extensions and remote users via the new database tables, so
-- that we always have a reliable end date for a person's accounts in the
-- database person_role table rather than this data being split between the
-- database _hr_role table and whatever date has been set on the AD account, we
-- no longer need a separate process to close the accounts. We just do a daily
-- update of the expiry dates to match what the person_role table has.

-- Replacement for apps.user_accounts_to_create_in_ad_v2
CREATE VIEW apps.user_accounts_to_create_in_ad_v3 AS
SELECT
    it_access_current_v1.id,
    it_access_current_v1.crsid,
    it_access_current_v1.first_names,
    it_access_current_v1.surname,
    it_access_current_v1.email_address,
    max(COALESCE(research_group.active_directory_container, research_group.name)::text) AS rg
FROM apps.it_access_current_v1
    LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = it_access_current_v1.id
    LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
WHERE account_type = 'Admitto'
GROUP BY it_access_current_v1.id, it_access_current_v1.crsid, it_access_current_v1.first_names, it_access_current_v1.surname, it_access_current_v1.email_address
;

GRANT SELECT ON apps.user_accounts_to_create_in_ad_v3 TO useradder, ad_accounts;
