GRANT UPDATE(end_date) ON TABLE public.post_history TO onlineleaversform;
GRANT UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship TO onlineleaversform;
GRANT UPDATE(end_date) ON TABLE public.part_iii_studentship TO onlineleaversform;
GRANT UPDATE(end_date) ON TABLE public.visitorship TO onlineleaversform;

REVOKE UPDATE(end_date) ON TABLE public.post_history FROM role_end_date_setter;
REVOKE UPDATE(end_date) ON TABLE public.erasmus_socrates_studentship FROM role_end_date_setter;
REVOKE UPDATE(end_date) ON TABLE public.part_iii_studentship FROM role_end_date_setter;
REVOKE UPDATE(end_date) ON TABLE public.visitorship FROM role_end_date_setter;
