SET ROLE dev;

-- Adding the roles below does not get reversed by the remove.sql script; it edits the
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'swipe_card_administration') THEN
      CREATE ROLE swipe_card_administration NOLOGIN;
      GRANT ro_hid TO swipe_card_administration;
   END IF;
END
$do$;

GRANT SELECT ON hotwire3."10_View/People/Swipe_Cards" TO swipe_card_administration;
GRANT SELECT,UPDATE ON hotwire3."10_View/People/MiFare_Access" TO swipe_card_administration;
