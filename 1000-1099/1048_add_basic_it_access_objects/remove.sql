SET ROLE dev;

DROP TABLE it_access.mm_role_type_account_type;

DROP TABLE it_access.hod_authorizer_role;

ALTER TABLE it_access.application
    DROP CONSTRAINT enabling_role_fk_person_role;

DROP TABLE it_access.person_role;

DROP TABLE it_access.application;

DROP FUNCTION it_access.check_enabling_role_for_extensions ();

DROP FUNCTION it_access.cap_role_end_dates ();

DROP TABLE it_access.researcher_terms;

DROP TABLE it_access.authorizer_terms;

DROP TABLE it_access.role_type;

DROP TABLE it_access.account_type;

DROP SCHEMA it_access;

