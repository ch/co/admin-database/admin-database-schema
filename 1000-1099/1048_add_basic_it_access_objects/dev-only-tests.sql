BEGIN;

\set schema '\'it_access\''
\set test_crsid1 '\'fjc55\''
\set test_crsid2 '\'spqr1\''

SELECT plan(17);

-----------------------------
-- Test person_role behaviour
-----------------------------

-- prepared statements for testing person_role behaviour

-- add a person
PREPARE add_person(varchar(32),varchar(32),varchar(8)) AS
    INSERT INTO public.person
        (surname, first_names, crsid)
    VALUES
        ($1,$2,$3)
    ;

-- ensures a specific role type for a person exists with given end date
PREPARE ensure_person_role(text,text,date) AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = $1),
            (SELECT role_type_id FROM it_access.role_type WHERE role_type = $2),
            current_date,
            $3
        )
    ON CONFLICT (person_id, role_type_id) DO UPDATE SET start_date = current_date, end_date = $3;
    ;

-- clean up roles for specific person
PREPARE remove_roles_for_person(text) AS
    DELETE FROM it_access.person_role WHERE person_id = (SELECT id FROM public.person WHERE crsid = $1)
    ;

-- add some test people
EXECUTE add_person('Test Person','Tess',:test_crsid1);
EXECUTE add_person('Test Person2','Tess',:test_crsid2);

---------------------------------------------------------
-- test capping of end dates for some non-extension roles
---------------------------------------------------------

-- Check Linux ssh role (a capped role which does not need enabling)
\set test_role_type '\'Linux ssh only\''
PREPARE get_role_end_date AS
    SELECT end_date
    FROM it_access.person_role
    WHERE role_type_id = (
        SELECT role_type_id
        FROM it_access.role_type
        WHERE role_type = :test_role_type
    ) AND person_id = (
        SELECT id
        FROM public.person
        WHERE crsid = :test_crsid1
    )
    ;


-- ensure no pre-existing roles for test user
EXECUTE remove_roles_for_person(:test_crsid1);

-- Check the cases
-- Linux ssh only capped at 12 months
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months' + interval '1 day')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months')::date],
                  'Linux ssh only capped at 12 months'
);

-- Linux ssh only for exactly 12 months allowed
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months')::date],
                  'Linux ssh only for exactly 12 months allowed'
);

-- Linux ssh for less than 12 months not changed
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months' - interval '1 day')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months' - interval '1 day')::date],
                  'Linux ssh only for less than 12 months allowed'
);

-- Check full remote access role (capped, does not need enabling)
\set test_role_type '\'Full remote access\''
DEALLOCATE get_role_end_date;
PREPARE get_role_end_date AS
    SELECT end_date
    FROM it_access.person_role
    WHERE role_type_id = (
        SELECT role_type_id
        FROM it_access.role_type
        WHERE role_type = :test_role_type
    ) AND person_id = (
        SELECT id
        FROM public.person
        WHERE crsid = :test_crsid1
    )
    ;


-- Ensure no pre-existing roles for user
EXECUTE remove_roles_for_person(:test_crsid1);

-- Check the cases
-- Full remote access for more than 12 months capped at 12 months
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months' + interval '1 day')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months')::date],
                  'Full remote access capped at 12 months'
);

-- allow full remote access of exactly 12 months
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months')::date],
                  'Full remote access of exactly 12 months unchanged'
);

-- Full remote access for less than 12 months should not be capped
EXECUTE ensure_person_role(:test_crsid1,
                           :test_role_type,
                           (current_date + interval '12 months' - interval '1 day')
);

SELECT results_eq('get_role_end_date',
                  ARRAY[(current_date + interval '12 months' - interval '1 day')::date],
                  'Full remote access end date cap unchanged if less than 12 months away'
);

---------------------------------------------------------------------
-- test extension roles behave properly, these have extra restrictions
---------------------------------------------------------------------

-- start with no roles for test users
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE remove_roles_for_person(:test_crsid2);

-- set up to add a role we can reference later by the notes column
PREPARE add_person_role_note(text,text,date,text) AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date, notes)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = $1),
            (SELECT role_type_id FROM it_access.role_type WHERE role_type = $2),
            current_date,
            $3,
            $4
        )
    ;

-- adding this extension should fail because no enabling role is given at all
PREPARE add_extension_without_enabling_role AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = :test_crsid1),
            (SELECT role_type_id FROM it_access.role_type WHERE role_type = 'First leaver extension'),
            current_date,
            current_date + interval '3 months'
        )
;
SELECT throws_like('add_extension_without_enabling_role','%violates check constraint "extensions_have_enabling_role"%','We should fail when adding an extension without an enabling role');

-- Now create a valid enabling role
EXECUTE add_person_role_note(:test_crsid1,'Current Chemist',current_date + interval '12 months','fjc55 current chemist');

-- adding an extension role with this enabling role should fail when the extension role is for a different person
PREPARE add_extension_role_for_wrong_person AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date, enabling_role_id)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = :test_crsid2),
            5,
            current_date,
            current_date + interval '1 month',
            (SELECT person_role_id FROM it_access.person_role WHERE notes = 'fjc55 current chemist')
        )
    ;
SELECT throws_like('add_extension_role_for_wrong_person','%Person for this extension does not match person for enabling role%','Should fail when adding extension where enabling role person does not match');

-- adding a role with an enabling role of type SPRI should fail because the enabling role is of the wrong type
-- add the SPRI role
EXECUTE add_person_role_note(:test_crsid1,'SPRI',current_date + interval '12 months','fjc55 SPRI');
-- set up adding the extension
PREPARE add_extension_wrong_role_type AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date, enabling_role_id)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = :test_crsid1),
            5,
            current_date,
            current_date + interval '1 month',
            (SELECT person_role_id FROM it_access.person_role WHERE notes = 'fjc55 SPRI')
        )
    ;
-- execute adding the extension
SELECT throws_like('add_extension_wrong_role_type','%Cannot grant an extension to a non Current Chemist role%','Should fail when adding extension with enabling role of wrong type');

-----------------------------------
-- test extension role date capping
-----------------------------------

-- remove all existing roles for a test user
EXECUTE remove_roles_for_person(:test_crsid1);
-- give our test person a current chemist role that started today and ends very soon, this can be used to enable extension roles
EXECUTE add_person_role_note(:test_crsid1,
                        'Current Chemist',
                        (current_date + interval '1 week'),
                        'fjc55 Current Chemist'
);
-- SQL to add an extension role using the valid enabling role we just set up
PREPARE add_person_role_enabled_by(text,text,date,text) AS
    INSERT INTO it_access.person_role
        (person_id, role_type_id, start_date, end_date, enabling_role_id)
    VALUES
        (
            (SELECT id FROM public.person WHERE crsid = $1),
            (SELECT role_type_id FROM it_access.role_type WHERE role_type = $2),
            current_date,
            $3,
            (SELECT person_role_id FROM it_access.person_role WHERE notes = $4)
        )
    ON CONFLICT (person_id, role_type_id) DO UPDATE SET start_date = current_date, end_date = $3;
    ;

-- set up for checking first extension
\set test_role_type '\'First leaver extension\''

PREPARE get_extension_end_date AS
    SELECT end_date
    FROM it_access.person_role
    WHERE role_type_id = (SELECT role_type_id FROM it_access.role_type WHERE role_type = :test_role_type)
    AND person_id = (SELECT id FROM public.person WHERE crsid = :test_crsid1)
;

-- Check the cases
-- First leaver extension of more than six months should be capped at six months after the enabling role ends
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '12 months' + interval '1 day'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '1 week' + interval '6 months')::date],
                  'First leaver extension capped at end of enabling role plus 6 months'
);

-- first leaver extension of exactly 6 months from enabling role end date should not be capped
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '1 week' + interval '6 months'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '1 week' + interval '6 months')::date],
                  'First leaver extension of exactly 6 months unchanged'
);

-- First leaver extension of less than six months should not be capped
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '3 months'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '3 months')::date],
                  'First leaver extension of less than 6 months unchanged'
);

-- Reset statements for checking second leaver extension
\set test_role_type '\'Second leaver extension\''
DEALLOCATE get_extension_end_date;
PREPARE get_extension_end_date AS
    SELECT end_date
    FROM it_access.person_role
    WHERE role_type_id = (SELECT role_type_id FROM it_access.role_type WHERE role_type = :test_role_type)
    AND person_id = (SELECT id FROM public.person WHERE crsid = :test_crsid1)

;
-- Check the cases
-- second leaver extension of more than 12 months capped at 12 months from enabling role end
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '24 months'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '1 week' + interval '12 months')::date],
                  'Second leaver extension capped at end of enabling role plus 12 months'
);



-- second leaver extension allowed to be exactly 12 months from enabling role end
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '12 months' + interval '1 week'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '1 week' + interval '12 months')::date],
                  'Second leaver extension allowed to be end of enabling role plus 12 months'
);

-- second leaver extension of less than 12 months from enabling role end should not be capped
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '9 months'),
                                   'fjc55 Current Chemist'
);

SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '9 months')::date],
                  'Second leaver extension of less than 12 months should not have end date changed'
);

-- now check enabling roles with unusual end dates
-- reset for checking first extensions
\set test_role_type '\'First leaver extension\''
DEALLOCATE get_extension_end_date;
PREPARE get_extension_end_date AS
    SELECT end_date
    FROM it_access.person_role
    WHERE role_type_id = (SELECT role_type_id FROM it_access.role_type WHERE role_type = :test_role_type)
    AND person_id = (SELECT id FROM public.person WHERE crsid = :test_crsid1)

;
-- start off with no roles for test users
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE remove_roles_for_person(:test_crsid2);

-- add an enabling role with an end date of +infinity
EXECUTE add_person_role_note(:test_crsid1,'Current Chemist','infinity'::date,'fjc55 current assistant staff');
-- add an extension using the above enabling role
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '24 months'),
                                   'fjc55 current assistant staff'
);
-- check it got its end date capped
SELECT results_eq('get_extension_end_date',
                  ARRAY[(current_date + interval '6 months')::date],
                  'First leaver extension for open ended enabling role capped at today plus 6 months'
);

-- remove those roles 
EXECUTE remove_roles_for_person(:test_crsid1);
-- add an enabling role with an end date of -infinity
EXECUTE add_person_role_note(:test_crsid1,'Current Chemist','-infinity'::date,'fjc55 left at unknown time');
-- add an extension using the above enabling role
EXECUTE add_person_role_enabled_by(:test_crsid1,
                                   :test_role_type,
                                   (current_date + interval '24 months'),
                                   'fjc55 left at unknown time'
);
-- check it has an end date of -infinity
SELECT results_eq('get_extension_end_date',
                  ARRAY['-infinity'::date],
                  'First leaver extension for person with unknown end date has end date of -infinity'
);




SELECT * FROM finish();

ROLLBACK;
