SET ROLE dev;

CREATE SCHEMA it_access;

GRANT USAGE ON SCHEMA it_access TO PUBLIC;

GRANT USAGE ON SCHEMA it_access TO _pgbackup;

CREATE TABLE it_access.role_type (
  role_type_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  role_type text UNIQUE NOT NULL,
  -- weight for a role is defined so if there are overlapping roles which
  -- clash for a person we can determine which to use
  weight int NOT NULL,
  show_on_registration boolean NOT NULL DEFAULT 't',
  description text,
  time_limit interval
);

GRANT SELECT ON it_access.role_type TO PUBLIC;

-- All have a time limit except Current Chemist, where we can tell from the database when
-- a person has gone. SPRI's time limit is generous, however.
INSERT INTO it_access.role_type (role_type, description, weight, show_on_registration, time_limit)
  VALUES ('SPRI', 'Working in SPRI', 1, 'f', interval '5 years'), -- 1
  ('Current Chemist', 'Chemistry people with current roles', 1, 'f', NULL), -- 2
  ('Linux ssh only', 'ssh to managed Linux systems via bastion host only; no ChemNet or VPN', 2, 't', interval '12 months'), -- 3
  ('Full remote access', 'Same access as Current Chemistry or SPRI people', 1, 't', interval '12 months'), -- 4
  ('First leaver extension', 'Same access as Current Chemistry or SPRI people', 1, 'f', interval '6 months'), -- 5
  -- the limit for second extension is 12 months, because this is from the Current Chemist role's end date not the first extension end date
  ('Second leaver extension', 'Same access as Current Chemistry or SPRI people', 1, 'f', interval '12 months'), -- 6
  ('Early access for incoming Current Chemist', 'Same access as Current Chemistry or SPRI people', 1, 'f', interval '3 months'), -- 7
  ('Short emergency extension', 'Same access as Current Chemistry or SPRI people', 1, 'f', interval '1 month'), -- 8
  ('Current Chemistry undergraduate', 'Same access as Current Chemistry or SPRI people', 1, 'f', interval '12 months') -- 9
;

-- tables to hold terms and conditions
CREATE TABLE it_access.researcher_terms (
  researcher_terms_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  role_type_id bigint NOT NULL REFERENCES it_access.role_type (role_type_id),
  effective timestamp NOT NULL, -- so we can display the latest version
  terms text NOT NULL
);

GRANT SELECT ON it_access.researcher_terms TO PUBLIC;

CREATE UNIQUE INDEX researcher_terms_idx ON it_access.researcher_terms (role_type_id, effective);

CREATE TABLE it_access.authorizer_terms (
  authorizer_terms_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  role_type_id bigint NOT NULL REFERENCES it_access.role_type (role_type_id),
  effective timestamp NOT NULL, -- so we can display the latest version
  terms text NOT NULL
);

GRANT SELECT ON it_access.authorizer_terms TO PUBLIC;

CREATE UNIQUE INDEX authorizer_terms_idx ON it_access.authorizer_terms (role_type_id, effective);

-- table to hold applications for remote access, hence most colums are nullable
-- as applications get built up gradually, or may not need all the columns
CREATE TABLE it_access.application (
  application_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
   -- person_id can be null as the applicant might not be in the database yet
  person_id bigint REFERENCES public.person (id),
  first_names text,
  surname text,
  email_address text,
  crsid text,
  date_of_birth date,
  person_terms_id bigint REFERENCES it_access.researcher_terms (researcher_terms_id),
  person_terms_agreed_date date,
  authorizer_terms_id bigint REFERENCES it_access.authorizer_terms (authorizer_terms_id),
  authorizer_terms_agreed_date date,
  authorizer_id bigint REFERENCES public.person (id),
  authorizer_justification text,
  -- a delegate of head of department signs off for full access
  hod_authorizer_id bigint REFERENCES public.person (id),
  -- a delegate of head of department signs off for full access
  hod_authorizer_agreed_date date,
  role_type_id bigint REFERENCES it_access.role_type (role_type_id),
  -- eventually enabling_role_id will have REFERENCES it_access.person_role(id) once that table is defined,
  -- for extensions we need to have had a previous Current Chemist role. code in applications interface can
  -- look this up and fill it in, the user doesn't need to.
  enabling_role_id bigint,
  -- end_date is not null on application because the only role type that can be open-ended is Current Chemist,
  -- which doesn't need an application
  end_date date NOT NULL,
  -- previously_registered is useful to know when a 100% remote person applies
  -- for either full remote or ssh only and doesn't give a crsid
  previously_registered boolean,
  -- filled in by the trigger that checks applications? not sure how useful this is but keep it for now.
  completed_date date
);
CREATE INDEX application_person_idx ON it_access.application(person_id);

ALTER TABLE it_access.application
  ADD CONSTRAINT only_extensions_need_a_previous_role CHECK (
    (enabling_role_id IS NULL AND (role_type_id <> 5 OR role_type_id <> 6)) 
    OR 
    (enabling_role_id IS NOT NULL AND (role_type_id = 5 OR role_type_id = 6))
  );

COMMENT ON TABLE it_access.application IS $$Table to hold applications for remote access$$;

-- list of roles that can authorize on behalf of HoD
CREATE TABLE it_access.hod_authorizer_role (
  hod_authorizer_role_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  post_history_id bigint NOT NULL REFERENCES public.post_history (id)
);

COMMENT ON TABLE it_access.hod_authorizer_role IS $$Table of roles that can authorize remote access on behalf of HoD$$;

-- add current academic secretary to the list
INSERT INTO it_access.hod_authorizer_role (post_history_id) (
  SELECT
    id
  FROM
    post_history
  WHERE
    job_title LIKE '%Academic Secretary%'
    AND start_date <= CURRENT_DATE
    AND end_date IS NULL);

-- the table to hold the roles which have been granted to a person
-- a person can have multiple roles in this table; they might get an extension approved before their 'Current Chemist' role ends
-- it should not be possible to have more than one of the same role /type/ for a person though
CREATE TABLE it_access.person_role (
  person_role_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  -- application_id can be null as people can get roles other than by formally applying
  application_id bigint REFERENCES it_access.application (application_id),
  -- enabling_role_id can be null as not everything needs one
  -- see also below where this is made a foreign key to person_role(person_role_id)
  enabling_role_id bigint,
  -- if you've got IT access, you have to be in the database so person_id NOT NULL
  person_id bigint REFERENCES public.person (id) NOT NULL,
  role_type_id bigint NOT NULL REFERENCES it_access.role_type (role_type_id),
  start_date date NOT NULL,
  -- we get around the problem of staff on open-ended contracts by setting their dates to the special date value 'infinity'
  end_date date NOT NULL,
  notes text
);

COMMENT ON TABLE it_access.person_role IS $$IT access roles that have been granted$$;
CREATE INDEX person_role_person_idx ON it_access.person_role(person_id);

ALTER TABLE it_access.person_role
  ADD CONSTRAINT extensions_have_enabling_role CHECK ((enabling_role_id IS NULL AND role_type_id <> 5 AND role_type_id <> 6) OR (enabling_role_id IS NOT NULL AND (role_type_id = 5 OR role_type_id = 6)));

-- In future we will mandate that some role types need associated applications
CREATE UNIQUE INDEX person_only_has_one_of_each_it_role ON it_access.person_role (person_id, role_type_id);

-- add the foreign key references to the person_role table in itself and it_access.application
-- which couldn't be done until both tables were defined
ALTER TABLE it_access.application
  ADD CONSTRAINT enabling_role_fk_person_role FOREIGN KEY (enabling_role_id) REFERENCES it_access.person_role (person_role_id);

ALTER TABLE it_access.person_role
  ADD CONSTRAINT enabling_role_fk_person_role FOREIGN KEY (enabling_role_id) REFERENCES it_access.person_role (person_role_id);


-- a table of the different account types we have
CREATE TABLE it_access.account_type (
  account_type_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  account_type varchar UNIQUE NOT NULL
);

ALTER TABLE it_access.account_type OWNER TO dev;
GRANT SELECT ON it_access.account_type TO PUBLIC;

INSERT INTO it_access.account_type (account_type)
  VALUES ('Admitto'), ('ChemNet'), ('Database');

-- which types of role get which types of account
CREATE TABLE it_access.mm_role_type_account_type (
    role_type_id BIGINT NOT NULL REFERENCES it_access.role_type(role_type_id),
    account_type_id BIGINT NOT NULL REFERENCES it_access.account_type(account_type_id),
    PRIMARY KEY (role_type_id, account_type_id)
);

GRANT SELECT ON it_access.mm_role_type_account_type TO PUBLIC;

-- Populate table linking role type to account type.

-- Most role types get all account types.
-- This is a difference from before; we only made database accounts for Current Chemists.
-- It makes sense to give them access though, as then they can see the data we hold about them.
INSERT INTO it_access.mm_role_type_account_type (
    role_type_id,
    account_type_id
)
    SELECT role_type_id, account_type_id
    FROM it_access.role_type, it_access.account_type
    WHERE role_type NOT IN ('Linux ssh only');

-- But 'Linux ssh only' does not get ChemNet
INSERT INTO it_access.mm_role_type_account_type (
    role_type_id,
    account_type_id
)
    SELECT role_type_id, account_type_id
    FROM it_access.role_type, it_access.account_type
    WHERE role_type = 'Linux ssh only' AND account_type <> 'ChemNet';

-- function to cap end dates for person_role inserts/updates
CREATE FUNCTION it_access.cap_role_end_dates ()
  RETURNS TRIGGER
  AS $body$
DECLARE
  role_time_limit interval;
  previous_role_end_date date;
BEGIN

  -- get end date of any enabling role, which becomes the effective start date of this one.
  -- if the role is one with an end_date of +infinity, we use the current date instead.
  -- A negative infinite date is left unchanged, which will lead to the extension
  -- having an end date of -infinity too, which is fine because that's a case where
  -- we should not be granting an extension until the person has sorted their records
  -- out.
  SELECT
    CASE WHEN end_date = 'infinity'::date THEN current_date
    ELSE end_date
    END AS end_date INTO previous_role_end_date
  FROM
    it_access.person_role
  WHERE
    person_role_id = NEW.enabling_role_id;

  -- time limit of new role, which is given by its role_type
  SELECT
    time_limit INTO role_time_limit
  FROM
    it_access.role_type
  WHERE
    role_type.role_type_id = NEW.role_type_id;

  -- cap any end dates that go past the limit for this role
  IF NEW.end_date > COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit THEN
    NEW.end_date = COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit;
  END IF;

  RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION it_access.cap_role_end_dates () OWNER TO dev;

CREATE TRIGGER cap_end_dates
  BEFORE INSERT OR UPDATE ON it_access.person_role
  FOR EACH ROW
  EXECUTE FUNCTION it_access.cap_role_end_dates ();

-- ensure extension roles and applications have a valid enabling role
CREATE FUNCTION it_access.check_enabling_role_for_extensions ()
  RETURNS TRIGGER
  AS $body$
DECLARE
  enabling_role_person_id bigint;
  enabling_role_role_type_id bigint;
BEGIN
  SELECT
    person_id,
    role_type_id INTO enabling_role_person_id,
    enabling_role_role_type_id
  FROM
    it_access.person_role
  WHERE
    person_role_id = NEW.enabling_role_id;
  IF enabling_role_person_id <> NEW.person_id THEN
    RAISE EXCEPTION 'Person for this extension does not match person for enabling role';
    RETURN NULL;
  END IF;
  IF enabling_role_role_type_id <> 2 THEN
    RAISE EXCEPTION 'Cannot grant an extension to a non Current Chemist role';
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

CREATE TRIGGER check_enabling_role
  BEFORE INSERT OR UPDATE ON it_access.application
  FOR EACH ROW
  WHEN (NEW.enabling_role_id IS NOT NULL)
  EXECUTE FUNCTION it_access.check_enabling_role_for_extensions ();

CREATE TRIGGER check_enabling_role
  BEFORE INSERT OR UPDATE ON it_access.person_role
  FOR EACH ROW
  WHEN (NEW.enabling_role_id IS NOT NULL)
  EXECUTE FUNCTION it_access.check_enabling_role_for_extensions ();


-- We need to audit the new tables that control access. The applications are audited for debugging purposes.
CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.role_type
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.application
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.person_role
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.researcher_terms
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.authorizer_terms
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.hod_authorizer_role
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.account_type
  FOR EACH ROW
  EXECUTE PROCEDURE public.audit_generic ();

CREATE TRIGGER audit
  AFTER INSERT OR UPDATE OR DELETE ON it_access.mm_role_type_account_type
    FOR EACH ROW
      EXECUTE PROCEDURE public.audit_generic ();

-- set up some initial terms for the access types:
--
-- 1 - SPRI
-- 2 - Current chemist
-- 3 - Linux ssh
-- 4 - Full remote access
-- 5 - first leaver extension
-- 6 - second leaver extension
-- 7 - pre-arrival access for someone about to be Current Chemist
-- 8 - emergency extension to allow COs to grant short extensions to work around problems
-- 9 - current undergraduate (before Part III so not in database, for people needing access for teaching theory practicals)
--
-- no terms for PI to agree for the first extension (5) or for current chemists (2) or SPRI people (1) or pre-arrival access (7) or emergency extension (8) or current undergrad (9)
-- there are terms for ssh-only (3), full remote (4), and second leaver extension (6)
INSERT INTO it_access.authorizer_terms (role_type_id, effective, terms)
  VALUES (3, '2022-09-30', 'Placeholder PI terms for ssh access only for computational chemists');

INSERT INTO it_access.authorizer_terms (role_type_id, effective, terms)
  VALUES (4, '2022-09-30', 'Placeholder PI terms for full remote access');

INSERT INTO it_access.authorizer_terms (role_type_id, effective, terms)
  VALUES (6, '2022-09-30', 'Placeholder PI terms for second extension for leavers');

-- no terms for applicant for current chemists (2) or SPRI people (1) or pre-arrival access (7) or emergency extension (8), or current undergrad (9)
-- there are terms for ssh-only (3), full remote (4), and both leavers extension (5,6)
INSERT INTO it_access.researcher_terms (role_type_id, effective, terms)
  VALUES (3, '2022-09-30', 'Placeholder terms for ssh access only for computational chemists');

INSERT INTO it_access.researcher_terms (role_type_id, effective, terms)
  VALUES (4, '2022-09-30', 'Placeholder terms for full remote access');

INSERT INTO it_access.researcher_terms (role_type_id, effective, terms)
  VALUES (5, '2022-09-30', 'Placeholder terms for first extension for leavers');

INSERT INTO it_access.researcher_terms (role_type_id, effective, terms)
  VALUES (6, '2022-09-30', 'Placeholder terms for second extension for leavers');
