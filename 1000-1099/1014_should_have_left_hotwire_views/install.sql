CREATE VIEW apps.process_leavers_with_id_v3 AS
 SELECT DISTINCT person.id,
    person.surname,
    person.first_names,
    title_hid.long_title AS title,
    person.email_address AS email,
    person_hid.person_hid AS full_name,
    supervisor_hid.supervisor_hid AS supervisor,
    supervisor.email_address AS supervisor_email,
    person_futuremost_role.estimated_leaving_date AS probable_leaving_date,
    person_futuremost_role.post_category,
    (((COALESCE(title_hid.salutation_title, person.first_names))::text || ' '::text) || (person.surname)::text) AS salutation,
    (((COALESCE(pi_title.salutation_title, supervisor.first_names))::text || ' '::text) || (supervisor.surname)::text) AS pi_salutation,
    (((((COALESCE(title_hid.long_title, ''::character varying))::text || ' '::text) || (person.first_names)::text) || ' '::text) || (person.surname)::text) AS person_title_surname,
        CASE
            WHEN (firstaider.id IS NOT NULL) THEN 't'::text
            ELSE 'f'::text
        END AS is_firstaider,
    string_agg((administrator.email_address)::text, ','::text) AS additional_contact_emails
   FROM (((((((((((public.person
     LEFT JOIN public.title_hid USING (title_id))
     JOIN public.person_hid ON ((person.id = person_hid.person_id)))
     LEFT JOIN apps.person_futuremost_role ON ((person_futuremost_role.person_id = person.id)))
     LEFT JOIN public.person supervisor ON ((person_futuremost_role.supervisor_id = supervisor.id)))
     LEFT JOIN public.supervisor_hid USING (supervisor_id))
     LEFT JOIN public.title_hid pi_title ON ((supervisor.title_id = pi_title.title_id)))
     LEFT JOIN public._physical_status_v3 ps ON ((person.id = ps.person_id)))
     LEFT JOIN public.firstaider ON ((person.id = firstaider.person_id)))
     LEFT JOIN public.mm_person_research_group ON ((person.id = mm_person_research_group.person_id)))
     LEFT JOIN public.research_group ON ((mm_person_research_group.research_group_id = research_group.id)))
     LEFT JOIN public.person administrator ON ((research_group.administrator_id = administrator.id)))
  WHERE (((ps.status_id)::text = 'Current'::text) AND ((person_futuremost_role.post_category)::text <> 'Assistant staff'::text) AND ((person_futuremost_role.post_category)::text <> 'Part III'::text))
  GROUP BY person.id, person.surname, person.first_names, title_hid.long_title, person.email_address, person_hid.person_hid, supervisor_hid.supervisor_hid, supervisor.email_address, person_futuremost_role.estimated_leaving_date, person_futuremost_role.post_category, (((COALESCE(title_hid.salutation_title, person.first_names))::text || ' '::text) || (person.surname)::text), (((COALESCE(pi_title.salutation_title, supervisor.first_names))::text || ' '::text) || (supervisor.surname)::text), (((((COALESCE(title_hid.long_title, ''::character varying))::text || ' '::text) || (person.first_names)::text) || ' '::text) || (person.surname)::text),
        CASE
            WHEN (firstaider.id IS NOT NULL) THEN 't'::text
            ELSE 'f'::text
        END
  ORDER BY person.surname, person.first_names;

ALTER TABLE apps.process_leavers_with_id_v3 OWNER TO dev;

GRANT SELECT ON TABLE apps.process_leavers_with_id_v3 TO leavers_management;




CREATE VIEW hotwire3."10_View/People/Should_Have_Left/All" AS
select * from apps.process_leavers_with_id_v3 where probable_leaving_date <= current_date;

CREATE VIEW hotwire3."10_View/People/Should_Have_Left/Students" AS
select * from apps.process_leavers_with_id_v3 where probable_leaving_date <= current_date
AND (post_category = 'MPhil' OR post_category = 'PhD' OR post_category = 'Erasmus exchange student')
;

CREATE VIEW hotwire3."10_View/People/Should_Have_Left/Visitors" AS
select * from apps.process_leavers_with_id_v3 where probable_leaving_date <= current_date
AND post_category Like 'Visitor%'
;

CREATE VIEW hotwire3."10_View/People/Should_Have_Left/Staff" AS
select * from apps.process_leavers_with_id_v3 where probable_leaving_date <= current_date
AND post_category not Like 'Visitor%'
AND NOT (post_category = 'MPhil' OR post_category = 'PhD' OR post_category = 'Erasmus exchange student')
;

