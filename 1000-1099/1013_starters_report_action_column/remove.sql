DROP VIEW apps.people_who_started;

CREATE OR REPLACE VIEW apps.people_who_started
 AS
 SELECT person.surname,
    person.email_address,
    person.first_names,
    person.date_of_birth,
    cambridge_college.name AS college,
    title_hid.long_title AS title,
    person.crsid,
    person.previous_surname,
    gender_hid.gender_hid AS gender,
    person.arrival_date,
    person_futuremost_role.post_category,
    person_futuremost_role.estimated_leaving_date,
    _physical_status_changelog.stamp AS status_changed
   FROM _physical_status_changelog
     JOIN person ON _physical_status_changelog.person_id = person.id
     LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
     JOIN title_hid USING (title_id)
     LEFT JOIN gender_hid USING (gender_id)
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN apps.person_futuremost_role ON person_futuremost_role.person_id = person.id
  WHERE _physical_status_changelog.new_status_id::text = 'Current'::text AND _physical_status_v3.status_id::text = 'Current'::text AND person_futuremost_role.post_category_id <> 'v-17'::text
  ORDER BY _physical_status_changelog.stamp;

ALTER TABLE apps.people_who_started
    OWNER TO dev;

GRANT ALL ON TABLE apps.people_who_started TO dev;
GRANT SELECT ON TABLE apps.people_who_started TO leavers_management;

