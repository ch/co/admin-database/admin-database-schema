CREATE OR REPLACE VIEW public._latest_employment
 AS
 SELECT l.person_id,
    l.start_date,
    l.intended_end_date,
    l.end_date,
    l.supervisor_id,
    l.mentor_id,
    l.post_category_id,
    l.post_category,
    l.status,
    l.funding,
    l.fees_funding,
    l.research_grant_number,
    l.paid_by_university,
    l.hesa_leaving_code,
    l.role_id,
    l.role_tablename,
    l.role_target_viewname
   FROM _all_roles_v11 l
     JOIN ( SELECT lengths.person_id,
            lengths.fixed_start_date,
            max(lengths.length_of_role) AS max_length
           FROM _all_roles_v11 lengths
             JOIN ( SELECT starts.person_id,
                    max(starts.fixed_start_date) AS max_start
                   FROM _all_roles_v11 starts
                  WHERE starts.fixed_start_date <= now()
                  GROUP BY starts.person_id) newest_start ON newest_start.person_id = lengths.person_id AND newest_start.max_start = lengths.fixed_start_date
          GROUP BY lengths.person_id, lengths.fixed_start_date) latest ON latest.person_id = l.person_id AND latest.max_length = l.length_of_role AND latest.fixed_start_date = l.fixed_start_date
  WHERE l.role_tablename = 'post_history'::text
  ORDER BY l.person_id;
