CREATE OR REPLACE VIEW _latest_employment AS
WITH employed AS (
    SELECT distinct person_id
    FROM public.post_history
)
SELECT
    most_recent.person_id,
    most_recent.start_date,
    most_recent.intended_end_date,
    most_recent.end_date,
    most_recent.supervisor_id,
    most_recent.mentor_id,
    'sc-'::text ||most_recent.staff_category_id::text as post_category_id,
    staff_category.category AS post_category,
    role_status(most_recent.start_date,most_recent.end_date,most_recent.funding_end_date,most_recent.force_role_status_to_past,'post_history')::VARCHAR(10) as status,
    most_recent.filemaker_funding AS funding,
    NULL::varchar AS fees_funding,
    most_recent.research_grant_number,
    most_recent.paid_by_university,
    NULL::varchar(2) AS hesa_leaving_code,
    most_recent.id as role_id,
    'post_history'::text AS role_tablename,
    '10_View/Roles/Post_History'::text AS role_target_viewname
FROM employed
JOIN LATERAL (
    SELECT *
    FROM public.post_history
    WHERE post_history.person_id = employed.person_id AND (
        role_status(post_history.start_date,post_history.end_date, post_history.funding_end_date, post_history.force_role_status_to_past, 'post_history') <> 'Future'
        )
    ORDER BY post_history.start_date DESC NULLS LAST, (COALESCE(end_date,funding_end_date,intended_end_date)-start_date) DESC NULLS LAST
    LIMIT 1
) most_recent ON true
JOIN public.staff_category ON most_recent.staff_category_id = staff_category.id
ORDER BY employed.person_id;
