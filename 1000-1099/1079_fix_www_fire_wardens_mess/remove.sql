CREATE VIEW www.fire_wardens_v1
 AS
 SELECT person_hid.person_hid,
    fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid
   FROM fire_warden fw
     JOIN person_hid USING (person_id)
     JOIN _physical_status_v3 USING (person_id)
     JOIN fire_warden_area fwa USING (fire_warden_area_id)
     JOIN _fire_warden_status s USING (fire_warden_id)
     JOIN building_hid USING (building_id)
     JOIN building_floor_hid USING (building_floor_id)
  WHERE s.in_date = true AND _physical_status_v3.status_id::text = 'Current'::text
  ORDER BY fwa.name;

ALTER TABLE www.fire_wardens_v1
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v1
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v1 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v1 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v1 TO www_sites;



CREATE VIEW www.fire_wardens_v2
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.person_hid AS primary_firewarden,
    sfw.people AS secondary_firewardens
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT person_hid.person_hid,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id);

ALTER TABLE www.fire_wardens_v2
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v2
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v2 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v2 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v2 TO www_sites;



CREATE VIEW www.fire_wardens_v3
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.person_hid AS primary_firewarden,
    sfw.people AS secondary_firewardens
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT person_hid.person_hid,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _fire_warden_status USING (fire_warden_id)
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _fire_warden_status.in_date = true AND _physical_status_v3.status_id::text = 'Current'::text) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _fire_warden_status USING (fire_warden_id)
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _fire_warden_status.in_date = true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id);

ALTER TABLE www.fire_wardens_v3
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v3
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v3 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v3 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v3 TO www_sites;


CREATE VIEW www.fire_wardens_v4
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.person_hid AS primary_firewarden,
    sfw.people AS secondary_firewardens
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT person_hid.person_hid,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _physical_status_v3.status_id::text = 'Current'::text) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id);

ALTER TABLE www.fire_wardens_v4
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v4
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v4 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v4 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v4 TO www_sites;



CREATE VIEW www.fire_wardens_v5
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.primary_firewarden,
    sfw.people AS secondary_firewardens
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS primary_firewarden,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id);

ALTER TABLE www.fire_wardens_v5
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v5
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v5 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v5 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v5 TO www_sites;

CREATE VIEW www.fire_wardens_v6
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.primary_firewarden,
    sfw.people AS secondary_firewardens,
    row_number() OVER (ORDER BY building_hid.building_id, fwa.name) AS floor_sort
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS primary_firewarden,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_agg(person_hid.person_hid) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id)
  ORDER BY building_hid.building_id, fwa.name;

ALTER TABLE www.fire_wardens_v6
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v6
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v6 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v6 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v6 TO www_sites;


DROP VIEW www.fire_wardens_v7;

CREATE OR REPLACE VIEW www.fire_wardens_v7
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.primary_firewarden,
    sfw.people AS secondary_firewardens,
    row_number() OVER (ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END)) AS floor_sort
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT array_to_string(array_agg(person_hid.person_hid), '; '::text) AS primary_firewarden,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_to_string(array_agg(person_hid.person_hid), '; '::text) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END);

ALTER TABLE www.fire_wardens_v7
    OWNER TO cen1001;
COMMENT ON VIEW www.fire_wardens_v7
    IS 'see ticket 148999';

GRANT ALL ON TABLE www.fire_wardens_v7 TO cen1001;
GRANT SELECT ON TABLE www.fire_wardens_v7 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v7 TO www_sites;

