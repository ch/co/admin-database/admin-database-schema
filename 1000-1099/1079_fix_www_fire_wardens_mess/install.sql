DROP VIEW www.fire_wardens_v1;
DROP VIEW www.fire_wardens_v2;
DROP VIEW www.fire_wardens_v3;
DROP VIEW www.fire_wardens_v4;
DROP VIEW www.fire_wardens_v5;
DROP VIEW www.fire_wardens_v6;

DROP VIEW www.fire_wardens_v7;

CREATE OR REPLACE VIEW www.fire_wardens_v7
 AS
 SELECT fwa.name,
    fwa.location,
    building_hid.building_hid,
    building_floor_hid.building_floor_hid,
    pfw.primary_firewarden,
    sfw.people AS secondary_firewardens,
    row_number() OVER (ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END)) AS floor_sort
   FROM fire_warden_area fwa
     LEFT JOIN building_hid USING (building_id)
     LEFT JOIN building_floor_hid USING (building_floor_id)
     LEFT JOIN ( SELECT array_to_string(array_agg(person_hid.person_hid), '; '::text) AS primary_firewarden,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary = true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) pfw USING (fire_warden_area_id)
     LEFT JOIN ( SELECT array_to_string(array_agg(person_hid.person_hid), '; '::text) AS people,
            fire_warden.fire_warden_area_id
           FROM fire_warden
             JOIN _physical_status_v3 USING (person_id)
             JOIN person_hid USING (person_id)
          WHERE fire_warden.is_primary <> true AND _physical_status_v3.status_id::text = 'Current'::text
          GROUP BY fire_warden.fire_warden_area_id) sfw USING (fire_warden_area_id)
  ORDER BY ((building_hid.building_id * 100)::numeric +
        CASE
            WHEN building_floor_hid.building_floor_hid::text = 'Basement'::text THEN 1::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'Main building'::text THEN '-1.5'::numeric
            WHEN building_floor_hid.building_floor_hid::text = 'Mezzanine'::text AND building_hid.building_hid::text = 'UCC'::text THEN '-2.5'::numeric
            ELSE ('-1'::integer * building_floor_hid.building_floor_id)::numeric
        END);

ALTER TABLE www.fire_wardens_v7
    OWNER TO dev;
COMMENT ON VIEW www.fire_wardens_v7
    IS 'see ticket 148999';

GRANT SELECT ON TABLE www.fire_wardens_v7 TO web_editors;
GRANT SELECT ON TABLE www.fire_wardens_v7 TO www_sites;

