CREATE ROLE role_end_date_setter;
GRANT role_end_date_setter TO dev;

CREATE OR REPLACE FUNCTION leavers_form.set_role_end_date_do()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    SECURITY DEFINER
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
   _sql text;
  begin
    SET search_path = public, pg_temp;
    if NEW.role_id != OLD.role_id OR NEW.role_type != OLD.role_type
    then
      RAISE 'Not possible to change role id or type';
    end if;

    if NEW.role_id is not null AND NEW.role_type is not null
    then
      if NEW.role_type != 'postgraduate_studentship' then
        _sql = format('UPDATE public.%1$I SET end_date = $1 WHERE id = $2'
                   , NEW.role_type);
        EXECUTE _sql USING NEW.end_date, NEW.role_id;
        return NEW;
      else
        RAISE 'Cannot set end date for postgraduate_studentship';
      end if;
    end if;
    return NULL;
  end;
$BODY$;

ALTER FUNCTION leavers_form.set_role_end_date_do()
    OWNER TO role_end_date_setter;

REVOKE ALL ON FUNCTION leavers_form.set_role_end_date_do() FROM public;
GRANT EXECUTE ON FUNCTION leavers_form.set_role_end_date_do() TO onlineleaversform;

GRANT SELECT ON _physical_status_v3 TO onlineleaversform;
GRANT SELECT, UPDATE ON _hr_role TO role_end_date_setter;
