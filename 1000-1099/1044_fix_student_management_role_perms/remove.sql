REVOKE SELECT,INSERT,UPDATE,DELETE ON public.postgraduate_studentship FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.person FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public._physical_status_v3 FROM student_management;
REVOKE INSERT ON public._physical_status_changelog FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_dept_telephone_number FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_room FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_nationality FROM student_management;
REVOKE SELECT,INSERT,UPDATE,DELETE ON public.mm_person_research_group FROM student_management;
