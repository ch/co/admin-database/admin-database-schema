GRANT SELECT,INSERT,UPDATE,DELETE ON public.postgraduate_studentship TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.person TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public._physical_status_v3 TO student_management;
GRANT INSERT ON public._physical_status_changelog TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_dept_telephone_number TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_room TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_nationality TO student_management;
GRANT SELECT,INSERT,UPDATE,DELETE ON public.mm_person_research_group TO student_management;
