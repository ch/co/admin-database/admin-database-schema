CREATE OR REPLACE VIEW apps.xymon_hardware_info
 AS
 SELECT ip_address.hostname,
    hardware.date_purchased,
    hardware.warranty_end_date
   FROM hardware
     JOIN system_image ON hardware.id = system_image.hardware_id
     JOIN mm_system_image_ip_address mm ON system_image.id = mm.system_image_id
     JOIN ip_address ON ip_address.id = mm.ip_address_id
  WHERE ip_address.hostname IS NOT NULL;

ALTER TABLE apps.xymon_hardware_info
    OWNER TO dev;

GRANT SELECT ON TABLE apps.xymon_hardware_info TO cos;
GRANT ALL ON TABLE apps.xymon_hardware_info TO dev;
GRANT SELECT ON TABLE apps.xymon_hardware_info TO osbuilder;

