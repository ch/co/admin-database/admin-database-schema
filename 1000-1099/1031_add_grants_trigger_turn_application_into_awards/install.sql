SET ROLE dev;

CREATE FUNCTION grants.insert_award_on_application_funded() RETURNS TRIGGER AS
$$
DECLARE
   funded_id BIGINT;
   pending_id BIGINT;
BEGIN
   SELECT id FROM grants.application_status WHERE status = 'Funded' INTO funded_id;
   SELECT id FROM grants.application_status WHERE status = 'Pending' INTO pending_id;
   IF OLD.status_id = pending_id AND NEW.status_id = funded_id THEN
       INSERT INTO grants.award (
           application_id,
           grant_holder_id,
           funder_id,
           project_title,
           award_number,
           project_number,
           end_date,
           co_investigators,
           consumables,
           contingency,
           equipment,
           estates_costs,
           exceptional,
           facilities,
           indirect_costs,
           infrastructure_tech,
           notes,
           overheads,
           pi_costs,
           pooled_labour,
           staff,
           start_date,
           travel,
           univ_contribution 
        ) VALUES ( 
           OLD.id,
           NEW.grant_holder_id,
           NEW.funder_id,
           NEW.project_title,
           NEW.award_number::bigint,
           NEW.project_number,
           NEW.end_date,
           NEW.co_investigators,
           NEW.consumables,
           NEW.contingency,
           NEW.equipment,
           NEW.estates_costs,
           NEW.exceptional_items, -- this field has different names in the 2 Filemaker databases
           NEW.facilities,
           NEW.indirect_costs,
           NEW.infrastructure_tech,
           NEW.notes,
           NEW.overheads,
           NEW.pi_costs,
           NEW.pooled_labour,
           NEW.staff,
           NEW.start_date,
           NEW.travel,
           NEW.univ_contribution 
           );
   END IF;
   RETURN NULL;
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION grants.insert_award_on_application_funded() OWNER TO dev;

CREATE TRIGGER insert_award_on_application_funded_trig AFTER UPDATE ON grants.application FOR EACH ROW EXECUTE FUNCTION grants.insert_award_on_application_funded();
