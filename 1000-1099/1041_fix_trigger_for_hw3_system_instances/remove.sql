CREATE OR REPLACE FUNCTION hotwire3.system_image_all_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 
  declare
          v_system_image_id BIGINT;
          v_hardware_id BIGINT;
  begin
 
  IF NEW.id IS NOT NULL THEN
 
    v_system_image_id := NEW.id;
    v_hardware_id := NEW.hardware_pk;
 
    UPDATE hardware SET
         manufacturer=NEW.manufacturer, 
         model=NEW.model, 
         name=NEW.hardware_name,
         hardware_type_id=NEW.hardware_type_id, 
         asset_tag = NEW.asset_tag,
         serial_number = NEW.serial_number,
         monitor_serial_number = NEW.monitor_serial_number,
         room_id=NEW.room_id,
         owner_id=NEW.owner_id,
         comments=NEW.hardware_comments::varchar(1000),
         date_purchased = NEW.date_hardware_purchased,
         warranty_end_date = NEW.hardware_warranty_end_date,
         delete_after_date = NEW.hardware_delete_after_date
    WHERE hardware.id = v_hardware_id;
 
    UPDATE system_image SET
          user_id=NEW.user_id,
          research_group_id=NEW.research_group_id,
          operating_system_id = NEW.operating_system_id,
          wired_mac_1 = NEW.wired_mac_1, 
          wired_mac_2 = NEW.wired_mac_2,
          wireless_mac = NEW.wireless_mac,
          comments = NEW.system_image_comments::varchar(1000),
          hobbit_tier = NEW.hobbit_tier,
          hobbit_flags = NEW.hobbit_flags,
          is_development_system = NEW.is_development_system,
	  reboot_policy_id = coalesce(NEW.reboot_policy_id, 1),
          expiry_date = NEW.system_image_expiry_date,
          is_managed_mac = NEW.is_managed_macintosh
    WHERE system_image.id = NEW.id;
 
  ELSE
 
    v_system_image_id := nextval('system_image_id_seq');
    v_hardware_id := nextval('hardware_id_seq');
 
    INSERT INTO hardware
          (manufacturer, model, name, hardware_type_id, asset_tag, serial_number, monitor_serial_number, room_id, owner_id, comments, date_purchased, warranty_end_date, delete_after_date )
    VALUES
          (NEW.manufacturer, NEW.model, NEW.hardware_name, NEW.hardware_type_id, NEW.asset_tag, NEW.serial_number, NEW.monitor_serial_number,
           NEW.room_id, NEW.owner_id, NEW.hardware_comments::varchar(1000), NEW.date_hardware_purchased, NEW.hardware_warranty_end_date, NEW.hardware_delete_after_date) returning id INTO v_hardware_id;
 
    INSERT INTO system_image
          (hardware_id, operating_system_id, wired_mac_1, wired_mac_2,
  wireless_mac,
  comments,user_id,research_group_id,hobbit_tier,hobbit_flags,is_development_system,reboot_policy_id,expiry_date,is_managed_mac)
    VALUES
          (v_hardware_id, NEW.operating_system_id,
           NEW.wired_mac_1, NEW.wired_mac_2, NEW.wireless_mac,
           NEW.system_image_comments::varchar(1000),NEW.user_id,NEW.research_group_id,NEW.hobbit_tier,NEW.hobbit_flags,NEW.is_development_system,coalesce(NEW.reboot_policy_id,1),NEW.system_image_expiry_date,NEW.is_managed_macintosh) RETURNING id into v_system_image_id;
 
  END IF;
  PERFORM fn_mm_array_update(NEW.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,v_system_image_id);
 
  return NEW;
 
  end;
 
$BODY$;
