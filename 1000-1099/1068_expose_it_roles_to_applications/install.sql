ALTER TABLE it_access.role_type ADD COLUMN internal_use_only BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE it_access.role_type ADD COLUMN rules TEXT;

UPDATE it_access.role_type SET internal_use_only = TRUE WHERE role_type = ANY(ARRAY['Database-Admitto reconciliation','Linux ssh only','Current Chemist']);
-- These are not quite the same as the rules we intend to use in the future; this is for the transition period
UPDATE it_access.role_type SET rules = 'Grant only with permission from a current PI' WHERE role_type = 'Full remote access';
UPDATE it_access.role_type SET rules = 'The time limit is measured from the date the person left Chemistry. No extra permission needed.' WHERE role_type = 'First leaver extension';
UPDATE it_access.role_type SET rules = 'The time limit is measured from the date the person left Chemistry, not the end of the first extension. Grant only with permission from a current PI.' WHERE role_type = 'Second leaver extension';
UPDATE it_access.role_type SET rules = 'For Chemistry undergraduates other than Part III who need access for their studies. Grant only with permission from a current PI.' WHERE role_type = 'Current Chemistry undergraduate';

CREATE VIEW hotwire3."10_View/IT_Accounts/IT_Access_Roles" AS
    SELECT
        role_type_id AS id,
        role_type,
        weight,
        description,
        rules,
        time_limit,
        internal_use_only
    FROM it_access.role_type;

ALTER VIEW hotwire3."10_View/IT_Accounts/IT_Access_Roles" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/IT_Accounts/IT_Access_Roles" TO cos;

CREATE VIEW apps.it_access_roles AS
    SELECT 
        role_type_id,
        role_type,
        weight,
        description,
        rules,
        time_limit
    FROM it_access.role_type
    WHERE internal_use_only = FALSE;

ALTER VIEW apps.it_access_roles OWNER TO dev;
GRANT SELECT ON apps.it_access_roles TO PUBLIC;
