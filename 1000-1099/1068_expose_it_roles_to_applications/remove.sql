DROP VIEW apps.it_access_roles;
DROP VIEW hotwire3."10_View/IT_Accounts/IT_Access_Roles";
ALTER TABLE it_access.role_type DROP COLUMN internal_use_only;
ALTER TABLE it_access.role_type DROP COLUMN rules;
