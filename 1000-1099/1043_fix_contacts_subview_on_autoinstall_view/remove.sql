-- View: hotwire3."10_View/Computers/Autoinstallation_for_COs"

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Autoinstallation_for_COs"
 AS
 SELECT system_image.id,
    hardware.name AS hardware_name,
    hardware.hardware_type_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wireless_mac,
    system_image.architecture_id,
    ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    ARRAY( SELECT mm_system_image_installer_tag.installer_tag_id
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id) AS installer_tag_id,
    ARRAY( SELECT mm_system_image_software_package_jumpstart.software_package_id
           FROM mm_system_image_software_package_jumpstart
          WHERE mm_system_image_software_package_jumpstart.system_image_id = system_image.id) AS software_package_id,
    system_image.reinstall_on_next_boot,
    COALESCE(system_image.ansible_branch, 'master'::character varying) AS ansible_branch,
    hardware.room_id,
    system_image.user_id,
    hardware.owner_id,
    system_image.research_group_id,
    research_group.fai_class_id AS ro_fai_class_id,
    system_image.host_system_image_id,
    system_image.comments AS system_image_comments,
    hardware.comments AS hardware_comments,
    system_image.netboot_id,
        CASE
            WHEN system_image.reinstall_on_next_boot = true THEN true
            WHEN system_image.was_managed_machine_on > ('now'::text::date - '3 mons'::interval) THEN true
            WHEN system_image.is_managed_mac = true THEN true
            ELSE false
        END AS ro_is_managed_machine,
    system_image.hardware_id AS hardware_pk,
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses",
    hotwire3.to_hwsubviewb('10_View/Computers/System_Image/_MAC_to_VLAN_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/MAC_to_VLAN'::character varying, 'mac'::character varying, 'mac'::character varying) AS "VLAN_assignments"
   FROM system_image
     JOIN hardware ON hardware.id = system_image.hardware_id
     JOIN research_group ON system_image.research_group_id = research_group.id;
