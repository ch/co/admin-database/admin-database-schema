CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent_submitted AS
 SELECT
       x.person_id,
       x.person_crsid,
       x.person_email,
       x.person_name,
       x.person_surname,
       x.estimated_leaving_date,
       x.confirmed_leaving_date,
       x.leavers_form_id,
       x.leavers_form_exists,
       x.leavers_form_last_updated,
       x.leavers_form_last_submitted,
       x.leavers_form_last_submitted >= x.leavers_form_last_updated AS leavers_form_latest_updates_submitted,
       x.firstaider_qualification_up_to_date,
       x.fire_warden_in_date
 FROM
 (SELECT leaver_soon_or_recent.person_id,
    leaver_soon_or_recent.person_crsid,
    leaver_soon_or_recent.person_email,
    leaver_soon_or_recent.person_name,
    leaver_soon_or_recent.person_surname,
    leaver_soon_or_recent.estimated_leaving_date,
    leaver_soon_or_recent.confirmed_leaving_date,
    leaver_soon_or_recent.leavers_form_id,
    COALESCE(leaver_soon_or_recent.leavers_form_exists, false) AS leavers_form_exists,
    leaver_soon_or_recent.leavers_form_last_updated,
    (SELECT MAX(leavers_form_submission.submitted_when)
           FROM leavers_form.leavers_form_submission
          WHERE (leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id)) AS leavers_form_last_submitted,
    safety_status.firstaider_qualification_up_to_date,
    safety_status.fire_warden_in_date
   FROM leavers_form.leaver_soon_or_recent
   LEFT JOIN leavers_form.safety_status
   USING (person_id)
  ) x
  ORDER BY x.person_surname, x.person_name;


ALTER TABLE leavers_form.leaver_soon_or_recent_submitted OWNER TO dev;

GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;
