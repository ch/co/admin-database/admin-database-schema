CREATE OR REPLACE VIEW leavers_form.leaver_soon_or_recent_submitted AS
 SELECT leaver_soon_or_recent.person_id,
    leaver_soon_or_recent.person_crsid,
    leaver_soon_or_recent.person_email,
    leaver_soon_or_recent.person_name,
    leaver_soon_or_recent.person_surname,
    leaver_soon_or_recent.estimated_leaving_date,
    leaver_soon_or_recent.confirmed_leaving_date,
    leaver_soon_or_recent.leavers_form_id,
    COALESCE(leaver_soon_or_recent.leavers_form_exists, false) AS leavers_form_exists,
    leaver_soon_or_recent.leavers_form_last_updated,
    (SELECT MAX(leavers_form_submission.submitted_when)
           FROM leavers_form.leavers_form_submission
          WHERE (leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id)) AS leavers_form_last_submitted,
    (EXISTS ( SELECT 1
           FROM leavers_form.leavers_form_submission
          WHERE ((leavers_form_submission.leavers_form_id = leaver_soon_or_recent.leavers_form_id) AND (leavers_form_submission.submitted_when >= leaver_soon_or_recent.leavers_form_last_updated)))) AS leavers_form_latest_updates_submitted,
    safety_status.firstaider_qualification_up_to_date,
    safety_status.fire_warden_in_date
   FROM leavers_form.leaver_soon_or_recent
   LEFT JOIN leavers_form.safety_status
   USING (person_id)
  ORDER BY leaver_soon_or_recent.person_surname, leaver_soon_or_recent.person_name;


ALTER TABLE leavers_form.leaver_soon_or_recent_submitted OWNER TO dev;

GRANT SELECT ON TABLE leavers_form.leaver_soon_or_recent_submitted TO onlineleaversform;
