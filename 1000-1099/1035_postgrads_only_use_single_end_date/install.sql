DROP VIEW hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Postgrad_Students"
 AS
 WITH a AS (
         SELECT postgraduate_studentship.id,
            postgraduate_studentship.person_id,
            person.surname AS ro_surname,
            person.first_names AS ro_first_names,
            role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10) AS ro_role_status,
            person.email_address AS ro_email_address,
            postgraduate_studentship.postgraduate_studentship_type_id,
            postgraduate_studentship.part_time,
            postgraduate_studentship.first_supervisor_id AS supervisor_id,
            postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
            postgraduate_studentship.external_co_supervisor,
            postgraduate_studentship.substitute_supervisor_id,
            postgraduate_studentship.first_mentor_id AS mentor_id,
            postgraduate_studentship.second_mentor_id AS co_mentor_id,
            postgraduate_studentship.external_mentor,
            postgraduate_studentship.university,
            person.cambridge_college_id,
            postgraduate_studentship.paid_through_payroll,
            postgraduate_studentship.emplid_number,
            postgraduate_studentship.start_date,
            person.leaving_date as dept_leaving_date,
            postgraduate_studentship.intended_end_date as role_intended_end_date,
            postgraduate_studentship.end_date as role_end_date,
            postgraduate_studentship.force_role_status_to_past,
-- add status here
            postgraduate_studentship.filemaker_funding::text AS funding,
            postgraduate_studentship.filemaker_fees_funding::text AS fees_funding,
            postgraduate_studentship.funding_end_date,
            postgraduate_studentship.progress_notes::text AS progress_notes,
            ARRAY( SELECT mm2.nationality_id
                   FROM person p3
                     JOIN mm_person_nationality mm2 ON p3.id = mm2.person_id
                  WHERE p3.id = postgraduate_studentship.person_id) AS nationality_id,
                CASE
                    WHEN role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10)::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM postgraduate_studentship
             JOIN person ON postgraduate_studentship.person_id = person.id
        )
 SELECT a.id,
    a.person_id,
    a.ro_surname,
    a.ro_first_names,
    a.ro_email_address,
    a.postgraduate_studentship_type_id,
    a.part_time,
    a.supervisor_id,
    a.co_supervisor_id,
    a.external_co_supervisor,
    a.substitute_supervisor_id,
    a.mentor_id,
    a.co_mentor_id,
    a.external_mentor,
    a.university,
    a.cambridge_college_id,
    a.paid_through_payroll,
    a.emplid_number,
    a.nationality_id,
    a.start_date,
    a.dept_leaving_date,
    a.role_intended_end_date,
    a.role_end_date,
    a.force_role_status_to_past,
    a.ro_role_status,
    a.funding,
    a.fees_funding,
    a.funding_end_date,
    a.progress_notes,
    a._cssclass
   FROM a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire3."10_View/Roles/Postgrad_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management_ro;


CREATE OR REPLACE RULE hotwire3_view_postgrad_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
(DELETE FROM postgraduate_studentship
  WHERE (postgraduate_studentship.id = old.id));

CREATE FUNCTION hotwire3.postgrad_studentship_upd() RETURNS TRIGGER AS
$body$
DECLARE
    my_id bigint;
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO public.postgraduate_studentship
            ( 
                person_id,
                postgraduate_studentship_type_id,
                part_time,
                first_supervisor_id,
                second_supervisor_id,
                external_co_supervisor,
                substitute_supervisor_id,
                first_mentor_id,
                second_mentor_id,
                external_mentor,
                university,
                paid_through_payroll,
                emplid_number,
                start_date,
                intended_end_date,
                end_date,
                force_role_status_to_past,
                filemaker_funding,
                filemaker_fees_funding,
                funding_end_date,
                progress_notes
            )
            VALUES
            (
                NEW.person_id,
                NEW.postgraduate_studentship_type_id,
                NEW.part_time,
                NEW.supervisor_id,
                NEW.co_supervisor_id,
                NEW.external_co_supervisor,
                NEW.substitute_supervisor_id,
                NEW.mentor_id,
                NEW.co_mentor_id,
                NEW.external_mentor,
                NEW.university,
                NEW.paid_through_payroll,
                NEW.emplid_number,
                NEW.start_date,
                NEW.role_intended_end_date,
                NEW.role_end_date,
                NEW.force_role_status_to_past,
                NEW.funding::varchar(500),
                NEW.fees_funding::varchar(80),
                NEW.funding_end_date,
                NEW.progress_notes
            )
        RETURNING id INTO my_id;
        NEW.id = my_id;
    ELSE
        UPDATE public.postgraduate_studentship SET
            postgraduate_studentship_type_id = NEW.postgraduate_studentship_type_id,
            part_time = NEW.part_time,
            first_supervisor_id = NEW.supervisor_id,
            second_supervisor_id = NEW.co_supervisor_id,
            external_co_supervisor = NEW.external_co_supervisor,
            substitute_supervisor_id = NEW.substitute_supervisor_id,
            first_mentor_id = NEW.mentor_id,
            second_mentor_id = NEW.co_mentor_id,
            external_mentor = NEW.external_mentor,
            university = NEW.university,
            paid_through_payroll = NEW.paid_through_payroll,
            emplid_number = NEW.emplid_number,
            start_date = NEW.start_date,
            intended_end_date = NEW.role_intended_end_date,
            end_date = NEW.role_end_date,
            force_role_status_to_past = NEW.force_role_status_to_past,
            filemaker_funding = NEW.funding::varchar(500),
            filemaker_fees_funding = NEW.fees_funding::varchar(80),
            funding_end_date = NEW.funding_end_date,
            progress_notes = NEW.progress_notes
        WHERE id = NEW.id;
    END IF;
    
    UPDATE public.person SET
        leaving_date = NEW.dept_leaving_date,
        cambridge_college_id = NEW.cambridge_college_id
    WHERE id = NEW.person_id;

    PERFORM fn_mm_array_update(NEW.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, NEW.person_id);

    RETURN NEW;
END;
$body$ LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.postgrad_studentship_upd() OWNER TO dev;

CREATE TRIGGER hotwire3_view_postgrad_students INSTEAD OF INSERT OR UPDATE ON hotwire3."10_View/Roles/Postgrad_Students" FOR EACH ROW EXECUTE FUNCTION hotwire3.postgrad_studentship_upd();

DROP TRIGGER pg_end_date ON public.postgraduate_studentship;
DROP FUNCTION public.postgrad_end_dates_trig();
