CREATE OR REPLACE FUNCTION public.postgrad_end_dates_trig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    my_end_date DATE := NULL;
    my_type TEXT;
BEGIN
    SELECT t.name INTO my_type FROM public.postgraduate_studentship_type t WHERE NEW.postgraduate_studentship_type_id = t.id;
    IF NEW.force_role_status_to_past THEN
        my_end_date := COALESCE(NEW.phd_date_awarded, NEW.phd_date_submitted, NEW.date_withdrawn_from_register);
    ELSE
        CASE my_type
            WHEN 'PhD' THEN
                my_end_date := COALESCE(NEW.phd_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'MPhil' THEN
                my_end_date := COALESCE(NEW.mphil_date_awarded, NEW.cpgs_or_mphil_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'CPGS' THEN
                my_end_date := COALESCE(NEW.cpgs_or_mphil_date_awarded, NEW.date_withdrawn_from_register);
            WHEN 'MSc' THEN
                my_end_date := COALESCE(NEW.msc_date_awarded, NEW.date_withdrawn_from_register);
        END CASE;
    END IF;
    NEW.end_date := my_end_date;
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.postgrad_end_dates_trig()
    OWNER TO dev;

CREATE TRIGGER pg_end_date
    BEFORE INSERT OR UPDATE 
    ON public.postgraduate_studentship
    FOR EACH ROW
    EXECUTE FUNCTION public.postgrad_end_dates_trig();

DROP VIEW hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Postgrad_Students"
 AS
 WITH a AS (
         SELECT postgraduate_studentship.id,
            postgraduate_studentship.person_id,
            person.surname AS ro_surname,
            person.first_names AS ro_first_names,
            role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10) AS status,
            person.email_address AS ro_email_address,
            postgraduate_studentship.postgraduate_studentship_type_id,
            postgraduate_studentship.part_time,
            postgraduate_studentship.first_supervisor_id AS supervisor_id,
            postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
            postgraduate_studentship.external_co_supervisor,
            postgraduate_studentship.substitute_supervisor_id,
            postgraduate_studentship.first_mentor_id AS mentor_id,
            postgraduate_studentship.second_mentor_id AS co_mentor_id,
            postgraduate_studentship.external_mentor,
            postgraduate_studentship.university,
            person.cambridge_college_id,
            postgraduate_studentship.paid_through_payroll,
            postgraduate_studentship.emplid_number,
            postgraduate_studentship.start_date,
            person.leaving_date,
            postgraduate_studentship.cpgs_or_mphil_date_submission_due,
            postgraduate_studentship.cpgs_or_mphil_date_awarded,
            postgraduate_studentship.mphil_date_submission_due,
            postgraduate_studentship.mphil_date_awarded,
            postgraduate_studentship.msc_date_submission_due,
            postgraduate_studentship.msc_date_awarded,
            postgraduate_studentship.date_registered_for_phd,
            postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
            postgraduate_studentship.phd_date_submitted,
            postgraduate_studentship.phd_date_awarded,
            postgraduate_studentship.date_withdrawn_from_register,
            postgraduate_studentship.date_reinstated_on_register,
            postgraduate_studentship.intended_end_date,
            postgraduate_studentship.filemaker_funding::text AS funding,
            postgraduate_studentship.filemaker_fees_funding::text AS fees_funding,
            postgraduate_studentship.funding_end_date,
            postgraduate_studentship.progress_notes::text AS progress_notes,
            postgraduate_studentship.force_role_status_to_past,
            ARRAY( SELECT mm2.nationality_id
                   FROM person p3
                     JOIN mm_person_nationality mm2 ON p3.id = mm2.person_id
                  WHERE p3.id = postgraduate_studentship.person_id) AS nationality_id,
                CASE
                    WHEN role_status(postgraduate_studentship.start_date, postgraduate_studentship.end_date, postgraduate_studentship.funding_end_date, postgraduate_studentship.force_role_status_to_past, 'postgraduate_studentship'::text)::character varying(10)::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM postgraduate_studentship
             JOIN person ON postgraduate_studentship.person_id = person.id
        )
 SELECT a.id,
    a.person_id,
    a.ro_surname,
    a.ro_first_names,
    a.ro_email_address,
    a.postgraduate_studentship_type_id,
    a.part_time,
    a.supervisor_id,
    a.co_supervisor_id,
    a.external_co_supervisor,
    a.substitute_supervisor_id,
    a.mentor_id,
    a.co_mentor_id,
    a.external_mentor,
    a.university,
    a.cambridge_college_id,
    a.paid_through_payroll,
    a.emplid_number,
    a.nationality_id,
    a.start_date,
    a.leaving_date,
    a.cpgs_or_mphil_date_submission_due,
    a.cpgs_or_mphil_date_awarded,
    a.mphil_date_submission_due,
    a.mphil_date_awarded,
    a.msc_date_submission_due,
    a.msc_date_awarded,
    a.date_registered_for_phd,
    a.phd_four_year_submission_date,
    a.phd_date_submitted,
    a.phd_date_awarded,
    a.date_withdrawn_from_register,
    a.date_reinstated_on_register,
    a.intended_end_date,
    a.funding,
    a.fees_funding,
    a.funding_end_date,
    a.progress_notes,
    a.force_role_status_to_past,
    a._cssclass
   FROM a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire3."10_View/Roles/Postgrad_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management_ro;


-- Rule: hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
(DELETE FROM postgraduate_studentship
  WHERE (postgraduate_studentship.id = old.id));

-- Rule: hotwire3_view_postgrad_students_ins ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_ins ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
( SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
 UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date
  WHERE (person.id = new.person_id);
 INSERT INTO postgraduate_studentship (id, person_id, postgraduate_studentship_type_id, part_time, first_supervisor_id, second_supervisor_id, external_co_supervisor, substitute_supervisor_id, first_mentor_id, second_mentor_id, external_mentor, university, paid_through_payroll, emplid_number, start_date, cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_awarded, mphil_date_submission_due, mphil_date_awarded, msc_date_submission_due, msc_date_awarded, date_registered_for_phd, end_of_registration_date, phd_date_submitted, phd_date_awarded, date_withdrawn_from_register, date_reinstated_on_register, intended_end_date, filemaker_funding, filemaker_fees_funding, funding_end_date, progress_notes, force_role_status_to_past)
  VALUES (nextval('postgraduate_studentship_id_seq'::regclass), new.person_id, new.postgraduate_studentship_type_id, new.part_time, new.supervisor_id, new.co_supervisor_id, new.external_co_supervisor, new.substitute_supervisor_id, new.mentor_id, new.co_mentor_id, new.external_mentor, new.university, new.paid_through_payroll, new.emplid_number, new.start_date, new.cpgs_or_mphil_date_submission_due, new.cpgs_or_mphil_date_awarded, new.mphil_date_submission_due, new.mphil_date_awarded, new.msc_date_submission_due, new.msc_date_awarded, new.date_registered_for_phd, new.phd_four_year_submission_date, new.phd_date_submitted, new.phd_date_awarded, new.date_withdrawn_from_register, new.date_reinstated_on_register, new.intended_end_date, (new.funding)::character varying(500), (new.fees_funding)::character varying(80), new.funding_end_date, (new.progress_notes)::character varying, new.force_role_status_to_past)
  RETURNING currval('postgraduate_studentship_id_seq'::regclass) AS currval,
    postgraduate_studentship.person_id,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar",
    NULL::character varying(48) AS "varchar",
    postgraduate_studentship.postgraduate_studentship_type_id,
    postgraduate_studentship.part_time,
    postgraduate_studentship.first_supervisor_id,
    postgraduate_studentship.second_supervisor_id,
    postgraduate_studentship.external_co_supervisor,
    postgraduate_studentship.substitute_supervisor_id,
    postgraduate_studentship.first_mentor_id,
    postgraduate_studentship.second_mentor_id,
    postgraduate_studentship.external_mentor,
    postgraduate_studentship.university,
    NULL::bigint AS int8,
    postgraduate_studentship.paid_through_payroll,
    postgraduate_studentship.emplid_number,
    NULL::bigint[] AS int8,
    postgraduate_studentship.start_date,
    NULL::date AS date,
    postgraduate_studentship.cpgs_or_mphil_date_submission_due,
    postgraduate_studentship.cpgs_or_mphil_date_awarded,
    postgraduate_studentship.mphil_date_submission_due,
    postgraduate_studentship.mphil_date_awarded,
    postgraduate_studentship.msc_date_submission_due,
    postgraduate_studentship.msc_date_awarded,
    postgraduate_studentship.date_registered_for_phd,
    postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
    postgraduate_studentship.phd_date_submitted,
    postgraduate_studentship.phd_date_awarded,
    postgraduate_studentship.date_withdrawn_from_register,
    postgraduate_studentship.date_reinstated_on_register,
    postgraduate_studentship.intended_end_date,
    (postgraduate_studentship.filemaker_funding)::text AS filemaker_funding,
    (postgraduate_studentship.filemaker_fees_funding)::text AS filemaker_fees_funding,
    postgraduate_studentship.funding_end_date,
    (postgraduate_studentship.progress_notes)::text AS progress_notes,
    postgraduate_studentship.force_role_status_to_past,
    NULL::text AS text;
);

-- Rule: hotwire3_view_postgrad_students_upd ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_upd ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
( UPDATE postgraduate_studentship SET postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, part_time = new.part_time, first_supervisor_id = new.supervisor_id, second_supervisor_id = new.co_supervisor_id, external_co_supervisor = new.external_co_supervisor, substitute_supervisor_id = new.substitute_supervisor_id, first_mentor_id = new.mentor_id, second_mentor_id = new.co_mentor_id, external_mentor = new.external_mentor, university = new.university, paid_through_payroll = new.paid_through_payroll, emplid_number = new.emplid_number, start_date = new.start_date, cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded, mphil_date_submission_due = new.mphil_date_submission_due, mphil_date_awarded = new.mphil_date_awarded, msc_date_submission_due = new.msc_date_submission_due, msc_date_awarded = new.msc_date_awarded, date_registered_for_phd = new.date_registered_for_phd, end_of_registration_date = new.phd_four_year_submission_date, phd_date_submitted = new.phd_date_submitted, phd_date_awarded = new.phd_date_awarded, date_withdrawn_from_register = new.date_withdrawn_from_register, date_reinstated_on_register = new.date_reinstated_on_register, intended_end_date = new.intended_end_date, filemaker_funding = (new.funding)::character varying(500), filemaker_fees_funding = (new.fees_funding)::character varying(80), funding_end_date = new.funding_end_date, progress_notes = (new.progress_notes)::character varying, force_role_status_to_past = new.force_role_status_to_past
  WHERE (postgraduate_studentship.id = old.id);
 UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date
  WHERE (person.id = new.person_id);
 SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
);

DROP FUNCTION hotwire3.postgrad_studentship_upd();
