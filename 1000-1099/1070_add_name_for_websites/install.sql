-- add new column
ALTER TABLE person ADD COLUMN name_for_websites TEXT;
ALTER TABLE person ADD COLUMN sortable_name_for_websites TEXT;

-- public.www_person_hid_v2
CREATE OR REPLACE VIEW public.www_person_hid_v2
 AS
 SELECT person.id AS person_id,
    COALESCE(title_hid.website_title_hid::text || ' '::text, ''::text) || COALESCE(name_for_websites,COALESCE((COALESCE(person.known_as, person.first_names)::text || ' '::text),''::text) || person.surname::text) || COALESCE(' '::text || person.name_suffix::text, ''::text) AS www_person_hid
   FROM person
     LEFT JOIN title_hid USING (title_id);

-- www.person_info_v8
CREATE OR REPLACE VIEW www.person_info_v8
 AS
 SELECT person.id,
    person.crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            WHEN s.status_id::text = 'Past'::text THEN NULL::character varying
            ELSE person.email_address
        END AS email_address,
        CASE
            WHEN person.hide_photo_from_website THEN 0::bigint
            ELSE COALESCE(person.image_lo::bigint, 0::bigint)
        END AS image_oid,
    www_person_hid_v2.www_person_hid,
    COALESCE(sortable_name_for_websites,(person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names))::text AS sortable_name,
    cambridge_college.name AS college_name,
    cambridge_college.website AS college_website,
        CASE
            WHEN person.hide_phone_no_from_website THEN NULL::character varying::text
            ELSE telephone_numbers.telephone_numbers
        END AS telephone_numbers,
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text, 'sc-10'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-5'::text THEN 'Research assistant'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE post_category_hid.post_category_hid
        END AS post_category,
    person.counts_as_academic,
    s.status_id AS physical_status,
    r.rigs,
    pr.primary_rig,
    rgs.research_groups,
    rgs.website_addresses,
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title,
        CASE
            WHEN person.counts_as_academic IS TRUE THEN 'Head of group'::text
            WHEN person.counts_as_postdoc IS TRUE THEN 'Postdoctoral researcher'::text
            ELSE h.post_category_hid
        END AS research_group_post_category
   FROM ( SELECT _latest_role_v1.person_id,
            max(_latest_role_v1.post_category_weight) AS max
           FROM www._latest_role_v1
          GROUP BY _latest_role_v1.person_id) lr
     JOIN cache._latest_role_v12 _latest_role ON lr.person_id = _latest_role.person_id AND lr.max = _latest_role.post_category_weight
     JOIN person ON person.id = lr.person_id
     JOIN www.post_category_hid h ON h.post_category_id = _latest_role.post_category_id
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN post_history ON post_history.id = _latest_role.role_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
     LEFT JOIN www.post_category_hid ON post_category_hid.post_category_id = _latest_role.post_category_id
     LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);


-- personnel data entry
DROP VIEW hotwire3."10_View/People/Personnel_Data_Entry";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Data_Entry"
 AS
 WITH a AS (
         SELECT person.id,
            person.id AS ro_person_id,
            person.image_lo AS image_oid,
            person.surname,
            person.first_names,
            person.title_id,
            person.known_as,
            person.name_suffix,
            person.previous_surname,
            person.date_of_birth,
            date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age,
            person.gender_id,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.chem AS ro_chem,
            person.crsid,
            person.email_address,
            person.hide_email,
            person.do_not_show_on_website AS hide_from_website,
            person.name_for_websites::varchar AS override_fullname_for_websites,
            person.sortable_name_for_websites::varchar AS override_sortable_name_for_websites,
            person.arrival_date,
            person.leaving_date,
            _physical_status.status_id AS ro_physical_status_id,
            person.left_but_no_leaving_date_given,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            person.hide_phone_no_from_website,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            _latest_role.co_supervisor_id AS ro_co_supervisor_id,
            _latest_role.mentor_id AS ro_mentor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            person.cambridge_address::text AS home_address,
            person.cambridge_phone_number AS home_phone_number,
            person.cambridge_college_id,
            person.mobile_number,
            ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE mm_person_nationality.person_id = person.id) AS nationality_id,
            person.emergency_contact::text AS emergency_contact,
            person.other_information,
            person.notes,
            person.forwarding_address::text AS forwarding_address,
            person.new_employer_address::text AS new_employer_address,
            person.chem_at_cam,
                CASE
                    WHEN retired_staff_list.include_person_id IS NOT NULL THEN true
                    ELSE false
                END AS retired_staff_mailing_list,
            person.continuous_employment_start_date,
            person.paper_file_details,
            person.registration_completed,
            person.clearance_cert_signed,
            ( SELECT max(leavers_form_submission.submitted_when) AS max
                   FROM leavers_form.leavers_form_submission
                  WHERE (leavers_form_submission.leavers_form_id IN ( SELECT leavers_form_overview.id
                           FROM leavers_form.leavers_form_overview
                          WHERE leavers_form_overview.person = person.id))) AS ro_leaving_form_submitted,
            person.counts_as_academic AS treat_as_academic_staff,
            person.passport_issuing_country_id,
            person.passport_number,
            person.passport_expiry_date,
            person.immigration_status_id,
            person.immigration_status_expiry_date,
            person.brp_number AS "BRP_number",
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN LATERAL ( SELECT _hr_role.person_id,
                    _hr_role.role_id,
                    _hr_role.role_tablename,
                    _hr_role.start_date,
                    _hr_role.intended_end_date,
                    _hr_role.estimated_role_end_date,
                    _hr_role.end_date,
                    _hr_role.funding_end_date,
                    _hr_role.supervisor_id,
                    _hr_role.post_category_id,
                    _hr_role.post_category,
                    _hr_role.predicted_role_status_id,
                    _hr_role.funding,
                    _hr_role.fees_funding,
                    _hr_role.research_grant_number,
                    _hr_role.paid_by_university,
                    _hr_role.job_title,
                    _hr_role.left_but_no_leaving_date_given,
                    _hr_role.chem,
                    _hr_role.mentor_id,
                    _hr_role.co_supervisor_id
                   FROM _hr_role
                  WHERE person.id = _hr_role.person_id
                  ORDER BY _hr_role.start_date DESC NULLS LAST
                 LIMIT 1) _latest_role ON true
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
             LEFT JOIN ( SELECT mm_mailinglist_include_person.mailinglist_id,
                    mm_mailinglist_include_person.include_person_id
                   FROM mm_mailinglist_include_person
                     JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
                  WHERE mailinglist.name::text = 'chem-retiredstaff'::text) retired_staff_list ON person.id = retired_staff_list.include_person_id
        )
 SELECT a.id,
    a.ro_person_id,
    a.image_oid,
    a.surname,
    a.first_names,
    a.title_id,
    a.known_as,
    a.name_suffix,
    a.previous_surname,
    a.date_of_birth,
    a.ro_age,
    a.gender_id,
    a.ro_post_category_id,
    a.ro_chem,
    a.crsid,
    a.email_address,
    a.hide_email,
    a.arrival_date,
    a.leaving_date,
    a.ro_physical_status_id,
    a.left_but_no_leaving_date_given,
    a.dept_telephone_number_id,
    a.hide_phone_no_from_website,
    a.hide_from_website,
    a.override_fullname_for_websites,
    a.override_sortable_name_for_websites,
    a.room_id,
    a.ro_supervisor_id,
    a.ro_co_supervisor_id,
    a.research_group_id,
    a.home_address,
    a.home_phone_number,
    a.cambridge_college_id,
    a.mobile_number,
    a.nationality_id,
    a.emergency_contact,
    a.other_information,
    a.notes,
    a.forwarding_address,
    a.new_employer_address,
    a.chem_at_cam AS "chem_@_cam",
    a.retired_staff_mailing_list,
    a.continuous_employment_start_date,
    a.paper_file_details AS paper_file_status,
    a.registration_completed,
    a.clearance_cert_signed,
    a.ro_leaving_form_submitted,
    a._cssclass,
    a.treat_as_academic_staff,
    a.passport_issuing_country_id,
    a.passport_number,
    a.passport_expiry_date,
    a.immigration_status_id,
    a.immigration_status_expiry_date,
    a."BRP_number"
   FROM a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire3."10_View/People/Personnel_Data_Entry"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO dev;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO student_management;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Personnel_Data_Entry" TO tkd25;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_del AS
    ON DELETE TO hotwire3."10_View/People/Personnel_Data_Entry"
    DO INSTEAD
(DELETE FROM person
  WHERE (person.id = old.id));

CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare 
	d_do_not_show_on_website boolean := 'f';
	d_hide_phone_no_from_website boolean := 'f';
	d_immigration_status_id bigint := 1 ;
	on_retired_staff_list bigint;
	retired_staff_list_id bigint;

begin
	if NEW.id is not null 
	then
		update person set
			surname = NEW.surname,
			image_lo = NEW.image_oid,
			first_names = NEW.first_names, 
			title_id = NEW.title_id, 
			known_as = NEW.known_as,
			name_suffix = NEW.name_suffix,
			previous_surname = NEW.previous_surname,
			date_of_birth = NEW.date_of_birth, 
			gender_id = NEW.gender_id, 
			crsid = NEW.crsid,
			email_address = NEW.email_address, 
			hide_email = NEW.hide_email, 
			do_not_show_on_website = COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
			hide_phone_no_from_website = COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
                        name_for_websites = new.override_fullname_for_websites,
                        sortable_name_for_websites = new.override_sortable_name_for_websites,
			arrival_date = NEW.arrival_date,
			leaving_date = NEW.leaving_date,
			left_but_no_leaving_date_given = NEW.left_but_no_leaving_date_given,
			cambridge_address = NEW.home_address::varchar(500),
			cambridge_phone_number = NEW.home_phone_number,
			cambridge_college_id = NEW.cambridge_college_id,
			mobile_number = NEW.mobile_number,
			emergency_contact = NEW.emergency_contact::varchar(500), 
			other_information = NEW.other_information,
			notes = NEW.notes,
			forwarding_address = NEW.forwarding_address,
			new_employer_address = NEW.new_employer_address, 
			chem_at_cam = NEW."chem_@_cam",
			continuous_employment_start_date = NEW.continuous_employment_start_date,
			paper_file_details = NEW.paper_file_status, 
			registration_completed = NEW.registration_completed,
			clearance_cert_signed = NEW.clearance_cert_signed,
			counts_as_academic=NEW.treat_as_academic_staff,
                        passport_issuing_country_id=NEW.passport_issuing_country_id,
                        passport_number=NEW.passport_number,
                        passport_expiry_date=NEW.passport_expiry_date,
                        immigration_status_id = COALESCE(NEW.immigration_status_id, d_immigration_status_id),
                        immigration_status_expiry_date=NEW.immigration_status_expiry_date,
                        brp_number=NEW."BRP_number"
		where person.id = OLD.id;
	else
                NEW.id := nextval('person_id_seq');
		insert into person (
                                id,
				surname,
				image_lo, 
				first_names, 
				title_id, 
				known_as,
				name_suffix,
				previous_surname, 
				date_of_birth, 
				gender_id, 
				crsid,
				email_address, 
				hide_email, 
				do_not_show_on_website,
                                name_for_websites,
                                sortable_name_for_websites,
				arrival_date,
				leaving_date,
				cambridge_address,
				cambridge_phone_number, 
				cambridge_college_id,
				mobile_number,
				emergency_contact, 
				other_information,
				notes,
				forwarding_address,
				new_employer_address, 
				chem_at_cam,
				continuous_employment_start_date,
				paper_file_details, 
				registration_completed,
				clearance_cert_signed,
				counts_as_academic,
				hide_phone_no_from_website,
                                passport_issuing_country_id,
                                passport_number,
                                passport_expiry_date,
                                immigration_status_id,
                                immigration_status_expiry_date,
                                brp_number
			) values (
                                NEW.id,
				NEW.surname, 
				NEW.image_oid,
				NEW.first_names, 
				NEW.title_id, 
				NEW.known_as,
				NEW.name_suffix,
				NEW.previous_surname,
				NEW.date_of_birth, 
				NEW.gender_id, 
				NEW.crsid,
				NEW.email_address, 
				NEW.hide_email, 
				COALESCE(NEW.hide_from_website,d_do_not_show_on_website),
                                NEW.override_fullname_for_websites,
                                NEW.override_sortable_name_for_websites,
				NEW.arrival_date,
				NEW.leaving_date,
				NEW.home_address::varchar(500),
				NEW.home_phone_number, 
				NEW.cambridge_college_id,
				NEW.mobile_number,
				NEW.emergency_contact::varchar(500), 
				NEW.other_information,
				NEW.notes,
				NEW.forwarding_address,
				NEW.new_employer_address, 
				NEW."chem_@_cam",
				NEW.continuous_employment_start_date,
				NEW.paper_file_status, 
				NEW.registration_completed,
				NEW.clearance_cert_signed,
				NEW.treat_as_academic_staff,
				COALESCE(NEW.hide_phone_no_from_website,d_hide_phone_no_from_website),
                                NEW.passport_issuing_country_id,
                                NEW.passport_number,
                                NEW.passport_expiry_date,
                                COALESCE(NEW.immigration_status_id,d_immigration_status_id),
                                NEW.immigration_status_expiry_date,
                                NEW."BRP_number"
			) ;
	end if;

	-- do retired staff list update
	select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
	select include_person_id from mm_mailinglist_include_person where include_person_id = NEW.id and mailinglist_id = retired_staff_list_id into on_retired_staff_list;
	if NEW.retired_staff_mailing_list = 't' and on_retired_staff_list is null
	then
	   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, NEW.id); 
	end if;
	if NEW.retired_staff_mailing_list = 'f' and on_retired_staff_list is not null
	then
	   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = NEW.id; 
	end if;

	-- many-many updates here
	perform fn_mm_array_update(NEW.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, NEW.id);
	perform fn_mm_array_update(NEW.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, NEW.id);

	
	return NEW;
end;
$BODY$;

ALTER FUNCTION hotwire3.personnel_data_entry()
    OWNER TO dev;

CREATE TRIGGER hotwire3_view_personnel_data_entry
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/People/Personnel_Data_Entry"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.personnel_data_entry();


-- websites view
DROP VIEW hotwire3."10_View/People/Website_View";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Website_View"
 AS
 SELECT person.id,
    person.crsid AS ro_crsid,
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS ro_email_address,
    person.image_lo AS image,
    www_person_hid_v2.www_person_hid AS ro_display_name,
    person.title_id as ro_title_id,
    person.first_names AS ro_first_names,
    person.known_as AS ro_known_as,
    person.surname AS ro_surname,
    name_for_websites::varchar AS override_fullname_for_websites,
    person.name_suffix AS ro_name_suffix,
    COALESCE(sortable_name_for_websites,(person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names))::text AS ro_name_for_sorting,
    sortable_name_for_websites::varchar AS override_sortable_name_for_websites,
    cambridge_college.name AS ro_college_name,
    cambridge_college.website AS ro_college_website,
    telephone_numbers.telephone_numbers AS ro_telephone_numbers,
        CASE
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'sc-3'::text THEN 'Assistant'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            WHEN _latest_role.role_tablename = 'postgraduate_studentship'::text THEN 'Postgrad'::text
            WHEN _latest_role.role_tablename = 'visitorship'::text THEN 'Visitor'::text
            WHEN _latest_role.role_tablename = 'part_iii_studentship'::text THEN 'Part III'::text
            WHEN _latest_role.role_tablename = 'erasmus_socrates_studentship'::text THEN 'Erasmus'::text
            ELSE 'Other'::text
        END AS ro_post_category,
    person.website_staff_category_id,
    person.counts_as_academic AS ro_counts_as_academic,
    s.status_id AS ro_physical_status,
    r.rigs AS ro_rigs,
    pr.primary_rig AS ro_primary_rig,
    rgs.research_groups AS ro_research_groups,
    rgs.website_addresses AS ro_website_addresses,
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title,
    person.do_not_show_on_website AS ro_hide_person_from_website,
    person.hide_photo_from_website AS ro_hide_photo_from_website,
        CASE
            WHEN person.do_not_show_on_website = true THEN 'red'::character varying
            ELSE NULL::character varying
        END AS _cssclass
   FROM person
     LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
     LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
     LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
     LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
     LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
     LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
     LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
     LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
     LEFT JOIN post_history ON post_history.id = _latest_role.role_id
  WHERE person.is_spri IS NOT TRUE
  ORDER BY ((person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text);

ALTER TABLE hotwire3."10_View/People/Website_View"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Website_View" TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE hotwire3."10_View/People/Website_View" TO web_editors;

CREATE OR REPLACE RULE people_website_upd AS
    ON UPDATE TO hotwire3."10_View/People/Website_View"
    DO INSTEAD
( UPDATE person SET 
    website_staff_category_id = new.website_staff_category_id,
    name_for_websites = NEW.override_fullname_for_websites,
    sortable_name_for_websites = NEW.override_sortable_name_for_websites
  WHERE (person.id = old.id
);
 UPDATE post_history SET job_title = new.job_title
  WHERE (post_history.id = ( SELECT ph.id
           FROM ((person p
             JOIN cache._latest_role lr ON ((p.id = lr.person_id)))
             JOIN post_history ph ON ((lr.role_id = ph.id)))
          WHERE ((p.id = old.id) AND (lr.role_tablename = 'post_history'::text))));
);

