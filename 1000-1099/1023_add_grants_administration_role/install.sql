SET ROLE dev;

-- Adding the roles below does not get reversed by the remove.sql script; it edits the
-- 'postgres' database rather than the database we're connected to, and so makes
-- for difficulties if we want to apply this changeset to two databases on the same
-- cluster - which we usually do
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'grants_administration') THEN
      CREATE ROLE grants_administration NOLOGIN;
      GRANT ro_hid TO grants_administration;
   END IF;
END
$do$;

DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'grants_ro') THEN
      CREATE ROLE grants_ro NOLOGIN;
      GRANT ro_hid TO grants_ro;
   END IF;
END
$do$;

