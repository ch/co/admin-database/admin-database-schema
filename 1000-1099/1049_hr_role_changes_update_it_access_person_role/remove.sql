REVOKE SELECT,INSERT,UPDATE,DELETE ON it_access.person_role FROM gdpr_purge;
REVOKE SELECT,INSERT,UPDATE ON it_access.person_role FROM hr;
REVOKE SELECT,INSERT,UPDATE ON it_access.person_role FROM student_management;
REVOKE SELECT,UPDATE ON it_access.person_role FROM role_end_date_setter;
REVOKE SELECT (id, arrival_date, leaving_date, left_but_no_leaving_date_given) ON public.person FROM role_end_date_setter;

DROP TRIGGER update_it_roles ON public.person;
DROP TRIGGER update_it_roles ON public._hr_role;
DROP FUNCTION it_access.update_it_access_person_role();
DROP FUNCTION it_access.get_current_chemist_it_role_dates_for_person(BIGINT);
DELETE FROM it_access.person_role;
