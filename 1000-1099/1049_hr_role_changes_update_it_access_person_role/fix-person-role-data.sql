TRUNCATE it_access.person_role CASCADE;

-- Populate it_access.person_role Current Chemist data
WITH calculated_dates AS (
    SELECT DISTINCT id, (it_access.get_current_chemist_it_role_dates_for_person(id)).*
    FROM person
    JOIN public._hr_role ON person.id = _hr_role.person_id -- only include people with Chemistry HR roles
    ),
    current_chemist_role AS (
    SELECT role_type_id
    FROM it_access.role_type
    WHERE role_type = 'Current Chemist'
    )
INSERT INTO it_access.person_role (
        person_id,
        role_type_id,
        start_date,
        end_date,
        notes
    )
    SELECT
        id,
        current_chemist_role.role_type_id,
        COALESCE(current_chemist_role_start_date,'-infinity'::date),
        COALESCE(current_chemist_role_end_date,'-infinity'::date),
        FORMAT(E'%s: Initial insert of current chemist role by %s\n',now(),current_user )
    FROM calculated_dates, current_chemist_role
;

