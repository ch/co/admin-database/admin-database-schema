BEGIN;

\set schemaname '\'it_access\''

CREATE TEMPORARY TABLE funcs ( fname TEXT, args TEXT[] );

INSERT INTO funcs VALUES ( 'update_it_access_person_role', '{}'::TEXT[] ), ( 'get_current_chemist_it_role_dates_for_person', (ARRAY['bigint'])::TEXT[] );

SELECT plan(6);

SELECT has_function(:schemaname, fname, FORMAT('Function %I.%I exists',:schemaname,fname)) FROM funcs;
SELECT function_owner_is(:schemaname, fname, args::name[], 'dev', FORMAT('Function %I.%I owned by dev',:schemaname,fname)) FROM funcs;

SELECT has_trigger('public',tname,'update_it_roles',FORMAT('%I.%I has trigger update_it_roles','public',tname)) FROM (VALUES ('person'), ('_hr_role')) AS t(tname);

SELECT finish();

ROLLBACK;
