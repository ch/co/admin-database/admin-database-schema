-- function to work out the limits of a person's Current Chemist IT role entitlement
CREATE FUNCTION it_access.get_current_chemist_it_role_dates_for_person(IN affected_person_id BIGINT, OUT current_chemist_role_start_date DATE, OUT current_chemist_role_end_date DATE) AS
$body$
DECLARE
    commercial_visitor_category_id TEXT = 'v-17';
BEGIN
    -- Our job is to work out what the start and end dates of this person's
    -- current chemist IT role should now be.

    -- Start should always be their person.arrival_date. No recorded arrival_date means no current chemist IT access.
    SELECT arrival_date INTO current_chemist_role_start_date FROM public.person WHERE id = affected_person_id;

    -- End should be the person.leaving_date if it is set. If that is not set 
    -- then we look at the roles.
    SELECT COALESCE(

        CASE
            WHEN person.left_but_no_leaving_date_given THEN '-infinity'::date -- we know they're definitely gone, but don't know when
            WHEN person.arrival_date IS NULL THEN '-infinity'::date -- if they haven't arrived they are certainly not a current chemist, so set the end date in the past to ensure they are not granted access
            ELSE NULL::date
        END,

        person.leaving_date, -- if this is set we know they're gone/going and that was/is when

        -- Failing that, look for the role that ends furthest into the future, hence the MAX
        MAX(
            -- but we have to try a few different columns get the best estimate of when each role ends/ended
            COALESCE(
                -- any roles marked as left_but_no_leaving_date_given are considered definitely over, but the end date sorts older than any other date
                CASE WHEN _hr_role.left_but_no_leaving_date_given THEN '-infinity'::date
                ELSE NULL::date
                END,
                -- If an end_date is recorded it is likely to be the most correct value, as we only put those in when people actually go
                _hr_role.end_date,
                -- failing definite information about the role having ended, when did they say they were ending? We'll hold them to that one. 
                COALESCE(
                    _hr_role.intended_end_date,
                    -- and if the intended_end_date is null then this means 'future' so we replace null with +infinity.
                    -- Only permanent staff with no defined retirement date are allowed NULL intended end dates.
                    'infinity'::date
                )
            )
        ), -- this is our best guess at when their roles ended or will be ending
        '-infinity'::date -- if there are no roles left for a person we need a value to mark this current chemist role as definitely in the past
    ) INTO current_chemist_role_end_date
    FROM public._hr_role
    JOIN public.person ON _hr_role.person_id = person.id
    WHERE
        person_id = affected_person_id
        AND
        post_category_id <> commercial_visitor_category_id
    GROUP BY _hr_role.person_id, person.arrival_date, person.leaving_date, person.left_but_no_leaving_date_given;
    RETURN; -- function has OUT parameters which are implicitly returned

END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION it_access.get_current_chemist_it_role_dates_for_person(BIGINT) OWNER TO dev;

-- Do the initial population of it_access.person_role Current Chemist data
WITH calculated_dates AS (
    SELECT DISTINCT id, (it_access.get_current_chemist_it_role_dates_for_person(id)).*
    FROM person
    JOIN public._hr_role ON person.id = _hr_role.person_id -- only include people with Chemistry HR roles
    ),
    current_chemist_role AS (
    SELECT role_type_id
    FROM it_access.role_type
    WHERE role_type = 'Current Chemist'
    )
INSERT INTO it_access.person_role ( 
        person_id,
        role_type_id,
        start_date,
        end_date,
        notes
    )
    SELECT
        id,
        current_chemist_role.role_type_id,
        COALESCE(current_chemist_role_start_date,'-infinity'::date),
        COALESCE(current_chemist_role_end_date,'-infinity'::date),
        FORMAT(E'%s: Initial insert of current chemist role by %s\n',now(),current_user ) 
    FROM calculated_dates, current_chemist_role
;

-- Trigger to update it_access.person_role on updates to person and inserts or updates or deletes to _hr_role
CREATE FUNCTION it_access.update_it_access_person_role() RETURNS TRIGGER AS
$body$
DECLARE

    affected_person_id BIGINT;
    new_it_role_start_date DATE;
    new_it_role_end_date DATE;
    original_notes TEXT;
    original_it_role_start_date DATE;
    original_it_role_end_date DATE;

    current_chem_role_type_id BIGINT = 2;

BEGIN

    -- check what table and operation called us, and set the person_id to
    -- operate on accordingly, or exit without changes.
    IF TG_TABLE_NAME = 'person' AND TG_OP = 'UPDATE' THEN
        -- If we're been triggered by an update on person we also check if any
        -- column changed that could affect it_access.person_role. If not we
        -- return straight away. Doing it this way means we don't have to grant as many
        -- permissions, because lots of roles can update a few columns in public.person
        -- and so will fire this trigger, but very few can update the columns
        -- that cause the trigger to make any changes to it_access.person_role.
        -- We also install the trigger as ON UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given
        -- which doesn't achieve quite the same thing as checking if a column changed, as one can update a column
        -- to the same value as before.
        IF (
            (NEW.arrival_date IS DISTINCT FROM OLD.arrival_date)
            OR
            (NEW.leaving_date IS DISTINCT FROM OLD.leaving_date)
            OR
            (NEW.left_but_no_leaving_date_given IS DISTINCT FROM OLD.left_but_no_leaving_date_given)
        ) THEN
            affected_person_id = NEW.id;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF TG_TABLE_NAME = '_hr_role' THEN
        -- Only student_management, role_end_date_setter, gdpr_purge, and hr
        -- can make changes to the _hr_role table, and the changes they could make are
        -- very likely to cause a change in it_access.person_role. So for simplicity we
        -- don't do a detailed check of OLD vs NEW before running the rest of
        -- the trigger if we're fired on a change to _hr_role.
        IF ( TG_OP = 'DELETE' ) THEN
            affected_person_id = OLD.person_id;
        ELSIF ( TG_OP = 'INSERT' ) THEN
            affected_person_id = NEW.person_id;
        ELSIF ( TG_OP = 'UPDATE' ) THEN
            affected_person_id = NEW.person_id;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        -- We weren't fired on the right table or operation, so return unchanged
        RETURN NEW;
    END IF;

    -- get new dates for Current Chemist IT access
    SELECT
        current_chemist_role_start_date,
        current_chemist_role_end_date
    INTO
        new_it_role_start_date,
        new_it_role_end_date
    FROM it_access.get_current_chemist_it_role_dates_for_person(affected_person_id) ;

    -- get current dates for Current Chemist IT access
    SELECT
        start_date,
        end_date
    INTO
        original_it_role_start_date,
        original_it_role_end_date
    FROM it_access.person_role
    WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

    -- Did anything actually change?
    IF (
            (new_it_role_start_date IS DISTINCT FROM original_it_role_start_date)
            OR
            (new_it_role_end_date IS DISTINCT FROM original_it_role_end_date)
    ) THEN
        -- If either of the new dates is null then that's probably because an _hr_role added
        -- by mistake was removed. We want to keep a record that the person had some IT access
        -- so we set the new dates to '-infinity'::DATE in that case.
        new_it_role_start_date = COALESCE(new_it_role_start_date,'-infinity'::DATE);
        new_it_role_end_date = COALESCE(new_it_role_end_date,'-infinity'::DATE);

        -- get any existing notes about the person's Current Chemist role
        SELECT COALESCE(notes,'') INTO original_notes
        FROM it_access.person_role
        WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

        -- Changes to _hr_role can insert a new Current Chemist role so do an upsert
        IF TG_TABLE_NAME = '_hr_role' THEN
            INSERT INTO it_access.person_role
                (
                    person_id,
                    role_type_id,
                    start_date,
                    end_date,
                    notes
                )
                VALUES
                (
                    affected_person_id,
                    current_chem_role_type_id,
                    new_it_role_start_date,
                    new_it_role_end_date,
                    FORMAT(E'%s: Insert new Current Chemist IT role from changes in _hr_role by %s\n',now(), current_user)
                )
            ON CONFLICT (person_id, role_type_id)
            DO UPDATE SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in _hr_role by %s\n',now(), current_user)||original_notes;

        -- changes to person can only update an existing current chemist role, not create one
        ELSIF TG_TABLE_NAME = 'person' THEN

            UPDATE it_access.person_role SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in person by %s\n',now(), current_user)||original_notes
            WHERE person_id = affected_person_id AND role_type_id = current_chem_role_type_id;

       END IF;
    END IF;

    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION it_access.update_it_access_person_role() OWNER TO dev;

CREATE TRIGGER update_it_roles AFTER UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given ON public.person FOR EACH ROW EXECUTE FUNCTION it_access.update_it_access_person_role();
CREATE TRIGGER update_it_roles AFTER UPDATE OR INSERT OR DELETE ON public._hr_role FOR EACH ROW EXECUTE FUNCTION it_access.update_it_access_person_role();

-- Set permissions to allow roles other than dev to run the trigger

-- gdpr_purge should be able to do anything
GRANT SELECT,INSERT,UPDATE,DELETE ON it_access.person_role TO gdpr_purge;

-- hr can add people and roles, and update start/end dates, which could insert or update rows here but not delete them
GRANT SELECT,INSERT,UPDATE ON it_access.person_role TO hr;

-- student_management role can add new postgrad roles and update leaving dates so can add or update rows here
GRANT SELECT,INSERT,UPDATE ON it_access.person_role TO student_management;

-- role_end_date_setter can update end dates, but not add new roles, so only needs select and update
GRANT SELECT,UPDATE ON it_access.person_role TO role_end_date_setter;

-- extra perms needed needed to allow role_end_date_setter to check on arrival and leaving dates, as this role can update role end dates and so fire the trigger
GRANT SELECT (id, arrival_date, leaving_date, left_but_no_leaving_date_given) ON public.person TO role_end_date_setter;
