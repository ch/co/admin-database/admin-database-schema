BEGIN;

\set testcrsid '\'fjc55\''
\set current_chemist_roletype '2'

-- here is a person to test on
INSERT INTO public.person ( surname, first_names, crsid ) VALUES ( 'Test', 'Test', :testcrsid );
-- as they are a brand new person they have no roles yet

-- some prepared statements to update the person and give them roles
PREPARE set_person_arrival_leaving_dates(date,date,boolean) AS
    UPDATE public.person SET arrival_date = $1, leaving_date = $2, left_but_no_leaving_date_given = $3 WHERE crsid = :testcrsid;

PREPARE add_visitor_role(text,date,date,date,boolean) AS
    INSERT INTO public.visitorship
        (
            person_id,
            visitor_type_id,
            start_date,
            intended_end_date,
            end_date,
            force_role_status_to_past
        )
        VALUES
        (
            (SELECT id FROM public.person WHERE crsid = :testcrsid),
            (SELECT visitor_type_id FROM public.visitor_type_hid WHERE visitor_type_hid = $1),
            $2,
            $3,
            $4,
            $5
        );

PREPARE update_visitor_role(text,date,date,boolean) AS
    UPDATE public.visitorship SET
            end_date = $2,
            intended_end_date = $3,
            force_role_status_to_past = $4
    WHERE person_id = (SELECT id FROM public.person WHERE crsid = :testcrsid) AND visitor_type_id = (SELECT visitor_type_id FROM public.visitor_type_hid WHERE visitor_type_hid = $1);

PREPARE get_current_chemist_role AS
    SELECT start_date, end_date
    FROM it_access.person_role
    WHERE person_id = (SELECT id FROM public.person WHERE crsid = :testcrsid)
    AND role_type_id = :current_chemist_roletype;

SELECT plan(9);

SELECT is_empty('get_current_chemist_role','No current chemist role for a person with no HR roles and not arrived');

-- The person has arrived
EXECUTE set_person_arrival_leaving_dates((current_date - interval '1 week'),NULL::DATE,NULL::boolean);
SELECT is_empty('get_current_chemist_role','No current chemist role for a person with no HR roles even if arrived');

-- add commercial visitor role, this will insert a current chemist IT role with an end date of -infinity
EXECUTE add_visitor_role('Visitor (Commercial)', (current_date - interval '1 week'), (current_date + interval ' 12 months'),NULL::date,NULL::boolean);
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, '-infinity'::date)$$,'Commercial Visitor has current chemist role that is inactive');

-- add a PDRA visitor role, this should update the current chemist role to have a future end date
EXECUTE add_visitor_role('Visitor (PDRA)', (current_date - interval '1 week'), (current_date + interval ' 12 months'),NULL::date,NULL::boolean);
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, (current_date + interval ' 12 months')::date)$$,'Visitor PDRA has current chemist role with correct end date');

-- update the PDRA role intended end date and check the Current Chemist role end date updates
EXECUTE update_visitor_role('Visitor (PDRA)',NULL::date,(current_date + interval '24 months'),NULL::boolean); 
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, (current_date + interval '24 months')::date)$$,'Visitor PDRA has current chemist role with updated end date in future');

-- update the PDRA role end date and check the Current Chemist role end date updates
EXECUTE update_visitor_role('Visitor (PDRA)',(current_date - interval '1 day'),(current_date + interval '24 months'),NULL::boolean); 
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, (current_date - interval '1 day')::date)$$,'Visitor PDRA has current chemist role with updated end date in past');

-- update the PDRA role to be past without a specific leaving date, and check the Current Chemist role end date updates
EXECUTE update_visitor_role('Visitor (PDRA)',NULL::date,(current_date + interval '24 months'),'t'::boolean); 
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, '-infinity'::date)$$,'Visitor PDRA has current chemist role with end date of -infinity');

-- remove the person's HR roles again
DELETE FROM public.visitorship WHERE person_id = (SELECT id FROM public.person WHERE crsid = :testcrsid);
SELECT results_eq('get_current_chemist_role',$$VALUES ( (current_date - interval '1 week')::date, '-infinity'::date)$$,'Person with all HR roles deleted has current chemist role that is inactive');

-- This checks we do the right things with some of our less consistent HR data
-- Make the person not have arrived after all
EXECUTE set_person_arrival_leaving_dates(NULL::DATE,NULL::DATE,NULL::boolean);
-- But give them an HR role anyway
EXECUTE add_visitor_role('Visitor (PDRA)', (current_date - interval '1 week'), (current_date + interval ' 12 months'),NULL::date,NULL::boolean);
-- This should not result in IT access
SELECT results_eq('get_current_chemist_role',$$VALUES ( '-infinity'::date, '-infinity'::date)$$,'Visitor PDRA who has not arrived has current chemist role with start and end date of -infinity');

SELECT finish();

ROLLBACK;
