CREATE OR REPLACE VIEW public._chem_firewardens_mailinglist
 AS
 SELECT person.email_address
   FROM ( SELECT fire_warden.person_id
           FROM fire_warden
        UNION
         SELECT mm_mailinglist_include_person.include_person_id AS person_id
           FROM mm_mailinglist_include_person
             JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
          WHERE mailinglist.name::text = 'chem-firewardens'::text) fw
     JOIN person ON fw.person_id = person.id
     JOIN _physical_status_v3 ps USING (person_id)
  WHERE ps.status_id::text = 'Current'::text;
