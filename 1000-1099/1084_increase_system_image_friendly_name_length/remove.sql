DROP VIEW hotwire3.system_image_hid;

ALTER TABLE system_image ALTER COLUMN friendly_name TYPE varchar(50);

CREATE OR REPLACE VIEW hotwire3.system_image_hid
 AS
 SELECT system_image.id AS system_image_id,
    ((COALESCE(COALESCE(system_image.friendly_name, first_ip.hostname::character varying), (COALESCE(hardware.name::text || ' '::text, ' '::text) || COALESCE(hardware.manufacturer::text, ' '::text))::character varying)::text || ' ('::text) || operating_system.os::text) || ')'::text AS system_image_hid
   FROM system_image
     JOIN hardware ON system_image.hardware_id = hardware.id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN ( SELECT si.id,
            min(ip.hostname::text) AS hostname
           FROM system_image si
             JOIN mm_system_image_ip_address mm ON si.id = mm.system_image_id
             JOIN ip_address ip ON ip.id = mm.ip_address_id
          GROUP BY si.id) first_ip ON system_image.id = first_ip.id
  GROUP BY system_image.id, system_image.friendly_name, first_ip.hostname, hardware.name, hardware.manufacturer, operating_system.os;

ALTER TABLE hotwire3.system_image_hid
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3.system_image_hid TO dev;
GRANT SELECT ON TABLE hotwire3.system_image_hid TO ro_hid;
GRANT SELECT ON TABLE hotwire3.system_image_hid TO wpkguser;


