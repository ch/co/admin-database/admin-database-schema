DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Awards_Summary';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Current_Month_End';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Grants/Month_End';

DROP VIEW hotwire3."10_View/Grants/Month_End";
DROP VIEW hotwire3."10_View/Grants/Current_Month_End";
DROP VIEW hotwire3."10_View/Grants/Awards_Summary";
