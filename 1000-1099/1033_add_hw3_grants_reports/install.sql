CREATE VIEW hotwire3."10_View/Grants/Month_End" AS
SELECT
    award.id,
    holder.holder AS "PI",
    funder.funder,
    award.project_title,
    award.award_number,
    award.project_number,
    award.end_date,
    CASE
        WHEN end_date < current_date THEN 'orange'::text
        ELSE NULL::text
    END AS _cssclass
FROM grants.award
JOIN grants.holder ON award.grant_holder_id = holder.id
JOIN grants.funder ON award.funder_id = funder.id
ORDER BY end_date DESC NULLS LAST
;

ALTER VIEW hotwire3."10_View/Grants/Month_End" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/Month_End" TO grants_administration,grants_ro;

CREATE VIEW hotwire3."10_View/Grants/Current_Month_End" AS
SELECT
    award.id,
    holder.holder AS "PI",
    funder.funder,
    award.project_title,
    award.award_number,
    award.project_number,
    award.end_date,
    CASE
        WHEN end_date < current_date THEN 'orange'::text
        ELSE NULL::text
    END AS _cssclass
FROM grants.award
JOIN grants.holder ON award.grant_holder_id = holder.id
JOIN grants.funder ON award.funder_id = funder.id
WHERE end_date >= make_date(date_part('year',current_date)::int,date_part('month',current_date)::int,1) AND end_date < (make_date(date_part('year',current_date)::int,(date_part('month',current_date)::int),1) + interval '1 month')
ORDER BY end_date DESC NULLS LAST
;

ALTER VIEW hotwire3."10_View/Grants/Current_Month_End" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/Current_Month_End" TO grants_administration,grants_ro;

CREATE VIEW hotwire3."10_View/Grants/Awards_Summary" AS
WITH awards_summary AS (
SELECT
    award.id,
    award.award_number,
    award.project_number,
    holder.holder,
    funder.funder,
    award.funder_ref,
    award.project_title,
    award.staff + award.pooled_labour + award.travel + award.equipment + award.exceptional + award.consumables + award.facilities + award.overheads + award.estates_costs + award.indirect_costs + award.pi_costs + award.infrastructure_tech + award.univ_contribution + award.contingency AS project_value,
    award.start_date,
    award.end_date,
    CASE
        WHEN end_date < current_date THEN 'orange'::text
        ELSE NULL::text
    END AS _cssclass
FROM grants.award
JOIN grants.holder ON award.grant_holder_id = holder.id
JOIN grants.funder ON award.funder_id = funder.id
ORDER BY id DESC
)
SELECT * from awards_summary
;

ALTER VIEW hotwire3."10_View/Grants/Awards_Summary" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/Awards_Summary" TO grants_administration,grants_ro;

INSERT INTO hotwire3._primary_table
    ( view_name, primary_table )
        VALUES
    ( '10_View/Grants/Awards_Summary', 'grants.award'),
    ( '10_View/Grants/Current_Month_End', 'grants.award'),
    ( '10_View/Grants/Month_End', 'grants.award');
