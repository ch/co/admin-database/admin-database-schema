CREATE OR REPLACE VIEW hotwire3."10_View/People/Personnel_Data_Entry"
 AS
 SELECT a.id,
    a.ro_person_id,
    a.image_oid,
    a.surname,
    a.first_names,
    a.title_id,
    a.known_as,
    a.name_suffix,
    a.previous_surname,
    a.date_of_birth,
    a.ro_age,
    a.gender_id,
    a.ro_post_category_id,
    a.ro_chem,
    a.crsid,
    a.email_address,
    a.hide_email,
    a.arrival_date,
    a.leaving_date,
    a.ro_physical_status_id,
    a.left_but_no_leaving_date_given,
    a.dept_telephone_number_id,
    a.hide_phone_no_from_website,
    a.hide_from_website,
    a.room_id,
    a.ro_supervisor_id,
    a.ro_co_supervisor_id,
    a.research_group_id,
    a.home_address,
    a.home_phone_number,
    a.cambridge_college_id,
    a.mobile_number,
    a.nationality_id,
    a.emergency_contact,
    a.other_information,
    a.notes,
    a.forwarding_address,
    a.new_employer_address,
    a.chem_at_cam AS "chem_@_cam",
    a.retired_staff_mailing_list,
    a.continuous_employment_start_date,
    a.paper_file_details AS paper_file_status,
    a.registration_completed,
    a.clearance_cert_signed,
    a.ro_leaving_form_submitted,
    a._cssclass,
    a.treat_as_academic_staff,
    a.passport_issuing_country_id,
    a.passport_number,
    a.passport_expiry_date,
    a.immigration_status_id,
    a.immigration_status_expiry_date,
    a."BRP_number"
   FROM ( SELECT person.id,
            person.id AS ro_person_id,
            person.image_lo AS image_oid,
            person.surname,
            person.first_names,
            person.title_id,
            person.known_as,
            person.name_suffix,
            person.previous_surname,
            person.date_of_birth,
            date_part('year'::text, age(person.date_of_birth::timestamp with time zone)) AS ro_age,
            person.gender_id,
            _latest_role.post_category_id AS ro_post_category_id,
            _latest_role.chem AS ro_chem,
            person.crsid,
            person.email_address,
            person.hide_email,
            person.do_not_show_on_website AS hide_from_website,
            person.arrival_date,
            person.leaving_date,
            _physical_status.status_id AS ro_physical_status_id,
            person.left_but_no_leaving_date_given,
            ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
                   FROM mm_person_dept_telephone_number
                  WHERE person.id = mm_person_dept_telephone_number.person_id) AS dept_telephone_number_id,
            person.hide_phone_no_from_website,
            ARRAY( SELECT mm_person_room.room_id
                   FROM mm_person_room
                  WHERE person.id = mm_person_room.person_id) AS room_id,
            _latest_role.supervisor_id AS ro_supervisor_id,
            _latest_role.co_supervisor_id AS ro_co_supervisor_id,
            _latest_role.mentor_id AS ro_mentor_id,
            ARRAY( SELECT mm_person_research_group.research_group_id
                   FROM mm_person_research_group
                  WHERE person.id = mm_person_research_group.person_id) AS research_group_id,
            person.cambridge_address::text AS home_address,
            person.cambridge_phone_number AS home_phone_number,
            person.cambridge_college_id,
            person.mobile_number,
            ARRAY( SELECT mm_person_nationality.nationality_id
                   FROM mm_person_nationality
                  WHERE mm_person_nationality.person_id = person.id) AS nationality_id,
            person.emergency_contact::text AS emergency_contact,
            person.other_information,
            person.notes,
            person.forwarding_address::text AS forwarding_address,
            person.new_employer_address::text AS new_employer_address,
            person.chem_at_cam,
                CASE
                    WHEN retired_staff_list.include_person_id IS NOT NULL THEN true
                    ELSE false
                END AS retired_staff_mailing_list,
            person.continuous_employment_start_date,
            person.paper_file_details,
            person.registration_completed,
            person.clearance_cert_signed,
            (SELECT MAX(submitted_when) FROM leavers_form.leavers_form_submission WHERE leavers_form_submission.leavers_form_id IN (SELECT leavers_form_overview.id FROM leavers_form.leavers_form_overview WHERE leavers_form_overview.person = person.id)) AS ro_leaving_form_submitted,
            person.counts_as_academic AS treat_as_academic_staff,
            person.passport_issuing_country_id,
            person.passport_number,
            person.passport_expiry_date,
            person.immigration_status_id,
            person.immigration_status_expiry_date,
            person.brp_number AS "BRP_number",
                CASE
                    WHEN _physical_status.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM person
             LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
             LEFT JOIN _physical_status_v3 _physical_status ON person.id = _physical_status.person_id
             LEFT JOIN ( SELECT mm_mailinglist_include_person.mailinglist_id,
                    mm_mailinglist_include_person.include_person_id
                   FROM mm_mailinglist_include_person
                     JOIN mailinglist ON mm_mailinglist_include_person.mailinglist_id = mailinglist.id
                  WHERE mailinglist.name::text = 'chem-retiredstaff'::text) retired_staff_list ON person.id = retired_staff_list.include_person_id) a
  ORDER BY a.surname, a.first_names;
