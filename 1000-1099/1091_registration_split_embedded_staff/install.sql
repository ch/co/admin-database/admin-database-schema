CREATE VIEW registration.embedded_company_staff_type_hid AS
    SELECT
        'v-'::text || visitor_type_hid.visitor_type_id AS id,
        visitor_type_hid.registration_form_name AS hid
    FROM public.visitor_type_hid
    WHERE visitor_type_hid = 'Visitor (Commercial)';

ALTER VIEW registration.embedded_company_staff_type_hid OWNER TO dev;

GRANT SELECT ON registration.embedded_company_staff_type_hid TO starters_registration;

UPDATE public.visitor_type_hid SET show_on_registration_form = FALSE WHERE visitor_type_hid = 'Visitor (Commercial)';
UPDATE public.visitor_type_hid SET show_on_registration_form = TRUE, registration_form_name = 'Other', safety_training_url = (SELECT safety_training_url FROM public.visitor_type_hid WHERE visitor_type_hid = 'Visitor (PDRA)') WHERE visitor_type_hid = 'Visitor (Other)';
UPDATE public.visitor_type_hid SET registration_form_name = 'PDRA' WHERE visitor_type_hid = 'Visitor (PDRA)';

INSERT INTO registration.form_type ( form_type) VALUES ( 'embedded_company_staff' );
