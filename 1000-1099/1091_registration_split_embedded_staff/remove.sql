DROP VIEW registration.embedded_company_staff_type_hid;

UPDATE public.visitor_type_hid SET show_on_registration_form = TRUE WHERE visitor_type_hid = 'Visitor (Commercial)';
UPDATE public.visitor_type_hid SET show_on_registration_form = FALSE, registration_form_name = NULL, safety_training_url = NULL WHERE visitor_type_hid = 'Visitor (Other)';
UPDATE public.visitor_type_hid SET registration_form_name = 'Postdoc' WHERE visitor_type_hid = 'Visitor (PDRA)';

DELETE FROM registration.form_type WHERE form_type = 'embedded_company_staff'; 
