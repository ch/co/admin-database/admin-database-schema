SET ROLE dev;
SET ROLE slony;

-- Adding the roles below  edits the 'postgres' database rather than the
-- database we're connected to, and so makes for difficulties if we want to
-- apply this changeset to two databases on the same cluster - which we usually
-- do, hence the if exists.

DO
$do$
BEGIN
   IF EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'adreconcile') THEN
      DROP ROLE adreconcile;
   END IF;
   IF EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'guacamoleuser') THEN
      DROP ROLE guacamoleuser;
   END IF;
END
$do$;

