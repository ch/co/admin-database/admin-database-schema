SET ROLE dev;
SET ROLE slony;

-- Adding the roles below  edits the 'postgres' database rather than the
-- database we're connected to, and so makes for difficulties if we want to
-- apply this changeset to two databases on the same cluster - which we usually
-- do, hence the if not exists.

DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'adreconcile') THEN
      CREATE ROLE adreconcile LOGIN;
   END IF;
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE  rolname = 'guacamoleuser') THEN
      CREATE ROLE guacamoleuser LOGIN;
   END IF;
END
$do$;

