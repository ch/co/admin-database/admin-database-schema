BEGIN;

SELECT plan(23);

CREATE TEMPORARY TABLE c (
    colname TEXT
);
INSERT INTO c (colname) VALUES ('person_id'), ('photo'), ('photo_version');

-- photo table correctly defined
SELECT has_table('public','person_photo', 'public.person_photo table exists');
SELECT relation_owner_is('public','person_photo', 'dev', 'public.person_photo table owned by dev');
SELECT has_column('public','person_photo',colname,format('person_photo table has column %I',colname)) FROM c;
SELECT col_not_null('public','person_photo',colname,format('person_photo table has not-null column %I',colname)) FROM c;
SELECT col_is_unique('public','person_photo', 'person_id', 'public.person_photo table has unique person_id');
SELECT col_is_fk('public','person_photo', 'person_id', 'public.person_photo person_id column is foreign key');

-- trigger functions
CREATE TEMPORARY TABLE f (
    fname TEXT
);
INSERT INTO f (fname) VALUES ('update_person_photo'), ('audit_photo_table'), ('increment_photo_version');

SELECT has_function('public',fname,'{}'::name[], format('function %I exists',fname)) FROM f;
SELECT function_owner_is('public',fname,'{}'::name[], 'dev',format('function %I owned by dev',fname)) FROM f;
SELECT function_returns('public',fname,'{}'::name[], 'trigger',format('function %I returns trigger',fname)) FROM f;

SELECT has_trigger('public','person','update_photo_table','public.person has trigger to update person_photo');
SELECT has_trigger('public','person_photo','audit_photo_table','public.person_photo has audit trigger');
SELECT has_trigger('public','person_photo','increment_photo_version','public.person_photo has version increment trigger');

-- check the initial data populated
PREPARE large_object_images AS SELECT id FROM public.person WHERE image_lo IS NOT NULL AND image_lo NOT IN (3977791,64253) ORDER BY id;
PREPARE person_photo_rows AS SELECT person_id FROM public.person_photo ORDER BY person_id;

SELECT results_eq('large_object_images', 'person_photo_rows', 'Every non-blank image_lo in person has a person_photo entry');

SELECT * FROM finish();

ROLLBACK;
