BEGIN;

SELECT plan(16);

\set testcrsid '\'fjc55\''
\set minimaljpg '\'/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=\''
\set anotherjpg '\'/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAABAAEDAREAAhEBAxEB/8QAFAABAAAAAAAAAAAAAAAAAAAAC//EABQQAQAAAAAAAAAAAAAAAAAAAAD/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8AP/B//9k=\''

PREPARE find_in_person AS SELECT id FROM public.person WHERE crsid = :testcrsid;
PREPARE find_in_person_photo AS SELECT person_id FROM public.person_photo WHERE person_id = ( SELECT id FROM public.person WHERE crsid = :testcrsid);
PREPARE find_photo AS SELECT photo FROM public.person_photo WHERE person_id = ( SELECT id FROM public.person WHERE crsid = :testcrsid);
PREPARE find_photo_version AS SELECT photo_version::int FROM public.person_photo WHERE person_id = ( SELECT id FROM public.person WHERE crsid = :testcrsid);
PREPARE return_minimaljpg AS SELECT decode(:minimaljpg,'base64');
PREPARE return_anotherjpg AS SELECT decode(:anotherjpg,'base64');
-- Because this all takes place inside a single transaction we can find our audit log entries by checking for 'current time' which is constant within transaction
PREPARE audit_ops AS SELECT COUNT(operation)::int,operation::text FROM _audit WHERE stamp = now() AND tablename = 'person_photo' AND schemaname = 'public' GROUP BY operation ORDER BY operation;

-- ensure nothing else writes person_photo while we're running this, because we need to do audit table checks
-- so we don't want unrelated changes appearing whose timestamp happens to match ours.
LOCK table public.person_photo IN SHARE MODE;

-- make sure there are no matching _audit entries before we start
DELETE FROM _audit WHERE stamp = now() AND tablename = 'person_photo' AND schemaname = 'public';
SELECT is_empty('audit_ops', 'Ensure there are no audit table entries with the current time before we start testing');

-- create a person without a picture, no entry in person_photo
INSERT INTO public.person (surname, first_names, crsid ) VALUES ( 'Test', 'Test', :testcrsid );
SELECT results_ne('find_in_person', 'find_in_person_photo', 'no entry added to person_photo for insert on person without picture');

-- add a photo to it, entry appears
UPDATE person SET image_lo = lo_from_bytea(0,decode(:minimaljpg,'base64')) WHERE crsid = :testcrsid;
SELECT results_eq('find_in_person', 'find_in_person_photo', 'entry added to person_photo for update on person adding picture');
-- version 1
SELECT results_eq('find_photo_version', 'VALUES (1)','photo_version should be 1 on insert');
-- check audit log
SELECT results_eq('audit_ops', $$VALUES (1,'INSERT')$$,'One insert in audit log');

-- remove the photo, entry disappears
UPDATE person SET image_lo = NULL WHERE crsid = :testcrsid;
SELECT results_ne('find_in_person', 'find_in_person_photo', 'entry removed from person_photo when setting image_lo null on person');
-- check audit log
SELECT results_eq('audit_ops', $$VALUES (1,'DELETE'), (1,'INSERT')$$,'One insert and one delete in audit log');

-- add the photo again
UPDATE person SET image_lo = lo_from_bytea(0,decode(:minimaljpg,'base64')) WHERE crsid = :testcrsid;
SELECT results_eq('return_minimaljpg', 'find_photo', 'person_photo data matches added photo');
-- version 1
SELECT results_eq('find_photo_version', 'VALUES (1)','photo_version should be 1 on insert');
-- check audit log
SELECT results_eq('audit_ops', $$VALUES (1,'DELETE'), (2,'INSERT')$$,'Two inserts and one delete in audit log');

-- update the photo, updates
UPDATE person SET image_lo = lo_from_bytea(0,decode(:anotherjpg,'base64')) WHERE crsid = :testcrsid;
SELECT results_eq('find_in_person', 'find_in_person_photo', 'no additional entry added to person_photo for insert on person without picture');
SELECT results_eq('return_anotherjpg', 'find_photo', 'person_photo data has updated');
-- version 2
SELECT results_eq('find_photo_version', 'VALUES (2)','photo_version should be 2 after update');
-- check audit log
SELECT results_eq('audit_ops', $$VALUES (1,'DELETE'), (2,'INSERT'), (1,'UPDATE')$$,'Two inserts, one delete, one update in audit log');

-- remove the person, entry disappears
DELETE FROM public.person WHERE crsid = :testcrsid;
SELECT results_eq('find_in_person', 'find_in_person_photo', 'entry removed from person_photo for delete on person with picture');
-- audit log check
SELECT results_eq('audit_ops', $$VALUES (2,'DELETE'), (2,'INSERT'), (1,'UPDATE')$$,'Two inserts, one delete, one update in audit log');

SELECT * FROM finish();

ROLLBACK;
