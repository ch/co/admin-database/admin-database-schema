CREATE TABLE public.person_photo (
    person_id BIGINT UNIQUE NOT NULL REFERENCES public.person(id) ON DELETE CASCADE,
    photo BYTEA NOT NULL,
    photo_version BIGINT NOT NULL DEFAULT 1
);

-- 64253 and 3977791 are known to be blank images that were set on some rows in the past
INSERT INTO public.person_photo ( person_id, photo ) SELECT id, lo_get(image_lo) FROM public.person WHERE image_lo IS NOT NULL AND image_lo NOT IN (3977791,64253);

ALTER TABLE public.person_photo OWNER TO dev;

GRANT SELECT ON public.person_photo TO www_sites;
-- reception probably should not be allowed to edit photos - this means we need to fix a few views to make images readonly
-- and then remove their rights to this table
GRANT SELECT,INSERT,UPDATE,DELETE ON public.person_photo TO hr,photography,cos,web_editors,reception,gdpr_purge;

CREATE FUNCTION public.update_person_photo() RETURNS TRIGGER AS
$body$
BEGIN
    IF TG_OP = 'INSERT' AND NEW.image_lo IS NOT NULL THEN
        -- inserts with a photo
        INSERT INTO public.person_photo (person_id, photo) VALUES (NEW.id, lo_get(NEW.image_lo));
    ELSE
        -- updates to person may add a photo where there was none, change an existing photo, or make no change to photo
        IF NEW.image_lo IS NOT NULL AND NEW.image_lo IS DISTINCT FROM OLD.image_lo THEN
            INSERT INTO public.person_photo
                (person_id, photo)
                VALUES
                (NEW.id, lo_get(NEW.image_lo))
                ON CONFLICT (person_id) DO UPDATE SET photo = lo_get(NEW.image_lo);
        ELSIF NEW.image_lo IS NULL AND OLD.image_lo IS NOT NULL THEN
            DELETE FROM public.person_photo WHERE person_id = NEW.id;
        END IF;
    END IF;
    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION public.update_person_photo() OWNER TO dev;

-- no need for delete trigger as FK is ON DELETE CASCADE
CREATE TRIGGER update_photo_table AFTER INSERT OR UPDATE ON public.person FOR EACH ROW EXECUTE FUNCTION public.update_person_photo();


CREATE FUNCTION public.increment_photo_version() RETURNS TRIGGER AS
$body$
BEGIN
    -- for updates only: inserts get '1' from the column default
    IF NEW.photo IS DISTINCT FROM OLD.photo THEN
        NEW.photo_version = NEW.photo_version + 1;
    END IF;
    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION public.increment_photo_version() OWNER TO dev;

CREATE TRIGGER increment_photo_version BEFORE UPDATE ON public.person_photo FOR EACH ROW EXECUTE FUNCTION public.increment_photo_version();

-- we need a special audit function as we don't want the bytea content of the photo column in the _audit table
CREATE FUNCTION public.audit_photo_table() RETURNS TRIGGER AS
$body$
DECLARE
    record_id BIGINT;
    before TEXT;
    after TEXT;

BEGIN
    IF TG_OP = 'DELETE' THEN
        record_id = OLD.person_id;
    ELSE
        record_id = NEW.person_id;
    END IF;

    IF TG_OP = 'DELETE' OR TG_OP = 'UPDATE' THEN
        before = 'person_id: ' || OLD.person_id;
        before = before || E'\nphoto_version: ' || OLD.photo_version;
        IF OLD.photo IS NOT NULL THEN
            before = before || E'\nphoto: data';
        END IF;
    END IF;

    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        after = 'person_id: ' || NEW.person_id;
        after = after || E'\nphoto_version: ' || NEW.photo_version;
        IF NEW.photo IS NOT NULL THEN
            after = after || E'\nphoto: data';
        END IF;
    END IF;

    INSERT INTO public._audit
        ( operation, stamp, username, tablename, id, schemaname, oldcols, newcols )
        VALUES
        ( TG_OP, now(), current_user, TG_TABLE_NAME, record_id, TG_TABLE_SCHEMA, before, after );
    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION public.audit_photo_table() OWNER TO dev;

CREATE TRIGGER audit_photo_table AFTER INSERT OR UPDATE OR DELETE ON public.person_photo FOR EACH ROW EXECUTE FUNCTION public.audit_photo_table();
