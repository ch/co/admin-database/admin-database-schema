DROP TRIGGER update_photo_table ON public.person;
DROP FUNCTION public.update_person_photo();
DROP TABLE public.person_photo;
DROP FUNCTION public.increment_photo_version();
DROP FUNCTION public.audit_photo_table();
