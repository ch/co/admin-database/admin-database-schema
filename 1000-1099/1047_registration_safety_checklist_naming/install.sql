
CREATE OR REPLACE FUNCTION registration.state_of_form(
	form_uuid uuid)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE SECURITY DEFINER PARALLEL UNSAFE
    SET search_path=registration, public, pg_temp
AS $BODY$
DECLARE
    form registration.form%ROWTYPE;
    reason text[] := '{}';
BEGIN

-- The logic in here must match that in the registration web app

SELECT * INTO form FROM registration.form WHERE uuid = form_uuid;

IF form.submitted IS DISTINCT FROM true THEN
    reason = array_append(reason,'Main form not submitted');
END IF;

-- originally the safety checklist was done on separate paper forms
IF form.separate_safety_form = false THEN
    IF form.safety_submitted IS DISTINCT FROM true THEN
        reason = array_append(reason,'Safety checklist not submitted');
    END IF;

    -- if null, the date comparison returns false
    IF form.safety_induction_signed_off_date > current_date OR form.safety_induction_signed_off_date IS NULL THEN
	reason = array_append(reason,'Safety induction not signed off');
    END IF;
END IF;

IF form.safety_training_needs_signoff AND (form.safety_training_signed_off_date > current_date OR form.safety_training_signed_off_date IS NULL) THEN
    reason = array_append(reason,'Safety training requires signing off which is not yet done');
END IF;

IF reason = '{}' THEN
    RETURN 'Complete';
ELSE
    RETURN array_to_string(reason, E'\n');
END IF;

RETURN reason;

END;
$BODY$;

-- 10_View/Safety/Registration

DROP VIEW hotwire3."10_View/Safety/Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Registration"
 AS
 SELECT form.form_id AS id,
    COALESCE(person.first_names, form.first_names::character varying) AS first_names,
    COALESCE(person.surname, form.surname::character varying) AS surname,
    COALESCE(person.known_as, form.known_as::character varying) AS known_as,
    COALESCE(person.title_id, form.title_id) AS title_id,
    COALESCE(_latest_role.post_category_id, form.post_category_id) AS post_category_id,
    COALESCE(ARRAY( SELECT mm_person_room.room_id
           FROM mm_person_room
          WHERE person.id = mm_person_room.person_id), form.dept_room_id) AS room_id,
    COALESCE(ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE person.id = mm_person_dept_telephone_number.person_id), form.dept_telephone_id) AS dept_telephone_number_id,
    COALESCE(person.email_address, form.email::character varying) AS email,
    COALESCE(person.crsid, form.crsid::character varying) AS crsid,
    COALESCE(person.arrival_date, form.start_date) AS arrival_date,
    COALESCE(_latest_role.intended_end_date, form.intended_end_date) AS intended_end_date,
    COALESCE(_latest_role.supervisor_id, form.department_host_id) AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.separate_safety_form as ro_paper_safety_checklist,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    safety_induction_carried_out_by.person_hid AS ro_safety_induction_carried_out_by,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_off_on,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN hotwire3.person_hid safety_induction_carried_out_by ON form.safety_induction_person_signing_off_id = safety_induction_carried_out_by.person_id
     LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
  WHERE form.submitted = true;

ALTER TABLE hotwire3."10_View/Safety/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Registration" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Registration" TO safety_management;

-- 10_View/People/Incomplete_Registration_Forms

DROP VIEW hotwire3."10_View/People/Incomplete_Registration_Forms";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Incomplete_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form._form_started AS ro_form_started_on,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    post_category_hid.post_category_hid::character varying AS ro_post_category,
    form.nationality_id,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.email::character varying AS email,
    form.crsid::character varying AS crsid,
    form.start_date,
    form.intended_end_date,
    form.department_host_id AS supervisor_id,
    form.submitted AS _personal_details_submitted,
    NULL::boolean AS reopen_personal_details_form,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signer_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_paper_safety_checklist,
    form.safety_submitted AS _safety_submitted,
    NULL::boolean AS reopen_safety_checklist,
    registration.state_of_form(form.uuid) AS ro_reason,
    form.uuid AS form_uuid,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
     LEFT JOIN hotwire3.post_category_hid ON post_category_hid.post_category_id = form.post_category_id
  WHERE registration.state_of_form(form.uuid) <> 'Complete'::text;

ALTER TABLE hotwire3."10_View/People/Incomplete_Registration_Forms"
    OWNER TO dev;

GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO dev;
GRANT SELECT, DELETE ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/Incomplete_Registration_Forms" TO student_management;

CREATE RULE hw3_incomplete_reg_del AS
    ON DELETE TO hotwire3."10_View/People/Incomplete_Registration_Forms"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.uuid = old.form_uuid));

CREATE OR REPLACE FUNCTION hotwire3.incomplete_registration_form_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    -- reopen forms for editing in the registration app
    IF NEW.reopen_personal_details_form THEN
        NEW._personal_details_submitted = FALSE::boolean;
    END IF;
    IF NEW.reopen_safety_checklist THEN
        NEW._safety_submitted = FALSE::boolean;
    END IF;
    UPDATE registration.form SET
    first_names = NEW.first_names,
    surname = NEW.surname,
    known_as = NEW.known_as,
    title_id = NEW.title_id,
    date_of_birth = NEW.date_of_birth,
    gender_id = NEW.gender_id,
    nationality_id = NEW.nationality_id,
    dept_room_id = NEW.room_id,
    dept_telephone_id = NEW.dept_telephone_number_id,
    email = NEW.email::text,
    crsid = NEW.crsid,
    start_date = NEW.start_date,
    intended_end_date = NEW.intended_end_date,
    department_host_id = NEW.supervisor_id,
    submitted = NEW._personal_details_submitted,
    safety_submitted = NEW._safety_submitted
     WHERE form_id = OLD.id;
RETURN NEW;
END;
$BODY$;

CREATE TRIGGER incomplete_form_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Incomplete_Registration_Forms"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.incomplete_registration_form_upd();

-- 10_View/People/Registration

DROP VIEW hotwire3."10_View/People/Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Registration"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_paper_safety_checklist,
    form.hide_from_website,
    form.already_has_university_card,
    form.previously_registered,
        CASE
            WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
            ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid::character varying, form.date_of_birth)
        END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    false AS import_this_record,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE registration.state_of_form(form.uuid) = 'Complete'::text AND form._imported_on IS NULL;

ALTER TABLE hotwire3."10_View/People/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Registration" TO hr;

CREATE RULE hotwire3_view_registration_del AS
    ON DELETE TO hotwire3."10_View/People/Registration"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.form_id = old.id));

CREATE TRIGGER registration_form_update
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/People/Registration"
    FOR EACH ROW
    EXECUTE FUNCTION registration.people_registration_upd();

-- 10_View/People/Processed_Registration_Forms

DROP VIEW hotwire3."10_View/People/Processed_Registration_Forms";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Processed_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form as paper_safety_checklist,
    form.hide_from_website,
    form.previously_registered,
    form.already_has_university_card,
    form._match_to_person_id AS match_to_person_id,
    form._imported_on AS ro_imported_on,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE form._imported_on IS NOT NULL;

ALTER TABLE hotwire3."10_View/People/Processed_Registration_Forms"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO hr;
