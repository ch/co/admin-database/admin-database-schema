BEGIN;
\set schema '\'it_access\''
\set test_crsid1 '\'fjc55\''
\set test_crsid2 '\'spqr1\''
SELECT
    plan(23);
-----------------------------
-- Test person_role behaviour
-----------------------------
-- prepared statements for testing person_role behaviour
-- add a person
PREPARE add_person(varchar(32), varchar(32), varchar(8)) AS
INSERT INTO public.person(surname, first_names, crsid)
    VALUES ($1, $2, $3);
-- ensures a specific role type for a person exists with given end date
PREPARE ensure_person_role(text, text, date, date) AS
INSERT INTO it_access.person_role(person_id, role_type_id, start_date, end_date)
    VALUES ((
            SELECT
                id
            FROM
                public.person
            WHERE
                crsid = $1),(
                SELECT
                    role_type_id
                FROM
                    it_access.role_type
                WHERE
                    role_type = $2), $3, $4)
    ON CONFLICT (person_id,
        role_type_id)
WHERE
    role_type_id = 2
        DO UPDATE SET
            start_date = $3,
            end_date = $4;
;
-- clean up roles for specific person
PREPARE remove_roles_for_person(text) AS
DELETE FROM it_access.person_role
WHERE person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = $1);
-- add some test people
EXECUTE add_person('Test Person', 'Tess', :test_crsid1);
EXECUTE add_person('Test Person2', 'Tess', :test_crsid2);
---------------------------------------------------------
-- test capping of end dates for some non-extension roles
---------------------------------------------------------
-- Check Linux ssh role (a capped role)
\set test_role_type '\'Linux ssh only\''
PREPARE get_role_end_date AS
SELECT
    end_date
FROM
    it_access.person_role
WHERE
    role_type_id =(
        SELECT
            role_type_id
        FROM
            it_access.role_type
        WHERE
            role_type = :test_role_type)
    AND person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = :test_crsid1);
-- ensure no pre-existing roles for test user
EXECUTE remove_roles_for_person(:test_crsid1);
-- Check the cases
-- Linux ssh only capped at 12 months
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months' + interval '1 day'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months')::date], 'Linux ssh only capped at 12 months');
-- Linux ssh only for exactly 12 months allowed
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months')::date], 'Linux ssh only for exactly 12 months allowed');
-- Linux ssh for less than 12 months not changed
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months' - interval '1 day'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months' - interval '1 day')::date], 'Linux ssh only for less than 12 months allowed');
-- Check full remote access role (capped)
\set test_role_type '\'Full remote access\''
DEALLOCATE get_role_end_date;
PREPARE get_role_end_date AS
SELECT
    end_date
FROM
    it_access.person_role
WHERE
    role_type_id =(
        SELECT
            role_type_id
        FROM
            it_access.role_type
        WHERE
            role_type = :test_role_type)
    AND person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = :test_crsid1);
-- Ensure no pre-existing roles for user
EXECUTE remove_roles_for_person(:test_crsid1);
-- Check the cases
-- Full remote access for more than 12 months capped at 12 months
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months' + interval '1 day'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months')::date], 'Full remote access capped at 12 months');
-- allow full remote access of exactly 12 months
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months')::date], 'Full remote access of exactly 12 months unchanged');
-- Full remote access for less than 12 months should not be capped
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE,(CURRENT_DATE + interval '12 months' - interval '1 day'));
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months' - interval '1 day')::date], 'Full remote access end date cap unchanged if less than 12 months away');
---------------------------------------------------------------------
-- test extension roles behave properly, these have extra restrictions
---------------------------------------------------------------------
-- set up statement to get end date for tests for first leaver extension
DEALLOCATE get_role_end_date;
PREPARE get_role_end_date AS
SELECT
    end_date
FROM
    it_access.person_role
WHERE
    role_type_id =(
        SELECT
            role_type_id
        FROM
            it_access.role_type
        WHERE
            role_type = 'First leaver extension')
    AND person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = :test_crsid1);
-- start with no roles for test users
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE remove_roles_for_person(:test_crsid2);
-- create an extension role without a current chemist role for the same person
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '5 months');
SELECT
    results_eq('get_role_end_date', ARRAY['-infinity'::date], 'First extension without current chemist role has -infinity end date');
-- remove roles again
EXECUTE remove_roles_for_person(:test_crsid1);
-- Now create a valid enabling role and an extension within the limit
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '5 months');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '5 months')::date], 'First extension with current chemist role has set end date');
-- remove roles again
EXECUTE remove_roles_for_person(:test_crsid1);
-- Now create a valid enabling role and an extension at the limit
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '6 months');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '6 months')::date], 'First extension with current chemist role can have end date 6 months after current chemist role end');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role and an extension with a date that is over the limit
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '1 week' + interval '6 months')::date], 'First extension with current chemist role has end date capped at 6 months after current chemist role end');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role with an infinite end date
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', 'infinity'::date);
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '6 months')::date], 'First extension with current chemist role with infinite end date has end date capped at 6 months after current date');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role with a = infinite end date
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', '-infinity'::date);
EXECUTE ensure_person_role(:test_crsid1, 'First leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_role_end_date', ARRAY['-infinity'::date], 'First extension with current chemist role with -infinity end date has end date -infinity');
------------------------------------------------------------------------------
-- second leaver extensions
------------------------------------------------------------------------------
-- set up statement to get end date for tests for second leaver extension
DEALLOCATE get_role_end_date;
PREPARE get_role_end_date AS
SELECT
    end_date
FROM
    it_access.person_role
WHERE
    role_type_id =(
        SELECT
            role_type_id
        FROM
            it_access.role_type
        WHERE
            role_type = 'Second leaver extension')
    AND person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = :test_crsid1);
-- start with no roles for test users
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE remove_roles_for_person(:test_crsid2);
-- create an extension role without a current chemist role for the same person
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '5 months');
SELECT
    results_eq('get_role_end_date', ARRAY['-infinity'::date], 'Second extension without current chemist role has -infinity end date');
-- remove roles again
EXECUTE remove_roles_for_person(:test_crsid1);
-- Now create a valid enabling role and an extension
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '7 months');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '7 months')::date], 'Second extension with current chemist role has set end date');
-- remove roles again
EXECUTE remove_roles_for_person(:test_crsid1);
-- Now create a valid enabling role and an extension at the limit
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 week' + interval '12 months');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '1 week' + interval '12 months')::date], 'Second extension with current chemist role can have end date 12 months after current chemist role end');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role and an extension with a date that is over the limit
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', CURRENT_DATE + interval '1 week');
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '2 years');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '1 week' + interval '12 months')::date], 'Second extension with current chemist role has end date capped at 12 months after current chemist role end');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role with an infinite end date
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', 'infinity'::date);
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE + interval '12 months')::date], 'Second extension with current chemist role with infinite end date has end date capped at 12 months after current date');
-- remove roles
EXECUTE remove_roles_for_person(:test_crsid1);
-- create a valid enabling role with an - infinite end date
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '1 year', '-infinity'::date);
EXECUTE ensure_person_role(:test_crsid1, 'Second leaver extension', CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_role_end_date', ARRAY['-infinity'::date], 'Second extension with current chemist role with -infinite end date has end date -infinity');
----------------------------------------------------------------------------
-- test that extensions behave properly when a former current chemist returns
----------------------------------------------------------------------------
\set test_role_type '\'First leaver extension\''
-- set up a get_role_end_date statement for the current extension type and person
DEALLOCATE get_role_end_date;
PREPARE get_role_end_date AS
SELECT
    end_date
FROM
    it_access.person_role
WHERE
    role_type_id =(
        SELECT
            role_type_id
        FROM
            it_access.role_type
        WHERE
            role_type = :test_role_type)
    AND person_id =(
        SELECT
            id
        FROM
            public.person
        WHERE
            crsid = :test_crsid1)
ORDER BY
    end_date;
-- start off with no roles for test person
EXECUTE remove_roles_for_person(:test_crsid1);
-- add a current chemist role with start and end in the past
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '2 years',(CURRENT_DATE - interval '1 year'));
-- add an extension using the above enabling role, also past
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE - interval '1 year',(CURRENT_DATE - interval '9 months'));
-- check that worked OK and the date is as expected
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE - interval '9 months')::date], 'First extension of 3 months is uncapped, in the past');
-- update the current chemist role as if the person has returned
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE, CURRENT_DATE + interval '1 year');
-- check the previous extension
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE - interval '9 months')::date], 'A past first extension is unchanged when the current chemist role is updated because the person returned');
-- they left again and got another extension
EXECUTE ensure_person_role(:test_crsid1, :test_role_type, CURRENT_DATE + interval '1 year', CURRENT_DATE + interval '17 months');
-- check they got a new extension
SELECT
    results_eq('get_role_end_date', ARRAY[(CURRENT_DATE - interval '9 months')::date,(CURRENT_DATE + interval '17 months')::date], 'A past first extension can be updated when the person leaves again');
-------------------------------------------------------------------------------------------------------------------
-- FIXME check we can have multiple roles of non current chemist types other than SPRI
-------------------------------------------------------------------------------------------------------------------
PREPARE get_person_role_count AS
SELECT
    count(*)
FROM
    it_access.person_role
WHERE
    person_id =(
        SELECT
            id
        FROM
            person
        WHERE
            crsid = :test_crsid1);
PREPARE insert_one_of_each_non_chemist_role(text, date, date) AS
INSERT INTO it_access.person_role(person_id, role_type_id, start_date, end_date)
SELECT
    (
        SELECT
            id
        FROM
            person
        WHERE
            crsid = $1), role_type_id, $2, $3
FROM
    it_access.role_type
WHERE
    role_type <> 'Current Chemist';
PREPARE count_non_chemist_role_types_and_double AS
SELECT
    2 * COUNT(role_type)
FROM
    it_access.role_type
WHERE
    role_type <> 'Current Chemist';
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE insert_one_of_each_non_chemist_role(:test_crsid1, CURRENT_DATE - interval '3 years', CURRENT_DATE - interval '2 years');
EXECUTE insert_one_of_each_non_chemist_role(:test_crsid1, CURRENT_DATE, CURRENT_DATE + interval '1 year');
SELECT
    results_eq('get_person_role_count', 'count_non_chemist_role_types_and_double', 'Can have two non-Current Chemist roles');
----------------------------------------------------------------------------------------
-- check we cannot have two current chemist roles for the same person
----------------------------------------------------------------------------------------
EXECUTE remove_roles_for_person(:test_crsid1);
EXECUTE ensure_person_role(:test_crsid1, 'Current Chemist', CURRENT_DATE - interval '3 years', CURRENT_DATE - interval '1 year');
PREPARE add_another_current_chemist AS
INSERT INTO it_access.person_role(person_id, role_type_id, start_date, end_date)
    VALUES ((
            SELECT
                id
            FROM
                public.person
            WHERE
                crsid = :test_crsid1),(
                SELECT
                    role_type_id
                FROM
                    it_access.role_type
                WHERE
                    role_type = 'Current Chemist'), CURRENT_DATE, CURRENT_DATE + interval '1 year');

SELECT
    throws_ok('add_another_current_chemist', 'duplicate key value violates unique constraint "person_only_has_one_current_chemist_role"', 'Cannot add 2 current chemist roles for same person');


SELECT
    *
FROM
    finish();
ROLLBACK;

