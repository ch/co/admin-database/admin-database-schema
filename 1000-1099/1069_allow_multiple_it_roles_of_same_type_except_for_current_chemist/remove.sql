SET ROLE dev;


ALTER TABLE it_access.application ADD COLUMN enabling_role_id bigint;

ALTER TABLE it_access.application
  ADD CONSTRAINT only_extensions_need_a_previous_role CHECK (
    (enabling_role_id IS NULL AND (role_type_id <> 5 OR role_type_id <> 6)) 
    OR 
    (enabling_role_id IS NOT NULL AND (role_type_id = 5 OR role_type_id = 6))
  );

ALTER TABLE it_access.person_role ADD COLUMN enabling_role_id bigint;

ALTER TABLE it_access.person_role
  ADD CONSTRAINT extensions_have_enabling_role CHECK ((enabling_role_id IS NULL AND role_type_id <> 5 AND role_type_id <> 6) OR (enabling_role_id IS NOT NULL AND (role_type_id = 5 OR role_type_id = 6)));


ALTER TABLE it_access.application
  ADD CONSTRAINT enabling_role_fk_person_role FOREIGN KEY (enabling_role_id) REFERENCES it_access.person_role (person_role_id);

ALTER TABLE it_access.person_role
  ADD CONSTRAINT enabling_role_fk_person_role FOREIGN KEY (enabling_role_id) REFERENCES it_access.person_role (person_role_id);

-- function to cap end dates for person_role inserts/updates
CREATE OR REPLACE FUNCTION it_access.cap_role_end_dates ()
  RETURNS TRIGGER
  AS $body$
DECLARE
  role_time_limit interval;
  previous_role_end_date date;
BEGIN

  -- get end date of any enabling role, which becomes the effective start date of this one.
  -- if the role is one with an end_date of +infinity, we use the current date instead.
  -- A negative infinite date is left unchanged, which will lead to the extension
  -- having an end date of -infinity too, which is fine because that's a case where
  -- we should not be granting an extension until the person has sorted their records
  -- out.
  SELECT
    CASE WHEN end_date = 'infinity'::date THEN current_date
    ELSE end_date
    END AS end_date INTO previous_role_end_date
  FROM
    it_access.person_role
  WHERE
    person_role_id = NEW.enabling_role_id;

  -- time limit of new role, which is given by its role_type
  SELECT
    time_limit INTO role_time_limit
  FROM
    it_access.role_type
  WHERE
    role_type.role_type_id = NEW.role_type_id;

  -- cap any end dates that go past the limit for this role
  IF NEW.end_date > COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit THEN
    NEW.end_date = COALESCE(previous_role_end_date, NEW.start_date) + role_time_limit;
  END IF;

  RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

-- ensure extension roles and applications have a valid enabling role
CREATE FUNCTION it_access.check_enabling_role_for_extensions ()
  RETURNS TRIGGER
  AS $body$
DECLARE
  enabling_role_person_id bigint;
  enabling_role_role_type_id bigint;
BEGIN
  SELECT
    person_id,
    role_type_id INTO enabling_role_person_id,
    enabling_role_role_type_id
  FROM
    it_access.person_role
  WHERE
    person_role_id = NEW.enabling_role_id;
  IF enabling_role_person_id <> NEW.person_id THEN
    RAISE EXCEPTION 'Person for this extension does not match person for enabling role';
    RETURN NULL;
  END IF;
  IF enabling_role_role_type_id <> 2 THEN
    RAISE EXCEPTION 'Cannot grant an extension to a non Current Chemist role';
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$body$
LANGUAGE plpgsql;

CREATE TRIGGER check_enabling_role
  BEFORE INSERT OR UPDATE ON it_access.application
  FOR EACH ROW
  WHEN (NEW.enabling_role_id IS NOT NULL)
  EXECUTE FUNCTION it_access.check_enabling_role_for_extensions ();

CREATE TRIGGER check_enabling_role
  BEFORE INSERT OR UPDATE ON it_access.person_role
  FOR EACH ROW
  WHEN (NEW.enabling_role_id IS NOT NULL)
  EXECUTE FUNCTION it_access.check_enabling_role_for_extensions ();

DROP INDEX it_access.person_only_has_one_current_chemist_role;
CREATE UNIQUE INDEX person_only_has_one_of_each_it_role ON it_access.person_role (person_id, role_type_id);

CREATE OR REPLACE FUNCTION it_access.update_it_access_person_role() RETURNS TRIGGER AS
$body$
DECLARE

    affected_person_id BIGINT;
    new_it_role_start_date DATE;
    new_it_role_end_date DATE;
    original_notes TEXT;
    original_it_role_start_date DATE;
    original_it_role_end_date DATE;

    current_chem_role_type_id BIGINT = 2;

BEGIN

    -- check what table and operation called us, and set the person_id to
    -- operate on accordingly, or exit without changes.
    IF TG_TABLE_NAME = 'person' AND TG_OP = 'UPDATE' THEN
        -- If we're been triggered by an update on person we also check if any
        -- column changed that could affect it_access.person_role. If not we
        -- return straight away. Doing it this way means we don't have to grant as many
        -- permissions, because lots of roles can update a few columns in public.person
        -- and so will fire this trigger, but very few can update the columns
        -- that cause the trigger to make any changes to it_access.person_role.
        -- We also install the trigger as ON UPDATE OF leaving_date, arrival_date, left_but_no_leaving_date_given
        -- which doesn't achieve quite the same thing as checking if a column changed, as one can update a column
        -- to the same value as before.
        IF (
            (NEW.arrival_date IS DISTINCT FROM OLD.arrival_date)
            OR
            (NEW.leaving_date IS DISTINCT FROM OLD.leaving_date)
            OR
            (NEW.left_but_no_leaving_date_given IS DISTINCT FROM OLD.left_but_no_leaving_date_given)
        ) THEN
            affected_person_id = NEW.id;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF TG_TABLE_NAME = '_hr_role' THEN
        -- Only student_management, role_end_date_setter, gdpr_purge, and hr
        -- can make changes to the _hr_role table, and the changes they could make are
        -- very likely to cause a change in it_access.person_role. So for simplicity we
        -- don't do a detailed check of OLD vs NEW before running the rest of
        -- the trigger if we're fired on a change to _hr_role.
        IF ( TG_OP = 'DELETE' ) THEN
            affected_person_id = OLD.person_id;
        ELSIF ( TG_OP = 'INSERT' ) THEN
            affected_person_id = NEW.person_id;
        ELSIF ( TG_OP = 'UPDATE' ) THEN
            affected_person_id = NEW.person_id;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        -- We weren't fired on the right table or operation, so return unchanged
        RETURN NEW;
    END IF;

    -- get new dates for Current Chemist IT access
    SELECT
        current_chemist_role_start_date,
        current_chemist_role_end_date
    INTO
        new_it_role_start_date,
        new_it_role_end_date
    FROM it_access.get_current_chemist_it_role_dates_for_person(affected_person_id) ;

    -- get current dates for Current Chemist IT access
    SELECT
        start_date,
        end_date
    INTO
        original_it_role_start_date,
        original_it_role_end_date
    FROM it_access.person_role
    WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

    -- Did anything actually change?
    IF (
            (new_it_role_start_date IS DISTINCT FROM original_it_role_start_date)
            OR
            (new_it_role_end_date IS DISTINCT FROM original_it_role_end_date)
    ) THEN
        -- If either of the new dates is null then that's probably because an _hr_role added
        -- by mistake was removed. We want to keep a record that the person had some IT access
        -- so we set the new dates to '-infinity'::DATE in that case.
        new_it_role_start_date = COALESCE(new_it_role_start_date,'-infinity'::DATE);
        new_it_role_end_date = COALESCE(new_it_role_end_date,'-infinity'::DATE);

        -- get any existing notes about the person's Current Chemist role
        SELECT COALESCE(notes,'') INTO original_notes
        FROM it_access.person_role
        WHERE role_type_id = current_chem_role_type_id AND person_id = affected_person_id;

        -- Changes to _hr_role can insert a new Current Chemist role so do an upsert
        IF TG_TABLE_NAME = '_hr_role' THEN
            INSERT INTO it_access.person_role
                (
                    person_id,
                    role_type_id,
                    start_date,
                    end_date,
                    notes
                )
                VALUES
                (
                    affected_person_id,
                    current_chem_role_type_id,
                    new_it_role_start_date,
                    new_it_role_end_date,
                    FORMAT(E'%s: Insert new Current Chemist IT role from changes in _hr_role by %s\n',now(), current_user)
                )
            ON CONFLICT (person_id, role_type_id)
            DO UPDATE SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in _hr_role by %s\n',now(), current_user)||original_notes;

        -- changes to person can only update an existing current chemist role, not create one
        ELSIF TG_TABLE_NAME = 'person' THEN

            UPDATE it_access.person_role SET
                start_date = new_it_role_start_date,
                end_date = new_it_role_end_date,
                notes = FORMAT(E'%s: Update Current Chemist role from changes in person by %s\n',now(), current_user)||original_notes
            WHERE person_id = affected_person_id AND role_type_id = current_chem_role_type_id;

       END IF;
    END IF;

    RETURN NEW;
END;
$body$
LANGUAGE plpgsql;