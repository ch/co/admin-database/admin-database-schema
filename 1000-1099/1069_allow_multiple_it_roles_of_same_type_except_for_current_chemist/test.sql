BEGIN;

\set schema '\'it_access\''

SELECT plan(30);

-- schema exists
SELECT has_schema(:schema);

-- make lists of things to check
CREATE TEMPORARY TABLE test_tabs ( tabname text NOT NULL );
INSERT INTO test_tabs ( tabname ) 
VALUES 
        ('role_type'),
        ('researcher_terms'),
        ('authorizer_terms'),
        ('application'),
        ('hod_authorizer_role'),
        ('person_role'),
        ('account_type'),
        ('mm_role_type_account_type')
;

CREATE TEMPORARY TABLE test_funcs ( funcname text NOT NULL );
INSERT INTO test_funcs ( funcname ) 
VALUES
    ('cap_role_end_dates')
;

-- tables exist
SELECT has_table(:schema,
                 tabname,
                 format('Has %I.%I', :schema, tabname)
       )
FROM test_tabs;

-- table ownership correct
SELECT table_owner_is(:schema, tabname, 'dev', format('Owner of %I.%I is dev', :schema, tabname))
FROM test_tabs;

-- Tables have audit trigger
SELECT has_trigger(:schema, tabname, 'audit', format('Audit trigger exists on %I.%I', :schema, tabname))
FROM test_tabs;

-- Application and person_role tables have not got enabling role trigger
SELECT hasnt_trigger(:schema, tabname, 'check_enabling_role', format('Enabling role trigger does not exist on %I.%I', :schema, tabname))
FROM ( VALUES ('person_role' ), ('application' ) ) AS x (tabname);

-- functions exist and are owned by dev
SELECT has_function(:schema,funcname, format('Has function %I.%I',:schema,funcname)) FROM test_funcs;
SELECT function_owner_is(:schema, funcname,'{}', 'dev', format('Has function %I.%I',:schema,funcname)) FROM test_funcs;

-- the right number of roles have been set up
SELECT results_eq('SELECT COUNT(*) FROM it_access.role_type',ARRAY[10::bigint]);

SELECT * FROM finish();

ROLLBACK;
