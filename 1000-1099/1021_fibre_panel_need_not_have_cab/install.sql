CREATE OR REPLACE VIEW hotwire3.fibre_panel_a_hid
 AS
 SELECT fibre_panel.id AS fibre_panel_a_id,
    COALESCE((cabinet.cabname::text || '.'::text),''::text) || fibre_panel.name::text AS fibre_panel_a_hid
   FROM fibre_panel
     LEFT JOIN cabinet ON fibre_panel.cabinet_id = cabinet.id;

CREATE OR REPLACE VIEW hotwire3.fibre_panel_b_hid
 AS
 SELECT fibre_panel.id AS fibre_panel_b_id,
    COALESCE((cabinet.cabname::text || '.'::text),''::text) || fibre_panel.name::text AS fibre_panel_b_hid
   FROM fibre_panel
     LEFT JOIN cabinet ON fibre_panel.cabinet_id = cabinet.id;
