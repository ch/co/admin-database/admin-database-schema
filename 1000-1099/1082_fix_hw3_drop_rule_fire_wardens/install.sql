CREATE OR REPLACE RULE fire_warden_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_wardens"
    DO INSTEAD
(DELETE FROM mm_person_fire_warden_area
  WHERE (mm_person_fire_warden_area_id = OLD.id));

