CREATE OR REPLACE RULE fire_warden_del AS
    ON DELETE TO hotwire3."10_View/Safety/Fire_wardens"
    DO INSTEAD
(DELETE FROM fire_warden
  WHERE (fire_warden.fire_warden_id = old.id));

