SET ROLE dev;

--
-- Grant holders
--

-- subviews
CREATE VIEW hotwire3."10_View/Grants/_grant_holder_applications" AS
SELECT
    application.id,
    application.grant_holder_id AS _grant_holder_id,
    funder.funder,
    application_status.status,
    application.project_title,
    application.start_date,
    application.end_date,
    application.project_number,
    application.award_number
FROM grants.application
LEFT JOIN grants.application_status ON application.status_id = application_status.id
LEFT JOIN grants.funder ON application.funder_id = funder.id
ORDER BY start_date DESC NULLS LAST, end_date DESC NULLS LAST
;

ALTER VIEW hotwire3."10_View/Grants/_grant_holder_applications" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/_grant_holder_applications" TO grants_administration;

CREATE VIEW  hotwire3."10_View/Grants/_grant_holder_awards" AS
SELECT
    award.id,
    award.grant_holder_id AS _grant_holder_id,
    award.funder_id,
    CASE 
        WHEN start_date > current_date THEN 'Future'::varchar
        WHEN end_date >= current_date THEN 'Current'::varchar
        WHEN end_date < current_date THEN 'Past'::varchar
        ELSE 'Unknown'::varchar
    END AS status,
    award.project_title,
    award.start_date,
    award.end_date,
    award.project_number,
    award.award_number
FROM grants.award
ORDER BY start_date DESC NULLS LAST, end_date DESC NULLS LAST
;
ALTER VIEW hotwire3."10_View/Grants/_grant_holder_awards" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/_grant_holder_awards" TO grants_administration;



-- main view, needs updates trigger because of subview
CREATE VIEW hotwire3."10_View/Grants/Grant_holders" AS
SELECT
    id,
    holder,
    person_id,
    _to_hwsubviewb('10_View/Grants/_grant_holder_applications','_grant_holder_id','10_View/Grants/Applications','id','id') AS applications,
    _to_hwsubviewb('10_View/Grants/_grant_holder_awards','_grant_holder_id','10_View/Grants/Awards','id','id') AS awards
FROM grants.holder
ORDER BY holder;

ALTER TABLE hotwire3."10_View/Grants/Grant_holders" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE, DELETE ON hotwire3."10_View/Grants/Grant_holders" TO grants_administration;

CREATE FUNCTION hotwire3.grants_holder_upd() RETURNS TRIGGER AS $$
DECLARE
    new_id BIGINT := NULL;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO grants.holder ( holder, person_id ) VALUES ( NEW.holder, NEW.person_id ) RETURNING id INTO new_id;
        NEW.id := new_id;
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN
        UPDATE grants.holder SET holder = NEW.holder, person_id = NEW.person_id WHERE id = OLD.id;
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        DELETE FROM grants.holder WHERE id = OLD.id;
        RETURN NULL;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION  hotwire3.grants_holder_upd() OWNER TO dev;

CREATE TRIGGER grant_holders_trig INSTEAD OF UPDATE OR INSERT OR DELETE ON hotwire3."10_View/Grants/Grant_holders" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_holder_upd();

--
-- Funders
--

-- subviews
CREATE VIEW hotwire3."10_View/Grants/_funders_applications_subview" AS
SELECT
    application.id,
    application.funder_id as _funder_id,
    application.application_date,
    holder.holder,
    application_status.status,
    application.project_title,
    application.start_date,
    application.end_date,
    application.project_number,
    application.award_number
FROM grants.application
LEFT JOIN grants.holder ON application.grant_holder_id = holder.id
LEFT JOIN grants.application_status ON application.status_id = application_status.id
ORDER BY start_date DESC NULLS LAST, end_date DESC NULLS LAST
;

ALTER TABLE hotwire3."10_View/Grants/_funders_applications_subview" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/_funders_applications_subview" TO grants_administration;

CREATE VIEW hotwire3."10_View/Grants/_funders_awards_subview" AS
SELECT
    award.id,
    award.funder_id as _funder_id,
    holder.holder,
    CASE 
        WHEN start_date > current_date THEN 'Future'::varchar
        WHEN end_date >= current_date THEN 'Current'::varchar
        WHEN end_date < current_date THEN 'Past'::varchar
        ELSE 'Unknown'::varchar
    END AS status,
    award.project_title,
    award.start_date,
    award.end_date,
    award.project_number,
    award.award_number
FROM grants.award
LEFT JOIN grants.holder ON award.grant_holder_id = holder.id
ORDER BY start_date DESC NULLS LAST;

ALTER TABLE hotwire3."10_View/Grants/_funders_awards_subview" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Grants/_funders_awards_subview" TO grants_administration;

-- main view
CREATE VIEW hotwire3."10_View/Grants/Funders" AS
SELECT
    id,
    funder,
    funder_type_id,
    code,
    _to_hwsubviewb('10_View/Grants/_funders_applications_subview','_funder_id','10_View/Grants/Applications','id','id') AS applications,
    _to_hwsubviewb('10_View/Grants/_funders_awards_subview','_funder_id','10_View/Grants/Awards','id','id') AS awards
FROM grants.funder
ORDER BY funder;

-- triggers
CREATE FUNCTION hotwire3.grants_funders_upd() RETURNS TRIGGER AS
$$
DECLARE
    new_id BIGINT := NULL;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO grants.funder ( funder, funder_type_id, code ) VALUES ( NEW.funder, NEW.funder_type_id, NEW.code ) RETURNING id INTO new_id;
        NEW.id := new_id;
        RETURN NEW;
    ELSIF ( TG_OP = 'UPDATE') THEN
        UPDATE grants.funder SET funder = NEW.funder, funder_type_id = NEW.funder_type_id, code = NEW.code WHERE id = OLD.id;
        RETURN NEW;
    ELSIF ( TG_OP = 'DELETE') THEN
        DELETE FROM grants.funder WHERE id = OLD.id;
        RETURN NULL;
    END IF;
    -- shouldn't get here, but in case something changes
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.grants_funders_upd() OWNER TO dev;

CREATE TRIGGER grants_funders_trig INSTEAD OF UPDATE OR INSERT OR DELETE ON hotwire3."10_View/Grants/Funders" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_funders_upd();

--
-- Research area
--

ALTER TABLE hotwire3."10_View/Grants/Funders" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Grants/Funders" TO grants_administration;

-- auto updates
CREATE VIEW hotwire3."10_View/Grants/Research_areas" AS
SELECT
    id,
    research_area,
    research_code
FROM grants.research_area
ORDER BY research_area
;

ALTER TABLE hotwire3."10_View/Grants/Research_areas" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Grants/Research_areas" TO grants_administration;

--
-- Application status
--

CREATE VIEW hotwire3."10_View/Grants/_application_status_subview" AS
SELECT
    application.id,
    status_id as _application_status_id,
    application_date,
    holder.holder,
    funder.funder,
    project_title,
    start_date,
    end_date,
    project_number,
    grant_awarded,
    x5_number,
    award_number
FROM grants.application
LEFT JOIN grants.holder ON application.grant_holder_id = holder.id
LEFT JOIN grants.funder ON application.funder_id = funder.id
ORDER BY start_date DESC NULLS LAST;
ALTER TABLE hotwire3."10_View/Grants/_application_status_subview" OWNER TO dev;

-- main view, cannot auto update because of subview
CREATE VIEW hotwire3."10_View/Grants/Application_Statuses" AS
SELECT
    id,
    status,
    _to_hwsubviewb('10_View/Grants/_application_status_subview','_application_status_id','10_View/Grants/Applications','id','id') AS applications
FROM grants.application_status
;

ALTER TABLE hotwire3."10_View/Grants/Application_Statuses" OWNER TO dev;
GRANT SELECT,INSERT,UPDATE,DELETE ON hotwire3."10_View/Grants/Application_Statuses" TO grants_administration;

-- triggers
CREATE FUNCTION hotwire3.grants_application_status_upd() RETURNS TRIGGER AS
$$
DECLARE
    new_id BIGINT := NULL;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO grants.application_status ( status ) VALUES ( NEW.status ) RETURNING id INTO new_id;
        NEW.id := new_id;
        RETURN NEW;
    ELSIF ( TG_OP = 'UPDATE') THEN
        UPDATE grants.application_status SET status = NEW.status WHERE id = OLD.id;
        RETURN NEW;
    ELSIF ( TG_OP = 'DELETE') THEN
        DELETE FROM grants.application_status WHERE id = OLD.id;
        RETURN NULL;
    END IF;
    -- shouldn't get here, but in case something changes
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION hotwire3.grants_application_status_upd() OWNER TO dev;

CREATE TRIGGER grants_application_status_trig INSTEAD OF UPDATE OR INSERT OR DELETE ON hotwire3."10_View/Grants/Application_Statuses" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_application_status_upd();
