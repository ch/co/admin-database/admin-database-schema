SET ROLE dev;

CREATE VIEW hotwire3.funder_to_merge_hid AS
SELECT id AS funder_to_merge_id, funder AS funder_to_merge_hid
FROM grants.funder
ORDER BY funder_to_merge_hid;

ALTER VIEW hotwire3.funder_to_merge_hid OWNER TO dev;
GRANT SELECT ON hotwire3.funder_to_merge_hid TO grants_administration;

CREATE VIEW hotwire3."10_View/Grants/Merge_Funders" AS
SELECT
    funder.id,
    funder.funder,
    funder.funder_type_id,
    funder.code,
    NULL::BIGINT AS funder_to_merge_id,
    _to_hwsubviewb('10_View/Grants/_funders_applications_subview','_funder_id','10_View/Grants/Applications','id','id') AS applications,
    _to_hwsubviewb('10_View/Grants/_funders_awards_subview','_funder_id','10_View/Grants/Awards','id','id') AS awards
FROM grants.funder
ORDER BY funder;

ALTER VIEW hotwire3."10_View/Grants/Merge_Funders" OWNER TO dev;
GRANT SELECT,UPDATE ON hotwire3."10_View/Grants/Merge_Funders" TO grants_administration;

CREATE FUNCTION hotwire3.grants_merge_funders_upd() RETURNS TRIGGER AS $$
DECLARE
    new_funder_type_id BIGINT := NEW.funder_type_id;
    new_funder_code VARCHAR := NEW.code;
BEGIN
    -- merge the code and funder type, this row's id wins
    IF NEW.funder_to_merge_id IS NOT NULL THEN
        SELECT
            COALESCE(first.funder_type_id, second.funder_type_id),
            COALESCE(first.code, second.code)
        INTO new_funder_type_id, new_funder_code
        FROM grants.funder first, grants.funder second
        WHERE first.id = NEW.id AND second.id = NEW.funder_to_merge_id;
    END IF;

    IF NEW.id <> NEW.funder_to_merge_id THEN
        UPDATE grants.application SET funder_id = NEW.id WHERE funder_id = NEW.funder_to_merge_id;
        UPDATE grants.award SET funder_id = NEW.id WHERE funder_id = NEW.funder_to_merge_id;
        DELETE FROM grants.funder WHERE id = NEW.funder_to_merge_id;
    END IF;
    UPDATE grants.funder SET funder = NEW.funder, code = new_funder_code, funder_type_id = new_funder_type_id WHERE id = NEW.id;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION hotwire3.grants_merge_funders_upd() OWNER TO dev;

CREATE TRIGGER grants_merge_funders_trig INSTEAD OF UPDATE ON  hotwire3."10_View/Grants/Merge_Funders" FOR EACH ROW EXECUTE FUNCTION hotwire3.grants_merge_funders_upd();
