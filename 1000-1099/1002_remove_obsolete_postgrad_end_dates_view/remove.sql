CREATE VIEW public._postgrad_end_dates_v5
 AS
 SELECT postgraduate_studentship.id,
    best_end_dates.end_date,
    postgraduate_studentship.funding_end_date AS intended_end_date,
    best_end_dates.intended_end_date AS estimated_leaving_date,
        CASE
            WHEN postgraduate_studentship.force_role_status_to_past = true THEN 'Past'::character varying(20)
            WHEN best_end_dates.end_date < now() THEN 'Past'::character varying(20)
            WHEN postgraduate_studentship.start_date < now() AND best_end_dates.end_date > now() THEN 'Current'::character varying(20)
            WHEN postgraduate_studentship.start_date < now() AND best_end_dates.end_date IS NULL THEN 'Current'::character varying(20)
            WHEN postgraduate_studentship.start_date > now() THEN 'Future'::character varying(20)
            ELSE 'Unknown'::character varying(20)
        END AS status_id,
    btrim(age(COALESCE(postgraduate_studentship.phd_date_submitted, 'now'::text::date)::timestamp with time zone, COALESCE(postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.start_date)::timestamp with time zone)::text, '@ '::text)::character varying(30) AS phd_duration,
    COALESCE(postgraduate_studentship.phd_date_submitted, 'now'::text::date) - COALESCE(postgraduate_studentship.date_registered_for_phd, postgraduate_studentship.start_date) AS phd_duration_days
   FROM postgraduate_studentship
     JOIN ( SELECT postgraduate_studentship_1.id,
                CASE
                    WHEN postgraduate_studentship_1.force_role_status_to_past = true THEN COALESCE(postgraduate_studentship_1.phd_date_awarded, postgraduate_studentship_1.phd_date_submitted, postgraduate_studentship_1.date_withdrawn_from_register, 'now'::text::date - 1)
                    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN COALESCE(postgraduate_studentship_1.phd_date_awarded, postgraduate_studentship_1.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(postgraduate_studentship_1.mphil_date_awarded, postgraduate_studentship_1.cpgs_or_mphil_date_awarded, postgraduate_studentship_1.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN COALESCE(postgraduate_studentship_1.cpgs_or_mphil_date_awarded, postgraduate_studentship_1.date_withdrawn_from_register)
                    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN COALESCE(postgraduate_studentship_1.msc_date_awarded, postgraduate_studentship_1.date_withdrawn_from_register)
                    ELSE NULL::date
                END AS end_date,
                CASE
                    WHEN postgraduate_studentship_type.name::text = 'PhD'::text THEN
                    CASE
                        WHEN postgraduate_studentship_1.end_of_registration_date IS NULL THEN COALESCE(postgraduate_studentship_1.intended_end_date, (postgraduate_studentship_1.start_date + '4 years'::interval + '3 mons'::interval)::date)
                        ELSE COALESCE(postgraduate_studentship_1.intended_end_date, (postgraduate_studentship_1.end_of_registration_date + '3 mons'::interval)::date)
                    END
                    WHEN postgraduate_studentship_type.name::text = 'MPhil'::text THEN COALESCE(postgraduate_studentship_1.intended_end_date, postgraduate_studentship_1.mphil_date_submission_due, postgraduate_studentship_1.cpgs_or_mphil_date_submission_due)
                    WHEN postgraduate_studentship_type.name::text = 'CPGS'::text THEN COALESCE(postgraduate_studentship_1.intended_end_date, postgraduate_studentship_1.cpgs_or_mphil_date_submission_due)
                    WHEN postgraduate_studentship_type.name::text = 'MSc'::text THEN COALESCE(postgraduate_studentship_1.intended_end_date, postgraduate_studentship_1.msc_date_submission_due)
                    ELSE NULL::date
                END AS intended_end_date
           FROM postgraduate_studentship postgraduate_studentship_1
             JOIN postgraduate_studentship_type ON postgraduate_studentship_type.id = postgraduate_studentship_1.postgraduate_studentship_type_id
             JOIN person person_1 ON person_1.id = postgraduate_studentship_1.person_id) best_end_dates USING (id)
     JOIN person ON person.id = postgraduate_studentship.person_id;

ALTER TABLE public._postgrad_end_dates_v5
    OWNER TO dev;

GRANT ALL ON TABLE public._postgrad_end_dates_v5 TO dev;
GRANT SELECT ON TABLE public._postgrad_end_dates_v5 TO mgmt_ro;
GRANT SELECT ON TABLE public._postgrad_end_dates_v5 TO student_management;


CREATE OR REPLACE VIEW public._chem_theor_mailinglist
 AS
 SELECT person.email_address,
    person.crsid,
    person_hid.person_hid
   FROM ( SELECT all_possible.id
           FROM ( SELECT mm_member_research_interest_group.member_id AS id
                   FROM person person_1
                     LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person_1.id
                     LEFT JOIN mm_member_research_interest_group ON mm_member_research_interest_group.member_id = person_1.id
                     LEFT JOIN research_interest_group ON mm_member_research_interest_group.research_interest_group_id = research_interest_group.id
                  WHERE _physical_status_v3.status_id::text = 'Current'::text AND research_interest_group.id = 1
                UNION
                 SELECT postgraduate_studentship.person_id AS id
                   FROM mm_member_research_interest_group
                     JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id
                     JOIN postgraduate_studentship ON postgraduate_studentship.first_supervisor_id = supervisor.id
                     JOIN _postgrad_end_dates_v5 ON _postgrad_end_dates_v5.id = postgraduate_studentship.id
                     JOIN _physical_status_v3 ON _physical_status_v3.person_id = postgraduate_studentship.person_id
                  WHERE mm_member_research_interest_group.research_interest_group_id = 1 AND mm_member_research_interest_group.is_primary = true AND _postgrad_end_dates_v5.status_id::text = 'Current'::text AND _physical_status_v3.status_id::text = 'Current'::text
                UNION
                 SELECT DISTINCT post_history.person_id
                   FROM mm_member_research_interest_group
                     JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id
                     JOIN post_history ON post_history.supervisor_id = supervisor.id
                     JOIN staff_category ON post_history.staff_category_id = staff_category.id
                     JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = post_history.person_id
                     JOIN _physical_status_v3 ON _physical_status_v3.person_id = post_history.person_id
                  WHERE mm_member_research_interest_group.research_interest_group_id = 1 AND mm_member_research_interest_group.is_primary = true AND _latest_role.status::text = 'Current'::text AND _physical_status_v3.status_id::text = 'Current'::text AND (staff_category.category::text = 'PDRA'::text OR staff_category.category::text = 'Senior PDRA'::text OR staff_category.category::text = 'Research Fellow'::text OR staff_category.category::text = 'Principal Research Associate'::text OR staff_category.category::text = 'Research Assistant'::text OR staff_category.category::text = 'Teaching Fellow'::text)
                UNION
                 SELECT DISTINCT visitorship.person_id
                   FROM mm_member_research_interest_group
                     JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id
                     JOIN visitorship ON supervisor.id = visitorship.host_person_id
                     JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = visitorship.person_id
                     JOIN _physical_status_v3 ON _physical_status_v3.person_id = visitorship.person_id
                  WHERE mm_member_research_interest_group.research_interest_group_id = 1 AND mm_member_research_interest_group.is_primary = true AND _latest_role.post_category_id ~~ 'v-%'::text AND _latest_role.status::text = 'Current'::text AND _physical_status_v3.status_id::text = 'Current'::text
                UNION
                 SELECT piii.person_id
                   FROM mm_member_research_interest_group
                     JOIN person supervisor ON mm_member_research_interest_group.member_id = supervisor.id
                     JOIN part_iii_studentship piii ON supervisor.id = piii.supervisor_id
                     JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = piii.person_id
                     JOIN _physical_status_v3 ON _physical_status_v3.person_id = piii.person_id
                  WHERE mm_member_research_interest_group.research_interest_group_id = 1 AND mm_member_research_interest_group.is_primary = true AND _latest_role.post_category_id = 'p3-1'::text AND _latest_role.status::text = 'Current'::text AND _physical_status_v3.status_id::text = 'Current'::text) all_possible
          WHERE NOT (all_possible.id IN ( SELECT person_1.id
                   FROM person person_1
                     JOIN mm_mailinglist_exclude_person mm ON person_1.id = mm.exclude_person_id
                  WHERE mm.mailinglist_id = 26))
        UNION
         SELECT mm.include_person_id
           FROM mm_mailinglist_include_person mm
          WHERE mm.mailinglist_id = 26) all_ids
     JOIN person ON all_ids.id = person.id
     JOIN person_hid ON person.id = person_hid.person_id
  ORDER BY person_hid.person_hid;



DROP VIEW hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE VIEW hotwire3."10_View/Roles/Postgrad_Students"
 AS
 WITH a AS (
         SELECT postgraduate_studentship.id,
            postgraduate_studentship.person_id,
            person.surname AS ro_surname,
            person.first_names AS ro_first_names,
            _postgrad_end_dates_v5.status_id AS ro_role_status_id,
            person.email_address AS ro_email_address,
            postgraduate_studentship.postgraduate_studentship_type_id,
            postgraduate_studentship.part_time,
            postgraduate_studentship.first_supervisor_id AS supervisor_id,
            postgraduate_studentship.second_supervisor_id AS co_supervisor_id,
            postgraduate_studentship.external_co_supervisor,
            postgraduate_studentship.substitute_supervisor_id,
            postgraduate_studentship.first_mentor_id AS mentor_id,
            postgraduate_studentship.second_mentor_id AS co_mentor_id,
            postgraduate_studentship.external_mentor,
            postgraduate_studentship.university,
            person.cambridge_college_id,
            postgraduate_studentship.paid_through_payroll,
            postgraduate_studentship.emplid_number,
            postgraduate_studentship.start_date,
            person.leaving_date,
            postgraduate_studentship.cpgs_or_mphil_date_submission_due,
            postgraduate_studentship.cpgs_or_mphil_date_awarded,
            postgraduate_studentship.mphil_date_submission_due,
            postgraduate_studentship.mphil_date_awarded,
            postgraduate_studentship.msc_date_submission_due,
            postgraduate_studentship.msc_date_awarded,
            postgraduate_studentship.date_registered_for_phd,
            postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
            postgraduate_studentship.phd_date_submitted,
            postgraduate_studentship.phd_date_awarded,
            postgraduate_studentship.date_withdrawn_from_register,
            postgraduate_studentship.date_reinstated_on_register,
            postgraduate_studentship.intended_end_date,
            postgraduate_studentship.filemaker_funding::text AS funding,
            postgraduate_studentship.filemaker_fees_funding::text AS fees_funding,
            postgraduate_studentship.funding_end_date,
            _postgrad_end_dates_v5.phd_duration AS ro_phd_duration,
            _postgrad_end_dates_v5.phd_duration_days AS ro_phd_duration_days,
            postgraduate_studentship.progress_notes::text AS progress_notes,
            postgraduate_studentship.force_role_status_to_past,
            ARRAY( SELECT mm2.nationality_id
                   FROM person p3
                     JOIN mm_person_nationality mm2 ON p3.id = mm2.person_id
                  WHERE p3.id = postgraduate_studentship.person_id) AS nationality_id,
                CASE
                    WHEN _postgrad_end_dates_v5.status_id::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass
           FROM postgraduate_studentship
             JOIN _postgrad_end_dates_v5 USING (id)
             JOIN person ON postgraduate_studentship.person_id = person.id
        )
 SELECT a.id,
    a.person_id,
    a.ro_surname,
    a.ro_first_names,
    a.ro_role_status_id,
    a.ro_email_address,
    a.postgraduate_studentship_type_id,
    a.part_time,
    a.supervisor_id,
    a.co_supervisor_id,
    a.external_co_supervisor,
    a.substitute_supervisor_id,
    a.mentor_id,
    a.co_mentor_id,
    a.external_mentor,
    a.university,
    a.cambridge_college_id,
    a.paid_through_payroll,
    a.emplid_number,
    a.nationality_id,
    a.start_date,
    a.leaving_date,
    a.cpgs_or_mphil_date_submission_due,
    a.cpgs_or_mphil_date_awarded,
    a.mphil_date_submission_due,
    a.mphil_date_awarded,
    a.msc_date_submission_due,
    a.msc_date_awarded,
    a.date_registered_for_phd,
    a.phd_four_year_submission_date,
    a.phd_date_submitted,
    a.phd_date_awarded,
    a.date_withdrawn_from_register,
    a.date_reinstated_on_register,
    a.intended_end_date,
    a.funding,
    a.fees_funding,
    a.funding_end_date,
    a.ro_phd_duration,
    a.ro_phd_duration_days,
    a.progress_notes,
    a.force_role_status_to_past,
    a._cssclass
   FROM a
  ORDER BY a.ro_surname, a.ro_first_names;

ALTER TABLE hotwire3."10_View/Roles/Postgrad_Students"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO mgmt_ro;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management;
GRANT SELECT ON TABLE hotwire3."10_View/Roles/Postgrad_Students" TO student_management_ro;


-- Rule: hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_del ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_del AS
    ON DELETE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
(DELETE FROM postgraduate_studentship
  WHERE (postgraduate_studentship.id = old.id));

-- Rule: hotwire3_view_postgrad_students_ins ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_ins ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_ins AS
    ON INSERT TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
( SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
 UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date
  WHERE (person.id = new.person_id);
 INSERT INTO postgraduate_studentship (id, person_id, postgraduate_studentship_type_id, part_time, first_supervisor_id, second_supervisor_id, external_co_supervisor, substitute_supervisor_id, first_mentor_id, second_mentor_id, external_mentor, university, paid_through_payroll, emplid_number, start_date, cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_awarded, mphil_date_submission_due, mphil_date_awarded, msc_date_submission_due, msc_date_awarded, date_registered_for_phd, end_of_registration_date, phd_date_submitted, phd_date_awarded, date_withdrawn_from_register, date_reinstated_on_register, intended_end_date, filemaker_funding, filemaker_fees_funding, funding_end_date, progress_notes, force_role_status_to_past)
  VALUES (nextval('postgraduate_studentship_id_seq'::regclass), new.person_id, new.postgraduate_studentship_type_id, new.part_time, new.supervisor_id, new.co_supervisor_id, new.external_co_supervisor, new.substitute_supervisor_id, new.mentor_id, new.co_mentor_id, new.external_mentor, new.university, new.paid_through_payroll, new.emplid_number, new.start_date, new.cpgs_or_mphil_date_submission_due, new.cpgs_or_mphil_date_awarded, new.mphil_date_submission_due, new.mphil_date_awarded, new.msc_date_submission_due, new.msc_date_awarded, new.date_registered_for_phd, new.phd_four_year_submission_date, new.phd_date_submitted, new.phd_date_awarded, new.date_withdrawn_from_register, new.date_reinstated_on_register, new.intended_end_date, (new.funding)::character varying(500), (new.fees_funding)::character varying(80), new.funding_end_date, (new.progress_notes)::character varying, new.force_role_status_to_past)
  RETURNING currval('postgraduate_studentship_id_seq'::regclass) AS currval,
    postgraduate_studentship.person_id,
    NULL::character varying(32) AS "varchar",
    NULL::character varying(32) AS "varchar",
    NULL::character varying AS text,
    NULL::character varying(48) AS "varchar",
    postgraduate_studentship.postgraduate_studentship_type_id,
    postgraduate_studentship.part_time,
    postgraduate_studentship.first_supervisor_id,
    postgraduate_studentship.second_supervisor_id,
    postgraduate_studentship.external_co_supervisor,
    postgraduate_studentship.substitute_supervisor_id,
    postgraduate_studentship.first_mentor_id,
    postgraduate_studentship.second_mentor_id,
    postgraduate_studentship.external_mentor,
    postgraduate_studentship.university,
    NULL::bigint AS int8,
    postgraduate_studentship.paid_through_payroll,
    postgraduate_studentship.emplid_number,
    NULL::bigint[] AS int8,
    postgraduate_studentship.start_date,
    NULL::date AS date,
    postgraduate_studentship.cpgs_or_mphil_date_submission_due,
    postgraduate_studentship.cpgs_or_mphil_date_awarded,
    postgraduate_studentship.mphil_date_submission_due,
    postgraduate_studentship.mphil_date_awarded,
    postgraduate_studentship.msc_date_submission_due,
    postgraduate_studentship.msc_date_awarded,
    postgraduate_studentship.date_registered_for_phd,
    postgraduate_studentship.end_of_registration_date AS phd_four_year_submission_date,
    postgraduate_studentship.phd_date_submitted,
    postgraduate_studentship.phd_date_awarded,
    postgraduate_studentship.date_withdrawn_from_register,
    postgraduate_studentship.date_reinstated_on_register,
    postgraduate_studentship.intended_end_date,
    (postgraduate_studentship.filemaker_funding)::text AS filemaker_funding,
    (postgraduate_studentship.filemaker_fees_funding)::text AS filemaker_fees_funding,
    postgraduate_studentship.funding_end_date,
    (btrim((age((postgraduate_studentship.phd_date_submitted)::timestamp with time zone, (postgraduate_studentship.date_registered_for_phd)::timestamp with time zone))::text, '@ '::text))::character varying(30) AS btrim,
    NULL::integer AS int4,
    (postgraduate_studentship.progress_notes)::text AS progress_notes,
    postgraduate_studentship.force_role_status_to_past,
    NULL::text AS text;
);

-- Rule: hotwire3_view_postgrad_students_upd ON hotwire3."10_View/Roles/Postgrad_Students"

-- DROP Rule hotwire3_view_postgrad_students_upd ON hotwire3."10_View/Roles/Postgrad_Students";

CREATE OR REPLACE RULE hotwire3_view_postgrad_students_upd AS
    ON UPDATE TO hotwire3."10_View/Roles/Postgrad_Students"
    DO INSTEAD
( UPDATE postgraduate_studentship SET postgraduate_studentship_type_id = new.postgraduate_studentship_type_id, part_time = new.part_time, first_supervisor_id = new.supervisor_id, second_supervisor_id = new.co_supervisor_id, external_co_supervisor = new.external_co_supervisor, substitute_supervisor_id = new.substitute_supervisor_id, first_mentor_id = new.mentor_id, second_mentor_id = new.co_mentor_id, external_mentor = new.external_mentor, university = new.university, paid_through_payroll = new.paid_through_payroll, emplid_number = new.emplid_number, start_date = new.start_date, cpgs_or_mphil_date_submission_due = new.cpgs_or_mphil_date_submission_due, cpgs_or_mphil_date_awarded = new.cpgs_or_mphil_date_awarded, mphil_date_submission_due = new.mphil_date_submission_due, mphil_date_awarded = new.mphil_date_awarded, msc_date_submission_due = new.msc_date_submission_due, msc_date_awarded = new.msc_date_awarded, date_registered_for_phd = new.date_registered_for_phd, end_of_registration_date = new.phd_four_year_submission_date, phd_date_submitted = new.phd_date_submitted, phd_date_awarded = new.phd_date_awarded, date_withdrawn_from_register = new.date_withdrawn_from_register, date_reinstated_on_register = new.date_reinstated_on_register, intended_end_date = new.intended_end_date, filemaker_funding = (new.funding)::character varying(500), filemaker_fees_funding = (new.fees_funding)::character varying(80), funding_end_date = new.funding_end_date, progress_notes = (new.progress_notes)::character varying, force_role_status_to_past = new.force_role_status_to_past
  WHERE (postgraduate_studentship.id = old.id);
 UPDATE person SET cambridge_college_id = new.cambridge_college_id, leaving_date = new.leaving_date
  WHERE (person.id = new.person_id);
 SELECT fn_mm_array_update(new.nationality_id, 'mm_person_nationality'::character varying, 'person_id'::character varying, 'nationality_id'::character varying, new.person_id) AS fn_mm_array_update;
);

