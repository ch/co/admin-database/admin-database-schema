CREATE SEQUENCE public.fire_warden_fire_warden_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.fire_warden_fire_warden_id_seq
    OWNER TO dev;

GRANT ALL ON SEQUENCE public.fire_warden_fire_warden_id_seq TO dev;

GRANT USAGE ON SEQUENCE public.fire_warden_fire_warden_id_seq TO safety_management;

CREATE TABLE public.fire_warden
(
    fire_warden_id bigint NOT NULL DEFAULT nextval('fire_warden_fire_warden_id_seq'::regclass),
    person_id bigint NOT NULL,
    qualification_date date,
    fire_warden_area_id bigint,
    is_primary boolean NOT NULL DEFAULT true,
    CONSTRAINT fire_warden_pkey PRIMARY KEY (fire_warden_id),
    CONSTRAINT fire_warden_person_id_key UNIQUE (person_id),
    CONSTRAINT fire_warden_fk_person FOREIGN KEY (person_id)
        REFERENCES public.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.fire_warden
    OWNER to dev;

REVOKE ALL ON TABLE public.fire_warden FROM safety_management;

GRANT ALL ON TABLE public.fire_warden TO dev;

GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE public.fire_warden TO safety_management;

COMMENT ON TABLE public.fire_warden
    IS 'ticket 148999';

CREATE UNIQUE INDEX idx_primary_fire_warden
    ON public.fire_warden USING btree
    (fire_warden_area_id ASC NULLS LAST)
    TABLESPACE pg_default
    WHERE is_primary = true;


CREATE VIEW hotwire3.fire_warden_hid
 AS
 SELECT fire_warden.fire_warden_id,
    person_hid.person_hid AS fire_warden_hid
   FROM fire_warden
     JOIN hotwire3.person_hid USING (person_id);

ALTER TABLE hotwire3.fire_warden_hid
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3.fire_warden_hid TO dev;
GRANT SELECT ON TABLE hotwire3.fire_warden_hid TO PUBLIC;


