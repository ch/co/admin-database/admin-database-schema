CREATE TABLE public._hr_role (
    person_id BIGINT NOT NULL CONSTRAINT hr_role_fk_person REFERENCES person(id),
    role_id BIGINT NOT NULL,
    role_tablename TEXT NOT NULL,
    start_date DATE,
    intended_end_date DATE,
    estimated_role_end_date DATE,
    end_date DATE,
    funding_end_date DATE,
    supervisor_id BIGINT CONSTRAINT hr_role_fk_supervisor REFERENCES person(id),
    post_category_id TEXT NOT NULL,
    post_category VARCHAR(80) NOT NULL,
    predicted_role_status_id VARCHAR(10) NOT NULL CONSTRAINT hr_role_fk_status REFERENCES status(status_id), -- needs to be kept up to date as time passes
    funding VARCHAR(500),
    fees_funding VARCHAR(500),
    research_grant_number VARCHAR(500),
    paid_by_university BOOLEAN,
    job_title VARCHAR(120),
    left_but_no_leaving_date_given BOOLEAN,
    CONSTRAINT hr_role_pk PRIMARY KEY (role_id, role_tablename)
    )
;

ALTER TABLE public._hr_role OWNER TO dev;
GRANT SELECT,UPDATE,INSERT,DELETE ON public._hr_role TO hr,student_management,gdpr_purge;
CREATE INDEX hr_role_person_id_idx ON public._hr_role (person_id);
CREATE INDEX hr_role_status_idx ON public._hr_role (predicted_role_status_id);

INSERT INTO public._hr_role (
    person_id,
    role_id,
    role_tablename,
    start_date,
    intended_end_date,
    estimated_role_end_date,
    end_date,
    funding_end_date,
    supervisor_id,
    post_category_id,
    post_category,
    predicted_role_status_id,
    funding,
    fees_funding,
    research_grant_number,
    paid_by_university,
    job_title,
    left_but_no_leaving_date_given
    )
    SELECT
        visitorship.person_id,
        visitorship.id AS role_id,
        'visitorship'::text AS role_tablename,
        visitorship.start_date,
        visitorship.intended_end_date,
        COALESCE(visitorship.end_date,visitorship.intended_end_date) AS estimated_role_end_date,
        visitorship.end_date,
        NULL::date as funding_end_date,
        visitorship.host_person_id AS supervisor_id,
        'v-'::text || visitor_type_id::text AS post_category_id,
        visitor_type_hid.visitor_type_hid AS post_category,
        public.role_predicted_status(visitorship.start_date,visitorship.intended_end_date,visitorship.end_date,NULL::DATE,visitorship.force_role_status_to_past,'visitorship') AS predicted_role_status_id,
        NULL::varchar(500) AS funding,
        NULL::varchar(500) AS fees_funding,
        NULL::varchar(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        NULL::varchar(120) AS job_title,
        visitorship.force_role_status_to_past
    FROM public.visitorship
    LEFT JOIN public.visitor_type_hid USING (visitor_type_id)
    UNION
    SELECT
        part_iii_studentship.person_id,
        part_iii_studentship.id AS role_id,
        'part_iii_studentship'::text AS role_tablename,
        part_iii_studentship.start_date,
        part_iii_studentship.intended_end_date,
        COALESCE(part_iii_studentship.end_date,part_iii_studentship.intended_end_date) AS estimated_role_end_date,
        part_iii_studentship.end_date,
        NULL::date AS funding_end_date,
        part_iii_studentship.supervisor_id,
        'p3-1'::text AS post_category_id,
        'Part III'::varchar(80) AS post_category_id,
        public.role_predicted_status(part_iii_studentship.start_date,part_iii_studentship.intended_end_date,part_iii_studentship.end_date,NULL::DATE,part_iii_studentship.force_role_status_to_past,'part_iii_studentship') AS predicted_role_status_id,
        NULL::varchar(500) AS funding,
        NULL::varchar(500) AS fees_funding,
        NULL::varchar(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        NULL::varchar(120) AS job_title,
        part_iii_studentship.force_role_status_to_past AS left_but_no_leaving_date_given
    FROM public.part_iii_studentship
    UNION
    SELECT
        erasmus_socrates_studentship.person_id,
        erasmus_socrates_studentship.id AS role_id,
        'erasmus_socrates_studentship'::text AS role_tablename,
        erasmus_socrates_studentship.start_date,
        erasmus_socrates_studentship.intended_end_date,
        COALESCE(erasmus_socrates_studentship.end_date,erasmus_socrates_studentship.intended_end_date) AS estimated_role_end_date,
        erasmus_socrates_studentship.end_date,
        NULL::date as funding_end_date,
        erasmus_socrates_studentship.supervisor_id,
        'e-'::text || erasmus_type_id::text AS post_category_id,
        erasmus_type_hid.erasmus_type_hid AS post_category,
        public.role_predicted_status(erasmus_socrates_studentship.start_date,erasmus_socrates_studentship.intended_end_date,erasmus_socrates_studentship.end_date,NULL::DATE,erasmus_socrates_studentship.force_role_status_to_past,'erasmus_socrates_studentship') AS predicted_role_status_id,
        NULL::varchar(500) AS funding,
        NULL::varchar(500) AS fees_funding,
        NULL::varchar(500) AS research_grant_number,
        NULL::boolean AS paid_by_university,
        NULL::varchar(120) AS job_title,
        erasmus_socrates_studentship.force_role_status_to_past
    FROM public.erasmus_socrates_studentship
    LEFT JOIN public.erasmus_type_hid USING (erasmus_type_id)
    UNION
    SELECT
        post_history.person_id,
        post_history.id AS role_id,
        'post_history'::text AS role_tablename,
        post_history.start_date,
        post_history.intended_end_date,
        COALESCE(post_history.end_date,LEAST(post_history.intended_end_date,post_history.funding_end_date)) AS estimated_role_end_date,
        post_history.end_date,
        post_history.funding_end_date,
        post_history.supervisor_id,
        'sc-'::text || post_history.staff_category_id::text AS post_category_id,
        staff_category.category AS post_category,
        public.role_predicted_status(post_history.start_date,post_history.intended_end_date,post_history.end_date,post_history.funding_end_date,post_history.force_role_status_to_past,'post_history') AS predicted_role_status_id,
        post_history.filemaker_funding AS funding,
        NULL::varchar(500) AS fees_funding,
        post_history.research_grant_number,
        post_history.paid_by_university,
        post_history.job_title,
        post_history.force_role_status_to_past
    FROM public.post_history
    LEFT JOIN public.staff_category ON post_history.staff_category_id = staff_category.id
    UNION
    SELECT
        postgraduate_studentship.person_id,
        postgraduate_studentship.id AS role_id,
        'postgraduate_studentship'::text AS role_tablename,
        postgraduate_studentship.start_date,
        postgraduate_studentship.intended_end_date,
        COALESCE(postgraduate_studentship.end_date,postgraduate_studentship.intended_end_date) AS estimated_role_end_date,
        postgraduate_studentship.end_date,
        postgraduate_studentship.funding_end_date,
        postgraduate_studentship.first_supervisor_id AS supervisor_id,
        'pg-'::text || postgraduate_studentship.postgraduate_studentship_type_id::text AS post_category_id,
        postgraduate_studentship_type.name AS post_category,
        public.role_predicted_status(postgraduate_studentship.start_date,postgraduate_studentship.intended_end_date,postgraduate_studentship.end_date,postgraduate_studentship.funding_end_date,postgraduate_studentship.force_role_status_to_past,'postgraduate_studentship') AS predicted_role_status_id,
        postgraduate_studentship.filemaker_funding AS funding,
        postgraduate_studentship.filemaker_fees_funding AS fees_funding,
        NULL::VARCHAR(500) AS research_grant_number,
        postgraduate_studentship.paid_through_payroll AS paid_by_university,
        NULL::VARCHAR(120) AS job_title,
        postgraduate_studentship.force_role_status_to_past
    FROM public.postgraduate_studentship
    LEFT JOIN public.postgraduate_studentship_type ON postgraduate_studentship.postgraduate_studentship_type_id = postgraduate_studentship_type.id
;

-- to allow the trigger to be run by people who might edit role tables
-- staff_category and postgraduate_studentship_type already have the right permissions
GRANT SELECT ON public.erasmus_type_hid, public.visitor_type_hid TO hr,gdpr_purge;

CREATE FUNCTION public.update_hr_role() RETURNS TRIGGER AS
$BODY$
DECLARE
    my_estimated_role_end_date DATE;
    my_post_category_id TEXT;
    my_post_category VARCHAR(80);
    my_status_id VARCHAR(10);
    my_supervisor_id BIGINT;
    my_json_new JSONB;

BEGIN

    -- if it's a delete, just delete it
    IF TG_OP = 'DELETE' THEN
        DELETE FROM public._hr_role WHERE person_id = OLD.person_id AND role_id = OLD.id and role_tablename = TG_TABLE_NAME;
        RETURN NULL;
    END IF;

    -- get data set up
    -- convert row to json so we can check for columns that
    -- don't exist in all rows
    my_json_new = row_to_json(NEW);

    -- status
    SELECT public.role_predicted_status(NEW.start_date,NEW.end_date,NEW.intended_end_date,(my_json_new->>'funding_end_date')::DATE,NEW.force_role_status_to_past,TG_TABLE_NAME) INTO my_status_id;

    -- estimated_role_end_date is calculated like this for most role types, and overridden below as necessary
    SELECT COALESCE(NEW.end_date,NEW.intended_end_date) into my_estimated_role_end_date;

    -- post category, supervisor, and sometimes estimated_role_end_date depend on role type
    CASE TG_TABLE_NAME
        WHEN 'part_iii_studentship' THEN
            my_post_category := 'Part III'::varchar(80);
            my_post_category_id := 'p3-1'::text;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'erasmus_socrates_studentship' THEN
            SELECT erasmus_type_hid INTO my_post_category FROM erasmus_type_hid WHERE erasmus_type_hid.erasmus_type_id = NEW.erasmus_type_id;
            SELECT 'e-' || NEW.erasmus_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'postgraduate_studentship' THEN
            SELECT name INTO my_post_category FROM postgraduate_studentship_type WHERE postgraduate_studentship_type.id = NEW.postgraduate_studentship_type_id;
            SELECT 'pg-' || NEW.postgraduate_studentship_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.first_supervisor_id;
        WHEN 'post_history' THEN
            SELECT category INTO my_post_category FROM staff_category WHERE id = NEW.staff_category_id;
            SELECT 'sc-' || NEW.staff_category_id::text INTO my_post_category_id;
            -- update the estimated role end date as the definition is different for staff
            SELECT COALESCE(NEW.end_date,LEAST(NEW.intended_end_date,NEW.funding_end_date)) into my_estimated_role_end_date;
            my_supervisor_id := NEW.supervisor_id;
        WHEN 'visitorship' THEN
            SELECT visitor_type_hid INTO my_post_category FROM visitor_type_hid WHERE visitor_type_hid.visitor_type_id = NEW.visitor_type_id;
            SELECT 'v-' || NEW.visitor_type_id::text INTO my_post_category_id;
            my_supervisor_id := NEW.host_person_id;
    END CASE;

    -- if it's an insert, insert, or update, update
    IF TG_OP = 'INSERT' THEN
        INSERT INTO public._hr_role (
            person_id,
            role_id,
            role_tablename,
            start_date,
            intended_end_date,
            estimated_role_end_date,
            end_date,
            funding_end_date,
            supervisor_id,
            post_category_id,
            post_category,
            predicted_role_status_id,
            funding,
            fees_funding,
            research_grant_number,
            paid_by_university,
            job_title,
            left_but_no_leaving_date_given
        ) VALUES (
            NEW.person_id,
            NEW.id,
            TG_TABLE_NAME,
            NEW.start_date,
            NEW.intended_end_date,
            my_estimated_role_end_date,
            NEW.end_date,
            (my_json_new->>'funding_end_date')::DATE,
            my_supervisor_id,
            my_post_category_id,
            my_post_category,
            my_status_id,
            (my_json_new->>'filemaker_funding')::VARCHAR(500),
            (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            (my_json_new->>'research_grant_number')::VARCHAR(500),
            (my_json_new->>'paid_by_university')::BOOLEAN,
            (my_json_new->>'job_title')::VARCHAR(120),
            NEW.force_role_status_to_past
        );

    ELSIF TG_OP = 'UPDATE' THEN
        UPDATE _hr_role SET
            person_id = NEW.person_id,
            start_date = NEW.start_date,
            intended_end_date = NEW.intended_end_date,
            estimated_role_end_date = my_estimated_role_end_date,
            end_date = NEW.end_date,
            funding_end_date = (my_json_new->>'funding_end_date')::DATE,
            supervisor_id = my_supervisor_id,
            post_category_id = my_post_category_id,
            post_category = my_post_category,
            predicted_role_status_id = my_status_id,
            funding = (my_json_new->>'filemaker_funding')::VARCHAR(500),
            fees_funding = (my_json_new->>'filemaker_fees_funding')::VARCHAR(500),
            research_grant_number = (my_json_new->>'research_grant_number')::VARCHAR(500),
            paid_by_university = (my_json_new->>'paid_by_university')::BOOLEAN,
            job_title = (my_json_new->>'job_title')::VARCHAR(120),
            left_but_no_leaving_date_given = NEW.force_role_status_to_past
        WHERE role_id = NEW.id AND role_tablename = TG_TABLE_NAME;
    END IF;

    RETURN NEW;
END;

$BODY$ LANGUAGE PLPGSQL;

ALTER FUNCTION public.update_hr_role() OWNER TO dev;

CREATE FUNCTION public.cron_update_hr_role() RETURNS VOID AS
$BODY$
UPDATE public._hr_role SET predicted_role_status_id = public.role_predicted_status(start_date,end_date,intended_end_date,funding_end_date,left_but_no_leaving_date_given,role_tablename);
$BODY$ LANGUAGE SQL;

ALTER FUNCTION public.cron_update_hr_role() OWNER TO dev;

CREATE TRIGGER hr_roles AFTER UPDATE OR INSERT OR DELETE ON part_iii_studentship FOR EACH ROW EXECUTE FUNCTION public.update_hr_role();
CREATE TRIGGER hr_roles AFTER UPDATE OR INSERT OR DELETE ON erasmus_socrates_studentship FOR EACH ROW EXECUTE FUNCTION public.update_hr_role();
CREATE TRIGGER hr_roles AFTER UPDATE OR INSERT OR DELETE ON postgraduate_studentship FOR EACH ROW EXECUTE FUNCTION public.update_hr_role();
CREATE TRIGGER hr_roles AFTER UPDATE OR INSERT OR DELETE ON post_history FOR EACH ROW EXECUTE FUNCTION public.update_hr_role();
CREATE TRIGGER hr_roles AFTER UPDATE OR INSERT OR DELETE ON visitorship FOR EACH ROW EXECUTE FUNCTION public.update_hr_role();
