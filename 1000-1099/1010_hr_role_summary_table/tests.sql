BEGIN;

SELECT plan(47);

-- table exists
SELECT has_table('public','_hr_role','Has public._hr_role table');

-- make lists of role tables to check
CREATE TEMPORARY TABLE test_roles ( tabname text NOT NULL );
INSERT INTO test_roles ( tabname ) 
VALUES 
        ('visitorship'),
        ('part_iii_studentship'),
        ('post_history'),
        ('postgraduate_studentship'),
        ('erasmus_socrates_studentship')
;

-- trigger func exists
SELECT has_function('public','update_hr_role','Has trigger function update_hr_role');

-- cron function exists
SELECT has_function('public','cron_update_hr_role','Has function to update hr_role from cron');

-- role tables have trigger
SELECT has_trigger('public', tabname, 'hr_roles', format('hr_roles trigger exists on %I.%I','public', tabname))
FROM test_roles;

-- the right number of roles have been set up
SELECT results_eq(format('SELECT COUNT(*) FROM public._hr_role WHERE role_tablename = %L',tabname),
                  format('SELECT COUNT(*) FROM %I',tabname),
                  format('Rowcount for role %I in _hr_role matches rowcount in %I',tabname,tabname))
FROM test_roles;

-- check the status has updated (as time passes, status must be kept up to date)
SELECT results_eq(format('SELECT predicted_role_status_id from public._hr_role WHERE role_tablename = %L ORDER BY role_id',tabname),
                  format('SELECT public.role_predicted_status(start_date,end_date,intended_end_date,null::date,force_role_status_to_past,%L) FROM %I ORDER BY id',tabname,tabname),
                  format('Status in _hr_role up to date for %s table roles',tabname))
FROM test_roles where tabname <> 'post_history';

SELECT results_eq('SELECT predicted_role_status_id from public._hr_role WHERE role_tablename = $$post_history$$ ORDER BY role_id',
                  'SELECT public.role_predicted_status(start_date,end_date,intended_end_date,funding_end_date,force_role_status_to_past,$$post_history$$) FROM public.post_history ORDER BY id',
                  'Status in _hr_role up to date for post_history table roles');

-- check basic dates match
SELECT results_eq(format('SELECT %I FROM _hr_role WHERE role_tablename = %L ORDER BY role_id',date_type,tabname),
                  format('SELECT %I FROM %I ORDER BY id',date_type,tabname),
                  format('%s matches in _hr_role %s and %s',date_type,tabname,tabname))
FROM test_roles, (values ('start_date'),('end_date'),('intended_end_date')) as v (date_type);

-- check funding end dates match for roles with those
SELECT results_eq(format('SELECT %I FROM _hr_role WHERE role_tablename = %L ORDER BY role_id',date_type,tabname),
                  format('SELECT %I FROM %I ORDER BY id',date_type,tabname),
                  format('%s matches in _hr_role %s and %s',date_type,tabname,tabname))
FROM (values ('post_history'),('postgraduate_studentship')) as t (tabname), (values ('funding_end_date')) as v (date_type);

-- check supervisors match
SELECT results_eq(format('SELECT supervisor_id FROM _hr_role WHERE role_tablename = %L ORDER BY role_id',tabname),
                  format('SELECT %I FROM %I ORDER BY id',supervisor_col,tabname),
                  format('supervisors matches in _hr_role %s and %s',tabname,tabname))
FROM (values ('post_history', 'supervisor_id'),
             ('postgraduate_studentship','first_supervisor_id'),
             ('visitorship','host_person_id'),
             ('part_iii_studentship','supervisor_id'),
             ('erasmus_socrates_studentship','supervisor_id')) as t ( tabname, supervisor_col);

-- check post_category_id matches
SELECT results_eq(format('SELECT post_category_id FROM _hr_role WHERE role_tablename = %L ORDER BY role_id',tabname),
                  format('SELECT %L || $$-$$ || %I FROM %I ORDER BY id',prefix,catgeory_col,tabname),
                  format('post_category_id matches in _hr_role %s and %s',tabname,tabname))
FROM (values ('post_history', 'sc', 'staff_category_id'),
             ('postgraduate_studentship','pg','postgraduate_studentship_type_id'),
             ('visitorship','v','visitor_type_id'),
             ('erasmus_socrates_studentship','e','erasmus_type_id')) as t ( tabname, prefix, catgeory_col);

SELECT results_eq(format('SELECT post_category_id FROM _hr_role WHERE role_tablename = %L ORDER BY role_id','part_iii_studentship'),
                  'select $$p3-1$$ from part_iii_studentship order by id',
                  'post_category_id matches in _hr_role part iii studentship');

-- check post category
SELECT results_eq(format('SELECT post_category FROM _hr_role WHERE role_tablename = %L ORDER BY role_id','part_iii_studentship'),
                  'select $$Part III$$::varchar(80) from part_iii_studentship order by id',
                  'post category matches for Part III');

SELECT results_eq(format('SELECT post_category FROM _hr_role WHERE role_tablename = %L ORDER BY role_id','post_history'),
                  format('select staff_category.category from post_history left join staff_category on post_history.staff_category_id = staff_category.id order by post_history.id'),
                  'post category matches for post history');


SELECT * FROM finish();

ROLLBACK;
