DROP TRIGGER hr_roles ON public.part_iii_studentship;
DROP TRIGGER hr_roles ON public.erasmus_socrates_studentship;
DROP TRIGGER hr_roles ON public.postgraduate_studentship;
DROP TRIGGER hr_roles ON public.post_history;
DROP TRIGGER hr_roles ON public.visitorship;

DROP FUNCTION public.cron_update_hr_role();

DROP TABLE public._hr_role;
DROP FUNCTION public.update_hr_role();

REVOKE SELECT ON public.erasmus_type_hid, public.visitor_type_hid FROM hr,gdpr_purge;
