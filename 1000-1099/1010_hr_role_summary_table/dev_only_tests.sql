-- Do not run this on the live database as id sequences will be consumed
BEGIN;

\set p_crsid '\'fjc55\''
\set s_crsid '\'spqr1\''
\set s2_crsid '\'spqr2\''

SELECT plan(16);

PREPARE add_person(text, text, text) AS INSERT INTO person (first_names, surname, crsid, arrival_date ) VALUES ( $1, $2, $3, current_date - interval ' 1 day');

EXECUTE add_person('FJ','C',:p_crsid);
EXECUTE add_person('SPQ','R',:s_crsid);
EXECUTE add_person('SPQ','R',:s2_crsid);

PREPARE new_hr_role AS SELECT _hr_role.* FROM _hr_role JOIN person ON _hr_role.person_id = person.id WHERE person.crsid = :p_crsid;

------------
-- visitors
------------

-- add a new one
INSERT INTO visitorship
    ( person_id, host_person_id, visitor_type_id, start_date, intended_end_date )
    VALUES
    (
        (SELECT id FROM person WHERE crsid = :p_crsid ),
        (SELECT id FROM person WHERE crsid = :s_crsid ),
        (SELECT visitor_type_id FROM visitor_type_hid WHERE visitor_type_hid = 'Visitor (Academic)'),
        current_date - interval '1 day',
        current_date + interval '1 year'
    );

PREPARE new_visitor AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM visitorship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'visitorship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date + interval '1 year')::date,
    null::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s_crsid),
    'v-4',
    'Visitor (Academic)'::varchar(80),
    'Current'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);


SELECT results_eq('execute new_hr_role',
                 'execute new_visitor',
                 'Inserted new visitor into _hr_role ok');

-- update supervisor and end date and type
UPDATE visitorship SET end_date = (current_date - interval '1 month')::date, host_person_id = (SELECT id FROM person WHERE crsid = :s2_crsid), visitor_type_id = (SELECT visitor_type_id FROM visitor_type_hid WHERE visitor_type_hid = 'Visitor (PDRA)') WHERE person_id = (SELECT id from person where crsid = :p_crsid);
PREPARE updated_visitor AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM visitorship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'visitorship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date - interval '1 month')::date,
    (current_date - interval '1 month')::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'v-12',
    'Visitor (PDRA)'::varchar(80),
    'Past'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_visitor',
                 'Updated new visitor in _hr_role ok');

-- update some status related things
UPDATE visitorship SET end_date = null::date, force_role_status_to_past = 't'::boolean WHERE person_id = (SELECT id from person where crsid = :p_crsid);
PREPARE updated_visitor2 AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM visitorship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'visitorship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date + interval '1 year')::date,
    null::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'v-12',
    'Visitor (PDRA)'::varchar(80),
    'Past'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    't'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_visitor2',
                 'Updated new visitor in _hr_role ok');

-- delete the role
DELETE FROM visitorship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid);
SELECT is_empty('execute new_hr_role','Deleted visitor successfully');

---------------
-- post_history
---------------

-- add a new one
INSERT INTO post_history
    ( person_id, supervisor_id, start_date, intended_end_date, staff_category_id, funding_end_date, filemaker_funding, job_title, paid_by_university, research_grant_number )
    VALUES
    (
        (SELECT id FROM person WHERE crsid = :p_crsid ),
        (SELECT id FROM person WHERE crsid = :s_crsid ),
        current_date - interval '1 day',
        current_date + interval '1 year' + interval '3 months',
        (SELECT id FROM staff_category WHERE category = 'PDRA'),
        current_date + interval '1 year',
        'PQ',
        'Researcher in Recent Runes',
        't'::boolean,
        'RG12345'
    );

PREPARE new_post AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM post_history WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'post_history',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year' + interval '3 months')::date,
    (current_date + interval '1 year')::date, -- expect the funding end date not the intended end date here
    null::date,
    (current_date + interval '1 year')::date,
    (SELECT id FROM person WHERE crsid = :s_crsid),
    'sc-4',
    'PDRA'::varchar(80),
    'Current'::varchar(10),
    'PQ'::varchar(500),
    null::varchar(500),
    'RG12345'::varchar(500),
    't'::boolean,
    'Researcher in Recent Runes'::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute new_post',
                 'Inserted new staff member into _hr_role ok');

-- update supervisor and end date etc
UPDATE post_history SET start_date = (current_date - interval '1 month')::date, end_date = (current_date - interval '1 month')::date, supervisor_id = (SELECT id FROM person WHERE crsid = :s2_crsid), filemaker_funding = 'AZ', research_grant_number = 'RG67890', job_title = 'Bottle washer', paid_by_university = 'f' WHERE person_id = (SELECT id from person where crsid = :p_crsid);

PREPARE updated_post AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM post_history WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'post_history',
    (current_date - interval '1 month')::date,
    (current_date + interval '1 year' + interval '3 months')::date,
    (current_date - interval '1 month')::date, -- now expect the end date
    (current_date - interval '1 month')::date,
    (current_date + interval '1 year')::date,
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'sc-4',
    'PDRA'::varchar(80),
    'Past'::varchar(10),
    'AZ'::varchar(500),
    null::varchar(500),
    'RG67890'::varchar(500),
    'f'::boolean,
    'Bottle washer'::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_post',
                 'Updated new post in _hr_role ok');

-- delete the role
DELETE FROM post_history WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid);
SELECT is_empty('execute new_hr_role','Deleted post successfully');


-------------------
-- part III student
-------------------

-- add a new one
INSERT INTO part_iii_studentship
    ( person_id, supervisor_id, start_date, intended_end_date )
    VALUES
    (
        (SELECT id FROM person WHERE crsid = :p_crsid ),
        (SELECT id FROM person WHERE crsid = :s_crsid ),
        current_date - interval '1 day',
        current_date + interval '1 year'
    );

PREPARE new_p3 AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM part_iii_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'part_iii_studentship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date + interval '1 year')::date,
    null::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s_crsid),
    'p3-1',
    'Part III'::varchar(80),
    'Current'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute new_p3',
                 'Inserted new part III into _hr_role ok');

-- update supervisor and dates
UPDATE part_iii_studentship SET end_date = (current_date - interval '1 month')::date, supervisor_id = (SELECT id FROM person WHERE crsid = :s2_crsid) WHERE person_id = (SELECT id from person where crsid = :p_crsid);
PREPARE updated_p3 AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM part_iii_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'part_iii_studentship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date - interval '1 month')::date,
    (current_date - interval '1 month')::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'p3-1',
    'Part III'::varchar(80),
    'Past'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_p3',
                 'Updated new Part III in _hr_role ok');

-- delete the role
DELETE FROM part_iii_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid);
SELECT is_empty('execute new_hr_role','Deleted Part III successfully');

-------------------
-- Erasmus
-------------------

-- add a new one
INSERT INTO erasmus_socrates_studentship
    ( person_id, supervisor_id, start_date, intended_end_date, erasmus_type_id )
    VALUES
    (
        (SELECT id FROM person WHERE crsid = :p_crsid ),
        (SELECT id FROM person WHERE crsid = :s_crsid ),
        current_date - interval '1 day',
        current_date + interval '1 year',
        (SELECT erasmus_type_id FROM erasmus_type_hid WHERE erasmus_type_hid = 'Erasmus exchange student')
    );

PREPARE new_erasmus AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM erasmus_socrates_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'erasmus_socrates_studentship',
    (current_date - interval '1 day')::date,
    (current_date + interval '1 year')::date,
    (current_date + interval '1 year')::date,
    null::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s_crsid),
    'e-3',
    'Erasmus exchange student'::varchar(80),
    'Current'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute new_erasmus',
                 'Inserted new Erasmus student into _hr_role ok');

-- update supervisor and dates
UPDATE erasmus_socrates_studentship SET start_date = (current_date - interval '1 month')::date, end_date = (current_date - interval '1 month')::date, supervisor_id = (SELECT id FROM person WHERE crsid = :s2_crsid) WHERE person_id = (SELECT id from person where crsid = :p_crsid);
PREPARE updated_erasmus AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM erasmus_socrates_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'erasmus_socrates_studentship',
    (current_date - interval '1 month')::date,
    (current_date + interval '1 year')::date,
    (current_date - interval '1 month')::date,
    (current_date - interval '1 month')::date,
    null::date,
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'e-3',
    'Erasmus exchange student'::varchar(80),
    'Past'::varchar(10),
    null::varchar(500),
    null::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_erasmus',
                 'Updated new Erasmus in _hr_role ok');

-- delete the role
DELETE FROM erasmus_socrates_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid);
SELECT is_empty('execute new_hr_role','Deleted Erasmus student successfully');

----------------------------
-- postgraduate_studentship
----------------------------

-- add a new one
INSERT INTO postgraduate_studentship
    ( person_id, first_supervisor_id, start_date, intended_end_date, postgraduate_studentship_type_id, filemaker_funding, filemaker_fees_funding, funding_end_date )
    VALUES
    (
        (SELECT id FROM person WHERE crsid = :p_crsid ),
        (SELECT id FROM person WHERE crsid = :s_crsid ),
        current_date - interval '1 day',
        current_date + interval '4 years',
        (SELECT id FROM postgraduate_studentship_type WHERE name = 'PhD'),
        'CASE',
        'Self-funded',
        current_date + interval '3 years'
    );

PREPARE new_pg AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM postgraduate_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'postgraduate_studentship',
    (current_date - interval '1 day')::date,
    (current_date + interval '4 years')::date,
    (current_date + interval '4 years')::date,
    null::date,
    (current_date + interval '3 years')::date,
    (SELECT id FROM person WHERE crsid = :s_crsid),
    'pg-1',
    'PhD'::varchar(80),
    'Current'::varchar(10),
    'CASE'::varchar(500),
    'Self-funded'::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute new_pg',
                 'Inserted new postgrad student into _hr_role ok');

-- update supervisor etc FIXME once we do away with date_withdrawn_from_register etc columns, update end_date instead
UPDATE postgraduate_studentship SET start_date = (current_date - interval '2 months')::date, date_withdrawn_from_register = (current_date - interval '1 month')::date, first_supervisor_id = (SELECT id FROM person WHERE crsid = :s2_crsid), filemaker_funding = 'Dept', filemaker_fees_funding = 'Dept2' WHERE person_id = (SELECT id from person where crsid = :p_crsid);

PREPARE updated_pg AS VALUES (
    (SELECT id FROM person WHERE crsid = :p_crsid),
    (SELECT id FROM postgraduate_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid)),
    'postgraduate_studentship',
    (current_date - interval '2 months')::date, -- start
    (current_date + interval '4 years')::date, -- intended end
    (current_date - interval '1 month')::date, -- estimated role end
    (current_date - interval '1 month')::date, -- end date
    (current_date + interval '3 years')::date, -- funding
    (SELECT id FROM person WHERE crsid = :s2_crsid),
    'pg-1',
    'PhD'::varchar(80),
    'Past'::varchar(10),
    'Dept'::varchar(500),
    'Dept2'::varchar(500),
    null::varchar(500),
    null::boolean,
    null::varchar(120),
    'f'::boolean
);

SELECT results_eq('execute new_hr_role',
                 'execute updated_pg',
                 'Updated new postgrad in _hr_role ok');

-- delete the role
DELETE FROM postgraduate_studentship WHERE person_id = (SELECT id FROM person WHERE crsid = :p_crsid);
SELECT is_empty('execute new_hr_role','Deleted postgrad student successfully');


SELECT * FROM finish();

ROLLBACK;
