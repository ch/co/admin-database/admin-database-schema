ALTER TABLE public.person ADD COLUMN last_leaving_reminder_sent TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL;
ALTER TABLE public.person ADD COLUMN safety_last_notified_leaving_date DATE NULL DEFAULT NULL;

CREATE VIEW leavers_form.person_notification AS
    SELECT
      person.id AS person_id,
      person.last_leaving_reminder_sent,
      person.safety_last_notified_leaving_date
    FROM person
;

ALTER TABLE leavers_form.person_notification OWNER TO dev;

GRANT SELECT, UPDATE ON leavers_form.person_notification TO onlineleaversform;
