DROP VIEW leavers_form.person_notification;

ALTER TABLE public.person DROP COLUMN last_leaving_reminder_sent;
ALTER TABLE public.person DROP COLUMN safety_last_notified_leaving_date;
