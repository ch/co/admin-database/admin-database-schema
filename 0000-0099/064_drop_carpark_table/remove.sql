CREATE SEQUENCE carpark_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 156
  CACHE 1;
ALTER TABLE carpark_id_seq
  OWNER TO cen1001;
GRANT ALL ON SEQUENCE carpark_id_seq TO cen1001;
GRANT USAGE ON SEQUENCE carpark_id_seq TO carpark_management;


CREATE TABLE carpark
(
  id bigint NOT NULL DEFAULT nextval('carpark_id_seq'::regclass),
  person_id bigint,
  car_registration1 character varying(20),
  car_registration2 character varying(20),
  frequency_id bigint,
  notes character varying(50),
  badge_number character varying(20),
  CONSTRAINT carpark_pkey PRIMARY KEY (id),
  CONSTRAINT carpark_fk_frequency_id FOREIGN KEY (frequency_id)
      REFERENCES frequency_hid (frequency_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT carpark_fk_person_id FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT carpark_new_badge_number_key UNIQUE (badge_number)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE carpark
  OWNER TO cen1001;

CREATE TRIGGER carpark_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON carpark
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();
