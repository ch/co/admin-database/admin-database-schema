CREATE TABLE workload_marking (
    id integer NOT NULL,
    person_id integer,
    tripos_id integer,
    hours numeric,
    marking_role_id integer
);


ALTER TABLE public.workload_marking OWNER TO rl201;

CREATE SEQUENCE workload_marking_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_marking_id_seq OWNER TO rl201;

ALTER SEQUENCE workload_marking_id_seq OWNED BY workload_marking.id;

ALTER TABLE ONLY workload_marking ALTER COLUMN id SET DEFAULT nextval('workload_marking_id_seq'::regclass);

ALTER TABLE ONLY workload_marking
    ADD CONSTRAINT workload_marking_pkey PRIMARY KEY (id);

ALTER TABLE ONLY workload_marking
    ADD CONSTRAINT workload_marking_marking_type_id_fkey FOREIGN KEY (marking_role_id) REFERENCES workload_marking_type(id);

ALTER TABLE ONLY workload_marking
    ADD CONSTRAINT workload_marking_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);

ALTER TABLE ONLY workload_marking
    ADD CONSTRAINT workload_marking_tripos_id_fkey FOREIGN KEY (tripos_id) REFERENCES examination(id);

REVOKE ALL ON SEQUENCE workload_marking_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_marking_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_marking_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_marking_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_marking_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_marking_id_seq TO workload;
