alter view public.www_academic_staff_v1 owner to dev;
revoke select on public.www_academic_staff_v1 from rc495;
revoke select on public.www_academic_staff_v1 from cen1001;
grant select on public.www_academic_staff_v1 to www_sites;

alter view public.www_telephone_directory_v1 owner to dev;
revoke select on public.www_telephone_directory_v1 from rc495;
grant select on public.www_telephone_directory_v1 to dev;
grant select on public.www_telephone_directory_v1 to www_sites;
