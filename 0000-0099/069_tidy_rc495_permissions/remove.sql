ALTER TABLE www_telephone_directory_v1 OWNER TO postgres;
GRANT ALL ON TABLE www_telephone_directory_v1 TO postgres;
GRANT SELECT ON TABLE www_telephone_directory_v1 TO www_sites;
GRANT SELECT ON TABLE www_telephone_directory_v1 TO rc495;

ALTER TABLE www_academic_staff_v1 OWNER TO postgres;
GRANT ALL ON TABLE www_academic_staff_v1 TO postgres;
GRANT SELECT ON TABLE www_academic_staff_v1 TO rc495;
GRANT SELECT ON TABLE www_academic_staff_v1 TO www_sites;
GRANT SELECT ON TABLE www_academic_staff_v1 TO cen1001;
GRANT ALL ON TABLE www_academic_staff_v1 TO dev;
