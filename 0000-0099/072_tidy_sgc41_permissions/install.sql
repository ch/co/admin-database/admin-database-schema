ALTER TABLE _login_background OWNER TO dev;
REVOKE ALL ON TABLE _login_background FROM sgc41;
GRANT ALL ON TABLE _login_background TO dev;
GRANT SELECT ON TABLE _login_background TO osbuilder;
GRANT SELECT ON TABLE _login_background TO cos;
