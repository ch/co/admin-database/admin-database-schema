--
-- Name: 10_Workload/Admin/College types; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/College types" AS
SELECT workload_college_type.id, workload_college_type.name, workload_college_type.weight, workload_college_type.hourly FROM public.workload_college_type;


ALTER TABLE hotwire."10_Workload/Admin/College types" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Committees/Departmental; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Committees/Departmental" AS
SELECT workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight, workload_committee.weight_chair, workload_committee.weight_secretary FROM public.workload_committee WHERE workload_committee.departmental;


ALTER TABLE hotwire."10_Workload/Admin/Committees/Departmental" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Committees/External; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Committees/External" AS
SELECT workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight FROM public.workload_committee WHERE ((NOT workload_committee.ucam) AND (NOT workload_committee.departmental));


ALTER TABLE hotwire."10_Workload/Admin/Committees/External" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Committees/University; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Committees/University" AS
SELECT workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight FROM public.workload_committee WHERE (workload_committee.ucam AND (NOT workload_committee.departmental));


ALTER TABLE hotwire."10_Workload/Admin/Committees/University" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Examining Roles; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Examining Roles" AS
SELECT workload_examining_role.id, workload_examining_role.name, workload_examining_role.ucam AS internal, workload_examining_role.departmental, workload_examining_role.postgrad, workload_examining_role.weight, workload_examining_role.js_extra AS _js_extra FROM public.workload_examining_role;


ALTER TABLE hotwire."10_Workload/Admin/Examining Roles" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Grants_Summary; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hotwire."10_Workload/Admin/Grants_Summary" AS
SELECT workload_grants.id, workload_grants.person_id, workload_grants.big_grants, workload_grants.small_grants FROM public.workload_grants;


ALTER TABLE hotwire."10_Workload/Admin/Grants_Summary" OWNER TO postgres;

--
-- Name: 10_Workload/Admin/Internal/Lecture Courses; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Internal/Lecture Courses" AS
SELECT lecture_course.id, lecture_course.name, lecture_course.tripos_id AS examination_id, lecture_course.active, examination.postgrad AS postgraduate_ro FROM (public.lecture_course JOIN public.examination ON ((lecture_course.tripos_id = examination.id))) WHERE (NOT lecture_course.is_practicals);


ALTER TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Internal/New or Old Lecture Course Options; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hotwire."10_Workload/Admin/Internal/New or Old Lecture Course Options" AS
SELECT workload_lecturing_type.id, workload_lecturing_type.name, workload_lecturing_type.weight FROM public.workload_lecturing_type;


ALTER TABLE hotwire."10_Workload/Admin/Internal/New or Old Lecture Course Options" OWNER TO postgres;

--
-- Name: 10_Workload/Admin/Internal/Practical Classes; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Internal/Practical Classes" AS
SELECT lecture_course.id, lecture_course.name, lecture_course.tripos_id, lecture_course.active FROM public.lecture_course WHERE lecture_course.is_practicals;


ALTER TABLE hotwire."10_Workload/Admin/Internal/Practical Classes" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Marking Roles; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Marking Roles" AS
SELECT workload_marking_type.id, workload_marking_type.name, workload_marking_type.weight FROM public.workload_marking_type;


ALTER TABLE hotwire."10_Workload/Admin/Marking Roles" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Misc_Weighting_Factors; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Misc_Weighting_Factors" AS
SELECT workload_weights.id, workload_weights.weight, workload_weights.notes AS ro_notes FROM public.workload_weights;


ALTER TABLE hotwire."10_Workload/Admin/Misc_Weighting_Factors" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Outreach Types; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Outreach Types" AS
SELECT workload_outreach_type.id, workload_outreach_type.name, workload_outreach_type.weight FROM public.workload_outreach_type;


ALTER TABLE hotwire."10_Workload/Admin/Outreach Types" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Personnel types; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Personnel types" AS
SELECT workload_student_type.id, workload_student_type.name, workload_student_type.weight FROM public.workload_student_type;


ALTER TABLE hotwire."10_Workload/Admin/Personnel types" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Research types; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Research types" AS
SELECT workload_research_type.id, workload_research_type.name, workload_research_type.weight FROM public.workload_research_type;


ALTER TABLE hotwire."10_Workload/Admin/Research types" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Supervising Roles; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Supervising Roles" AS
SELECT workload_supervising_type.id, workload_supervising_type.name, workload_supervising_type.weight FROM public.workload_supervising_type;


ALTER TABLE hotwire."10_Workload/Admin/Supervising Roles" OWNER TO rl201;

--
-- Name: 10_Workload/Admin/Supervision Types; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Admin/Supervision Types" AS
SELECT workload_supervising_type.id, workload_supervising_type.name, workload_supervising_type.weight FROM public.workload_supervising_type;


ALTER TABLE hotwire."10_Workload/Admin/Supervision Types" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Additional Notes; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Additional Notes" AS
SELECT workload_extra.id, workload_extra.person_id, workload_extra.hours, workload_extra.notes, workload_extra.process_notes FROM public.workload_extra;


ALTER TABLE hotwire."10_Workload/Everybody/Additional Notes" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/College; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/College" AS
SELECT workload_college.id, workload_college.workload_college_type_id, workload_college.person_id, workload_college.hours FROM public.workload_college;


ALTER TABLE hotwire."10_Workload/Everybody/College" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/External/Committee_Membership; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/External/Committee_Membership" AS
SELECT workload_committeemembership.id, workload_committeemembership.workload_committee_id AS external_committee_id, workload_committeemembership.person_id, workload_committeemembership.hours, workload_committeemembership.chair AS "Chair or Secretary", workload_committeemembership.notes FROM (public.workload_committee_membership workload_committeemembership JOIN public.workload_committee ON ((workload_committeemembership.workload_committee_id = workload_committee.id))) WHERE ((NOT workload_committee.ucam) AND (NOT workload_committee.departmental));


ALTER TABLE hotwire."10_Workload/Everybody/External/Committee_Membership" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/External/Consulting_and_Charities; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/External/Consulting_and_Charities" AS
SELECT workload_external_consulting.id, workload_external_consulting.name, workload_external_consulting.role, workload_external_consulting.hours, workload_external_consulting.notes, workload_external_consulting.person_id FROM public.workload_external_consulting;


ALTER TABLE hotwire."10_Workload/Everybody/External/Consulting_and_Charities" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/External/Examining; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/External/Examining" AS
SELECT workload_examining.id, workload_examining.person_id, workload_examining.hours, workload_examining.role_id AS external_examiner_role_id FROM (public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) WHERE (NOT workload_examining_type.departmental);


ALTER TABLE hotwire."10_Workload/Everybody/External/Examining" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Committee_Membership; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Committee_Membership" AS
SELECT workload_committeemembership.id, workload_committeemembership.workload_committee_id AS departmental_committee_id, workload_committeemembership.person_id, workload_committeemembership.hours, workload_committeemembership.chair AS "Chair or Secretary", workload_committeemembership.notes FROM (public.workload_committee_membership workload_committeemembership JOIN public.workload_committee ON ((workload_committeemembership.workload_committee_id = workload_committee.id))) WHERE workload_committee.departmental;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Committee_Membership" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Departmental Supervisions; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" AS
SELECT workload_supervising.id, workload_supervising.person_id, workload_supervising.tripos_id, workload_supervising.hours, workload_supervising.supervision_type_id FROM public.workload_supervising;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Laboratory Demonstrating; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" AS
SELECT workload_demonstrating.id, workload_demonstrating.person_id, workload_demonstrating.academic_year, workload_demonstrating.practical_course_id, workload_demonstrating.hours FROM public.workload_demonstrating;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Lecturing; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Lecturing" AS
SELECT workload_lecturing.id, workload_lecturing.person_id, workload_lecturing.lecture_course_id, workload_lecturing.hours, workload_lecturing.tripos_id, workload_lecturing.lecture_course_type_id AS "First_Time_id" FROM public.workload_lecturing;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Outreach; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Outreach" AS
SELECT workload_outreach.id, workload_outreach.outreach_type_id, workload_outreach.hours, workload_outreach.notes, workload_outreach.person_id FROM public.workload_outreach;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Outreach" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Postgraduate_Exams; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" AS
SELECT workload_examining.id, workload_examining.person_id, workload_examining.hours, workload_examining.role_id AS internal_pg_examiner_role_id FROM (public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) WHERE (workload_examining_type.postgrad AND workload_examining_type.departmental);


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Research; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Research" AS
SELECT workload_research.id, workload_research.person_id, workload_research.workload_research_type_id AS research_type_id, workload_research.number FROM public.workload_research;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Research" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Students; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Students" AS
SELECT workload_students.id, workload_students.person_id, workload_students.student_type_id, workload_students.number FROM public.workload_students;


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Students" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Internal/Undergraduate_Exams; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" AS
SELECT workload_examining.id, workload_examining.person_id, workload_examining.examination_id AS undergraduate_exam_id, workload_examining.hours, workload_examining.role_id AS internal_ug_examiner_role_id FROM ((public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) JOIN public.examination ON ((workload_examining.examination_id = examination.id))) WHERE (((NOT examination.postgrad) AND (NOT workload_examining_type.postgrad)) AND workload_examining_type.departmental);


ALTER TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/Total Workload; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/Total Workload" AS
SELECT person.id, ((((('Workload summary for '::text || (COALESCE(person.known_as, person.first_names))::text) || ' '::text) || (person.surname)::text) || ' for the academic year '::text) || (public.academic_year((now())::date))::text) AS ro_header, 'Departmental'::character varying AS ro_header_2, (SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE (workload_committee.departmental AND (workload_committee_membership.person_id = person.id))) AS "ro_Total Committee Time (hours)", public._to_hwsubviewb('10_Workload/Internal/Committee_Membership'::character varying, 'person_id'::character varying, '10_Workload/Internal/Committee_Membership'::character varying, NULL::character varying, NULL::character varying) AS "Committee Membership", (SELECT sum(workload_examining.hours) AS sum FROM (public.workload_examining JOIN public.workload_examining_role ON ((workload_examining_role.id = workload_examining.role_id))) WHERE (workload_examining_role.departmental AND (workload_examining.person_id = person.id))) AS "ro_Total Internal Examining Time (hours)", public._to_hwsubviewb('10_Workload/Internal/Undergraduate_Exams'::character varying, 'person_id'::character varying, '10_Workload/Internal/Undergraduate_Exams'::character varying, NULL::character varying, NULL::character varying) AS "Undergraduate Exams", public._to_hwsubviewb('10_Workload/Internal/Postgraduate_Exams'::character varying, 'person_id'::character varying, '10_Workload/Internal/Postgraduate_Exams'::character varying, NULL::character varying, NULL::character varying) AS "Postgraduate Exams", (SELECT sum(workload_marking.hours) AS sum FROM public.workload_marking WHERE (workload_marking.person_id = person.id)) AS "ro_Total Marking Time (hours)", public._to_hwsubviewb('10_Workload/Internal/Marking'::character varying, 'person_id'::character varying, '10_Workload/Internal/Marking'::character varying, NULL::character varying, NULL::character varying) AS "Marking", (SELECT sum(workload_outreach.hours) AS sum FROM public.workload_outreach WHERE (workload_outreach.person_id = person.id)) AS "ro_Total Outreach Time (hours)", public._to_hwsubviewb('10_Workload/Internal/Outreach'::character varying, 'person_id'::character varying, '10_Workload/Internal/Outreach'::character varying, NULL::character varying, NULL::character varying) AS "Outreach", 'University'::character varying AS ro_header_3, (SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE ((workload_committee.ucam AND (NOT workload_committee.departmental)) AND (workload_committee_membership.person_id = person.id))) AS "ro_Total Time to University (hours)", public._to_hwsubviewb('10_Workload/University_Committees'::character varying, 'person_id'::character varying, '10_Workload/University_Committees'::character varying, NULL::character varying, NULL::character varying) AS "University time", 'College'::character varying AS ro_header_4, (SELECT sum(workload_college.hours) AS sum FROM public.workload_college WHERE (workload_college.person_id = person.id)) AS "ro_Total Time to College (hours)", public._to_hwsubviewb('10_Workload/College'::character varying, 'person_id'::character varying, '10_Workload/College'::character varying, NULL::character varying, NULL::character varying) AS "College time", 'External'::character varying AS ro_header_5, ((SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE ((NOT workload_committee.ucam) AND (workload_committee_membership.person_id = person.id))) + (SELECT sum(workload_examining.hours) AS sum FROM (public.workload_examining JOIN public.workload_examining_role ON ((workload_examining_role.id = workload_examining.role_id))) WHERE ((NOT workload_examining_role.ucam) AND (workload_examining.person_id = person.id)))) AS "Total Time spent externally (hours)", public._to_hwsubviewb('10_Workload/External/Committee_Membership'::character varying, 'person_id'::character varying, '10_Workload/External/Committee_Membership'::character varying, NULL::character varying, NULL::character varying) AS "Committee time", public._to_hwsubviewb('10_Workload/External/Examining'::character varying, 'person_id'::character varying, '10_Workload/External/Examining'::character varying, NULL::character varying, NULL::character varying) AS "Examining time", 'Additional'::character varying AS ro_header_6, workload_extra.hours AS "ro_Additional Hours (please give details)", workload_extra.notes AS "Additional details", 'Feedback'::character varying AS ro_header_7, workload_extra.process_notes AS "Comments on this process" FROM (public.person LEFT JOIN public.workload_extra ON ((person.id = workload_extra.person_id)));


ALTER TABLE hotwire."10_Workload/Everybody/Total Workload" OWNER TO rl201;

--
-- Name: 10_Workload/Everybody/University_Committees; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Everybody/University_Committees" AS
SELECT workload_committee_membership.id, workload_committee_membership.workload_committee_id AS university_committee_id, workload_committee_membership.person_id, workload_committee_membership.hours, workload_committee_membership.chair AS "Chair or Secretary", workload_committee_membership.notes FROM (public.workload_committee_membership workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE (workload_committee.ucam AND (NOT workload_committee.departmental));


ALTER TABLE hotwire."10_Workload/Everybody/University_Committees" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/College; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/College" AS
SELECT workload_college.id, workload_college.workload_college_type_id, workload_college.hours FROM (public.workload_college JOIN public.person ON ((workload_college.person_id = person.id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/College" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/External/Committee_Membership; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" AS
SELECT workload_committeemembership.id, workload_committeemembership.workload_committee_id AS external_committee_id, workload_committeemembership.hours, workload_committeemembership.chair, workload_committeemembership.secretary, workload_committeemembership.notes FROM ((public.workload_committee_membership workload_committeemembership JOIN public.workload_committee ON ((workload_committeemembership.workload_committee_id = workload_committee.id))) JOIN public.person ON ((workload_committeemembership.person_id = person.id))) WHERE (((NOT workload_committee.ucam) AND (NOT workload_committee.departmental)) AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/External/Consulting_and_Charitie; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" AS
SELECT workload_external_consulting.id, workload_external_consulting.name, workload_external_consulting.role, workload_external_consulting.hours, workload_external_consulting.notes FROM (public.workload_external_consulting JOIN public.person ON ((workload_external_consulting.person_id = person.id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/External/Examining; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/External/Examining" AS
SELECT workload_examining.id, workload_examining.role_id AS external_examiner_role_id, workload_examining.hours FROM ((public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) JOIN public.person ON ((workload_examining.person_id = person.id))) WHERE ((NOT workload_examining_type.departmental) AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/External/Examining" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Histogram; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Histogram" AS
SELECT 1 AS id, ''::character varying AS histogram;


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Histogram" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Committee_Membership; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" AS
SELECT workload_committeemembership.id, workload_committeemembership.workload_committee_id AS departmental_committee_id, workload_committeemembership.hours, workload_committeemembership.chair, workload_committeemembership.secretary, workload_committeemembership.notes FROM ((public.workload_committee_membership workload_committeemembership JOIN public.workload_committee ON ((workload_committeemembership.workload_committee_id = workload_committee.id))) JOIN public.person ON ((workload_committeemembership.person_id = person.id))) WHERE (workload_committee.departmental AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Departmental Supervisio; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" AS
SELECT workload_supervising.id, workload_supervising.tripos_id AS dept_supervision_tripos_id, workload_supervising.hours, workload_supervising.supervision_type_id FROM (public.workload_supervising JOIN public.person ON ((person.id = workload_supervising.person_id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" AS
SELECT workload_demonstrating.id, workload_demonstrating.practical_course_id, workload_demonstrating.tripos_id, workload_demonstrating.hours FROM (public.workload_demonstrating JOIN public.person ON ((workload_demonstrating.person_id = person.id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Outreach; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Outreach" AS
SELECT workload_outreach.id, workload_outreach.outreach_type_id, workload_outreach.hours, workload_outreach.notes FROM (public.workload_outreach JOIN public.person ON ((workload_outreach.person_id = person.id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Outreach" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" AS
SELECT workload_lecturing.id, workload_lecturing.lecture_course_id AS postgraduate_lecture_course_id, workload_lecturing.hours, workload_lecturing.lecture_course_type_id AS "First Time_id" FROM (((public.workload_lecturing JOIN public.lecture_course ON ((lecture_course.id = workload_lecturing.lecture_course_id))) JOIN public.examination ON ((lecture_course.tripos_id = examination.id))) JOIN public.person ON ((workload_lecturing.person_id = person.id))) WHERE (((person.crsid)::name = "current_user"()) AND examination.postgrad);


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate_Exams; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" AS
SELECT workload_examining.id, workload_examining.hours, workload_examining.role_id AS internal_pg_examiner_role_id FROM ((public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) JOIN public.person ON ((workload_examining.person_id = person.id))) WHERE ((workload_examining_type.postgrad AND workload_examining_type.departmental) AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Research; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Research" AS
SELECT workload_research.id, workload_research.workload_research_type_id AS research_type_id, workload_research.number FROM (public.workload_research JOIN public.person ON ((workload_research.person_id = person.id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Research_Group_Personne; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" AS
SELECT workload_students.id, workload_students.student_type_id AS personnel_type_id, workload_students.number FROM (public.workload_students JOIN public.person ON ((workload_students.person_id = person.id)));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" AS
SELECT workload_lecturing.id, workload_lecturing.tripos_id, workload_lecturing.lecture_course_id AS undergraduate_lecture_course_id, workload_lecturing.hours, workload_lecturing.lecture_course_type_id AS "First Time_id" FROM (((public.workload_lecturing JOIN public.lecture_course ON ((lecture_course.id = workload_lecturing.lecture_course_id))) JOIN public.examination ON ((lecture_course.tripos_id = examination.id))) JOIN public.person ON ((workload_lecturing.person_id = person.id))) WHERE (((person.crsid)::name = "current_user"()) AND (NOT examination.postgrad));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate_Exams; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" AS
SELECT workload_examining.id, workload_examining.examination_id AS undergraduate_exam_id, workload_examining.hours, workload_examining.role_id AS internal_ug_examiner_role_id FROM (((public.workload_examining JOIN public.workload_examining_role workload_examining_type ON ((workload_examining.role_id = workload_examining_type.id))) JOIN public.examination ON ((workload_examining.examination_id = examination.id))) JOIN public.person ON ((workload_examining.person_id = person.id))) WHERE ((((NOT examination.postgrad) AND (NOT workload_examining_type.postgrad)) AND workload_examining_type.departmental) AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" OWNER TO rl201;

--
-- Name: 10_Workload/Former_My_Workload/University_Committees; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/Former_My_Workload/University_Committees" AS
SELECT workload_committee_membership.id, workload_committee_membership.workload_committee_id AS university_committee_id, workload_committee_membership.hours, workload_committee_membership.chair AS "Chair or Secretary", workload_committee_membership.notes FROM ((public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) JOIN public.person ON ((person.id = workload_committee_membership.person_id))) WHERE ((workload_committee.ucam AND (NOT workload_committee.departmental)) AND ((person.crsid)::name = "current_user"()));


ALTER TABLE hotwire."10_Workload/Former_My_Workload/University_Committees" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Broken_Outreach; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Broken_Outreach" AS
SELECT a.id, a.ro_name, a.outreach_type_id, a.workload, a.notes FROM (SELECT COALESCE((SELECT (workload_outreach.id)::text AS id FROM public.workload_outreach WHERE ((workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_outreach.outreach_type_id = t.id))), ('NEW'::text || t.id)) AS id, t.id AS outreach_type_id, (SELECT sum(workload_outreach.hours) AS sum FROM public.workload_outreach WHERE ((workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_outreach.outreach_type_id = t.id))) AS workload, (SELECT workload_outreach.notes FROM public.workload_outreach WHERE ((workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_outreach.outreach_type_id = t.id))) AS notes, t.name AS ro_name FROM public.workload_outreach_type t) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Broken_Outreach" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//College; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//College" AS
SELECT a.id, a.workload_college_type_id, a.ro_workload_college_type, a.workload FROM (SELECT COALESCE((SELECT (workload_college.id)::text AS id FROM public.workload_college WHERE ((workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_college.workload_college_type_id = workload_college_type.id))), ('NEW'::text || workload_college_type.id)) AS id, workload_college_type.name AS ro_workload_college_type, workload_college_type.id AS workload_college_type_id, (SELECT workload_college.hours FROM public.workload_college WHERE ((workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_college.workload_college_type_id = workload_college_type.id))) AS workload FROM public.workload_college_type ORDER BY workload_college_type.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//College" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//College_Old; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//College_Old" AS
SELECT a.id, a.workload_college_type_id, a.workload FROM (SELECT COALESCE((SELECT (workload_college.id)::text AS id FROM public.workload_college WHERE ((workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_college.workload_college_type_id = workload_college_type.id))), ('NEW'::text || workload_college_type.id)) AS id, workload_college_type.id AS workload_college_type_id, (SELECT workload_college.hours FROM public.workload_college WHERE ((workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_college.workload_college_type_id = workload_college_type.id))) AS workload FROM public.workload_college_type ORDER BY workload_college_type.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//College_Old" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Consulting; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Consulting" AS
SELECT a.id, a.name, a.role, a.hours, a.notes FROM (SELECT (workload_external_consulting.id)::text AS id, workload_external_consulting.name, workload_external_consulting.role, workload_external_consulting.hours, (workload_external_consulting.notes)::character varying AS notes FROM public.workload_external_consulting WHERE (workload_external_consulting.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))) UNION ALL SELECT 'NEW', 'click to add a new entry', 'click to add a new entry', NULL::unknown, 'click to add a new entry') a;


ALTER TABLE hotwire."10_Workload/My_Workload//Consulting" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Committees; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Departmental_Committees" AS
SELECT a.id, a.ro_departmental_committee_name, a.departmental_committee_id, a.workload FROM (SELECT COALESCE((SELECT (workload_committee_membership.id)::text AS id FROM public.workload_committee_membership WHERE ((workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_committee_membership.workload_committee_id = workload_committee.id))), ('NEW'::text || workload_committee.id)) AS id, workload_committee.id AS departmental_committee_id, (SELECT workload_committee_membership.hours FROM public.workload_committee_membership WHERE ((workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_committee_membership.workload_committee_id = workload_committee.id))) AS workload, workload_committee.name AS ro_departmental_committee_name FROM public.workload_committee WHERE workload_committee.departmental ORDER BY workload_committee.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Departmental_Committees" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Departmental_Demonstrating" AS
SELECT a.id, a.ro_coursename, a.practical_course_id, a.tripos_id, a.workload FROM (SELECT COALESCE((SELECT (workload_demonstrating.id)::text AS id FROM public.workload_demonstrating WHERE (((workload_demonstrating.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_demonstrating.practical_course_id = l.id)) AND (workload_demonstrating.tripos_id = e.id))), ((('NEW'::text || l.id) || '-'::text) || e.id)) AS id, l.id AS practical_course_id, e.id AS tripos_id, (SELECT sum(workload_demonstrating.hours) AS sum FROM public.workload_demonstrating WHERE (((workload_demonstrating.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_demonstrating.practical_course_id = l.id)) AND (workload_demonstrating.tripos_id = e.id))) AS workload, ((((e.name)::text || ' - '::text) || (l.name)::text))::character varying AS ro_coursename FROM public.lecture_course l, public.examination e WHERE (l.is_practicals AND (NOT e.postgrad))) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Departmental_Demonstrating" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" AS
SELECT a.id, a.ro_name, a.postgraduate_lecture_course_id, a."First_Time_id", a.workload FROM (SELECT COALESCE((SELECT (workload_lecturing.id)::text AS id FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id)) LIMIT 1), ('NEW'::text || l.id)) AS id, l.id AS postgraduate_lecture_course_id, (SELECT workload_lecturing.lecture_course_type_id FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id)) LIMIT 1) AS "First_Time_id", (SELECT sum(workload_lecturing.hours) AS sum FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id))) AS workload, l.name AS ro_name FROM (public.lecture_course l JOIN public.examination e ON ((l.tripos_id = e.id))) WHERE ((NOT l.is_practicals) AND e.postgrad) ORDER BY l.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Supervising; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Departmental_Supervising" AS
SELECT a.id, a.ro_name, a.dept_supervision_tripos_id, a.supervision_type_id, a.workload FROM (SELECT COALESCE((SELECT (workload_supervising.id)::text AS id FROM public.workload_supervising WHERE (((workload_supervising.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = 'rl201'::text))) AND (workload_supervising.tripos_id = examination.id)) AND (workload_supervising.supervision_type_id = workload_supervising_type.id)) LIMIT 1), ((('NEW'::text || examination.id) || '-'::text) || workload_supervising_type.id)) AS id, examination.id AS dept_supervision_tripos_id, (SELECT sum(workload_supervising.hours) AS sum FROM public.workload_supervising WHERE (((workload_supervising.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = 'rl201'::text))) AND (workload_supervising.tripos_id = examination.id)) AND (workload_supervising.supervision_type_id = workload_supervising_type.id))) AS workload, workload_supervising_type.id AS supervision_type_id, ((((examination.name)::text || ' - '::text) || (workload_supervising_type.name)::text))::character varying AS ro_name FROM public.examination, public.workload_supervising_type WHERE examination.supervised_by_dept ORDER BY examination.name, workload_supervising_type.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Departmental_Supervising" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" AS
SELECT a.id, a.lecture_course_id, a."First_Time_id", a.workload FROM (SELECT COALESCE((SELECT (workload_lecturing.id)::text AS id FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id)) LIMIT 1), ('NEW'::text || l.id)) AS id, l.id AS lecture_course_id, (SELECT workload_lecturing.lecture_course_type_id FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id)) LIMIT 1) AS "First_Time_id", (SELECT sum(workload_lecturing.hours) AS sum FROM public.workload_lecturing WHERE ((workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_lecturing.lecture_course_id = l.id))) AS workload FROM (public.lecture_course l JOIN public.examination e ON ((l.tripos_id = e.id))) WHERE ((NOT l.is_practicals) AND (NOT e.postgrad)) ORDER BY l.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Outreach; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Outreach" AS
SELECT a.id, a.ro_name, a.outreach_type_id, a.workload, a.notes FROM (SELECT (workload_outreach.id)::character varying AS id, workload_outreach_type.name AS ro_name, workload_outreach.outreach_type_id, workload_outreach.hours AS workload, workload_outreach.notes FROM (public.workload_outreach JOIN public.workload_outreach_type ON ((workload_outreach.outreach_type_id = workload_outreach_type.id))) WHERE (workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) UNION SELECT ('NEW'::text || workload_outreach_type.id), workload_outreach_type.name, workload_outreach_type.id, NULL::integer AS int4, 'Click to add a new item'::character varying AS notes FROM public.workload_outreach_type ORDER BY 1) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Outreach" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Postgraduate_Examining" AS
SELECT a.id, a.ro_name, a.internal_pg_examiner_role_id, a.workload FROM (SELECT COALESCE((SELECT (workload_examining.id)::text AS id FROM public.workload_examining WHERE ((workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_examining.role_id = r.id)) LIMIT 1), ('NEW'::text || r.id)) AS id, r.id AS internal_pg_examiner_role_id, (SELECT sum(workload_examining.hours) AS sum FROM public.workload_examining WHERE ((workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_examining.role_id = r.id))) AS workload, r.name AS ro_name FROM public.workload_examining_role r WHERE (r.postgrad AND r.ucam)) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Postgraduate_Examining" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Research; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Research" AS
SELECT a.id, a.ro_name, a.research_type_id, a.workload FROM (SELECT COALESCE((SELECT (workload_research.id)::text AS id FROM public.workload_research WHERE ((workload_research.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_research.workload_research_type_id = t.id))), ('NEW'::text || t.id)) AS id, t.id AS research_type_id, (SELECT sum(workload_research.number) AS sum FROM public.workload_research WHERE ((workload_research.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_research.workload_research_type_id = t.id))) AS workload, t.name AS ro_name FROM public.workload_research_type t) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Research" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Research_Group; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Research_Group" AS
SELECT a.id, a.ro_name, a.personnel_type_id, a.workload FROM (SELECT COALESCE((SELECT (workload_students.id)::text AS id FROM public.workload_students WHERE ((workload_students.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_students.student_type_id = s.id))), ('NEW'::text || s.id)) AS id, s.id AS personnel_type_id, (SELECT sum(workload_students.number) AS sum FROM public.workload_students WHERE ((workload_students.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_students.student_type_id = s.id))) AS workload, s.name AS ro_name FROM public.workload_student_type s) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Research_Group" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//Undergraduate_Examining" AS
SELECT a.id, a.ro_name, a.internal_ug_examiner_role_id, a.workload FROM (SELECT COALESCE((SELECT (workload_examining.id)::text AS id FROM public.workload_examining WHERE ((workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_examining.role_id = r.id)) LIMIT 1), ('NEW'::text || r.id)) AS id, r.id AS internal_ug_examiner_role_id, (SELECT sum(workload_examining.hours) AS sum FROM public.workload_examining WHERE ((workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text) LIMIT 1)) AND (workload_examining.role_id = r.id))) AS workload, r.name AS ro_name FROM public.workload_examining_role r WHERE ((NOT r.postgrad) AND r.ucam)) a;


ALTER TABLE hotwire."10_Workload/My_Workload//Undergraduate_Examining" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//University_Committees; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload//University_Committees" AS
SELECT a.id, a.ro_name, a.university_committee_id, a.notes, a.workload FROM (SELECT COALESCE((SELECT (workload_committee_membership.id)::text AS id FROM public.workload_committee_membership WHERE ((workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_committee_membership.workload_committee_id = workload_committee.id))), ('NEW'::text || workload_committee.id)) AS id, workload_committee.id AS university_committee_id, (SELECT workload_committee_membership.hours FROM public.workload_committee_membership WHERE ((workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_committee_membership.workload_committee_id = workload_committee.id))) AS workload, (SELECT workload_committee_membership.notes FROM public.workload_committee_membership WHERE ((workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = ("current_user"())::text))) AND (workload_committee_membership.workload_committee_id = workload_committee.id)) LIMIT 1) AS notes, workload_committee.name AS ro_name FROM public.workload_committee WHERE ((NOT workload_committee.departmental) AND workload_committee.ucam) ORDER BY workload_committee.name) a;


ALTER TABLE hotwire."10_Workload/My_Workload//University_Committees" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload_Summary; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire."10_Workload/My_Workload_Summary" AS
SELECT person.id, ((((('Workload summary for '::text || (COALESCE(person.known_as, person.first_names))::text) || ' '::text) || (person.surname)::text) || ' for the academic year '::text) || (public.academic_year((now())::date))::text) AS ro_header, 'Departmental'::character varying AS ro_header_2, (SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE (workload_committee.departmental AND (workload_committee_membership.person_id = person.id))) AS "ro_Total Committee Time (hours)", public._to_hwsubviewb('10_Workload/My_Workload/Internal/Committee_Membership'::character varying, NULL::character varying, '10_Workload/My_Workload/Internal/Committee_Membership'::character varying, NULL::character varying, NULL::character varying) AS "Committee Membership", (SELECT sum(workload_examining.hours) AS sum FROM (public.workload_examining JOIN public.workload_examining_role ON ((workload_examining_role.id = workload_examining.role_id))) WHERE (workload_examining_role.departmental AND (workload_examining.person_id = person.id))) AS "ro_Total Internal Examining Time (hours)", public._to_hwsubviewb('10_Workload/My_Workload/Internal/Undergraduate_Exams'::character varying, NULL::character varying, '10_Workload/My_Workload/Internal/Undergraduate_Exams'::character varying, NULL::character varying, NULL::character varying) AS "Undergraduate Exams", public._to_hwsubviewb('10_Workload/My_Workload/Internal/Postgraduate_Exams'::character varying, NULL::character varying, '10_Workload/My_Workload/Internal/Postgraduate_Exams'::character varying, NULL::character varying, NULL::character varying) AS "Postgraduate Exams", (SELECT sum(workload_marking.hours) AS sum FROM public.workload_marking WHERE (workload_marking.person_id = person.id)) AS "ro_Total Marking Time (hours)", public._to_hwsubviewb('10_Workload/My_Workload/Internal/Marking'::character varying, NULL::character varying, '10_Workload/My_Workload/Internal/Marking'::character varying, NULL::character varying, NULL::character varying) AS "Marking", (SELECT sum(workload_outreach.hours) AS sum FROM public.workload_outreach WHERE (workload_outreach.person_id = person.id)) AS "ro_Total Outreach Time (hours)", public._to_hwsubviewb('10_Workload/My_Workload/Internal/Outreach'::character varying, NULL::character varying, '10_Workload/My_Workload/Internal/Outreach'::character varying, NULL::character varying, NULL::character varying) AS "Outreach", 'University'::character varying AS ro_header_3, (SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE ((workload_committee.ucam AND (NOT workload_committee.departmental)) AND (workload_committee_membership.person_id = person.id))) AS "ro_Total Time to University (hours)", public._to_hwsubviewb('10_Workload/My_Workload/University_Committees'::character varying, NULL::character varying, '10_Workload/My_Workload/University_Committees'::character varying, NULL::character varying, NULL::character varying) AS "University time", 'College'::character varying AS ro_header_4, (SELECT sum(workload_college.hours) AS sum FROM public.workload_college WHERE (workload_college.person_id = person.id)) AS "ro_Total Time to College (hours)", public._to_hwsubviewb('10_Workload/My_Workload/College'::character varying, NULL::character varying, '10_Workload/My_Workload/College'::character varying, NULL::character varying, NULL::character varying) AS "College time", 'External'::character varying AS ro_header_5, ((SELECT sum(workload_committee_membership.hours) AS sum FROM (public.workload_committee_membership JOIN public.workload_committee ON ((workload_committee_membership.workload_committee_id = workload_committee.id))) WHERE ((NOT workload_committee.ucam) AND (workload_committee_membership.person_id = person.id))) + (SELECT sum(workload_examining.hours) AS sum FROM (public.workload_examining JOIN public.workload_examining_role ON ((workload_examining_role.id = workload_examining.role_id))) WHERE ((NOT workload_examining_role.ucam) AND (workload_examining.person_id = person.id)))) AS "Total Time spent externally (hours)", public._to_hwsubviewb('10_Workload/My_Workload/External/Committee_Membership'::character varying, NULL::character varying, '10_Workload/My_Workload/External/Committee_Membership'::character varying, NULL::character varying, NULL::character varying) AS "Committee time", public._to_hwsubviewb('10_Workload/My_Workload/External/Examining'::character varying, NULL::character varying, '10_Workload/My_Workload/External/Examining'::character varying, NULL::character varying, NULL::character varying) AS "Examining time", 'Additional'::character varying AS ro_header_6, workload_extra.hours AS "ro_Additional Hours (please give details)", workload_extra.notes AS "Additional details", 'Feedback'::character varying AS ro_header_7, workload_extra.process_notes AS "Comments on this process" FROM (public.person LEFT JOIN public.workload_extra ON ((person.id = workload_extra.person_id))) WHERE ((person.crsid)::name = "current_user"());


ALTER TABLE hotwire."10_Workload/My_Workload_Summary" OWNER TO rl201;

--
-- Name: 10_Workload/My_Workload//Departmental_Supervising del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Departmental_Supervising" DO INSTEAD DELETE FROM public.workload_supervising WHERE ((workload_supervising.id = (old.id)::integer) AND (workload_supervising.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Broken_Outreach del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Broken_Outreach" DO INSTEAD DELETE FROM public.workload_outreach WHERE ((workload_outreach.id = (old.id)::integer) AND (workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Postgraduate_Examining" DO INSTEAD DELETE FROM public.workload_examining WHERE ((workload_examining.id = (old.id)::integer) AND (workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Research del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Research" DO INSTEAD DELETE FROM public.workload_research WHERE ((workload_research.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))) AND (workload_research.id = (old.id)::integer));


--
-- Name: 10_Workload/My_Workload//Research_Group del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Research_Group" DO INSTEAD DELETE FROM public.workload_students WHERE ((workload_students.id = (old.id)::integer) AND (workload_students.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Undergraduate_Examining" DO INSTEAD DELETE FROM public.workload_examining WHERE ((workload_examining.id = (old.id)::integer) AND (workload_examining.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//University_Committees del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//University_Committees" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE ((workload_committee_membership.id = (old.id)::integer) AND (workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Outreach del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Outreach" DO INSTEAD DELETE FROM public.workload_outreach WHERE ((workload_outreach.id = (old.id)::integer) AND (workload_outreach.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" DO INSTEAD DELETE FROM public.workload_lecturing WHERE ((workload_lecturing.lecture_course_id = old.postgraduate_lecture_course_id) AND (workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE del AS ON DELETE TO hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" DO INSTEAD DELETE FROM public.workload_lecturing WHERE ((workload_lecturing.lecture_course_id = old.lecture_course_id) AND (workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Research dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/Everybody/Additional Notes hotwire_10_Workload/Additional Notes_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Additional Notes_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Additional Notes" DO INSTEAD DELETE FROM public.workload_extra WHERE (workload_extra.id = old.id);


--
-- Name: 10_Workload/Everybody/Additional Notes hotwire_10_Workload/Additional Notes_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Additional Notes_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Additional Notes" DO INSTEAD INSERT INTO public.workload_extra (person_id, hours, notes, process_notes) VALUES (new.person_id, new.hours, new.notes, new.process_notes) RETURNING workload_extra.id, workload_extra.person_id, workload_extra.hours, workload_extra.notes, workload_extra.process_notes;


--
-- Name: 10_Workload/Everybody/Additional Notes hotwire_10_Workload/Additional Notes_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Additional Notes_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Additional Notes" DO INSTEAD UPDATE public.workload_extra SET id = new.id, person_id = new.person_id, hours = new.hours, notes = new.notes, process_notes = new.process_notes WHERE (workload_extra.id = old.id);


--
-- Name: 10_Workload/Admin/College types hotwire_10_Workload/Admin/College types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/College types_del" AS ON DELETE TO hotwire."10_Workload/Admin/College types" DO INSTEAD DELETE FROM public.workload_college_type WHERE (workload_college_type.id = old.id);


--
-- Name: 10_Workload/Admin/College types hotwire_10_Workload/Admin/College types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/College types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/College types" DO INSTEAD INSERT INTO public.workload_college_type (name, weight, hourly) VALUES (new.name, new.weight, new.hourly) RETURNING workload_college_type.id, workload_college_type.name, workload_college_type.weight, workload_college_type.hourly;


--
-- Name: 10_Workload/Admin/College types hotwire_10_Workload/Admin/College types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/College types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/College types" DO INSTEAD UPDATE public.workload_college_type SET id = new.id, name = new.name, weight = new.weight, hourly = new.hourly WHERE (workload_college_type.id = old.id);


--
-- Name: 10_Workload/Admin/Examining Roles hotwire_10_Workload/Admin/Examining types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Examining types_del" AS ON DELETE TO hotwire."10_Workload/Admin/Examining Roles" DO INSTEAD DELETE FROM public.workload_examining_role WHERE (workload_examining_role.id = old.id);


--
-- Name: 10_Workload/Admin/Examining Roles hotwire_10_Workload/Admin/Examining types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Examining types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Examining Roles" DO INSTEAD INSERT INTO public.workload_examining_role (name, ucam, departmental, postgrad, weight) VALUES (new.name, new.internal, new.departmental, new.postgrad, new.weight) RETURNING workload_examining_role.id, workload_examining_role.name, workload_examining_role.ucam, workload_examining_role.departmental, workload_examining_role.postgrad, workload_examining_role.weight, workload_examining_role.js_extra;


--
-- Name: 10_Workload/Admin/Examining Roles hotwire_10_Workload/Admin/Examining types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Examining types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Examining Roles" DO INSTEAD UPDATE public.workload_examining_role SET id = new.id, name = new.name, ucam = new.internal, departmental = new.departmental, postgrad = new.postgrad, weight = new.weight WHERE (workload_examining_role.id = old.id);


--
-- Name: 10_Workload/Admin/Grants_Summary hotwire_10_Workload/Admin/Grants_Summary_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Grants_Summary_del" AS ON DELETE TO hotwire."10_Workload/Admin/Grants_Summary" DO INSTEAD DELETE FROM public.workload_grants WHERE (workload_grants.id = old.id);


--
-- Name: 10_Workload/Admin/Grants_Summary hotwire_10_Workload/Admin/Grants_Summary_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Grants_Summary_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Grants_Summary" DO INSTEAD INSERT INTO public.workload_grants (person_id, big_grants, small_grants) VALUES (new.person_id, new.big_grants, new.small_grants) RETURNING workload_grants.id, workload_grants.person_id, workload_grants.big_grants, workload_grants.small_grants;


--
-- Name: 10_Workload/Admin/Grants_Summary hotwire_10_Workload/Admin/Grants_Summary_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Grants_Summary_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Grants_Summary" DO INSTEAD UPDATE public.workload_grants SET id = new.id, person_id = new.person_id, big_grants = new.big_grants, small_grants = new.small_grants WHERE (workload_grants.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/Lecture Courses hotwire_10_Workload/Admin/Internal/Lecture Courses_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Lecture Courses_del" AS ON DELETE TO hotwire."10_Workload/Admin/Internal/Lecture Courses" DO INSTEAD DELETE FROM public.lecture_course WHERE (lecture_course.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/Lecture Courses hotwire_10_Workload/Admin/Internal/Lecture Courses_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Lecture Courses_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Internal/Lecture Courses" DO INSTEAD INSERT INTO public.lecture_course (name, tripos_id, active) VALUES (new.name, new.examination_id, new.active);


--
-- Name: 10_Workload/Admin/Internal/Lecture Courses hotwire_10_Workload/Admin/Internal/Lecture Courses_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Lecture Courses_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Internal/Lecture Courses" DO INSTEAD UPDATE public.lecture_course SET id = new.id, name = new.name, tripos_id = new.examination_id, active = new.active WHERE (lecture_course.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/New or Old Lecture Course Options hotwire_10_Workload/Admin/Internal/NewOldLecture_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/NewOldLecture_del" AS ON DELETE TO hotwire."10_Workload/Admin/Internal/New or Old Lecture Course Options" DO INSTEAD DELETE FROM public.workload_lecturing_type WHERE (workload_lecturing_type.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/New or Old Lecture Course Options hotwire_10_Workload/Admin/Internal/NewOldLecture_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/NewOldLecture_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Internal/New or Old Lecture Course Options" DO INSTEAD INSERT INTO public.workload_lecturing_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_lecturing_type.id, workload_lecturing_type.name, workload_lecturing_type.weight;


--
-- Name: 10_Workload/Admin/Internal/New or Old Lecture Course Options hotwire_10_Workload/Admin/Internal/NewOldLecture_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/NewOldLecture_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Internal/New or Old Lecture Course Options" DO INSTEAD UPDATE public.workload_lecturing_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_lecturing_type.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/Practical Classes hotwire_10_Workload/Admin/Internal/Practical Classes_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Practical Classes_del" AS ON DELETE TO hotwire."10_Workload/Admin/Internal/Practical Classes" DO INSTEAD DELETE FROM public.lecture_course WHERE (lecture_course.id = old.id);


--
-- Name: 10_Workload/Admin/Internal/Practical Classes hotwire_10_Workload/Admin/Internal/Practical Classes_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Practical Classes_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Internal/Practical Classes" DO INSTEAD INSERT INTO public.lecture_course (name, tripos_id, active, is_practicals) VALUES (new.name, new.tripos_id, new.active, true) RETURNING lecture_course.id, lecture_course.name, lecture_course.tripos_id, lecture_course.active;


--
-- Name: 10_Workload/Admin/Internal/Practical Classes hotwire_10_Workload/Admin/Internal/Practical Classes_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Internal/Practical Classes_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Internal/Practical Classes" DO INSTEAD UPDATE public.lecture_course SET id = new.id, name = new.name, tripos_id = new.tripos_id, active = new.active WHERE (lecture_course.id = old.id);


--
-- Name: 10_Workload/Admin/Marking Roles hotwire_10_Workload/Admin/Marking Roles_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Marking Roles_del" AS ON DELETE TO hotwire."10_Workload/Admin/Marking Roles" DO INSTEAD DELETE FROM public.workload_marking_type WHERE (workload_marking_type.id = old.id);


--
-- Name: 10_Workload/Admin/Marking Roles hotwire_10_Workload/Admin/Marking Roles_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Marking Roles_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Marking Roles" DO INSTEAD INSERT INTO public.workload_marking_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_marking_type.id, workload_marking_type.name, workload_marking_type.weight;


--
-- Name: 10_Workload/Admin/Marking Roles hotwire_10_Workload/Admin/Marking Roles_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Marking Roles_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Marking Roles" DO INSTEAD UPDATE public.workload_marking_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_marking_type.id = old.id);


--
-- Name: 10_Workload/Admin/Misc_Weighting_Factors hotwire_10_Workload/Admin/Misc_Weighting_Factors_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Misc_Weighting_Factors_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Misc_Weighting_Factors" DO INSTEAD UPDATE public.workload_weights SET weight = new.weight WHERE (workload_weights.id = old.id);


--
-- Name: 10_Workload/Admin/Outreach Types hotwire_10_Workload/Admin/Outreach Types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Outreach Types_del" AS ON DELETE TO hotwire."10_Workload/Admin/Outreach Types" DO INSTEAD DELETE FROM public.workload_outreach_type WHERE (workload_outreach_type.id = old.id);


--
-- Name: 10_Workload/Admin/Outreach Types hotwire_10_Workload/Admin/Outreach Types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Outreach Types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Outreach Types" DO INSTEAD INSERT INTO public.workload_outreach_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_outreach_type.id, workload_outreach_type.name, workload_outreach_type.weight;


--
-- Name: 10_Workload/Admin/Outreach Types hotwire_10_Workload/Admin/Outreach Types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Outreach Types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Outreach Types" DO INSTEAD UPDATE public.workload_outreach_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_outreach_type.id = old.id);


--
-- Name: 10_Workload/Admin/Research types hotwire_10_Workload/Admin/Research types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Research types_del" AS ON DELETE TO hotwire."10_Workload/Admin/Research types" DO INSTEAD DELETE FROM public.workload_research_type WHERE (workload_research_type.id = old.id);


--
-- Name: 10_Workload/Admin/Research types hotwire_10_Workload/Admin/Research types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Research types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Research types" DO INSTEAD INSERT INTO public.workload_research_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_research_type.id, workload_research_type.name, workload_research_type.weight;


--
-- Name: 10_Workload/Admin/Research types hotwire_10_Workload/Admin/Research types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Research types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Research types" DO INSTEAD UPDATE public.workload_research_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_research_type.id = old.id);


--
-- Name: 10_Workload/Admin/Personnel types hotwire_10_Workload/Admin/Student types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Student types_del" AS ON DELETE TO hotwire."10_Workload/Admin/Personnel types" DO INSTEAD DELETE FROM public.workload_student_type WHERE (workload_student_type.id = old.id);


--
-- Name: 10_Workload/Admin/Personnel types hotwire_10_Workload/Admin/Student types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Student types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Personnel types" DO INSTEAD INSERT INTO public.workload_student_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_student_type.id, workload_student_type.name, workload_student_type.weight;


--
-- Name: 10_Workload/Admin/Personnel types hotwire_10_Workload/Admin/Student types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Student types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Personnel types" DO INSTEAD UPDATE public.workload_student_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_student_type.id = old.id);


--
-- Name: 10_Workload/Admin/Supervising Roles hotwire_10_Workload/Admin/Supervising Roles_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervising Roles_del" AS ON DELETE TO hotwire."10_Workload/Admin/Supervising Roles" DO INSTEAD DELETE FROM public.workload_supervising_type WHERE (workload_supervising_type.id = old.id);


--
-- Name: 10_Workload/Admin/Supervising Roles hotwire_10_Workload/Admin/Supervising Roles_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervising Roles_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Supervising Roles" DO INSTEAD INSERT INTO public.workload_supervising_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_supervising_type.id, workload_supervising_type.name, workload_supervising_type.weight;


--
-- Name: 10_Workload/Admin/Supervising Roles hotwire_10_Workload/Admin/Supervising Roles_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervising Roles_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Supervising Roles" DO INSTEAD UPDATE public.workload_supervising_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_supervising_type.id = old.id);


--
-- Name: 10_Workload/Admin/Supervision Types hotwire_10_Workload/Admin/Supervision Types_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervision Types_del" AS ON DELETE TO hotwire."10_Workload/Admin/Supervision Types" DO INSTEAD DELETE FROM public.workload_supervising_type WHERE (workload_supervising_type.id = old.id);


--
-- Name: 10_Workload/Admin/Supervision Types hotwire_10_Workload/Admin/Supervision Types_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervision Types_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Supervision Types" DO INSTEAD INSERT INTO public.workload_supervising_type (name, weight) VALUES (new.name, new.weight) RETURNING workload_supervising_type.id, workload_supervising_type.name, workload_supervising_type.weight;


--
-- Name: 10_Workload/Admin/Supervision Types hotwire_10_Workload/Admin/Supervision Types_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Admin/Supervision Types_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Supervision Types" DO INSTEAD UPDATE public.workload_supervising_type SET id = new.id, name = new.name, weight = new.weight WHERE (workload_supervising_type.id = old.id);


--
-- Name: 10_Workload/Everybody/College hotwire_10_Workload/College_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_del" AS ON DELETE TO hotwire."10_Workload/Everybody/College" DO INSTEAD DELETE FROM public.workload_college WHERE (workload_college.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/College hotwire_10_Workload/College_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/College" DO INSTEAD DELETE FROM public.workload_college WHERE (workload_college.id = old.id);


--
-- Name: 10_Workload/Everybody/College hotwire_10_Workload/College_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/College" DO INSTEAD INSERT INTO public.workload_college (workload_college_type_id, person_id, hours) VALUES (new.workload_college_type_id, new.person_id, new.hours) RETURNING workload_college.id, workload_college.workload_college_type_id, workload_college.person_id, workload_college.hours;


--
-- Name: 10_Workload/Former_My_Workload/College hotwire_10_Workload/College_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/College" DO INSTEAD INSERT INTO public.workload_college (workload_college_type_id, person_id, hours) VALUES (new.workload_college_type_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours) RETURNING workload_college.id, workload_college.workload_college_type_id, workload_college.hours;


--
-- Name: 10_Workload/Everybody/College hotwire_10_Workload/College_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/College" DO INSTEAD UPDATE public.workload_college SET id = new.id, workload_college_type_id = new.workload_college_type_id, person_id = new.person_id, hours = new.hours WHERE (workload_college.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/College hotwire_10_Workload/College_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/College_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/College" DO INSTEAD UPDATE public.workload_college SET id = new.id, workload_college_type_id = new.workload_college_type_id, hours = new.hours WHERE (workload_college.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Committee_Membership" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Committee_Membership" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, notes) VALUES (new.departmental_committee_id, new.person_id, new.hours, new."Chair or Secretary", new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.person_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.notes;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, secretary, notes) VALUES (new.departmental_committee_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours, new.chair, new.secretary, new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.secretary, workload_committee_membership.notes;


--
-- Name: 10_Workload/Everybody/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Committee_Membership" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.departmental_committee_id, person_id = new.person_id, hours = new.hours, chair = new."Chair or Secretary", notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Committee_Membership hotwire_10_Workload/Departmental Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.departmental_committee_id, hours = new.hours, chair = new.chair, secretary = new.secretary, notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/Departmental hotwire_10_Workload/Departmental Committees_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committees_del" AS ON DELETE TO hotwire."10_Workload/Admin/Committees/Departmental" DO INSTEAD DELETE FROM public.workload_committee WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/Departmental hotwire_10_Workload/Departmental Committees_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committees_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Committees/Departmental" DO INSTEAD INSERT INTO public.workload_committee (name, active, weight, weight_chair, weight_secretary) VALUES (new.name, new.active, new.weight, new.weight_chair, new.weight_secretary) RETURNING workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight, workload_committee.weight_chair, workload_committee.weight_secretary;


--
-- Name: 10_Workload/Admin/Committees/Departmental hotwire_10_Workload/Departmental Committees_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Departmental Committees_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Committees/Departmental" DO INSTEAD UPDATE public.workload_committee SET id = new.id, name = new.name, active = new.active, weight = new.weight, weight_chair = new.weight_chair, weight_secretary = new.weight_secretary WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Consulting_and_Charities hotwire_10_Workload/Everybody/External/Consulting_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/External/Consulting_del" AS ON DELETE TO hotwire."10_Workload/Everybody/External/Consulting_and_Charities" DO INSTEAD DELETE FROM public.workload_external_consulting WHERE (workload_external_consulting.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Consulting_and_Charities hotwire_10_Workload/Everybody/External/Consulting_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/External/Consulting_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/External/Consulting_and_Charities" DO INSTEAD INSERT INTO public.workload_external_consulting (name, role, hours, notes, person_id) VALUES (new.name, new.role, new.hours, new.notes, new.person_id) RETURNING workload_external_consulting.id, workload_external_consulting.name, workload_external_consulting.role, workload_external_consulting.hours, workload_external_consulting.notes, workload_external_consulting.person_id;


--
-- Name: 10_Workload/Everybody/External/Consulting_and_Charities hotwire_10_Workload/Everybody/External/Consulting_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/External/Consulting_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/External/Consulting_and_Charities" DO INSTEAD UPDATE public.workload_external_consulting SET id = new.id, name = new.name, role = new.role, hours = new.hours, notes = new.notes, person_id = new.person_id WHERE (workload_external_consulting.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Laboratory Demonstrating hotwire_10_Workload/Everybody/Internal/Demonstrating_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Demonstrating_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" DO INSTEAD DELETE FROM public.workload_demonstrating WHERE (workload_demonstrating.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Laboratory Demonstrating hotwire_10_Workload/Everybody/Internal/Demonstrating_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Demonstrating_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" DO INSTEAD INSERT INTO public.workload_demonstrating (person_id, academic_year, practical_course_id, hours) VALUES (new.person_id, new.academic_year, new.practical_course_id, new.hours) RETURNING workload_demonstrating.id, workload_demonstrating.person_id, workload_demonstrating.academic_year, workload_demonstrating.practical_course_id, workload_demonstrating.hours;


--
-- Name: 10_Workload/Everybody/Internal/Laboratory Demonstrating hotwire_10_Workload/Everybody/Internal/Demonstrating_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Demonstrating_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" DO INSTEAD UPDATE public.workload_demonstrating SET id = new.id, person_id = new.person_id, academic_year = new.academic_year, practical_course_id = new.practical_course_id, hours = new.hours WHERE (workload_demonstrating.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Lecturing hotwire_10_Workload/Everybody/Internal/Lecturing_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Lecturing_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Lecturing" DO INSTEAD DELETE FROM public.workload_lecturing WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Lecturing hotwire_10_Workload/Everybody/Internal/Lecturing_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Lecturing_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Lecturing" DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, tripos_id, lecture_course_type_id) VALUES (new.person_id, new.lecture_course_id, new.hours, new.tripos_id, new."First_Time_id") RETURNING workload_lecturing.id, workload_lecturing.person_id, workload_lecturing.lecture_course_id, workload_lecturing.hours, workload_lecturing.tripos_id, workload_lecturing.lecture_course_type_id;


--
-- Name: 10_Workload/Everybody/Internal/Lecturing hotwire_10_Workload/Everybody/Internal/Lecturing_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Everybody/Internal/Lecturing_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Lecturing" DO INSTEAD UPDATE public.workload_lecturing SET id = new.id, person_id = new.person_id, lecture_course_id = new.lecture_course_id, hours = new.hours, tripos_id = new.tripos_id, lecture_course_type_id = new."First_Time_id" WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Committee_Membership hotwire_10_Workload/External Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Everybody/External/Committee_Membership" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Committee_Membership hotwire_10_Workload/External Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Committee_Membership hotwire_10_Workload/External Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/External/Committee_Membership" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, notes) VALUES (new.external_committee_id, new.person_id, new.hours, new."Chair or Secretary", new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.person_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.notes;


--
-- Name: 10_Workload/Former_My_Workload/External/Committee_Membership hotwire_10_Workload/External Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, secretary, notes) VALUES (new.external_committee_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours, new.chair, new.secretary, new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.secretary, workload_committee_membership.notes;


--
-- Name: 10_Workload/Everybody/External/Committee_Membership hotwire_10_Workload/External Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/External/Committee_Membership" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.external_committee_id, person_id = new.person_id, hours = new.hours, chair = new."Chair or Secretary", notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Committee_Membership hotwire_10_Workload/External Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.external_committee_id, hours = new.hours, chair = new.chair, secretary = new.secretary, notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/External hotwire_10_Workload/External Committees_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committees_del" AS ON DELETE TO hotwire."10_Workload/Admin/Committees/External" DO INSTEAD DELETE FROM public.workload_committee WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/External hotwire_10_Workload/External Committees_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committees_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Committees/External" DO INSTEAD INSERT INTO public.workload_committee (name, active, weight) VALUES (new.name, new.active, new.weight) RETURNING workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight;


--
-- Name: 10_Workload/Admin/Committees/External hotwire_10_Workload/External Committees_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/External Committees_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Committees/External" DO INSTEAD UPDATE public.workload_committee SET id = new.id, name = new.name, active = new.active, weight = new.weight WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Outreach hotwire_10_Workload/Internal/Outreach_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Outreach" DO INSTEAD DELETE FROM public.workload_outreach WHERE (workload_outreach.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Outreach hotwire_10_Workload/Internal/Outreach_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Outreach" DO INSTEAD DELETE FROM public.workload_outreach WHERE (workload_outreach.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Outreach hotwire_10_Workload/Internal/Outreach_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Outreach" DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.hours, new.notes, new.person_id) RETURNING workload_outreach.id, workload_outreach.outreach_type_id, workload_outreach.hours, workload_outreach.notes, workload_outreach.person_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Outreach hotwire_10_Workload/Internal/Outreach_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Outreach" DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.hours, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))) RETURNING workload_outreach.id, workload_outreach.outreach_type_id, workload_outreach.hours, workload_outreach.notes;


--
-- Name: 10_Workload/Everybody/Internal/Outreach hotwire_10_Workload/Internal/Outreach_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Outreach" DO INSTEAD UPDATE public.workload_outreach SET id = new.id, outreach_type_id = new.outreach_type_id, hours = new.hours, notes = new.notes, person_id = new.person_id WHERE (workload_outreach.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Outreach hotwire_10_Workload/Internal/Outreach_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Outreach_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Outreach" DO INSTEAD UPDATE public.workload_outreach SET id = new.id, outreach_type_id = new.outreach_type_id, hours = new.hours, notes = new.notes WHERE (workload_outreach.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Research hotwire_10_Workload/Internal/Research_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Research" DO INSTEAD DELETE FROM public.workload_research WHERE (workload_research.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research hotwire_10_Workload/Internal/Research_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Research" DO INSTEAD DELETE FROM public.workload_research WHERE (workload_research.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Research hotwire_10_Workload/Internal/Research_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Research" DO INSTEAD INSERT INTO public.workload_research (person_id, workload_research_type_id, number) VALUES (new.person_id, new.research_type_id, new.number) RETURNING workload_research.id, workload_research.person_id, workload_research.workload_research_type_id, workload_research.number;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research hotwire_10_Workload/Internal/Research_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Research" DO INSTEAD INSERT INTO public.workload_research (person_id, workload_research_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.research_type_id, new.number) RETURNING workload_research.id, workload_research.workload_research_type_id, workload_research.number;


--
-- Name: 10_Workload/Everybody/Internal/Research hotwire_10_Workload/Internal/Research_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Research" DO INSTEAD UPDATE public.workload_research SET id = new.id, person_id = new.person_id, workload_research_type_id = new.research_type_id, number = new.number WHERE (workload_research.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research hotwire_10_Workload/Internal/Research_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Research_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Research" DO INSTEAD UPDATE public.workload_research SET id = new.id, workload_research_type_id = new.research_type_id, number = new.number WHERE (workload_research.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Students hotwire_10_Workload/Internal/Students_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Students" DO INSTEAD DELETE FROM public.workload_students WHERE (workload_students.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research_Group_Personne hotwire_10_Workload/Internal/Students_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" DO INSTEAD DELETE FROM public.workload_students WHERE (workload_students.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Students hotwire_10_Workload/Internal/Students_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Students" DO INSTEAD INSERT INTO public.workload_students (person_id, student_type_id, number) VALUES (new.person_id, new.student_type_id, new.number) RETURNING workload_students.id, workload_students.person_id, workload_students.student_type_id, workload_students.number;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research_Group_Personne hotwire_10_Workload/Internal/Students_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" DO INSTEAD INSERT INTO public.workload_students (person_id, student_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.personnel_type_id, new.number) RETURNING workload_students.id, workload_students.student_type_id, workload_students.number;


--
-- Name: 10_Workload/Everybody/Internal/Students hotwire_10_Workload/Internal/Students_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Students" DO INSTEAD UPDATE public.workload_students SET id = new.id, person_id = new.person_id, student_type_id = new.student_type_id, number = new.number WHERE (workload_students.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Research_Group_Personne hotwire_10_Workload/Internal/Students_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Students_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" DO INSTEAD UPDATE public.workload_students SET id = new.id, student_type_id = new.personnel_type_id, number = new.number WHERE (workload_students.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Departmental Supervisions hotwire_10_Workload/Internal/Supervising_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" DO INSTEAD DELETE FROM public.workload_supervising WHERE (workload_supervising.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Departmental Supervisio hotwire_10_Workload/Internal/Supervising_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" DO INSTEAD DELETE FROM public.workload_supervising WHERE (workload_supervising.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Departmental Supervisions hotwire_10_Workload/Internal/Supervising_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" DO INSTEAD INSERT INTO public.workload_supervising (person_id, tripos_id, hours, supervision_type_id) VALUES (new.person_id, new.tripos_id, new.hours, new.supervision_type_id) RETURNING workload_supervising.id, workload_supervising.person_id, workload_supervising.tripos_id, workload_supervising.hours, workload_supervising.supervision_type_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Departmental Supervisio hotwire_10_Workload/Internal/Supervising_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" DO INSTEAD INSERT INTO public.workload_supervising (person_id, tripos_id, hours, supervision_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.dept_supervision_tripos_id, new.hours, new.supervision_type_id) RETURNING workload_supervising.id, workload_supervising.tripos_id, workload_supervising.hours, workload_supervising.supervision_type_id;


--
-- Name: 10_Workload/Everybody/Internal/Departmental Supervisions hotwire_10_Workload/Internal/Supervising_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" DO INSTEAD UPDATE public.workload_supervising SET id = new.id, person_id = new.person_id, tripos_id = new.tripos_id, hours = new.hours, supervision_type_id = new.supervision_type_id WHERE (workload_supervising.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Departmental Supervisio hotwire_10_Workload/Internal/Supervising_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Internal/Supervising_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" DO INSTEAD UPDATE public.workload_supervising SET id = new.id, tripos_id = new.dept_supervision_tripos_id, hours = new.hours, supervision_type_id = new.supervision_type_id WHERE (workload_supervising.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Consulting_and_Charitie hotwire_10_Workload/My_Workload/External/Consulting_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/External/Consulting_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" DO INSTEAD DELETE FROM public.workload_external_consulting WHERE (workload_external_consulting.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Consulting_and_Charitie hotwire_10_Workload/My_Workload/External/Consulting_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/External/Consulting_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" DO INSTEAD INSERT INTO public.workload_external_consulting (name, role, hours, notes, person_id) VALUES (new.name, new.role, new.hours, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))) RETURNING workload_external_consulting.id, workload_external_consulting.name, workload_external_consulting.role, workload_external_consulting.hours, workload_external_consulting.notes;


--
-- Name: 10_Workload/Former_My_Workload/External/Consulting_and_Charitie hotwire_10_Workload/My_Workload/External/Consulting_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/External/Consulting_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" DO INSTEAD UPDATE public.workload_external_consulting SET id = new.id, name = new.name, role = new.role, hours = new.hours, notes = new.notes WHERE (workload_external_consulting.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin hotwire_10_Workload/My_Workload/Internal/Demonstrating_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Demonstrating_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" DO INSTEAD DELETE FROM public.workload_demonstrating WHERE (workload_demonstrating.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin hotwire_10_Workload/My_Workload/Internal/Demonstrating_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Demonstrating_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" DO INSTEAD INSERT INTO public.workload_demonstrating (person_id, practical_course_id, tripos_id, hours) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.practical_course_id, new.tripos_id, new.hours) RETURNING workload_demonstrating.id, workload_demonstrating.practical_course_id, workload_demonstrating.tripos_id, workload_demonstrating.hours;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin hotwire_10_Workload/My_Workload/Internal/Demonstrating_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Demonstrating_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" DO INSTEAD UPDATE public.workload_demonstrating SET id = new.id, practical_course_id = new.practical_course_id, hours = new.hours, tripos_id = new.tripos_id WHERE (workload_demonstrating.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing hotwire_10_Workload/My_Workload/Internal/Lecturing_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Lecturing_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" DO INSTEAD DELETE FROM public.workload_lecturing WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing hotwire_10_Workload/My_Workload/Internal/Lecturing_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Lecturing_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" DO INSTEAD INSERT INTO public.workload_lecturing (person_id, tripos_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.tripos_id, new.undergraduate_lecture_course_id, new.hours, new."First Time_id") RETURNING workload_lecturing.id, workload_lecturing.tripos_id, workload_lecturing.lecture_course_id, workload_lecturing.hours, workload_lecturing.lecture_course_type_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing hotwire_10_Workload/My_Workload/Internal/Lecturing_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/Lecturing_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" DO INSTEAD UPDATE public.workload_lecturing SET id = new.id, tripos_id = new.tripos_id, lecture_course_id = new.undergraduate_lecture_course_id, hours = new.hours, lecture_course_type_id = new."First Time_id" WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" DO INSTEAD DELETE FROM public.workload_lecturing WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.postgraduate_lecture_course_id, new.hours, new."First Time_id") RETURNING workload_lecturing.id, workload_lecturing.lecture_course_id, workload_lecturing.hours, workload_lecturing.lecture_course_type_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/My_Workload/Internal/PG_Lecturing_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" DO INSTEAD UPDATE public.workload_lecturing SET id = new.id, lecture_course_id = new.postgraduate_lecture_course_id, hours = new.hours, lecture_course_type_id = new."First Time_id" WHERE (workload_lecturing.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Examining hotwire_10_Workload/Postgraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Everybody/External/Examining" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Examining hotwire_10_Workload/Postgraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/External/Examining" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES (new.person_id, new.hours, new.internal_pg_examiner_role_id) RETURNING workload_examining.id, workload_examining.person_id, workload_examining.hours, workload_examining.role_id;


--
-- Name: 10_Workload/Everybody/External/Examining hotwire_10_Workload/Postgraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/External/Examining" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES (new.person_id, new.hours, new.external_examiner_role_id) RETURNING workload_examining.id, workload_examining.person_id, workload_examining.hours, workload_examining.role_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours, new.internal_pg_examiner_role_id) RETURNING workload_examining.id, workload_examining.hours, workload_examining.role_id;


--
-- Name: 10_Workload/Former_My_Workload/External/Examining hotwire_10_Workload/Postgraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/External/Examining" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours, new.external_examiner_role_id) RETURNING workload_examining.id, workload_examining.role_id, workload_examining.hours;


--
-- Name: 10_Workload/Everybody/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" DO INSTEAD UPDATE public.workload_examining SET id = new.id, person_id = new.person_id, hours = new.hours, role_id = new.internal_pg_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/External/Examining hotwire_10_Workload/Postgraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/External/Examining" DO INSTEAD UPDATE public.workload_examining SET id = new.id, person_id = new.person_id, hours = new.hours, role_id = new.external_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Postgraduate_Exams hotwire_10_Workload/Postgraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" DO INSTEAD UPDATE public.workload_examining SET id = new.id, hours = new.hours, role_id = new.internal_pg_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/External/Examining hotwire_10_Workload/Postgraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Postgraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/External/Examining" DO INSTEAD UPDATE public.workload_examining SET id = new.id, hours = new.hours, role_id = new.external_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" DO INSTEAD DELETE FROM public.workload_examining WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" DO INSTEAD INSERT INTO public.workload_examining (person_id, examination_id, hours, role_id) VALUES (new.person_id, new.undergraduate_exam_id, new.hours, new.internal_ug_examiner_role_id) RETURNING workload_examining.id, workload_examining.person_id, workload_examining.examination_id, workload_examining.hours, workload_examining.role_id;


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" DO INSTEAD INSERT INTO public.workload_examining (person_id, examination_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.undergraduate_exam_id, new.hours, new.internal_ug_examiner_role_id) RETURNING workload_examining.id, workload_examining.examination_id, workload_examining.hours, workload_examining.role_id;


--
-- Name: 10_Workload/Everybody/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" DO INSTEAD UPDATE public.workload_examining SET id = new.id, person_id = new.person_id, examination_id = new.undergraduate_exam_id, hours = new.hours, role_id = new.internal_ug_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/Internal/Undergraduate_Exams hotwire_10_Workload/Undergraduate Exams_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/Undergraduate Exams_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" DO INSTEAD UPDATE public.workload_examining SET id = new.id, examination_id = new.undergraduate_exam_id, hours = new.hours, role_id = new.internal_ug_examiner_role_id WHERE (workload_examining.id = old.id);


--
-- Name: 10_Workload/Everybody/University_Committees hotwire_10_Workload/University Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Everybody/University_Committees" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/University_Committees hotwire_10_Workload/University Committee Membership_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_del" AS ON DELETE TO hotwire."10_Workload/Former_My_Workload/University_Committees" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Everybody/University_Committees hotwire_10_Workload/University Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Everybody/University_Committees" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, notes) VALUES (new.university_committee_id, new.person_id, new.hours, new."Chair or Secretary", new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.person_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.notes;


--
-- Name: 10_Workload/Former_My_Workload/University_Committees hotwire_10_Workload/University Committee Membership_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_ins" AS ON INSERT TO hotwire."10_Workload/Former_My_Workload/University_Committees" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, person_id, hours, chair, notes) VALUES (new.university_committee_id, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.hours, new."Chair or Secretary", new.notes) RETURNING workload_committee_membership.id, workload_committee_membership.workload_committee_id, workload_committee_membership.hours, workload_committee_membership.chair, workload_committee_membership.notes;


--
-- Name: 10_Workload/Everybody/University_Committees hotwire_10_Workload/University Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Everybody/University_Committees" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.university_committee_id, person_id = new.person_id, hours = new.hours, chair = new."Chair or Secretary", notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Former_My_Workload/University_Committees hotwire_10_Workload/University Committee Membership_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committee Membership_upd" AS ON UPDATE TO hotwire."10_Workload/Former_My_Workload/University_Committees" DO INSTEAD UPDATE public.workload_committee_membership SET id = new.id, workload_committee_id = new.university_committee_id, hours = new.hours, chair = new."Chair or Secretary", notes = new.notes WHERE (workload_committee_membership.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/University hotwire_10_Workload/University Committees_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committees_del" AS ON DELETE TO hotwire."10_Workload/Admin/Committees/University" DO INSTEAD DELETE FROM public.workload_committee WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/Admin/Committees/University hotwire_10_Workload/University Committees_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committees_ins" AS ON INSERT TO hotwire."10_Workload/Admin/Committees/University" DO INSTEAD INSERT INTO public.workload_committee (name, active, weight) VALUES (new.name, new.active, new.weight) RETURNING workload_committee.id, workload_committee.name, workload_committee.active, workload_committee.weight;


--
-- Name: 10_Workload/Admin/Committees/University hotwire_10_Workload/University Committees_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE "hotwire_10_Workload/University Committees_upd" AS ON UPDATE TO hotwire."10_Workload/Admin/Committees/University" DO INSTEAD UPDATE public.workload_committee SET id = new.id, name = new.name, active = new.active, weight = new.weight WHERE (workload_committee.id = old.id);


--
-- Name: 10_Workload/My_Workload//College_Old hotwire_10_workload_my_summary_college_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_del AS ON DELETE TO hotwire."10_Workload/My_Workload//College_Old" DO INSTEAD DELETE FROM public.workload_college WHERE ((workload_college.workload_college_type_id = (old.id)::integer) AND (workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_del AS ON DELETE TO hotwire."10_Workload/My_Workload//College" DO INSTEAD DELETE FROM public.workload_college WHERE ((workload_college.id = (old.id)::integer) AND (workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = (("current_user"())::character varying)::text) LIMIT 1)));


--
-- Name: 10_Workload/My_Workload//College_Old hotwire_10_workload_my_summary_college_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_ins AS ON INSERT TO hotwire."10_Workload/My_Workload//College_Old" DO INSTEAD INSERT INTO public.workload_college (workload_college_type_id, person_id, hours) VALUES ((ltrim(new.id, 'NEW'::text))::integer, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload);


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_ins AS ON INSERT TO hotwire."10_Workload/My_Workload//College" DO INSTEAD INSERT INTO public.workload_college (id, workload_college_type_id, person_id, hours) VALUES (DEFAULT, (ltrim(new.id, 'NEW'::text))::integer, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload) RETURNING (workload_college.id)::text AS id, workload_college.workload_college_type_id, (SELECT workload_college_type.name FROM public.workload_college_type WHERE (workload_college_type.id = workload_college.workload_college_type_id)), workload_college.hours;


--
-- Name: 10_Workload/My_Workload//College_Old hotwire_10_workload_my_summary_college_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd AS ON UPDATE TO hotwire."10_Workload/My_Workload//College_Old" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_college (workload_college_type_id, person_id, hours) VALUES ((ltrim(new.id, 'NEW'::text))::integer, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload);


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd AS ON UPDATE TO hotwire."10_Workload/My_Workload//College" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_college (workload_college_type_id, person_id, hours) VALUES ((ltrim(new.id, 'NEW'::text))::integer, (SELECT person.id FROM public.person WHERE ((person.crsid)::text = (("current_user"())::character varying)::text)), new.workload);


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_upd19; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd19 AS ON UPDATE TO hotwire."10_Workload/My_Workload//College" WHERE ((new.workload IS NULL) AND (substr(new.id, 1, 3) <> 'NEW'::text)) DO INSTEAD DELETE FROM public.workload_college WHERE ((workload_college.id = (old.id)::integer) AND (workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = (("current_user"())::character varying)::text))));


--
-- Name: 10_Workload/My_Workload//College_Old hotwire_10_workload_my_summary_college_upd2; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd2 AS ON UPDATE TO hotwire."10_Workload/My_Workload//College_Old" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_college SET hours = new.workload WHERE ((workload_college.workload_college_type_id = old.workload_college_type_id) AND (workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_upd2; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd2 AS ON UPDATE TO hotwire."10_Workload/My_Workload//College" WHERE ((new.workload IS NOT NULL) AND (substr(new.id, 1, 3) <> 'NEW'::text)) DO INSTEAD UPDATE public.workload_college SET hours = new.workload WHERE ((workload_college.workload_college_type_id = old.workload_college_type_id) AND (workload_college.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::text = (("current_user"())::character varying)::text))));


--
-- Name: 10_Workload/My_Workload//College_Old hotwire_10_workload_my_summary_college_upd3; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd3 AS ON UPDATE TO hotwire."10_Workload/My_Workload//College_Old" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//College hotwire_10_workload_my_summary_college_upd3; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_college_upd3 AS ON UPDATE TO hotwire."10_Workload/My_Workload//College" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Consulting hotwire_10_workload_my_summary_consulting_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_consulting_del AS ON DELETE TO hotwire."10_Workload/My_Workload//Consulting" DO INSTEAD DELETE FROM public.workload_external_consulting WHERE ((workload_external_consulting.id = (old.id)::integer) AND (workload_external_consulting.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Consulting hotwire_10_workload_my_summary_consulting_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_consulting_ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Consulting" DO INSTEAD INSERT INTO public.workload_external_consulting (name, role, hours, notes, person_id) VALUES (new.name, new.role, new.hours, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Consulting hotwire_10_workload_my_summary_consulting_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_consulting_upd AS ON UPDATE TO hotwire."10_Workload/My_Workload//Consulting" WHERE (new.id = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_external_consulting (name, role, hours, notes, person_id) VALUES (new.name, new.role, new.hours, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Consulting hotwire_10_workload_my_summary_consulting_upd2; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_consulting_upd2 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Consulting" WHERE (new.id <> 'NEW'::text) DO INSTEAD UPDATE public.workload_external_consulting SET name = new.name, role = new.role, hours = new.hours, notes = new.notes WHERE ((workload_external_consulting.id = (old.id)::integer) AND (workload_external_consulting.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Consulting hotwire_10_workload_my_summary_consulting_upd3; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_consulting_upd3 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Consulting" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Committees hotwire_10_workload_my_summary_departmental_committees_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_committees_del AS ON DELETE TO hotwire."10_Workload/My_Workload//Departmental_Committees" DO INSTEAD DELETE FROM public.workload_committee_membership WHERE ((workload_committee_membership.workload_committee_id = (old.id)::integer) AND (workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Committees hotwire_10_workload_my_summary_departmental_committees_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_committees_ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Departmental_Committees" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, hours, person_id) VALUES (new.departmental_committee_id, new.workload, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Departmental_Committees hotwire_10_workload_my_summary_departmental_committees_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_committees_upd AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Committees" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, hours, person_id) VALUES (new.departmental_committee_id, new.workload, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Departmental_Committees hotwire_10_workload_my_summary_departmental_committees_upd2; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_committees_upd2 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Committees" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_committee_membership SET hours = new.workload, workload_committee_id = new.departmental_committee_id WHERE ((workload_committee_membership.workload_committee_id = old.departmental_committee_id) AND (workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Committees hotwire_10_workload_my_summary_departmental_committees_upd3; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_committees_upd3 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Committees" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating hotwire_10_workload_my_summary_departmental_demonstrating_del; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_demonstrating_del AS ON DELETE TO hotwire."10_Workload/My_Workload//Departmental_Demonstrating" DO INSTEAD DELETE FROM public.workload_demonstrating WHERE ((workload_demonstrating.practical_course_id = (old.id)::integer) AND (workload_demonstrating.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating hotwire_10_workload_my_summary_departmental_demonstrating_ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_demonstrating_ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Departmental_Demonstrating" DO INSTEAD INSERT INTO public.workload_demonstrating (person_id, practical_course_id, hours, tripos_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.practical_course_id, new.workload, new.tripos_id);


--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating hotwire_10_workload_my_summary_departmental_demonstrating_upd; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_demonstrating_upd AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Demonstrating" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_demonstrating (person_id, practical_course_id, hours, tripos_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.practical_course_id, new.workload, new.tripos_id);


--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating hotwire_10_workload_my_summary_departmental_demonstrating_upd2; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_demonstrating_upd2 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Demonstrating" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_demonstrating SET practical_course_id = new.practical_course_id, hours = new.workload, tripos_id = new.tripos_id WHERE ((workload_demonstrating.id = (old.id)::integer) AND (workload_demonstrating.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Demonstrating hotwire_10_workload_my_summary_departmental_demonstrating_upd3; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE hotwire_10_workload_my_summary_departmental_demonstrating_upd3 AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Demonstrating" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Supervising ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Departmental_Supervising" DO INSTEAD INSERT INTO public.workload_supervising (person_id, tripos_id, hours, supervision_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.dept_supervision_tripos_id, new.workload, new.supervision_type_id);


--
-- Name: 10_Workload/My_Workload//Broken_Outreach ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Broken_Outreach" DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Postgraduate_Examining" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload, new.internal_pg_examiner_role_id);


--
-- Name: 10_Workload/My_Workload//Research ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Research" DO INSTEAD INSERT INTO public.workload_research (person_id, workload_research_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.research_type_id, new.workload);


--
-- Name: 10_Workload/My_Workload//Research_Group ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Research_Group" DO INSTEAD INSERT INTO public.workload_students (person_id, student_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.personnel_type_id, new.workload);


--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Undergraduate_Examining" DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload, new.internal_ug_examiner_role_id);


--
-- Name: 10_Workload/My_Workload//University_Committees ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//University_Committees" DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, hours, notes, person_id) VALUES (new.university_committee_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Outreach ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Outreach" DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.postgraduate_lecture_course_id, new.workload, new."First_Time_id");


--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing ins; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE ins AS ON INSERT TO hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.lecture_course_id, new.workload, new."First_Time_id");


--
-- Name: 10_Workload/My_Workload//Departmental_Supervising upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Supervising" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Broken_Outreach upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Broken_Outreach" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Postgraduate_Examining" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Research_Group upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research_Group" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Undergraduate_Examining" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//University_Committees upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//University_Committees" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Outreach upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Outreach" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing upd_dummy; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_dummy AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" DO INSTEAD NOTHING;


--
-- Name: 10_Workload/My_Workload//Departmental_Supervising upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Supervising" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_supervising (person_id, tripos_id, hours, supervision_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.dept_supervision_tripos_id, new.workload, new.supervision_type_id);


--
-- Name: 10_Workload/My_Workload//Broken_Outreach upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Broken_Outreach" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Postgraduate_Examining" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload, new.internal_pg_examiner_role_id);


--
-- Name: 10_Workload/My_Workload//Research upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_research (person_id, workload_research_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.research_type_id, new.workload);


--
-- Name: 10_Workload/My_Workload//Research_Group upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research_Group" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_students (person_id, student_type_id, number) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.personnel_type_id, new.workload);


--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Undergraduate_Examining" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_examining (person_id, hours, role_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.workload, new.internal_ug_examiner_role_id);


--
-- Name: 10_Workload/My_Workload//University_Committees upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//University_Committees" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_committee_membership (workload_committee_id, hours, notes, person_id) VALUES (new.university_committee_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Outreach upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Outreach" WHERE (substr((new.id)::text, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_outreach (outreach_type_id, hours, notes, person_id) VALUES (new.outreach_type_id, new.workload, new.notes, (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())));


--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.postgraduate_lecture_course_id, new.workload, new."First_Time_id");


--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing upd_new; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_new AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" WHERE (substr(new.id, 1, 3) = 'NEW'::text) DO INSTEAD INSERT INTO public.workload_lecturing (person_id, lecture_course_id, hours, lecture_course_type_id) VALUES ((SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())), new.lecture_course_id, new.workload, new."First_Time_id");


--
-- Name: 10_Workload/My_Workload//Departmental_Supervising upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Supervising" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_supervising SET tripos_id = new.dept_supervision_tripos_id, hours = new.workload, supervision_type_id = new.supervision_type_id WHERE (workload_supervising.id = (old.id)::integer);


--
-- Name: 10_Workload/My_Workload//Broken_Outreach upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Broken_Outreach" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_outreach SET outreach_type_id = new.outreach_type_id, hours = new.workload, notes = new.notes, person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()));


--
-- Name: 10_Workload/My_Workload//Postgraduate_Examining upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Postgraduate_Examining" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_examining SET hours = new.workload, role_id = new.internal_pg_examiner_role_id WHERE (workload_examining.id = (old.id)::integer);


--
-- Name: 10_Workload/My_Workload//Research upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_research SET workload_research_type_id = new.research_type_id, number = new.workload WHERE ((workload_research.id = (old.id)::integer) AND (workload_research.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Research_Group upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Research_Group" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_students SET student_type_id = new.personnel_type_id, number = new.workload WHERE ((workload_students.id = (old.id)::integer) AND (workload_students.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Undergraduate_Examining upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Undergraduate_Examining" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_examining SET hours = new.workload, role_id = new.internal_ug_examiner_role_id WHERE (workload_examining.id = (old.id)::integer);


--
-- Name: 10_Workload/My_Workload//University_Committees upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//University_Committees" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_committee_membership SET hours = new.workload, workload_committee_id = new.university_committee_id WHERE ((workload_committee_membership.id = (old.id)::integer) AND (workload_committee_membership.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Outreach upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Outreach" WHERE (substr((new.id)::text, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_outreach SET outreach_type_id = new.outreach_type_id, hours = new.workload, notes = new.notes, person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"())) WHERE (workload_outreach.id = (old.id)::integer);


--
-- Name: 10_Workload/My_Workload//Departmental_Postgraduate_Lecturing upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_lecturing SET lecture_course_id = new.postgraduate_lecture_course_id, lecture_course_type_id = new."First_Time_id", hours = new.workload WHERE ((workload_lecturing.id = (old.id)::integer) AND (workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: 10_Workload/My_Workload//Departmental_Undergraduate_Lecturing upd_old; Type: RULE; Schema: hotwire; Owner: rl201
--

CREATE RULE upd_old AS ON UPDATE TO hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" WHERE (substr(new.id, 1, 3) <> 'NEW'::text) DO INSTEAD UPDATE public.workload_lecturing SET lecture_course_id = new.lecture_course_id, lecture_course_type_id = new."First_Time_id", hours = new.workload WHERE ((workload_lecturing.id = (old.id)::integer) AND (workload_lecturing.person_id = (SELECT person.id FROM public.person WHERE ((person.crsid)::name = "current_user"()))));


--
-- Name: TABLE "10_Workload/Admin/College types"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/College types" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/College types" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/College types" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/College types" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Committees/Departmental"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/Departmental" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/Departmental" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/Departmental" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/Departmental" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Committees/External"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/External" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/External" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/External" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/External" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Committees/University"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/University" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Committees/University" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/University" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Committees/University" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Examining Roles"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Examining Roles" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Examining Roles" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Examining Roles" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Examining Roles" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Grants_Summary"; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Grants_Summary" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Grants_Summary" FROM postgres;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Grants_Summary" TO postgres;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Grants_Summary" TO workload_manager;


--
-- Name: TABLE "10_Workload/Admin/Internal/Lecture Courses"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" TO workload_admin;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE hotwire."10_Workload/Admin/Internal/Lecture Courses" TO workload_teaching;


--
-- Name: TABLE "10_Workload/Admin/Internal/Practical Classes"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Internal/Practical Classes" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Internal/Practical Classes" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Internal/Practical Classes" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Internal/Practical Classes" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Marking Roles"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Marking Roles" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Marking Roles" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Marking Roles" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Marking Roles" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Misc_Weighting_Factors"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Misc_Weighting_Factors" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Misc_Weighting_Factors" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Misc_Weighting_Factors" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Misc_Weighting_Factors" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Outreach Types"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Outreach Types" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Outreach Types" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Outreach Types" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Outreach Types" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Personnel types"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Personnel types" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Personnel types" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Personnel types" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Personnel types" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Research types"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Research types" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Research types" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Research types" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Research types" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Supervising Roles"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Supervising Roles" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Supervising Roles" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Supervising Roles" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Supervising Roles" TO workload_admin;


--
-- Name: TABLE "10_Workload/Admin/Supervision Types"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Supervision Types" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Admin/Supervision Types" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Supervision Types" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Admin/Supervision Types" TO workload_admin;


--
-- Name: TABLE "10_Workload/Everybody/Additional Notes"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Additional Notes" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Additional Notes" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Additional Notes" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Additional Notes" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/College"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/College" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/College" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/College" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/College" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/External/Committee_Membership"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/External/Committee_Membership" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/External/Committee_Membership" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/External/Committee_Membership" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/External/Committee_Membership" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/External/Examining"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/External/Examining" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/External/Examining" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/External/Examining" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/External/Examining" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Committee_Membership"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Committee_Membership" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Committee_Membership" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Committee_Membership" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Committee_Membership" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Departmental Supervisions"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" TO workload_manager;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE hotwire."10_Workload/Everybody/Internal/Departmental Supervisions" TO workload_teaching;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Laboratory Demonstrating"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" TO rl201;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE hotwire."10_Workload/Everybody/Internal/Laboratory Demonstrating" TO workload_teaching;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Lecturing"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" TO workload_teaching;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Lecturing" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Outreach"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Outreach" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Outreach" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Outreach" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Outreach" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Postgraduate_Exams"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Postgraduate_Exams" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Research"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Research" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Research" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Research" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Research" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Students"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Students" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Students" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Students" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Students" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/Internal/Undergraduate_Exams"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" TO workload_manager;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE hotwire."10_Workload/Everybody/Internal/Undergraduate_Exams" TO workload_teaching;


--
-- Name: TABLE "10_Workload/Everybody/Total Workload"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Total Workload" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/Total Workload" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Total Workload" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/Total Workload" TO workload_manager;


--
-- Name: TABLE "10_Workload/Everybody/University_Committees"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/University_Committees" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Everybody/University_Committees" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/University_Committees" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/Everybody/University_Committees" TO workload_manager;


--
-- Name: TABLE "10_Workload/Former_My_Workload/College"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/College" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/College" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/External/Committee_Membership"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Committee_Membership" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/External/Consulting_and_Charitie"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Consulting_and_Charitie" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/External/Examining"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Examining" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/External/Examining" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Histogram"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Histogram" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Histogram" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Committee_Membership"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Committee_Membership" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Departmental Supervisio"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Departmental Supervisio" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Laboratory Demonstratin" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Outreach"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Outreach" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Outreach" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate Lecturing" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Postgraduate_Exams"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Postgraduate_Exams" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Research"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Research_Group_Personne"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Research_Group_Personne" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate Lecturing" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/Internal/Undergraduate_Exams"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/Internal/Undergraduate_Exams" FROM rl201;


--
-- Name: TABLE "10_Workload/Former_My_Workload/University_Committees"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/University_Committees" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/Former_My_Workload/University_Committees" FROM rl201;


--
-- Name: TABLE "10_Workload/My_Workload//Broken_Outreach"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Broken_Outreach" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Broken_Outreach" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Broken_Outreach" TO rl201;


--
-- Name: TABLE "10_Workload/My_Workload//College"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//College" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//College" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//College" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//College" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//College_Old"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//College_Old" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//College_Old" FROM rl201;


--
-- Name: TABLE "10_Workload/My_Workload//Consulting"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Consulting" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Consulting" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Consulting" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Consulting" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Departmental_Committees"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Committees" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Committees" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Committees" TO rl201;
GRANT SELECT ON TABLE hotwire."10_Workload/My_Workload//Departmental_Committees" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Departmental_Demonstrating"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Demonstrating" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Demonstrating" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_Workload/My_Workload//Departmental_Demonstrating" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Departmental_Postgraduate_Lecturing"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_Workload/My_Workload//Departmental_Postgraduate_Lecturing" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Departmental_Supervising"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Supervising" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Supervising" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Supervising" TO rl201;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE hotwire."10_Workload/My_Workload//Departmental_Supervising" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Departmental_Undergraduate_Lecturing"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_Workload/My_Workload//Departmental_Undergraduate_Lecturing" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Outreach"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Outreach" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Outreach" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Outreach" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Outreach" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Postgraduate_Examining"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Postgraduate_Examining" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Postgraduate_Examining" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Postgraduate_Examining" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Postgraduate_Examining" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Research"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Research" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Research" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Research" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Research" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Research_Group"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Research_Group" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Research_Group" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Research_Group" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//Research_Group" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//Undergraduate_Examining"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Undergraduate_Examining" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//Undergraduate_Examining" FROM rl201;
GRANT SELECT ON TABLE hotwire."10_Workload/My_Workload//Undergraduate_Examining" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload//University_Committees"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//University_Committees" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload//University_Committees" FROM rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//University_Committees" TO rl201;
GRANT ALL ON TABLE hotwire."10_Workload/My_Workload//University_Committees" TO workload;


--
-- Name: TABLE "10_Workload/My_Workload_Summary"; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload_Summary" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."10_Workload/My_Workload_Summary" FROM rl201;


--
-- PostgreSQL database dump complete
--

