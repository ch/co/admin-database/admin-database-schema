create or replace function www.person_status_for_supervisor(_person text, _supervisor text)
returns varchar as
$$
declare
   status varchar;
   latest_end_date date;
begin
   IF _person = _supervisor THEN return 'Current'; END IF;
   
   latest_end_date := (select max(coalesce(nullif(end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date)) as end from www._all_roles_v1  
			join person p on person_id=p.id
			join person s on supervisor_id=s.id
			where p.crsid=_person and s.crsid=_supervisor
			group by person_id, supervisor_id);

  CASE 
	WHEN latest_end_date is null then return 'Unknown';
	WHEN latest_end_date >= now() THEN return 'Current';
	WHEN latest_end_date < now() THEN return 'Past';
	ELSE return 'Unknown';
  END CASE;
  
end;

$$ language plpgsql;

alter function www.person_status_for_supervisor(text, text) owner to dev;
grant execute on function www.person_status_for_supervisor(text, text) to www_sites;
