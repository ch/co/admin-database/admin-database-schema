-- View: www._all_roles_v1

DROP VIEW www._all_roles_v1;

CREATE OR REPLACE VIEW www._all_roles_v1 AS 
 SELECT _all_roles_v12.person_id, _all_roles_v12.start_date, _all_roles_v12.intended_end_date, _all_roles_v12.estimated_leaving_date, _all_roles_v12.end_date, _all_roles_v12.post_category_id, _all_roles_v12.supervisor_id, _all_roles_v12.weight
   FROM _all_roles_v12
   JOIN person ON _all_roles_v12.person_id = person.id
  WHERE person.do_not_show_on_website IS NOT TRUE;

ALTER TABLE www._all_roles_v1
  OWNER TO dev;
GRANT ALL ON TABLE www._all_roles_v1 TO dev;
GRANT SELECT ON TABLE www._all_roles_v1 TO www_sites;

