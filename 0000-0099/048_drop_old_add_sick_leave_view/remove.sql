CREATE OR REPLACE VIEW add_sick_leave_view AS 
 SELECT sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, person.surname AS _surname, person.first_names AS _first_names
   FROM sick_leave
   LEFT JOIN person ON person.id = sick_leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE add_sick_leave_view
  OWNER TO cen1001;
GRANT ALL ON TABLE add_sick_leave_view TO cen1001;
GRANT ALL ON TABLE add_sick_leave_view TO dev;
GRANT SELECT ON TABLE add_sick_leave_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE add_sick_leave_view TO hr;
GRANT SELECT ON TABLE add_sick_leave_view TO hr_ro;

CREATE OR REPLACE RULE add_sick_leave_del AS
    ON DELETE TO add_sick_leave_view DO INSTEAD  DELETE FROM sick_leave
  WHERE sick_leave.id = old.id;

CREATE OR REPLACE RULE add_sick_leave_ins AS
    ON INSERT TO add_sick_leave_view DO INSTEAD  INSERT INTO sick_leave (person_id, self_certification_start_date, self_certification_last_day_sick, self_certification_first_day_back, ssp_date_signed_off, ssp_date_returned_to_work, total_self_certification_days, total_self_certification_days_notes, total_ssp_days, total_ssp_days_notes) 
  VALUES (new.person_id, new.self_certification_start_date, new.self_certification_last_day_sick, new.self_certification_first_day_back, new.ssp_date_signed_off, new.ssp_date_returned_to_work, new.total_self_certification_days, new.total_self_certification_days_notes, new.total_ssp_days, new.total_ssp_days_notes)
  RETURNING sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE add_sick_leave_upd AS
    ON UPDATE TO add_sick_leave_view DO INSTEAD  UPDATE sick_leave SET person_id = new.person_id, self_certification_start_date = new.self_certification_start_date, self_certification_last_day_sick = new.self_certification_last_day_sick, self_certification_first_day_back = new.self_certification_first_day_back, ssp_date_signed_off = new.ssp_date_signed_off, ssp_date_returned_to_work = new.ssp_date_returned_to_work, total_self_certification_days = new.total_self_certification_days, total_self_certification_days_notes = new.total_self_certification_days_notes, total_ssp_days = new.total_ssp_days, total_ssp_days_notes = new.total_ssp_days_notes
  WHERE sick_leave.id = old.id;

