CREATE OR REPLACE VIEW sick_history_view AS 
 SELECT sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, 'add_sick_leave_view' AS _targetview
   FROM sick_leave;

ALTER TABLE sick_history_view
  OWNER TO cen1001;

