-- View: hotwire.subnet_hid

-- DROP VIEW hotwire.subnet_hid;

CREATE OR REPLACE VIEW hotwire.subnet_hid AS 
 SELECT subnet.id AS subnet_id, ((((subnet.network_address || ' ('::text) || subnet.notes::text) || ', '::text) || (( SELECT count(*) AS count
           FROM ip_address
          WHERE ip_address.subnet_id = subnet.id AND NOT ip_address.reserved AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)))) || ' free)'::text AS subnet_hid
   FROM subnet
  ORDER BY subnet.notes;

ALTER TABLE hotwire.subnet_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire.subnet_hid TO dev;
GRANT ALL ON TABLE hotwire.subnet_hid TO ro_hid;

