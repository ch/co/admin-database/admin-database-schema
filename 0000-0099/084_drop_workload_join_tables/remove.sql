--
-- Name: workload_college; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_college (
    id integer NOT NULL,
    workload_college_type_id integer,
    person_id integer,
    hours numeric NOT NULL
);


ALTER TABLE public.workload_college OWNER TO rl201;

--
-- Name: workload_college_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_college_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_college_id_seq OWNER TO rl201;

--
-- Name: workload_college_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_college_id_seq OWNED BY workload_college.id;


--
-- Name: workload_committee_membership; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_committee_membership (
    id integer NOT NULL,
    workload_committee_id integer NOT NULL,
    hours numeric,
    chair boolean DEFAULT false,
    notes text,
    person_id integer,
    secretary boolean DEFAULT false
);


ALTER TABLE public.workload_committee_membership OWNER TO rl201;

--
-- Name: workload_committeemembership_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_committeemembership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_committeemembership_id_seq OWNER TO rl201;

--
-- Name: workload_committeemembership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_committeemembership_id_seq OWNED BY workload_committee_membership.id;


--
-- Name: workload_demonstrating; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_demonstrating (
    id bigint NOT NULL,
    person_id integer,
    academic_year date,
    practical_course_id integer,
    hours numeric,
    tripos_id integer
);


ALTER TABLE public.workload_demonstrating OWNER TO rl201;

--
-- Name: workload_demonstrating_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_demonstrating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_demonstrating_id_seq OWNER TO rl201;

--
-- Name: workload_demonstrating_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_demonstrating_id_seq OWNED BY workload_demonstrating.id;


--
-- Name: workload_examining; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_examining (
    id integer NOT NULL,
    person_id integer,
    examination_id integer,
    hours numeric,
    number numeric,
    role_id integer
);


ALTER TABLE public.workload_examining OWNER TO rl201;

--
-- Name: workload_examining_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_examining_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_examining_id_seq OWNER TO rl201;

--
-- Name: workload_examining_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_examining_id_seq OWNED BY workload_examining.id;


--
-- Name: workload_external_consulting; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_external_consulting (
    id integer NOT NULL,
    name character varying,
    role character varying,
    hours numeric,
    notes text,
    person_id integer
);


ALTER TABLE public.workload_external_consulting OWNER TO rl201;

--
-- Name: workload_external_consulting_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_external_consulting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_external_consulting_id_seq OWNER TO rl201;

--
-- Name: workload_external_consulting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_external_consulting_id_seq OWNED BY workload_external_consulting.id;


--
-- Name: workload_extra; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_extra (
    id integer NOT NULL,
    person_id integer,
    hours numeric,
    notes text,
    process_notes text
);


ALTER TABLE public.workload_extra OWNER TO rl201;

--
-- Name: workload_extra_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_extra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_extra_id_seq OWNER TO rl201;

--
-- Name: workload_extra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_extra_id_seq OWNED BY workload_extra.id;


--
-- Name: workload_grants; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE workload_grants (
    id bigint NOT NULL,
    person_id bigint,
    big_grants integer,
    small_grants integer
);


ALTER TABLE public.workload_grants OWNER TO postgres;

--
-- Name: workload_grants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE workload_grants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_grants_id_seq OWNER TO postgres;

--
-- Name: workload_grants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE workload_grants_id_seq OWNED BY workload_grants.id;


--
-- Name: workload_lecturing; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_lecturing (
    id integer NOT NULL,
    person_id integer,
    academic_year date,
    lecture_course_id integer,
    hours numeric,
    firsttime boolean DEFAULT false,
    lecture_course_type_id integer,
    tripos_id bigint
);


ALTER TABLE public.workload_lecturing OWNER TO rl201;

--
-- Name: workload_lecturing_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_lecturing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_lecturing_id_seq OWNER TO rl201;

--
-- Name: workload_lecturing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_lecturing_id_seq OWNED BY workload_lecturing.id;


--
-- Name: workload_outreach; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_outreach (
    id integer NOT NULL,
    outreach_type_id integer,
    hours numeric,
    notes text,
    person_id integer
);


ALTER TABLE public.workload_outreach OWNER TO rl201;

--
-- Name: workload_outreach_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_outreach_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_outreach_id_seq OWNER TO rl201;

--
-- Name: workload_outreach_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_outreach_id_seq OWNED BY workload_outreach.id;


--
-- Name: workload_research; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_research (
    id integer NOT NULL,
    person_id integer,
    workload_research_type_id integer,
    number integer
);


ALTER TABLE public.workload_research OWNER TO rl201;

--
-- Name: workload_research_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_research_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_research_id_seq OWNER TO rl201;

--
-- Name: workload_research_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_research_id_seq OWNED BY workload_research.id;


--
-- Name: workload_students; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_students (
    id integer NOT NULL,
    person_id integer,
    student_type_id integer,
    number integer
);


ALTER TABLE public.workload_students OWNER TO rl201;

--
-- Name: workload_students_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_students_id_seq OWNER TO rl201;

--
-- Name: workload_students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_students_id_seq OWNED BY workload_students.id;


--
-- Name: workload_supervising; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_supervising (
    id integer NOT NULL,
    person_id integer,
    tripos_id integer,
    hours numeric,
    supervision_type_id integer
);


ALTER TABLE public.workload_supervising OWNER TO rl201;

--
-- Name: workload_supervising_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_supervising_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_supervising_id_seq OWNER TO rl201;

--
-- Name: workload_supervising_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_supervising_id_seq OWNED BY workload_supervising.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_college ALTER COLUMN id SET DEFAULT nextval('workload_college_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_committee_membership ALTER COLUMN id SET DEFAULT nextval('workload_committeemembership_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_demonstrating ALTER COLUMN id SET DEFAULT nextval('workload_demonstrating_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_examining ALTER COLUMN id SET DEFAULT nextval('workload_examining_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_external_consulting ALTER COLUMN id SET DEFAULT nextval('workload_external_consulting_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_extra ALTER COLUMN id SET DEFAULT nextval('workload_extra_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY workload_grants ALTER COLUMN id SET DEFAULT nextval('workload_grants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_lecturing ALTER COLUMN id SET DEFAULT nextval('workload_lecturing_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_outreach ALTER COLUMN id SET DEFAULT nextval('workload_outreach_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_research ALTER COLUMN id SET DEFAULT nextval('workload_research_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_students ALTER COLUMN id SET DEFAULT nextval('workload_students_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_supervising ALTER COLUMN id SET DEFAULT nextval('workload_supervising_id_seq'::regclass);


--
-- Name: person_college_role_uniq; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_college
    ADD CONSTRAINT person_college_role_uniq UNIQUE (person_id, workload_college_type_id);


--
-- Name: person_committee_uniq; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_committee_membership
    ADD CONSTRAINT person_committee_uniq UNIQUE (workload_committee_id, person_id);


--
-- Name: workload_college_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_college
    ADD CONSTRAINT workload_college_pkey PRIMARY KEY (id);


--
-- Name: workload_committeemembership_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_committee_membership
    ADD CONSTRAINT workload_committeemembership_pkey PRIMARY KEY (id);


--
-- Name: workload_demonstrating_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_demonstrating
    ADD CONSTRAINT workload_demonstrating_pkey PRIMARY KEY (id);


--
-- Name: workload_examining_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_examining
    ADD CONSTRAINT workload_examining_pkey PRIMARY KEY (id);


--
-- Name: workload_external_consulting_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_external_consulting
    ADD CONSTRAINT workload_external_consulting_pkey PRIMARY KEY (id);


--
-- Name: workload_extra_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_extra
    ADD CONSTRAINT workload_extra_pkey PRIMARY KEY (id);


--
-- Name: workload_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY workload_grants
    ADD CONSTRAINT workload_grants_pkey PRIMARY KEY (id);


--
-- Name: workload_lecturing_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_lecturing
    ADD CONSTRAINT workload_lecturing_pkey PRIMARY KEY (id);


--
-- Name: workload_outreach_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_outreach
    ADD CONSTRAINT workload_outreach_pkey PRIMARY KEY (id);


--
-- Name: workload_research_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_research
    ADD CONSTRAINT workload_research_pkey PRIMARY KEY (id);


--
-- Name: workload_students_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_students
    ADD CONSTRAINT workload_students_pkey PRIMARY KEY (id);


--
-- Name: workload_supervising_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_supervising
    ADD CONSTRAINT workload_supervising_pkey PRIMARY KEY (id);


--
-- Name: fki_workload_demonstrating_tripos_id_fley; Type: INDEX; Schema: public; Owner: rl201; Tablespace: 
--

CREATE INDEX fki_workload_demonstrating_tripos_id_fley ON workload_demonstrating USING btree (tripos_id);


--
-- Name: fki_workload_lecturing_course_type_id_fkey; Type: INDEX; Schema: public; Owner: rl201; Tablespace: 
--

CREATE INDEX fki_workload_lecturing_course_type_id_fkey ON workload_lecturing USING btree (lecture_course_type_id);


--
-- Name: fki_workload_lecturing_tripos_id_fkey; Type: INDEX; Schema: public; Owner: rl201; Tablespace: 
--

CREATE INDEX fki_workload_lecturing_tripos_id_fkey ON workload_lecturing USING btree (tripos_id);


--
-- Name: workload_college_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_college
    ADD CONSTRAINT workload_college_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_college_workload_college_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_college
    ADD CONSTRAINT workload_college_workload_college_type_id_fkey FOREIGN KEY (workload_college_type_id) REFERENCES workload_college_type(id);


--
-- Name: workload_committeemembership_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_committee_membership
    ADD CONSTRAINT workload_committeemembership_person_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_committeemembership_workload_committee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_committee_membership
    ADD CONSTRAINT workload_committeemembership_workload_committee_id_fkey FOREIGN KEY (workload_committee_id) REFERENCES workload_committee(id);


--
-- Name: workload_demonstrating_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_demonstrating
    ADD CONSTRAINT workload_demonstrating_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_demonstrating_practical_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_demonstrating
    ADD CONSTRAINT workload_demonstrating_practical_course_id_fkey FOREIGN KEY (practical_course_id) REFERENCES lecture_course(id);


--
-- Name: workload_demonstrating_tripos_id_fley; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_demonstrating
    ADD CONSTRAINT workload_demonstrating_tripos_id_fley FOREIGN KEY (tripos_id) REFERENCES examination(id);


--
-- Name: workload_examining_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_examining
    ADD CONSTRAINT workload_examining_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_examining_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_examining
    ADD CONSTRAINT workload_examining_role_id_fkey FOREIGN KEY (role_id) REFERENCES workload_examining_role(id);


--
-- Name: workload_examining_tripos_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_examining
    ADD CONSTRAINT workload_examining_tripos_id_fkey FOREIGN KEY (examination_id) REFERENCES examination(id);


--
-- Name: workload_external_consulting_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_external_consulting
    ADD CONSTRAINT workload_external_consulting_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_extra_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_extra
    ADD CONSTRAINT workload_extra_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_grants_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY workload_grants
    ADD CONSTRAINT workload_grants_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_lecturing_course_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_lecturing
    ADD CONSTRAINT workload_lecturing_course_type_id_fkey FOREIGN KEY (lecture_course_type_id) REFERENCES workload_lecturing_type(id);


--
-- Name: workload_lecturing_lecture_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_lecturing
    ADD CONSTRAINT workload_lecturing_lecture_course_id_fkey FOREIGN KEY (lecture_course_id) REFERENCES lecture_course(id);


--
-- Name: workload_lecturing_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_lecturing
    ADD CONSTRAINT workload_lecturing_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_lecturing_tripos_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_lecturing
    ADD CONSTRAINT workload_lecturing_tripos_id_fkey FOREIGN KEY (tripos_id) REFERENCES examination(id);


--
-- Name: workload_outreach_outreach_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_outreach
    ADD CONSTRAINT workload_outreach_outreach_type_id_fkey FOREIGN KEY (outreach_type_id) REFERENCES workload_outreach_type(id);


--
-- Name: workload_outreach_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_outreach
    ADD CONSTRAINT workload_outreach_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_research_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_research
    ADD CONSTRAINT workload_research_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_research_workload_research_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_research
    ADD CONSTRAINT workload_research_workload_research_type_id_fkey FOREIGN KEY (workload_research_type_id) REFERENCES workload_research_type(id);


--
-- Name: workload_students_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_students
    ADD CONSTRAINT workload_students_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_students_student_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_students
    ADD CONSTRAINT workload_students_student_type_fkey FOREIGN KEY (student_type_id) REFERENCES workload_student_type(id);


--
-- Name: workload_supervising_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_supervising
    ADD CONSTRAINT workload_supervising_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: workload_supervising_supervision_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_supervising
    ADD CONSTRAINT workload_supervising_supervision_type_id_fkey FOREIGN KEY (supervision_type_id) REFERENCES workload_supervising_type(id);


--
-- Name: workload_supervising_tripos_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_supervising
    ADD CONSTRAINT workload_supervising_tripos_id_fkey FOREIGN KEY (tripos_id) REFERENCES examination(id);


--
-- Name: workload_college_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_college_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_college_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_college_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_college_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_college_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_college_id_seq TO workload;


--
-- Name: workload_committeemembership_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_committeemembership_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_committeemembership_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_committeemembership_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_committeemembership_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_committeemembership_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_committeemembership_id_seq TO workload;


--
-- Name: workload_demonstrating_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_demonstrating_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_demonstrating_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_demonstrating_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_demonstrating_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_demonstrating_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_demonstrating_id_seq TO workload;
GRANT ALL ON SEQUENCE workload_demonstrating_id_seq TO workload_teaching;


--
-- Name: workload_examining_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_examining_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_examining_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_examining_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_examining_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_examining_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_examining_id_seq TO workload;


--
-- Name: workload_external_consulting_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_external_consulting_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_external_consulting_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_external_consulting_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_external_consulting_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_external_consulting_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_external_consulting_id_seq TO workload;


--
-- Name: workload_extra_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_extra_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_extra_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_extra_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_extra_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_extra_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_extra_id_seq TO workload;


--
-- Name: workload_grants_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE workload_grants_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_grants_id_seq FROM postgres;
GRANT ALL ON SEQUENCE workload_grants_id_seq TO postgres;
GRANT ALL ON SEQUENCE workload_grants_id_seq TO workload_manager;


--
-- Name: workload_lecturing_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_lecturing_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_lecturing_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_lecturing_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_lecturing_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_lecturing_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_lecturing_id_seq TO workload;
GRANT USAGE ON SEQUENCE workload_lecturing_id_seq TO workload_teaching;


--
-- Name: workload_outreach_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_outreach_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_outreach_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_outreach_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_outreach_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_outreach_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_outreach_id_seq TO workload;


--
-- Name: workload_research_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_research_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_research_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_research_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_research_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_research_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_research_id_seq TO workload;


--
-- Name: workload_students_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_students_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_students_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_students_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_students_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_students_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_students_id_seq TO workload;


--
-- Name: workload_supervising_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_supervising_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_supervising_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_supervising_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_supervising_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_supervising_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_supervising_id_seq TO workload;


--
-- PostgreSQL database dump complete
--

