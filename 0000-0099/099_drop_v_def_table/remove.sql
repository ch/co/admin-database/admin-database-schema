--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: v_def; Type: TABLE; Schema: public; Owner: sjt71; Tablespace: 
--

CREATE TABLE v_def (
    definition text
);


ALTER TABLE public.v_def OWNER TO sjt71;

--
-- Data for Name: v_def; Type: TABLE DATA; Schema: public; Owner: sjt71
--

COPY v_def (definition) FROM stdin;
SELECT person.id, person.image_id AS _image_id, person.surname, person.first_names, person.title, date_part('year'::text, age((('now'::text)::date)::timestamp with time zone, (person.date_of_birth)::timestamp with time zone)) AS age_ro, person.email_address, person.crsid, research_group_hid.research_group_hid AS mm_research_group, mm_person_research_group.research_group_id AS mm_research_group_id, dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number, mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id, room_hid.room_hid AS mm_room, mm_person_room.room_id AS mm_room_id, person.location, ps.period, person.extra_filemaker_data FROM (((((((person LEFT JOIN mm_person_research_group ON ((person.id = mm_person_research_group.person_id))) LEFT JOIN research_group_hid USING (research_group_id)) LEFT JOIN mm_person_dept_telephone_number ON ((person.id = mm_person_dept_telephone_number.person_id))) LEFT JOIN dept_telephone_number_hid USING (dept_telephone_number_id)) LEFT JOIN mm_person_room ON ((person.id = mm_person_room.person_id))) LEFT JOIN room_hid USING (room_id)) LEFT JOIN (SELECT physical_stay.person_id, CASE WHEN (physical_stay.end_date < now()) THEN 'Past'::text WHEN ((physical_stay.start_date < now()) AND (physical_stay.end_date > now())) THEN 'Current'::text WHEN (physical_stay.start_date > now()) THEN 'Future'::text ELSE 'Unknown'::text END AS period FROM physical_stay ORDER BY physical_stay.start_date) ps ON ((ps.person_id = person.id))) ORDER BY person.surname;
\.


--
-- PostgreSQL database dump complete
--

