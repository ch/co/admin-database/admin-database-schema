CREATE OR REPLACE VIEW hotwire."10_View/People/Misc_Leave" AS 
 SELECT academic_leave.id, academic_leave.person_id, academic_leave.leave_type_id, academic_leave.start_date, academic_leave.end_date, academic_leave.notes
   FROM leave academic_leave
  WHERE academic_leave.leave_type_id IS NULL OR academic_leave.leave_type_id <> 8;

ALTER TABLE hotwire."10_View/People/Misc_Leave"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Misc_Leave" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Misc_Leave" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Misc_Leave" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Misc_Leave" TO student_management;
GRANT SELECT ON TABLE hotwire."10_View/People/Misc_Leave" TO hr_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Misc_Leave" TO personnel_history_ro;

