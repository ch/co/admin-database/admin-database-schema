ALTER TABLE hotwire."10_View/Research_Group_Editing" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Research_Group_Editing" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Research_Group_Editing" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Research_Group_Editing" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Group_Editing" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Research_Group_Editing" TO hr_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Group_Editing" TO interviewtest;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Research_Group_Editing" TO chematcam;
