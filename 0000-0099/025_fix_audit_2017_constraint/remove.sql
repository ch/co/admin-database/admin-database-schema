alter table _audit_2017 drop constraint _audit_2017_stamp_check;

alter table _audit_2017 add constraint _audit_2017 CHECK (stamp >= '2017-01-01 00:00:00'::timestamp without time zone AND stamp <= '2017-01-01 00:00:00'::timestamp without time zone)
