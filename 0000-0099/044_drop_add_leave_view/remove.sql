CREATE OR REPLACE VIEW hotwire."10_View/Add_Leave_View" AS 
 SELECT a.id, a.person_id, a.start_date, a.end_date, a.leave_type_id, a.notes, a._surname, a._first_names
   FROM ( SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
           FROM leave
      LEFT JOIN person ON person.id = leave.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."10_View/Add_Leave_View"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Add_Leave_View" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Add_Leave_View" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Add_Leave_View" TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Add_Leave_View" TO student_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Add_Leave_View" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Add_Leave_View" TO hr_ro;

CREATE OR REPLACE RULE hotwire_view_add_leave_del AS
    ON DELETE TO hotwire."10_View/Add_Leave_View" DO INSTEAD  DELETE FROM leave
  WHERE leave.id = old.id;

CREATE OR REPLACE RULE hotwire_view_add_leave_ins AS
    ON INSERT TO hotwire."10_View/Add_Leave_View" DO INSTEAD  INSERT INTO leave (person_id, start_date, end_date, leave_type_id, notes) 
  VALUES (new.person_id, new.start_date, new.end_date, new.leave_type_id, new.notes)
  RETURNING leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE hotwire_view_add_leave_upd AS
    ON UPDATE TO hotwire."10_View/Add_Leave_View" DO INSTEAD  UPDATE leave SET person_id = new.person_id, start_date = new.start_date, end_date = new.end_date, leave_type_id = new.leave_type_id, notes = new.notes
  WHERE leave.id = old.id;


