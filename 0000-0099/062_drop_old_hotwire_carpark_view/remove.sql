CREATE OR REPLACE VIEW carpark_view AS 
 SELECT carpark.id, COALESCE(person_hid.person_hid, carpark.notes::text) AS ro_occupier, carpark.car_registration1, carpark.car_registration2, carpark.badge_number, carpark.frequency_id, carpark.person_id, carpark.notes, 
        CASE
            WHEN carpark.badge_number::text ~~* '%Union%'::text THEN regexp_replace(carpark.badge_number::text, '[^0-9]'::text, ''::text, 'g'::text)::integer + 1000
            ELSE regexp_replace(carpark.badge_number::text, 'a$'::text, ''::text)::integer
        END AS _magic
   FROM carpark
   LEFT JOIN person_hid USING (person_id)
  ORDER BY 
   CASE
       WHEN carpark.badge_number::text ~~* '%Union%'::text THEN regexp_replace(carpark.badge_number::text, '[^0-9]'::text, ''::text, 'g'::text)::integer + 1000
       ELSE regexp_replace(carpark.badge_number::text, 'a$'::text, ''::text)::integer
   END, carpark.badge_number, COALESCE(person_hid.person_hid, carpark.notes::text);

ALTER TABLE carpark_view
  OWNER TO cen1001;
GRANT ALL ON TABLE carpark_view TO cen1001;
GRANT ALL ON TABLE carpark_view TO dev;
GRANT SELECT ON TABLE carpark_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE carpark_view TO carpark_management;

CREATE OR REPLACE RULE carpark_del AS
    ON DELETE TO carpark_view DO INSTEAD  DELETE FROM carpark
  WHERE carpark.id = old.id;

CREATE OR REPLACE RULE carpark_ins AS
    ON INSERT TO carpark_view DO INSTEAD  INSERT INTO carpark (id, person_id, car_registration1, car_registration2, badge_number, frequency_id, notes) 
  VALUES (nextval('carpark_id_seq'::regclass), new.person_id, new.car_registration1, new.car_registration2, new.badge_number, new.frequency_id, new.notes)
  RETURNING carpark.id, carpark.notes::text AS ro_occupier, carpark.car_registration1, carpark.car_registration2, carpark.badge_number, carpark.frequency_id, carpark.person_id, carpark.notes, NULL::integer AS int4;

CREATE OR REPLACE RULE carpark_upd AS
    ON UPDATE TO carpark_view DO INSTEAD  UPDATE carpark SET person_id = new.person_id, car_registration1 = new.car_registration1, car_registration2 = new.car_registration2, badge_number = new.badge_number, frequency_id = new.frequency_id, notes = new.notes
  WHERE carpark.id = old.id;


