DROP RULE hotwire_view_research_group_editing_del ON hotwire."10_View/Research_Group_Editing";
DROP RULE hotwire_view_research_group_editing_upd ON hotwire."10_View/Research_Group_Editing";
DROP RULE hotwire_view_research_group_editing_ins ON hotwire."10_View/Research_Group_Editing";
DROP FUNCTION hw_fn_research_group_ins(hotwire."10_View/Research_Group_Editing");
drop view hotwire."10_View/Research_Group_Editing";

CREATE OR REPLACE VIEW hotwire."10_View/Research_Group_Editing" AS 
 SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, research_group.website_address, research_group.sector_id, ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id) AS computer_rep_id, ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id) AS person_id
   FROM research_group
  ORDER BY research_group.name;

ALTER TABLE hotwire."10_View/Research_Group_Editing" OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Research_Group_Editing" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Research_Group_Editing" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Group_Editing" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Research_Group_Editing" TO hr_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Research_Group_Editing" TO interviewtest;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Research_Group_Editing" TO chematcam;


CREATE OR REPLACE FUNCTION hw_fn_research_group_ins(hotwire."10_View/Research_Group_Editing")
  RETURNS bigint AS
$BODY$

declare 

r alias for $1;
r_id bigint;

begin

insert into research_group (id, name, head_of_group_id, administrator_id, website_address, sector_id ) values (r_id, r.name, r.head_of_group_id, r.alternate_admin_contact_id, r.website_address, r.sector_id ) returning id into r_id;

perform fn_mm_array_update(r.person_id,'mm_person_research_group'::varchar,
                           'research_group_id'::varchar,
                           'person_id'::varchar,
                           r_id);

 perform fn_mm_array_update(r.computer_rep_id,
                           'mm_research_group_computer_rep'::varchar,
                           'research_group_id'::varchar,
                           'computer_rep_id'::varchar,
                           r_id);
                           
return r_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_research_group_ins(hotwire."10_View/Research_Group_Editing")
  OWNER TO dev;

-- Rule: hotwire_view_research_group_editing_del ON hotwire."10_View/Research_Group_Editing"


CREATE OR REPLACE RULE hotwire_view_research_group_editing_del AS
    ON DELETE TO hotwire."10_View/Research_Group_Editing" DO INSTEAD  DELETE FROM research_group
  WHERE research_group.id = old.id;

-- Rule: hotwire_view_research_group_editing_ins ON hotwire."10_View/Research_Group_Editing"


CREATE OR REPLACE RULE hotwire_view_research_group_editing_ins AS
    ON INSERT TO hotwire."10_View/Research_Group_Editing" DO INSTEAD  SELECT hw_fn_research_group_ins(new.*) AS id;

-- Rule: hotwire_view_research_group_editing_upd ON hotwire."10_View/Research_Group_Editing"


CREATE OR REPLACE RULE hotwire_view_research_group_editing_upd AS
    ON UPDATE TO hotwire."10_View/Research_Group_Editing" DO INSTEAD ( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, sector_id = new.sector_id
  WHERE research_group.id = old.id;
 SELECT fn_mm_array_update(new.person_id, old.person_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.person_id, old.person_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) AS fn_mm_array_update;
);


