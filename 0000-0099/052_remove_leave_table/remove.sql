CREATE SEQUENCE academic_leave_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 180
  CACHE 1;
ALTER TABLE academic_leave_id_seq
  OWNER TO cen1001;
GRANT ALL ON SEQUENCE academic_leave_id_seq TO cen1001;
GRANT ALL ON SEQUENCE academic_leave_id_seq TO dev;
GRANT ALL ON SEQUENCE academic_leave_id_seq TO hr;
GRANT USAGE ON SEQUENCE academic_leave_id_seq TO student_management;

CREATE TABLE leave
(
  id bigint NOT NULL DEFAULT nextval('academic_leave_id_seq'::regclass),
  start_date date,
  end_date date,
  sabbatical boolean,
  person_id bigint NOT NULL,
  notes character varying(500),
  leave_type_id bigint,
  CONSTRAINT pk_academic_leave PRIMARY KEY (id),
  CONSTRAINT academic_leave_fk_person FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE leave
  OWNER TO cen1001;
GRANT ALL ON TABLE leave TO cen1001;
GRANT ALL ON TABLE leave TO dev;

CREATE TRIGGER leave_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON leave
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();

