CREATE OR REPLACE VIEW hotwire.leave_type_hid AS 
 SELECT leave_type_hid.leave_type_id, leave_type_hid.leave_type_hid
   FROM leave_type_hid;

ALTER TABLE hotwire.leave_type_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.leave_type_hid TO postgres;
GRANT SELECT ON TABLE hotwire.leave_type_hid TO ro_hid;
