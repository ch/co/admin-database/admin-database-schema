CREATE OR REPLACE VIEW hotwire."10_View/People/Sick_Leave_History" AS 
 SELECT sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes
   FROM sick_leave;

ALTER TABLE hotwire."10_View/People/Sick_Leave_History"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Sick_Leave_History" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Sick_Leave_History" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Sick_Leave_History" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Sick_Leave_History" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/People/Sick_Leave_History" TO student_management;
GRANT SELECT ON TABLE hotwire."10_View/People/Sick_Leave_History" TO hr_ro;

