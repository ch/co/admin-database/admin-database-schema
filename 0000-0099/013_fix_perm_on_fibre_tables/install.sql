ALTER TABLE hotwire."10_View/Network/Fibre_Panel" owner to dev;
grant all on hotwire."10_View/Network/Fibre_Panel" to cos;

ALTER table hotwire.fibre_termination_hid owner to dev;
grant all on table hotwire.fibre_termination_hid to cos;

ALTER table fibre_termination owner to dev;
grant all on table fibre_termination to cos;
