ALTER TABLE hotwire."10_View/Network/Fibre_Panel" owner to postgres;
revoke all on hotwire."10_View/Network/Fibre_Panel" from cos;

ALTER table hotwire.fibre_termination_hid owner to postgres;
revoke all on table hotwire.fibre_termination_hid from cos;

ALTER table fibre_termination owner to postgres;
revoke all on table fibre_termination from cos;
