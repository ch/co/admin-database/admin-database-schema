CREATE OR REPLACE VIEW leave_view AS 
 SELECT person.id, person.id AS person_id
   FROM person;

ALTER TABLE leave_view
  OWNER TO cen1001;
GRANT ALL ON TABLE leave_view TO cen1001;
GRANT SELECT ON TABLE leave_view TO hr;
GRANT SELECT ON TABLE leave_view TO student_management;
GRANT SELECT ON TABLE leave_view TO mgmt_ro;
GRANT SELECT ON TABLE leave_view TO dev;
GRANT SELECT ON TABLE leave_view TO hr_ro;
