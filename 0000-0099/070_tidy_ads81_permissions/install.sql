ALTER TABLE room_id_seq OWNER TO dev;
GRANT ALL ON SEQUENCE room_id_seq TO cen1001;
GRANT ALL ON SEQUENCE room_id_seq TO dev;
GRANT ALL ON SEQUENCE room_id_seq TO space_management;
revoke SELECT, USAGE ON SEQUENCE room_id_seq from ads81;
