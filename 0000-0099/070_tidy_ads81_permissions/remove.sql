ALTER TABLE room_id_seq OWNER TO cen1001;
GRANT ALL ON SEQUENCE room_id_seq TO cen1001;
GRANT ALL ON SEQUENCE room_id_seq TO dev;
GRANT ALL ON SEQUENCE room_id_seq TO space_management;
GRANT SELECT, USAGE ON SEQUENCE room_id_seq TO ads81;
