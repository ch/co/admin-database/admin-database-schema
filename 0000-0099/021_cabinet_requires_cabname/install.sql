-- hotwire.fibre_panel_{a,b}_hid uses cabinet.cabname to synthesis fibre panel names
alter table cabinet alter column cabname set not null;
