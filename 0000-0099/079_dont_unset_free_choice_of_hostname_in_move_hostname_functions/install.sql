CREATE OR REPLACE FUNCTION move_hostname_to_subnet(
    _hostname character varying,
    _subnetid integer)
  RETURNS SETOF character varying AS
$BODY$
DECLARE
 _newipid bigint;
BEGIN
 return next 'Moving '||_hostname||' into subnet '||_subnetid;
 select id from ip_address where hostname is null and subnet_id=_subnetid order by ip asc limit 1 into _newipid;
 if not found then
  RAISE EXCEPTION 'cannot find a spare IP address in %', _subnetid;
 end if;
 return next 'Renaming '||_hostname||' to old-'||_hostname;
 update ip_address set hostname='old-'||_hostname where hostname=_hostname;
 if not found then
  raise exception 'Cannot find old ip_address for %',_hostname;
  end if;
 return next 'Making IP address '||(select ip from ip_address where id=_newipid limit 1)||' be '||_hostname;
 update ip_address set hostname=_hostname where id=_newipid;
 return next 'Updating mm_system_image_ip_address';
 update mm_system_image_ip_address set ip_address_id=_newipid where ip_address_id=(select id from ip_address where hostname='old-'||_hostname limit 1);
 return next 'Removing old entry for '||_hostname;
 update ip_address set hobbit_flags=(select hobbit_flags from ip_address where hostname='old-'||_hostname) where hostname=_hostname;
 update ip_address set hostname=null, hobbit_flags=null where hostname='old-'||_hostname;
 
END
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100
  ROWS 1000;
ALTER FUNCTION move_hostname_to_subnet(character varying, integer)
  OWNER TO dev;


CREATE OR REPLACE FUNCTION move_hostname_to_ip(
    _hostname character varying,
    _ip inet)
  RETURNS SETOF character varying AS
$BODY$
DECLARE
 _newipid bigint;
 _thost varchar;

BEGIN
 -- Check _ip exists
 select id from ip_address where ip=_ip into _newipid;
 if not found then raise Exception 'IP address not found: %', _ip; end if;
 
 -- Check _ip doesn't already have a hostname
 select hostname from ip_address where ip=_ip and hostname is null into _thost;
 if not found then raise Exception 'IP address assigned to %', _thost; end if;

 return next 'Moving '||_hostname||' to ip address '||_ip;
 return next 'Renaming '||_hostname||' to old-'||_hostname;
 update ip_address set hostname='old-'||_hostname where hostname=_hostname;
 return next 'Making IP address '||(select ip from ip_address where id=_newipid limit 1)||' be '||_hostname;
 update ip_address set hostname=_hostname where id=_newipid;
 return next 'Updating mm_system_image_ip_address';
 update mm_system_image_ip_address set ip_address_id=_newipid where ip_address_id=(select id from ip_address where hostname='old-'||_hostname limit 1);
 return next 'Removing old entry for '||_hostname;
 
 
END
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100
  ROWS 1000;
ALTER FUNCTION move_hostname_to_ip(character varying, inet)
  OWNER TO dev;

