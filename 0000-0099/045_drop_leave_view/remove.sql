CREATE OR REPLACE VIEW hotwire."10_View/Leave" AS 
 SELECT a.id, a.person_id, a.sick_history_subview, a.misc_leave_subview, a.sabbatical_leave_subview, a._surname
   FROM ( SELECT person.id, person.id AS person_id, _to_hwsubviewb('10_View/People/Sick_Leave_History'::text::character varying, 'person_id'::text::character varying, '10_View/People/Sick_Leave_History'::text::character varying, NULL::character varying, NULL::character varying) AS sick_history_subview, _to_hwsubviewb('10_View/People/Misc_Leave'::text::character varying, 'person_id'::text::character varying, '10_View/People/Misc_Leave'::text::character varying, NULL::character varying, NULL::character varying) AS misc_leave_subview, _to_hwsubviewb('10_View/People/Sabbatical_Leave'::text::character varying, 'person_id'::text::character varying, '10_View/People/Sabbatical_Leave'::text::character varying, NULL::character varying, NULL::character varying) AS sabbatical_leave_subview, person.surname AS _surname
           FROM person) a
  ORDER BY a._surname;

ALTER TABLE hotwire."10_View/Leave"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Leave" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Leave" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Leave" TO student_management;
GRANT SELECT ON TABLE hotwire."10_View/Leave" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Leave" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Leave" TO hr_ro;
