alter table research_group add column roaming_profiles boolean;
comment on column research_group.roaming_profiles is $$To allow us to turn roaming profiles for new accounts off group by group. NULL means do whatever the default in the account creation code is. FALSE means do not create a profile. TRUE isn't implemented and should not be implemented.$$;
-- set new RGs to FALSE
alter table research_group alter column roaming_profiles set default false;



create view apps.add_ad_accounts_research_group_parameters_v6 as
SELECT 
    COALESCE(research_group.active_directory_container, research_group.name) AS name, 
    group_fileserver.hostname_for_users, 
    group_fileserver.shortname, 
    ip_address.ip, 
    group_fileserver.homepath, 
    group_fileserver.localhomepath, 
    group_fileserver.profilepath, 
    group_fileserver.localprofilepath, 
    os_class_hid.os_class_hid, 
    group_fileserver.automatic_homedir_creation_enabled,
    research_group.roaming_profiles as roaming_profiles
FROM research_group
LEFT JOIN group_fileserver ON research_group.group_fileserver_id = group_fileserver.id
LEFT JOIN ip_address ON group_fileserver.hostname_for_users::text = ip_address.hostname::text
LEFT JOIN system_image ON system_image.id = group_fileserver.system_image_id
LEFT JOIN operating_system ON system_image.operating_system_id = operating_system.id
LEFT JOIN os_class_hid USING (os_class_id);

alter view apps.add_ad_accounts_research_group_parameters_v6 owner to dev;
grant select on apps.add_ad_accounts_research_group_parameters_v6 to ad_accounts;

comment on view apps.add_ad_accounts_research_group_parameters_v6 is $$For the use of the chemaccmgmt tools$$;
