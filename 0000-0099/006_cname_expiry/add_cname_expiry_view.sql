CREATE VIEW apps.cname_expiry AS
SELECT
    cname.fromname,
    cname.expiry_date,
    rg.name as research_group,
    rg.active_directory_container
FROM dns_cnames cname
LEFT JOIN research_group rg on cname.research_group_id = rg.id;

ALTER VIEW apps.cname_expiry OWNER TO dev;

GRANT ALL ON apps.cname_expiry TO dev;
GRANT SELECT ON apps.cname_expiry TO cos;
GRANT SELECT ON apps.cname_expiry TO ad_accounts;



