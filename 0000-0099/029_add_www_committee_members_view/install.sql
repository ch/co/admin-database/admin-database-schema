CREATE OR REPLACE VIEW www.committee_members AS
SELECT person.crsid, person.email_address, www_person_hid,
(person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name FROM person
LEFT JOIN www_person_hid_v2 ON person.id=person_id
LEFT JOIN _physical_status_v3 ON _physical_status_v3.person_id = person.id
LEFT JOIN physical_status_hid ON  _physical_status_v3.status_id = physical_status_hid
WHERE physical_status_hid = 'Current' AND crsid IS NOT NULL;

COMMENT ON VIEW www.committee_members is 'View of people for committees website, not filtered by do_not_show_on_website, which is why the grants differ from most of the www schema';

ALTER VIEW www.committee_members OWNER TO dev;
GRANT SELECT ON www.committee_members to www_committees;
