CREATE OR REPLACE VIEW add_leave_view AS 
 SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
   FROM leave
   LEFT JOIN person ON person.id = leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE add_leave_view
  OWNER TO cen1001;
GRANT ALL ON TABLE add_leave_view TO cen1001;
GRANT SELECT ON TABLE add_leave_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE add_leave_view TO hr;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE add_leave_view TO student_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE add_leave_view TO dev;
GRANT SELECT ON TABLE add_leave_view TO hr_ro;

CREATE OR REPLACE RULE add_leave_del AS
    ON DELETE TO add_leave_view DO INSTEAD  DELETE FROM leave
  WHERE leave.id = old.id;

CREATE OR REPLACE RULE add_leave_ins AS
    ON INSERT TO add_leave_view DO INSTEAD  INSERT INTO leave (person_id, start_date, end_date, leave_type_id, notes) 
  VALUES (new.person_id, new.start_date, new.end_date, new.leave_type_id, new.notes)
  RETURNING leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE add_leave_upd AS
    ON UPDATE TO add_leave_view DO INSTEAD  UPDATE leave SET person_id = new.person_id, start_date = new.start_date, end_date = new.end_date, leave_type_id = new.leave_type_id, notes = new.notes
  WHERE leave.id = old.id;

