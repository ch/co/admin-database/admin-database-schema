CREATE OR REPLACE VIEW sabbatical_leave_subview AS 
 SELECT academic_leave.id, academic_leave.person_id, academic_leave.notes, academic_leave.start_date, academic_leave.end_date, 'add_leave_view'::text AS _targetview
   FROM leave academic_leave
  WHERE academic_leave.leave_type_id = 8;

ALTER TABLE sabbatical_leave_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE sabbatical_leave_subview TO cen1001;
GRANT SELECT ON TABLE sabbatical_leave_subview TO mgmt_ro;
GRANT SELECT ON TABLE sabbatical_leave_subview TO hr;
GRANT SELECT ON TABLE sabbatical_leave_subview TO student_management;
GRANT SELECT ON TABLE sabbatical_leave_subview TO hr_ro;

