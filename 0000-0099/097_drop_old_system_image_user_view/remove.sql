CREATE OR REPLACE VIEW system_image_user_view AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name, system_image.operating_system_id, system_image.wired_mac_1, system_image.wireless_mac, ip_address_hid.ip_address_hid AS mm_ip_address, mm_system_image_ip_address.ip_address_id AS mm_ip_address_id, hardware.hardware_type_id, hardware.room_id, system_image.user_id, hardware.owner_id, hardware.personally_owned, hardware.comments
   FROM system_image
   LEFT JOIN reinstall_results USING (id)
   JOIN hardware ON hardware.id = system_image.hardware_id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address_hid USING (ip_address_id);

ALTER TABLE system_image_user_view
  OWNER TO sjt71;
GRANT ALL ON TABLE system_image_user_view TO sjt71;
GRANT ALL ON TABLE system_image_user_view TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE system_image_user_view TO old_cos;

CREATE OR REPLACE FUNCTION fn_system_image_user_upd(system_image_user_view)
  RETURNS bigint AS
$BODY$
 declare
         v alias for $1;
         v_system_image_id BIGINT;
 v_hardware_id BIGINT;
 begin

 IF v.id IS NOT NULL THEN

   v_system_image_id := v.id;

   UPDATE hardware SET
         manufacturer=v.manufacturer, model=v.model, name=v.name,
 hardware_type_id=v.hardware_type_id, room_id=v.room_id,
 owner_id=v.owner_id,
 personally_owned=v.personally_owned, comments=v.comments
   WHERE hardware.id = v._hardware_id;

   UPDATE system_image SET
 user_id = v.user_id, operating_system_id = v.operating_system_id,
 wired_mac_1 = v.wired_mac_1, wireless_mac = v.wireless_mac
   WHERE system_image.id = v.id;

 ELSE
 v_system_image_id := nextval('system_image_id_seq');
   v_hardware_id := nextval('hardware_id_seq');

   INSERT INTO hardware
         (id, manufacturer, model, name, hardware_type_id, room_id,
         owner_id, personally_owned, comments)
   VALUES
         (v_hardware_id, v.manufacturer, v.model, v.name, v.hardware_type_id,
          v.room_id, v.owner_id, v.personally_owned, v.comments);

   INSERT INTO system_image
         (id, hardware_id, user_id, operating_system_id, wired_mac_1, wireless_mac)
   VALUES
         (v_system_image_id, v_hardware_id, v.user_id, v.operating_system_id,
          v.wired_mac_1, v.wireless_mac);

 END IF;

 PERFORM fn_upd_many_many (v_system_image_id, v.mm_ip_address, 'system_image',
 'ip_address');

 return v_system_image_id;
 end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_system_image_user_upd(system_image_user_view)
  OWNER TO sjt71;


CREATE OR REPLACE RULE system_image_user_del AS
    ON DELETE TO system_image_user_view DO INSTEAD  DELETE FROM system_image
      WHERE system_image.id = old.id;

CREATE OR REPLACE RULE system_image_user_ins AS
    ON INSERT TO system_image_user_view DO INSTEAD  SELECT fn_system_image_user_upd(new.*) AS id;

CREATE OR REPLACE RULE system_image_user_upd AS
    ON UPDATE TO system_image_user_view DO INSTEAD  SELECT fn_system_image_user_upd(new.*) AS id;

