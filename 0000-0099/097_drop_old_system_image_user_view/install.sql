DROP RULE system_image_user_upd ON system_image_user_view;
DROP RULE system_image_user_ins ON system_image_user_view;
DROP RULE system_image_user_del ON system_image_user_view;
DROP FUNCTION fn_system_image_user_upd(system_image_user_view);
DROP VIEW system_image_user_view;
