CREATE OR REPLACE VIEW usage_monitor_view AS 
 SELECT hardware.id, hardware.name, hardware.hardware_type_id, hardware.room_id, hardware.log_usage, hardware.usage_url
   FROM hardware
  WHERE hardware.log_usage = true;

ALTER TABLE usage_monitor_view
  OWNER TO sjt71;
GRANT ALL ON TABLE usage_monitor_view TO sjt71;
GRANT SELECT ON TABLE usage_monitor_view TO old_cos;
GRANT SELECT ON TABLE usage_monitor_view TO mgmt_ro;
GRANT SELECT ON TABLE usage_monitor_view TO secretariat;
