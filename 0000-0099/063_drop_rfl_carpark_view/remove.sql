CREATE OR REPLACE VIEW hotwire."10_View/RFL/Carpark" AS 
 SELECT a.id, a.ro_occupier, a.car_registration1, a.car_registration2, a.badge_number, a.frequency_id, a.person_id, a.notes, a._magic
   FROM ( SELECT carpark.id, COALESCE(person_hid.person_hid, carpark.notes::text) AS ro_occupier, carpark.car_registration1, carpark.car_registration2, carpark.badge_number, carpark.frequency_id, carpark.person_id, carpark.notes, 
                CASE
                    WHEN carpark.badge_number::text ~~* '%Union%'::text THEN regexp_replace(carpark.badge_number::text, '[^0-9]'::text, ''::text, 'g'::text)::integer + 1000
                    ELSE regexp_replace(carpark.badge_number::text, 'a$'::text, ''::text)::integer
                END AS _magic
           FROM carpark
      LEFT JOIN person_hid USING (person_id)) a
  ORDER BY a._magic, a.badge_number, a.ro_occupier;

ALTER TABLE hotwire."10_View/RFL/Carpark"
  OWNER TO rl201;
