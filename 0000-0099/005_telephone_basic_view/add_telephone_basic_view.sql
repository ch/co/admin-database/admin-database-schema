--  create view
CREATE OR REPLACE VIEW hotwire."10_View/Telephone_Basic" AS 
 SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, ARRAY( SELECT mm_person_dept_telephone_number.person_id
           FROM mm_person_dept_telephone_number
          WHERE mm_person_dept_telephone_number.dept_telephone_number_id = dept_telephone_number.id) AS person_id, dept_telephone_number.fax
   FROM dept_telephone_number;

ALTER TABLE hotwire."10_View/Telephone_Basic"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Telephone_Basic" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Telephone_Basic" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO admin_team;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO cos;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO space_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO student_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Telephone_Basic" TO phones_management;
GRANT SELECT ON TABLE hotwire."10_View/Telephone_Basic" TO building_security;

-- create update function
CREATE OR REPLACE FUNCTION hw_fn_telephone_basic_upd(hotwire."10_View/Telephone_Basic")
  RETURNS bigint AS
$BODY$

declare
        v alias for $1;
        v_dept_telephone_number_id BIGINT;
begin

IF v.id IS NOT NULL THEN

  v_dept_telephone_number_id := v.id;

  UPDATE dept_telephone_number SET
        extension_number=v.extension_number, room_id=v.room_id, fax=v.fax
  WHERE dept_telephone_number.id = v.id;

ELSE

  INSERT INTO dept_telephone_number
        (extension_number, room_id, fax)
  VALUES (v.extension_number, v.room_id, v.fax) returning id into v_dept_telephone_number_id;

END IF;

perform fn_mm_array_update(v.person_id,
                          'mm_person_dept_telephone_number'::varchar,
                          'dept_telephone_number_id'::varchar,
                          'person_id'::varchar,
                          v_dept_telephone_number_id);

return v_dept_telephone_number_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hw_fn_telephone_basic_upd(hotwire."10_View/Telephone_Basic")
  OWNER TO dev;

-- create update rules
CREATE OR REPLACE RULE hotwire_view_telephone_basic_del AS
    ON DELETE TO hotwire."10_View/Telephone_Basic" DO INSTEAD  DELETE FROM dept_telephone_number
  WHERE dept_telephone_number.id = old.id;

CREATE OR REPLACE RULE hotwire_view_telephone_basic_ins AS
    ON INSERT TO hotwire."10_View/Telephone_Basic" DO INSTEAD  SELECT hw_fn_telephone_basic_upd(new.*) AS id;

CREATE OR REPLACE RULE hotwire_view_telephone_basic_upd AS
    ON UPDATE TO hotwire."10_View/Telephone_Basic" DO INSTEAD  SELECT hw_fn_telephone_basic_upd(new.*) AS id;
