grant all on post_category_hid to dev;

create view apps.postgrad_applications_app_people_v2 as
select
    person.id as person_id,
    person.surname,
    coalesce(person.known_as,person.first_names) as first_names,
    title_hid.title_hid as title,
    person.crsid,
    role.post_category_id
from person 
left join title_hid using (title_id) 
left join _latest_role_v12 role on person.id = role.person_id 
left join _physical_status_v3 as presence on person.id = presence.person_id 
where presence.status_id = 'Current';  

alter view apps.postgrad_applications_app_people_v2 owner to dev;
grant select on apps.postgrad_applications_app_people_v2 to postgrad_applications_app;

create view apps.postgrad_applications_app_post_categories as
select
    post_category_id,
    post_category_hid as post_category
from post_category_hid;

alter view apps.postgrad_applications_app_post_categories owner to dev;
grant select on apps.postgrad_applications_app_post_categories to postgrad_applications_app;

create view apps.postgrad_applications_app_people_and_posts as
select
    person_id,
    surname,
    first_names,
    title,
    crsid,
    post_category_id,
    post_category
from apps.postgrad_applications_app_people_v2
join apps.postgrad_applications_app_post_categories using (post_category_id);

alter view apps.postgrad_applications_app_people_and_posts owner to dev;
grant select on apps.postgrad_applications_app_people_and_posts to postgrad_applications_app;
