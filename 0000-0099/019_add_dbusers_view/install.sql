create view hotwire."10_View/Database/Database_Users" as 
with dbusers as (
select 
    r.rolname as rolename,
    ARRAY(
        SELECT b.rolname
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = r.oid and b.rolname <> '_autoadded'
    ) as member_of,
    person_hid.person_hid as person
from pg_catalog.pg_roles r
left join person on rolname = person.crsid
left join person_hid on person.id = person_hid.person_id -- this is to work around a hotwire sorting bug
)
select row_number() over () as id,
    rolename,
    member_of,
    person
from dbusers
order by rolename,member_of,person
;

alter view hotwire."10_View/Database/Database_Users" owner to dev;
grant select on hotwire."10_View/Database/Database_Users" to cos,hr;
