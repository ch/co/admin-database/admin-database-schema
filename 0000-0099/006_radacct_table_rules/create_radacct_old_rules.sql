--
-- Name: radacct_insert_start_is_update; Type: RULE; Schema: public; Owner: dev
--

CREATE or replace RULE radacct_insert_start_is_update AS ON INSERT TO radacct_old WHERE (((new.reason)::text = 'start'::text) AND (EXISTS (SELECT radacct.radacctid FROM radacct_old radacct WHERE (((((radacct.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct.username)::text = (new.username)::text)) AND ((radacct.reason)::text = 'stop'::text))))) DO INSTEAD UPDATE radacct_old SET reason = 'ended'::character varying, acctstarttime = new.acctstarttime WHERE (((((radacct_old.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct_old.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct_old.username)::text = (new.username)::text)) AND ((radacct_old.reason)::text = 'stop'::text));


--
-- Name: radacct_insert_stop_is_update; Type: RULE; Schema: public; Owner: dev
--

CREATE or replace RULE radacct_insert_stop_is_update AS ON INSERT TO radacct_old WHERE (((new.reason)::text = 'stop'::text) AND (EXISTS (SELECT radacct.radacctid FROM radacct_old radacct WHERE (((((radacct.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct.username)::text = (new.username)::text)) AND ((radacct.reason)::text = 'start'::text))))) DO INSTEAD UPDATE radacct_old SET acctterminatecause = new.acctterminatecause, reason = 'ended'::character varying, acctstoptime = new.acctstoptime, acctsessiontime = new.acctsessiontime WHERE (((((radacct_old.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct_old.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct_old.username)::text = (new.username)::text)) AND ((radacct_old.reason)::text = 'start'::text));
