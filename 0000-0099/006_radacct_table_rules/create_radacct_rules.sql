--
-- Name: radacct2_insert_start_is_update; Type: RULE; Schema: public; Owner: dev
--

CREATE or replace RULE radacct2_insert_start_is_update AS ON INSERT TO radacct WHERE (((new.reason)::text = 'start'::text) AND (EXISTS (SELECT radacct2.radacctid FROM radacct radacct2 WHERE (((((radacct2.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct2.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct2.username)::text = (new.username)::text)) AND ((radacct2.reason)::text = 'stop'::text))))) DO INSTEAD UPDATE radacct SET reason = 'ended'::character varying, acctstarttime = new.acctstarttime WHERE (((((radacct.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct.username)::text = (new.username)::text)) AND ((radacct.reason)::text = 'stop'::text));


--
-- Name: radacct2_insert_stop_is_update; Type: RULE; Schema: public; Owner: dev
--

CREATE or replace RULE radacct2_insert_stop_is_update AS ON INSERT TO radacct WHERE (((new.reason)::text = 'stop'::text) AND (EXISTS (SELECT radacct2.radacctid FROM radacct radacct2 WHERE (((((radacct2.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct2.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct2.username)::text = (new.username)::text)) AND ((radacct2.reason)::text = 'start'::text))))) DO INSTEAD UPDATE radacct SET acctterminatecause = new.acctterminatecause, reason = 'ended'::character varying, acctstoptime = new.acctstoptime, acctsessiontime = new.acctsessiontime WHERE (((((radacct.acctsessionid)::text = (new.acctsessionid)::text) AND ((radacct.acctuniqueid)::text = (new.acctuniqueid)::text)) AND ((radacct.username)::text = (new.username)::text)) AND ((radacct.reason)::text = 'start'::text));
