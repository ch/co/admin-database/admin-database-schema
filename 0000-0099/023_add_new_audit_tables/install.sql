CREATE TABLE _audit_2015
(
  operation character varying NOT NULL,
  stamp timestamp without time zone NOT NULL,
  username text NOT NULL,
  tablename text NOT NULL,
  oldcols text,
  newcols text,
  id bigint,
  CONSTRAINT _audit_2015_stamp_check CHECK (stamp >= '2015-01-01 00:00:00'::timestamp without time zone AND stamp <= '2016-01-01 00:00:00'::timestamp without time zone)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX _audit_2015_stamp_idx
  ON _audit_2015
  USING btree
  (stamp);

alter table _audit_2015 owner to dev;

CREATE TABLE _audit_2016
(
  operation character varying NOT NULL,
  stamp timestamp without time zone NOT NULL,
  username text NOT NULL,
  tablename text NOT NULL,
  oldcols text,
  newcols text,
  id bigint,
  CONSTRAINT _audit_2016_stamp_check CHECK (stamp >= '2016-01-01 00:00:00'::timestamp without time zone AND stamp <= '2017-01-01 00:00:00'::timestamp without time zone)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX _audit_2016_stamp_idx
  ON _audit_2016
  USING btree
  (stamp);

alter table _audit_2016 owner to dev;

CREATE TABLE _audit_2017
(
  operation character varying NOT NULL,
  stamp timestamp without time zone NOT NULL,
  username text NOT NULL,
  tablename text NOT NULL,
  oldcols text,
  newcols text,
  id bigint,
  CONSTRAINT _audit_2017 CHECK (stamp >= '2017-01-01 00:00:00'::timestamp without time zone AND stamp <= '2017-01-01 00:00:00'::timestamp without time zone)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX _audit_2017_idx
  ON _audit_2017
  USING btree
  (stamp);

alter table _audit_2017 owner to dev;
