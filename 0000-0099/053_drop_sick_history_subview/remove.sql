CREATE OR REPLACE VIEW sick_history_subview AS 
 SELECT sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, 'sick_leave_updates_view' AS _targetview
   FROM sick_leave
  ORDER BY COALESCE(sick_leave.self_certification_start_date, sick_leave.ssp_date_signed_off);

ALTER TABLE sick_history_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE sick_history_subview TO cen1001;
GRANT ALL ON TABLE sick_history_subview TO dev;
GRANT SELECT ON TABLE sick_history_subview TO mgmt_ro;
GRANT SELECT ON TABLE sick_history_subview TO hr;
GRANT SELECT ON TABLE sick_history_subview TO hr_ro;
