--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: workload_lecturing_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE workload_lecturing_type (
    id bigint NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_lecturing_type OWNER TO postgres;

--
-- Name: examination; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE examination (
    id integer NOT NULL,
    name character varying,
    year integer,
    postgrad boolean DEFAULT false,
    valid_for_examination boolean DEFAULT true,
    count_hours boolean DEFAULT true,
    supervised_by_dept boolean
);


ALTER TABLE public.examination OWNER TO rl201;

--
-- Name: lecture_course; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE lecture_course (
    id integer NOT NULL,
    name character varying,
    tripos_id integer,
    active boolean DEFAULT true,
    weight numeric DEFAULT 1.0,
    is_practicals boolean DEFAULT false NOT NULL
);


ALTER TABLE public.lecture_course OWNER TO rl201;

--
-- Name: workload_outreach_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_outreach_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_outreach_type OWNER TO rl201;

--
-- Name: workload_student_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_student_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_student_type OWNER TO rl201;

--
-- Name: workload_supervising_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_supervising_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_supervising_type OWNER TO rl201;

--
-- Name: lecture_course_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE lecture_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.lecture_course_id_seq OWNER TO rl201;

--
-- Name: lecture_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE lecture_course_id_seq OWNED BY lecture_course.id;


--
-- Name: lecture_course_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('lecture_course_id_seq', 72, true);


--
-- Name: supervision_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE supervision_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.supervision_type_id_seq OWNER TO rl201;

--
-- Name: supervision_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE supervision_type_id_seq OWNED BY workload_supervising_type.id;


--
-- Name: supervision_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('supervision_type_id_seq', 4, true);


--
-- Name: tripos_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE tripos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tripos_id_seq OWNER TO rl201;

--
-- Name: tripos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE tripos_id_seq OWNED BY examination.id;


--
-- Name: tripos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('tripos_id_seq', 9, true);


--
-- Name: workload_college_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_college_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0,
    hourly boolean DEFAULT true
);


ALTER TABLE public.workload_college_type OWNER TO rl201;

--
-- Name: workload_college_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_college_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_college_type_id_seq OWNER TO rl201;

--
-- Name: workload_college_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_college_type_id_seq OWNED BY workload_college_type.id;


--
-- Name: workload_college_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_college_type_id_seq', 7, true);


--
-- Name: workload_committee; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_committee (
    id integer NOT NULL,
    name character varying,
    active boolean DEFAULT true,
    departmental boolean DEFAULT true,
    parent_committee_id integer,
    weight numeric DEFAULT 1.0,
    ucam boolean DEFAULT true,
    weight_chair numeric DEFAULT 1.0,
    weight_secretary numeric DEFAULT 1.0
);


ALTER TABLE public.workload_committee OWNER TO rl201;

--
-- Name: workload_committee_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_committee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_committee_id_seq OWNER TO rl201;

--
-- Name: workload_committee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_committee_id_seq OWNED BY workload_committee.id;


--
-- Name: workload_committee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_committee_id_seq', 50, true);


--
-- Name: workload_examining_role; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_examining_role (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0,
    js_extra character varying,
    hours boolean DEFAULT true,
    departmental boolean DEFAULT true,
    postgrad boolean DEFAULT false,
    ucam boolean DEFAULT true
);


ALTER TABLE public.workload_examining_role OWNER TO rl201;

--
-- Name: workload_examining_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_examining_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_examining_type_id_seq OWNER TO rl201;

--
-- Name: workload_examining_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_examining_type_id_seq OWNED BY workload_examining_role.id;


--
-- Name: workload_examining_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_examining_type_id_seq', 9, true);


--
-- Name: workload_lecturing_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE workload_lecturing_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_lecturing_type_id_seq OWNER TO postgres;

--
-- Name: workload_lecturing_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE workload_lecturing_type_id_seq OWNED BY workload_lecturing_type.id;


--
-- Name: workload_lecturing_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('workload_lecturing_type_id_seq', 6, true);


--
-- Name: workload_marking_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_marking_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_marking_type OWNER TO rl201;

--
-- Name: workload_marking_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_marking_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_marking_type_id_seq OWNER TO rl201;

--
-- Name: workload_marking_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_marking_type_id_seq OWNED BY workload_marking_type.id;


--
-- Name: workload_marking_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_marking_type_id_seq', 1, false);


--
-- Name: workload_outreach_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_outreach_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_outreach_type_id_seq OWNER TO rl201;

--
-- Name: workload_outreach_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_outreach_type_id_seq OWNED BY workload_outreach_type.id;


--
-- Name: workload_outreach_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_outreach_type_id_seq', 4, true);


--
-- Name: workload_research_type; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_research_type (
    id integer NOT NULL,
    name character varying,
    weight numeric DEFAULT 1.0
);


ALTER TABLE public.workload_research_type OWNER TO rl201;

--
-- Name: workload_research_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_research_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_research_type_id_seq OWNER TO rl201;

--
-- Name: workload_research_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_research_type_id_seq OWNED BY workload_research_type.id;


--
-- Name: workload_research_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_research_type_id_seq', 7, true);


--
-- Name: workload_student_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_student_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_student_type_id_seq OWNER TO rl201;

--
-- Name: workload_student_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_student_type_id_seq OWNED BY workload_student_type.id;


--
-- Name: workload_student_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_student_type_id_seq', 7, true);


--
-- Name: workload_weights; Type: TABLE; Schema: public; Owner: rl201; Tablespace: 
--

CREATE TABLE workload_weights (
    id integer NOT NULL,
    "table" character varying,
    weight numeric,
    notes character varying
);


ALTER TABLE public.workload_weights OWNER TO rl201;

--
-- Name: workload_weights_id_seq; Type: SEQUENCE; Schema: public; Owner: rl201
--

CREATE SEQUENCE workload_weights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.workload_weights_id_seq OWNER TO rl201;

--
-- Name: workload_weights_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rl201
--

ALTER SEQUENCE workload_weights_id_seq OWNED BY workload_weights.id;


--
-- Name: workload_weights_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rl201
--

SELECT pg_catalog.setval('workload_weights_id_seq', 2, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY examination ALTER COLUMN id SET DEFAULT nextval('tripos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY lecture_course ALTER COLUMN id SET DEFAULT nextval('lecture_course_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_college_type ALTER COLUMN id SET DEFAULT nextval('workload_college_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_committee ALTER COLUMN id SET DEFAULT nextval('workload_committee_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_examining_role ALTER COLUMN id SET DEFAULT nextval('workload_examining_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY workload_lecturing_type ALTER COLUMN id SET DEFAULT nextval('workload_lecturing_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_marking_type ALTER COLUMN id SET DEFAULT nextval('workload_marking_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_outreach_type ALTER COLUMN id SET DEFAULT nextval('workload_outreach_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_research_type ALTER COLUMN id SET DEFAULT nextval('workload_research_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_student_type ALTER COLUMN id SET DEFAULT nextval('workload_student_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_supervising_type ALTER COLUMN id SET DEFAULT nextval('supervision_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_weights ALTER COLUMN id SET DEFAULT nextval('workload_weights_id_seq'::regclass);


--
-- Data for Name: examination; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY examination (id, name, year, postgrad, valid_for_examination, count_hours, supervised_by_dept) FROM stdin;
1	Part IA	1	f	t	t	f
3	Part II	3	f	t	t	t
4	Part III	4	f	t	t	t
2	Part IB(a)	2	f	t	t	f
5	Part IB(b)	2	f	t	t	f
6	General Graduate Course	0	t	f	t	f
7	PhD	0	t	t	t	f
8	MPhil	0	t	t	f	f
9	CPGS	0	t	t	f	f
\.


--
-- Data for Name: lecture_course; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY lecture_course (id, name, tripos_id, active, weight, is_practicals) FROM stdin;
1	Shapes and Structures of Molecules	1	t	1.0	f
2	Reactions and Mechanisms in Organic Chemistry	1	t	1.0	f
3	Energetics and Equilibria	1	t	1.0	f
4	Kinetics of Chemical Reactions	1	t	1.0	f
5	Chemistry of the Elements	1	t	1.0	f
6	Introduction to Chemistry A	2	t	1.0	f
7	Introduction to Quantum Mechanics	2	t	1.0	f
8	Molecular Spectroscopy	2	t	1.0	f
10	Symmetry and Bonding	2	t	1.0	f
11	Molecular Energy Levels and Thermodynamics	2	t	1.0	f
12	Electrons in Solids	2	t	1.0	f
13	Aromatic and Enolate Chemistry	5	t	1.0	f
14	Nucleophilic Attack on pi Systems	5	t	1.0	f
15	Introduction to Stereochemistry	5	t	1.5	f
16	Transition Metal Chemistry	5	t	1.0	f
17	Main Group Inorganic Ring Chemistry	5	t	1.0	f
18	Introduction to Chemical Biology	5	t	1.0	f
19	Inorganic 1: Structure and Bonding	3	t	1.0	f
20	The foundations of organic synthesis	3	t	1.0	f
21	High resolution molecular spectroscopy	3	t	1.0	f
22	Theoretical techniques	3	t	1.0	f
23	Concepts in physical chemistry	3	t	1.0	f
24	Inorganic II: Transition metal reactivity and organometallic catalysis	3	t	1.0	f
25	Structure and reactivity	3	t	1.0	f
26	Chemical biology 1: Biological catalysis	3	t	1.0	f
27	Chemistry in the atmosphere	3	t	1.0	f
28	Statistical mechanics	3	t	1.0	f
29	Symmetry and perturbation theory	3	t	1.0	f
30	Investigating organic mechanisms	3	t	1.0	f
31	Inorganic III: Characterisation methods	3	t	1.0	f
32	Chemical biology II - Proteins and metalloproteins	3	t	1.0	f
33	Diffraction methods in chemistry	3	t	1.0	f
34	Introduction to polymers	3	t	1.0	f
35	Electronic structure	3	t	1.0	f
36	Chemical biology III: Nucleic acids	3	t	1.0	f
37	The physical basis of NMR spectroscopy	3	t	1.0	f
38	Chemical informatics	3	t	1.0	f
39	Mathematical methods	3	t	1.0	f
40	Protein folding, misfolding and disease	4	t	1.0	f
41	Advanced diffraction methods	4	t	1.0	f
42	Magnetic materials	4	t	1.0	f
43	Energy landscapes and soft materials	4	t	1.0	f
44	Organic synthesis	4	t	1.0	f
45	Computer simulation methods in chemistry and physics	4	t	1.0	f
46	Solid electrolytes	4	t	1.0	f
47	Chemical biology and drug discovery	4	t	1.0	f
48	Medicinal chemistry	4	t	1.0	f
49	Organic catalysis	4	t	1.0	f
50	Nano science and colloid science - chemistry at small lengthscales	4	t	1.0	f
51	electronic structure of solid surfaces	4	t	1.0	f
52	Main group organometallics	4	t	1.0	f
53	Chemical dynamics	4	t	1.0	f
54	Supramolecular chemistry and self-organisation	4	t	1.0	f
55	Organic solids	4	t	1.0	f
56	Total synthesis	4	t	1.0	f
57	Biosynthesis	4	t	1.0	f
58	Atmospheric chemistry and global change	4	t	1.0	f
59	Formal postgrad lecture	6	t	1.0	f
60	Research seminar	6	t	1.0	f
61	Organic	\N	t	1.0	t
62	Inorganic	\N	t	1.0	t
63	Physical	\N	t	1.0	t
64	Theoretical	\N	t	1.0	t
65	Shape and Organic Reactivity - Part IB Chem B 	5	t	1.0	f
66	Co-ordination Chemistry - Part IB Chem B 	5	t	1.0	f
67	Solid-state Chemistry - Part IB Chem B 	5	t	1.0	f
68	Organometallic Chemistry - Part IB Chem B 	5	t	1.0	f
69	The Chemistry of Materials- Part II	3	t	1.0	f
70	Control in Organic Chemistry - Part II	3	t	1.0	f
71	Surfaces - Part II	3	t	1.0	f
72	Catalysis for Chemical Synthesis - Part III	4	t	1.0	f
\.


--
-- Data for Name: workload_college_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_college_type (id, name, weight, hourly) FROM stdin;
1	College supervisions (excluding preparation time)	1.0	t
2	Admissions	1.0	t
3	Director of studies	1.0	f
4	Open days	1.0	t
5	Mock exams	1.0	t
6	Science prizes (e.g. essays)	1.0	t
7	Other college responsibilities related to Chemistry students	1.0	t
\.


--
-- Data for Name: workload_committee; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_committee (id, name, active, departmental, parent_committee_id, weight, ucam, weight_chair, weight_secretary) FROM stdin;
1	Chemistry Advisory Board	t	t	\N	1.0	t	1.0	1.0
2	Senior Management Team	t	t	\N	1.0	t	1.0	1.0
12	PostDoc	t	t	\N	1.0	t	1.0	1.0
3	Teaching and Outreach	t	t	2	1.0	t	1.0	1.0
4	Research Strategy	t	t	2	1.0	t	1.0	1.0
5	Resources and Support	t	t	2	1.0	t	1.0	1.0
6	Staff Management	t	t	2	1.0	t	1.0	1.0
7	Safety	t	t	2	1.0	t	1.0	1.0
9	Student	t	t	3	1.0	t	1.0	1.0
10	Research Interest Group	t	t	4	1.0	t	1.0	1.0
13	Equipment	t	t	5	1.0	t	1.0	1.0
14	Finance	t	t	5	1.0	t	1.0	1.0
15	Web task-force	t	t	5	1.0	t	1.0	1.0
16	Space	t	t	5	1.0	t	1.0	1.0
17	IT	t	t	5	1.0	t	1.0	1.0
18	Library	t	t	5	1.0	t	1.0	1.0
19	Buildings and Energy	t	t	5	1.0	t	1.0	1.0
8	First Aid	t	t	7	1.0	t	1.0	1.0
11	Academic Related Staff	t	t	6	1.0	t	1.0	1.0
20	Acadmic Teams	t	t	6	1.0	t	1.0	1.0
21	Assistant Staff	t	t	6	1.0	t	1.0	1.0
22	Postgraduate student	t	t	6	1.0	t	1.0	1.0
23	Council for the School of Physical Sciences	t	f	\N	1.0	t	1.0	1.0
24	Faculty Board of Physics and Chemistry	t	f	\N	1.0	t	1.0	1.0
25	NST management	t	f	\N	1.0	t	1.0	1.0
26	GB Education	t	f	\N	1.0	t	1.0	1.0
27	Physical Sciences Education	t	f	\N	1.0	t	1.0	1.0
28	Physical Sciences Graduate Education	t	f	\N	1.0	t	1.0	1.0
29	University Appointments Committee	t	f	\N	1.0	t	1.0	1.0
30	Promotions Panel	t	f	\N	1.0	t	1.0	1.0
31	Other (give details)	t	f	\N	1.0	t	1.0	1.0
32	External appointments committee	t	f	\N	1.0	f	1.0	1.0
33	Research Council	t	f	\N	1.0	f	1.0	1.0
34	Learned body (RS, RSC etc)	t	f	\N	1.0	f	1.0	1.0
35	REF panels	t	f	\N	1.0	f	1.0	1.0
36	Refereeing (journals, grants, promotions etc)	t	f	\N	1.0	f	1.0	1.0
37	Editorial board of journal	t	f	\N	1.0	f	1.0	1.0
38	Editing journals	t	f	\N	1.0	f	1.0	1.0
39	Advisory panels	t	f	\N	1.0	f	1.0	1.0
40	Other (give details)	t	f	\N	1.0	f	1.0	1.0
41	Ad-hoc taskforce [notes required]	t	t	\N	1.0	t	1.0	1.0
46	Appointment Committee (any)	t	t	\N	1.0	t	1.0	1.0
47	Athena Swan task force	t	t	\N	1.0	t	1.0	1.0
48	Degree Committee for Physics and Chemistry	t	t	\N	1.0	t	1.0	1.0
\.


--
-- Data for Name: workload_examining_role; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_examining_role (id, name, weight, js_extra, hours, departmental, postgrad, ucam) FROM stdin;
1	Senior Examiner for undergraduate course	1.0	specifytripos	t	t	f	t
2	Examiner for undergraduate course	1.0	specifytripos	t	t	f	t
3	Examiner for CPGS	1.0	\N	t	t	t	t
4	Internal examiner for PhD	1.0	\N	t	t	t	t
5	Internal examiner for MPhil	1.0	\N	t	t	t	t
6	External examiner for PhD	1.0	\N	t	f	t	f
7	External examiner for Masters	1.0	\N	t	f	t	f
8	External examiner for undergraduates	1.0	\N	t	f	f	f
9	Assessor for Part III project	1.0	\N	t	t	f	t
\.


--
-- Data for Name: workload_lecturing_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY workload_lecturing_type (id, name, weight) FROM stdin;
1	Yes - used existing materials	1.0
2	Yes - modified existing materials	1.0
3	Yes - rewrote course	1.0
4	No - used materials from previous year	1.0
5	No - made modifications to materials from previous year	1.0
6	No - rewrote course	1.0
\.


--
-- Data for Name: workload_marking_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_marking_type (id, name, weight) FROM stdin;
1	Lecture course	1.0
2	Project	1.0
\.


--
-- Data for Name: workload_outreach_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_outreach_type (id, name, weight) FROM stdin;
1	Activities (talks, science week, workshops etc)	1.0
2	Outreach development	1.0
3	Alumni events and Chem@Cam	1.0
4	Other (give details)	1.0
\.


--
-- Data for Name: workload_research_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_research_type (id, name, weight) FROM stdin;
2	Funded Projects (over £100k)	1.0
3	Funded Projects (up to £100k)	1.0
\.


--
-- Data for Name: workload_student_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_student_type (id, name, weight) FROM stdin;
1	PhD	1.0
2	MPhil	1.0
3	Part III Students	1.0
4	Erasmus Students	1.0
5	PDRAs/ SRAs	1.0
6	Technicians	1.0
7	Other	1.0
\.


--
-- Data for Name: workload_supervising_type; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_supervising_type (id, name, weight) FROM stdin;
1	Lecture course	1.0
2	Project	1.0
\.


--
-- Data for Name: workload_weights; Type: TABLE DATA; Schema: public; Owner: rl201
--

COPY workload_weights (id, "table", weight, notes) FROM stdin;
1	workload_lecturing	1.0	Repeating a lecture course
2	workload_lecturing.new	2.0	Lecturing a course for the first time
\.


--
-- Name: lecture_course_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY lecture_course
    ADD CONSTRAINT lecture_course_pkey PRIMARY KEY (id);


--
-- Name: supervision_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_supervising_type
    ADD CONSTRAINT supervision_type_pkey PRIMARY KEY (id);


--
-- Name: tripos_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY examination
    ADD CONSTRAINT tripos_pkey PRIMARY KEY (id);


--
-- Name: workload_college_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_college_type
    ADD CONSTRAINT workload_college_type_pkey PRIMARY KEY (id);


--
-- Name: workload_committee_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_committee
    ADD CONSTRAINT workload_committee_pkey PRIMARY KEY (id);


--
-- Name: workload_examining_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_examining_role
    ADD CONSTRAINT workload_examining_type_pkey PRIMARY KEY (id);


--
-- Name: workload_lecturing_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY workload_lecturing_type
    ADD CONSTRAINT workload_lecturing_type_pkey PRIMARY KEY (id);


--
-- Name: workload_marking_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_marking_type
    ADD CONSTRAINT workload_marking_type_pkey PRIMARY KEY (id);


--
-- Name: workload_outreach_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_outreach_type
    ADD CONSTRAINT workload_outreach_type_pkey PRIMARY KEY (id);


--
-- Name: workload_research_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_research_type
    ADD CONSTRAINT workload_research_type_pkey PRIMARY KEY (id);


--
-- Name: workload_student_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_student_type
    ADD CONSTRAINT workload_student_type_pkey PRIMARY KEY (id);


--
-- Name: workload_weights_pkey; Type: CONSTRAINT; Schema: public; Owner: rl201; Tablespace: 
--

ALTER TABLE ONLY workload_weights
    ADD CONSTRAINT workload_weights_pkey PRIMARY KEY (id);


--
-- Name: lecture_course_tripos_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY lecture_course
    ADD CONSTRAINT lecture_course_tripos_id_fkey FOREIGN KEY (tripos_id) REFERENCES examination(id);


--
-- Name: workload_committee_parent_committee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rl201
--

ALTER TABLE ONLY workload_committee
    ADD CONSTRAINT workload_committee_parent_committee_id_fkey FOREIGN KEY (parent_committee_id) REFERENCES workload_committee(id);


--
-- Name: lecture_course_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE lecture_course_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE lecture_course_id_seq FROM rl201;
GRANT ALL ON SEQUENCE lecture_course_id_seq TO rl201;
GRANT ALL ON SEQUENCE lecture_course_id_seq TO workload_teaching;


--
-- Name: workload_college_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_college_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_college_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_college_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_college_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_college_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_college_type_id_seq TO workload;


--
-- Name: workload_committee_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_committee_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_committee_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_committee_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_committee_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_committee_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_committee_id_seq TO workload;


--
-- Name: workload_examining_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_examining_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_examining_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_examining_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_examining_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_examining_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_examining_type_id_seq TO workload;


--
-- Name: workload_lecturing_type_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE workload_lecturing_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_lecturing_type_id_seq FROM postgres;
GRANT ALL ON SEQUENCE workload_lecturing_type_id_seq TO postgres;
GRANT ALL ON SEQUENCE workload_lecturing_type_id_seq TO workload;


--
-- Name: workload_marking_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_marking_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_marking_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_marking_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_marking_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_marking_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_marking_type_id_seq TO workload;


--
-- Name: workload_outreach_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_outreach_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_outreach_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_outreach_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_outreach_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_outreach_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_outreach_type_id_seq TO workload;


--
-- Name: workload_research_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_research_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_research_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_research_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_research_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_research_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_research_type_id_seq TO workload;


--
-- Name: workload_student_type_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_student_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_student_type_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_student_type_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_student_type_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_student_type_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_student_type_id_seq TO workload;


--
-- Name: workload_weights_id_seq; Type: ACL; Schema: public; Owner: rl201
--

REVOKE ALL ON SEQUENCE workload_weights_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE workload_weights_id_seq FROM rl201;
GRANT ALL ON SEQUENCE workload_weights_id_seq TO rl201;
GRANT ALL ON SEQUENCE workload_weights_id_seq TO workload_admin;
GRANT ALL ON SEQUENCE workload_weights_id_seq TO workload_manager;
GRANT ALL ON SEQUENCE workload_weights_id_seq TO workload;


--
-- PostgreSQL database dump complete
--

