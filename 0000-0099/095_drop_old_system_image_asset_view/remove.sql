CREATE OR REPLACE VIEW system_image_asset_view AS 
 SELECT system_image.id, system_image.hardware_id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name, hardware.hardware_type_id, hardware.serial_number, hardware.monitor_serial_number, system_image.operating_system_id, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.owner_id, hardware.value_when_new, hardware.personally_owned, hardware.comments
   FROM system_image
   JOIN hardware ON hardware.id = system_image.hardware_id;

ALTER TABLE system_image_asset_view
  OWNER TO sjt71;
GRANT ALL ON TABLE system_image_asset_view TO sjt71;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE system_image_asset_view TO old_cos;
GRANT ALL ON TABLE system_image_asset_view TO dev;

CREATE OR REPLACE FUNCTION fn_system_image_asset_ins(system_image_asset_view)
  RETURNS bigint AS
$BODY$

declare
        v alias for $1;
        new_hardware_id BIGINT;
        new_system_image_id BIGINT;
begin

new_hardware_id := nextval('hardware_id_seq');        

insert into hardware
        (id, manufacturer, model, name, hardware_type_id, serial_number, 
 monitor_serial_number, date_purchased, date_configured, 
 warranty_end_date, date_decommissioned, warranty_details,
 owner_id, value_when_new, personally_owned, comments)
        values
        (new_hardware_id, v.manufacturer, v.model, v.name, v.hardware_type_id,
 v.serial_number, v.monitor_serial_number, v.date_purchased,
 v.date_configured, v.warranty_end_date, v.date_decommissioned, 
 v.warranty_details, v.owner_id, v.value_when_new, v.personally_owned, 
 v.comments);

new_system_image_id := nextval('system_image_id_seq');

insert into system_image
        (id, hardware_id, operating_system_id)
values
        (new_system_image_id, new_hardware_id, v.operating_system_id);

return new_system_image_id;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_system_image_asset_ins(system_image_asset_view)
  OWNER TO sjt71;

-- Rule: system_image_asset_del ON system_image_asset_view

-- DROP RULE system_image_asset_del ON system_image_asset_view;

CREATE OR REPLACE RULE system_image_asset_del AS
    ON DELETE TO system_image_asset_view DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

-- Rule: system_image_asset_ins ON system_image_asset_view

-- DROP RULE system_image_asset_ins ON system_image_asset_view;

CREATE OR REPLACE RULE system_image_asset_ins AS
    ON INSERT TO system_image_asset_view DO INSTEAD  SELECT fn_system_image_asset_ins(new.*) AS id;

-- Rule: system_image_asset_upd ON system_image_asset_view

-- DROP RULE system_image_asset_upd ON system_image_asset_view;

CREATE OR REPLACE RULE system_image_asset_upd AS
    ON UPDATE TO system_image_asset_view DO INSTEAD ( UPDATE hardware SET manufacturer = new.manufacturer, model = new.model, name = new.name, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, monitor_serial_number = new.monitor_serial_number, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, owner_id = new.owner_id, value_when_new = new.value_when_new, personally_owned = new.personally_owned, comments = new.comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET operating_system_id = new.operating_system_id
  WHERE system_image.id = old.id;
);

