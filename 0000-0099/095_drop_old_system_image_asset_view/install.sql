DROP RULE system_image_asset_del ON system_image_asset_view;
DROP RULE system_image_asset_ins ON system_image_asset_view;
DROP RULE system_image_asset_upd ON system_image_asset_view;
DROP FUNCTION fn_system_image_asset_ins(system_image_asset_view);
DROP VIEW system_image_asset_view;
