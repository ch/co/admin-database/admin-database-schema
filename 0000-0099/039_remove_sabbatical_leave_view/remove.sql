CREATE OR REPLACE VIEW hotwire."10_View/People/Sabbatical_Leave" AS 
 SELECT academic_leave.id, academic_leave.person_id, academic_leave.notes, academic_leave.start_date, academic_leave.end_date
   FROM leave academic_leave
  WHERE academic_leave.leave_type_id = 8;

ALTER TABLE hotwire."10_View/People/Sabbatical_Leave"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Sabbatical_Leave" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/People/Sabbatical_Leave" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/People/Sabbatical_Leave" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/People/Sabbatical_Leave" TO student_management;
GRANT SELECT ON TABLE hotwire."10_View/People/Sabbatical_Leave" TO hr_ro;

