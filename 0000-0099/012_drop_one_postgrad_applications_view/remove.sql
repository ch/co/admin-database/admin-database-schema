-- a role for the postgrad applications app to use for access

create view apps.postgrad_applications_app_people as
select person.surname,
    coalesce(person.known_as,person.first_names) as first_names,
    title_hid.title_hid as title,
    person.crsid,
    role.post_category,
    presence.status_id 
from person 
left join title_hid using (title_id) 
left join _latest_role_v12 role on person.id = role.person_id 
left join _physical_status_v3 as presence on person.id = presence.person_id ;  

alter view apps.postgrad_applications_app_people owner to dev;

grant select on apps.postgrad_applications_app_people to postgrad_applications_app;
