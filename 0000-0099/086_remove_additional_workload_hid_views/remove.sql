CREATE VIEW hotwire."First_Time_hid" AS
SELECT workload_lecturing_type.id AS "First_Time_id", workload_lecturing_type.name AS "First_Time_hid" FROM public.workload_lecturing_type;

ALTER TABLE hotwire."First_Time_hid" OWNER TO postgres;

REVOKE ALL ON TABLE hotwire."First_Time_hid" FROM PUBLIC;
REVOKE ALL ON TABLE hotwire."First_Time_hid" FROM postgres;
GRANT ALL ON TABLE hotwire."First_Time_hid" TO postgres;
GRANT SELECT ON TABLE hotwire."First_Time_hid" TO ro_hid;

CREATE VIEW hotwire.postgraduate_lecture_course_hid AS
    SELECT lecture_course.id AS postgraduate_lecture_course_id, lecture_course.name AS postgraduate_lecture_course_hid FROM (public.lecture_course JOIN public.examination ON ((lecture_course.tripos_id = examination.id))) WHERE (examination.postgrad = true);

ALTER TABLE hotwire.postgraduate_lecture_course_hid OWNER TO postgres;

REVOKE ALL ON TABLE hotwire.postgraduate_lecture_course_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.postgraduate_lecture_course_hid FROM postgres;
GRANT ALL ON TABLE hotwire.postgraduate_lecture_course_hid TO postgres;
GRANT SELECT ON TABLE hotwire.postgraduate_lecture_course_hid TO ro_hid;

CREATE VIEW hotwire.lecture_course_hid AS
    SELECT lecture_course.id AS lecture_course_id, (((((lecture_course.name)::text || ' ('::text) || (examination.name)::text) || ')'::text))::character varying AS lecture_course_hid FROM (public.lecture_course JOIN public.examination ON ((lecture_course.tripos_id = examination.id)));

ALTER TABLE hotwire.lecture_course_hid OWNER TO rl201;

REVOKE ALL ON TABLE hotwire.lecture_course_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.lecture_course_hid FROM rl201;
GRANT ALL ON TABLE hotwire.lecture_course_hid TO rl201;
GRANT SELECT ON TABLE hotwire.lecture_course_hid TO ro_hid;

CREATE VIEW hotwire.supervision_type_hid AS
    SELECT workload_supervising_type.id AS supervision_type_id, workload_supervising_type.name AS supervision_type_hid FROM public.workload_supervising_type;

ALTER TABLE hotwire.supervision_type_hid OWNER TO rl201;

REVOKE ALL ON TABLE hotwire.supervision_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.supervision_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.supervision_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.supervision_type_hid TO ro_hid;

CREATE VIEW hotwire.personnel_type_hid AS
    SELECT workload_student_type.id AS personnel_type_id, workload_student_type.name AS personnel_type_hid FROM public.workload_student_type;

ALTER TABLE hotwire.personnel_type_hid OWNER TO rl201;

REVOKE ALL ON TABLE hotwire.personnel_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.personnel_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.personnel_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.personnel_type_hid TO ro_hid;

CREATE VIEW hotwire.student_type_hid AS
    SELECT workload_student_type.id AS student_type_id, workload_student_type.name AS student_type_hid FROM public.workload_student_type;

ALTER TABLE hotwire.student_type_hid OWNER TO rl201;

REVOKE ALL ON TABLE hotwire.student_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.student_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.student_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.student_type_hid TO ro_hid;

CREATE VIEW hotwire.outreach_type_hid AS
    SELECT workload_outreach_type.id AS outreach_type_id, workload_outreach_type.name AS outreach_type_hid FROM public.workload_outreach_type;

ALTER TABLE hotwire.outreach_type_hid OWNER TO rl201;

REVOKE ALL ON TABLE hotwire.outreach_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.outreach_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.outreach_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.outreach_type_hid TO ro_hid;

CREATE VIEW hotwire.practical_course_hid AS
    SELECT lecture_course.id AS practical_course_id, lecture_course.name AS practical_course_hid FROM public.lecture_course WHERE lecture_course.is_practicals;

ALTER TABLE hotwire.practical_course_hid OWNER TO postgres;

REVOKE ALL ON TABLE hotwire.practical_course_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.practical_course_hid FROM postgres;
GRANT ALL ON TABLE hotwire.practical_course_hid TO postgres;
GRANT SELECT ON TABLE hotwire.practical_course_hid TO ro_hid;
