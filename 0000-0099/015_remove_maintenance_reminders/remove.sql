CREATE TABLE maintenance_reminders
(
  id serial NOT NULL,
  taskname character varying(80) NOT NULL,
  taskdesc character varying(255),
  duedate date NOT NULL,
  overdue boolean,
  overduedays smallint NOT NULL,
  roomnumber character varying(10),
  previousdate date,
  lastpersoncrsid character varying(10),
  CONSTRAINT maintenance_reminders_pkey PRIMARY KEY (id),
  CONSTRAINT maintenance_reminders_lastpersoncrsid_fkey FOREIGN KEY (lastpersoncrsid)
      REFERENCES person (crsid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT maintenance_reminders_overdue_check CHECK (overdue = true OR overdue = false)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE maintenance_reminders OWNER TO postgres;
GRANT ALL ON TABLE maintenance_reminders TO postgres;
GRANT SELECT, UPDATE ON TABLE maintenance_reminders TO osbuilder;
GRANT SELECT, UPDATE ON TABLE maintenance_reminders TO maintremind;

CREATE OR REPLACE VIEW _chem_maintenance_reminders AS 
 SELECT maintenance_reminders.id, maintenance_reminders.taskname, maintenance_reminders.taskdesc, maintenance_reminders.duedate, maintenance_reminders.overdue, maintenance_reminders.overduedays, maintenance_reminders.roomnumber, maintenance_reminders.previousdate, maintenance_reminders.lastpersoncrsid, person.first_names, person.surname
   FROM maintenance_reminders
   JOIN person ON maintenance_reminders.lastpersoncrsid::text = person.crsid::text;

ALTER TABLE _chem_maintenance_reminders OWNER TO postgres;
GRANT ALL ON TABLE _chem_maintenance_reminders TO postgres;
GRANT SELECT ON TABLE _chem_maintenance_reminders TO osbuilder;
GRANT SELECT ON TABLE _chem_maintenance_reminders TO maintremind;
