CREATE TABLE frequency_hid
(
  frequency_id bigserial NOT NULL,
  frequency_hid character varying(20),
  CONSTRAINT frequency_hid_pkey PRIMARY KEY (frequency_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE frequency_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE frequency_hid TO cen1001;
GRANT SELECT ON TABLE frequency_hid TO ro_hid;

INSERT INTO frequency_hid ( frequency_hid ) values ( 'Occasional' );
INSERT INTO frequency_hid ( frequency_hid ) values ( 'Every day' );

CREATE OR REPLACE VIEW hotwire.frequency_hid AS 
 SELECT frequency_hid.frequency_id, frequency_hid.frequency_hid
   FROM frequency_hid;

ALTER TABLE hotwire.frequency_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.frequency_hid TO postgres;
GRANT ALL ON TABLE hotwire.frequency_hid TO ro_hid;
