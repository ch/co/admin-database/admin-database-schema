create view hotwire."10_View/Database/Access_Control_Groups" as
with acl_groups as (
    select 
        r.rolname as role,
        member.rolname as member,
        person.id as person_id
    from pg_catalog.pg_roles r 
    join pg_catalog.pg_auth_members m on m.roleid = r.oid
    join pg_catalog.pg_roles member on m.member = member.oid
    left join person on member.rolname = person.crsid
)
select
    row_number() over(order by role, member, person_id) as id,
    role,
    member,
    person_id
from acl_groups
where role <> '_autoadded'
order by role, member
;

alter view hotwire."10_View/Database/Access_Control_Groups" owner to dev;
grant select on hotwire."10_View/Database/Access_Control_Groups"  to hr, cos;
