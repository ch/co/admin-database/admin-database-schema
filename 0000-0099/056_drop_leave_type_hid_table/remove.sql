--
-- Name: leave_type_hid; Type: TABLE; Schema: public; Owner: cen1001
--

CREATE TABLE public.leave_type_hid (
    leave_type_id bigint NOT NULL,
    leave_type_hid character varying(50) NOT NULL
);


ALTER TABLE public.leave_type_hid OWNER TO cen1001;

--
-- Name: leave_hid_leave_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.leave_hid_leave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.leave_hid_leave_id_seq OWNER TO cen1001;

--
-- Name: leave_hid_leave_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cen1001
--

ALTER SEQUENCE public.leave_hid_leave_id_seq OWNED BY public.leave_type_hid.leave_type_id;


--
-- Name: leave_type_id; Type: DEFAULT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.leave_type_hid ALTER COLUMN leave_type_id SET DEFAULT nextval('public.leave_hid_leave_id_seq'::regclass);


--
-- Name: leave_hid_leave_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.leave_hid_leave_id_seq', 11, true);


--
-- Data for Name: leave_type_hid; Type: TABLE DATA; Schema: public; Owner: cen1001
--

COPY public.leave_type_hid (leave_type_id, leave_type_hid) FROM stdin;
1	Maternity
2	Paternity
3	Parental
4	Emergency
5	Compassionate
6	Study
7	Unpaid
8	Sabbatical
10	Graduate intermission
11	Graduate Leave to Work Away
\.


--
-- Name: leave_hid_pkey; Type: CONSTRAINT; Schema: public; Owner: cen1001
--

ALTER TABLE ONLY public.leave_type_hid
    ADD CONSTRAINT leave_hid_pkey PRIMARY KEY (leave_type_id);


--
-- Name: TABLE leave_type_hid; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON TABLE public.leave_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.leave_type_hid FROM cen1001;
GRANT ALL ON TABLE public.leave_type_hid TO cen1001;
GRANT SELECT ON TABLE public.leave_type_hid TO ro_hid;


--
-- Name: SEQUENCE leave_hid_leave_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

REVOKE ALL ON SEQUENCE public.leave_hid_leave_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.leave_hid_leave_id_seq FROM cen1001;
GRANT ALL ON SEQUENCE public.leave_hid_leave_id_seq TO cen1001;
GRANT USAGE ON SEQUENCE public.leave_hid_leave_id_seq TO hr;


--
-- PostgreSQL database dump complete
--

