CREATE OR REPLACE VIEW hotwire."10_View/A_Meta_Test//00_header" AS 
 SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
   FROM leave
   LEFT JOIN person ON person.id = leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/A_Meta_Test//00_header"
  OWNER TO rl201;

CREATE OR REPLACE VIEW hotwire."10_View/A_Meta_Test//03_Two" AS 
 SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
   FROM leave
   LEFT JOIN person ON person.id = leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/A_Meta_Test//03_Two"
  OWNER TO rl201;

CREATE OR REPLACE VIEW hotwire."10_View/A_Meta_Test//04_One" AS 
 SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
   FROM leave
   LEFT JOIN person ON person.id = leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/A_Meta_Test//04_One"
  OWNER TO rl201;

CREATE OR REPLACE VIEW hotwire."10_View/A_Meta_Test//99_footer" AS 
 SELECT leave.id, leave.person_id, leave.start_date, leave.end_date, leave.leave_type_id, leave.notes, person.surname AS _surname, person.first_names AS _first_names
   FROM leave
   LEFT JOIN person ON person.id = leave.person_id
  ORDER BY person.surname, person.first_names;

ALTER TABLE hotwire."10_View/A_Meta_Test//99_footer"
  OWNER TO rl201;
