drop table radacct cascade;
-- NOTICE:  drop cascades to 14 other objects
-- DETAIL:  drop cascades to table radacct2_2016_1
-- drop cascades to table radacct2_2016_2
-- drop cascades to table radacct2_2016_3
-- drop cascades to table radacct2_2016_4
-- drop cascades to table radacct2_2016_5
-- drop cascades to table radacct2_2016_6
-- drop cascades to table radacct_2016_10
-- drop cascades to table radacct_2016_11
-- drop cascades to table radacct_2016_12
-- drop cascades to table radacct_2016_7
-- drop cascades to table radacct_2016_8
-- drop cascades to table radacct_2016_9
-- drop cascades to table radacct_2017_1
-- drop cascades to table radacct_2017_2

drop table radacct_old;

drop table radpostauth cascade;
-- NOTICE:  drop cascades to 9 other objects
-- DETAIL:  drop cascades to table radpostauth_2016_10
-- drop cascades to table radpostauth_2016_11
-- drop cascades to table radpostauth_2016_12
-- drop cascades to table radpostauth_2016_6
-- drop cascades to table radpostauth_2016_7
-- drop cascades to table radpostauth_2016_8
-- drop cascades to table radpostauth_2016_9
-- drop cascades to table radpostauth_2017_1
-- drop cascades to table radpostauth_2017_2

drop table radpostauth2;

drop table radreply_hid;
