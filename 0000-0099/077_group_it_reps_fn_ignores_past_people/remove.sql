CREATE OR REPLACE FUNCTION fn_update_itreps()
  RETURNS SETOF text AS
$BODY$

declare
  itrep varchar;
  itrepid bigint;

begin
 -- IT reps without DB roles: create them
  for itrep in select distinct crsid from person inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id left join pg_roles on person.crsid=rolname where crsid is not null and rolname is null loop
   execute 'create role '||itrep||' with login in role groupitreps';
  end loop;

 -- IT reps who don't have groupitreps role
  for itrep in select distinct crsid from person inner join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id where (select count(*) from pg_auth_members inner join pg_roles u on member=u.oid inner join pg_roles g on roleid=g.oid where u.rolname=crsid and g.rolname='groupitreps')=0 and crsid is not null loop
   execute 'grant groupitreps to '||itrep;
  end loop;
 -- people with the IT rep role who shouldn't have
 for itrep in select u.rolname from pg_auth_members inner join pg_authid u on u.oid=pg_auth_members.member inner join pg_authid g on pg_auth_members.roleid=g.oid left join person on person.crsid=u.rolname left join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id where g.rolname='groupitreps' and research_group_id is null loop
   execute 'revoke groupitreps from '||itrep;
 end loop;
 -- set homepage in hotwire for people who have no other homepage
 for itrepid in select distinct pg_user.usesysid from pg_user join person on pg_user.usename = person.crsid join mm_research_group_computer_rep on mm_research_group_computer_rep.computer_rep_id=person.id left join ( select user_id, preference_id from hotwire."hw_User Preferences" where hotwire."hw_User Preferences".preference_id = 9 ) homepages on homepages.user_id = pg_user.usesysid where homepages.preference_id is null loop
   execute 'insert into hotwire."hw_User Preferences" ( preference_id, preference_value, user_id ) values ( 9, $$10_View/Group_Computers$$, '||itrepid ||')';
 end loop;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION fn_update_itreps()
  OWNER TO dev;

