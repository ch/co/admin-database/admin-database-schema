-- Function: www.person_status_for_supervisor(text, text)

-- DROP FUNCTION www.person_status_for_supervisor(text, text);

CREATE OR REPLACE FUNCTION www.person_status_for_supervisor(
    _person text,
    _supervisor text)
  RETURNS character varying AS
$BODY$
declare
begin
   IF _person = _supervisor THEN return 'Current'; END IF;


IF EXISTS ( select person_id from www._all_roles_v1  join person p on person_id=p.id join person s on supervisor_id=s.id where p.crsid=_person and s.crsid=_supervisor and status='Current' group by person_id, supervisor_id) THEN
  return 'Current';
ELSIF EXISTS ( select person_id from www._all_roles_v1  join person p on person_id=p.id join person s on supervisor_id=s.id where p.crsid=_person and s.crsid=_supervisor and status='Past' group by person_id, supervisor_id) THEN
  return 'Past';
ELSE
  return 'Unknown';
END IF;
  
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION www.person_status_for_supervisor(text, text)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO www_sites;

