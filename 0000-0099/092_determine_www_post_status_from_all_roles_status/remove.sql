-- Function: www.person_status_for_supervisor(text, text)

-- DROP FUNCTION www.person_status_for_supervisor(text, text);

CREATE OR REPLACE FUNCTION www.person_status_for_supervisor(
    _person text,
    _supervisor text)
  RETURNS character varying AS
$BODY$
declare
   status varchar;
   latest_end_date date;
begin
   IF _person = _supervisor THEN return 'Current'; END IF;
   
   latest_end_date := (select max(coalesce(nullif(end_date,NULL), nullif(estimated_leaving_date,NULL), intended_end_date)) as end from www._all_roles_v1  
			join person p on person_id=p.id
			join person s on supervisor_id=s.id
			where p.crsid=_person and s.crsid=_supervisor
			group by person_id, supervisor_id);
RAISE NOTICE '%', latest_end_date;
  CASE 
	WHEN latest_end_date is null then return 'Unknown';
	WHEN latest_end_date >= now() THEN return 'Current';
	WHEN latest_end_date < now() THEN return 'Past';
	ELSE return 'Unknown';
  END CASE;
  
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION www.person_status_for_supervisor(text, text)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO dev;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO public;
GRANT EXECUTE ON FUNCTION www.person_status_for_supervisor(text, text) TO www_sites;

