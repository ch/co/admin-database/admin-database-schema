CREATE OR REPLACE VIEW hotwire."10_View/Add_Sick_Leave" AS 
 SELECT a.id, a.person_id, a.self_certification_start_date, a.self_certification_last_day_sick, a.self_certification_first_day_back, a.ssp_date_signed_off, a.ssp_date_returned_to_work, a.total_self_certification_days, a.total_self_certification_days_notes, a.total_ssp_days, a.total_ssp_days_notes, a._surname, a._first_names
   FROM ( SELECT sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, person.surname AS _surname, person.first_names AS _first_names
           FROM sick_leave
      LEFT JOIN person ON person.id = sick_leave.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."10_View/Add_Sick_Leave"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Add_Sick_Leave" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Add_Sick_Leave" TO dev;
GRANT SELECT ON TABLE hotwire."10_View/Add_Sick_Leave" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Add_Sick_Leave" TO hr;
GRANT SELECT ON TABLE hotwire."10_View/Add_Sick_Leave" TO hr_ro;

CREATE OR REPLACE RULE hotwire_view_add_sick_leave_del AS
    ON DELETE TO hotwire."10_View/Add_Sick_Leave" DO INSTEAD  DELETE FROM sick_leave
  WHERE sick_leave.id = old.id;

CREATE OR REPLACE RULE hotwire_view_add_sick_leave_ins AS
    ON INSERT TO hotwire."10_View/Add_Sick_Leave" DO INSTEAD  INSERT INTO sick_leave (person_id, self_certification_start_date, self_certification_last_day_sick, self_certification_first_day_back, ssp_date_signed_off, ssp_date_returned_to_work, total_self_certification_days, total_self_certification_days_notes, total_ssp_days, total_ssp_days_notes) 
  VALUES (new.person_id, new.self_certification_start_date, new.self_certification_last_day_sick, new.self_certification_first_day_back, new.ssp_date_signed_off, new.ssp_date_returned_to_work, new.total_self_certification_days, new.total_self_certification_days_notes, new.total_ssp_days, new.total_ssp_days_notes)
  RETURNING sick_leave.id, sick_leave.person_id, sick_leave.self_certification_start_date, sick_leave.self_certification_last_day_sick, sick_leave.self_certification_first_day_back, sick_leave.ssp_date_signed_off, sick_leave.ssp_date_returned_to_work, sick_leave.total_self_certification_days, sick_leave.total_self_certification_days_notes, sick_leave.total_ssp_days, sick_leave.total_ssp_days_notes, NULL::character varying(32) AS "varchar", NULL::character varying(32) AS "varchar";

CREATE OR REPLACE RULE hotwire_view_add_sick_leave_upd AS
    ON UPDATE TO hotwire."10_View/Add_Sick_Leave" DO INSTEAD  UPDATE sick_leave SET person_id = new.person_id, self_certification_start_date = new.self_certification_start_date, self_certification_last_day_sick = new.self_certification_last_day_sick, self_certification_first_day_back = new.self_certification_first_day_back, ssp_date_signed_off = new.ssp_date_signed_off, ssp_date_returned_to_work = new.ssp_date_returned_to_work, total_self_certification_days = new.total_self_certification_days, total_self_certification_days_notes = new.total_self_certification_days_notes, total_ssp_days = new.total_ssp_days, total_ssp_days_notes = new.total_ssp_days_notes
  WHERE sick_leave.id = old.id;


