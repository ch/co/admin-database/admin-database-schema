create view hotwire."10_View/Database/Access_Control_Lists" as
with acls as 
	(
	SELECT
		c.relname as "Name",
		pg_catalog.array_to_string(c.relacl, E'\n') AS "Access privileges"
	FROM pg_catalog.pg_class c
	LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
	WHERE c.relkind = 'v'
		AND 
	n.nspname ~ '^(hotwire)$'
        ORDER BY 1, 2 
	) 
select 
	row_number() over(order by "Name") as id, 
	trim(leading '0123456789_' from "Name") as "Name", 
	"Access privileges" 
from acls;

alter view hotwire."10_View/Database/Access_Control_Lists" owner to dev;
grant select on hotwire."10_View/Database/Access_Control_Lists" to hr,cos;
