-- delete from visitor_type_hid where visitor_type_hid = 'Visitor (Commercial)';
delete from visitor_type_hid where visitor_type_hid = 'Visitor (Commercial)';
select setval('visitor_type_hid_visitor_type_id_seq',16);

-- www.post_category_weights
delete from www.post_category_weights where post_category_id = 'v-17';
-- bump all the ones at or above X
update www.post_category_weights set weight = (select (weight - 1)) where weight > 13;
-- insert v-17 at X

-- www.post_category_hid
delete from www.post_category_hid where post_category_id = 'v-17';
