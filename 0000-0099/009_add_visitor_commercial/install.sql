insert into visitor_type_hid ( visitor_type_id, visitor_type_hid ) values ( 17, 'Visitor (Commercial)' );
-- set value of visitor_type_hid_visitor_type_id_seq to 17
select setval('visitor_type_hid_visitor_type_id_seq',17);

-- www.post_category_weights
-- bump all the ones at or above X
update www.post_category_weights set weight = (select (weight + 1)) where weight > 12;
-- insert v-17 at X
insert into www.post_category_weights ( post_category_id, weight ) values ( 'v-17', 13 );

-- www.post_category_hid
insert into www.post_category_hid ( post_category_id, post_category_hid ) values ( 'v-17', 'Visitor' );
