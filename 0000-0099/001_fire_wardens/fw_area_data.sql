insert into fire_warden_area ( fire_warden_area_id, name, building_id, building_region_id, building_floor_id, location ) values
( 1,'41', 1, 1, 6, 'West End - Atmospheric Chemistry incl. 5th Floor Lab.' ),
( 2,'31', 1, 1, 4, 'Theoretical Chemistry' ),
( 3,'32', 1, 3, 4, 'Inorganic Chemistry' ),
( 4,'33', 1, 2, 4, 'Lab 354 and rooms to staircase' );
select setval(pg_get_serial_sequence('fire_warden_area','fire_warden_area_id'),max(fire_warden_area_id),'t') from fire_warden_area;
;

insert into fire_warden ( fire_warden_id, person_id, qualification_date, fire_warden_area_id, is_primary ) values 
(1, 425, '2013-01-01', 1, 't'),
(2, 6249, '2017-01-01', 1, 'f'),
(3, 1735, '2017-01-01', 1, 'f'),
(4, 769, '2017-01-01',3, 't'),
(5, 7897, '2017-01-01',2,'f')

;
select setval(pg_get_serial_sequence('fire_warden','fire_warden_id'),max(fire_warden_id),'t') from fire_warden;
