
create table fire_warden (
        fire_warden_id bigserial primary key,
        person_id bigint unique not null,
        qualification_date date ,
        fire_warden_area_id bigint, 
        is_primary boolean not null default true
);
comment on table fire_warden is 'ticket 148999';

create unique index idx_primary_fire_warden on fire_warden(fire_warden_area_id) where is_primary = 't';

-- needs insert trigger

--create function fire_warden_pk_trigger() returns trigger as
--$body$
--    begin
--        NEW.fire_warden_id := NEW.person_id;
--        return NEW;
--    end;
--$body$ language plpgsql;
--alter function fire_warden_pk_trigger owner to dev;

alter table fire_warden add constraint fire_warden_fk_person foreign key (person_id) references person(id) on delete cascade;
alter table fire_warden owner to dev;
grant all on fire_warden to dev;
-- needed because update rules use functions which edit the table
grant select,insert,update,delete on fire_warden to safety_management ;

create table fire_warden_area (
    fire_warden_area_id bigserial primary key,
    name varchar unique not null,
    building_id bigint,
    building_floor_id bigint,
    building_region_id bigint,
    location varchar not null
);
comment on table fire_warden_area is 'ticket 148999';
alter table fire_warden_area add constraint fire_warden_area_fk_building foreign key (building_id) references building_hid(building_id) on delete cascade;
alter table fire_warden_area add constraint fire_warden_area_fk_building_region foreign key (building_region_id) references building_region_hid(building_region_id) on delete cascade;
alter table fire_warden_area add constraint fire_warden_area_fk_building_floor foreign key (building_floor_id) references building_floor_hid(building_floor_id) on delete cascade;
alter table fire_warden_area owner to dev;
grant all on fire_warden_area to dev;
-- needed because update rules use functions which edit the table
grant select,insert,update,delete on fire_warden_area to safety_management ;


create view hotwire.fire_warden_area_hid as 
select 
    fire_warden_area.fire_warden_area_id as fire_warden_area_id,
    fire_warden_area.name as fire_warden_area_hid
from
    fire_warden_area;

grant select on hotwire.fire_warden_area_hid to public;

create view hotwire.fire_warden_hid as
select
    fire_warden_id,
    person_hid as fire_warden_hid
from
    fire_warden
    join hotwire.person_hid using(person_id) ;

grant select on hotwire.fire_warden_hid to public;

create view hotwire.deputy_fire_warden_hid as
select fire_warden_id as deputy_fire_warden_id, fire_warden_hid as deputy_fire_warden_hid
from hotwire.fire_warden_hid;

grant select on hotwire.deputy_fire_warden_hid to public;

create view _fire_warden_status as
select
    fire_warden_id,
    fire_warden.qualification_date + interval '2 years' as requalification_date,
    age(fire_warden.qualification_date) < interval '2 years' as in_date
from fire_warden;
alter view _fire_warden_status owner to dev;

create or replace view hotwire."10_View/Safety/Fire_wardens" as
select
    fire_warden_id as id,
    person_id,
    qualification_date,
    fire_warden_area.fire_warden_area_id,
    fire_warden_area.building_id as ro_building_id,
    fire_warden_area.building_region_id as ro_building_region_id,
    fire_warden_area.building_floor_id as ro_building_floor_id,
    is_primary as main_firewarden_for_area,
    array(
        select mm_person_dept_telephone_number.dept_telephone_number_id
        from mm_person_dept_telephone_number
        where fire_warden.person_id = mm_person_dept_telephone_number.person_id
    ) as dept_telephone_number_id,
    _physical_status_v3.status_id as ro_physical_status,
    _fire_warden_status.requalification_date::date as ro_requalification_date,
    _fire_warden_status.in_date as ro_qualification_in_date,
    _latest_role_v12.post_category as ro_post_category,
    case
        when _fire_warden_status.in_date = 'f' then 'orange'::text
        when _fire_warden_status.in_date is null then 'blue'::text
        else null::text
    end as _cssclass
from fire_warden
left join _latest_role_v12 using(person_id)
left join fire_warden_area using (fire_warden_area_id)
left join _physical_status_v3 using (person_id)
left join _fire_warden_status using (fire_warden_id)
;
alter view hotwire."10_View/Safety/Fire_wardens" owner to dev;
grant select,insert,update,delete on hotwire."10_View/Safety/Fire_wardens" to safety_management;
grant select on hotwire."10_View/Safety/Fire_wardens" to mgmt_ro;

create or replace rule fire_warden_del as on delete to hotwire."10_View/Safety/Fire_wardens" do instead
    delete from fire_warden where fire_warden.fire_warden_id = old.id;

create function hotwire.fn_fire_warden_upd(hotwire."10_View/Safety/Fire_wardens") returns bigint as
$body$
declare
    v alias for $1;
    v_fire_warden_id bigint;
begin
    if v.main_firewarden_for_area = 't' and v.fire_warden_area_id is not null
    then
        update fire_warden set is_primary = 'f' where fire_warden_area_id = v.fire_warden_area_id;
    end if;
    if v.id is not null 
    then
        v_fire_warden_id = v.id;
        update fire_warden set 
            qualification_date = v.qualification_date,
            fire_warden_area_id = v.fire_warden_area_id,
            is_primary = v.main_firewarden_for_area
        where fire_warden_id = v_fire_warden_id;
    else
        insert into fire_warden ( 
            qualification_date,
            person_id,
            fire_warden_area_id,
            is_primary
        ) values (
            v.qualification_date,
            v.person_id,
            v.fire_warden_area_id,
            coalesce(v.main_firewarden_for_area,'t')
        ) returning fire_warden_id into v_fire_warden_id;
    end if;
    return v_fire_warden_id;
end;
$body$
language plpgsql;

alter function hotwire.fn_fire_warden_upd(hotwire."10_View/Safety/Fire_wardens") owner to dev;


create or replace rule fire_warden_ins as on insert to hotwire."10_View/Safety/Fire_wardens" do instead
    select hotwire.fn_fire_warden_upd(new.*);

create or replace rule fire_warden_upd as on update to hotwire."10_View/Safety/Fire_wardens" do instead
    select hotwire.fn_fire_warden_upd(new.*);


create or replace view hotwire."10_View/Safety/Fire_warden_areas" as
select
    fire_warden_area_id as id,
    name,
    location,
    building_id,
    building_region_id,
    building_floor_id,
    primary_fw.fire_warden_id as fire_warden_id,
    array (
        select fire_warden_id 
        from fire_warden fw3
        where fw3.fire_warden_area_id = fire_warden_area.fire_warden_area_id
        and fw3.is_primary = 'f'

    ) as deputy_fire_warden_id
from fire_warden_area
left join ( select a2.fire_warden_area_id, fw2.fire_warden_id from fire_warden_area a2 join fire_warden fw2 using (fire_warden_area_id ) where fw2.is_primary = 't' ) primary_fw using (fire_warden_area_id)
;

alter view hotwire."10_View/Safety/Fire_warden_areas" owner to dev;
grant select,insert,update,delete on hotwire."10_View/Safety/Fire_warden_areas" to safety_management;
grant select on hotwire."10_View/Safety/Fire_warden_areas" to mgmt_ro;

create function hotwire.fn_fire_warden_area_upd(hotwire."10_View/Safety/Fire_warden_areas") returns bigint as
$body$
declare
    v alias for $1;
    v_fire_warden_area_id bigint;
begin
    if v.id is not null 
    then
        v_fire_warden_area_id = v.id;
        update fire_warden_area set 
            name = v.name,
            location = v.location,
            building_id = v.building_id,
            building_region_id = v.building_region_id,
            building_floor_id = v.building_floor_id
        where fire_warden_area_id = v_fire_warden_area_id;
    else
        insert into fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) values (
            v.name,
            v.location,
            v.building_id,
            v.building_region_id,
            v.building_floor_id
        ) returning fire_warden_area_id into v_fire_warden_area_id;
    end if;
    -- remove all fire wardens for this area
    update fire_warden set fire_warden_area_id = null where fire_warden.fire_warden_area_id = v_fire_warden_area_id;
    -- update the primary fire warden
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 't' where fire_warden.fire_warden_id = v.fire_warden_id;
    -- update secondary fire wardens
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 'f' where fire_warden.fire_warden_id = any(v.deputy_fire_warden_id);
    return v_fire_warden_area_id;
end;
$body$
language plpgsql;

alter function hotwire.fn_fire_warden_area_upd(hotwire."10_View/Safety/Fire_warden_areas") owner to dev;

create or replace rule fire_warden_area_del as on delete to hotwire."10_View/Safety/Fire_warden_areas" do instead
    delete from fire_warden_area where fire_warden_area.fire_warden_area_id = old.id;

create or replace rule fire_warden_area_ins as on insert to hotwire."10_View/Safety/Fire_warden_areas" do instead
    select hotwire.fn_fire_warden_area_upd(new.*);

create or replace rule fire_warden_area_upd as on update to hotwire."10_View/Safety/Fire_warden_areas" do instead
    select hotwire.fn_fire_warden_area_upd(new.*);
