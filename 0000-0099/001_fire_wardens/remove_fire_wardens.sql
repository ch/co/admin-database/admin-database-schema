drop rule fire_warden_area_ins on hotwire."10_View/Safety/Fire_warden_areas";
drop rule fire_warden_area_upd on hotwire."10_View/Safety/Fire_warden_areas";
drop rule fire_warden_ins on hotwire."10_View/Safety/Fire_wardens";
drop rule fire_warden_upd on hotwire."10_View/Safety/Fire_wardens";
drop function hotwire.fn_fire_warden_area_upd(hotwire."10_View/Safety/Fire_warden_areas");
drop function hotwire.fn_fire_warden_upd(hotwire."10_View/Safety/Fire_wardens");
drop view hotwire."10_View/Safety/Fire_warden_areas";
drop view hotwire."10_View/Safety/Fire_wardens";
drop view hotwire.fire_warden_area_hid;
drop view hotwire.deputy_fire_warden_hid;
drop view hotwire.fire_warden_hid;
drop view _fire_warden_status;
-- drop function fn_fire_warden_qualification_in_date(bigint);
-- drop table mm_fire_warden_area_fire_warden;
drop table fire_warden_area;
drop index idx_primary_fire_warden ;
drop table fire_warden;
