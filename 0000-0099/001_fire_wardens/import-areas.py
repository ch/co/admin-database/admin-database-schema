#!/usr/bin/python

import chemaccmgmt
import pgdb
import csv

f = open('areas.csv')
r = csv.reader(f)

dbconn = chemaccmgmt.db.ChemDB(username='cen1001',database='chemistry')
sql = 'insert into fire_warden_area ( name, location, building_id,building_floor_id ) values ( %s, %s, %s, %s )'

for row in r:
   n = row[0]
   l = row[1]
   b = int(row[2])
   try:
       f = int(row[3])
   except:
       f = None

   dbconn.cursor.execute(sql,[n,l,b,f])

dbconn.conn.commit()

dbconn.conn.close()
