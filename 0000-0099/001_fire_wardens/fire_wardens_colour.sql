
create or replace view hotwire."10_View/Safety/Fire_wardens" as
select
    fire_warden_id as id,
    person_id,
    qualification_date,
    fire_warden_area.fire_warden_area_id,
    fire_warden_area.building_id as ro_building_id,
    fire_warden_area.building_region_id as ro_building_region_id,
    fire_warden_area.building_floor_id as ro_building_floor_id,
    is_primary as main_firewarden_for_area,
    array(
        select mm_person_dept_telephone_number.dept_telephone_number_id
        from mm_person_dept_telephone_number
        where fire_warden.person_id = mm_person_dept_telephone_number.person_id
    ) as dept_telephone_number_id,
    _physical_status_v3.status_id as ro_physical_status,
    _fire_warden_status.requalification_date::date as ro_requalification_date,
    _fire_warden_status.in_date as ro_qualification_in_date,
    _latest_role_v12.post_category as ro_post_category,
    case
        when _physical_status_v3.status_id <> 'Current' then 'orange'::text 
        when _fire_warden_status.in_date = 'f' then 'red'::text
        when _fire_warden_status.in_date is null then 'blue'::text
        else null::text
    end as _cssclass
from fire_warden
left join _latest_role_v12 using(person_id)
left join fire_warden_area using (fire_warden_area_id)
left join _physical_status_v3 using (person_id)
left join _fire_warden_status using (fire_warden_id)
;
alter view hotwire."10_View/Safety/Fire_wardens" owner to dev;
grant select,insert,update,delete on hotwire."10_View/Safety/Fire_wardens" to safety_management;
grant select on hotwire."10_View/Safety/Fire_wardens" to mgmt_ro;

