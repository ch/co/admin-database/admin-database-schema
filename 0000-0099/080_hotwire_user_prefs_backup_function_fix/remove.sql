CREATE OR REPLACE FUNCTION hotwire_user_migration_script()
  RETURNS SETOF character varying AS
$BODY$ select $$update hotwire."hw_User Preferences" set user_id = ( select usesysid from pg_user where usename = '$$ || usename || $$') where user_id = $$ || usesysid || ';' from pg_user; $BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION hotwire_user_migration_script()
  OWNER TO cen1001;
