CREATE TABLE sick_leave
(
  id bigserial NOT NULL,
  self_certification_start_date date,
  self_certification_last_day_sick date,
  self_certification_first_day_back date,
  ssp_date_signed_off date,
  ssp_date_returned_to_work date,
  person_id bigint NOT NULL,
  total_self_certification_days integer,
  total_self_certification_days_notes character varying(50),
  total_ssp_days integer,
  total_ssp_days_notes character varying(30),
  CONSTRAINT pk_sick_leave PRIMARY KEY (id),
  CONSTRAINT sick_leave_fk_person FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sick_leave
  OWNER TO cen1001;
GRANT ALL ON TABLE sick_leave TO cen1001;
GRANT ALL ON TABLE sick_leave TO dev;

CREATE TRIGGER sick_leave_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON sick_leave
  FOR EACH ROW
  EXECUTE PROCEDURE audit_generic();

