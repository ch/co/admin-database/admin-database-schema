CREATE OR REPLACE VIEW hotwire."10_View/People/Personnel_History_old" AS 
 SELECT a.id, a.ro_person, a.surname, a.first_names, a.title_id, a.ro_age, a.ro_post_category_id, a.continuous_employment_start_date, a.ro_latest_employment_start_date, a.ro_employment_end_date, a.ro_length_of_continuous_service, a.ro_length_of_service_in_current_contract, a.ro_final_length_of_continuous_service, a.ro_final_length_of_service_in_last_contract, a.ro_supervisor_id, a.ro_mentor_id, a.notes, a.other_information, a.ro_extra_filemaker_data, a.ro_filemaker_history, a._cssclass, a.staff_review_history_subview, a.misc_leave_subview, a.cambridge_history_subview
   FROM ( SELECT person.id, person_hid.person_hid AS ro_person, person.surname, person.first_names, person.title_id, date_part('year'::text, age('now'::text::date::timestamp with time zone, person.date_of_birth::timestamp with time zone)) AS ro_age, _latest_role.post_category_id AS ro_post_category_id, person.continuous_employment_start_date, _latest_employment.start_date AS ro_latest_employment_start_date, COALESCE(_latest_employment.end_date, _latest_employment.intended_end_date) AS ro_employment_end_date, btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text) AS ro_length_of_continuous_service, btrim(age(COALESCE(_latest_employment.end_date, 'now'::text::date)::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text) AS ro_length_of_service_in_current_contract, btrim(age(_latest_employment.end_date::timestamp with time zone, person.continuous_employment_start_date::timestamp with time zone)::text, '@'::text) AS ro_final_length_of_continuous_service, btrim(age(_latest_employment.end_date::timestamp with time zone, _latest_employment.start_date::timestamp with time zone)::text, '@'::text) AS ro_final_length_of_service_in_last_contract, _latest_role.supervisor_id AS ro_supervisor_id, _latest_role.mentor_id AS ro_mentor_id, person.notes, person.other_information, person.extra_filemaker_data AS ro_extra_filemaker_data, person.filemaker_history AS ro_filemaker_history, 
                CASE
                    WHEN _physical_status_v2.status_id::text = 'Past'::text THEN 'orange'::text
                    WHEN _physical_status_v2.status_id::text = 'Unknown'::text AND person._status::text = 'Past'::text THEN 'orange'::text
                    ELSE NULL::text
                END AS _cssclass, _to_hwsubview('person_id'::text, '10_View/People/Staff_Review_History'::text, 'id'::text) AS staff_review_history_subview, _to_hwsubview('person_id'::text, '10_View/People/Misc_Leave'::text, 'id'::text) AS misc_leave_subview, _to_hwsubview('person_id'::text, '10_View/Roles/Cambridge_History_V2'::text, 'id'::text) AS cambridge_history_subview
           FROM person
      LEFT JOIN _latest_role_v8 _latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
   LEFT JOIN person_hid ON person_hid.person_id = person.id
   LEFT JOIN _physical_status_v2 ON _physical_status_v2.id = person.id) a
  ORDER BY a.surname, a.first_names;

ALTER TABLE hotwire."10_View/People/Personnel_History_old"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History_old" TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/People/Personnel_History_old" TO dev;

CREATE OR REPLACE RULE hotwire_view_personnel_history_upd AS
    ON UPDATE TO hotwire."10_View/People/Personnel_History_old" DO INSTEAD  UPDATE person SET surname = new.surname, first_names = new.first_names, title_id = new.title_id, continuous_employment_start_date = new.continuous_employment_start_date, notes = new.notes, other_information = new.other_information
  WHERE person.id = old.id;


