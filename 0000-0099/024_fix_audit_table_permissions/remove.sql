alter table _audit_2009 owner to rl201;
alter table _audit_2010 owner to rl201;
alter table _audit_2011 owner to rl201;
alter table _audit_2012 owner to rl201;
alter table _audit_2013 owner to rl201;
alter table _audit_2014 owner to rl201;

grant select on _audit_2009 to dev;
grant select on _audit_2010 to dev;
grant select on _audit_2011 to dev;
grant select on _audit_2012 to dev;
grant select on _audit_2013 to dev;
grant select on _audit_2014 to dev;
