alter table _audit_2009 owner to dev;
alter table _audit_2010 owner to dev;
alter table _audit_2011 owner to dev;
alter table _audit_2012 owner to dev;
alter table _audit_2013 owner to dev;
alter table _audit_2014 owner to dev;

revoke all on _audit_2009 from rl201;
revoke all on _audit_2010 from rl201;
revoke all on _audit_2011 from rl201;
revoke all on _audit_2012 from rl201;
revoke all on _audit_2013 from rl201;
revoke all on _audit_2014 from rl201;
