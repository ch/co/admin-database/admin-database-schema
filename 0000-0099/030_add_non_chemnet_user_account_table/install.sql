create table non_chemnet_user_account (
 id serial,
 username character varying(32) NOT NULL,
 realm character varying(72) NOT NULL,
 token character varying(20) NOT NULL,
 email_address character varying(48) NOT NULL,
 description character varying(500),
 expiry_date date,
 is_disabled boolean DEFAULT false not null
 );

 alter table non_chemnet_user_account add primary key (username, realm);
 
alter table non_chemnet_user_account owner to dev;

