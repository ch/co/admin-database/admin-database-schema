--
-- Name: dept_telephone_number_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.dept_telephone_number_hid OWNER TO sjt71;

--
-- Name: research_group_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.research_group_hid OWNER TO sjt71;

--
-- Name: room_hid; Type: VIEW; Schema: public; Owner: sjt71
--


ALTER TABLE public.room_hid OWNER TO sjt71;

--
-- Name: person_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.person_hid OWNER TO sjt71;

--
-- Name: ip_address_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.ip_address_hid OWNER TO sjt71;

--
-- Name: operating_system_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.operating_system_hid OWNER TO sjt71;

--
-- Name: supervisor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.supervisor_hid OWNER TO sjt71;

--
-- Name: postgraduate_studentship_type_hid; Type: VIEW; Schema: public; Owner: sjt71
--


ALTER TABLE public.postgraduate_studentship_type_hid OWNER TO sjt71;

--
-- Name: staff_category_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.staff_category_hid OWNER TO sjt71;

--
-- Name: hardware_type_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.hardware_type_hid OWNER TO sjt71;

--
-- Name: mentor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.mentor_hid OWNER TO sjt71;

--
-- Name: postgraduate_studentship_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.postgraduate_studentship_hid OWNER TO sjt71;

--
-- Name: subnet_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.subnet_hid OWNER TO sjt71;

--
-- Name: room_type_hid; Type: VIEW; Schema: public; Owner: sjt71
--

ALTER TABLE public.room_type_hid OWNER TO sjt71;

--
-- Name: backup_strategy_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.backup_strategy_hid AS
SELECT backup_strategy.id AS backup_strategy_id, backup_strategy.name AS backup_strategy_hid FROM public.backup_strategy;


ALTER TABLE public.backup_strategy_hid OWNER TO sjt71;

--
-- Name: cabinet_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.cabinet_hid AS
SELECT cabinet.id AS cabinet_id, cabinet.name AS cabinet_hid FROM public.cabinet;


ALTER TABLE public.cabinet_hid OWNER TO sjt71;

--
-- Name: cambridge_college_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.cambridge_college_hid AS
SELECT cambridge_college.id AS cambridge_college_id, cambridge_college.name AS cambridge_college_hid FROM public.cambridge_college;


ALTER TABLE public.cambridge_college_hid OWNER TO sjt71;

--
-- Name: first_mentor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.first_mentor_hid AS
SELECT person.id AS first_mentor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS first_mentor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE public.first_mentor_hid OWNER TO sjt71;

--
-- Name: first_supervisor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.first_supervisor_hid AS
SELECT person.id AS first_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS first_supervisor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE public.first_supervisor_hid OWNER TO sjt71;

--
-- Name: funding_source_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.funding_source_hid AS
SELECT funding_source.id AS funding_source_id, ((funding_source.source)::text || COALESCE(((' ('::text || (funding_source.reference_number)::text) || ')'::text), ''::text)) AS funding_source_hid FROM public.funding_source;


ALTER TABLE public.funding_source_hid OWNER TO sjt71;

--
-- Name: funding_type_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.funding_type_hid AS
SELECT funding_type.id AS funding_type_id, funding_type.type AS funding_type_hid FROM public.funding_type;


ALTER TABLE public.funding_type_hid OWNER TO sjt71;

--
-- Name: head_of_group_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.head_of_group_hid AS
SELECT person_hid.person_id AS head_of_group_id, person_hid.person_hid AS head_of_group_hid FROM public.person_hid;


ALTER TABLE public.head_of_group_hid OWNER TO sjt71;

--
-- Name: owner_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.owner_hid AS
SELECT person.id AS owner_id, (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text) AS owner_hid FROM public.person ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER TABLE public.owner_hid OWNER TO sjt71;

--
-- Name: patch_strategy_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.patch_strategy_hid AS
SELECT patch_strategy.id AS patch_strategy_id, patch_strategy.name AS patch_strategy_hid FROM public.patch_strategy;


ALTER TABLE public.patch_strategy_hid OWNER TO sjt71;

--
-- Name: second_mentor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.second_mentor_hid AS
SELECT person.id AS second_mentor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS second_mentor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE public.second_mentor_hid OWNER TO sjt71;

--
-- Name: second_supervisor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.second_supervisor_hid AS
SELECT person.id AS second_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS second_supervisor_hid FROM public.person ORDER BY (((person.surname)::text || ', '::text) || (person.first_names)::text);


ALTER TABLE public.second_supervisor_hid OWNER TO sjt71;

--
-- Name: substitute_supervisor_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.substitute_supervisor_hid AS
SELECT person.id AS substitute_supervisor_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS substitute_supervisor_hid FROM public.person;


ALTER TABLE public.substitute_supervisor_hid OWNER TO sjt71;

--
-- Name: supervisor_person_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.supervisor_person_hid AS
SELECT person.id AS supervisor_person_id, (((person.surname)::text || ', '::text) || (person.first_names)::text) AS supervisor_person_hid FROM public.person;


ALTER TABLE public.supervisor_person_hid OWNER TO sjt71;

--
-- Name: user_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.user_hid AS
SELECT person.id AS user_id, (((person.surname)::text || ', '::text) || (COALESCE(person.known_as, person.first_names))::text) AS user_hid FROM public.person ORDER BY person.surname, COALESCE(person.known_as, person.first_names);


ALTER TABLE public.user_hid OWNER TO sjt71;

--
-- Name: virus_detection_strategy_hid; Type: VIEW; Schema: public; Owner: sjt71
--

CREATE VIEW public.virus_detection_strategy_hid AS
SELECT virus_detection_strategy.id AS virus_detection_strategy_id, virus_detection_strategy.name AS virus_detection_strategy_hid FROM public.virus_detection_strategy;


ALTER TABLE public.virus_detection_strategy_hid OWNER TO sjt71;

--
-- Name: TABLE dept_telephone_number_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.dept_telephone_number_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.dept_telephone_number_hid FROM sjt71;
GRANT ALL ON TABLE public.dept_telephone_number_hid TO sjt71;
GRANT SELECT ON TABLE public.dept_telephone_number_hid TO old_cos;
GRANT SELECT ON TABLE public.dept_telephone_number_hid TO mgmt_ro;
GRANT ALL ON TABLE public.dept_telephone_number_hid TO dev;
GRANT SELECT ON TABLE public.dept_telephone_number_hid TO ro_hid;


--
-- Name: TABLE research_group_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.research_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.research_group_hid FROM sjt71;
GRANT ALL ON TABLE public.research_group_hid TO sjt71;
GRANT ALL ON TABLE public.research_group_hid TO dev;
GRANT SELECT ON TABLE public.research_group_hid TO old_cos;
GRANT SELECT ON TABLE public.research_group_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.research_group_hid TO ipreg;
GRANT SELECT ON TABLE public.research_group_hid TO reception;
GRANT SELECT ON TABLE public.research_group_hid TO space_management;
GRANT SELECT ON TABLE public.research_group_hid TO phones;
GRANT SELECT ON TABLE public.research_group_hid TO student_management;
GRANT SELECT ON TABLE public.research_group_hid TO ro_hid;
GRANT SELECT ON TABLE public.research_group_hid TO selfservice;
GRANT SELECT ON TABLE public.research_group_hid TO adreconcile;
GRANT SELECT ON TABLE public.research_group_hid TO groupitreps;
GRANT SELECT ON TABLE public.research_group_hid TO www_sites;
GRANT SELECT ON TABLE public.research_group_hid TO chemnettokens;


--
-- Name: TABLE room_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.room_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.room_hid FROM sjt71;
GRANT ALL ON TABLE public.room_hid TO sjt71;
GRANT SELECT ON TABLE public.room_hid TO old_cos;
GRANT ALL ON TABLE public.room_hid TO dev;
GRANT SELECT ON TABLE public.room_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.room_hid TO ipreg;
GRANT SELECT ON TABLE public.room_hid TO space_management;
GRANT SELECT ON TABLE public.room_hid TO ro_hid;
GRANT SELECT ON TABLE public.room_hid TO groupitreps;


--
-- Name: TABLE person_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.person_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.person_hid FROM sjt71;
GRANT ALL ON TABLE public.person_hid TO sjt71;
GRANT ALL ON TABLE public.person_hid TO dev;
GRANT SELECT ON TABLE public.person_hid TO old_cos;
GRANT SELECT ON TABLE public.person_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.person_hid TO space_management;
GRANT SELECT ON TABLE public.person_hid TO ro_hid;
GRANT SELECT ON TABLE public.person_hid TO selfservice;
GRANT SELECT ON TABLE public.person_hid TO www_sites;
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE public.person_hid TO cos;
GRANT SELECT ON TABLE public.person_hid TO ipreg;


--
-- Name: TABLE ip_address_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.ip_address_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.ip_address_hid FROM sjt71;
GRANT ALL ON TABLE public.ip_address_hid TO sjt71;
GRANT SELECT ON TABLE public.ip_address_hid TO old_cos;
GRANT ALL ON TABLE public.ip_address_hid TO dev;
GRANT SELECT ON TABLE public.ip_address_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.ip_address_hid TO ro_hid;
GRANT SELECT ON TABLE public.ip_address_hid TO groupitreps;


--
-- Name: TABLE operating_system_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.operating_system_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.operating_system_hid FROM sjt71;
GRANT ALL ON TABLE public.operating_system_hid TO sjt71;
GRANT SELECT ON TABLE public.operating_system_hid TO old_cos;
GRANT SELECT ON TABLE public.operating_system_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.operating_system_hid TO ipreg;
GRANT SELECT ON TABLE public.operating_system_hid TO ro_hid;
GRANT SELECT ON TABLE public.operating_system_hid TO groupitreps;
GRANT ALL ON TABLE public.operating_system_hid TO dev;


--
-- Name: TABLE supervisor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.supervisor_hid FROM sjt71;
GRANT ALL ON TABLE public.supervisor_hid TO sjt71;
GRANT SELECT ON TABLE public.supervisor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.supervisor_hid TO ro_hid;
GRANT SELECT ON TABLE public.supervisor_hid TO selfservice;


--
-- Name: TABLE postgraduate_studentship_type_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.postgraduate_studentship_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.postgraduate_studentship_type_hid FROM sjt71;
GRANT ALL ON TABLE public.postgraduate_studentship_type_hid TO sjt71;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO old_cos;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO dev;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO admin_team;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO space_management;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO phones;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO student_management;
GRANT SELECT ON TABLE public.postgraduate_studentship_type_hid TO ro_hid;


--
-- Name: TABLE staff_category_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.staff_category_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.staff_category_hid FROM sjt71;
GRANT ALL ON TABLE public.staff_category_hid TO sjt71;
GRANT SELECT ON TABLE public.staff_category_hid TO dev;
GRANT SELECT ON TABLE public.staff_category_hid TO old_cos;
GRANT SELECT ON TABLE public.staff_category_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.staff_category_hid TO space_management;
GRANT SELECT ON TABLE public.staff_category_hid TO admin_team;
GRANT SELECT ON TABLE public.staff_category_hid TO phones;
GRANT SELECT ON TABLE public.staff_category_hid TO student_management;
GRANT SELECT ON TABLE public.staff_category_hid TO ro_hid;


--
-- Name: TABLE hardware_type_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.hardware_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.hardware_type_hid FROM sjt71;
GRANT ALL ON TABLE public.hardware_type_hid TO sjt71;
GRANT SELECT ON TABLE public.hardware_type_hid TO old_cos;
GRANT SELECT ON TABLE public.hardware_type_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.hardware_type_hid TO ipreg;
GRANT SELECT ON TABLE public.hardware_type_hid TO ro_hid;
GRANT SELECT ON TABLE public.hardware_type_hid TO groupitreps;
GRANT ALL ON TABLE public.hardware_type_hid TO dev;


--
-- Name: TABLE mentor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.mentor_hid FROM sjt71;
GRANT ALL ON TABLE public.mentor_hid TO sjt71;
GRANT SELECT ON TABLE public.mentor_hid TO dev;
GRANT SELECT ON TABLE public.mentor_hid TO admin_team;
GRANT SELECT ON TABLE public.mentor_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.mentor_hid TO ro_hid;


--
-- Name: TABLE postgraduate_studentship_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.postgraduate_studentship_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.postgraduate_studentship_hid FROM sjt71;
GRANT ALL ON TABLE public.postgraduate_studentship_hid TO sjt71;
GRANT SELECT ON TABLE public.postgraduate_studentship_hid TO PUBLIC;
GRANT SELECT ON TABLE public.postgraduate_studentship_hid TO ro_hid;


--
-- Name: TABLE subnet_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.subnet_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.subnet_hid FROM sjt71;
GRANT ALL ON TABLE public.subnet_hid TO sjt71;
GRANT SELECT ON TABLE public.subnet_hid TO old_cos;
GRANT SELECT ON TABLE public.subnet_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.subnet_hid TO ro_hid;


--
-- Name: TABLE room_type_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.room_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.room_type_hid FROM sjt71;
GRANT ALL ON TABLE public.room_type_hid TO sjt71;
GRANT SELECT ON TABLE public.room_type_hid TO old_cos;
GRANT SELECT ON TABLE public.room_type_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.room_type_hid TO space_management;
GRANT SELECT ON TABLE public.room_type_hid TO ro_hid;


--
-- Name: TABLE backup_strategy_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.backup_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.backup_strategy_hid FROM sjt71;
GRANT ALL ON TABLE public.backup_strategy_hid TO sjt71;
GRANT SELECT ON TABLE public.backup_strategy_hid TO old_cos;
GRANT SELECT ON TABLE public.backup_strategy_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.backup_strategy_hid TO ro_hid;


--
-- Name: TABLE cabinet_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.cabinet_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.cabinet_hid FROM sjt71;
GRANT ALL ON TABLE public.cabinet_hid TO sjt71;
GRANT SELECT ON TABLE public.cabinet_hid TO old_cos;
GRANT SELECT ON TABLE public.cabinet_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.cabinet_hid TO ro_hid;


--
-- Name: TABLE cambridge_college_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.cambridge_college_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.cambridge_college_hid FROM sjt71;
GRANT ALL ON TABLE public.cambridge_college_hid TO sjt71;
GRANT SELECT ON TABLE public.cambridge_college_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.cambridge_college_hid TO ro_hid;
GRANT SELECT ON TABLE public.cambridge_college_hid TO newusersignup;


--
-- Name: TABLE first_mentor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.first_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.first_mentor_hid FROM sjt71;
GRANT ALL ON TABLE public.first_mentor_hid TO sjt71;
GRANT SELECT ON TABLE public.first_mentor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.first_mentor_hid TO ro_hid;


--
-- Name: TABLE first_supervisor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.first_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.first_supervisor_hid FROM sjt71;
GRANT ALL ON TABLE public.first_supervisor_hid TO sjt71;
GRANT SELECT ON TABLE public.first_supervisor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.first_supervisor_hid TO ro_hid;


--
-- Name: TABLE funding_source_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.funding_source_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.funding_source_hid FROM sjt71;
GRANT ALL ON TABLE public.funding_source_hid TO sjt71;
GRANT SELECT ON TABLE public.funding_source_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.funding_source_hid TO old_cos;
GRANT SELECT ON TABLE public.funding_source_hid TO admin_team;
GRANT SELECT ON TABLE public.funding_source_hid TO phones;
GRANT SELECT ON TABLE public.funding_source_hid TO space_management;
GRANT SELECT ON TABLE public.funding_source_hid TO student_management;
GRANT SELECT ON TABLE public.funding_source_hid TO dev;
GRANT SELECT ON TABLE public.funding_source_hid TO ro_hid;


--
-- Name: TABLE funding_type_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.funding_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.funding_type_hid FROM sjt71;
GRANT ALL ON TABLE public.funding_type_hid TO sjt71;
GRANT SELECT ON TABLE public.funding_type_hid TO PUBLIC;
GRANT SELECT ON TABLE public.funding_type_hid TO ro_hid;


--
-- Name: TABLE head_of_group_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.head_of_group_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.head_of_group_hid FROM sjt71;
GRANT ALL ON TABLE public.head_of_group_hid TO sjt71;
GRANT SELECT ON TABLE public.head_of_group_hid TO mgmt_ro;
GRANT ALL ON TABLE public.head_of_group_hid TO dev;
GRANT SELECT ON TABLE public.head_of_group_hid TO admin_team;
GRANT SELECT ON TABLE public.head_of_group_hid TO student_management;
GRANT SELECT ON TABLE public.head_of_group_hid TO ro_hid;


--
-- Name: TABLE owner_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.owner_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.owner_hid FROM sjt71;
GRANT ALL ON TABLE public.owner_hid TO sjt71;
GRANT SELECT ON TABLE public.owner_hid TO old_cos;
GRANT SELECT ON TABLE public.owner_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.owner_hid TO ro_hid;
GRANT SELECT ON TABLE public.owner_hid TO groupitreps;


--
-- Name: TABLE patch_strategy_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.patch_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.patch_strategy_hid FROM sjt71;
GRANT ALL ON TABLE public.patch_strategy_hid TO sjt71;
GRANT SELECT ON TABLE public.patch_strategy_hid TO old_cos;
GRANT SELECT ON TABLE public.patch_strategy_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.patch_strategy_hid TO ro_hid;


--
-- Name: TABLE second_mentor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.second_mentor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.second_mentor_hid FROM sjt71;
GRANT ALL ON TABLE public.second_mentor_hid TO sjt71;
GRANT SELECT ON TABLE public.second_mentor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.second_mentor_hid TO ro_hid;


--
-- Name: TABLE second_supervisor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.second_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.second_supervisor_hid FROM sjt71;
GRANT ALL ON TABLE public.second_supervisor_hid TO sjt71;
GRANT SELECT ON TABLE public.second_supervisor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.second_supervisor_hid TO ro_hid;


--
-- Name: TABLE substitute_supervisor_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.substitute_supervisor_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.substitute_supervisor_hid FROM sjt71;
GRANT ALL ON TABLE public.substitute_supervisor_hid TO sjt71;
GRANT SELECT ON TABLE public.substitute_supervisor_hid TO PUBLIC;
GRANT SELECT ON TABLE public.substitute_supervisor_hid TO ro_hid;


--
-- Name: TABLE supervisor_person_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.supervisor_person_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.supervisor_person_hid FROM sjt71;
GRANT ALL ON TABLE public.supervisor_person_hid TO sjt71;
GRANT SELECT ON TABLE public.supervisor_person_hid TO dev;
GRANT SELECT ON TABLE public.supervisor_person_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.supervisor_person_hid TO ro_hid;


--
-- Name: TABLE user_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.user_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.user_hid FROM sjt71;
GRANT ALL ON TABLE public.user_hid TO sjt71;
GRANT SELECT ON TABLE public.user_hid TO old_cos;
GRANT SELECT ON TABLE public.user_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.user_hid TO ro_hid;
GRANT SELECT ON TABLE public.user_hid TO groupitreps;


--
-- Name: TABLE virus_detection_strategy_hid; Type: ACL; Schema: public; Owner: sjt71
--

REVOKE ALL ON TABLE public.virus_detection_strategy_hid FROM PUBLIC;
REVOKE ALL ON TABLE public.virus_detection_strategy_hid FROM sjt71;
GRANT ALL ON TABLE public.virus_detection_strategy_hid TO sjt71;
GRANT SELECT ON TABLE public.virus_detection_strategy_hid TO old_cos;
GRANT SELECT ON TABLE public.virus_detection_strategy_hid TO mgmt_ro;
GRANT SELECT ON TABLE public.virus_detection_strategy_hid TO ro_hid;


--
-- PostgreSQL database dump complete
--

REVOKE ALL ON TABLE public.building_region_hid FROM dev;

