--
-- Name: departmental_committee_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.departmental_committee_hid AS
SELECT workload_committee.id AS departmental_committee_id, workload_committee.name AS departmental_committee_hid FROM public.workload_committee WHERE workload_committee.departmental;


ALTER TABLE hotwire.departmental_committee_hid OWNER TO rl201;

--
-- Name: dept_supervision_tripos_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.dept_supervision_tripos_hid AS
SELECT examination.id AS dept_supervision_tripos_id, examination.name AS dept_supervision_tripos_hid FROM public.examination WHERE ((NOT examination.postgrad) AND examination.supervised_by_dept);


ALTER TABLE hotwire.dept_supervision_tripos_hid OWNER TO rl201;

--
-- Name: examination_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hotwire.examination_hid AS
SELECT examination.id AS examination_id, examination.name AS examination_hid FROM public.examination;


ALTER TABLE hotwire.examination_hid OWNER TO postgres;

--
-- Name: external_committee_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.external_committee_hid AS
SELECT workload_committee.id AS external_committee_id, workload_committee.name AS external_committee_hid FROM public.workload_committee WHERE ((NOT workload_committee.ucam) AND (NOT workload_committee.departmental));


ALTER TABLE hotwire.external_committee_hid OWNER TO rl201;

--
-- Name: external_examiner_role_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.external_examiner_role_hid AS
SELECT workload_examining_role.id AS external_examiner_role_id, workload_examining_role.name AS external_examiner_role_hid FROM public.workload_examining_role WHERE (NOT workload_examining_role.departmental);


ALTER TABLE hotwire.external_examiner_role_hid OWNER TO rl201;

--
-- Name: internal_pg_examiner_role_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.internal_pg_examiner_role_hid AS
SELECT workload_examining_role.id AS internal_pg_examiner_role_id, workload_examining_role.name AS internal_pg_examiner_role_hid FROM public.workload_examining_role WHERE (workload_examining_role.departmental AND workload_examining_role.postgrad);


ALTER TABLE hotwire.internal_pg_examiner_role_hid OWNER TO rl201;

--
-- Name: internal_postgraduate_exam_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.internal_postgraduate_exam_hid AS
SELECT examination.id AS internal_postgraduate_exam_id, examination.name AS internal_postgraduate_exam_hid FROM public.examination WHERE (examination.postgrad AND examination.valid_for_examination);


ALTER TABLE hotwire.internal_postgraduate_exam_hid OWNER TO rl201;

--
-- Name: internal_ug_examiner_role_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.internal_ug_examiner_role_hid AS
SELECT workload_examining_role.id AS internal_ug_examiner_role_id, workload_examining_role.name AS internal_ug_examiner_role_hid FROM public.workload_examining_role WHERE (workload_examining_role.departmental AND (NOT workload_examining_role.postgrad));


ALTER TABLE hotwire.internal_ug_examiner_role_hid OWNER TO rl201;

--
-- Name: marking_role_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.marking_role_hid AS
SELECT workload_marking_type.id AS marking_role_id, workload_marking_type.name AS marking_role_hid FROM public.workload_marking_type;


ALTER TABLE hotwire.marking_role_hid OWNER TO rl201;

--
-- Name: research_type_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.research_type_hid AS
SELECT workload_research_type.id AS research_type_id, workload_research_type.name AS research_type_hid FROM public.workload_research_type;


ALTER TABLE hotwire.research_type_hid OWNER TO rl201;

--
-- Name: tripos_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.tripos_hid AS
SELECT examination.id AS tripos_id, examination.name AS tripos_hid FROM public.examination WHERE (NOT examination.postgrad);


ALTER TABLE hotwire.tripos_hid OWNER TO rl201;

--
-- Name: undergraduate_exam_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.undergraduate_exam_hid AS
SELECT examination.id AS undergraduate_exam_id, examination.name AS undergraduate_exam_hid FROM public.examination WHERE (NOT examination.postgrad);


ALTER TABLE hotwire.undergraduate_exam_hid OWNER TO rl201;

--
-- Name: undergraduate_lecture_course_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW hotwire.undergraduate_lecture_course_hid AS
SELECT lecture_course.id AS undergraduate_lecture_course_id, lecture_course.name AS undergraduate_lecture_course_hid FROM (public.lecture_course JOIN public.examination ON ((lecture_course.tripos_id = examination.id))) WHERE (examination.postgrad = false);


ALTER TABLE hotwire.undergraduate_lecture_course_hid OWNER TO postgres;

--
-- Name: university_committee_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.university_committee_hid AS
SELECT workload_committee.id AS university_committee_id, workload_committee.name AS university_committee_hid FROM public.workload_committee WHERE (workload_committee.ucam AND (NOT workload_committee.departmental));


ALTER TABLE hotwire.university_committee_hid OWNER TO rl201;

--
-- Name: workload_college_type_hid; Type: VIEW; Schema: hotwire; Owner: rl201
--

CREATE VIEW hotwire.workload_college_type_hid AS
SELECT workload_college_type.id AS workload_college_type_id, workload_college_type.name AS workload_college_type_hid FROM public.workload_college_type;


ALTER TABLE hotwire.workload_college_type_hid OWNER TO rl201;

--
-- Name: TABLE departmental_committee_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.departmental_committee_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.departmental_committee_hid FROM rl201;
GRANT ALL ON TABLE hotwire.departmental_committee_hid TO rl201;
GRANT SELECT ON TABLE hotwire.departmental_committee_hid TO ro_hid;


--
-- Name: TABLE dept_supervision_tripos_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.dept_supervision_tripos_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.dept_supervision_tripos_hid FROM rl201;
GRANT ALL ON TABLE hotwire.dept_supervision_tripos_hid TO rl201;
GRANT SELECT ON TABLE hotwire.dept_supervision_tripos_hid TO ro_hid;


--
-- Name: TABLE examination_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hotwire.examination_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.examination_hid FROM postgres;
GRANT ALL ON TABLE hotwire.examination_hid TO postgres;
GRANT SELECT ON TABLE hotwire.examination_hid TO ro_hid;


--
-- Name: TABLE external_committee_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.external_committee_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.external_committee_hid FROM rl201;
GRANT ALL ON TABLE hotwire.external_committee_hid TO rl201;
GRANT SELECT ON TABLE hotwire.external_committee_hid TO ro_hid;


--
-- Name: TABLE external_examiner_role_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.external_examiner_role_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.external_examiner_role_hid FROM rl201;
GRANT ALL ON TABLE hotwire.external_examiner_role_hid TO rl201;
GRANT SELECT ON TABLE hotwire.external_examiner_role_hid TO ro_hid;


--
-- Name: TABLE internal_pg_examiner_role_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.internal_pg_examiner_role_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.internal_pg_examiner_role_hid FROM rl201;
GRANT ALL ON TABLE hotwire.internal_pg_examiner_role_hid TO rl201;
GRANT SELECT ON TABLE hotwire.internal_pg_examiner_role_hid TO ro_hid;


--
-- Name: TABLE internal_postgraduate_exam_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.internal_postgraduate_exam_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.internal_postgraduate_exam_hid FROM rl201;
GRANT ALL ON TABLE hotwire.internal_postgraduate_exam_hid TO rl201;
GRANT SELECT ON TABLE hotwire.internal_postgraduate_exam_hid TO ro_hid;


--
-- Name: TABLE internal_ug_examiner_role_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.internal_ug_examiner_role_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.internal_ug_examiner_role_hid FROM rl201;
GRANT ALL ON TABLE hotwire.internal_ug_examiner_role_hid TO rl201;
GRANT SELECT ON TABLE hotwire.internal_ug_examiner_role_hid TO ro_hid;


--
-- Name: TABLE marking_role_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.marking_role_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.marking_role_hid FROM rl201;
GRANT ALL ON TABLE hotwire.marking_role_hid TO rl201;
GRANT SELECT ON TABLE hotwire.marking_role_hid TO ro_hid;


--
-- Name: TABLE research_type_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.research_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.research_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.research_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.research_type_hid TO ro_hid;


--
-- Name: TABLE tripos_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.tripos_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.tripos_hid FROM rl201;
GRANT ALL ON TABLE hotwire.tripos_hid TO rl201;
GRANT SELECT ON TABLE hotwire.tripos_hid TO ro_hid;


--
-- Name: TABLE undergraduate_exam_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.undergraduate_exam_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.undergraduate_exam_hid FROM rl201;
GRANT ALL ON TABLE hotwire.undergraduate_exam_hid TO rl201;
GRANT SELECT ON TABLE hotwire.undergraduate_exam_hid TO ro_hid;


--
-- Name: TABLE undergraduate_lecture_course_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hotwire.undergraduate_lecture_course_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.undergraduate_lecture_course_hid FROM postgres;
GRANT ALL ON TABLE hotwire.undergraduate_lecture_course_hid TO postgres;
GRANT SELECT ON TABLE hotwire.undergraduate_lecture_course_hid TO ro_hid;


--
-- Name: TABLE university_committee_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.university_committee_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.university_committee_hid FROM rl201;
GRANT ALL ON TABLE hotwire.university_committee_hid TO rl201;
GRANT SELECT ON TABLE hotwire.university_committee_hid TO ro_hid;


--
-- Name: TABLE workload_college_type_hid; Type: ACL; Schema: hotwire; Owner: rl201
--

REVOKE ALL ON TABLE hotwire.workload_college_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hotwire.workload_college_type_hid FROM rl201;
GRANT ALL ON TABLE hotwire.workload_college_type_hid TO rl201;
GRANT SELECT ON TABLE hotwire.workload_college_type_hid TO ro_hid;
