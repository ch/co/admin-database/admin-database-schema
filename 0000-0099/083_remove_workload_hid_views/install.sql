DROP VIEW hotwire.departmental_committee_hid;
DROP VIEW hotwire.dept_supervision_tripos_hid;
DROP VIEW hotwire.examination_hid;
DROP VIEW hotwire.external_committee_hid;
DROP VIEW hotwire.external_examiner_role_hid;
DROP VIEW hotwire.internal_pg_examiner_role_hid;
DROP VIEW hotwire.internal_postgraduate_exam_hid;
DROP VIEW hotwire.internal_ug_examiner_role_hid;
DROP VIEW hotwire.marking_role_hid;
DROP VIEW hotwire.research_type_hid;
DROP VIEW hotwire.tripos_hid;
DROP VIEW hotwire.undergraduate_exam_hid;
DROP VIEW hotwire.undergraduate_lecture_course_hid;
DROP VIEW hotwire.university_committee_hid;
DROP VIEW hotwire.workload_college_type_hid;
