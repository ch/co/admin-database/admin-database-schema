CREATE OR REPLACE VIEW system_image_basic_view AS 
 SELECT system_image.id, system_image.hardware_id, system_image.operating_system_id, system_image.comments
   FROM system_image;

ALTER TABLE system_image_basic_view
  OWNER TO sjt71;
GRANT ALL ON TABLE system_image_basic_view TO sjt71;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE system_image_basic_view TO old_cos;
GRANT ALL ON TABLE system_image_basic_view TO dev;

-- Rule: system_image_basic_del ON system_image_basic_view

-- DROP RULE system_image_basic_del ON system_image_basic_view;

CREATE OR REPLACE RULE system_image_basic_del AS
    ON DELETE TO system_image_basic_view DO INSTEAD  DELETE FROM system_image
  WHERE system_image.id = old.id;

-- Rule: system_image_basic_ins ON system_image_basic_view

-- DROP RULE system_image_basic_ins ON system_image_basic_view;

CREATE OR REPLACE RULE system_image_basic_ins AS
    ON INSERT TO system_image_basic_view DO INSTEAD  INSERT INTO system_image (hardware_id, operating_system_id, comments) 
  VALUES (new.hardware_id, new.operating_system_id, new.comments)
  RETURNING system_image.id, NULL::bigint AS int8, NULL::bigint AS int8, system_image.comments;

-- Rule: system_image_basic_upd ON system_image_basic_view

-- DROP RULE system_image_basic_upd ON system_image_basic_view;

CREATE OR REPLACE RULE system_image_basic_upd AS
    ON UPDATE TO system_image_basic_view DO INSTEAD  UPDATE system_image SET hardware_id = new.hardware_id, operating_system_id = new.operating_system_id, comments = new.comments
  WHERE system_image.id = old.id;


