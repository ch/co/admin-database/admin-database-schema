CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Extract_Arms_and_Canopies" AS 
 SELECT extract_arm_canopy.id, extract_arm_canopy.room_id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id
   FROM extract_arm_canopy;

ALTER TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Extract_Arms_and_Canopies" TO safety_management;

-- Rule: hotwire3_view_extract_arm_canopies_del ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire3_view_extract_arm_canopies_del ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire3_view_extract_arm_canopies_del AS
    ON DELETE TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  DELETE FROM extract_arm_canopy
  WHERE extract_arm_canopy.id = old.id;

-- Rule: hotwire3_view_extract_arm_canopies_upd ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire3_view_extract_arm_canopies_upd ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire3_view_extract_arm_canopies_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  UPDATE extract_arm_canopy SET room_id = new.room_id, extract_device_type_id = new.extract_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes, extract_arm_canopy_type_id = new.extract_arm_canopy_type_id
  WHERE extract_arm_canopy.id = old.id;

-- Rule: hotwire3_view_extract_arm_canopy_ins ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire3_view_extract_arm_canopy_ins ON hotwire3."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire3_view_extract_arm_canopy_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  INSERT INTO extract_arm_canopy (room_id, extract_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes, extract_arm_canopy_type_id) 
  VALUES (new.room_id, new.extract_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes, new.extract_arm_canopy_type_id)
  RETURNING extract_arm_canopy.id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.room_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id;


CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Glove_Boxes" AS 
 SELECT glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes
   FROM glove_box;

ALTER TABLE hotwire3."10_View/Safety/Glove_Boxes"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Glove_Boxes" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Glove_Boxes" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Glove_Boxes" TO safety_management;

-- Rule: hotwire3_view_glove_box_ins ON hotwire3."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire3_view_glove_box_ins ON hotwire3."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire3_view_glove_box_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  INSERT INTO glove_box (room_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, notes) 
  VALUES (new.room_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.notes)
  RETURNING glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes;

-- Rule: hotwire3_view_glove_boxes_del ON hotwire3."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire3_view_glove_boxes_del ON hotwire3."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire3_view_glove_boxes_del AS
    ON DELETE TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  DELETE FROM glove_box
  WHERE glove_box.id = old.id;

-- Rule: hotwire3_view_glove_boxes_upd ON hotwire3."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire3_view_glove_boxes_upd ON hotwire3."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire3_view_glove_boxes_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Glove_Boxes" DO INSTEAD  UPDATE glove_box SET room_id = new.room_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, notes = new.notes
  WHERE glove_box.id = old.id;


CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Vented_Enclosures" AS 
 SELECT vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes
   FROM vented_enclosure;

ALTER TABLE hotwire3."10_View/Safety/Vented_Enclosures"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Safety/Vented_Enclosures" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Vented_Enclosures" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Safety/Vented_Enclosures" TO safety_management;

-- Rule: hotwire3_view_vented_enclosure_ins ON hotwire3."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire3_view_vented_enclosure_ins ON hotwire3."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire3_view_vented_enclosure_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  INSERT INTO vented_enclosure (room_id, vented_enclosure_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes) 
  VALUES (new.room_id, new.vented_enclosure_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes)
  RETURNING vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes;

-- Rule: hotwire3_view_vented_enclosures_del ON hotwire3."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire3_view_vented_enclosures_del ON hotwire3."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire3_view_vented_enclosures_del AS
    ON DELETE TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  DELETE FROM vented_enclosure
  WHERE vented_enclosure.id = old.id;

-- Rule: hotwire3_view_vented_enclosures_upd ON hotwire3."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire3_view_vented_enclosures_upd ON hotwire3."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire3_view_vented_enclosures_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Vented_Enclosures" DO INSTEAD  UPDATE vented_enclosure SET room_id = new.room_id, vented_enclosure_device_type_id = new.vented_enclosure_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes
  WHERE vented_enclosure.id = old.id;


