CREATE FUNCTION public.person_propagate_system_image_bans() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'UPDATE')
    THEN
        -- Exit if the flag we are interested in has not changed
        IF NEW.auto_banned_from_network IS NOT DISTINCT FROM OLD.auto_banned_from_network
        THEN
            RETURN NEW;
        END IF;
    END IF;

    IF NEW.auto_banned_from_network = 'f' OR NEW.auto_banned_from_network IS NULL
    THEN
        -- update system images and reset their override flags
        UPDATE system_image SET 
            auto_banned_from_network = 'f',
            override_auto_ban = 'f',
            network_ban_log = coalesce(network_ban_log,'') || 'Possibly allowed on network at ' || current_timestamp || ' because person ' || NEW.first_names || ' ' || NEW.surname || E' is allowed\n'
        FROM hardware
        WHERE system_image.hardware_id = hardware.id AND (hardware.owner_id = NEW.id OR system_image.user_id = NEW.id);
    ELSE
        -- auto_banned must be true
        -- A person's own override flag should not affect their associated systems, because 
        -- a system can be multiuser, so we don't check that
        UPDATE system_image SET 
            auto_banned_from_network = 't',
            network_ban_log = coalesce(network_ban_log,'') || 'Banned from network at ' || current_timestamp || ' because person ' || NEW.first_names || ' ' || NEW.surname || E' is banned\n'
        FROM hardware
        WHERE system_image.hardware_id = hardware.id AND (hardware.owner_id = NEW.id OR system_image.user_id = NEW.id);
    END IF;
    RETURN NEW;
END;
$$ 
LANGUAGE plpgsql 
SECURITY DEFINER
SET search_path = public, pg_temp;

GRANT CREATE ON SCHEMA public TO osbuilder;
ALTER FUNCTION person_propagate_system_image_bans() OWNER TO osbuilder;
REVOKE CREATE ON SCHEMA public FROM osbuilder;

REVOKE ALL ON FUNCTION person_propagate_system_image_bans() FROM PUBLIC;
GRANT EXECUTE ON FUNCTION person_propagate_system_image_bans() TO dev,cos,hr,student_management,photography,reception_cover,reception;

CREATE TRIGGER propagate_system_image_bans AFTER UPDATE OR INSERT ON person FOR EACH ROW EXECUTE PROCEDURE person_propagate_system_image_bans();

