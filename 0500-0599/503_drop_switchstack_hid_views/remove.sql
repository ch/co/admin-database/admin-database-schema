CREATE OR REPLACE VIEW switchstack_hid AS 
 SELECT switchstack.id AS switchstack_id, switchstack.name AS switchstack_hid
   FROM switchstack;

ALTER TABLE switchstack_hid
  OWNER TO cen1001;
GRANT ALL ON TABLE switchstack_hid TO cen1001;
GRANT SELECT ON TABLE switchstack_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire.switchstack_hid AS 
 SELECT switchstack.id AS switchstack_id, switchstack.name AS switchstack_hid
   FROM switchstack;

ALTER TABLE hotwire.switchstack_hid OWNER TO postgres;
GRANT ALL ON TABLE hotwire.switchstack_hid TO postgres;
GRANT ALL ON TABLE hotwire.switchstack_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3.switchstack_hid AS 
 SELECT switchstack.id AS switchstack_id, switchstack.name AS switchstack_hid
   FROM switchstack;

ALTER TABLE hotwire3.switchstack_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.switchstack_hid TO dev;
GRANT SELECT ON TABLE hotwire3.switchstack_hid TO cos;

