DROP VIEW hotwire3."10_View/People/Researcher_Staff_Reviews";
DROP FUNCTION hotwire3.researcher_staff_reviews();
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/People/Researcher_Staff_Reviews';
DROP VIEW hotwire3.new_reviewer_hid;
DROP VIEW hotwire3.update_usual_reviewer_hid;
REVOKE INSERT,SELECT,UPDATE,DELETE ON staff_review_meeting FROM hr;

