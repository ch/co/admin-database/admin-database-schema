ALTER TABLE hotwire.switch_hid OWNER TO dev;
REVOKE ALL ON TABLE hotwire.switch_hid FROM ro_hid;
REVOKE ALL ON TABLE hotwire.switch_hid FROM dev;
GRANT SELECT ON TABLE hotwire.switch_hid TO ro_hid;
