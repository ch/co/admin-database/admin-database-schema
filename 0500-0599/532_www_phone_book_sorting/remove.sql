-- View: www.phone_book_v1

DROP VIEW www.phone_book_v1;

CREATE OR REPLACE VIEW www.phone_book_v1 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, person.surname, telephone_numbers.telephone_numbers, person_rooms.rooms, supervisor_hid.supervisor_hid, _latest_role.post_category, 
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_rooms ON person.id = person_rooms.id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN supervisor_hid ON supervisor_hid.supervisor_id = _latest_role.supervisor_id
   LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.is_spri IS NOT TRUE AND s.status_id::text = 'Current'::text
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.phone_book_v1
  OWNER TO dev;
GRANT ALL ON TABLE www.phone_book_v1 TO dev;
GRANT SELECT ON TABLE www.phone_book_v1 TO www_sites;
GRANT ALL ON TABLE www.phone_book_v1 TO postgres;

