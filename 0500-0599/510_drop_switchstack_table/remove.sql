CREATE TABLE switchstack
(
  id bigint NOT NULL DEFAULT nextval('switchstack_id_seq'::regclass),
  description character varying(500),
  name character varying(80) NOT NULL,
  cabinet_id bigint NOT NULL,
  CONSTRAINT pk_switchstack PRIMARY KEY (id),
  CONSTRAINT switchstack_fk_cabinetid FOREIGN KEY (cabinet_id)
      REFERENCES cabinet (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE switchstack
  OWNER TO cen1001;
GRANT ALL ON TABLE switchstack TO cen1001;
GRANT ALL ON TABLE switchstack TO dev;

-- Index: idx_switchstack_name

-- DROP INDEX idx_switchstack_name;

CREATE UNIQUE INDEX idx_switchstack_name
  ON switchstack
  USING btree
  (name COLLATE pg_catalog."default");


