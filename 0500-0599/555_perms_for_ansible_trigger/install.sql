GRANT SELECT ON mm_person_research_group TO ansible_trigger;
GRANT SELECT (id,banned_for_lack_of_software_compliance) ON research_group TO ansible_trigger;
GRANT SELECT (id,owner_id) ON hardware TO ansible_trigger;
