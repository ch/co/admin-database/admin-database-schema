CREATE OR REPLACE VIEW switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text || COALESCE((' ('::text || switchstack.name::text) || ')'::text, ''::text) AS switch_hid
   FROM switch
   LEFT JOIN switchstack ON switch.switchstack_id = switchstack.id
   JOIN hardware ON switch.hardware_id = hardware.id;

ALTER TABLE switch_hid OWNER TO cen1001;
GRANT ALL ON TABLE switch_hid TO cen1001;
REVOKE ALL ON TABLE switch_hid FROM dev;
GRANT SELECT ON TABLE switch_hid TO ro_hid;

