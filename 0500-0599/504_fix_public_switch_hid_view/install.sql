CREATE OR REPLACE VIEW switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text AS switch_hid
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id;

ALTER TABLE switch_hid OWNER TO dev;
REVOKE ALL ON TABLE switch_hid FROM cen1001;
GRANT SELECT ON TABLE switch_hid TO ro_hid;

