DROP VIEW hotwire3."10_View/Computers/Virtual_Machines";
DROP FUNCTION hotwire3.virtual_machines_upd();

CREATE OR REPLACE VIEW hotwire3. "10_View/Computers/Virtual_Machines" AS
SELECT
    system_image.id,
    system_image.hardware_id,
    system_image.operating_system_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac,
    ARRAY (
        SELECT
            mm_system_image_ip_address.ip_address_id
        FROM
            mm_system_image_ip_address
        WHERE
            mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    system_image.comments::text AS comments,
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware_Full'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_address'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
FROM
    system_image;

ALTER TABLE hotwire3. "10_View/Computers/Virtual_Machines" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO dev;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO cos;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_del AS ON DELETE TO hotwire3. "10_View/Computers/Virtual_Machines"
    DO INSTEAD
    DELETE
FROM
    system_image
WHERE
    system_image.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_ins AS ON INSERT TO hotwire3. "10_View/Computers/Virtual_Machines"
    DO INSTEAD
    INSERT INTO system_image (
        hardware_id, operating_system_id, wired_mac_1, wired_mac_2, wired_mac_3, wired_mac_4, wireless_mac, comments)
    VALUES (
        new.hardware_id, new.operating_system_id, new.wired_mac_1, new.wired_mac_2, new.wired_mac_3, new.wired_mac_4, new.wireless_mac, new.comments ::character varying(1000))
RETURNING
    system_image.id, NULL::bigint AS int8, NULL::bigint AS int8, NULL::macaddr AS macaddr, NULL::macaddr AS macaddr, NULL::macaddr AS macaddr, NULL::macaddr AS macaddr, NULL::macaddr AS macaddr, ARRAY[NULL::bigint] AS "array", system_image.comments::text AS comments, NULL::_hwsubviewb AS _hwsubviewb, NULL::_hwsubviewb AS _hwsubviewb, NULL::_hwsubviewb AS _hwsubviewb;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_upd AS ON UPDATE TO hotwire3. "10_View/Computers/Virtual_Machines"
    DO INSTEAD
    UPDATE system_image
    SET hardware_id = new.hardware_id,
    operating_system_id = new.operating_system_id,
    wired_mac_1 = new.wired_mac_1,
    wired_mac_2 = new.wired_mac_2,
    wired_mac_3 = new.wired_mac_3,
    wired_mac_4 = new.wired_mac_4,
    wireless_mac = new.wireless_mac,
    comments = new.comments::character varying( 1000)
WHERE
    system_image.id = old.id;
