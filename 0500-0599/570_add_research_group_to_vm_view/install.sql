DROP VIEW hotwire3."10_View/Computers/Virtual_Machines";

CREATE OR REPLACE VIEW hotwire3. "10_View/Computers/Virtual_Machines" AS
SELECT
    system_image.id,
    system_image.hardware_id,
    system_image.operating_system_id,
    system_image.research_group_id,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac,
    ARRAY (
        SELECT
            mm_system_image_ip_address.ip_address_id
        FROM
            mm_system_image_ip_address
        WHERE
            mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id,
    system_image.comments::text AS comments,
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Contact_Details_ro'::character varying, 'system_image_id'::character varying, '10_View/People/Personnel_Basic'::character varying, 'person_id'::character varying, 'id'::character varying) AS "Contact_Details",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_Hardware_ro'::character varying, 'system_image_id'::character varying, '10_View/Computers/Hardware_Full'::character varying, 'hardware_id'::character varying, 'id'::character varying) AS "Hardware",
    hotwire3.to_hwsubviewb ('10_View/Computers/System_Image/_IP_address_ro'::character varying, 'system_image_id'::character varying, '10_View/Network/IP_addresses'::character varying, NULL::character varying, NULL::character varying) AS "IP_addresses"
FROM
    system_image;

ALTER TABLE hotwire3. "10_View/Computers/Virtual_Machines" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO dev;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Computers/Virtual_Machines" TO cos;

CREATE FUNCTION hotwire3.virtual_machines_upd() RETURNS TRIGGER AS
$$
DECLARE
    v_system_image_id bigint;
BEGIN
    IF NEW.id IS NOT NULL THEN
        v_system_image_id := NEW.id;
        UPDATE system_image SET
            hardware_id = new.hardware_id,
            operating_system_id = new.operating_system_id,
            research_group_id = new.research_group_id,
            wired_mac_1 = new.wired_mac_1,
            wired_mac_2 = new.wired_mac_2,
            wired_mac_3 = new.wired_mac_3,
            wired_mac_4 = new.wired_mac_4,
            wireless_mac = new.wireless_mac,
            comments = new.comments::character varying(1000)
        WHERE
            system_image.id = v_system_image_id;
    ELSE
        v_system_image_id := nextval('system_image_id_seq');
        NEW.id := v_system_image_id;
        INSERT INTO system_image (
            id,
            hardware_id,
            operating_system_id,
            research_group_id,
            wired_mac_1,
            wired_mac_2,
            wired_mac_3,
            wired_mac_4,
            wireless_mac,
            comments
        ) VALUES (
            v_system_image_id,
            new.hardware_id,
            new.operating_system_id,
            new.research_group_id,
            new.wired_mac_1,
            new.wired_mac_2,
            new.wired_mac_3,
            new.wired_mac_4,
            new.wireless_mac,
            new.comments::character varying(1000)
        );
    END IF;
    PERFORM fn_mm_array_update(NEW.ip_address_id, 'mm_system_image_ip_address'::varchar,'system_image_id'::varchar,'ip_address_id'::varchar,v_system_image_id);
    RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;

ALTER FUNCTION hotwire3.virtual_machines_upd() OWNER TO dev;

CREATE OR REPLACE RULE hotwire3_view_system_image_basic_del AS ON DELETE TO hotwire3. "10_View/Computers/Virtual_Machines" DO INSTEAD DELETE FROM system_image WHERE system_image.id = old.id;

CREATE TRIGGER virtual_machines_upd INSTEAD OF INSERT OR UPDATE ON hotwire3."10_View/Computers/Virtual_Machines" FOR EACH ROW EXECUTE PROCEDURE hotwire3.virtual_machines_upd();
