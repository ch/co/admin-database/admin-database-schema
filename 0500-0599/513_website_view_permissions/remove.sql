-- owned by cen1001, no rights for dev
--  cen1001=arwdDxt/cen1001+
--  old_cos=r/cen1001      +
--  www_sites=r/cen1001    +
--  cos=r/cen1001          +
--  ro_hid=r/cen1001       +
--  =r/cen1001
ALTER VIEW www_person_hid_v2 OWNER TO cen1001;
GRANT ALL ON TABLE www_person_hid_v2 TO cen1001;
REVOKE ALL ON TABLE www_person_hid_v2 FROM dev;

-- owned by alt36, had all rights for dev
-- alt36=arwdDxt/alt36
-- dev=arwdDxt/alt36
-- postgres=arwdDxt/alt36
-- www_sites=arwdDxt/alt36
ALTER VIEW www.research_groups OWNER TO alt36;
GRANT ALL ON TABLE www.research_groups TO alt36,dev;

-- owned by alt36, had all rights for dev
-- alt36=arwdDxt/alt36
-- dev=arwdDxt/alt36
-- postgres=arwdDxt/alt36
-- www_sites=arwdDxt/alt36
ALTER VIEW www.telephone_numbers OWNER TO alt36;
GRANT ALL ON TABLE www.telephone_numbers TO alt36,dev;

-- alt36=arwdDxt/alt36
-- www_sites=arwdDxt/alt36+
-- dev=arwdDxt/alt36
ALTER VIEW www.rig_membership_v1 OWNER TO alt36;
GRANT ALL ON TABLE www.rig_membership_v1 TO alt36,dev;

-- alt36=arwdDxt/alt36
-- www_sites=arwdDxt/alt36+
-- dev=arwdDxt/alt36
ALTER VIEW www.primary_rig_membership_v1 OWNER TO alt36;
GRANT ALL ON TABLE www.primary_rig_membership_v1 TO alt36,dev;

-- alt36=arwdDxt/alt36
-- www_sites=r/alt36
-- web_editors=r/alt36
-- dev=r/alt36
ALTER VIEW www.person_research_groups OWNER TO alt36;
REVOKE ALL ON TABLE www.person_research_groups FROM dev;
GRANT SELECT ON TABLE www.person_research_groups TO dev;
GRANT ALL ON TABLE www.person_research_groups TO alt36;

