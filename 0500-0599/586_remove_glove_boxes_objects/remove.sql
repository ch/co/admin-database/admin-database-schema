CREATE TABLE glove_box
(
  id bigserial NOT NULL,
  device_number character varying(80),
  serial_number character varying(80),
  recirc_or_ducted_id bigint,
  manufacturer character varying(80),
  notes character varying(500),
  room_id bigint,
  CONSTRAINT glove_box_pkey PRIMARY KEY (id),
  CONSTRAINT glove_box_fk_recirc_or_ducted FOREIGN KEY (recirc_or_ducted_id)
      REFERENCES recirc_or_ducted_hid (recirc_or_ducted_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT glove_box_fk_room FOREIGN KEY (room_id)
      REFERENCES room (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE glove_box
  OWNER TO dev;
GRANT ALL ON TABLE glove_box TO dev;
GRANT ALL ON TABLE glove_box TO cen1001;

CREATE OR REPLACE VIEW glove_boxes_view AS 
 SELECT glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes
   FROM glove_box;

ALTER TABLE glove_boxes_view
  OWNER TO cen1001;
GRANT ALL ON TABLE glove_boxes_view TO cen1001;
GRANT SELECT ON TABLE glove_boxes_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE glove_boxes_view TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE glove_boxes_view TO dev;

-- Rule: glove_box_ins ON glove_boxes_view

-- DROP RULE glove_box_ins ON glove_boxes_view;

CREATE OR REPLACE RULE glove_box_ins AS
    ON INSERT TO glove_boxes_view DO INSTEAD  INSERT INTO glove_box (room_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, notes) 
  VALUES (new.room_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.notes)
  RETURNING glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes;

-- Rule: glove_boxes_del ON glove_boxes_view

-- DROP RULE glove_boxes_del ON glove_boxes_view;

CREATE OR REPLACE RULE glove_boxes_del AS
    ON DELETE TO glove_boxes_view DO INSTEAD  DELETE FROM glove_box
  WHERE glove_box.id = old.id;

-- Rule: glove_boxes_upd ON glove_boxes_view

-- DROP RULE glove_boxes_upd ON glove_boxes_view;

CREATE OR REPLACE RULE glove_boxes_upd AS
    ON UPDATE TO glove_boxes_view DO INSTEAD  UPDATE glove_box SET room_id = new.room_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, notes = new.notes
  WHERE glove_box.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Safety/Glove_Boxes" AS 
 SELECT glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes
   FROM glove_box;

ALTER TABLE hotwire."10_View/Safety/Glove_Boxes"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Safety/Glove_Boxes" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Safety/Glove_Boxes" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Glove_Boxes" TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Glove_Boxes" TO dev;

-- Rule: hotwire_view_glove_box_ins ON hotwire."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire_view_glove_box_ins ON hotwire."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire_view_glove_box_ins AS
    ON INSERT TO hotwire."10_View/Safety/Glove_Boxes" DO INSTEAD  INSERT INTO glove_box (room_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, notes) 
  VALUES (new.room_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.notes)
  RETURNING glove_box.id, glove_box.room_id, glove_box.device_number, glove_box.serial_number, glove_box.recirc_or_ducted_id, glove_box.manufacturer, glove_box.notes;

-- Rule: hotwire_view_glove_boxes_del ON hotwire."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire_view_glove_boxes_del ON hotwire."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire_view_glove_boxes_del AS
    ON DELETE TO hotwire."10_View/Safety/Glove_Boxes" DO INSTEAD  DELETE FROM glove_box
  WHERE glove_box.id = old.id;

-- Rule: hotwire_view_glove_boxes_upd ON hotwire."10_View/Safety/Glove_Boxes"

-- DROP RULE hotwire_view_glove_boxes_upd ON hotwire."10_View/Safety/Glove_Boxes";

CREATE OR REPLACE RULE hotwire_view_glove_boxes_upd AS
    ON UPDATE TO hotwire."10_View/Safety/Glove_Boxes" DO INSTEAD  UPDATE glove_box SET room_id = new.room_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, notes = new.notes
  WHERE glove_box.id = old.id;


