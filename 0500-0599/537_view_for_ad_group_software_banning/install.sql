CREATE VIEW apps.people_banned_for_software_compliance AS
SELECT
    person.crsid
FROM person
JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
WHERE research_group.banned_for_lack_of_software_compliance = 't'
    AND person.override_auto_ban <> 't'
; 

ALTER VIEW apps.people_banned_for_software_compliance OWNER TO dev;
GRANT SELECT ON apps.people_banned_for_software_compliance TO ad_accounts;
