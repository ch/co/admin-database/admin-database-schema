DROP TRIGGER hotwire3_view_personnel_data_entry ON hotwire3."10_View/People/Personnel_Data_Entry";

DROP FUNCTION hotwire3.personnel_data_entry();

CREATE OR REPLACE FUNCTION hotwire3.personnel_data_entry_upd(hotwire3."10_View/People/Personnel_Data_Entry")
  RETURNS bigint AS
$BODY$
        declare v alias for $1;
                v_person_id bigint;
                v_do_not_show_on_website boolean;
                v_hide_phone_no_from_website boolean;
                v_on_retired_staff_list bigint;
                retired_staff_list_id bigint;

        begin
                v_do_not_show_on_website := coalesce(v.hide_from_website,'f');
		v_hide_phone_no_from_website := coalesce(v.hide_phone_no_from_website,'f');
                if v.id is not null 
                then
                        v_person_id = v.id;
                        update person set
                                surname = v.surname,
                                image_oid = v.image_oid::bigint,
                                first_names = v.first_names, 
                                title_id = v.title_id, 
                                known_as = v.known_as,
                                name_suffix = v.name_suffix,
                                previous_surname = v.previous_surname,
                                date_of_birth = v.date_of_birth, 
                                gender_id = v.gender_id, 
                                crsid = v.crsid,
                                email_address = v.email_address, 
                                hide_email = v.hide_email, 
                                do_not_show_on_website = v_do_not_show_on_website,
				hide_phone_no_from_website = v_hide_phone_no_from_website,
                                arrival_date = v.arrival_date,
                                leaving_date = v.leaving_date,
                                left_but_no_leaving_date_given = v.left_but_no_leaving_date_given,
                                cambridge_address = v.home_address::varchar(500),
                                cambridge_phone_number = v.home_phone_number,
                                cambridge_college_id = v.cambridge_college_id,
                                mobile_number = v.mobile_number,
                                emergency_contact = v.emergency_contact::varchar(500), 
                                location = v.location,
                                other_information = v.other_information,
                                notes = v.notes,
                                forwarding_address = v.forwarding_address,
                                new_employer_address = v.new_employer_address, 
                                chem_at_cam = v."chem_@_cam",
                                continuous_employment_start_date = v.continuous_employment_start_date,
                                paper_file_details = v.paper_file_status, 
                                registration_completed = v.registration_completed,
                                clearance_cert_signed = v.clearance_cert_signed,
                                counts_as_academic=v.treat_as_academic_staff
                        where person.id = v.id;
                else
                        insert into person (
                                        surname,
                                        image_oid, 
                                        first_names, 
                                        title_id, 
                                        known_as,
                                        name_suffix,
                                        previous_surname, 
                                        date_of_birth, 
                                        gender_id, 
                                        crsid,
                                        email_address, 
                                        hide_email, 
                                        do_not_show_on_website,
                                        arrival_date,
                                        leaving_date,
                                        cambridge_address,
                                        cambridge_phone_number, 
                                        cambridge_college_id,
                                        mobile_number,
                                        emergency_contact, 
                                        location,
                                        other_information,
                                        notes,
                                        forwarding_address,
                                        new_employer_address, 
                                        chem_at_cam,
                                        continuous_employment_start_date,
                                        paper_file_details, 
                                        registration_completed,
                                        clearance_cert_signed,
                                        counts_as_academic,
					hide_phone_no_from_website

                                ) values (
                                        v.surname, 
                                        v.image_oid::bigint,
                                        v.first_names, 
                                        v.title_id, 
                                        v.known_as,
                                        v.name_suffix,
                                        v.previous_surname,
                                        v.date_of_birth, 
                                        v.gender_id, 
                                        v.crsid,
                                        v.email_address, 
                                        v.hide_email, 
                                        v_do_not_show_on_website,
                                        v.arrival_date,
                                        v.leaving_date,
                                        v.home_address::varchar(500),
                                        v.home_phone_number, 
                                        v.cambridge_college_id,
                                        v.mobile_number,
                                        v.emergency_contact::varchar(500), 
                                        v.location,
                                        v.other_information,
                                        v.notes,
                                        v.forwarding_address,
                                        v.new_employer_address, 
                                        v."chem_@_cam",
                                        v.continuous_employment_start_date,
                                        v.paper_file_status, 
                                        v.registration_completed,
                                        v.clearance_cert_signed,
                                        v.treat_as_academic_staff,
					v_hide_phone_no_from_website


                                ) returning id into v_person_id ;
                end if;

                -- do retired staff list update
                select id from mailinglist where name = 'chem-retiredstaff' into retired_staff_list_id;
                select include_person_id from mm_mailinglist_include_person where include_person_id = v_person_id and mailinglist_id = retired_staff_list_id into v_on_retired_staff_list;
                if v.retired_staff_mailing_list = 't' and v_on_retired_staff_list is null
                then
                   insert into mm_mailinglist_include_person ( mailinglist_id, include_person_id) values ( retired_staff_list_id, v_person_id); 
                end if;
                if v.retired_staff_mailing_list = 'f' and v_on_retired_staff_list is not null
                then
                   delete from mm_mailinglist_include_person where mailinglist_id = retired_staff_list_id and include_person_id = v_person_id; 
                end if;

                -- many-many updates here
		perform fn_mm_array_update(v.research_group_id, 'mm_person_research_group'::varchar,'person_id'::varchar, 'research_group_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.dept_telephone_number_id, 'mm_person_dept_telephone_number'::varchar,'person_id'::varchar, 'dept_telephone_number_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.room_id, 'mm_person_room'::varchar,'person_id'::varchar, 'room_id'::varchar, v_person_id);
		perform fn_mm_array_update(v.nationality_id, 'mm_person_nationality'::varchar,'person_id'::varchar, 'nationality_id'::varchar, v_person_id);

		
                return v_person_id;
        end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.personnel_data_entry_upd(hotwire3."10_View/People/Personnel_Data_Entry")
  OWNER TO dev;


CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_ins AS
    ON INSERT TO hotwire3."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire3.personnel_data_entry_upd(new.*) AS id;

CREATE OR REPLACE RULE hotwire3_view_personnel_data_entry_upd AS
    ON UPDATE TO hotwire3."10_View/People/Personnel_Data_Entry" DO INSTEAD  SELECT hotwire3.personnel_data_entry_upd(new.*) AS id;

