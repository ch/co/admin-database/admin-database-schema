CREATE OR REPLACE VIEW hotwire3."10_View/My_Groups/Unknown_Group_Computers" AS 
 SELECT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer AS ro_manufacturer, hardware.model AS ro_model, hardware.name AS ro_hardware_name, hardware.hardware_type_id AS ro_hardware_type_id, system_image.operating_system_id AS ro_operating_system_id, system_image.wired_mac_1 AS ro_wired_mac_1, system_image.wired_mac_2 AS ro_wired_mac_2, system_image.wireless_mac AS ro_wireless_mac, ip_address_hid.ip_address_hid AS ro_mm_ip_address, hardware.asset_tag AS ro_asset_tag, hardware.serial_number AS ro_serial_number, hardware.monitor_serial_number AS ro_monitor_serial_number, hardware.room_id AS ro_room_id, system_image.user_id AS ro_user_id, hardware.owner_id AS ro_owner_id, system_image.research_group_id, system_image.host_system_image_id AS ro_host_system_image_id, system_image.comments::text AS ro_system_image_comments, hardware.comments::text AS ro_hardware_comments
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address_hid USING (ip_address_id)
  WHERE system_image.research_group_id = 2 AND ip_address_hid.ip_address_hid !~~ '%personal.private%'::text;

ALTER TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" TO cos;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" TO groupitreps;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/My_Groups/Unknown_Group_Computers" TO headsofgroup;

-- Rule: hotwire3_unknown_group_computers_upd ON hotwire3."10_View/My_Groups/Unknown_Group_Computers"

-- DROP RULE hotwire3_unknown_group_computers_upd ON hotwire3."10_View/My_Groups/Unknown_Group_Computers";

CREATE OR REPLACE RULE hotwire3_unknown_group_computers_upd AS
    ON UPDATE TO hotwire3."10_View/My_Groups/Unknown_Group_Computers" DO INSTEAD  UPDATE system_image SET research_group_id = new.research_group_id
  WHERE system_image.id = old.id;


CREATE OR REPLACE VIEW hotwire."10_View/Unknown_Group_Computers" AS 
 SELECT system_image.id, hardware.id AS _hardware_id, hardware.manufacturer, hardware.model, hardware.name AS hardware_name, hardware.hardware_type_id, system_image.operating_system_id, system_image.wired_mac_1, system_image.wired_mac_2, system_image.wireless_mac, ip_address_hid.ip_address_hid AS ro_mm_ip_address, hardware.asset_tag, hardware.serial_number, hardware.monitor_serial_number, hardware.room_id, system_image.user_id, hardware.owner_id, system_image.research_group_id, system_image.host_system_image_id, system_image.comments AS system_image_comments, hardware.comments AS hardware_comments
   FROM system_image
   JOIN hardware ON system_image.hardware_id = hardware.id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address_hid USING (ip_address_id)
  WHERE system_image.research_group_id = 2 AND ip_address_hid.ip_address_hid !~~ '%personal.private%'::text;

ALTER TABLE hotwire."10_View/Unknown_Group_Computers"
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Unknown_Group_Computers" TO postgres;
GRANT ALL ON TABLE hotwire."10_View/Unknown_Group_Computers" TO groupitreps;

