CREATE OR REPLACE VIEW _old_review_meetings AS 
 SELECT all_meetings_per_person.person_id, all_meetings_per_person.date_of_meeting
   FROM ( SELECT ph.person_id, srm.date_of_meeting
           FROM staff_review_meeting srm
      JOIN post_history ph ON srm.post_history_id = ph.id) all_meetings_per_person
   JOIN ( SELECT ph.person_id, max(srm.date_of_meeting) AS latest_date
           FROM staff_review_meeting srm
      JOIN post_history ph ON srm.post_history_id = ph.id
     GROUP BY ph.person_id) latest_meeting ON all_meetings_per_person.person_id = latest_meeting.person_id
  WHERE all_meetings_per_person.date_of_meeting <> latest_meeting.latest_date;

ALTER TABLE _old_review_meetings
  OWNER TO cen1001;

CREATE OR REPLACE VIEW staff_reviews_summary_oldsubview AS 
 SELECT staff_review_meeting.id, post_history.id AS post_history_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, 'staff_reviews_view'::text AS _view
   FROM staff_review_meeting
   JOIN post_history ON post_history.id = staff_review_meeting.post_history_id;

ALTER TABLE staff_reviews_summary_oldsubview
  OWNER TO cen1001;
GRANT ALL ON TABLE staff_reviews_summary_oldsubview TO cen1001;
GRANT SELECT ON TABLE staff_reviews_summary_oldsubview TO mgmt_ro;
GRANT SELECT ON TABLE staff_reviews_summary_oldsubview TO hr;

CREATE VIEW staff_reviews_summary_subview AS
    SELECT DISTINCT staff_review_meeting.id, staff_review_meeting.post_history_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id, 'staff_reviews_view'::text AS _view FROM ((staff_review_meeting JOIN person ON ((staff_review_meeting.person_id = person.id))) JOIN post_history ON ((post_history.person_id = person.id))) ORDER BY staff_review_meeting.id;


ALTER TABLE public.staff_reviews_summary_subview OWNER TO dev;
REVOKE ALL ON TABLE staff_reviews_summary_subview FROM PUBLIC;
REVOKE ALL ON TABLE staff_reviews_summary_subview FROM dev;
GRANT ALL ON TABLE staff_reviews_summary_subview TO dev;
GRANT ALL ON TABLE staff_reviews_summary_subview TO cen1001;
GRANT SELECT ON TABLE staff_reviews_summary_subview TO mgmt_ro;
GRANT SELECT ON TABLE staff_reviews_summary_subview TO hr;

