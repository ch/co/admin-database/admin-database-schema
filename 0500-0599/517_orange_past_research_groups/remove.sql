DROP VIEW hotwire3."10_View/Groups/Research_Groups_Computing";

CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Groups_Computing" AS 
 SELECT research_group.id, research_group.name, research_group.head_of_group_id, research_group.deputy_head_of_group_id, research_group.administrator_id AS alternate_admin_contact_id, research_group.website_address, research_group.internal_use_only AS hide_from_websites, research_group.ignore_for_software_compliance_purposes, research_group.group_fileserver_id, research_group.active_directory_container, research_group.fai_class_id, array_to_string(ARRAY( SELECT person_hid.person_hid
           FROM mm_person_research_group
      JOIN _physical_status_v3 member_status USING (person_id)
   JOIN hotwire3.person_hid USING (person_id)
  WHERE mm_person_research_group.research_group_id = research_group.id AND member_status.status_id::text = 'Current'::text), ';
'::text) AS ro_list_of_members_who_are_current, ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id) AS member_id, ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id) AS computer_rep_id, 
        CASE
            WHEN pi_status.status_id::text = 'Current'::text THEN 'Active'::character varying
            WHEN has_current_members.has_current_members = true THEN 'Active'::character varying
            ELSE 'Inactive'::character varying
        END AS ro_group_status, 
        CASE
            WHEN pi_status.status_id::text = 'Current'::text THEN NULL::text
            WHEN has_current_members.has_current_members = true THEN NULL::text
            ELSE 'orange'::text
        END AS _cssclass
   FROM research_group
   JOIN _physical_status_v3 pi_status ON research_group.head_of_group_id = pi_status.person_id
   JOIN ( SELECT research_group.id, 'Current'::character varying(20)::text = ANY (((ARRAY( SELECT _physical_status.status_id
              FROM mm_person_research_group
         JOIN _physical_status_v3 _physical_status USING (person_id)
        WHERE mm_person_research_group.research_group_id = research_group.id))::text[])) AS has_current_members
      FROM research_group) has_current_members USING (id)
  ORDER BY research_group.name;

ALTER TABLE hotwire3."10_View/Groups/Research_Groups_Computing"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Groups/Research_Groups_Computing" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Groups/Research_Groups_Computing" TO cos;

-- Rule: hotwire3_view_research_group_editing_del ON hotwire3."10_View/Groups/Research_Groups_Computing"

-- DROP RULE hotwire3_view_research_group_editing_del ON hotwire3."10_View/Groups/Research_Groups_Computing";

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_del AS
    ON DELETE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD  DELETE FROM research_group
  WHERE research_group.id = old.id;

-- Rule: hotwire3_view_research_group_editing_upd ON hotwire3."10_View/Groups/Research_Groups_Computing"

-- DROP RULE hotwire3_view_research_group_editing_upd ON hotwire3."10_View/Groups/Research_Groups_Computing";

CREATE OR REPLACE RULE hotwire3_view_research_group_editing_upd AS
    ON UPDATE TO hotwire3."10_View/Groups/Research_Groups_Computing" DO INSTEAD ( UPDATE research_group SET name = new.name, head_of_group_id = new.head_of_group_id, deputy_head_of_group_id = new.deputy_head_of_group_id, administrator_id = new.alternate_admin_contact_id, website_address = new.website_address, internal_use_only = new.hide_from_websites, ignore_for_software_compliance_purposes = new.ignore_for_software_compliance_purposes, group_fileserver_id = new.group_fileserver_id, active_directory_container = new.active_directory_container, fai_class_id = new.fai_class_id
  WHERE research_group.id = old.id;
 SELECT fn_mm_array_update(new.member_id, old.member_id, 'mm_person_research_group'::character varying, 'research_group_id'::character varying, 'person_id'::character varying, old.id) AS fn_mm_array_update;
 SELECT fn_mm_array_update(new.computer_rep_id, old.computer_rep_id, 'mm_research_group_computer_rep'::character varying, 'research_group_id'::character varying, 'computer_rep_id'::character varying, old.id) AS fn_mm_array_update;
);


-- Trigger: hotwire3_groups_research_groups_ins on hotwire3."10_View/Groups/Research_Groups_Computing"

-- DROP TRIGGER hotwire3_groups_research_groups_ins ON hotwire3."10_View/Groups/Research_Groups_Computing";

CREATE TRIGGER hotwire3_groups_research_groups_ins
  INSTEAD OF INSERT
  ON hotwire3."10_View/Groups/Research_Groups_Computing"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.groups_research_groups_computing_ins();

