ALTER TABLE staff_review_meeting ADD COLUMN post_history_id bigint;
ALTER TABLE staff_review_meeting ADD CONSTRAINT staff_review_meeting_fk_post_history FOREIGN KEY (post_history_id) REFERENCES post_history (id);

CREATE OR REPLACE VIEW hotwire."10_View/Probation_and_Reviews" AS 
 SELECT a.id, a.ro_person_id, a.supervisor_id, a.start_date, a.ro_post_category, a.continuous_employment_start_date, a.probation_period, a.date_probation_letters_sent, a.date_of_first_probation_meeting, a.first_probation_meeting_comments, a.date_of_second_probation_meeting, a.second_probation_meeting_comments, a.date_of_third_probation_meeting, a.third_probation_meeting_comments, a.date_of_fourth_probation_meeting, a.fourth_probation_meeting_comments, a.probation_outcome, a.usual_reviewer_id, a.next_review_due, a.timing_of_reviews, a.gender_id, a.status_id, a._surname, a._first_names, a.staff_reviews_summary_subview
   FROM ( SELECT post_history.id, post_history.person_id AS ro_person_id, post_history.supervisor_id, post_history.start_date, staff_category.category AS ro_post_category, person.continuous_employment_start_date, post_history.probation_period, post_history.date_probation_letters_sent, post_history.date_of_first_probation_meeting, post_history.first_probation_meeting_comments, post_history.date_of_second_probation_meeting, post_history.second_probation_meeting_comments, post_history.date_of_third_probation_meeting, post_history.third_probation_meeting_comments, post_history.date_of_fourth_probation_meeting, post_history.fourth_probation_meeting_comments, post_history.probation_outcome, person.usual_reviewer_id, person.next_review_due, post_history.timing_of_reviews, person.gender_id, ps.status_id, person.surname AS _surname, person.first_names AS _first_names, _to_hwsubview('post_history_id'::text, '10_View/Roles/Staff_Reviews_Summary'::text, 'id'::text) AS staff_reviews_summary_subview
           FROM post_history
      JOIN person ON post_history.person_id = person.id
   LEFT JOIN staff_category ON post_history.staff_category_id = staff_category.id
   LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id) a
  ORDER BY a._surname, a._first_names;

ALTER TABLE hotwire."10_View/Probation_and_Reviews"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Probation_and_Reviews" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Probation_and_Reviews" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Probation_and_Reviews" TO mgmt_ro;
GRANT SELECT, UPDATE ON TABLE hotwire."10_View/Probation_and_Reviews" TO hr;

-- Rule: hotwire_view_probation_and_reviews_upd ON hotwire."10_View/Probation_and_Reviews"

-- DROP RULE hotwire_view_probation_and_reviews_upd ON hotwire."10_View/Probation_and_Reviews";

CREATE OR REPLACE RULE hotwire_view_probation_and_reviews_upd AS
    ON UPDATE TO hotwire."10_View/Probation_and_Reviews" DO INSTEAD ( UPDATE post_history SET supervisor_id = new.supervisor_id, start_date = new.start_date, probation_period = new.probation_period, date_probation_letters_sent = new.date_probation_letters_sent, date_of_first_probation_meeting = new.date_of_first_probation_meeting, first_probation_meeting_comments = new.first_probation_meeting_comments, date_of_second_probation_meeting = new.date_of_second_probation_meeting, second_probation_meeting_comments = new.second_probation_meeting_comments, date_of_third_probation_meeting = new.date_of_third_probation_meeting, third_probation_meeting_comments = new.third_probation_meeting_comments, date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting, fourth_probation_meeting_comments = new.fourth_probation_meeting_comments, probation_outcome = new.probation_outcome, timing_of_reviews = new.timing_of_reviews
  WHERE post_history.id = old.id;
 UPDATE person SET continuous_employment_start_date = new.continuous_employment_start_date, usual_reviewer_id = new.usual_reviewer_id, next_review_due = new.next_review_due, gender_id = new.gender_id
  WHERE person.id = old.ro_person_id;
);



--DROP VIEW hotwire."10_View/Roles/Staff_Reviews_Summary";
CREATE OR REPLACE VIEW hotwire."10_View/Roles/Staff_Reviews_Summary" AS 
 SELECT DISTINCT staff_review_meeting.id, staff_review_meeting.post_history_id, staff_review_meeting.date_of_meeting, staff_review_meeting.reviewer_id
   FROM staff_review_meeting
   JOIN person ON staff_review_meeting.person_id = person.id
   JOIN post_history ON post_history.person_id = person.id
  ORDER BY staff_review_meeting.id;

ALTER TABLE hotwire."10_View/Roles/Staff_Reviews_Summary"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Staff_Reviews_Summary" TO dev;
GRANT ALL ON TABLE hotwire."10_View/Roles/Staff_Reviews_Summary" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Roles/Staff_Reviews_Summary" TO mgmt_ro;
GRANT SELECT ON TABLE hotwire."10_View/Roles/Staff_Reviews_Summary" TO hr;


