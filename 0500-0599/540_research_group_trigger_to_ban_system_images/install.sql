CREATE FUNCTION research_group_propagate_system_image_bans() RETURNS TRIGGER AS
$$
BEGIN
    -- If nothing significant has changed, exit quickly
    IF (TG_OP = 'UPDATE')
    THEN
        IF NEW.banned_for_lack_of_software_compliance IS NOT DISTINCT FROM OLD.banned_for_lack_of_software_compliance
        THEN
            RETURN NEW;
        END IF;
    END IF;

    -- Something changed: either an update to banned_for_lack_of_software_compliance or
    -- we're doing an insert
    IF NEW.banned_for_lack_of_software_compliance = 't'
    THEN
        UPDATE system_image SET
            auto_banned_from_network = 't',
            network_ban_log = coalesce(network_ban_log,'') || 'Banned from network at ' || current_timestamp || ' because group ' || NEW.name || E' is banned\n'
        WHERE research_group_id = NEW.id;
    ELSE
       UPDATE system_image SET 
           auto_banned_from_network = 'f',
           override_auto_ban = 'f',
            network_ban_log = coalesce(network_ban_log,'') || 'Possibly allowed on network at ' || current_timestamp || ' because group ' || NEW.name || E' is allowed\n'
       WHERE research_group_id = NEW.id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

GRANT CREATE ON SCHEMA public TO osbuilder;
ALTER FUNCTION research_group_propagate_system_image_bans() OWNER TO osbuilder;
REVOKE CREATE ON SCHEMA public FROM osbuilder;

REVOKE ALL ON FUNCTION research_group_propagate_system_image_bans() FROM PUBLIC;
GRANT EXECUTE ON FUNCTION research_group_propagate_system_image_bans() TO dev,cos,hr,student_management;

CREATE TRIGGER propagate_system_image_bans AFTER UPDATE OR INSERT ON research_group FOR EACH ROW EXECUTE PROCEDURE research_group_propagate_system_image_bans();
