DROP RULE system_image_del_nuke_ip ON system_image;

CREATE FUNCTION system_image_del_nuke_ip() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE ip_address SET hostname = NULL::character varying
    FROM mm_system_image_ip_address
    WHERE mm_system_image_ip_address.ip_address_id = ip_address.id AND mm_system_image_ip_address.system_image_id = OLD.id;
    RETURN OLD;
END;
$$ 
LANGUAGE PLPGSQL 
SECURITY DEFINER 
SET search_path = public, pg_temp;
ALTER FUNCTION system_image_del_nuke_ip() OWNER TO ipreg;
REVOKE ALL ON FUNCTION system_image_del_nuke_ip() FROM public;
GRANT EXECUTE ON FUNCTION system_image_del_nuke_ip() TO dev, cos, groupitreps, headsofgroup;

GRANT DELETE ON dns_cnames TO ipreg;
GRANT DELETE ON mm_system_image_ip_address TO ipreg;

CREATE OR REPLACE FUNCTION blankhostname_remove_systemimage() RETURNS TRIGGER AS
$$
begin
if ((new.hostname is null) 
 or (new.hostname='')) THEN
  new.last_seen=NULL;
 delete from mm_system_image_ip_address where ip_address_id=old.id;
 new.hobbit_flags=NULL;
 delete from dns_cnames where toname=old.hostname;
end if;
return null;
end
$$
LANGUAGE PLPGSQL
SECURITY DEFINER
SET search_path = public, pg_temp;
ALTER FUNCTION blankhostname_remove_systemimage() OWNER TO ipreg;
REVOKE ALL ON FUNCTION blankhostname_remove_systemimage() FROM public;
GRANT EXECUTE ON FUNCTION blankhostname_remove_systemimage() TO dev, cos, groupitreps, headsofgroup;

CREATE TRIGGER system_image_del_nuke_ip BEFORE DELETE ON system_image FOR EACH ROW EXECUTE PROCEDURE system_image_del_nuke_ip();
