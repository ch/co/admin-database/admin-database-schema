REVOKE DELETE ON dns_cnames FROM ipreg;
REVOKE DELETE ON mm_system_image_ip_address FROM ipreg;

CREATE OR REPLACE FUNCTION blankhostname_remove_systemimage()
  RETURNS trigger AS
$BODY$
begin
if ((new.hostname is null) 
 or (new.hostname='')) THEN
  new.last_seen=NULL;
 delete from mm_system_image_ip_address where ip_address_id=old.id;
 new.hobbit_flags=NULL;
 delete from dns_cnames where toname=old.hostname;
end if;
return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION blankhostname_remove_systemimage() OWNER TO dev;
GRANT EXECUTE ON FUNCTION blankhostname_remove_systemimage() TO PUBLIC;

DROP TRIGGER system_image_del_nuke_ip ON system_image;
DROP FUNCTION system_image_del_nuke_ip();
CREATE OR REPLACE RULE system_image_del_nuke_ip AS
ON DELETE TO system_image DO
    UPDATE ip_address i SET hostname = NULL::character varying
    FROM mm_system_image_ip_address
    WHERE mm_system_image_ip_address.ip_address_id = i.id AND mm_system_image_ip_address.system_image_id = old.id;

