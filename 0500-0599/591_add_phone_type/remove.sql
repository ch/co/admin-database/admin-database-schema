DROP VIEW hotwire3."10_View/Phones/Phone_Types";
DROP VIEW hotwire3."10_View/Phones/Phones";

CREATE OR REPLACE VIEW hotwire3."10_View/Phones/Phones" AS 
 SELECT dept_telephone_number.id, dept_telephone_number.extension_number, dept_telephone_number.room_id, dept_telephone_number.fax, dept_telephone_number.personal_line, ARRAY( SELECT mm_person_dept_telephone_number.person_id
           FROM mm_person_dept_telephone_number
          WHERE dept_telephone_number.id = mm_person_dept_telephone_number.dept_telephone_number_id) AS person_id
   FROM dept_telephone_number;

ALTER TABLE hotwire3."10_View/Phones/Phones" OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Phones/Phones" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Phones/Phones" TO phones_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Phones/Phones" TO cos;

CREATE OR REPLACE RULE hotwire3_view_telephone_management_del AS
    ON DELETE TO hotwire3."10_View/Phones/Phones" DO INSTEAD ( DELETE FROM mm_person_dept_telephone_number
  WHERE mm_person_dept_telephone_number.dept_telephone_number_id = old.id;
 DELETE FROM dept_telephone_number
  WHERE dept_telephone_number.id = old.id;
);

CREATE OR REPLACE FUNCTION hotwire3.phones()
  RETURNS trigger AS
$BODY$
begin
    if new.id is null then
        new.id := nextval('dept_telephone_number_id_seq');
        insert into dept_telephone_number ( id, extension_number, room_id, fax, personal_line ) VALUES ( new.id, new.extension_number, new.room_id, new.fax, new.personal_line );
    perform fn_mm_array_update(new.person_id,array[]::bigint[],'mm_person_dept_telephone_number'::varchar,'dept_telephone_number_id','person_id',new.id);
    else
        update dept_telephone_number set
            extension_number = new.extension_number,
            room_id = new.room_id,
            fax = new.fax,
            personal_line = new.personal_line
        where id = old.id;
    perform fn_mm_array_update(new.person_id,old.person_id,'mm_person_dept_telephone_number'::varchar,'dept_telephone_number_id','person_id',new.id);
    end if;
    return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.phones()
  OWNER TO dev;


CREATE TRIGGER hotwire3_view_telephone_management_ins
  INSTEAD OF INSERT OR UPDATE
  ON hotwire3."10_View/Phones/Phones"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.phones();

ALTER TABLE dept_telephone_number DROP COLUMN phone_type_id;

DROP VIEW hotwire3.phone_type_hid;

DROP TABLE phone_type_hid;



