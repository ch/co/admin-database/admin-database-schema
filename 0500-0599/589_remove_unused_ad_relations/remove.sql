CREATE TABLE adsoftwaregroup
(
  id bigint NOT NULL DEFAULT nextval('adsoftwaregroup_id_seq'::regclass),
  packagename character varying(80) NOT NULL,
  description character varying(500), -- Why do we need this as well as packageName? Well there may be different types of licences. I'm not sure it's vital though.
  CONSTRAINT pk_adsoftwaregroup PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE adsoftwaregroup
  OWNER TO cen1001;
GRANT ALL ON TABLE adsoftwaregroup TO cen1001;
GRANT ALL ON TABLE adsoftwaregroup TO dev;
COMMENT ON COLUMN adsoftwaregroup.description IS 'Why do we need this as well as packageName? Well there may be different types of licences. I''m not sure it''s vital though.';

CREATE TABLE adnetworkdrivegroup
(
  id bigint NOT NULL DEFAULT nextval('adnetworkdrivegroup_id_seq'::regclass),
  networkdrive character varying(256), -- Phil says maximum lkength of this is 256
  CONSTRAINT pk_adnetworkdrivegroup PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE adnetworkdrivegroup
  OWNER TO cen1001;
GRANT ALL ON TABLE adnetworkdrivegroup TO cen1001;
GRANT ALL ON TABLE adnetworkdrivegroup TO dev;
COMMENT ON COLUMN adnetworkdrivegroup.networkdrive IS 'Phil says maximum lkength of this is 256';


-- Index: idx_adnetworkdrivegroup_networkdrive

-- DROP INDEX idx_adnetworkdrivegroup_networkdrive;

CREATE UNIQUE INDEX idx_adnetworkdrivegroup_networkdrive
  ON adnetworkdrivegroup
  USING btree
  (networkdrive COLLATE pg_catalog."default");

CREATE TABLE adnetworkdrivegroupuseraccount
(
  adnetworkdrivegroupid bigint NOT NULL,
  useraccountid bigint NOT NULL,
  CONSTRAINT pk_adnetworkdrivegroupuseraccount PRIMARY KEY (adnetworkdrivegroupid, useraccountid),
  CONSTRAINT adnetworkdrivegroupuseraccount_fk_adnetworkdrivegroupid FOREIGN KEY (adnetworkdrivegroupid)
      REFERENCES adnetworkdrivegroup (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT adnetworkdrivegroupuseraccount_fk_useraccountid FOREIGN KEY (useraccountid)
      REFERENCES user_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE adnetworkdrivegroupuseraccount
  OWNER TO cen1001;
GRANT ALL ON TABLE adnetworkdrivegroupuseraccount TO cen1001;
GRANT ALL ON TABLE adnetworkdrivegroupuseraccount TO dev;

CREATE TABLE adsoftwaregroupsystemimage
(
  adsoftwaregroupid bigint NOT NULL,
  systemimageid bigint NOT NULL,
  CONSTRAINT pk_adsoftwaregroupsystemimage PRIMARY KEY (adsoftwaregroupid, systemimageid),
  CONSTRAINT adsoftwaregroupsystemimage_fk_adsoftwaregroupid FOREIGN KEY (adsoftwaregroupid)
      REFERENCES adsoftwaregroup (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT adsoftwaregroupsystemimage_fk_systemimageid FOREIGN KEY (systemimageid)
      REFERENCES system_image (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE adsoftwaregroupsystemimage
  OWNER TO cen1001;
GRANT ALL ON TABLE adsoftwaregroupsystemimage TO cen1001;
GRANT ALL ON TABLE adsoftwaregroupsystemimage TO dev;
