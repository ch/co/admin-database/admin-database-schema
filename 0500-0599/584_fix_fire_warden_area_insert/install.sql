DROP RULE fire_warden_area_ins ON hotwire3."10_View/Safety/Fire_warden_areas";

DROP RULE fire_warden_area_upd ON hotwire3."10_View/Safety/Fire_warden_areas";

DROP FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas");

CREATE FUNCTION hotwire3.fire_warden_area_trig() RETURNS TRIGGER AS
$$
BEGIN
    IF new.id is not null
    then
        update fire_warden_area set
            name = new.name,
            location = new.location,
            building_id = new.building_id,
            building_region_id = new.building_region_id,
            building_floor_id = new.building_floor_id
            where fire_warden_area_id = new.id;
    else
        insert into fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) values (
            new.name,
            new.location,
            new.building_id,
            new.building_region_id,
            new.building_floor_id
        ) returning fire_warden_area_id into new.id;
    end if;
    -- remove all fire wardens for this area
    update fire_warden set fire_warden_area_id = null where fire_warden.fire_warden_area_id = new.id;
    -- update the primary fire warden
    update fire_warden set fire_warden_area_id = new.id, is_primary = 't' where fire_warden.fire_warden_id = new.fire_warden_id;
    -- update secondary fire wardens
    update fire_warden set fire_warden_area_id = new.id, is_primary = 'f' where fire_warden.fire_warden_id = any(new.deputy_fire_warden_id);
    
    return new;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER fire_warden_area_upd INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/Safety/Fire_warden_areas" FOR EACH ROW EXECUTE PROCEDURE hotwire3.fire_warden_area_trig();
