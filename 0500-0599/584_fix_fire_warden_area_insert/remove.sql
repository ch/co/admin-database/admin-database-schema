DROP TRIGGER fire_warden_area_upd ON hotwire3."10_View/Safety/Fire_warden_areas";
DROP FUNCTION hotwire3.fire_warden_area_trig();

CREATE OR REPLACE FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas")
  RETURNS bigint AS
$BODY$
declare
    v alias for $1;
    v_fire_warden_area_id bigint;
begin
    if v.id is not null 
    then
        v_fire_warden_area_id = v.id;
        update fire_warden_area set 
            name = v.name,
            location = v.location,
            building_id = v.building_id,
            building_region_id = v.building_region_id,
            building_floor_id = v.building_floor_id
        where fire_warden_area_id = v_fire_warden_area_id;
    else
        insert into fire_warden_area ( 
            name,
            location,
            building_id,
            building_region_id,
            building_floor_id
        ) values (
            v.name,
            v.location,
            v.building_id,
            v.building_region_id,
            v.building_floor_id
        ) returning fire_warden_area_id into v_fire_warden_area_id;
    end if;
    -- remove all fire wardens for this area
    update fire_warden set fire_warden_area_id = null where fire_warden.fire_warden_area_id = v_fire_warden_area_id;
    -- update the primary fire warden
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 't' where fire_warden.fire_warden_id = v.fire_warden_id;
    -- update secondary fire wardens
    update fire_warden set fire_warden_area_id = v_fire_warden_area_id, is_primary = 'f' where fire_warden.fire_warden_id = any(v.deputy_fire_warden_id);
    return v_fire_warden_area_id;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.fire_warden_area_upd(hotwire3."10_View/Safety/Fire_warden_areas")
  OWNER TO dev;

CREATE OR REPLACE RULE fire_warden_area_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Fire_warden_areas" DO INSTEAD  SELECT hotwire3.fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;

CREATE OR REPLACE RULE fire_warden_area_upd AS
    ON UPDATE TO hotwire3."10_View/Safety/Fire_warden_areas" DO INSTEAD  SELECT hotwire3.fire_warden_area_upd(new.*) AS fn_fire_warden_area_upd;


