CREATE OR REPLACE VIEW apps.user_accounts_to_create_in_ad AS 
SELECT
    person.id,
    person.crsid,
    person.first_names,
    person.surname,
    person.email_address,
    max(COALESCE(research_group.active_directory_container,research_group.name)::text) AS rg
FROM person
LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
JOIN _physical_status_v3 ON person.id = _physical_status_v3.person_id
WHERE _physical_status_v3.status_id::text = 'Current'::text AND person.crsid IS NOT NULL
GROUP BY person.id, person.crsid, person.first_names, person.surname, person.email_address;

ALTER TABLE apps.user_accounts_to_create_in_ad OWNER TO dev;
GRANT ALL ON TABLE apps.user_accounts_to_create_in_ad TO dev;
GRANT SELECT ON TABLE apps.user_accounts_to_create_in_ad TO ad_accounts;

