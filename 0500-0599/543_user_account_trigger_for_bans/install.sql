CREATE FUNCTION user_account_banning() RETURNS TRIGGER AS
$$
DECLARE
    person_is_banned boolean;
    person_ban_overridden boolean;
BEGIN
    SELECT auto_banned_from_network FROM person WHERE id = NEW.person_id INTO person_is_banned;
    SELECT override_auto_ban FROM person WHERE id = NEW.person_id INTO person_ban_overridden;
    IF person_is_banned = 't'
    THEN
        NEW.owner_banned = NOT(COALESCE(person_ban_overridden,'f'::boolean));
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION user_account_banning() OWNER TO dev;

CREATE TRIGGER check_ban_status BEFORE INSERT OR UPDATE ON user_account FOR EACH ROW EXECUTE PROCEDURE user_account_banning();
