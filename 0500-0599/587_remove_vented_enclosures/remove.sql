CREATE TABLE vented_enclosure_device_type_hid
(
  vented_enclosure_device_type_id bigserial NOT NULL,
  vented_enclosure_device_type_hid character varying(80) NOT NULL,
  CONSTRAINT vented_enclosure_device_type_hid_pkey PRIMARY KEY (vented_enclosure_device_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vented_enclosure_device_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE vented_enclosure_device_type_hid TO dev;
GRANT SELECT ON TABLE vented_enclosure_device_type_hid TO ro_hid;
GRANT ALL ON TABLE vented_enclosure_device_type_hid TO cen1001;

CREATE TABLE vented_enclosure
(
  id bigserial NOT NULL,
  vented_enclosure_device_type_id bigint,
  device_number character varying(80),
  serial_number character varying(80),
  recirc_or_ducted_id bigint,
  manufacturer character varying(80),
  firetrace_type_id bigint,
  install_year integer,
  last_inspection date,
  next_inspection date,
  notes character varying(500),
  room_id bigint,
  inspection_frequency interval,
  CONSTRAINT vented_enclosure_pkey PRIMARY KEY (id),
  CONSTRAINT vented_enclosure_fk_firetrace_type FOREIGN KEY (firetrace_type_id)
      REFERENCES firetrace_type_hid (firetrace_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT vented_enclosure_fk_recirc_or_ducted FOREIGN KEY (recirc_or_ducted_id)
      REFERENCES recirc_or_ducted_hid (recirc_or_ducted_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT vented_enclosure_fk_room FOREIGN KEY (room_id)
      REFERENCES room (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT vented_enclosure_fk_vented_enclosure_device_type FOREIGN KEY (vented_enclosure_device_type_id)
      REFERENCES vented_enclosure_device_type_hid (vented_enclosure_device_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vented_enclosure
  OWNER TO dev;
GRANT ALL ON TABLE vented_enclosure TO dev;
GRANT ALL ON TABLE vented_enclosure TO cen1001;

CREATE OR REPLACE VIEW hotwire.vented_enclosure_device_type_hid AS 
 SELECT vented_enclosure_device_type_hid.vented_enclosure_device_type_id, vented_enclosure_device_type_hid.vented_enclosure_device_type_hid
   FROM vented_enclosure_device_type_hid;

ALTER TABLE hotwire.vented_enclosure_device_type_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.vented_enclosure_device_type_hid TO postgres;
GRANT ALL ON TABLE hotwire.vented_enclosure_device_type_hid TO ro_hid;

CREATE OR REPLACE VIEW vented_enclosures_view AS 
 SELECT vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes
   FROM vented_enclosure;

ALTER TABLE vented_enclosures_view
  OWNER TO cen1001;
GRANT ALL ON TABLE vented_enclosures_view TO cen1001;
GRANT SELECT ON TABLE vented_enclosures_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vented_enclosures_view TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vented_enclosures_view TO dev;

-- Rule: vented_enclosure_ins ON vented_enclosures_view

-- DROP RULE vented_enclosure_ins ON vented_enclosures_view;

CREATE OR REPLACE RULE vented_enclosure_ins AS
    ON INSERT TO vented_enclosures_view DO INSTEAD  INSERT INTO vented_enclosure (room_id, vented_enclosure_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes) 
  VALUES (new.room_id, new.vented_enclosure_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes)
  RETURNING vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes;

-- Rule: vented_enclosures_del ON vented_enclosures_view

-- DROP RULE vented_enclosures_del ON vented_enclosures_view;

CREATE OR REPLACE RULE vented_enclosures_del AS
    ON DELETE TO vented_enclosures_view DO INSTEAD  DELETE FROM vented_enclosure
  WHERE vented_enclosure.id = old.id;

-- Rule: vented_enclosures_upd ON vented_enclosures_view

-- DROP RULE vented_enclosures_upd ON vented_enclosures_view;

CREATE OR REPLACE RULE vented_enclosures_upd AS
    ON UPDATE TO vented_enclosures_view DO INSTEAD  UPDATE vented_enclosure SET room_id = new.room_id, vented_enclosure_device_type_id = new.vented_enclosure_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes
  WHERE vented_enclosure.id = old.id;

CREATE OR REPLACE VIEW hotwire."10_View/Safety/Vented_Enclosures" AS 
 SELECT vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes
   FROM vented_enclosure;

ALTER TABLE hotwire."10_View/Safety/Vented_Enclosures"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Safety/Vented_Enclosures" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Safety/Vented_Enclosures" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Vented_Enclosures" TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Vented_Enclosures" TO dev;

-- Rule: hotwire_view_vented_enclosure_ins ON hotwire."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire_view_vented_enclosure_ins ON hotwire."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire_view_vented_enclosure_ins AS
    ON INSERT TO hotwire."10_View/Safety/Vented_Enclosures" DO INSTEAD  INSERT INTO vented_enclosure (room_id, vented_enclosure_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes) 
  VALUES (new.room_id, new.vented_enclosure_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes)
  RETURNING vented_enclosure.id, vented_enclosure.room_id, vented_enclosure.vented_enclosure_device_type_id, vented_enclosure.device_number, vented_enclosure.serial_number, vented_enclosure.recirc_or_ducted_id, vented_enclosure.manufacturer, vented_enclosure.firetrace_type_id, vented_enclosure.install_year, vented_enclosure.inspection_frequency, vented_enclosure.last_inspection, vented_enclosure.next_inspection, vented_enclosure.notes;

-- Rule: hotwire_view_vented_enclosures_del ON hotwire."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire_view_vented_enclosures_del ON hotwire."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire_view_vented_enclosures_del AS
    ON DELETE TO hotwire."10_View/Safety/Vented_Enclosures" DO INSTEAD  DELETE FROM vented_enclosure
  WHERE vented_enclosure.id = old.id;

-- Rule: hotwire_view_vented_enclosures_upd ON hotwire."10_View/Safety/Vented_Enclosures"

-- DROP RULE hotwire_view_vented_enclosures_upd ON hotwire."10_View/Safety/Vented_Enclosures";

CREATE OR REPLACE RULE hotwire_view_vented_enclosures_upd AS
    ON UPDATE TO hotwire."10_View/Safety/Vented_Enclosures" DO INSTEAD  UPDATE vented_enclosure SET room_id = new.room_id, vented_enclosure_device_type_id = new.vented_enclosure_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes
  WHERE vented_enclosure.id = old.id;

CREATE OR REPLACE VIEW hotwire3.vented_enclosure_device_type_hid AS 
 SELECT vented_enclosure_device_type_hid.vented_enclosure_device_type_id, vented_enclosure_device_type_hid.vented_enclosure_device_type_hid
   FROM vented_enclosure_device_type_hid;

ALTER TABLE hotwire3.vented_enclosure_device_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.vented_enclosure_device_type_hid TO dev;
GRANT SELECT ON TABLE hotwire3.vented_enclosure_device_type_hid TO public;

