DROP VIEW hotwire."10_View/Safety/Vented_Enclosures";
DROP VIEW hotwire.vented_enclosure_device_type_hid;
DROP VIEW vented_enclosures_view;
DROP VIEW hotwire3.vented_enclosure_device_type_hid;

DROP TABLE vented_enclosure;
DROP TABLE vented_enclosure_device_type_hid;
