CREATE TABLE servicetype
(
  id bigint NOT NULL DEFAULT nextval('servicetype_id_seq'::regclass),
  name character varying(80),
  CONSTRAINT pk_servicetype PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE servicetype
  OWNER TO cen1001;
GRANT ALL ON TABLE servicetype TO cen1001;
GRANT ALL ON TABLE servicetype TO dev;

-- Index: idx_servicetype_name

-- DROP INDEX idx_servicetype_name;

CREATE UNIQUE INDEX idx_servicetype_name
  ON servicetype
    USING btree
      (name COLLATE pg_catalog."default");



CREATE TABLE service
(
  id bigint NOT NULL DEFAULT nextval('service_id_seq'::regclass),
  name character varying(80),
  description character varying(500),
  dhcpsettingsid bigint,
  servicetypeid bigint NOT NULL,
  CONSTRAINT pk_service PRIMARY KEY (id),
  CONSTRAINT service_fk_dhcpsettingsid FOREIGN KEY (dhcpsettingsid)
      REFERENCES dhcpsettings (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_fk_servicetypeid FOREIGN KEY (servicetypeid)
      REFERENCES servicetype (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE service
  OWNER TO cen1001;
GRANT ALL ON TABLE service TO cen1001;
GRANT ALL ON TABLE service TO dev;

-- Index: idx_name

-- DROP INDEX idx_name;

CREATE UNIQUE INDEX idx_name
  ON service
  USING btree
  (name COLLATE pg_catalog."default");

CREATE TABLE mm_useraccount_service
(
  id bigint NOT NULL DEFAULT nextval('useraccountservice_id_seq'::regclass),
  lastused date, -- This is to record when this user last accessed the service. Frank wants to be able to automatically disable access to a service that has been unused for a period of time.
  useraccountid bigint NOT NULL,
  serviceid bigint NOT NULL,
  CONSTRAINT pk_useraccountservice PRIMARY KEY (id),
  CONSTRAINT useraccountservice_fk_serviceid FOREIGN KEY (serviceid)
      REFERENCES service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT useraccountservice_fk_useraccountid FOREIGN KEY (useraccountid)
      REFERENCES user_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mm_useraccount_service
  OWNER TO cen1001;
GRANT ALL ON TABLE mm_useraccount_service TO cen1001;
GRANT ALL ON TABLE mm_useraccount_service TO dev;
COMMENT ON COLUMN mm_useraccount_service.lastused IS 'This is to record when this user last accessed the service. Frank wants to be able to automatically disable access to a service that has been unused for a period of time.';



CREATE TABLE depttelephonenumberperson
(
  depttelephonenumberid bigint NOT NULL,
  personid bigint NOT NULL,
  CONSTRAINT pk_depttelephonenumberperson PRIMARY KEY (depttelephonenumberid, personid),
  CONSTRAINT depttelephonenumberperson_fk_depttelephonenumberid FOREIGN KEY (depttelephonenumberid)
      REFERENCES dept_telephone_number (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT depttelephonenumberperson_fk_personid FOREIGN KEY (personid)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE depttelephonenumberperson
  OWNER TO cen1001;
GRANT ALL ON TABLE depttelephonenumberperson TO cen1001;
GRANT ALL ON TABLE depttelephonenumberperson TO dev;

CREATE TABLE hardwaresocket
(
  hardwareid bigint NOT NULL,
  socketid bigint NOT NULL,
  CONSTRAINT pk_hardwaresocket PRIMARY KEY (hardwareid, socketid),
  CONSTRAINT hardwaresocket_fk_hardwareid FOREIGN KEY (hardwareid)
      REFERENCES hardware (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT hardwaresocket_fk_socketid FOREIGN KEY (socketid)
      REFERENCES socket (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hardwaresocket
  OWNER TO cen1001;
GRANT ALL ON TABLE hardwaresocket TO cen1001;
GRANT ALL ON TABLE hardwaresocket TO dev;


CREATE TABLE researchgroupservice
(
  researchgroupid bigint NOT NULL,
  serviceid bigint NOT NULL,
  CONSTRAINT pk_researchgroupservice PRIMARY KEY (researchgroupid, serviceid),
  CONSTRAINT researchgroupservice_fk_researchgroupid FOREIGN KEY (researchgroupid)
      REFERENCES research_group (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT researchgroupservice_fk_serviceid FOREIGN KEY (serviceid)
      REFERENCES service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE researchgroupservice
  OWNER TO cen1001;
GRANT ALL ON TABLE researchgroupservice TO cen1001;
GRANT ALL ON TABLE researchgroupservice TO dev;

CREATE TABLE useraccountunixgroup
(
  useraccountid bigint NOT NULL,
  unixgroupid bigint NOT NULL,
  CONSTRAINT pk_useraccountunixgroup PRIMARY KEY (useraccountid, unixgroupid),
  CONSTRAINT useraccountunixgroup_fk_unixgroupid FOREIGN KEY (unixgroupid)
      REFERENCES unixgroup (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT useraccountunixgroup_fk_useraccountid FOREIGN KEY (useraccountid)
      REFERENCES user_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE useraccountunixgroup
  OWNER TO cen1001;
GRANT ALL ON TABLE useraccountunixgroup TO cen1001;
GRANT ALL ON TABLE useraccountunixgroup TO dev;

CREATE TABLE systemusesservice
(
  serviceid bigint NOT NULL,
  systemimageid bigint NOT NULL,
  CONSTRAINT pk_systemusesservice PRIMARY KEY (serviceid, systemimageid),
  CONSTRAINT systemusesservice_fk_serviceid FOREIGN KEY (serviceid)
      REFERENCES service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT systemusesservice_fk_systemimageid FOREIGN KEY (systemimageid)
      REFERENCES system_image (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE systemusesservice
  OWNER TO cen1001;
GRANT ALL ON TABLE systemusesservice TO cen1001;
GRANT ALL ON TABLE systemusesservice TO dev;


CREATE TABLE systemprovidesservice
(
  serviceid bigint NOT NULL,
  systemimageid bigint NOT NULL,
  CONSTRAINT pk_systemprovidesservice PRIMARY KEY (serviceid, systemimageid),
  CONSTRAINT systemprovidesservice_fk_serviceid FOREIGN KEY (serviceid)
      REFERENCES service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT systemprovidesservice_fk_systemimageid FOREIGN KEY (systemimageid)
      REFERENCES system_image (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE systemprovidesservice
  OWNER TO cen1001;
GRANT ALL ON TABLE systemprovidesservice TO cen1001;
GRANT ALL ON TABLE systemprovidesservice TO dev;

CREATE TABLE personmanageslist
(
  mailinglistid bigint NOT NULL,
  personid bigint NOT NULL,
  CONSTRAINT pk_personmanageslist PRIMARY KEY (mailinglistid, personid),
  CONSTRAINT personmanageslist_fk_mailinglistid FOREIGN KEY (mailinglistid)
      REFERENCES mailinglist (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT personmanageslist_fk_personid FOREIGN KEY (personid)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personmanageslist
  OWNER TO cen1001;
GRANT ALL ON TABLE personmanageslist TO cen1001;
GRANT ALL ON TABLE personmanageslist TO dev;

CREATE TABLE personmanagessystem
(
  personid bigint NOT NULL,
  systemimageid bigint NOT NULL,
  CONSTRAINT pk_personmanagessystem PRIMARY KEY (personid, systemimageid),
  CONSTRAINT personmanagessystem_fk_personid FOREIGN KEY (personid)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT personmanagessystem_fk_systemimageid FOREIGN KEY (systemimageid)
      REFERENCES system_image (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personmanagessystem
  OWNER TO cen1001;
GRANT ALL ON TABLE personmanagessystem TO cen1001;
GRANT ALL ON TABLE personmanagessystem TO dev;

