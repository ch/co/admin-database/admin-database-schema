CREATE OR REPLACE VIEW hotwire3.switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text || COALESCE((' ('::text || switchstack.name::text) || ')'::text, ''::text) AS switch_hid
   FROM switch
   LEFT JOIN switchstack ON switch.switchstack_id = switchstack.id
   JOIN hardware ON switch.hardware_id = hardware.id;

CREATE OR REPLACE VIEW hotwire.switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text || COALESCE((' ('::text || switchstack.name::text) || ')'::text, ''::text) AS switch_hid
   FROM switch
   LEFT JOIN switchstack ON switch.switchstack_id = switchstack.id
   JOIN hardware ON switch.hardware_id = hardware.id;
