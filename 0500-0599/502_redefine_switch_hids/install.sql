CREATE OR REPLACE VIEW hotwire3.switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text AS switch_hid
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id;

CREATE OR REPLACE VIEW hotwire.switch_hid AS 
 SELECT switch.id AS switch_id, hardware.name::text AS switch_hid
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id;
