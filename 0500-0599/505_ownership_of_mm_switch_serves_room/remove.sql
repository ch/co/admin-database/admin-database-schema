ALTER TABLE mm_switch_serves_room OWNER TO postgres;
GRANT ALL ON TABLE mm_switch_serves_room TO postgres;
GRANT ALL ON TABLE mm_switch_serves_room TO cos;
REVOKE ALL ON TABLE mm_switch_serves_room FROM dev;
