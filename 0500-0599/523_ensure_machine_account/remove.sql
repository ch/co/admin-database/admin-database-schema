CREATE OR REPLACE FUNCTION ensure_machine_account(_mac macaddr)
  RETURNS character varying AS
$BODY$
 declare
  _sysim_id bigint;
  _result varchar;
 begin
  select system_image.id from system_image where _mac in (wired_mac_1, wired_mac_2, wired_mac_3, wired_mac_4, wireless_mac) into _sysim_id;
  if found then
   select name||':'||chemnet_token from machine_account where system_image_id=_sysim_id into _result;
   if not found then
    
    insert into machine_account(name, chemnet_token, system_image_id) values (
    (
    select replace(ip_address.hostname, '.cam.ac.uk','') from ip_address inner join mm_system_image_ip_address on ip_address.id=ip_address_id where system_image_id=_sysim_id limit 1)
    ,random_string(16),_sysim_id);
    select name||':'||chemnet_token from machine_account where system_image_id=_sysim_id into _result;

   end if;
   return _result;
  else
   return null;
  end if;
  return 'Odd'::varchar;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100;
ALTER FUNCTION ensure_machine_account(macaddr)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION ensure_machine_account(macaddr) TO dev;
GRANT EXECUTE ON FUNCTION ensure_machine_account(macaddr) TO public;
GRANT EXECUTE ON FUNCTION ensure_machine_account(macaddr) TO postgres;

CREATE OR REPLACE FUNCTION ensure_machine_account(_ip inet)
  RETURNS character varying AS
$BODY$
 declare
  _sysim_id bigint;
  _result varchar;
 begin
  select system_image.id from system_image inner join mm_system_image_ip_address on system_image.id=system_image_id inner join ip_address on ip_address.id=ip_address_id where ip_address.ip=_ip limit 1 into _sysim_id;
  if found then
   select name||':'||chemnet_token from machine_account where system_image_id=_sysim_id into _result;
   if not found then
    insert into machine_account(name, chemnet_token, system_image_id) values ((select replace(ip_address.hostname,'.cam.ac.uk','') from ip_address where ip=_ip limit 1),random_string(16),_sysim_id);
    select name||':'||chemnet_token from machine_account where system_image_id=_sysim_id into _result;

   end if;
   return _result;
  else
   return null;
  end if;
  return 'Odd'::varchar;
 end;
$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100;
ALTER FUNCTION ensure_machine_account(inet)
  OWNER TO dev;
GRANT EXECUTE ON FUNCTION ensure_machine_account(inet) TO dev;
GRANT EXECUTE ON FUNCTION ensure_machine_account(inet) TO public;
GRANT EXECUTE ON FUNCTION ensure_machine_account(inet) TO postgres;
