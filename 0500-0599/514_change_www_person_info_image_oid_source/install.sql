CREATE OR REPLACE VIEW www.person_info_v8 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, cambridge_college.name AS college_name, cambridge_college.website AS college_website, 
        CASE
            WHEN person.hide_phone_no_from_website THEN NULL::character varying::text
            ELSE telephone_numbers.telephone_numbers
        END AS telephone_numbers, 
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text, 'sc-10'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-5'::text THEN 'Research assistant'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE post_category_hid.post_category_hid
        END AS post_category, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig, rgs.research_groups, rgs.website_addresses, 
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title, 
        CASE
            WHEN person.counts_as_academic IS TRUE THEN 'Head of group'::text
            WHEN person.counts_as_postdoc IS TRUE THEN 'Postdoctoral researcher'::text
            ELSE h.post_category_hid
        END AS research_group_post_category
   FROM ( SELECT _latest_role_v1.person_id, max(_latest_role_v1.post_category_weight) AS max
           FROM www._latest_role_v1
          GROUP BY _latest_role_v1.person_id) lr
   JOIN cache._latest_role_v12 _latest_role ON lr.person_id = _latest_role.person_id AND lr.max = _latest_role.post_category_weight
   JOIN person ON person.id = lr.person_id
   JOIN www.post_category_hid h ON h.post_category_id = _latest_role.post_category_id
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN www.post_category_hid ON post_category_hid.post_category_id = _latest_role.post_category_id
   LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v8 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v8 TO dev;
GRANT SELECT ON TABLE www.person_info_v8 TO www_sites;
GRANT ALL ON TABLE www.person_info_v8 TO postgres;

CREATE OR REPLACE VIEW www.person_info_v7 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, 
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE post_category_hid.post_category_hid
        END AS post_category, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig, rgs.research_groups, rgs.website_addresses, 
        CASE
            WHEN _latest_role.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN www.post_category_hid ON post_category_hid.post_category_id = _latest_role.post_category_id
   LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v7 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v7 TO dev;
GRANT SELECT ON TABLE www.person_info_v7 TO www_sites;
GRANT ALL ON TABLE www.person_info_v7 TO postgres;


CREATE OR REPLACE VIEW www.person_info_v6 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, 
        CASE
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS post_category, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig, rgs.research_groups, rgs.website_addresses, post_history.job_title, website_staff_category_hid.website_staff_category_hid AS website_staff_category
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v6 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v6 TO dev;
GRANT SELECT ON TABLE www.person_info_v6 TO www_sites;
GRANT ALL ON TABLE www.person_info_v6 TO postgres;

CREATE OR REPLACE VIEW www.person_info_v5 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig, rgs.research_groups, rgs.website_addresses
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v5 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v5 TO alt36;
GRANT ALL ON TABLE www.person_info_v5 TO postgres;
GRANT ALL ON TABLE www.person_info_v5 TO www_sites;

CREATE OR REPLACE VIEW www.person_info_v4 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, research_groups.names AS research_groups, research_groups.website_addresses, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v4 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v4 TO alt36;
GRANT ALL ON TABLE www.person_info_v4 TO dev;
GRANT ALL ON TABLE www.person_info_v4 TO postgres;
GRANT ALL ON TABLE www.person_info_v4 TO www_sites;

CREATE OR REPLACE VIEW www.person_info_v3 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, research_groups.names AS research_groups, research_groups.website_addresses, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
  WHERE person.do_not_show_on_website IS FALSE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v3 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v3 TO alt36;
GRANT ALL ON TABLE www.person_info_v3 TO dev;
GRANT ALL ON TABLE www.person_info_v3 TO postgres;
GRANT ALL ON TABLE www.person_info_v3 TO www_sites;

CREATE OR REPLACE VIEW www.person_info_v2 AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, research_groups.names AS research_groups, research_groups.website_addresses, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  WHERE person.do_not_show_on_website = true
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_v2 OWNER TO dev;
GRANT ALL ON TABLE www.person_info_v2 TO alt36;
GRANT ALL ON TABLE www.person_info_v2 TO dev;
GRANT ALL ON TABLE www.person_info_v2 TO postgres;
GRANT ALL ON TABLE www.person_info_v2 TO www_sites;

CREATE OR REPLACE VIEW www.person_info_test AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN NULL::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers, 
        CASE
            WHEN website_staff_category_hid.website_staff_category_hid IS NOT NULL THEN website_staff_category_hid.website_staff_category_hid::text
            WHEN (_latest_role.post_category_id = ANY (ARRAY['sc-1'::text, 'sc-9'::text])) OR person.counts_as_academic IS TRUE THEN 'Academic'::text
            WHEN ((_latest_role.post_category_id = ANY (ARRAY['sc-4'::text, 'sc-6'::text, 'sc-7'::text])) OR person.counts_as_postdoc IS TRUE) AND person.counts_as_academic IS NOT TRUE THEN 'Postdoc'::text
            WHEN _latest_role.post_category_id = 'sc-2'::text THEN 'Academic-Related'::text
            WHEN _latest_role.post_category_id = 'v-8'::text AND person.counts_as_academic IS NOT TRUE THEN 'Emeritus'::text
            ELSE 'Other'::text
        END AS post_category, _latest_role.post_category_id, person.counts_as_academic, s.status_id AS physical_status, r.rigs, pr.primary_rig, rgs.research_groups, rgs.website_addresses, post_history.job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.person_research_groups rgs ON person.id = rgs.person_id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
   LEFT JOIN cache._latest_role ON person.id = _latest_role.person_id
   LEFT JOIN post_history ON post_history.id = _latest_role.role_id
   LEFT JOIN _physical_status_v3 s ON s.person_id = person.id
   LEFT JOIN www.rig_membership_v1 r ON r.id = person.id
   LEFT JOIN www.primary_rig_membership_v1 pr ON pr.id = person.id
   LEFT JOIN website_staff_category_hid ON website_staff_category_hid.website_staff_category_id = person.website_staff_category_id
  WHERE person.do_not_show_on_website IS FALSE AND person.is_spri IS NOT TRUE
  ORDER BY (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text;

ALTER TABLE www.person_info_test OWNER TO dev;
GRANT ALL ON TABLE www.person_info_test TO dev;
GRANT SELECT ON TABLE www.person_info_test TO www_sites;
GRANT ALL ON TABLE www.person_info_test TO postgres;

CREATE OR REPLACE VIEW www.person_info AS 
 SELECT person.id, person.crsid, 
        CASE
            WHEN person.hide_email THEN '(not available)'::character varying
            ELSE person.email_address
        END AS email_address, person.image_lo::bigint as image_oid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, research_groups.names AS research_groups, research_groups.website_addresses, cambridge_college.name AS college_name, cambridge_college.website AS college_website, telephone_numbers.telephone_numbers
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN www.research_groups ON research_groups.head_of_group_id = person.id
   LEFT JOIN www.telephone_numbers ON person.id = telephone_numbers.id
   LEFT JOIN cambridge_college ON person.cambridge_college_id = cambridge_college.id
  ORDER BY person.crsid;

ALTER TABLE www.person_info OWNER TO dev;
GRANT ALL ON TABLE www.person_info TO alt36;
GRANT ALL ON TABLE www.person_info TO dev;
GRANT ALL ON TABLE www.person_info TO postgres;
GRANT ALL ON TABLE www.person_info TO www_sites;
