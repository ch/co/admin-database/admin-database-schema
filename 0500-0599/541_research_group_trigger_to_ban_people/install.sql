CREATE FUNCTION research_group_propagate_person_bans() RETURNS TRIGGER AS
$$
BEGIN
    -- If nothing significant has changed, exit quickly
    IF (TG_OP = 'UPDATE')
    THEN
        IF NEW.banned_for_lack_of_software_compliance IS NOT DISTINCT FROM OLD.banned_for_lack_of_software_compliance
        THEN
            RETURN NEW;
        END IF;
    END IF;

    -- Something changed: either an update to banned_for_lack_of_software_compliance or
    -- we're doing an insert
    IF NEW.banned_for_lack_of_software_compliance = 't'
    THEN
        UPDATE person SET
            auto_banned_from_network = 't',
            network_ban_log = coalesce(network_ban_log,'') || 'Banned from network at ' || current_timestamp || ' because group ' || NEW.name || E' is banned\n'
        FROM mm_person_research_group
        WHERE mm_person_research_group.research_group_id = NEW.id AND person.id = mm_person_research_group.person_id;
    ELSE
       UPDATE person SET 
           auto_banned_from_network = 'f',
           override_auto_ban = 'f',
            network_ban_log = coalesce(network_ban_log,'') || 'Allowed on network at ' || current_timestamp || ' because group ' || NEW.name || E' is allowed\n'
       FROM mm_person_research_group
       WHERE research_group_id = NEW.id AND person.id = mm_person_research_group.person_id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql
SET search_path = public, pg_temp;

ALTER FUNCTION research_group_propagate_person_bans() OWNER TO dev;

CREATE TRIGGER propagate_person_bans AFTER UPDATE OR INSERT ON research_group FOR EACH ROW EXECUTE PROCEDURE research_group_propagate_person_bans();
