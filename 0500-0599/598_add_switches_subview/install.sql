CREATE VIEW hotwire3."10_View/Network/Switches/_switches" AS
SELECT
  switch.id,
  switchport.id as switch_port_id,
  hardware.name as switch_name,
  hardware.manufacturer,
  switch_model_hid.switch_model_hid,
  room_hid.room_hid
FROM switchport
LEFT JOIN switch ON switchport.switch_id = switch.id
JOIN hardware ON switch.hardware_id = hardware.id
LEFT JOIN hotwire3.room_hid USING (room_id)
LEFT JOIN hotwire3.switch_model_hid USING (switch_model_id)
;

ALTER VIEW hotwire3."10_View/Network/Switches/_switches" OWNER TO dev;
GRANT SELECT ON hotwire3."10_View/Network/Switches/_switches" TO cos;

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Ports" AS 
 SELECT switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id,
hotwire3.to_hwsubviewb('10_View/Network/Switches/_switches'::varchar,'switch_port_id'::varchar,'10_View/Network/Switches/Switches'::varchar,null::varchar,null::varchar) as switch
   FROM switchport;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Ports"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  DELETE FROM switchport
  WHERE switchport.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  INSERT INTO switchport (name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id) 
  VALUES (new.name, new.socket_id, new.switch_id, new.ifacename, new.disabled, new.speed_id, new.switch_auth_id)
  RETURNING switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id, null::_hwsubviewb;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD ( UPDATE switchport SET id = new.id, name = new.name, socket_id = new.socket_id, switch_id = new.switch_id, ifacename = new.ifacename, disabled = new.disabled, speed_id = new.speed_id, switch_auth_id = new.switch_auth_id
  WHERE switchport.id = old.id;
 SELECT fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
);
