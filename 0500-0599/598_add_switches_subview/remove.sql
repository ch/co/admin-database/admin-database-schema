DROP VIEW hotwire3."10_View/Network/Switches/_switches";

DROP VIEW hotwire3."10_View/Network/Switches/Switch_Ports";

CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/Switch_Ports" AS
 SELECT switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id
   FROM switchport;

ALTER TABLE hotwire3."10_View/Network/Switches/Switch_Ports"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO dev;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3."10_View/Network/Switches/Switch_Ports" TO cos;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_del" AS
    ON DELETE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  DELETE FROM switchport
  WHERE switchport.id = old.id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_ins" AS
    ON INSERT TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD  INSERT INTO switchport (name, socket_id, switch_id, ifacename, disabled, speed_id, switch_auth_id)
  VALUES (new.name, new.socket_id, new.switch_id, new.ifacename, new.disabled, new.speed_id, new.switch_auth_id)
  RETURNING switchport.id, switchport.name, switchport.socket_id, switchport.switch_id, switchport.ifacename, switchport.disabled, switchport.speed_id, switchport.switch_auth_id, ARRAY( SELECT mm_switch_config_switch_port.switch_port_config_goal_id
           FROM mm_switch_config_switch_port
          WHERE mm_switch_config_switch_port.switch_port_id = switchport.id) AS switch_port_goal_id;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/Switches/Switch_Ports_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/Switches/Switch_Ports" DO INSTEAD ( UPDATE switchport SET id = new.id, name = new.name, socket_id = new.socket_id, switch_id = new.switch_id, ifacename = new.ifacename, disabled = new.disabled, speed_id = new.speed_id, switch_auth_id = new.switch_auth_id
  WHERE switchport.id = old.id;
 SELECT fn_mm_array_update(new.switch_port_goal_id, old.switch_port_goal_id, 'mm_switch_config_switch_port'::character varying, 'switch_port_id'::character varying, 'switch_port_config_goal_id'::character varying, old.id) AS fn_mm_array_update2;
);

