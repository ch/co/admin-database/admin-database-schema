ALTER TABLE research_group DROP COLUMN banned_for_lack_of_software_compliance;

-- person columns are nullable so that we can do GDPR pruning on them
ALTER TABLE person DROP COLUMN auto_banned_from_network;
ALTER TABLE person DROP COLUMN override_auto_ban;
ALTER TABLE person DROP COLUMN network_ban_log;
