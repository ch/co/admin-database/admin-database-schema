ALTER TABLE research_group ADD COLUMN banned_for_lack_of_software_compliance boolean DEFAULT false NOT NULL;

-- person columns are nullable so that we can do GDPR pruning on them
ALTER TABLE person ADD COLUMN auto_banned_from_network boolean DEFAULT FALSE;
ALTER TABLE person ADD COLUMN override_auto_ban boolean DEFAULT FALSE;
ALTER TABLE person ADD COLUMN network_ban_log varchar;
