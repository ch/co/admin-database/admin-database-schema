CREATE OR REPLACE VIEW apps.personal_data_selfservice_v5 AS
SELECT DISTINCT	person.id,
	person.surname::varchar AS ro_surname,
	person.first_names::varchar AS ro_first_names,
	title_hid.title_hid::varchar AS ro_title,
	person.known_as,
	person.name_suffix AS name_suffix_eg_frs,
	gender_hid.gender_hid::varchar AS ro_gender,
	person.previous_surname::varchar AS ro_previous_name,
        nationality_string.ro_nationality,
	person.email_address,
	person.hide_email AS hide_email_from_dept_websites,
	ARRAY ( 
		SELECT mm.room_id
		FROM mm_person_room mm
		WHERE mm.person_id = person.id
	) AS mm_room_id,
	ARRAY ( 
		SELECT mm.dept_telephone_number_id
		FROM mm_person_dept_telephone_number mm
		WHERE mm.person_id = person.id
	) AS mm_dept_telephone_number_id,
	cambridge_college_hid.cambridge_college_hid::varchar AS ro_cambridge_college,
	person.cambridge_address::text AS home_address,
	person.cambridge_phone_number AS home_phone_number,
	person.emergency_contact::text,
	research_groups_string.ro_research_group,
	post_category_hid.post_category_hid::varchar AS ro_post_category,
	supervisor_hid.supervisor_hid::varchar AS ro_supervisor,
	person.arrival_date AS ro_arrival_date,
	_latest_role_v12.funding_end_date AS ro_funding_end_date,
	COALESCE(_latest_role_v12.intended_end_date,_latest_role_v12.end_date) AS ro_expected_leaving_date,
	person.crsid AS ro_crsid,
	person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain,
	person.do_not_show_on_website AS hide_from_website,
	user_account.chemnet_token AS ro_chemnet_token,
	false::boolean AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username = person.crsid
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
   LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
   LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
   LEFT JOIN (
       SELECT person.id AS person_id, string_agg(nationality.nationality,', ')::varchar AS ro_nationality
       FROM person
       LEFT JOIN mm_person_nationality ON mm_person_nationality.person_id = person.id
       LEFT JOIN nationality ON mm_person_nationality.nationality_id = nationality.id
       GROUP BY person.id
   ) nationality_string ON nationality_string.person_id = person.id
   LEFT JOIN (
       SELECT person.id AS person_id, string_agg(research_group.name,', ')::varchar AS ro_research_group
       FROM person
       LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
       LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
       GROUP BY person.id
   ) research_groups_string ON research_groups_string.person_id = person.id
  WHERE person.ban_from_self_service <> true
GROUP BY person.id, title_hid.title_hid, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, intended_end_date, end_date, chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group
;

ALTER VIEW apps.personal_data_selfservice_v5 OWNER TO dev;
GRANT SELECT,UPDATE ON apps.personal_data_selfservice_v5 TO selfservice;

