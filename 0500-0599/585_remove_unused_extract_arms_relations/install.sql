DROP VIEW extract_arms_and_canopies_view;

DROP VIEW hotwire."10_View/Safety/Extract_Arms_and_Canopies";

DROP VIEW hotwire.extract_device_type_hid;
DROP VIEW hotwire3.extract_device_type_hid;

DROP VIEW hotwire.extract_arm_canopy_type_hid;
DROP VIEW hotwire3.extract_arm_canopy_type_hid;

DROP TABLE extract_arm_canopy;
DROP TABLE extract_device_type_hid;
DROP TABLE extract_arm_canopy_type_hid;
