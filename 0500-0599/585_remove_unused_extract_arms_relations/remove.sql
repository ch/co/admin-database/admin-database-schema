CREATE TABLE extract_device_type_hid
(
  extract_device_type_id bigserial NOT NULL,
  extract_device_type_hid character varying(80) NOT NULL,
  CONSTRAINT extract_device_type_hid_pkey PRIMARY KEY (extract_device_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE extract_device_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE extract_device_type_hid TO dev;
GRANT SELECT ON TABLE extract_device_type_hid TO ro_hid;
GRANT ALL ON TABLE extract_device_type_hid TO cen1001;

CREATE TABLE extract_arm_canopy_type_hid
(
  extract_arm_canopy_type_id bigserial NOT NULL,
  extract_arm_canopy_type_hid character varying(80) NOT NULL,
  CONSTRAINT extract_arm_canopy_type_hid_pkey PRIMARY KEY (extract_arm_canopy_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE extract_arm_canopy_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE extract_arm_canopy_type_hid TO dev;
GRANT SELECT ON TABLE extract_arm_canopy_type_hid TO ro_hid;
GRANT ALL ON TABLE extract_arm_canopy_type_hid TO cen1001;

CREATE TABLE extract_arm_canopy
(
  id bigserial NOT NULL,
  room_id bigint,
  extract_device_type_id bigint,
  device_number character varying(80),
  serial_number character varying(80),
  recirc_or_ducted_id bigint,
  manufacturer character varying(80),
  firetrace_type_id bigint,
  install_year integer,
  inspection_frequency interval,
  notes character varying(500),
  extract_arm_canopy_type_id bigint,
  last_inspection date,
  next_inspection date,
  CONSTRAINT extract_arm_canopy_pkey PRIMARY KEY (id),
  CONSTRAINT extract_arm_canopy_fk_extract_device_type FOREIGN KEY (extract_device_type_id)
      REFERENCES extract_device_type_hid (extract_device_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT extract_arm_canopy_fk_firetrace_type FOREIGN KEY (firetrace_type_id)
      REFERENCES firetrace_type_hid (firetrace_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT extract_arm_canopy_fk_recirc_or_ducted FOREIGN KEY (recirc_or_ducted_id)
      REFERENCES recirc_or_ducted_hid (recirc_or_ducted_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT extract_arm_canopy_fk_room FOREIGN KEY (room_id)
      REFERENCES room (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE extract_arm_canopy
  OWNER TO dev;
GRANT ALL ON TABLE extract_arm_canopy TO dev;
GRANT ALL ON TABLE extract_arm_canopy TO cen1001;

CREATE OR REPLACE VIEW extract_arms_and_canopies_view AS 
 SELECT extract_arm_canopy.id, extract_arm_canopy.room_id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id
   FROM extract_arm_canopy;

ALTER TABLE extract_arms_and_canopies_view
  OWNER TO cen1001;
GRANT ALL ON TABLE extract_arms_and_canopies_view TO cen1001;
GRANT SELECT ON TABLE extract_arms_and_canopies_view TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE extract_arms_and_canopies_view TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE extract_arms_and_canopies_view TO dev;

-- Rule: extract_arm_canopies_del ON extract_arms_and_canopies_view

-- DROP RULE extract_arm_canopies_del ON extract_arms_and_canopies_view;

CREATE OR REPLACE RULE extract_arm_canopies_del AS
    ON DELETE TO extract_arms_and_canopies_view DO INSTEAD  DELETE FROM extract_arm_canopy
  WHERE extract_arm_canopy.id = old.id;

-- Rule: extract_arm_canopies_upd ON extract_arms_and_canopies_view

-- DROP RULE extract_arm_canopies_upd ON extract_arms_and_canopies_view;

CREATE OR REPLACE RULE extract_arm_canopies_upd AS
    ON UPDATE TO extract_arms_and_canopies_view DO INSTEAD  UPDATE extract_arm_canopy SET room_id = new.room_id, extract_device_type_id = new.extract_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes, extract_arm_canopy_type_id = new.extract_arm_canopy_type_id
  WHERE extract_arm_canopy.id = old.id;

-- Rule: extract_arm_canopy_ins ON extract_arms_and_canopies_view

-- DROP RULE extract_arm_canopy_ins ON extract_arms_and_canopies_view;

CREATE OR REPLACE RULE extract_arm_canopy_ins AS
    ON INSERT TO extract_arms_and_canopies_view DO INSTEAD  INSERT INTO extract_arm_canopy (room_id, extract_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes, extract_arm_canopy_type_id) 
  VALUES (new.room_id, new.extract_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes, new.extract_arm_canopy_type_id)
  RETURNING extract_arm_canopy.id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.room_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id;


CREATE OR REPLACE VIEW hotwire."10_View/Safety/Extract_Arms_and_Canopies" AS 
 SELECT extract_arm_canopy.id, extract_arm_canopy.room_id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id
   FROM extract_arm_canopy;

ALTER TABLE hotwire."10_View/Safety/Extract_Arms_and_Canopies"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Safety/Extract_Arms_and_Canopies" TO cen1001;
GRANT SELECT ON TABLE hotwire."10_View/Safety/Extract_Arms_and_Canopies" TO mgmt_ro;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Extract_Arms_and_Canopies" TO safety_management;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Safety/Extract_Arms_and_Canopies" TO dev;

-- Rule: hotwire_view_extract_arm_canopies_del ON hotwire."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire_view_extract_arm_canopies_del ON hotwire."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire_view_extract_arm_canopies_del AS
    ON DELETE TO hotwire."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  DELETE FROM extract_arm_canopy
  WHERE extract_arm_canopy.id = old.id;

-- Rule: hotwire_view_extract_arm_canopies_upd ON hotwire."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire_view_extract_arm_canopies_upd ON hotwire."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire_view_extract_arm_canopies_upd AS
    ON UPDATE TO hotwire."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  UPDATE extract_arm_canopy SET room_id = new.room_id, extract_device_type_id = new.extract_device_type_id, device_number = new.device_number, serial_number = new.serial_number, recirc_or_ducted_id = new.recirc_or_ducted_id, manufacturer = new.manufacturer, firetrace_type_id = new.firetrace_type_id, install_year = new.install_year, inspection_frequency = new.inspection_frequency, last_inspection = new.last_inspection, next_inspection = new.next_inspection, notes = new.notes, extract_arm_canopy_type_id = new.extract_arm_canopy_type_id
  WHERE extract_arm_canopy.id = old.id;

-- Rule: hotwire_view_extract_arm_canopy_ins ON hotwire."10_View/Safety/Extract_Arms_and_Canopies"

-- DROP RULE hotwire_view_extract_arm_canopy_ins ON hotwire."10_View/Safety/Extract_Arms_and_Canopies";

CREATE OR REPLACE RULE hotwire_view_extract_arm_canopy_ins AS
    ON INSERT TO hotwire."10_View/Safety/Extract_Arms_and_Canopies" DO INSTEAD  INSERT INTO extract_arm_canopy (room_id, extract_device_type_id, device_number, serial_number, recirc_or_ducted_id, manufacturer, firetrace_type_id, install_year, inspection_frequency, last_inspection, next_inspection, notes, extract_arm_canopy_type_id) 
  VALUES (new.room_id, new.extract_device_type_id, new.device_number, new.serial_number, new.recirc_or_ducted_id, new.manufacturer, new.firetrace_type_id, new.install_year, new.inspection_frequency, new.last_inspection, new.next_inspection, new.notes, new.extract_arm_canopy_type_id)
  RETURNING extract_arm_canopy.id, extract_arm_canopy.extract_device_type_id, extract_arm_canopy.room_id, extract_arm_canopy.device_number, extract_arm_canopy.serial_number, extract_arm_canopy.recirc_or_ducted_id, extract_arm_canopy.manufacturer, extract_arm_canopy.firetrace_type_id, extract_arm_canopy.install_year, extract_arm_canopy.inspection_frequency, extract_arm_canopy.last_inspection, extract_arm_canopy.next_inspection, extract_arm_canopy.notes, extract_arm_canopy.extract_arm_canopy_type_id;



CREATE OR REPLACE VIEW hotwire.extract_device_type_hid AS 
 SELECT extract_device_type_hid.extract_device_type_id, extract_device_type_hid.extract_device_type_hid
   FROM extract_device_type_hid;

ALTER TABLE hotwire.extract_device_type_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.extract_device_type_hid TO postgres;
GRANT ALL ON TABLE hotwire.extract_device_type_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3.extract_device_type_hid AS 
 SELECT extract_device_type_hid.extract_device_type_id, extract_device_type_hid.extract_device_type_hid
   FROM extract_device_type_hid;

ALTER TABLE hotwire3.extract_device_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.extract_device_type_hid TO dev;
GRANT SELECT ON TABLE hotwire3.extract_device_type_hid TO public;


CREATE OR REPLACE VIEW hotwire.extract_arm_canopy_type_hid AS 
 SELECT extract_arm_canopy_type_hid.extract_arm_canopy_type_id, extract_arm_canopy_type_hid.extract_arm_canopy_type_hid
   FROM extract_arm_canopy_type_hid;

ALTER TABLE hotwire.extract_arm_canopy_type_hid
  OWNER TO postgres;
GRANT ALL ON TABLE hotwire.extract_arm_canopy_type_hid TO postgres;
GRANT ALL ON TABLE hotwire.extract_arm_canopy_type_hid TO ro_hid;

CREATE OR REPLACE VIEW hotwire3.extract_arm_canopy_type_hid AS 
 SELECT extract_arm_canopy_type_hid.extract_arm_canopy_type_id, extract_arm_canopy_type_hid.extract_arm_canopy_type_hid
   FROM extract_arm_canopy_type_hid;

ALTER TABLE hotwire3.extract_arm_canopy_type_hid
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3.extract_arm_canopy_type_hid TO dev;
GRANT SELECT ON TABLE hotwire3.extract_arm_canopy_type_hid TO public;


