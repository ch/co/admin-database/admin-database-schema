DROP VIEW hotwire3."10_View/People/Researcher_Staff_Reviews";
CREATE VIEW hotwire3."10_View/People/Researcher_Staff_Reviews" AS
SELECT
    person.id,
    person.surname as ro_surname,
    person.first_names as ro_first_names,
    person.title_id as ro_title_id,
    person.email_address as ro_email_address,
    _latest_role.post_category_id as ro_post_category_id,
    coalesce(person.usual_reviewer_id,_latest_role.supervisor_id) as ro_reviewer_id,
    coalesce(usual_reviewer.email_address,supervisor.email_address) as ro_reviewer_email_address,
    _latest_role.supervisor_id as ro_supervisor_id,
    person.next_review_due,
    last_review.date_of_meeting AS ro_date_of_last_review,
    NULL::date AS record_a_new_review,
    coalesce(person.usual_reviewer_id,_latest_role.supervisor_id) AS new_reviewer_id,
    NULL::text as new_review_notes,
    NULL::bigint AS update_usual_reviewer_id,
    person.silence_staff_review_reminders,
    coalesce(usual_reviewer.silence_staff_review_reminders,supervisor.silence_staff_review_reminders) AS silence_all_reminders_for_reviewer,
    CASE
        WHEN person.next_review_due < current_date THEN 'red'::text
        WHEN person.next_review_due < current_date + interval '1 month' THEN 'orange'::text
        ELSE NULL::text
    END AS _cssclass
FROM person
JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
JOIN person supervisor ON _latest_role.supervisor_id = supervisor.id
LEFT JOIN person usual_reviewer ON person.usual_reviewer_id = usual_reviewer.id
LEFT JOIN (
    SELECT
        person_id,
        max(date_of_meeting) AS date_of_meeting
    FROM staff_review_meeting
    WHERE date_of_meeting IS NOT NULL -- FIXME this col should not be nullable
    GROUP BY person_id
) last_review ON last_review.person_id = person.id
WHERE
    _physical_status.status_id = 'Current'
AND
   (
       _latest_role.post_category_id = 'sc-7' -- Senior PDRA
       OR
       _latest_role.post_category_id = 'sc-4' -- PDRA
       OR
       _latest_role.post_category_id = 'sc-10' -- Principal Research Associate
       OR
       _latest_role.post_category_id = 'sc-6' -- Research Fellow
       OR
       _latest_role.post_category_id = 'sc-5' -- Research Assistant
   )
ORDER BY next_review_due asc;
;

ALTER TABLE hotwire3."10_View/People/Researcher_Staff_Reviews" OWNER TO dev;
GRANT SELECT, UPDATE ON hotwire3."10_View/People/Researcher_Staff_Reviews" TO hr;

DROP FUNCTION hotwire3.researcher_staff_reviews();
CREATE FUNCTION hotwire3.researcher_staff_reviews() RETURNS TRIGGER AS
$$
BEGIN
    IF NEW.update_usual_reviewer_id IS NOT NULL 
    THEN
        UPDATE person SET usual_reviewer_id = NEW.update_usual_reviewer_id WHERE id = NEW.id;
    END IF;
    IF NEW.record_a_new_review IS NOT NULL AND NEW.new_reviewer_id IS NOT NULL
    THEN
        INSERT INTO staff_review_meeting ( date_of_meeting, reviewer_id, notes, person_id, last_updated ) VALUES ( NEW.record_a_new_review, NEW.new_reviewer_id, NEW.new_review_notes::varchar(500), NEW.id, current_timestamp );
        UPDATE person SET next_review_due = NEW.record_a_new_review + interval '1 year' WHERE id = NEW.id;
    END IF;
    IF NEW.next_review_due IS DISTINCT FROM OLD.next_review_due
    THEN
        UPDATE person SET next_review_due = NEW.next_review_due WHERE id = NEW.id;
    END IF;
    IF NEW.silence_staff_review_reminders IS DISTINCT FROM OLD.silence_staff_review_reminders
    THEN
        UPDATE person SET silence_staff_review_reminders = NEW.silence_staff_review_reminders WHERE id = OLD.id;
    END IF;
    IF NEW.silence_all_reminders_for_reviewer IS DISTINCT FROM OLD.silence_all_reminders_for_reviewer
    THEN
        UPDATE person SET silence_staff_review_reminders = NEW.silence_all_reminders_for_reviewer WHERE id = OLD.ro_reviewer_id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;

ALTER FUNCTION hotwire3.researcher_staff_reviews() OWNER TO dev;

CREATE TRIGGER update_researcher_reviews INSTEAD OF UPDATE ON hotwire3."10_View/People/Researcher_Staff_Reviews" FOR EACH ROW EXECUTE PROCEDURE hotwire3.researcher_staff_reviews();
