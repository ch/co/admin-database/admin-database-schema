CREATE OR REPLACE VIEW jump_to_person_from_erasmus_subview AS 
 SELECT erasmus_socrates_studentship.person_id AS id, erasmus_socrates_studentship.id AS erasmus_socrates_studentship_id, 'personnel history'::text AS view_to_jump_to, 'personnel_history_view'::text AS _targetview
   FROM erasmus_socrates_studentship;

ALTER TABLE jump_to_person_from_erasmus_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE jump_to_person_from_erasmus_subview TO cen1001;
GRANT SELECT ON TABLE jump_to_person_from_erasmus_subview TO student_management;
GRANT SELECT ON TABLE jump_to_person_from_erasmus_subview TO mgmt_ro;
GRANT SELECT ON TABLE jump_to_person_from_erasmus_subview TO dev;

CREATE OR REPLACE VIEW jump_to_person_from_part_iii_subview AS 
 SELECT part_iii_studentship.person_id AS id, part_iii_studentship.id AS part_iii_studentship_id, 'personnel history'::text AS view_to_jump_to, 'personnel_history_view'::text AS _targetview
   FROM part_iii_studentship;

ALTER TABLE jump_to_person_from_part_iii_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE jump_to_person_from_part_iii_subview TO cen1001;
GRANT SELECT ON TABLE jump_to_person_from_part_iii_subview TO student_management;
GRANT SELECT ON TABLE jump_to_person_from_part_iii_subview TO mgmt_ro;
GRANT SELECT ON TABLE jump_to_person_from_part_iii_subview TO dev;

CREATE OR REPLACE VIEW jump_to_person_from_post_history_subview AS 
 SELECT post_history.person_id AS id, post_history.id AS post_history_id, 'personnel history'::text AS view_to_jump_to, 'personnel_history_view'::text AS _targetview
   FROM post_history;

ALTER TABLE jump_to_person_from_post_history_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE jump_to_person_from_post_history_subview TO cen1001;
GRANT SELECT ON TABLE jump_to_person_from_post_history_subview TO hr;
GRANT SELECT ON TABLE jump_to_person_from_post_history_subview TO mgmt_ro;
GRANT SELECT ON TABLE jump_to_person_from_post_history_subview TO dev;

CREATE OR REPLACE VIEW jump_to_person_from_postgrad_subview AS 
         SELECT postgraduate_studentship.person_id || '-history'::text AS id, postgraduate_studentship.id AS postgraduate_studentship_id, postgraduate_studentship.person_id, 'personnel history'::text AS view_to_jump_to, 'personnel_history_view'::text AS _targetview
           FROM postgraduate_studentship
UNION 
         SELECT postgraduate_studentship.person_id || '-data-entry'::text AS id, postgraduate_studentship.id AS postgraduate_studentship_id, postgraduate_studentship.person_id, 'personnel data entry'::text AS view_to_jump_to, 'personnel_data_entry_view'::text AS _targetview
           FROM postgraduate_studentship;

ALTER TABLE jump_to_person_from_postgrad_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE jump_to_person_from_postgrad_subview TO cen1001;
GRANT SELECT ON TABLE jump_to_person_from_postgrad_subview TO student_management;
GRANT SELECT ON TABLE jump_to_person_from_postgrad_subview TO mgmt_ro;
GRANT SELECT ON TABLE jump_to_person_from_postgrad_subview TO dev;

CREATE OR REPLACE VIEW jump_to_person_from_visitor_subview AS 
 SELECT visitorship.person_id AS id, visitorship.id AS visitorship_id, 'personnel history'::text AS view_to_jump_to, 'personnel_history_view'::text AS _targetview
   FROM visitorship;

ALTER TABLE jump_to_person_from_visitor_subview
  OWNER TO cen1001;
GRANT ALL ON TABLE jump_to_person_from_visitor_subview TO cen1001;
GRANT SELECT ON TABLE jump_to_person_from_visitor_subview TO hr;
GRANT SELECT ON TABLE jump_to_person_from_visitor_subview TO mgmt_ro;
GRANT SELECT ON TABLE jump_to_person_from_visitor_subview TO dev;
