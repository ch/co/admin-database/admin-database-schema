DROP RULE "hotwire3_10_View/Network/MAC_to_VLAN2_ins" ON hotwire3."10_View/Network/MAC_to_VLAN";

DROP RULE "hotwire3_10_View/Network/MAC_to_VLAN2_upd" ON hotwire3."10_View/Network/MAC_to_VLAN";

CREATE FUNCTION hotwire3.mac_to_vlan_trig() RETURNS TRIGGER AS
$$
DECLARE
    v_id bigint;
    old_vid bigint := null;
BEGIN
    IF NEW.id IS NULL THEN
        INSERT INTO shadow_mac_to_vlan ( mac, vid ) VALUES ( NEW.mac, NEW.vlan_vid_id ) RETURNING id INTO v_id;
        NEW.id := v_id;
    ELSE
        old_vid := OLD.vlan_vid_id;
        v_id := NEW.id;
        UPDATE shadow_mac_to_vlan SET mac = NEW.mac, vid = NEW.vlan_vid_id WHERE id = NEW.id;
    END IF;
    INSERT INTO network_vlan_change (shadow_mac_to_vlan_id, mac, infraction_type_id, ticket, notes, old_vid, new_vid) VALUES (v_id, NEW.mac, NEW.infraction_type_id, NEW.ticket, NEW."Infraction notes", old_vid, NEW.vlan_vid_id);
    RETURN NEW;
END;
$$
LANGUAGE PLPGSQL
;

ALTER FUNCTION hotwire3.mac_to_vlan_trig() OWNER TO dev;

CREATE TRIGGER mac_to_vlan_infractions INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/Network/MAC_to_VLAN" FOR EACH ROW EXECUTE PROCEDURE hotwire3.mac_to_vlan_trig();
