DROP TRIGGER mac_to_vlan_infractions ON hotwire3."10_View/Network/MAC_to_VLAN";
DROP FUNCTION hotwire3.mac_to_vlan_trig();

CREATE OR REPLACE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_ins" AS
    ON INSERT TO hotwire3."10_View/Network/MAC_to_VLAN" DO INSTEAD  INSERT INTO shadow_mac_to_vlan (mac, vid) 
  VALUES (new.mac, new.vlan_vid_id)
  RETURNING shadow_mac_to_vlan.id, shadow_mac_to_vlan.mac, shadow_mac_to_vlan.vid, NULL::integer AS int4, NULL::integer AS int4, NULL::character varying AS "varchar", NULL::character varying AS "varchar", NULL::_hwsubviewb AS _hwsubviewb;

CREATE OR REPLACE RULE "hotwire3_10_View/Network/MAC_to_VLAN2_upd" AS
    ON UPDATE TO hotwire3."10_View/Network/MAC_to_VLAN" DO INSTEAD ( INSERT INTO network_vlan_change (shadow_mac_to_vlan_id, mac, infraction_type_id, ticket, notes, old_vid, new_vid) 
  VALUES (old.id, new.mac, new.infraction_type_id, new.ticket, new."Infraction notes", old.vlan_vid_id, new.vlan_vid_id);
 UPDATE shadow_mac_to_vlan SET id = new.id, mac = new.mac, vid = new.vlan_vid_id
  WHERE shadow_mac_to_vlan.id = old.id;
);


