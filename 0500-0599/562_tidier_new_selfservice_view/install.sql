CREATE VIEW apps.personal_data_selfservice_v5 AS
SELECT DISTINCT	person.id,
	person.surname::varchar AS ro_surname,
	person.first_names::varchar AS ro_first_names,
	title_hid.title_hid::varchar AS ro_title,
	person.known_as,
	person.name_suffix AS name_suffix_eg_frs,
	gender_hid.gender_hid::varchar AS ro_gender,
	person.previous_surname::varchar AS ro_previous_name,
	string_agg( 
            (
		SELECT nationality
		FROM mm_person_nationality mm
                JOIN nationality ON mm.nationality_id = nationality.id
		WHERE mm.person_id = person.id
	    ), ','
        )::varchar AS ro_nationality,
	person.email_address,
	person.hide_email AS hide_email_from_dept_websites,
	ARRAY ( 
		SELECT mm.room_id
		FROM mm_person_room mm
		WHERE mm.person_id = person.id
	) AS mm_room_id,
	ARRAY ( 
		SELECT mm.dept_telephone_number_id
		FROM mm_person_dept_telephone_number mm
		WHERE mm.person_id = person.id
	) AS mm_dept_telephone_number_id,
	cambridge_college_hid.cambridge_college_hid::varchar AS ro_cambridge_college,
	person.cambridge_address::text AS home_address,
	person.cambridge_phone_number AS home_phone_number,
	person.emergency_contact::text,
	string_agg(
            (
		SELECT research_group.name
		FROM mm_person_research_group mm
                JOIN research_group ON research_group.id = mm.research_group_id
		WHERE mm.person_id = person.id
            ),','
	)::varchar AS ro_research_group,
	post_category_hid.post_category_hid::varchar AS ro_post_category,
	supervisor_hid.supervisor_hid::varchar AS ro_supervisor,
	person.arrival_date AS ro_arrival_date,
	_latest_role_v12.funding_end_date AS ro_funding_end_date,
	COALESCE(_latest_role_v12.intended_end_date,_latest_role_v12.end_date) AS ro_expected_leaving_date,
	person.crsid AS ro_crsid,
	person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain,
	person.do_not_show_on_website AS hide_from_website,
	user_account.chemnet_token AS ro_chemnet_token,
	false::boolean AS create_new_token
   FROM person
   LEFT JOIN _latest_role_v12 ON person.id = _latest_role_v12.person_id
   LEFT JOIN user_account ON user_account.username = person.crsid
   LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
   LEFT JOIN supervisor_hid USING (supervisor_id)
   LEFT JOIN gender_hid USING (gender_id)
   LEFT JOIN title_hid USING (title_id)
   LEFT JOIN post_category_hid USING (post_category_id)
  WHERE person.ban_from_self_service <> true
GROUP BY person.id, title_hid.title_hid, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role_v12.funding_end_date, intended_end_date, end_date, chemnet_token
;

ALTER VIEW apps.personal_data_selfservice_v5 OWNER TO dev;
GRANT SELECT,UPDATE ON apps.personal_data_selfservice_v5 TO selfservice;

CREATE OR REPLACE FUNCTION apps_personal_data_v5_upd() RETURNS TRIGGER AS
$body$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address::varchar(500),
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact::varchar(500),
            managed_mail_domain_optout = NEW.opt_out_of_chemistry_mail_domain,
            do_not_show_on_website = NEW.hide_from_website
    WHERE id = OLD.id;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.mm_dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.mm_room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$body$ LANGUAGE plpgsql;
ALTER FUNCTION apps_personal_data_v5_upd() OWNER TO dev;

CREATE TRIGGER apps_personal_data_v5_trig INSTEAD OF UPDATE ON apps.personal_data_selfservice_v5 FOR EACH ROW EXECUTE PROCEDURE apps_personal_data_v5_upd();
