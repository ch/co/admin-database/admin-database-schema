DROP VIEW hotwire3."10_View/People/Titles";
CREATE VIEW hotwire3."10_View/People/Titles" AS
SELECT 
    title_hid.title_id AS id,
    title_hid.title_hid AS title,
    title_hid.long_title_hid AS long_title,
    title_hid.website_title_hid AS title_shown_on_websites
   FROM title_hid;

ALTER VIEW hotwire3."10_View/People/Titles" OWNER TO dev;

CREATE RULE hotwire3_people_titles_ins AS ON INSERT TO hotwire3."10_View/People/Titles" DO INSTEAD  INSERT INTO title_hid (title_hid, long_title_hid, website_title_hid) VALUES (new.title, new.long_title, new.title_shown_on_websites) RETURNING title_hid.title_id, title_hid.title_hid, title_hid.long_title_hid, title_hid.website_title_hid;

CREATE RULE hotwire3_people_titles_upd AS ON UPDATE TO hotwire3."10_View/People/Titles" DO INSTEAD  UPDATE title_hid SET title_hid = new.title, long_title_hid = new.long_title, website_title_hid = new.title_shown_on_websites WHERE title_hid.title_id = old.id; 

ALTER TABLE title_hid DROP COLUMN salutation_title_hid ;
