ALTER TABLE title_hid ADD COLUMN salutation_title_hid varchar;
UPDATE title_hid SET salutation_title_hid = title_hid;
UPDATE title_hid SET salutation_title_hid = 'Professor' WHERE title_hid like 'Prof%';
-- INSERT INTO title_hid (title_hid, long_title_hid, salutation_title_hid) VALUES ('Mx','Mx','Mx');

DROP VIEW hotwire3."10_View/People/Titles";
CREATE VIEW hotwire3."10_View/People/Titles" AS
SELECT 
    title_hid.title_id AS id,
    title_hid.title_hid AS title,
    title_hid.long_title_hid AS long_title,
    title_hid.website_title_hid AS title_shown_on_websites,
    title_hid.salutation_title_hid AS title_for_written_salutation
   FROM title_hid;

ALTER VIEW hotwire3."10_View/People/Titles" OWNER TO dev;

CREATE RULE hotwire3_people_titles_ins AS ON INSERT TO hotwire3."10_View/People/Titles" DO INSTEAD  INSERT INTO title_hid (title_hid, long_title_hid, website_title_hid, salutation_title_hid) VALUES (new.title, new.long_title, new.title_shown_on_websites,new.title_for_written_salutation) RETURNING title_hid.title_id, title_hid.title_hid, title_hid.long_title_hid, title_hid.website_title_hid, title_hid.salutation_title_hid;

CREATE RULE hotwire3_people_titles_upd AS ON UPDATE TO hotwire3."10_View/People/Titles" DO INSTEAD  UPDATE title_hid SET title_hid = new.title, long_title_hid = new.long_title, website_title_hid = new.title_shown_on_websites, salutation_title_hid = new.title_for_written_salutation WHERE title_hid.title_id = old.id; 

ALTER TABLE title_hid ALTER COLUMN salutation_title_hid SET NOT NULL;
