-- ALTER TABLE staff_review_meeting ADD COLUMN post_history_id bigint;
-- ALTER TABLE staff_review_meeting ADD CONSTRAINT staff_review_meeting_fk_post_history FOREIGN KEY (post_history_id) REFERENCES post_history(id);
ALTER TABLE staff_review_meeting ALTER COLUMN date_of_meeting DROP NOT NULL;
