CREATE FUNCTION mm_person_research_group_propagate_group_bans(p_id bigint) RETURNS VOID AS
$$
DECLARE
    person_is_banned boolean;
    person_should_be_banned boolean := 'f';
BEGIN
    -- Is the person currently banned or not?
    SELECT auto_banned_from_network FROM person WHERE person.id = p_id INTO person_is_banned;
    -- what should their ban status be?
    -- We ban a person if they are in any banned group. 
    SELECT bool_or(banned_for_lack_of_software_compliance)
    FROM research_group
    JOIN mm_person_research_group ON mm_person_research_group.research_group_id = research_group.id
    WHERE mm_person_research_group.person_id = p_id INTO person_should_be_banned;

    IF person_is_banned IS DISTINCT FROM person_should_be_banned
    THEN
        -- If they absolutely should be banned, ban em
        IF person_should_be_banned = 't'
        THEN
            UPDATE person SET 
                auto_banned_from_network = 't',
                network_ban_log = COALESCE(network_ban_log,'') || E'Banning at ' || current_timestamp || ' by virtue of group membership change\n'
            WHERE id = p_id;
        END IF;
        -- If they absolutely should not be banned, unban them
        -- unless person_is_banned was null, in which case leave it alone
        -- because we don't want to update long-gone people
        IF person_should_be_banned = 'f' AND person_is_banned = 't'
        THEN
            UPDATE person SET 
                auto_banned_from_network = 'f',
                override_auto_ban = 'f',
                network_ban_log = COALESCE(network_ban_log,'') || E'Unbanning at ' || current_timestamp || ' by virtue of group membership change\n'
            WHERE id = p_id;
        END IF;
        -- If we have no remaining group memberships and they were banned, unban them.
        -- Otherwise do nothing; we won't be blocking them by mistake and this situation
        -- is most likely to arise when purging so we don't want to make any extra changes
        IF person_should_be_banned IS NULL AND person_is_banned = 't'
        THEN
            UPDATE person SET 
                auto_banned_from_network = 'f',
                override_auto_ban = 'f',
                network_ban_log = COALESCE(network_ban_log,'') || E'Unbanning at ' || current_timestamp || ' because no remaining group memberships\n'
            WHERE id = p_id;
        END IF;
    END IF;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION mm_person_research_group_propagate_group_bans(bigint) OWNER TO dev;

CREATE FUNCTION mm_person_research_group_del_propagate_group_bans() RETURNS TRIGGER AS
$$
BEGIN
    PERFORM mm_person_research_group_propagate_group_bans(OLD.person_id);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

CREATE FUNCTION mm_person_research_group_ins_propagate_group_bans() RETURNS TRIGGER AS
$$
BEGIN
    PERFORM mm_person_research_group_propagate_group_bans(NEW.person_id);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

CREATE FUNCTION mm_person_research_group_upd_propagate_group_bans() RETURNS TRIGGER AS
$$
BEGIN
    IF OLD.person_id <> NEW.person_id
    THEN
        PERFORM mm_person_research_group_propagate_group_bans(OLD.person_id);
    END IF;
    PERFORM mm_person_research_group_propagate_group_bans(NEW.person_id);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public, pg_temp;

GRANT CREATE ON SCHEMA public TO useradder;
ALTER FUNCTION mm_person_research_group_del_propagate_group_bans() OWNER TO useradder;
ALTER FUNCTION mm_person_research_group_ins_propagate_group_bans() OWNER TO useradder;
ALTER FUNCTION mm_person_research_group_upd_propagate_group_bans() OWNER TO useradder;
REVOKE CREATE ON SCHEMA public FROM useradder;

REVOKE ALL ON FUNCTION mm_person_research_group_del_propagate_group_bans() FROM PUBLIC;
REVOKE ALL ON FUNCTION mm_person_research_group_ins_propagate_group_bans() FROM PUBLIC;
REVOKE ALL ON FUNCTION mm_person_research_group_upd_propagate_group_bans() FROM PUBLIC;
GRANT EXECUTE ON FUNCTION mm_person_research_group_del_propagate_group_bans() TO dev,cos,hr,student_management,photography,reception_cover,reception;

CREATE TRIGGER propagate_group_bans_del AFTER DELETE ON mm_person_research_group FOR EACH ROW EXECUTE PROCEDURE mm_person_research_group_del_propagate_group_bans();
CREATE TRIGGER propagate_group_bans_ins AFTER INSERT ON mm_person_research_group FOR EACH ROW EXECUTE PROCEDURE mm_person_research_group_ins_propagate_group_bans();
CREATE TRIGGER propagate_group_bans_upd AFTER UPDATE ON mm_person_research_group FOR EACH ROW EXECUTE PROCEDURE mm_person_research_group_upd_propagate_group_bans();

GRANT SELECT (id,auto_banned_from_network,override_auto_ban,network_ban_log) ON person to useradder;
GRANT SELECT ON mm_person_research_group TO useradder;
GRANT UPDATE (auto_banned_from_network,override_auto_ban,network_ban_log) ON person to useradder;
GRANT SELECT (id,banned_for_lack_of_software_compliance) ON research_group TO useradder;
