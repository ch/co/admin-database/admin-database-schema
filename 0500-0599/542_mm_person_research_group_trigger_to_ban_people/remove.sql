DROP TRIGGER propagate_group_bans_del ON mm_person_research_group;
DROP TRIGGER propagate_group_bans_ins ON mm_person_research_group;
DROP TRIGGER propagate_group_bans_upd ON mm_person_research_group;
DROP FUNCTION mm_person_research_group_del_propagate_group_bans();
DROP FUNCTION mm_person_research_group_ins_propagate_group_bans();
DROP FUNCTION mm_person_research_group_upd_propagate_group_bans();
DROP FUNCTION mm_person_research_group_propagate_group_bans(p_id bigint);
REVOKE SELECT (id,auto_banned_from_network,override_auto_ban,network_ban_log) ON person FROM useradder;
REVOKE SELECT ON mm_person_research_group FROM useradder;
REVOKE UPDATE (auto_banned_from_network,override_auto_ban,network_ban_log) ON person FROM useradder;
REVOKE SELECT (id,banned_for_lack_of_software_compliance) ON research_group FROM useradder;

