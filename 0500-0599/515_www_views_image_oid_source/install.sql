CREATE OR REPLACE VIEW www.intranet_staff_list AS 
 SELECT person.image_lo::bigint AS image_oid, person.crsid, www_person_hid_v2.www_person_hid, (person.surname::text || ', '::text) || COALESCE(person.known_as, person.first_names)::text AS sortable_name, 
        CASE
            WHEN _latest_role_v12.role_tablename = 'post_history'::text THEN post_history.job_title
            ELSE NULL::character varying(120)
        END AS job_title
   FROM person
   LEFT JOIN www_person_hid_v2 ON www_person_hid_v2.person_id = person.id
   LEFT JOIN _latest_role_v12 ON _latest_role_v12.person_id = person.id
   LEFT JOIN post_history ON post_history.id = _latest_role_v12.role_id
  WHERE _latest_role_v12.status::text = 'Current'::text AND (_latest_role_v12.post_category_id = ANY (ARRAY['sc-3'::text, 'sc-2'::text]));

ALTER TABLE www.intranet_staff_list OWNER TO dev;
GRANT ALL ON TABLE www.intranet_staff_list TO dev;
GRANT SELECT ON TABLE www.intranet_staff_list TO www_intranet;
GRANT ALL ON TABLE www.intranet_staff_list TO postgres;
COMMENT ON VIEW www.intranet_staff_list
  IS 'This view deliberately does not respect the do_not_show_on_website field. In turn, only www_intranet has SELECT permissions, and this user is specifically used on the intranet in tandem with the chemistry_intranet_staff module to produce a View that has Dept-only access controls in place.';

CREATE OR REPLACE VIEW www.firstaiders AS 
 SELECT firstaider.id, person.image_lo::bigint AS _image_oid, person_hid.person_hid, firstaider.firstaider_funding_id, firstaider.requalify_date, firstaider.hf_cn_trained, array_to_string(ARRAY( SELECT DISTINCT building_hid.building_hid
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
   LEFT JOIN building_hid USING (building_id)
  WHERE p2.id = firstaider.person_id
  ORDER BY building_hid.building_hid), '/'::text) AS ro_building, array_to_string(ARRAY( SELECT DISTINCT building_floor_hid.building_floor_hid
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
   LEFT JOIN building_floor_hid USING (building_floor_id)
  WHERE p2.id = firstaider.person_id
  ORDER BY building_floor_hid.building_floor_hid), '/'::text) AS ro_building_floor, array_to_string(ARRAY( SELECT DISTINCT room.name
           FROM person p2
      LEFT JOIN mm_person_room ON p2.id = mm_person_room.person_id
   LEFT JOIN room ON room.id = mm_person_room.room_id
  WHERE p2.id = firstaider.person_id
  ORDER BY room.name), '/'::text) AS ro_room, _physical_status_v2.status_id AS status
   FROM firstaider
   JOIN person ON firstaider.person_id = person.id
   JOIN person_hid USING (person_id)
   JOIN _physical_status_v2 USING (person_id);

ALTER TABLE www.firstaiders OWNER TO dev;
GRANT SELECT ON TABLE www.firstaiders TO www_sites;

CREATE OR REPLACE VIEW www.firstaiders_v2 AS 
 SELECT DISTINCT firstaider.id, person.image_lo::bigint AS _image_oid, person_hid.person_hid AS person, firstaider.firstaider_funding_id, firstaider.requalify_date, 
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained, building_hid.building_hid AS building, building_floor_hid.building_floor_hid AS building_floor, array_to_string(ARRAY( SELECT r2.name
           FROM room r2
      JOIN mm_person_room mp2 ON r2.id = mp2.room_id
   JOIN person p2 ON mp2.person_id = p2.id
  WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms, _physical_status_v2.status_id AS status
   FROM firstaider
   JOIN person ON firstaider.person_id = person.id
   JOIN person_hid USING (person_id)
   LEFT JOIN mm_person_room USING (person_id)
   LEFT JOIN room ON mm_person_room.room_id = room.id
   LEFT JOIN building_hid ON room.building_id = building_hid.building_id
   LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
   JOIN _physical_status_v2 USING (person_id)
  ORDER BY building_hid.building_hid, building_floor_hid.building_floor_hid, person_hid.person_hid, firstaider.id, person.image_lo::bigint, firstaider.firstaider_funding_id, firstaider.requalify_date, 
CASE
    WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
    ELSE NULL::character varying(3)
END, array_to_string(ARRAY( SELECT r2.name
   FROM room r2
   JOIN mm_person_room mp2 ON r2.id = mp2.room_id
   JOIN person p2 ON mp2.person_id = p2.id
  WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text), _physical_status_v2.status_id;

ALTER TABLE www.firstaiders_v2 OWNER TO dev;
GRANT SELECT ON TABLE www.firstaiders_v2 TO www_sites;

CREATE OR REPLACE VIEW www.first_aiders_v3 AS 
 SELECT DISTINCT firstaider.id, person.image_lo::bigint AS _image_oid, person_hid.person_hid AS person, firstaider.firstaider_funding_id, firstaider.requalify_date, 
        CASE
            WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS hf_cn_trained, 
        CASE
            WHEN firstaider.defibrillator_trained = true THEN 'Yes'::character varying(3)
            ELSE NULL::character varying(3)
        END AS defibrillator_trained, building_hid.building_hid AS building, building_floor_hid.building_floor_hid AS building_floor, array_to_string(ARRAY( SELECT r2.name
           FROM room r2
      JOIN mm_person_room mp2 ON r2.id = mp2.room_id
   JOIN person p2 ON mp2.person_id = p2.id
  WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text) AS rooms, _physical_status_v2.status_id AS status, (building_hid.building_id * 100)::numeric + 
        CASE
            WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN (-0.5)
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
            WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
            ELSE building_floor_hid.building_floor_id::numeric
        END AS floor_sort, 
        CASE
            WHEN firstaider.requalify_date > ('now'::text::date - '1 mon'::interval) THEN true
            ELSE false
        END AS qualification_up_to_date
   FROM firstaider
   JOIN person ON firstaider.person_id = person.id
   JOIN person_hid USING (person_id)
   LEFT JOIN mm_person_room USING (person_id)
   LEFT JOIN room ON mm_person_room.room_id = room.id
   LEFT JOIN building_hid ON room.building_id = building_hid.building_id
   LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
   JOIN _physical_status_v2 USING (person_id)
  ORDER BY (building_hid.building_id * 100)::numeric + 
CASE
    WHEN building_floor_hid.building_floor_hid::text ~~ 'Basement'::text THEN (-0.5)
    WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'Main building'::text THEN 1.5
    WHEN building_floor_hid.building_floor_hid::text ~~ '%Mezzanine%'::text AND building_hid.building_hid::text = 'UCC'::text THEN 2.5
    ELSE building_floor_hid.building_floor_id::numeric
END, firstaider.id, person.image_lo::bigint, person_hid.person_hid, firstaider.firstaider_funding_id, firstaider.requalify_date, 
CASE
    WHEN firstaider.hf_cn_trained = true THEN 'Yes'::character varying(3)
    ELSE NULL::character varying(3)
END, building_hid.building_hid, building_floor_hid.building_floor_hid, array_to_string(ARRAY( SELECT r2.name
   FROM room r2
   JOIN mm_person_room mp2 ON r2.id = mp2.room_id
   JOIN person p2 ON mp2.person_id = p2.id
  WHERE p2.id = firstaider.person_id AND r2.building_id = building_hid.building_id AND r2.building_floor_id = building_floor_hid.building_floor_id), '/'::text), _physical_status_v2.status_id;

ALTER TABLE www.first_aiders_v3 OWNER TO dev;
GRANT ALL ON TABLE www.first_aiders_v3 TO dev;
GRANT SELECT ON TABLE www.first_aiders_v3 TO www_sites;

