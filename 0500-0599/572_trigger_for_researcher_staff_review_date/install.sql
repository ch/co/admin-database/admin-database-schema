CREATE FUNCTION set_researcher_staff_review_due() RETURNS TRIGGER AS
$$
-- ensure we have a valid 'previous start date' for comparisons; null if this is an insert
DECLARE
    previous_start_date date := NULL;
    orig_next_review_due date;
BEGIN
    IF TG_OP = 'UPDATE'
    THEN
        previous_start_date := OLD.start_date;
    END IF;

-- check staff category
    IF NEW.staff_category_id <> 4 AND NEW.staff_category_id <> 5 AND NEW.staff_category_id <> 6 AND NEW.staff_category_id <> 7 AND NEW.staff_category_id <> 10
    THEN
        RETURN NULL;
    END IF;

-- check if we changed the start date
    IF NEW.start_date IS NOT DISTINCT FROM previous_start_date
    THEN
        RETURN NULL;
    END IF;

    SELECT next_review_due FROM person WHERE id = NEW.person_id INTO orig_next_review_due;
-- check person review date is blank
    IF orig_next_review_due IS NULL and NEW.start_date IS NOT NULL
    THEN
        UPDATE person SET next_review_due = NEW.start_date + interval '1 year' WHERE id = NEW.person_id;
    END IF;
-- what happens if next review due date is > intended end date? KJP says that's OK as contracts
-- are very often renewed and then we would still want to do a review at original date + 1 year
    RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

ALTER FUNCTION set_researcher_staff_review_due() OWNER TO dev;

CREATE TRIGGER staff_review_due AFTER INSERT OR UPDATE ON post_history FOR EACH ROW EXECUTE PROCEDURE set_researcher_staff_review_due();
