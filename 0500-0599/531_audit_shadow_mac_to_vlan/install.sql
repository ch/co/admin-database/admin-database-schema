CREATE TRIGGER shadow_mac_to_vlan_audit_trig AFTER INSERT OR DELETE OR UPDATE ON shadow_mac_to_vlan FOR EACH ROW EXECUTE PROCEDURE audit_generic();
