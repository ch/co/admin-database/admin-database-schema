DROP RULE fire_warden_ins ON hotwire3."10_View/Safety/Fire_wardens";
DROP RULE fire_warden_upd ON hotwire3."10_View/Safety/Fire_wardens";
DROP FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens");

CREATE FUNCTION hotwire3.fire_wardens_trig() RETURNS TRIGGER AS
$$
BEGIN
    if new.main_firewarden_for_area = 't' and new.fire_warden_area_id is not null
    then
        update fire_warden set is_primary = 'f' where fire_warden_area_id = new.fire_warden_area_id;
    end if;
    if new.id is not null 
    then
        update fire_warden set 
            qualification_date = new.qualification_date,
            fire_warden_area_id = new.fire_warden_area_id,
            is_primary = new.main_firewarden_for_area
        where fire_warden_id = new.id;
    else
        insert into fire_warden ( 
            qualification_date,
            person_id,
            fire_warden_area_id,
            is_primary
        ) values (
            new.qualification_date,
            new.person_id,
            new.fire_warden_area_id,
            coalesce(new.main_firewarden_for_area,'f')
        ) returning fire_warden_id into new.id;
    end if;
    return new;     
END;
$$ LANGUAGE PLPGSQL;

ALTER FUNCTION hotwire3.fire_wardens_trig() OWNER TO dev;

CREATE TRIGGER fire_warden_upd INSTEAD OF UPDATE OR INSERT ON hotwire3."10_View/Safety/Fire_wardens" FOR EACH ROW EXECUTE PROCEDURE hotwire3.fire_wardens_trig();
