DROP TRIGGER fire_warden_upd ON hotwire3."10_View/Safety/Fire_wardens";
DROP FUNCTION hotwire3.fire_wardens_trig();

CREATE OR REPLACE FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens")
  RETURNS bigint AS
$BODY$
declare
    v alias for $1;
    v_fire_warden_id bigint;
begin
    if v.main_firewarden_for_area = 't' and v.fire_warden_area_id is not null
    then
        update fire_warden set is_primary = 'f' where fire_warden_area_id = v.fire_warden_area_id;
    end if;
    if v.id is not null 
    then
        v_fire_warden_id = v.id;
        update fire_warden set 
            qualification_date = v.qualification_date,
            fire_warden_area_id = v.fire_warden_area_id,
            is_primary = v.main_firewarden_for_area
        where fire_warden_id = v_fire_warden_id;
    else
        insert into fire_warden ( 
            qualification_date,
            person_id,
            fire_warden_area_id,
            is_primary
        ) values (
            v.qualification_date,
            v.person_id,
            v.fire_warden_area_id,
            coalesce(v.main_firewarden_for_area,'t')
        ) returning fire_warden_id into v_fire_warden_id;
    end if;
    return v_fire_warden_id;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.fire_warden_upd(hotwire3."10_View/Safety/Fire_wardens")
  OWNER TO dev;

CREATE OR REPLACE RULE fire_warden_ins AS
    ON INSERT TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  SELECT hotwire3.fire_warden_upd(new.*) AS fn_fire_warden_upd;

CREATE OR REPLACE RULE fire_warden_upd AS
        ON UPDATE TO hotwire3."10_View/Safety/Fire_wardens" DO INSTEAD  SELECT hotwire3.fire_warden_upd(new.*) AS fn_fire_warden_upd;

