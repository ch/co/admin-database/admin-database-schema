REVOKE ALL ON person FROM selfservice;
GRANT SELECT,UPDATE,INSERT,DELETE ON person TO selfservice;
REVOKE ALL ON user_account FROM selfservice;
GRANT ALL ON user_account TO selfservice;
