REVOKE ALL ON person FROM selfservice;
GRANT UPDATE (known_as, name_suffix, email_address, hide_email, cambridge_address, cambridge_phone_number, emergency_contact, managed_mail_domain_optout, do_not_show_on_website ) ON person TO selfservice;
GRANT SELECT (id,auto_banned_from_network,override_auto_ban) ON person TO selfservice;
REVOKE ALL ON user_account FROM selfservice;
GRANT UPDATE (chemnet_token) ON user_account TO selfservice;
-- Do we really need the insert rights? 
GRANT INSERT (username, person_id, chemnet_token) ON user_account TO selfservice;
GRANT SELECT (username, person_id) ON user_account TO selfservice;
