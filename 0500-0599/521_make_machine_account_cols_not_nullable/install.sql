-- Warning: machine_account is slony-replicated so this must be applied
-- using slony tools
ALTER TABLE machine_account ALTER COLUMN name SET NOT NULL;
ALTER TABLE machine_account ALTER COLUMN system_image_id SET NOT NULL;
