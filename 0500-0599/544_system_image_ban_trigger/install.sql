CREATE FUNCTION system_image_update_bans() RETURNS TRIGGER AS
$$
DECLARE
    group_banned boolean;
    owner_banned boolean;
    user_banned boolean;
    system_should_be_banned boolean;
    system_was_banned boolean;
    ban_log varchar;
BEGIN
    IF (TG_OP = 'UPDATE')
    THEN
        SELECT OLD.auto_banned_from_network INTO system_was_banned;
    END IF;

    SELECT banned_for_lack_of_software_compliance FROM research_group WHERE id = NEW.research_group_id INTO group_banned;
    SELECT COALESCE(bool_or(banned_for_lack_of_software_compliance),'f') FROM research_group JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id WHERE mm_person_research_group.person_id = NEW.user_id INTO user_banned;
    SELECT COALESCE(bool_or(banned_for_lack_of_software_compliance),'f') FROM research_group JOIN mm_person_research_group ON research_group.id = mm_person_research_group.research_group_id JOIN hardware ON mm_person_research_group.person_id = hardware.owner_id WHERE hardware.id = NEW.hardware_id INTO owner_banned;


    SELECT group_banned OR owner_banned OR user_banned INTO system_should_be_banned;

    IF group_banned = 't'
    THEN
        ban_log := COALESCE(ban_log,'') || E'research group for this system is banned';
    END IF;
    IF owner_banned = 't'
    THEN
        ban_log := COALESCE((ban_log || ' and '),'') || E'owner for this system is banned';
    END IF;
    IF user_banned = 't'
    THEN
        ban_log := COALESCE((ban_log || ' and '),'') || E'user for this system is banned';
    END IF;
    IF system_should_be_banned = 'f'
    THEN
        ban_log := E'no reason to ban this system';
    END IF;
    
    IF system_was_banned IS DISTINCT FROM system_should_be_banned
    THEN
        NEW.auto_banned_from_network := system_should_be_banned;
        NEW.network_ban_log := COALESCE(NEW.network_ban_log,'') || E'Auto ban status changed at ' || current_timestamp || E' because ' || ban_log || E'\n' ;
    END IF;
    IF system_should_be_banned = 'f' OR system_should_be_banned IS NULL
    THEN
        NEW.override_auto_ban := 'f';
    END IF;
    IF system_was_banned = 't' AND system_should_be_banned = 't'
    THEN
        NEW.network_ban_log := COALESCE(NEW.network_ban_log,'') || E'Maintaining auto ban at ' || current_timestamp || ' because ' || ban_log || E'\n';
    END IF;

    RETURN NEW;
    
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION system_image_update_bans() OWNER TO dev;

CREATE TRIGGER check_bans BEFORE INSERT OR UPDATE ON system_image FOR EACH ROW EXECUTE PROCEDURE system_image_update_bans();

GRANT SELECT ON mm_person_research_group,research_group TO osbuilder,groupitreps;

-- perms: cos (no problem) osbuilder (?) groupitreps (bother)
