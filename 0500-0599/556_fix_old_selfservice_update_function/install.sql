DROP RULE personal_data_upd ON personal_data_view;
DROP FUNCTION fn_personal_data_upd(personal_data_view);
DROP VIEW personal_data_view;

CREATE OR REPLACE VIEW personal_data_view AS 
SELECT DISTINCT person.id,
  person.surname AS ro_surname,
  person.first_names AS ro_first_names,
  person.title_id AS ro_title_id,
  person.known_as,
  person.name_suffix AS name_suffix_eg_frs,
  person.gender_id AS ro_gender_id,
  person.previous_surname AS ro_maiden_name,
  mm_person_nationality.nationality_id AS mm_ro_nationality_id,
  nationality_hid.nationality_hid AS mm_nationality,
  person.email_address,
  person.hide_email AS hide_email_from_dept_websites,
  room_hid.room_hid AS mm_room,
  mm_person_room.room_id AS mm_room_id,
  dept_telephone_number_hid.dept_telephone_number_hid AS mm_dept_telephone_number,
  mm_person_dept_telephone_number.dept_telephone_number_id AS mm_dept_telephone_number_id,
  person.cambridge_college_id AS ro_cambridge_college_id,
  person.cambridge_address as home_address,
  person.cambridge_phone_number as home_phone_number,
  person.emergency_contact,
  array_to_string(ARRAY(
      SELECT research_group.name
      FROM mm_person_research_group mm
      JOIN research_group ON mm.research_group_id = research_group.id
      WHERE mm.person_id = person.id
  ),','
  ) AS ro_research_groups,
  _latest_role.post_category_id AS ro_post_category_id,
  _latest_role.supervisor_id AS ro_supervisor_id,
  person.arrival_date AS ro_arrival_date,
  _latest_role.funding_end_date AS ro_funding_end_date,
  COALESCE(_latest_role.intended_end_date, _latest_role.end_date) AS ro_expected_leaving_date,
  person.crsid AS ro_crsid,
  person.managed_mail_domain_optout AS opt_out_of_chemistry_mail_domain,
  person.do_not_show_on_website AS hide_from_website,
  user_account.chemnet_token AS ro_chemnet_token,
  false AS create_new_token
FROM person
LEFT JOIN mm_person_room ON person.id = mm_person_room.person_id
LEFT JOIN room_hid ON room_hid.room_id = mm_person_room.room_id
LEFT JOIN mm_person_dept_telephone_number ON person.id = mm_person_dept_telephone_number.person_id
LEFT JOIN dept_telephone_number_hid ON dept_telephone_number_hid.dept_telephone_number_id = mm_person_dept_telephone_number.dept_telephone_number_id
LEFT JOIN mm_person_nationality ON person.id = mm_person_nationality.person_id
LEFT JOIN nationality_hid USING (nationality_id)
LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
LEFT JOIN user_account ON person.id = user_account.person_id
WHERE person.ban_from_self_service <> true;

ALTER TABLE personal_data_view
  OWNER TO dev;
GRANT SELECT, UPDATE ON TABLE personal_data_view TO selfservice;


CREATE OR REPLACE FUNCTION fn_personal_data_upd(personal_data_view)
  RETURNS bigint AS
$BODY$
       declare          v alias for $1;
 _crsid varchar;
       begin
 
       UPDATE person SET
 
       name_suffix=v.name_suffix_eg_frs,
       known_as=v.known_as,
       email_address=v.email_address, 
       hide_email=v.hide_email_from_dept_websites,
       cambridge_address=v.home_address, 
       cambridge_phone_number=v.home_phone_number, 
       emergency_contact=v.emergency_contact, 
       managed_mail_domain_optout=v.opt_out_of_chemistry_mail_domain,
       do_not_show_on_website=v.hide_from_website
       WHERE person.id = v.id;
       if v."create_new_token" then
         update user_account set chemnet_token=random_string_lower(16) where person_id=v.id;
         if not found then
           -- Can only add people with CRSIDs
           select crsid from person where person.id=v.id into _crsid;
           if found then
             insert into user_account (username, person_id, chemnet_token) values (_crsid, v.id, random_string_lower(16));
           end if;
         end if;
       end if;
       PERFORM fn_upd_many_many (v.id, v.mm_room, 'person', 'room');
       PERFORM fn_upd_many_many (v.id, v.mm_dept_telephone_number, 'person', 'dept_telephone_number');
 
       return v.id;
  end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_personal_data_upd(personal_data_view)
  OWNER TO dev;


CREATE OR REPLACE RULE personal_data_upd AS
    ON UPDATE TO personal_data_view DO INSTEAD  SELECT fn_personal_data_upd(new.*) AS id;


