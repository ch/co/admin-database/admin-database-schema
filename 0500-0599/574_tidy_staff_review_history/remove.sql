DROP VIEW hotwire3."10_View/People/_Staff_Review_History";

CREATE OR REPLACE VIEW hotwire3."10_View/People/_Staff_Review_History" AS 
 SELECT staff_review_meeting.id, person.id AS person_id, staff_review_meeting.date_of_meeting, person.next_review_due, staff_review_meeting.reviewer_id, staff_review_meeting.notes
   FROM staff_review_meeting
   JOIN person ON staff_review_meeting.person_id = person.id;

ALTER TABLE hotwire3."10_View/People/_Staff_Review_History"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO hr;
GRANT SELECT ON TABLE hotwire3."10_View/People/_Staff_Review_History" TO personnel_history_ro;

