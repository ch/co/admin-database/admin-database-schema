DROP VIEW hotwire3."10_View/Network/Switches/_Switchport_ro";
CREATE OR REPLACE VIEW hotwire3."10_View/Network/Switches/_Switchport_ro" AS 
 SELECT switchport.id, switchport.switch_id, switchport.name, socket_hid.socket_hid as socket, switchport.ifacename, switch_auth_hid.switch_auth_hid, speed_hid.speed_hid
   FROM switchport
   LEFT JOIN hotwire3.switch_auth_hid USING (switch_auth_id)
   LEFT JOIN hotwire3.speed_hid USING (speed_id)
   LEFT JOIN hotwire3.socket_hid USING (socket_id)
  ORDER BY 
CASE
    WHEN switchport.name::text ~ '^[A-Za-z]+'::text THEN 10 * ascii(substr(switchport.name::text, 1, 1)) + regexp_replace(switchport.name::text, '[A-Za-z]+'::text, ''::text)::integer
    ELSE switchport.name::integer
END ASC;

ALTER TABLE hotwire3."10_View/Network/Switches/_Switchport_ro"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/Network/Switches/_Switchport_ro" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Network/Switches/_Switchport_ro" TO cos;

