DROP VIEW apps.personal_data_selfservice_v6;
DROP FUNCTION apps_personal_data_v6_upd();
REVOKE SELECT (id,auto_banned_from_network,override_auto_ban) ON person FROM selfservice_updater;
REVOKE UPDATE (known_as,name_suffix,email_address,hide_email,cambridge_address,cambridge_phone_number,emergency_contact,managed_mail_domain_optout,do_not_show_on_website) ON person FROM selfservice_updater;
REVOKE SELECT,INSERT,DELETE ON mm_person_dept_telephone_number FROM selfservice_updater;
REVOKE SELECT,INSERT,DELETE ON mm_person_room FROM selfservice_updater;
REVOKE SELECT (person_id,username) ON user_account FROM selfservice_updater;
REVOKE UPDATE (chemnet_token) ON user_account FROM selfservice_updater;
REVOKE INSERT (person_id,chemnet_token,username) ON user_account FROM selfservice_updater;
