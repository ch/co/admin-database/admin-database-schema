DROP VIEW hotwire3."10_View/People/Researcher_Staff_Reviews";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Researcher_Staff_Reviews" AS 
 SELECT person.id, person.surname AS ro_surname, person.first_names AS ro_first_names, person.title_id AS ro_title_id, person.email_address AS ro_email_address, _latest_role.post_category_id AS ro_post_category_id, COALESCE(person.usual_reviewer_id, _latest_role.supervisor_id) AS ro_reviewer_id, COALESCE(usual_reviewer.email_address, supervisor.email_address) AS ro_reviewer_email_address, _latest_role.supervisor_id AS ro_supervisor_id, person.next_review_due, last_review.date_of_meeting AS ro_date_of_last_review, NULL::date AS record_a_new_review, COALESCE(person.usual_reviewer_id, _latest_role.supervisor_id) AS new_reviewer_id, NULL::text AS new_review_notes, NULL::bigint AS update_usual_reviewer_id, person.silence_staff_review_reminders as silence_reminders_for_this_reviewee, COALESCE(usual_reviewer.silence_staff_review_reminders, supervisor.silence_staff_review_reminders) AS silence_all_reminders_for_this_reviewer,person.staff_review_notes AS reminder_notes, 
        CASE
            WHEN person.next_review_due < 'now'::text::date THEN 'red'::text
            WHEN person.next_review_due < ('now'::text::date + '1 mon'::interval) THEN 'orange'::text
            ELSE NULL::text
        END AS _cssclass
   FROM person
   JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
   JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
   JOIN person supervisor ON _latest_role.supervisor_id = supervisor.id
   LEFT JOIN person usual_reviewer ON person.usual_reviewer_id = usual_reviewer.id
   LEFT JOIN ( SELECT staff_review_meeting.person_id, max(staff_review_meeting.date_of_meeting) AS date_of_meeting
   FROM staff_review_meeting
  WHERE staff_review_meeting.date_of_meeting IS NOT NULL
  GROUP BY staff_review_meeting.person_id) last_review ON last_review.person_id = person.id
  WHERE _physical_status.status_id::text = 'Current'::text AND (_latest_role.post_category_id = 'sc-7'::text OR _latest_role.post_category_id = 'sc-4'::text OR _latest_role.post_category_id = 'sc-10'::text OR _latest_role.post_category_id = 'sc-6'::text OR _latest_role.post_category_id = 'sc-5'::text) AND _latest_role.paid_by_university <> 'f'
  ORDER BY person.next_review_due;

ALTER TABLE hotwire3."10_View/People/Researcher_Staff_Reviews"
  OWNER TO dev;
GRANT ALL ON TABLE hotwire3."10_View/People/Researcher_Staff_Reviews" TO dev;
GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/People/Researcher_Staff_Reviews" TO hr;

DROP FUNCTION hotwire3.researcher_staff_reviews();

CREATE OR REPLACE FUNCTION hotwire3.researcher_staff_reviews()
  RETURNS trigger AS
$BODY$
BEGIN
    IF NEW.update_usual_reviewer_id IS NOT NULL 
    THEN
        UPDATE person SET usual_reviewer_id = NEW.update_usual_reviewer_id WHERE id = NEW.id;
    END IF;
    IF NEW.record_a_new_review IS NOT NULL AND NEW.new_reviewer_id IS NOT NULL
    THEN
        INSERT INTO staff_review_meeting ( date_of_meeting, reviewer_id, notes, person_id, last_updated ) VALUES ( NEW.record_a_new_review, NEW.new_reviewer_id, NEW.new_review_notes::varchar(500), NEW.id, current_timestamp );
        UPDATE person SET next_review_due = NEW.record_a_new_review + interval '1 year' WHERE id = NEW.id;
    END IF;
    IF NEW.next_review_due IS DISTINCT FROM OLD.next_review_due
    THEN
        UPDATE person SET next_review_due = NEW.next_review_due WHERE id = NEW.id;
    END IF;
    IF NEW.silence_reminders_for_this_reviewee IS DISTINCT FROM OLD.silence_reminders_for_this_reviewee
    THEN
        UPDATE person SET silence_staff_review_reminders = NEW.silence_reminders_for_this_reviewee WHERE id = OLD.id;
    END IF;
    IF NEW.silence_all_reminders_for_this_reviewer IS DISTINCT FROM OLD.silence_all_reminders_for_this_reviewer
    THEN
        UPDATE person SET silence_staff_review_reminders = NEW.silence_all_reminders_for_this_reviewer WHERE id = OLD.ro_reviewer_id;
    END IF;
    IF NEW.reminder_notes IS DISTINCT FROM OLD.reminder_notes
    THEN
        UPDATE person SET staff_review_notes = NEW.reminder_notes WHERE id = OLD.id;
    END IF;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hotwire3.researcher_staff_reviews()
  OWNER TO dev;

CREATE TRIGGER update_researcher_reviews
  INSTEAD OF UPDATE
  ON hotwire3."10_View/People/Researcher_Staff_Reviews"
  FOR EACH ROW
  EXECUTE PROCEDURE hotwire3.researcher_staff_reviews();


CREATE OR REPLACE VIEW apps.researcher_staff_review_reminders AS
SELECT
    person.id,
    person.surname,
    person.first_names,
    person_title.title_hid,
    coalesce((person_title.salutation_title_hid || ' '),'') || coalesce(person.known_as,person.first_names) || ' ' || person.surname AS reviewee,
    person.email_address AS reviewee_email_address,
    _latest_role.post_category_id as post_category_id,
    CASE
        WHEN person.usual_reviewer_id IS NOT NULL THEN
            COALESCE(reviewer_title.salutation_title_hid,COALESCE(usual_reviewer.known_as,usual_reviewer.first_names)) || ' ' || usual_reviewer.surname
        ELSE
            COALESCE(supervisor_title.salutation_title_hid,COALESCE(supervisor.known_as,supervisor.first_names)) || ' ' || supervisor.surname
    END AS reviewer,
    CASE
        WHEN person.usual_reviewer_id IS NOT NULL THEN
            usual_reviewer.email_address
        ELSE
            supervisor.email_address
    END AS reviewer_email_address,
    person.next_review_due,
    last_review.date_of_meeting AS date_of_last_review,
    person.silence_staff_review_reminders,
    COALESCE(usual_reviewer.silence_staff_review_reminders,supervisor.silence_staff_review_reminders) AS silence_staff_review_reminders_for_reviewer
FROM person
JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
JOIN person supervisor ON _latest_role.supervisor_id = supervisor.id
LEFT JOIN person usual_reviewer ON person.usual_reviewer_id = usual_reviewer.id
LEFT JOIN (
    SELECT
        person_id,
        max(date_of_meeting) AS date_of_meeting
    FROM staff_review_meeting
    GROUP BY person_id
) last_review ON last_review.person_id = person.id
LEFT JOIN title_hid person_title ON person.title_id = person_title.title_id
LEFT JOIN title_hid reviewer_title ON usual_reviewer.title_id = reviewer_title.title_id
LEFT JOIN title_hid supervisor_title ON supervisor.title_id = supervisor_title.title_id
WHERE
    _physical_status.status_id = 'Current'
AND
   (
       _latest_role.post_category_id = 'sc-7' -- Senior PDRA
       OR
       _latest_role.post_category_id = 'sc-4' -- PDRA
       OR
       _latest_role.post_category_id = 'sc-10' -- Principal Research Associate
       OR
       _latest_role.post_category_id = 'sc-6' -- Research Fellow
       OR
       _latest_role.post_category_id = 'sc-5' -- Research Assistant
   )
AND
    _latest_role.paid_by_university <> 'f'
;

ALTER VIEW apps.researcher_staff_review_reminders OWNER TO dev;
GRANT SELECT ON apps.researcher_staff_review_reminders TO staff_reviews_management;
