DROP TRIGGER propagate_chemnet_bans ON person;
DROP FUNCTION public.person_propagate_chemnet_bans();
REVOKE UPDATE ON user_account FROM useradder;
