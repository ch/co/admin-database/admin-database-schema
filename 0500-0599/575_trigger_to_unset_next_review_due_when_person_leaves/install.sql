CREATE FUNCTION unset_review_due_date_on_leaving() RETURNS TRIGGER AS
$$
DECLARE
    new_physical_status varchar(20);
    old_physical_status varchar(20);
BEGIN
    select fn_calculate_physical_status(NEW.arrival_date,NEW.leaving_date,NEW.left_but_no_leaving_date_given) into new_physical_status;
    select fn_calculate_physical_status(OLD.arrival_date,OLD.leaving_date,OLD.left_but_no_leaving_date_given) into old_physical_status;
    if new_physical_status = 'Past' and new_physical_status is distinct from old_physical_status
    then
        new.next_review_due := NULL::date;
    end if;
    RETURN NEW;
    
END;
$$ LANGUAGE PLPGSQL;

ALTER FUNCTION unset_review_due_date_on_leaving() OWNER TO dev;

CREATE TRIGGER unset_review_due_date BEFORE UPDATE OF arrival_date, leaving_date, left_but_no_leaving_date_given ON PERSON FOR EACH ROW EXECUTE PROCEDURE unset_review_due_date_on_leaving();
