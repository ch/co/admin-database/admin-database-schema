CREATE OR REPLACE VIEW apps.system_image_id_hostname_os_class AS 
 SELECT system_image.id, ip_address.hostname, os_class_hid.os_class_hid, system_image.ad_logon_seen
   FROM system_image
   JOIN operating_system ON system_image.operating_system_id = operating_system.id
   LEFT JOIN mm_system_image_ip_address ON system_image.id = mm_system_image_ip_address.system_image_id
   LEFT JOIN ip_address ON mm_system_image_ip_address.ip_address_id = ip_address.id
   LEFT JOIN os_class_hid USING (os_class_id);

ALTER TABLE apps.system_image_id_hostname_os_class OWNER TO dev;
GRANT SELECT, UPDATE ON TABLE apps.system_image_id_hostname_os_class TO ad_accounts;
COMMENT ON VIEW apps.system_image_id_hostname_os_class
  IS 'Used by update-windows-managed-machines-in-db script in chemaccmgmt';

CREATE OR REPLACE RULE ad_last_logon_upd AS
    ON UPDATE TO apps.system_image_id_hostname_os_class DO INSTEAD  UPDATE system_image SET ad_logon_seen = new.ad_logon_seen, was_managed_machine_on = new.ad_logon_seen
  WHERE system_image.id = old.id;


CREATE OR REPLACE VIEW apps.ansible_ran_date AS 
 SELECT si.id, ip.hostname, ip.ip, si.ansible_ran_date, si.ansible_repository
   FROM system_image si
   JOIN mm_system_image_ip_address mm ON si.id = mm.system_image_id
   JOIN ip_address ip ON mm.ip_address_id = ip.id
   JOIN subnet ON ip.subnet_id = subnet.id
  WHERE subnet.monitor_with_hobbit = true;

ALTER TABLE apps.ansible_ran_date OWNER TO dev;
GRANT ALL ON TABLE apps.ansible_ran_date TO dev;
GRANT SELECT ON TABLE apps.ansible_ran_date TO cos;
GRANT SELECT, UPDATE ON TABLE apps.ansible_ran_date TO ansible_trigger;

CREATE OR REPLACE RULE apps_ansible_ran_date_upd AS
    ON UPDATE TO apps.ansible_ran_date DO INSTEAD  UPDATE system_image SET ansible_ran_date = 'now'::text::date, was_managed_machine_on = 'now'::text::date, ansible_repository = new.ansible_repository
  WHERE system_image.id = old.id;


