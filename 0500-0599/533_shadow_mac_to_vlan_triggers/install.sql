DROP TRIGGER system_image_shadow_trig ON system_image;

DROP TRIGGER mm_system_image_ip_address_shadow_trig ON mm_system_image_ip_address;

DROP TRIGGER subnet_shadow_trig ON subnet;

DROP FUNCTION shadow_mac_to_vlan_update_from_system_image_change();

DROP FUNCTION shadow_mac_to_vlan_update_from_subnet_change();

DROP FUNCTION shadow_mac_to_vlan_update_from_mm_system_image_ip_address();

