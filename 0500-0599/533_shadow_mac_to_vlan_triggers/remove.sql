CREATE OR REPLACE FUNCTION shadow_mac_to_vlan_update_from_subnet_change()
  RETURNS trigger AS
$BODY$begin
if (TG_OP='DELETE') then
 -- Removing a subnet? Remove any MAC addresses with IP addresses in that subnet
 delete from shadow_mac_to_vlan where mac in (
    select wired_mac_1 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_2 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_3 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_4
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wireless_mac 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
 );  
elsif (TG_OP='INSERT') then
 -- Adding a subnet; will just have created a bunch of IP addresses but they can't have system images yet
 -- NO OP
else -- (TG_OP='UPDATE')
  -- Updating a subnet: calculate 
  if (old.vlan_id!=new.vlan_id) then
    update shadow_mac_to_vlan set vid=(select vid from vlan where vlan.id=new.vlan_id limit 1) 
     where mac in (
    select wired_mac_1 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_2 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_3 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wired_mac_4
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
   union
    select wireless_mac 
      from system_image 
inner join mm_system_image_ip_address on system_image_id=system_image.id 
inner join ip_address on ip_address_id=ip_address.id 
     where ip_address.subnet_id=old.id
);
  end if;
end if;
return new;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION shadow_mac_to_vlan_update_from_subnet_change()
  OWNER TO dev;
COMMENT ON FUNCTION shadow_mac_to_vlan_update_from_subnet_change() IS 'Updates the shadow_mac_to_vlan table when the subnet table changes';

CREATE OR REPLACE FUNCTION shadow_mac_to_vlan_update_from_system_image_change()
  RETURNS trigger AS
$BODY$
begin

if (TG_OP='DELETE') then
 -- Remove a system image? Nuke its MAC addresses
 delete from shadow_mac_to_vlan where mac in (old.wired_mac_1, old.wired_mac_2, old.wired_mac_4, old.wireless_mac);
elsif (TG_OP='INSERT') then
 -- Adding a system image? Find appropriate MAC addresses and VLANs
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_1, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_1 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_2, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_2 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_3, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_3 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wired_mac_4, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_4 is not null;
 insert into shadow_mac_to_vlan (mac,vid) select new.wireless_mac, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wireless_mac is not null;
else --TG_OP='UPDATE'
  -- We might change MAC address:
 if (new.wired_mac_1 !=  old.wired_mac_1)  then update shadow_mac_to_vlan set mac=new.wired_mac_1  where mac=old.wired_mac_1; end if;
 if (new.wired_mac_2 !=  old.wired_mac_2)  then update shadow_mac_to_vlan set mac=new.wired_mac_2  where mac=old.wired_mac_2; end if;
 if (new.wired_mac_3 !=  old.wired_mac_3)  then update shadow_mac_to_vlan set mac=new.wired_mac_3  where mac=old.wired_mac_3; end if;
 if (new.wired_mac_4 !=  old.wired_mac_4)  then update shadow_mac_to_vlan set mac=new.wired_mac_4  where mac=old.wired_mac_4; end if;
 if (new.wireless_mac != old.wireless_mac) then update shadow_mac_to_vlan set mac=new.wireless_mac where mac=old.wireless_mac; end if;
 if (old.wired_mac_1 is null and new.wired_mac_1 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_1, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_1 is not null limit 1;
 end if;
 if (old.wired_mac_2 is null and new.wired_mac_2 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_2, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_2 is not null limit 1;
 end if;
 if (old.wired_mac_3 is null and new.wired_mac_3 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_3, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_3 is not null limit 1;
 end if;
 if (old.wired_mac_4 is null and new.wired_mac_4 is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wired_mac_4, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wired_mac_4 is not null limit 1;
 end if;
 if (old.wireless_mac is null and new.wireless_mac is not null) then 
  insert into shadow_mac_to_vlan (mac,vid) select distinct new.wireless_mac, vid from vlan
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id
  inner join mm_system_image_ip_address on ip_address.id=ip_address_id
  where system_image_id=new.id and new.wireless_mac is not null limit 1;
 end if;
 if (new.wireless_mac is null and old.wireless_mac is not null) then
  delete from shadow_mac_to_vlan where mac=old.wireless_mac;
 end if;
 if (new.wired_mac_1 is null and old.wired_mac_1 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_1;
 end if;
 if (new.wired_mac_2 is null and old.wired_mac_2 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_2;
 end if;
 if (new.wired_mac_3 is null and old.wired_mac_3 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_3;
 end if;
 if (new.wired_mac_4 is null and old.wired_mac_4 is not null) then
  delete from shadow_mac_to_vlan where mac=old.wired_mac_4;
 end if;
 

 
end if;
return new;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION shadow_mac_to_vlan_update_from_system_image_change() SET search_path=public, pg_temp;

ALTER FUNCTION shadow_mac_to_vlan_update_from_system_image_change()
  OWNER TO shadow_mac_to_vlan_maintenance;
COMMENT ON FUNCTION shadow_mac_to_vlan_update_from_system_image_change() IS 'Updates the shadow_mac_to_vlan table when the system_image table changes';

CREATE OR REPLACE FUNCTION shadow_mac_to_vlan_update_from_mm_system_image_ip_address()
  RETURNS trigger AS
$BODY$declare
 v_newvid integer;
 v_oldvid integer;
 v_oldmacs macaddr[];
 v_newmacs macaddr[];
begin
if (TG_OP='DELETE') then
 -- Remove MAC addresses from the system image with VLANs from the ipaddress
 delete from shadow_mac_to_vlan where mac in (
  select wired_mac_1 from system_image where id=old.system_image_id
    union
  select wired_mac_2 from system_image where id=old.system_image_id
    union
  select wired_mac_3 from system_image where id=old.system_image_id
    union
  select wired_mac_4 from system_image where id=old.system_image_id
    union
  select wireless_mac from system_image where id=old.system_image_id
 ) and vid in (
    select vid 
      from vlan 
inner join subnet on vlan.id=vlan_id 
inner join ip_address on subnet.id=subnet_id 
     where ip_address.id=old.ip_address_id and ignore_vlan
 );
   
elsif (TG_OP='INSERT') then
 -- Adding a new entry
 perform * from shadow_mac_to_vlan smv inner join system_image si on si.wired_mac_1=smv.mac where si.id=new.system_image_id;
 if not found then 
  insert into shadow_mac_to_vlan (mac, vid) 
       select wired_mac_1, vid 
         from system_image, 
              vlan 
   inner join subnet on vlan_id=vlan.id 
   inner join ip_address on subnet_id=subnet.id 
        where system_image.id = new.system_image_id
          and ip_address.id = new.ip_address_id and ignore_vlan;
 end if;
 perform * from shadow_mac_to_vlan smv inner join system_image si on si.wired_mac_1=smv.mac where si.id=new.system_image_id;
 if not found then 
 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_2, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan;
 end if;
 perform * from shadow_mac_to_vlan smv inner join system_image si on si.wired_mac_1=smv.mac where si.id=new.system_image_id;
 if not found then 

 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_3, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan;
 end if;
 perform * from shadow_mac_to_vlan smv inner join system_image si on si.wired_mac_1=smv.mac where si.id=new.system_image_id;
 if not found then 

 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_4, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan;
 end if;
  perform * from shadow_mac_to_vlan smv inner join system_image si on si.wired_mac_1=smv.mac where si.id=new.system_image_id;
 if not found then 

 insert into shadow_mac_to_vlan (mac, vid) 
      select wireless_mac, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan;
 end if;

else --(TG_OP='UPDATE') 
 -- Inefficient, but treat as DELETE then INSERT
 delete from shadow_mac_to_vlan where mac in (
  select wired_mac_1 from system_image where id=old.system_image_id
    union
  select wired_mac_2 from system_image where id=old.system_image_id
    union
  select wired_mac_3 from system_image where id=old.system_image_id
    union
  select wired_mac_4 from system_image where id=old.system_image_id
    union
  select wireless_mac from system_image where id=old.system_image_id
 ) ;
  -- Adding a new entry
 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_1, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan and wired_mac_1 is not null;

 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_2, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan and wired_mac_2 is not null;

 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_3, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan and wired_mac_3 is not null;

 insert into shadow_mac_to_vlan (mac, vid) 
      select wired_mac_4, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan and wired_mac_4 is not null;

 insert into shadow_mac_to_vlan (mac, vid) 
      select wireless_mac, vid 
        from system_image, 
             vlan 
  inner join subnet on vlan_id=vlan.id 
  inner join ip_address on subnet_id=subnet.id 
       where system_image.id = new.system_image_id
         and ip_address.id = new.ip_address_id and ignore_vlan and wireless_mac is not null;
end if;
return new;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION shadow_mac_to_vlan_update_from_mm_system_image_ip_address()
  OWNER TO dev;



CREATE TRIGGER subnet_shadow_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON subnet
  FOR EACH ROW
  EXECUTE PROCEDURE shadow_mac_to_vlan_update_from_subnet_change();

CREATE TRIGGER system_image_shadow_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON system_image
  FOR EACH ROW
  EXECUTE PROCEDURE shadow_mac_to_vlan_update_from_system_image_change();

CREATE TRIGGER mm_system_image_ip_address_shadow_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON mm_system_image_ip_address
  FOR EACH ROW
  EXECUTE PROCEDURE shadow_mac_to_vlan_update_from_mm_system_image_ip_address();
