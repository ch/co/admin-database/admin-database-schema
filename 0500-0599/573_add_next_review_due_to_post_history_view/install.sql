DROP VIEW hotwire3."10_View/Roles/Post_History";
CREATE OR REPLACE VIEW hotwire3. "10_View/Roles/Post_History" AS
SELECT
    a.id,
    a.staff_category_id,
    a.person_id,
    a.ro_role_status,
    a.supervisor_id,
    a.mentor_id,
    a.external_mentor,
    a.start_date_for_continuous_employment_purposes,
    a.start_date,
    a.intended_end_date,
    a.end_date,
    a.funding_end_date,
    a.force_role_status_to_past,
    a.staff_review_due,
    a.chem,
    a.paid_through_payroll,
    a.hours_worked,
    a.percentage_of_fulltime_hours,
    a.research_grant_number,
    a.sponsor,
    a.probation_period,
    a.date_of_first_probation_meeting,
    a.date_of_second_probation_meeting,
    a.date_of_third_probation_meeting,
    a.date_of_fourth_probation_meeting,
    a.first_probation_meeting_comments,
    a.second_probation_meeting_comments,
    a.third_probation_meeting_comments,
    a.fourth_probation_meeting_comments,
    a.probation_outcome,
    a.date_probation_letters_sent,
    a.job_title,
    a._person
FROM (
    SELECT
        post_history.id,
        post_history.staff_category_id,
        post_history.person_id,
        _all_roles.status AS ro_role_status,
        post_history.supervisor_id,
        post_history.mentor_id,
        post_history.external_mentor,
        person.continuous_employment_start_date AS start_date_for_continuous_employment_purposes,
        post_history.start_date,
        post_history.intended_end_date,
        post_history.end_date,
        post_history.funding_end_date,
        person.next_review_due AS staff_review_due,
        post_history.chem,
        post_history.paid_by_university AS paid_through_payroll,
        post_history.hours_worked,
        post_history.percentage_of_fulltime_hours,
        post_history.probation_period::text AS probation_period,
        post_history.date_of_first_probation_meeting,
        post_history.date_of_second_probation_meeting,
        post_history.date_of_third_probation_meeting,
        post_history.date_of_fourth_probation_meeting,
        post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments,
        post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments,
        post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments,
        post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments,
        post_history.probation_outcome::text AS probation_outcome,
        post_history.date_probation_letters_sent,
        post_history.research_grant_number::text AS research_grant_number,
        post_history.filemaker_funding::text AS sponsor,
        post_history.job_title,
        post_history.force_role_status_to_past,
        person_hid.person_hid AS _person
    FROM
        post_history
    LEFT JOIN _all_roles_v13 _all_roles ON post_history.id = _all_roles.role_id
        AND _all_roles.role_tablename = 'post_history'::text
    JOIN person ON post_history.person_id = person.id
    JOIN person_hid ON post_history.person_id = person_hid.person_id) a
ORDER BY
    a._person,
    a.start_date DESC;

ALTER TABLE hotwire3. "10_View/Roles/Post_History" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/Roles/Post_History" TO dev;

GRANT SELECT ON TABLE hotwire3. "10_View/Roles/Post_History" TO mgmt_ro;

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire3. "10_View/Roles/Post_History" TO hr;

CREATE OR REPLACE RULE hotwire3_view_post_history_del AS ON DELETE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    DELETE
FROM
    post_history
WHERE
    post_history.id = old.id;

CREATE OR REPLACE RULE hotwire3_view_post_history_update_rules AS ON UPDATE TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person SET
            continuous_employment_start_date = new.start_date_for_continuous_employment_purposes,
            next_review_due = new.staff_review_due
	WHERE
	    person.id = new.person_id;

	UPDATE post_history SET
	    staff_category_id = new.staff_category_id,
	    supervisor_id = new.supervisor_id,
	    mentor_id = new.mentor_id,
	    external_mentor = new.external_mentor,
	    start_date = new.start_date,
	    intended_end_date = new.intended_end_date,
	    end_date = new.end_date,
	    funding_end_date = new.funding_end_date,
	    chem = new.chem,
	    paid_by_university = new.paid_through_payroll,
	    hours_worked = new.hours_worked,
	    percentage_of_fulltime_hours = new.percentage_of_fulltime_hours,
	    probation_period = new.probation_period::character varying(80),
	    date_of_first_probation_meeting = new.date_of_first_probation_meeting,
	    date_of_second_probation_meeting = new.date_of_second_probation_meeting,
	    date_of_third_probation_meeting = new.date_of_third_probation_meeting,
	    date_of_fourth_probation_meeting = new.date_of_fourth_probation_meeting,
	    first_probation_meeting_comments = new.first_probation_meeting_comments::character varying(500),
	    second_probation_meeting_comments = new.second_probation_meeting_comments::character varying(500),
	    third_probation_meeting_comments = new.third_probation_meeting_comments::character varying(500),
	    fourth_probation_meeting_comments = new.fourth_probation_meeting_comments::character varying(500),
	    probation_outcome = new.probation_outcome::character varying(200),
	    date_probation_letters_sent = new.date_probation_letters_sent,
	    research_grant_number = new.research_grant_number::character varying(500),
	    filemaker_funding = new.sponsor::character varying(500),
	    job_title = new.job_title,
	    force_role_status_to_past = new.force_role_status_to_past
	WHERE
	    post_history.id = old.id;

);

CREATE OR REPLACE RULE post_history_ins AS ON INSERT TO hotwire3. "10_View/Roles/Post_History"
    DO INSTEAD
    (
        UPDATE person
        SET continuous_employment_start_date = new.start_date_for_continuous_employment_purposes,
            next_review_due = new.staff_review_due
    WHERE
        person.id = new.person_id;

	INSERT INTO post_history (
	    staff_category_id,
            person_id,
            supervisor_id,
            mentor_id,
            external_mentor,
            start_date,
            intended_end_date,
            end_date,
            funding_end_date,
            chem,
            paid_by_university,
            hours_worked,
            percentage_of_fulltime_hours,
            probation_period,
            date_of_first_probation_meeting,
            date_of_second_probation_meeting,
            date_of_third_probation_meeting,
            date_of_fourth_probation_meeting,
            first_probation_meeting_comments,
            second_probation_meeting_comments,
            third_probation_meeting_comments,
            fourth_probation_meeting_comments,
            probation_outcome,
            date_probation_letters_sent,
            research_grant_number,
            filemaker_funding,
            job_title,
            force_role_status_to_past
        ) VALUES (
            new.staff_category_id,
            new.person_id,
            new.supervisor_id,
            new.mentor_id,
            new.external_mentor,
            new.start_date,
            new.intended_end_date,
            new.end_date,
            new.funding_end_date,
            new.chem,
            new.paid_through_payroll,
            new.hours_worked,
            new.percentage_of_fulltime_hours,
            new.probation_period::character varying(80),
            new.date_of_first_probation_meeting,
            new.date_of_second_probation_meeting,
            new.date_of_third_probation_meeting,
            new.date_of_fourth_probation_meeting,
            new.first_probation_meeting_comments::character varying(500),
            new.second_probation_meeting_comments::character varying(500),
            new.third_probation_meeting_comments::character varying(500),
            new.fourth_probation_meeting_comments::character varying(500),
            new.probation_outcome::character varying(200),
            new.date_probation_letters_sent,
            new.research_grant_number::character varying(500),
            new.sponsor::character varying(500),
            new.job_title,
            new.force_role_status_to_past
        )
        RETURNING
            post_history.id,
            post_history.staff_category_id,
            post_history.person_id,
            NULL::character varying(10) AS "varchar",
            post_history.supervisor_id,
            post_history.mentor_id,
            post_history.external_mentor,
            NULL::date AS date,
            post_history.start_date,
            post_history.intended_end_date,
            post_history.end_date,
            post_history.funding_end_date,
            post_history.force_role_status_to_past,
            NULL::date,
            post_history.chem,
            post_history.paid_by_university,
            post_history.hours_worked,
            post_history.percentage_of_fulltime_hours,
            post_history.research_grant_number::text AS research_grant_number,
            post_history.filemaker_funding::text AS filemaker_funding,
            post_history.probation_period::text AS probation_period,
            post_history.date_of_first_probation_meeting,
            post_history.date_of_second_probation_meeting,
            post_history.date_of_third_probation_meeting,
            post_history.date_of_fourth_probation_meeting,
            post_history.first_probation_meeting_comments::text AS first_probation_meeting_comments,
            post_history.second_probation_meeting_comments::text AS second_probation_meeting_comments,
            post_history.third_probation_meeting_comments::text AS third_probation_meeting_comments,
            post_history.fourth_probation_meeting_comments::text AS fourth_probation_meeting_comments,
            post_history.probation_outcome::text AS probation_outcome,
            post_history.date_probation_letters_sent,
            post_history.job_title,
            NULL::text AS text;
);


DROP VIEW hotwire3."10_View/People/Personnel_History";
CREATE OR REPLACE VIEW hotwire3. "10_View/People/Personnel_History" AS WITH personnel_history AS (
    SELECT
        person.id,
        person_hid.person_hid AS ro_person,
        person.surname,
        person.first_names,
        person.title_id,
        date_part('year'::text,
            age(person.date_of_birth::timestamp WITH time zone
)
) AS ro_age,
        _latest_role.post_category_id AS ro_post_category_id,
        person.continuous_employment_start_date,
        _latest_employment.start_date AS ro_latest_employment_start_date,
        COALESCE(
            _latest_employment.end_date,
            _latest_employment.intended_end_date
) AS ro_employment_end_date,
        age(
            COALESCE(
                _latest_employment.end_date,
                'now'::text::date
)::timestamp WITH time zone,
            person.continuous_employment_start_date::timestamp WITH time zone
)::text AS ro_length_of_continuous_service,
        age(
            COALESCE(
                _latest_employment.end_date,
                'now'::text::date
)::timestamp WITH time zone,
            _latest_employment.start_date::timestamp WITH time zone
)::text AS ro_length_of_service_in_current_contract,
        age(
            _latest_employment.end_date::timestamp WITH time zone,
            person.continuous_employment_start_date::timestamp WITH time zone
) AS ro_final_length_of_continuous_service,
        age(
            _latest_employment.end_date::timestamp WITH time zone,
            _latest_employment.start_date::timestamp WITH time zone
) AS ro_final_length_of_service_in_last_contract,
        _latest_role.supervisor_id AS ro_supervisor_id,
        person.other_information,
        person.notes,
        emplids.emplid_number AS ro_emplid_number,
        person.next_review_due AS staff_review_due,
        CASE WHEN _physical_status.status_id::text = 'Past'::text THEN
            'orange'::text
        ELSE
            NULL::text
        END AS _cssclass,
        _to_hwsubviewb (
            '10_View/People/_Staff_Review_History'::character varying,
            'person_id'::character varying,
            '10_View/People/Staff_Reviews'::character varying,
            NULL::character varying,
            NULL::character varying
) AS staff_review_history_subview,
        _to_hwsubviewb (
            '10_View/Roles/_Cambridge_History_V2'::character varying,
            'person_id'::character varying,
            '_target_viewname'::character varying,
            '_role_xid'::character varying,
            NULL::character varying
) AS cambridge_history_subview
    FROM
        person
    LEFT JOIN _latest_role_v12 _latest_role ON person.id = _latest_role.person_id
    LEFT JOIN _latest_employment ON person.id = _latest_employment.person_id
    LEFT JOIN person_hid ON person_hid.person_id = person.id
    LEFT JOIN _physical_status_v3 _physical_status ON _physical_status.person_id = person.id
    LEFT JOIN (
        SELECT
            all_emplids.person_id,
            string_agg(
                all_emplids.emplid_number::text,
                '/'::text
)::character varying AS emplid_number
        FROM (
            SELECT DISTINCT
                postgraduate_studentship.person_id,
                postgraduate_studentship.emplid_number
            FROM
                postgraduate_studentship
            WHERE
                postgraduate_studentship.emplid_number IS NOT NULL
) all_emplids
        GROUP BY
            all_emplids.person_id
) emplids ON person.id = emplids.person_id
)
SELECT
    personnel_history.id,
    personnel_history.ro_person,
    personnel_history.surname,
    personnel_history.first_names,
    personnel_history.title_id,
    personnel_history.ro_age,
    personnel_history.ro_post_category_id,
    personnel_history.continuous_employment_start_date,
    personnel_history.ro_latest_employment_start_date,
    personnel_history.ro_employment_end_date,
    personnel_history.ro_length_of_continuous_service,
    personnel_history.ro_length_of_service_in_current_contract,
    personnel_history.ro_final_length_of_continuous_service,
    personnel_history.ro_final_length_of_service_in_last_contract,
    personnel_history.ro_supervisor_id,
    personnel_history.other_information,
    personnel_history.notes,
    personnel_history.ro_emplid_number,
    personnel_history.staff_review_due,
    personnel_history._cssclass,
    personnel_history.staff_review_history_subview,
    personnel_history.cambridge_history_subview
FROM
    personnel_history
ORDER BY
    personnel_history.surname,
    personnel_history.first_names;

ALTER TABLE hotwire3. "10_View/People/Personnel_History" OWNER TO dev;

GRANT ALL ON TABLE hotwire3. "10_View/People/Personnel_History" TO dev;

GRANT SELECT ON TABLE hotwire3. "10_View/People/Personnel_History" TO mgmt_ro;

GRANT SELECT, UPDATE ON TABLE hotwire3. "10_View/People/Personnel_History" TO hr;

GRANT SELECT ON TABLE hotwire3. "10_View/People/Personnel_History" TO personnel_history_ro;

GRANT SELECT ON TABLE hotwire3. "10_View/People/Personnel_History" TO accounts;

CREATE OR REPLACE RULE hotwire3_view_personnel_history_v2_upd AS ON UPDATE TO hotwire3. "10_View/People/Personnel_History"
    DO INSTEAD
    UPDATE person
    SET surname = new.surname,
    first_names = new.first_names,
    title_id = new.title_id,
    continuous_employment_start_date = new.continuous_employment_start_date,
    next_review_due = new.staff_review_due,
    other_information = new.other_information,
    notes = new.notes
WHERE
    person.id = old.id;


