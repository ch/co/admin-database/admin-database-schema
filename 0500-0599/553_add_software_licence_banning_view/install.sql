CREATE VIEW hotwire3."10_View/Groups/Software_Licence_Banning" AS
WITH q AS (
  SELECT
    research_group.id,
    research_group.name AS ro_name,
    research_group.head_of_group_id AS ro_head_of_group_id,
    CASE
      WHEN latest_software_declaration.declaration_date IS NULL THEN false
      WHEN (latest_software_declaration.declaration_date + '1 year'::interval) > 'now'::text::date THEN true
      ELSE false
    END AS ro_declaration_made,
    latest_software_declaration.declaration_date AS ro_date_of_last_declaration,
    latest_software_declaration.username_of_signer AS ro_declared_by,
    banned_for_lack_of_software_compliance
  FROM research_group
  LEFT JOIN ( 
    SELECT
      group_software_licence_declarations.research_group_id,
      max(group_software_licence_declarations.declaration_date) AS declaration_date
    FROM group_software_licence_declarations
    GROUP BY group_software_licence_declarations.research_group_id
  ) latest_software_declaration_date ON latest_software_declaration_date.research_group_id = research_group.id
    LEFT JOIN group_software_licence_declarations latest_software_declaration ON latest_software_declaration_date.declaration_date = latest_software_declaration.declaration_date AND latest_software_declaration_date.research_group_id = latest_software_declaration.research_group_id
    JOIN person head_of_group ON research_group.head_of_group_id = head_of_group.id
    JOIN _physical_status_v3 ps ON head_of_group.id = ps.person_id
    LEFT JOIN ( 
    SELECT members.head_of_group_id, count(members.person_id) AS members
    FROM (
      SELECT
         _latest_role.supervisor_id AS head_of_group_id,
         _latest_role.person_id
      FROM cache._latest_role
      JOIN _physical_status_v3 ps USING (person_id)
      WHERE ps.status_id::text = 'Current'::text
      UNION 
      SELECT 
        _latest_role.co_supervisor_id AS head_of_group_id,
        _latest_role.person_id
      FROM cache._latest_role
      JOIN _physical_status_v3 ps USING (person_id)
      WHERE ps.status_id::text = 'Current'::text) members
      GROUP BY members.head_of_group_id
    ) group_members ON group_members.head_of_group_id = head_of_group.id
    WHERE (ps.status_id::text = 'Current'::text OR group_members.members > 0) AND research_group.ignore_for_software_compliance_purposes IS NOT TRUE
)
SELECT 
  q.id,
  q.ro_name,
  q.ro_head_of_group_id,
  q.ro_declaration_made,
  q.ro_date_of_last_declaration,
  q.ro_declared_by,
  q.banned_for_lack_of_software_compliance
FROM q
ORDER BY q.ro_name;

ALTER VIEW hotwire3."10_View/Groups/Software_Licence_Banning" OWNER TO dev;

-- should be IT manager
GRANT SELECT,UPDATE ON hotwire3."10_View/Groups/Software_Licence_Banning" TO tkd25;

CREATE RULE update_bans AS ON UPDATE TO hotwire3."10_View/Groups/Software_Licence_Banning" DO INSTEAD UPDATE research_group SET banned_for_lack_of_software_compliance = NEW.banned_for_lack_of_software_compliance WHERE id = OLD.id;

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Groups/Software_Licence_Banning', 'research_group');
