CREATE OR REPLACE FUNCTION public.person_propagate_chemnet_bans() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'UPDATE')
    THEN
        -- Only do any work if one or both of the two flags we're interested in have changed
        IF (
                (NEW.auto_banned_from_network = OLD.auto_banned_from_network)
            AND
                (NEW.override_auto_ban = OLD.override_auto_ban)
           )
        THEN
            RETURN NEW;
        END IF;
    END IF;

    IF NEW.auto_banned_from_network = 'f' OR NEW.auto_banned_from_network IS NULL
    THEN
        UPDATE user_account SET owner_banned = 'f' WHERE person_id = NEW.id;
    ELSE
        -- auto_banned must be true
        UPDATE user_account 
        SET owner_banned = NOT(COALESCE(NEW.override_auto_ban,'f'::boolean))
        WHERE person_id = NEW.id;
    END IF;
    RETURN NEW;
END;
$$ 
LANGUAGE plpgsql 
SECURITY DEFINER
SET search_path = public, pg_temp;

GRANT CREATE ON SCHEMA public TO useradder;
ALTER FUNCTION person_propagate_chemnet_bans() OWNER TO useradder;
REVOKE CREATE ON SCHEMA public FROM useradder;

REVOKE EXECUTE ON FUNCTION person_propagate_chemnet_bans() FROM dev,cos,hr,student_management,photography,reception_cover,reception;
GRANT EXECUTE ON FUNCTION person_propagate_chemnet_bans() TO PUBLIC;

