CREATE OR REPLACE VIEW hotwire."10_View/Switch_With_Rooms" AS 
 SELECT switch.id, hardware.name, switch.switchstack_id, hardware.id AS _hardware_id, system_image.id AS _system_image_id, hardware.manufacturer, hardware.model, hardware.hardware_type_id, system_image.wired_mac_1, ARRAY( SELECT mm_system_image_ip_address.ip_address_id
           FROM mm_system_image_ip_address
          WHERE mm_system_image_ip_address.system_image_id = system_image.id) AS ip_address_id, hardware.serial_number, hardware.asset_tag, hardware.date_purchased, hardware.date_configured, hardware.warranty_end_date, hardware.date_decommissioned, hardware.warranty_details, hardware.room_id AS physical_room_id, hardware.owner_id, system_image.research_group_id, hardware.comments, ARRAY( SELECT mm_switch_serves_room.room_id
           FROM mm_switch_serves_room
          WHERE mm_switch_serves_room.switch_id = switch.id) AS room_id
   FROM switch
   JOIN hardware ON switch.hardware_id = hardware.id
   JOIN system_image ON system_image.hardware_id = hardware.id;

ALTER TABLE hotwire."10_View/Switch_With_Rooms"
  OWNER TO cen1001;
GRANT ALL ON TABLE hotwire."10_View/Switch_With_Rooms" TO cen1001;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE hotwire."10_View/Switch_With_Rooms" TO old_cos;
GRANT ALL ON TABLE hotwire."10_View/Switch_With_Rooms" TO cos;

-- Rule: switch_del ON hotwire."10_View/Switch_With_Rooms"

-- DROP RULE switch_del ON hotwire."10_View/Switch_With_Rooms";

CREATE OR REPLACE RULE switch_del AS
    ON DELETE TO hotwire."10_View/Switch_With_Rooms" DO INSTEAD  DELETE FROM switch
  WHERE switch.id = old.id;

-- Rule: switch_upd ON hotwire."10_View/Switch_With_Rooms"

-- DROP RULE switch_upd ON hotwire."10_View/Switch_With_Rooms";

CREATE OR REPLACE RULE switch_upd AS
    ON UPDATE TO hotwire."10_View/Switch_With_Rooms" DO INSTEAD ( UPDATE hardware SET name = new.name, manufacturer = new.manufacturer, model = new.model, hardware_type_id = new.hardware_type_id, serial_number = new.serial_number, asset_tag = new.asset_tag, date_purchased = new.date_purchased, date_configured = new.date_configured, warranty_end_date = new.warranty_end_date, date_decommissioned = new.date_decommissioned, warranty_details = new.warranty_details, room_id = new.physical_room_id, owner_id = new.owner_id, comments = new.comments
  WHERE hardware.id = old._hardware_id;
 UPDATE system_image SET research_group_id = new.research_group_id, wired_mac_1 = new.wired_mac_1
  WHERE system_image.id = old._system_image_id;
 UPDATE switch SET switchstack_id = new.switchstack_id
  WHERE switch.id = old.id;
);


