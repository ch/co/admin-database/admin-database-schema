CREATE TABLE selfservice.hw_preference_type_hid (
    hw_preference_type_id bigserial NOT NULL,
    hw_preference_type_hid character varying
);
ALTER TABLE selfservice.hw_preference_type_hid OWNER TO dev;

INSERT INTO selfservice.hw_preference_type_hid VALUES (1, 'boolean');
INSERT INTO selfservice.hw_preference_type_hid VALUES (2, 'integer');
INSERT INTO selfservice.hw_preference_type_hid VALUES (3, 'string');

ALTER TABLE ONLY selfservice.hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);

GRANT ALL ON TABLE selfservice.hw_preference_type_hid TO postgres;
GRANT ALL ON TABLE selfservice.hw_preference_type_hid TO dev;

SELECT setval('selfservice.hw_preference_type_hid_hw_preference_type_id_seq',3);

CREATE TABLE selfservice."hw_Preferences"
( 
  id serial NOT NULL ,
  hw_preference_name character varying,
  hw_preference_type_id integer,
  hw_preference_const varchar,
  CONSTRAINT "Preferences_pkey" PRIMARY KEY (id),
  CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id)
      REFERENCES selfservice.hw_preference_type_hid (hw_preference_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

)
WITH (
  OIDS=FALSE
);
ALTER TABLE selfservice."hw_Preferences" OWNER TO dev;
GRANT ALL ON TABLE selfservice."hw_Preferences" TO postgres;
GRANT ALL ON TABLE selfservice."hw_Preferences" TO dev;

INSERT INTO selfservice."hw_Preferences" VALUES (1, 'Floating table headers', 1, 'FLOATHEAD');
INSERT INTO selfservice."hw_Preferences" VALUES (2, 'Jump on a single search result', 1, 'JUMPSEARCH');
INSERT INTO selfservice."hw_Preferences" VALUES (3, 'Jump on a single view result', 1, 'JUMPVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (4, 'CSS path', 3, 'CSSPATH');
INSERT INTO selfservice."hw_Preferences" VALUES (5, 'Warn if record data changes', 1, 'CHANGE');
INSERT INTO selfservice."hw_Preferences" VALUES (6, 'AJAX min dd size', 2, 'AJAXMIN');
INSERT INTO selfservice."hw_Preferences" VALUES (7, 'JS path', 3, 'JSPATH');
INSERT INTO selfservice."hw_Preferences" VALUES (9, 'Default view', 3, 'DEFVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (10, 'Warn if view data changes', 1, 'CHECKSUM');
INSERT INTO selfservice."hw_Preferences" VALUES (11, 'Alternative views enabled', 1, 'ALTVIEW');
INSERT INTO selfservice."hw_Preferences" VALUES (12, 'Related Records enabled', 1, 'RELREC');
INSERT INTO selfservice."hw_Preferences" VALUES (13, 'Resizable Table columns', 1, 'RESIZECOLS');
INSERT INTO selfservice."hw_Preferences" VALUES (14, 'Sort NULLS in numerical order', 1, 'NUMNULL');
INSERT INTO selfservice."hw_Preferences" VALUES (15, 'Hideable Table columns', 1, 'HIDECOLS');
INSERT INTO selfservice."hw_Preferences" VALUES (16, 'Reorderable Table columns', 1, 'COLREORDER');
INSERT INTO selfservice."hw_Preferences" VALUES (17, 'Show old search', 1, 'OLDSEARCH');
INSERT INTO selfservice."hw_Preferences" VALUES (18, 'Include "... show next" buttons', 1, 'SHOWNEXT');
INSERT INTO selfservice."hw_Preferences" VALUES (19, 'Warn on missing rules', 1, 'WARNRULES');
INSERT INTO selfservice."hw_Preferences" VALUES (20, 'Cache menu structure', 1, 'CACHEMENU');
INSERT INTO selfservice."hw_Preferences" VALUES (21, 'Remove accents from search', 1, 'UNACCENT');
INSERT INTO selfservice."hw_Preferences" VALUES (22, 'Date format', 3, 'DATEFORMAT');
INSERT INTO selfservice."hw_Preferences" VALUES (23, 'Show web interface hints', 1, 'SHOWHINTS');
INSERT INTO selfservice."hw_Preferences" VALUES (24, 'Show post-update options', 1, 'POSTUPDATEOPT');
INSERT INTO selfservice."hw_Preferences" VALUES (25, 'Show post-insert options', 1, 'POSTINSERTOPT');
INSERT INTO selfservice."hw_Preferences" VALUES (26, 'Offer xdebug option to developers', 1, 'XDEBUG');
INSERT INTO selfservice."hw_Preferences" VALUES (27, 'DB Development group', 3, 'DBDEV');
INSERT INTO selfservice."hw_Preferences" VALUES (28, 'Database internal date format', 3, 'DBDATEFORMAT');
INSERT INTO selfservice."hw_Preferences" VALUES (29, 'Use Ajax many-many selects', 1, 'MM_UI_AJAX');
INSERT INTO selfservice."hw_Preferences" VALUES (30, 'Use many-many tag interface', 1, 'MM_UI_TAG');
INSERT INTO selfservice."hw_Preferences" VALUES (31, 'No JS on many-many selects', 1, 'MM_UI_NOJS');
INSERT INTO selfservice."hw_Preferences" VALUES (32, 'First day of the week', 2, 'JS_UI_DAYSTART');
INSERT INTO selfservice."hw_Preferences" VALUES (33, 'Open record in own tab', 1, 'OWNTAB');

SELECT setval('selfservice."hw_Preferences_id_seq"',(select max(id) from selfservice."hw_Preferences"));

CREATE TABLE selfservice."hw_User Preferences" (
    id serial NOT NULL,
    preference_id integer not null,
    preference_value character varying not null,
    user_id bigint,
    CONSTRAINT "User_Preferences_pkey" PRIMARY KEY (id),
    CONSTRAINT "User_Preferences_fk_preference_id" FOREIGN KEY (preference_id) REFERENCES selfservice."hw_Preferences"(id) ON DELETE CASCADE
);
ALTER TABLE selfservice."hw_User Preferences" OWNER TO dev;
GRANT ALL ON TABLE selfservice."hw_User Preferences" TO postgres;
GRANT ALL ON TABLE selfservice."hw_User Preferences" TO dev;

-- populate defaults
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 2, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 3, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 4, 'user-css');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 5, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 7, 'user-js');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 9, '40_Selfservice/Database_Selfservice');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 11, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 12, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 18, 'FALSE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 28, 'd/m/Y');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 29, 'TRUE');
INSERT INTO selfservice."hw_User Preferences" ( preference_id, preference_value ) VALUES ( 30, 'TRUE');

SELECT setval('selfservice."hw_User Preferences_id_seq"',(select max(id) from selfservice."hw_User Preferences"));
