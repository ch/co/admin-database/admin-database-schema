CREATE VIEW selfservice.room_hid AS
SELECT room.id AS room_id, ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48) AS room_hid
   FROM room
      LEFT JOIN building_floor_hid ON room.building_floor_id = building_floor_hid.building_floor_id
         LEFT JOIN building_region_hid ON building_region_hid.building_region_id = room.building_region_id
            LEFT JOIN building_hid ON building_hid.building_id = room.building_id
              ORDER BY ((room.name::text || COALESCE(((' ('::text || COALESCE(building_floor_hid.building_floor_hid::text || ', '::text, ''::text)) || COALESCE(building_region_hid.building_region_hid, building_hid.building_hid)::text) || ')'::text, ''::text)))::character varying(48);

ALTER VIEW selfservice.room_hid OWNER TO dev;
GRANT SELECT ON selfservice.room_hid TO PUBLIC;

CREATE VIEW selfservice.dept_telephone_number_hid AS
SELECT dept_telephone_number.id AS dept_telephone_number_id, dept_telephone_number.extension_number::text || 
        CASE
            WHEN dept_telephone_number.fax = true THEN ' (fax)'::text
            WHEN dept_telephone_number.personal_line = false THEN ' (shared)'::text
            ELSE ''::text
        END AS dept_telephone_number_hid
   FROM dept_telephone_number
  ORDER BY dept_telephone_number.extension_number;
;
ALTER VIEW selfservice.dept_telephone_number_hid OWNER TO dev;
GRANT SELECT ON selfservice.dept_telephone_number_hid TO PUBLIC;

