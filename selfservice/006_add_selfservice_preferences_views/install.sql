CREATE VIEW selfservice._preferences AS 
 SELECT "Preferences".hw_preference_const AS preference_name, a.preference_value, hw_preference_type_hid.hw_preference_type_hid AS preference_type
   FROM (        (         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM selfservice."hw_User Preferences" "User Preferences"
                          WHERE "User Preferences".user_id IS NULL
                UNION ALL 
                         SELECT "User Preferences".preference_id, "User Preferences".preference_value
                           FROM pg_auth_members m
                      JOIN pg_roles b ON m.roleid = b.oid
                 JOIN pg_roles r ON m.member = r.oid
            JOIN selfservice."hw_User Preferences" "User Preferences" ON b.oid = "User Preferences".user_id::oid
           WHERE r.rolname = "current_user"())
        UNION ALL 
                 SELECT "User Preferences".preference_id, "User Preferences".preference_value
                   FROM pg_roles
              JOIN selfservice."hw_User Preferences" "User Preferences" ON pg_roles.oid = "User Preferences".user_id::oid
             WHERE pg_roles.rolname = "current_user"()) a
   JOIN selfservice."hw_Preferences" "Preferences" ON a.preference_id = "Preferences".id
   JOIN selfservice.hw_preference_type_hid USING (hw_preference_type_id);

ALTER TABLE selfservice._preferences OWNER TO dev;
GRANT ALL ON TABLE selfservice._preferences TO dev;
GRANT SELECT ON TABLE selfservice._preferences TO public;

