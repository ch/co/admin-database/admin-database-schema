CREATE TABLE selfservice._primary_table (
	view_name varchar primary key,
	primary_table varchar);

ALTER TABLE selfservice._primary_table OWNER TO dev;
GRANT SELECT ON selfservice._primary_table TO PUBLIC;


CREATE VIEW selfservice._view_data AS 
 SELECT 
	pgns.nspname AS schema, 
	pg_class.relname AS view, 
	pg_class.relacl AS perms, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'insert'::text) AS insert, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'update'::text) AS update, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'delete'::text) AS delete, 
	has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'select'::text) AS "select", 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '4'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS delete_rule, 
	(( 
		SELECT count(*) AS count
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	)) > 0 AS insert_rule, 
	( 
		SELECT r.ev_action::text ~~ '%returningList:%'::text
           	FROM pg_rewrite r
      		JOIN pg_class c ON c.oid = r.ev_class
   		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  		WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace
	) AS insert_returning, 
	(( SELECT count(*) AS count
           FROM pg_rewrite r
      JOIN pg_class c ON c.oid = r.ev_class
   LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE r.ev_type = '2'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND c.relnamespace = pg_class.relnamespace)) > 0 AS update_rule, pg_views.definition, 
	_primary_table.primary_table, 
        CASE
            WHEN regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ '%ORDER BY%'::character varying::text THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text)
            ELSE NULL::text
        END AS order_by
   FROM pg_class
   JOIN pg_namespace pgns ON pg_class.relnamespace = pgns.oid
   JOIN pg_views ON pg_class.relname = pg_views.viewname AND pgns.nspname = pg_views.schemaname
   LEFT JOIN selfservice._primary_table on pg_class.relname = _primary_table.view_name
  WHERE pg_class.relname ~~ '%/%'::text AND (pgns.nspname = ANY (ARRAY['selfservice'::name, 'public'::name]));


ALTER TABLE selfservice._view_data OWNER TO dev;
GRANT ALL ON TABLE selfservice._view_data TO dev;
GRANT SELECT ON TABLE selfservice._view_data TO public;

