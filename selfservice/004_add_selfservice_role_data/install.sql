-- What permissions/roles do we have?
CREATE OR REPLACE VIEW selfservice._role_data AS 
 SELECT member.rolname AS member, role.rolname AS role
   FROM pg_roles role
   LEFT JOIN pg_auth_members ON role.oid = pg_auth_members.roleid
   RIGHT JOIN pg_roles member ON member.oid = pg_auth_members.member;

ALTER TABLE selfservice._role_data OWNER TO dev;
GRANT ALL ON TABLE selfservice._role_data TO dev;
GRANT SELECT ON TABLE selfservice._role_data TO public;
