CREATE SCHEMA selfservice;
ALTER SCHEMA selfservice OWNER to dev;
GRANT USAGE on SCHEMA selfservice to PUBLIC;
GRANT USAGE on SCHEMA selfservice to _pgbackup;
