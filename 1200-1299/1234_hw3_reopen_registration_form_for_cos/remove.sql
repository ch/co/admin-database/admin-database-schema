
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Registration/Reopen_Form';
DELETE FROM hotwire3._primary_table WHERE view_name = '10_View/Registration/All_Forms_Summary';

DROP VIEW hotwire3."10_View/Registration/Reopen_Form";

DROP FUNCTION hotwire3.registration_form_reopen();
