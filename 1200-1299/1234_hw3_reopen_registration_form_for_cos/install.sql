CREATE VIEW hotwire3."10_View/Registration/Reopen_Form"
 AS
 SELECT form.form_id AS id,
    form._form_started AS ro_form_started_on,
    form._form_started,
    creator.person_hid AS ro_form_created_by,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    post_category_hid.post_category_hid::character varying AS ro_post_category,
    form.nationality_id,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.email::character varying AS email,
    form.crsid::character varying AS crsid,
    form.start_date,
    form.intended_end_date,
    form.department_host_id AS supervisor_id,
    form.submitted AS personal_details_submitted,
    NULL::boolean AS reopen_personal_details_form,
    '\x436f6e74656e742d747970653a206170706c69636174696f6e2f7064660a0a'::bytea || form.safety_form_pdf AS "safety_checklist.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signer_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_paper_safety_checklist,
    form.safety_submitted,
    NULL::boolean AS reopen_safety_checklist,
    form._imported_on AS ro_imported_on,
        CASE
            WHEN registration.state_of_form(form.uuid) <> 'Complete'::text THEN registration.state_of_form(form.uuid)
            WHEN registration.state_of_form(form.uuid) = 'Complete'::text AND form._imported_on IS NOT NULL THEN 'Registered'::text
            ELSE 'Awaiting processing'::text
        END AS ro_state,
    form.uuid AS ro_form_uuid,
        CASE
            WHEN registration.state_of_form(form.uuid) = 'Complete'::text AND form._imported_on IS NOT NULL THEN 'blue'::text
            WHEN registration.state_of_form(form.uuid) <> 'Complete'::text THEN 'red'::text
            ELSE 'orange'::text
        END AS _cssclass,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
     LEFT JOIN hotwire3.post_category_hid ON post_category_hid.post_category_id = form.post_category_id
     LEFT JOIN hotwire3.person_hid creator ON form.notify_person_id = creator.person_id
  ORDER BY form._form_started DESC;

ALTER TABLE hotwire3."10_View/Registration/All_Forms_Summary"
    OWNER TO dev;

GRANT SELECT,UPDATE ON TABLE hotwire3."10_View/Registration/Reopen_Form" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Registration/All_Forms_Summary" TO dev;

CREATE FUNCTION hotwire3.registration_form_reopen()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    -- reopen forms for editing in the registration app
    IF NEW.reopen_personal_details_form THEN
        NEW.personal_details_submitted = FALSE::boolean;
    END IF;
    IF NEW.reopen_safety_checklist THEN
        NEW.safety_submitted = FALSE::boolean;
    END IF;
    UPDATE registration.form SET
    submitted = NEW.personal_details_submitted,
    safety_submitted = NEW.safety_submitted
     WHERE form_id = OLD.id;
RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.registration_form_summary_upd()
    OWNER TO dev;


CREATE TRIGGER registration_form_reopen
    INSTEAD OF UPDATE 
    ON hotwire3."10_View/Registration/Reopen_Form"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.registration_form_reopen();

INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Registration/Reopen_Form', 'registration.form');
INSERT INTO hotwire3._primary_table ( view_name, primary_table ) VALUES ( '10_View/Registration/All_Forms_Summary', 'registration.form');
