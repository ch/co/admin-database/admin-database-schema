-- View: public._autobuild

DROP VIEW public._autobuild;

CREATE OR REPLACE VIEW public._autobuild
 AS
 SELECT ip_address_view.ip,
    netboot_options.command AS netboot,
    system_image.reinstall_on_next_boot AS reinstall,
    operating_system.os,
    replace(architectures.arch::text, 'i386'::text, 'x86'::text) AS arch,
    split_part(operating_system.path_to_autoinstaller::text, ':'::text, 1) AS pxename,
    split_part(operating_system.path_to_autoinstaller::text, ':'::text, 2) AS winver,
    split_part(operating_system.path_to_autoinstaller::text, ':'::text, 3) AS winsrc,
    split_part(operating_system.path_to_autoinstaller::text, ':'::text, 4) AS wincmd,
    split_part(operating_system.path_to_autoinstaller::text, ':'::text, 5) AS winwim,
    os_and_arch_have_netboot_filename.netboot_filename,
        CASE architectures.arch
            WHEN 'i386'::text THEN split_part(operating_system.default_key::text, ':'::text, 1)
            WHEN 'amd64'::text THEN COALESCE(split_part(operating_system.default_key::text, ':'::text, 2) || split_part(operating_system.default_key::text, ':'::text, 1))
            ELSE NULL::text
        END AS default_key,
    system_image.wired_mac_1,
    system_image.wired_mac_2,
    system_image.wired_mac_3,
    system_image.wired_mac_4,
    system_image.wireless_mac,
    system_image.ansible_branch,
    def_sysim.partition,
    ip_address_view.hostname,
    ( SELECT 0 < count(*)
           FROM mm_system_image_installer_tag
          WHERE mm_system_image_installer_tag.system_image_id = system_image.id AND mm_system_image_installer_tag.installer_tag_id = 8) AS dsk_nuke
   FROM ip_address_view
     JOIN mm_system_image_ip_address ON ip_address_view.id = mm_system_image_ip_address.ip_address_id
     JOIN system_image ON mm_system_image_ip_address.system_image_id = system_image.id
     JOIN operating_system ON system_image.operating_system_id = operating_system.id
     LEFT JOIN architectures ON system_image.architecture_id = architectures.id
     JOIN hardware ON system_image.hardware_id = hardware.id
     LEFT JOIN netboot_options ON system_image.netboot_id = netboot_options.id
     LEFT JOIN system_image def_sysim ON hardware.default_system_image_id = def_sysim.id
     LEFT JOIN os_and_arch_have_netboot_filename ON os_and_arch_have_netboot_filename.os_id = operating_system.id AND os_and_arch_have_netboot_filename.arch_id = architectures.id
  ORDER BY system_image.reinstall_on_next_boot DESC;

ALTER TABLE public._autobuild
    OWNER TO dev;

GRANT ALL ON TABLE public._autobuild TO dev;
GRANT SELECT ON TABLE public._autobuild TO osbuilder;


