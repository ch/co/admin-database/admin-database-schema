CREATE FUNCTION public.is_research_group_active(id_research_group BIGINT) RETURNS BOOLEAN AS
$body$
DECLARE
    is_group_active BOOLEAN;
BEGIN
    SELECT TRUE INTO is_group_active
    FROM public.research_group AS rg
    JOIN public.mm_person_research_group AS prg ON prg.research_group_id = rg.id
    JOIN public._physical_status_v3 AS ps ON ps.person_id = prg.person_id
    WHERE rg.id = id_research_group
    AND ps.status_id = 'Current';

    IF is_group_active is NULL THEN
        SELECT TRUE INTO is_group_active
        FROM public.research_group AS rg
        JOIN public._physical_status_v3 AS ps ON ps.person_id = rg.head_of_group_id
        WHERE rg.id = id_research_group
        AND ps.status_id = 'Current';
    END IF;

    IF is_group_active is NULL THEN
        is_group_active := FALSE;   
    END IF;

    RETURN is_group_active;

END;
$body$ LANGUAGE PLPGSQL;

ALTER FUNCTION public.is_research_group_active(BIGINT) OWNER TO dev;
