BEGIN;

SELECT plan(3);

PREPARE hotwire_version AS
    SELECT id, CASE WHEN ro_group_status = 'Active' THEN TRUE ELSE FALSE END as group_status
    FROM hotwire3."10_View/Groups/Research_Groups_Computing"
    ORDER BY id;

PREPARE function_version AS
    SELECT id, public.is_research_group_active(id) AS group_status
    FROM public.research_group
    ORDER BY id;

SELECT has_function('public','is_research_group_active','{bigint}'::name[],'function public.is_research_group_active(bigint) should exist');
SELECT function_owner_is('public','is_research_group_active','{bigint}'::name[], 'dev', 'function public.is_research_group_active(bigint) should be owned by dev');

SELECT results_eq('hotwire_version','function_version');

ROLLBACK;
