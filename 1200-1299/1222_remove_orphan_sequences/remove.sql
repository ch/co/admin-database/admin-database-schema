--
-- Name: adnetworkdrivegroup_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.adnetworkdrivegroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.adnetworkdrivegroup_id_seq OWNER TO cen1001;

--
-- Name: adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq OWNER TO cen1001;

--
-- Name: adsoftwaregroup_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.adsoftwaregroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.adsoftwaregroup_id_seq OWNER TO cen1001;

--
-- Name: adsoftwaregroupsystemimage_adsoftwaregroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.adsoftwaregroupsystemimage_adsoftwaregroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.adsoftwaregroupsystemimage_adsoftwaregroupid_seq OWNER TO cen1001;

--
-- Name: adsoftwaregroupsystemimage_systemimageid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.adsoftwaregroupsystemimage_systemimageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.adsoftwaregroupsystemimage_systemimageid_seq OWNER TO cen1001;

--
-- Name: depttelephonenumberperson_depttelephonenumberid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.depttelephonenumberperson_depttelephonenumberid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.depttelephonenumberperson_depttelephonenumberid_seq OWNER TO cen1001;

--
-- Name: depttelephonenumberperson_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.depttelephonenumberperson_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.depttelephonenumberperson_personid_seq OWNER TO cen1001;

--
-- Name: funding_type_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.funding_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.funding_type_id_seq OWNER TO cen1001;

--
-- Name: grouphascomputerrep_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.grouphascomputerrep_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.grouphascomputerrep_personid_seq OWNER TO cen1001;

--
-- Name: grouphascomputerrep_researchgroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.grouphascomputerrep_researchgroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.grouphascomputerrep_researchgroupid_seq OWNER TO cen1001;

--
-- Name: grouphasmember_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.grouphasmember_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.grouphasmember_personid_seq OWNER TO cen1001;

--
-- Name: grouphasmember_researchgroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.grouphasmember_researchgroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.grouphasmember_researchgroupid_seq OWNER TO cen1001;

--
-- Name: hardwaresocket_hardwareid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.hardwaresocket_hardwareid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.hardwaresocket_hardwareid_seq OWNER TO cen1001;

--
-- Name: hardwaresocket_socketid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.hardwaresocket_socketid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.hardwaresocket_socketid_seq OWNER TO cen1001;

--
-- Name: mailinglistexcludesperson_mailinglistid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.mailinglistexcludesperson_mailinglistid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.mailinglistexcludesperson_mailinglistid_seq OWNER TO cen1001;

--
-- Name: mailinglistexcludesperson_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.mailinglistexcludesperson_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.mailinglistexcludesperson_personid_seq OWNER TO cen1001;

--
-- Name: mm_postgraduate_studentship_funding_source_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.mm_postgraduate_studentship_funding_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.mm_postgraduate_studentship_funding_source_id_seq OWNER TO cen1001;

--
-- Name: personmanageslist_mailinglistid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personmanageslist_mailinglistid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personmanageslist_mailinglistid_seq OWNER TO cen1001;

--
-- Name: personmanageslist_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personmanageslist_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personmanageslist_personid_seq OWNER TO cen1001;

--
-- Name: personmanagessystem_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personmanagessystem_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personmanagessystem_personid_seq OWNER TO cen1001;

--
-- Name: personmanagessystem_systemimageid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personmanagessystem_systemimageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personmanagessystem_systemimageid_seq OWNER TO cen1001;

--
-- Name: personstatus_personid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personstatus_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personstatus_personid_seq OWNER TO cen1001;

--
-- Name: personstatus_statusid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.personstatus_statusid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.personstatus_statusid_seq OWNER TO cen1001;

--
-- Name: researchgroupservice_researchgroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.researchgroupservice_researchgroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.researchgroupservice_researchgroupid_seq OWNER TO cen1001;

--
-- Name: researchgroupservice_serviceid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.researchgroupservice_serviceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.researchgroupservice_serviceid_seq OWNER TO cen1001;

--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_id_seq OWNER TO cen1001;

--
-- Name: servicetype_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.servicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.servicetype_id_seq OWNER TO cen1001;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.status_id_seq OWNER TO cen1001;

--
-- Name: switchstack_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.switchstack_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.switchstack_id_seq OWNER TO cen1001;

--
-- Name: system_image_ip_address_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.system_image_ip_address_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.system_image_ip_address_ip_address_id_seq OWNER TO cen1001;

--
-- Name: system_image_ip_address_system_image_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.system_image_ip_address_system_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.system_image_ip_address_system_image_id_seq OWNER TO cen1001;

--
-- Name: systemprovidesservice_serviceid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.systemprovidesservice_serviceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.systemprovidesservice_serviceid_seq OWNER TO cen1001;

--
-- Name: systemprovidesservice_systemimageid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.systemprovidesservice_systemimageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.systemprovidesservice_systemimageid_seq OWNER TO cen1001;

--
-- Name: systemusesservice_serviceid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.systemusesservice_serviceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.systemusesservice_serviceid_seq OWNER TO cen1001;

--
-- Name: systemusesservice_systemimageid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.systemusesservice_systemimageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.systemusesservice_systemimageid_seq OWNER TO cen1001;

--
-- Name: useraccountservice_id_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.useraccountservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.useraccountservice_id_seq OWNER TO cen1001;

--
-- Name: useraccountunixgroup_unixgroupid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.useraccountunixgroup_unixgroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.useraccountunixgroup_unixgroupid_seq OWNER TO cen1001;

--
-- Name: useraccountunixgroup_useraccountid_seq; Type: SEQUENCE; Schema: public; Owner: cen1001
--

CREATE SEQUENCE public.useraccountunixgroup_useraccountid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.useraccountunixgroup_useraccountid_seq OWNER TO cen1001;

--
-- Name: adnetworkdrivegroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.adnetworkdrivegroup_id_seq', 1, false);


--
-- Name: adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq', 1, false);


--
-- Name: adsoftwaregroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.adsoftwaregroup_id_seq', 1, false);


--
-- Name: adsoftwaregroupsystemimage_adsoftwaregroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.adsoftwaregroupsystemimage_adsoftwaregroupid_seq', 1, false);


--
-- Name: adsoftwaregroupsystemimage_systemimageid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.adsoftwaregroupsystemimage_systemimageid_seq', 1, false);


--
-- Name: depttelephonenumberperson_depttelephonenumberid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.depttelephonenumberperson_depttelephonenumberid_seq', 1, false);


--
-- Name: depttelephonenumberperson_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.depttelephonenumberperson_personid_seq', 1, false);


--
-- Name: funding_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.funding_type_id_seq', 3, true);


--
-- Name: grouphascomputerrep_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.grouphascomputerrep_personid_seq', 1, false);


--
-- Name: grouphascomputerrep_researchgroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.grouphascomputerrep_researchgroupid_seq', 1, false);


--
-- Name: grouphasmember_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.grouphasmember_personid_seq', 1, false);


--
-- Name: grouphasmember_researchgroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.grouphasmember_researchgroupid_seq', 1, false);


--
-- Name: hardwaresocket_hardwareid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.hardwaresocket_hardwareid_seq', 1, false);


--
-- Name: hardwaresocket_socketid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.hardwaresocket_socketid_seq', 1, false);


--
-- Name: mailinglistexcludesperson_mailinglistid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.mailinglistexcludesperson_mailinglistid_seq', 1, false);


--
-- Name: mailinglistexcludesperson_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.mailinglistexcludesperson_personid_seq', 1, false);


--
-- Name: mm_postgraduate_studentship_funding_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.mm_postgraduate_studentship_funding_source_id_seq', 2, true);


--
-- Name: personmanageslist_mailinglistid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personmanageslist_mailinglistid_seq', 1, false);


--
-- Name: personmanageslist_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personmanageslist_personid_seq', 1, false);


--
-- Name: personmanagessystem_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personmanagessystem_personid_seq', 1, false);


--
-- Name: personmanagessystem_systemimageid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personmanagessystem_systemimageid_seq', 1, false);


--
-- Name: personstatus_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personstatus_personid_seq', 1, false);


--
-- Name: personstatus_statusid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.personstatus_statusid_seq', 1, false);


--
-- Name: researchgroupservice_researchgroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.researchgroupservice_researchgroupid_seq', 1, false);


--
-- Name: researchgroupservice_serviceid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.researchgroupservice_serviceid_seq', 1, false);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.service_id_seq', 1, false);


--
-- Name: servicetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.servicetype_id_seq', 1, false);


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.status_id_seq', 1, false);


--
-- Name: switchstack_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.switchstack_id_seq', 3, true);


--
-- Name: system_image_ip_address_ip_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.system_image_ip_address_ip_address_id_seq', 1, false);


--
-- Name: system_image_ip_address_system_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.system_image_ip_address_system_image_id_seq', 1, false);


--
-- Name: systemprovidesservice_serviceid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.systemprovidesservice_serviceid_seq', 1, false);


--
-- Name: systemprovidesservice_systemimageid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.systemprovidesservice_systemimageid_seq', 1, false);


--
-- Name: systemusesservice_serviceid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.systemusesservice_serviceid_seq', 1, false);


--
-- Name: systemusesservice_systemimageid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.systemusesservice_systemimageid_seq', 1, false);


--
-- Name: useraccountservice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.useraccountservice_id_seq', 1, false);


--
-- Name: useraccountunixgroup_unixgroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.useraccountunixgroup_unixgroupid_seq', 1, false);


--
-- Name: useraccountunixgroup_useraccountid_seq; Type: SEQUENCE SET; Schema: public; Owner: cen1001
--

SELECT pg_catalog.setval('public.useraccountunixgroup_useraccountid_seq', 1, false);


--
-- Name: SEQUENCE adnetworkdrivegroup_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.adnetworkdrivegroup_id_seq TO dev;


--
-- Name: SEQUENCE adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.adnetworkdrivegroupuseraccount_adnetworkdrivegroupid_seq TO dev;


--
-- Name: SEQUENCE adsoftwaregroup_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.adsoftwaregroup_id_seq TO dev;


--
-- Name: SEQUENCE adsoftwaregroupsystemimage_adsoftwaregroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.adsoftwaregroupsystemimage_adsoftwaregroupid_seq TO dev;


--
-- Name: SEQUENCE adsoftwaregroupsystemimage_systemimageid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.adsoftwaregroupsystemimage_systemimageid_seq TO dev;


--
-- Name: SEQUENCE depttelephonenumberperson_depttelephonenumberid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.depttelephonenumberperson_depttelephonenumberid_seq TO dev;


--
-- Name: SEQUENCE depttelephonenumberperson_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.depttelephonenumberperson_personid_seq TO dev;


--
-- Name: SEQUENCE funding_type_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.funding_type_id_seq TO dev;


--
-- Name: SEQUENCE grouphascomputerrep_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.grouphascomputerrep_personid_seq TO dev;


--
-- Name: SEQUENCE grouphascomputerrep_researchgroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.grouphascomputerrep_researchgroupid_seq TO dev;


--
-- Name: SEQUENCE grouphasmember_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.grouphasmember_personid_seq TO dev;


--
-- Name: SEQUENCE grouphasmember_researchgroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.grouphasmember_researchgroupid_seq TO dev;


--
-- Name: SEQUENCE hardwaresocket_hardwareid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.hardwaresocket_hardwareid_seq TO dev;


--
-- Name: SEQUENCE hardwaresocket_socketid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.hardwaresocket_socketid_seq TO dev;


--
-- Name: SEQUENCE mailinglistexcludesperson_mailinglistid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.mailinglistexcludesperson_mailinglistid_seq TO dev;


--
-- Name: SEQUENCE mailinglistexcludesperson_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.mailinglistexcludesperson_personid_seq TO dev;


--
-- Name: SEQUENCE mm_postgraduate_studentship_funding_source_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.mm_postgraduate_studentship_funding_source_id_seq TO dev;


--
-- Name: SEQUENCE personmanageslist_mailinglistid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personmanageslist_mailinglistid_seq TO dev;


--
-- Name: SEQUENCE personmanageslist_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personmanageslist_personid_seq TO dev;


--
-- Name: SEQUENCE personmanagessystem_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personmanagessystem_personid_seq TO dev;


--
-- Name: SEQUENCE personmanagessystem_systemimageid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personmanagessystem_systemimageid_seq TO dev;


--
-- Name: SEQUENCE personstatus_personid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personstatus_personid_seq TO dev;


--
-- Name: SEQUENCE personstatus_statusid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.personstatus_statusid_seq TO dev;


--
-- Name: SEQUENCE researchgroupservice_researchgroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.researchgroupservice_researchgroupid_seq TO dev;


--
-- Name: SEQUENCE researchgroupservice_serviceid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.researchgroupservice_serviceid_seq TO dev;


--
-- Name: SEQUENCE service_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.service_id_seq TO dev;


--
-- Name: SEQUENCE servicetype_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.servicetype_id_seq TO dev;


--
-- Name: SEQUENCE status_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.status_id_seq TO dev;


--
-- Name: SEQUENCE switchstack_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.switchstack_id_seq TO dev;


--
-- Name: SEQUENCE system_image_ip_address_ip_address_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.system_image_ip_address_ip_address_id_seq TO dev;


--
-- Name: SEQUENCE system_image_ip_address_system_image_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.system_image_ip_address_system_image_id_seq TO dev;


--
-- Name: SEQUENCE systemprovidesservice_serviceid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.systemprovidesservice_serviceid_seq TO dev;


--
-- Name: SEQUENCE systemprovidesservice_systemimageid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.systemprovidesservice_systemimageid_seq TO dev;


--
-- Name: SEQUENCE systemusesservice_serviceid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.systemusesservice_serviceid_seq TO dev;


--
-- Name: SEQUENCE systemusesservice_systemimageid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.systemusesservice_systemimageid_seq TO dev;


--
-- Name: SEQUENCE useraccountservice_id_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.useraccountservice_id_seq TO dev;


--
-- Name: SEQUENCE useraccountunixgroup_unixgroupid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.useraccountunixgroup_unixgroupid_seq TO dev;


--
-- Name: SEQUENCE useraccountunixgroup_useraccountid_seq; Type: ACL; Schema: public; Owner: cen1001
--

GRANT ALL ON SEQUENCE public.useraccountunixgroup_useraccountid_seq TO dev;
