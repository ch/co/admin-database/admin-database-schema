ALTER TABLE public.person ADD COLUMN managed_mail_domain_optout BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE public.person ADD COLUMN managed_mail_domain_target TEXT;
