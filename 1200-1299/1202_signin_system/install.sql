CREATE OR REPLACE FUNCTION bigendian_to_littleendian(hex_string TEXT)
RETURNS TEXT AS $$
DECLARE
    bin_data BYTEA;
    bin_length INT;
    reversed_bin BYTEA := '';
    i INT;
BEGIN
    -- Convert the hexadecimal string to binary
    bin_data := decode(hex_string, 'hex');

    bin_length := length(bin_data);

    -- Reverse the byte order
    FOR i IN 0..(bin_length - 1) LOOP
        reversed_bin := reversed_bin || substr(bin_data, bin_length - i, 1);
    END LOOP;

    -- Convert the reversed binary data back to a hexadecimal string
    RETURN encode(reversed_bin, 'hex');
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION bigendian_to_littleendian(TEXT) OWNER TO dev;

CREATE FUNCTION mifare_id_to_signin(mifare_id BIGINT)
RETURNS TEXT AS $$
BEGIN
    RETURN upper(bigendian_to_littleendian((lpad(to_hex(mifare_id),8,'0'))));
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION mifare_id_to_signin(BIGINT) OWNER TO dev;


DROP VIEW apps.signin_system_all_cards;

CREATE VIEW apps.signin_system_all_cards AS
 SELECT
    COALESCE(COALESCE(COALESCE(p.known_as, p.first_names)::text || ' '::text, ''::text) || p.surname::text, n.lookup_visible_name, c.full_name, c.crsid) AS "Name",
    COALESCE(p.email_address, n.lookup_email::character varying, lower(c.crsid)::character varying || '@cam.ac.uk') AS "Email",
    NULL::text AS "Mobile",
    NULL::text AS "Role",
    mifare_id_to_signin(mifare_id) AS "RFID"
   FROM university_card c
     LEFT JOIN person p ON lower(p.crsid::text) = lower(c.crsid)
     LEFT JOIN university_cardholder_detail n ON n.crsid = lower(c.crsid)
  WHERE c.expires_at > CURRENT_TIMESTAMP AND (c.crsid IS NOT NULL) AND c.mifare_id IS NOT NULL
  ORDER BY c.crsid;

ALTER VIEW apps.signin_system_all_cards OWNER TO dev;

GRANT SELECT ON apps.signin_system_all_cards TO reception, cos;


DROP VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads";
CREATE VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads" AS
    SELECT 
        row_number() over() AS id,
        COALESCE((COALESCE(p.known_as,p.first_names) || ' '),'')|| p.surname AS "Name",
        COALESCE(p.email_address, lower(c.crsid) || '@cam.ac.uk') AS "Email",
        NULL::text AS "Mobile",
        NULL::text AS "Role",
        mifare_id_to_signin(mifare_id) AS "RFID"
    FROM public.person p
    JOIN public.university_card c ON lower(p.crsid) = lower(c.crsid)
    JOIN public._hr_role r on r.person_id = p.id
    WHERE r.predicted_role_status_id = 'Current'
    AND c.expires_at > current_timestamp
    AND c.mifare_id IS NOT NULL
    AND 
        (
        r.role_tablename = 'postgraduate_studentship'
        OR
        r.role_tablename = 'erasmus_socrates_studentship'
        OR
        (
            r.role_tablename = 'visitorship'
            AND 
            (
                ( r.post_category_id = 'v-3' OR r.post_category = 'Visitor (MPhil Postgraduate)')
                OR
                ( r.post_category_id = 'v-2' OR r.post_category = 'Visitor (PhD Postgraduate)')
            )
        )
    )
;

ALTER VIEW hotwire3."10_View/Sign_In_System/Current_Postgrads" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/Sign_In_System/Current_Postgrads" TO cos, student_management;

DROP VIEW hotwire3."10_View/Sign_In_System/Current_University_Cards";
CREATE VIEW hotwire3."10_View/Sign_In_System/Current_University_Cards" AS
    SELECT
        row_number() over() AS id,
        COALESCE(
            COALESCE((COALESCE(p.known_as,p.first_names) || ' '),'')|| p.surname,
            n.lookup_visible_name,
            c.full_name,
            c.crsid
        ) AS "Name",
        COALESCE(p.email_address, n.lookup_email::character varying, lower(c.crsid)::character varying || '@cam.ac.uk') AS "Email",
        NULL::text AS "Mobile",
        NULL::text AS "Role",
        mifare_id_to_signin(mifare_id) AS "RFID"
    FROM public.university_card c
    LEFT JOIN public.person p ON lower(p.crsid) = lower(c.crsid)
    LEFT JOIN public.university_cardholder_detail n ON n.crsid = lower(c.crsid)  -- n.crsid is constrained to be lowercase
    WHERE c.expires_at > current_timestamp
    AND c.mifare_id IS NOT NULL
    AND (
        c.crsid IS NOT NULL
    )
    ;

ALTER VIEW  hotwire3."10_View/Sign_In_System/Current_University_Cards" OWNER TO dev;

GRANT SELECT ON hotwire3."10_View/Sign_In_System/Current_University_Cards" TO cos, reception;
