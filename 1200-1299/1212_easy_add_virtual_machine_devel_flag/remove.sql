DROP VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine";

CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
 AS
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id,
    ''::character varying(63) AS hostname,
    NULL::integer AS subnet_id,
    NULL::integer AS hardware_id,
    NULL::macaddr AS wired_mac_1,
    ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text ~~* 'ubuntu%'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id,
    1 AS reboot_policy_id,
    ''::text AS system_image_comments,
    NULL::integer AS host_system_image_id,
    NULL::macaddr AS wired_mac_2,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id,
    ( SELECT mm_person_research_group.research_group_id
           FROM person
             JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS research_group_id;

ALTER TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    OWNER TO dev;

GRANT SELECT, UPDATE ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO cos;
GRANT ALL ON TABLE hotwire3."10_View/Computers/Easy_Add_Virtual_Machine" TO dev;

CREATE OR REPLACE FUNCTION hotwire3.easy_add_virtual_machine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE
    ip_id BIGINT;                                                                                        
    new_system_image_id BIGINT;
BEGIN                                                                                                              
    ip_id =  (SELECT ip_address.id
              FROM ip_address
              WHERE ip_address.subnet_id = NEW.subnet_id 
	          AND ip_address.reserved <> true 
	          AND (ip_address.hostname IS NULL OR ip_address.hostname::text = ''::text)
              LIMIT 1);

    INSERT INTO system_image 
        (wired_mac_1,
         wired_mac_2, hardware_id, 
		operating_system_id, reboot_policy_id, comments,
		user_id, research_group_id) 
    VALUES (NEW.wired_mac_1, NEW.wired_mac_2, NEW.hardware_id, 
		NEW.operating_system_id, NEW.reboot_policy_id, NEW.system_image_comments::varchar(1000),
		NEW.user_id, NEW.research_group_id)
    RETURNING id INTO new_system_image_id;
    UPDATE ip_address SET hostname = NEW.hostname WHERE id = ip_id;
    INSERT INTO mm_system_image_ip_address
        ( ip_address_id, system_image_id )
    VALUES
        ( ip_id, new_system_image_id);
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION hotwire3.easy_add_virtual_machine()
    OWNER TO dev;


CREATE TRIGGER hw3_easy_add_virtual_machine
    INSTEAD OF INSERT OR UPDATE 
    ON hotwire3."10_View/Computers/Easy_Add_Virtual_Machine"
    FOR EACH ROW
    EXECUTE FUNCTION hotwire3.easy_add_virtual_machine();


