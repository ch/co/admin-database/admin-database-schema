CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Groups"
 AS
 SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.deputy_head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    research_group.website_address,
    research_group.internal_use_only AS hide_from_websites,
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id) AS person_id,
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id) AS computer_rep_id,
        CASE
            WHEN pi_status.status_id::text = 'Current'::text THEN 'Active'::character varying
            WHEN has_current_members.has_current_members = true THEN 'Active'::character varying
            ELSE 'Inactive'::character varying
        END AS ro_group_status,
        CASE
            WHEN pi_status.status_id::text = 'Current'::text THEN NULL::text
            WHEN has_current_members.has_current_members = true THEN NULL::text
            ELSE 'orange'::text
        END AS _cssclass
   FROM research_group
    JOIN _physical_status_v3 pi_status ON research_group.head_of_group_id = pi_status.person_id
    JOIN ( SELECT research_group_1.id,
            'Current'::character varying(20)::text = ANY (((ARRAY( SELECT _physical_status.status_id
                   FROM mm_person_research_group
                     JOIN _physical_status_v3 _physical_status USING (person_id)
                  WHERE mm_person_research_group.research_group_id = research_group_1.id))::text[])) AS has_current_members
           FROM research_group research_group_1) has_current_members USING (id)
   ORDER BY research_group.name;
