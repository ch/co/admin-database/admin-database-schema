CREATE OR REPLACE FUNCTION public.ensure_staff_intended_end_dates()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare
        sc_id bigint;
        assistant_staff bigint;
        acad_related_staff bigint;
        intended_end_date date;
        sp_pk bigint;
begin

        intended_end_date = NEW.intended_end_date;
        sc_id = NEW.staff_category_id;

        select id from staff_category where category = 'Assistant staff' into assistant_staff;
        select id from staff_category where category = 'Academic-related staff' into acad_related_staff;

        if (sc_id <> assistant_staff) and (sc_id <> acad_related_staff) and (intended_end_date is NULL) then
                raise exception 'Intended end date cannot be null for non-assistant or academic-related staff';
                return NULL;
        else
                return NEW;
        end if;

end;
$function$
;

ALTER FUNCTION public.ensure_staff_intended_end_dates() OWNER TO dev;
