CREATE OR REPLACE VIEW hotwire3."10_View/Computers/Easy_Add_Machine"
 AS
 SELECT ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS id,
    ''::character varying(63) AS hostname,
    ( SELECT easy_addable_subnet_hid.easy_addable_subnet_id
           FROM hotwire3.easy_addable_subnet_hid
         LIMIT 1) AS easy_addable_subnet_id,
    NULL::macaddr AS wired_mac_1,
    ( SELECT operating_system_hid.operating_system_id
           FROM hotwire3.operating_system_hid
          WHERE operating_system_hid.operating_system_hid::text = 'Windows 11'::text
          ORDER BY operating_system_hid.operating_system_id DESC
         LIMIT 1) AS operating_system_id,
    1 AS reboot_policy_id,
    'Unknown'::character varying(80) AS manufacturer,
    'Unknown'::character varying(80) AS model,
    ''::character varying(80) AS hardware_name,
    ( SELECT hardware_type_hid.hardware_type_id
           FROM hotwire3.hardware_type_hid
          WHERE hardware_type_hid.hardware_type_hid::text = 'PC'::text
         LIMIT 1) AS hardware_type_id,
    CURRENT_DATE AS date_purchased,
    ''::text AS system_image_comments,
    NULL::integer AS host_system_image_id,
    ( SELECT mm_person_room.room_id
           FROM person
             JOIN mm_person_room ON person.id = mm_person_room.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS room_id,
    NULL::macaddr AS wired_mac_2,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS user_id,
    ( SELECT person.id
           FROM person
          WHERE person.crsid::name = "current_user"()) AS owner_id,
    ( SELECT mm_person_research_group.research_group_id
           FROM person
             JOIN mm_person_research_group ON person.id = mm_person_research_group.person_id
          WHERE person.crsid::name = "current_user"()
         LIMIT 1) AS research_group_id;
