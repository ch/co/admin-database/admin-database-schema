CREATE VIEW _chem_288_mailinglist AS
    SELECT email_address
    FROM public.fn_standard_rg_mailing_list('chem-288','Zhang')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-288','Forse')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-288','Bronstein')
;

ALTER VIEW _chem_288_mailinglist OWNER TO dev;
GRANT SELECT ON _chem_288_mailinglist TO mailinglists,www_sites,mailinglist_readers,leavers_trigger;
