DROP VIEW apps.leavers_it_signature;

ALTER TABLE public.it_leaving_form DROP CONSTRAINT it_leaving_form_username_key;
ALTER TABLE public.it_leaving_form ALTER COLUMN username TYPE varchar(80);
ALTER TABLE public.it_leaving_form RENAME COLUMN username TO crsid;
ALTER TABLE public.it_leaving_form ADD CONSTRAINT it_leaving_form_crsid_key PRIMARY KEY (crsid);

CREATE VIEW apps.leavers_it_signature
 AS
 SELECT it_leaving_form.crsid,
    it_leaving_form.user_date,
    it_leaving_form.co_date,
    it_leaving_form.co_id,
    person_hid.person_hid
   FROM it_leaving_form
     LEFT JOIN person ON it_leaving_form.crsid::text = person.crsid::text
     LEFT JOIN person_hid ON person.id = person_hid.person_id;

ALTER TABLE apps.leavers_it_signature
    OWNER TO dev;

GRANT ALL ON TABLE apps.leavers_it_signature TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE apps.leavers_it_signature TO leavers_trigger;

CREATE RULE it_sig_ins AS
    ON INSERT TO apps.leavers_it_signature
    DO INSTEAD
(INSERT INTO it_leaving_form (crsid, user_date, co_date, co_id)
  VALUES (new.crsid, new.user_date, new.co_date, new.co_id));

CREATE RULE it_sig_upd AS
    ON UPDATE TO apps.leavers_it_signature
    DO INSTEAD
(UPDATE it_leaving_form SET user_date = new.user_date, co_date = new.co_date, co_id = new.co_id
  WHERE (it_leaving_form.crsid = old.crsid));


CREATE VIEW apps.leavers_pi_details
 AS
 SELECT person.crsid AS leaver_crsid,
    person_hid.person_hid AS leaver_name,
    s.id AS pi_id,
    s.crsid AS pi_crsid,
    s.email_address AS pi_email,
    supervisor_hid.supervisor_hid AS pi_name,
    it_leaving_form.pi_date,
    last_signer_hid.person_hid AS last_signature
   FROM person
     LEFT JOIN _current_hr_role_for_person lr ON person.id = lr.person_id
     JOIN person s ON lr.supervisor_id = s.id
     LEFT JOIN supervisor_hid ON s.id = supervisor_hid.supervisor_id
     JOIN person_hid ON person.id = person_hid.person_id
     LEFT JOIN it_leaving_form ON it_leaving_form.crsid::text = person.crsid::text
     LEFT JOIN person_hid last_signer_hid ON it_leaving_form.pi_id = last_signer_hid.person_id
     LEFT JOIN _physical_status_v3 ps ON person.id = ps.person_id
  WHERE person.crsid IS NOT NULL AND s.email_address IS NOT NULL AND ps.status_id::text <> 'Past'::text;

ALTER TABLE apps.leavers_pi_details
    OWNER TO dev;

GRANT ALL ON TABLE apps.leavers_pi_details TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE apps.leavers_pi_details TO leavers_trigger;


CREATE RULE apps_leavers_pi_ins AS
    ON INSERT TO apps.leavers_pi_details
    DO INSTEAD
(INSERT INTO it_leaving_form (crsid, pi_date, pi_id)
  VALUES (new.leaver_crsid, new.pi_date, ( SELECT person.id
           FROM person
          WHERE ((person.crsid)::text = (new.pi_crsid)::text))));

CREATE RULE apps_leavers_pi_upd AS
    ON UPDATE TO apps.leavers_pi_details
    DO INSTEAD
(UPDATE it_leaving_form SET pi_date = new.pi_date, pi_id = ( SELECT person.id
           FROM person
          WHERE ((person.crsid)::text = (new.pi_crsid)::text))
  WHERE ((it_leaving_form.crsid)::text = (new.leaver_crsid)::text));

