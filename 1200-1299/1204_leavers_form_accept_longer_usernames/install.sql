DROP VIEW apps.leavers_it_signature;
DROP VIEW apps.leavers_pi_details;

ALTER TABLE public.it_leaving_form DROP CONSTRAINT it_leaving_form_crsid_key;
ALTER TABLE public.it_leaving_form ALTER COLUMN crsid TYPE TEXT;
ALTER TABLE public.it_leaving_form RENAME COLUMN crsid TO username;
ALTER TABLE public.it_leaving_form ADD CONSTRAINT it_leaving_form_username_key PRIMARY KEY (username);

CREATE VIEW apps.leavers_it_signature
 AS
 SELECT it_leaving_form.username AS crsid,
    it_leaving_form.user_date,
    it_leaving_form.co_date,
    it_leaving_form.co_id,
    person_hid.person_hid
   FROM it_leaving_form
     LEFT JOIN person ON it_leaving_form.username = person.crsid::text
     LEFT JOIN person_hid ON person.id = person_hid.person_id;

ALTER TABLE apps.leavers_it_signature
    OWNER TO dev;

GRANT ALL ON TABLE apps.leavers_it_signature TO dev;
GRANT INSERT, SELECT, UPDATE ON TABLE apps.leavers_it_signature TO leavers_trigger;


CREATE RULE it_sig_ins AS
    ON INSERT TO apps.leavers_it_signature
    DO INSTEAD
(INSERT INTO it_leaving_form (username, user_date, co_date, co_id)
  VALUES (new.crsid, new.user_date, new.co_date, new.co_id));

CREATE RULE it_sig_upd AS
    ON UPDATE TO apps.leavers_it_signature
    DO INSTEAD
(UPDATE it_leaving_form SET user_date = new.user_date, co_date = new.co_date, co_id = new.co_id
  WHERE (it_leaving_form.username = old.crsid));
