CREATE TABLE  public._debug
(
    debug character varying COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE  public._debug
    OWNER to cen1001;

CREATE TABLE public._au252 (
    operation character varying,
    stamp timestamp without time zone,
    username text,
    tablename text,
    oldcols text,
    newcols text,
    id bigint
);


ALTER TABLE public._au252 OWNER TO postgres;


CREATE TABLE public._function_debug (
    stamp timestamp without time zone,
    debug character varying
);


ALTER TABLE public._function_debug OWNER TO cen1001;


CREATE TABLE  public._menu
(
    submenu text COLLATE pg_catalog."default" NOT NULL,
    view text COLLATE pg_catalog."default" NOT NULL
)

TABLESPACE pg_default;

ALTER TABLE  public._menu
    OWNER to cen1001;

REVOKE ALL ON TABLE public._menu FROM PUBLIC;

GRANT SELECT ON TABLE public._menu TO PUBLIC;

GRANT ALL ON TABLE public._menu TO cen1001;


CREATE TABLE  public._subviews
(
    view name COLLATE pg_catalog."C" NOT NULL,
    subview name COLLATE pg_catalog."C" NOT NULL,
    target name COLLATE pg_catalog."C",
    CONSTRAINT pk_subviews PRIMARY KEY (view, subview)
)

TABLESPACE pg_default;

ALTER TABLE  public._subviews
    OWNER to dev;

REVOKE ALL ON TABLE public._subviews FROM PUBLIC;
REVOKE ALL ON TABLE public._subviews FROM cen1001;
REVOKE ALL ON TABLE public._subviews FROM mgmt_ro;

GRANT SELECT ON TABLE public._subviews TO PUBLIC;

GRANT INSERT, UPDATE ON TABLE public._subviews TO cen1001;

GRANT ALL ON TABLE public._subviews TO dev;

GRANT SELECT ON TABLE public._subviews TO mgmt_ro;


CREATE TABLE public.dns_tmp (
    operation character varying,
    stamp timestamp without time zone,
    username text,
    tablename text,
    oldcols text,
    newcols text,
    id bigint
);


ALTER TABLE public.dns_tmp OWNER TO postgres;


CREATE TABLE  public.duplicates
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.duplicates
    OWNER to cen1001;


CREATE TABLE public.rfltest (
    id character varying,
    t character varying
);


ALTER TABLE public.rfltest OWNER TO postgres;


CREATE TABLE  public.rig_temp
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.rig_temp
    OWNER to cen1001;


CREATE TABLE public.swconffixup
(
    id bigint,
    switch_auth_id integer
)

TABLESPACE pg_default;

ALTER TABLE  public.swconffixup
    OWNER to postgres;


CREATE TABLE  public.temp_photo
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.temp_photo
    OWNER to cen1001;


CREATE TABLE  public.temp_photo2
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.temp_photo2
    OWNER to cen1001;

CREATE TABLE  public.temp_recover
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.temp_recover
    OWNER to cen1001;

CREATE TABLE  public.temp_recover_si
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.temp_recover_si
    OWNER to cen1001;

CREATE TABLE  public.tempaa
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.tempaa
    OWNER to cen1001;

CREATE TABLE  public.tempbb
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.tempbb
    OWNER to cen1001;

CREATE TABLE  public.ticket_95243
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.ticket_95243
    OWNER to cen1001;

CREATE TABLE  public.tmp_lastseen
(
    mac macaddr,
    stamp timestamp without time zone
)

TABLESPACE pg_default;

ALTER TABLE  public.tmp_lastseen
    OWNER to slony;

CREATE TABLE  public.tmp_mac_to_switch
(
    host_mac macaddr,
    switch_ip inet
)

TABLESPACE pg_default;

ALTER TABLE  public.tmp_mac_to_switch
    OWNER to postgres;
