DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = 'slony') THEN
          CREATE ROLE slony WITH LOGIN SUPERUSER REPLICATION VALID UNTIL 'infinity';
   END IF;
END
$do$;

