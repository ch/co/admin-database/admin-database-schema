CREATE VIEW _chem_290_mailinglist AS
    SELECT email_address
    FROM public.fn_standard_rg_mailing_list('chem-290','Zhang')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-290','Willis')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-290','Barker')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-290','Jackson')
    UNION
        SELECT email_address
        FROM public.fn_standard_rg_mailing_list('chem-290','Sanchez')
;

ALTER VIEW _chem_290_mailinglist OWNER TO dev;
GRANT SELECT ON _chem_290_mailinglist TO mailinglists,www_sites,mailinglist_readers,leavers_trigger;
