-- tables for mifare signin groups
CREATE TABLE public.signin_system_group (
    signin_system_group_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name TEXT UNIQUE NOT NULL
);

ALTER TABLE public.signin_system_group OWNER TO dev;

-- This is set up as a many-many join to allow  us to easily move from a many-one relationship
-- now to a many-many relationship in future
CREATE TABLE public.mm_person_signin_system_group (
    person_id BIGINT NOT NULL REFERENCES person(id),
    signin_system_group_id BIGINT NOT NULL REFERENCES signin_system_group(signin_system_group_id),
    PRIMARY KEY (person_id, signin_system_group_id)
);

-- For now, one group per person
CREATE UNIQUE INDEX one_signin_group_per_person ON mm_person_signin_system_group(person_id);

ALTER TABLE public.mm_person_signin_system_group OWNER TO dev;

-- start with the cleaners group
INSERT INTO public.signin_system_group ( name ) VALUES ( 'Cleaners' );

-- hotwire views to edit it

CREATE VIEW hotwire3.signin_system_group_hid AS
SELECT signin_system_group_id,
    name AS signin_system_group_hid
FROM public.signin_system_group;

ALTER VIEW hotwire3.signin_system_group_hid OWNER TO dev;
GRANT SELECT ON hotwire3.signin_system_group_hid TO PUBLIC;

CREATE VIEW hotwire3."10_View/Sign_In_System/Group_members" AS
SELECT person_id AS id,
       person_id,
       signin_system_group_id
FROM public.mm_person_signin_system_group;

ALTER VIEW hotwire3."10_View/Sign_In_System/Group_members" OWNER TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON hotwire3."10_View/Sign_In_System/Group_members" TO cos, reception;

CREATE VIEW hotwire3."10_View/Sign_In_System/Groups" AS
SELECT signin_system_group_id as id,
       name
FROM public.signin_system_group;

ALTER VIEW hotwire3."10_View/Sign_In_System/Groups" OWNER TO dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON hotwire3."10_View/Sign_In_System/Groups" TO cos;

-- for the CSV generating app
CREATE VIEW apps.signin_system_grouped_cards AS
SELECT row_number() OVER () AS id,
    COALESCE(COALESCE(COALESCE(p.known_as, p.first_names)::text || ' '::text, ''::text) || p.surname::text, n.lookup_visible_name, c.full_name, c.crsid) AS "Name",
    COALESCE(p.email_address, n.lookup_email::character varying, lower(c.crsid)::character varying) AS "Email",
    NULL::text AS "Mobile",
    NULL::text AS "Role",
    mifare_id_to_signin(c.mifare_id) AS "RFID",
    COALESCE(signin_system_group.name,'Other') AS group_name
   FROM university_card c
     LEFT JOIN person p ON lower(p.crsid::text) = lower(c.crsid)
     LEFT JOIN university_cardholder_detail n ON n.crsid = lower(c.crsid)
     LEFT JOIN public.mm_person_signin_system_group ON p.id = mm_person_signin_system_group.person_id
     LEFT JOIN public.signin_system_group USING (signin_system_group_id)
  WHERE c.expires_at > CURRENT_TIMESTAMP AND c.crsid IS NOT NULL AND c.mifare_id IS NOT NULL;
;

ALTER VIEW apps.signin_system_grouped_cards OWNER TO dev;
GRANT SELECT ON apps.signin_system_grouped_cards TO cos,reception;
