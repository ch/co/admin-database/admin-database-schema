CREATE OR REPLACE VIEW hotwire3."10_View/Groups/Research_Groups_Computing"
 AS
 SELECT research_group.id,
    research_group.name,
    research_group.head_of_group_id,
    research_group.deputy_head_of_group_id,
    research_group.administrator_id AS alternate_admin_contact_id,
    research_group.website_address,
    research_group.internal_use_only AS hide_from_websites,
    research_group.ignore_for_software_compliance_purposes,
    research_group.group_fileserver_id,
    research_group.active_directory_container,
    research_group.fai_class_id,
    array_to_string(ARRAY( SELECT person_hid.person_hid
           FROM mm_person_research_group
             JOIN _physical_status_v3 member_status USING (person_id)
             JOIN hotwire3.person_hid USING (person_id)
          WHERE mm_person_research_group.research_group_id = research_group.id AND member_status.status_id::text = 'Current'::text), ';'::text) AS ro_list_of_members_who_are_current,
    ARRAY( SELECT mm_person_research_group.person_id
           FROM mm_person_research_group
          WHERE mm_person_research_group.research_group_id = research_group.id) AS member_id,
    ARRAY( SELECT mm_research_group_computer_rep.computer_rep_id
           FROM mm_research_group_computer_rep
          WHERE mm_research_group_computer_rep.research_group_id = research_group.id) AS computer_rep_id,
        CASE
            WHEN is_research_group_active(id) THEN 'Active'::character varying
            ELSE 'Inactive'::character varying
        END AS ro_group_status,
        CASE
            WHEN is_research_group_active(id) THEN NULL::text
            ELSE 'orange'::text
        END AS _cssclass
   FROM research_group
   ORDER BY research_group.name;
