 ALTER TABLE public.architectures OWNER TO dev;
 GRANT ALL ON public.architectures TO postgres;
 ALTER SEQUENCE public.architectures_id_seq OWNER TO dev;
 GRANT ALL ON public.architectures_id_seq TO postgres;
 ALTER SEQUENCE public.architectures_id_seq OWNED BY public.architectures.id;

 ALTER SEQUENCE public.dns_cnames_seq OWNED BY public.dns_cnames.id;

 ALTER TABLE public.dns_anames OWNER TO dev;
 GRANT ALL ON public.dns_anames TO postgres;
 ALTER SEQUENCE public.dns_anames_id_seq OWNER TO dev;
 GRANT ALL ON public.dns_anames_id_seq TO postgres;
 ALTER SEQUENCE public.dns_anames_id_seq OWNED BY public.dns_anames.id;

 ALTER TABLE public.dns_mxrecords OWNER TO dev;
 GRANT ALL ON public.dns_mxrecords TO postgres;
 ALTER SEQUENCE public.dns_mxrecords_id_seq OWNER TO dev;
 GRANT ALL ON public.dns_mxrecords_id_seq TO postgres;
 ALTER SEQUENCE public.dns_mxrecords_id_seq OWNED BY public.dns_mxrecords.id;

 ALTER SEQUENCE public.hardware_id_seq OWNER TO dev;                                                                                                    
 ALTER SEQUENCE public.hardware_id_seq OWNED BY public.hardware.id;                                                                                        

 ALTER SEQUENCE public.hardwaretype_id_seq OWNED BY public.hardware_type.id;                                                                                        
 
 ALTER SEQUENCE public.ip_address_id_seq OWNER TO dev;                                                                                                  
 ALTER SEQUENCE public.ip_address_id_seq OWNED BY public.ip_address.id;                                                                                    
 GRANT USAGE ON SEQUENCE public.ip_address_id_seq TO cos;
 
 ALTER SEQUENCE public.subnet_id_seq OWNER TO dev;                                                                                                      
 ALTER SEQUENCE public.subnet_id_seq OWNED BY public.subnet.id;                                                                                            
 
 ALTER SEQUENCE public.switch_id_seq OWNER TO dev;                                                                                                      
 ALTER SEQUENCE public.switch_id_seq OWNED BY public.switch.id;                                                                                            
 
 ALTER SEQUENCE public.switchport_id_seq OWNER TO dev;                                                                                                  
 ALTER SEQUENCE public.switchport_id_seq OWNED BY public.switchport.id;                                                                                    
 
 ALTER SEQUENCE public.system_image_id_seq OWNER TO dev;                                                                                                
 ALTER SEQUENCE public.system_image_id_seq OWNED BY public.system_image.id;                                                                                
 
 ALTER SEQUENCE public.cabinet_id_seq OWNER TO dev;                                                                                                     
 ALTER SEQUENCE public.cabinet_id_seq OWNED BY public.cabinet.id;                                                                                          
 
 ALTER TABLE public.socket OWNER TO dev;
 ALTER SEQUENCE public.socket_id_seq OWNER TO dev;                                                                                                      
 ALTER SEQUENCE public.socket_id_seq OWNED BY public.socket.id;                                                                                            
 
 ALTER SEQUENCE public.person_id_seq OWNER TO dev;                                                                                                      
 ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;                                                                                            
 
 ALTER SEQUENCE public.operatingsystem_id_seq OWNER TO dev;                                                                                             
 ALTER SEQUENCE public.operatingsystem_id_seq OWNED BY public.operating_system.id;                                                                          
 
 ALTER SEQUENCE public.user_account_id_seq OWNER TO dev;                                                                                                
 ALTER SEQUENCE public.user_account_id_seq OWNED BY public.user_account.id;                                                                                
 
 ALTER SEQUENCE public.unidentified_software_licence_compliance_declaration_id_seq OWNER TO dev;                                                        
 ALTER SEQUENCE public.unidentified_software_licence_compliance_declaration_id_seq OWNED BY public.unidentified_software_licence_compliance_declaration.id;
 
 ALTER TABLE public.staff_review_meeting OWNER TO dev;
 ALTER SEQUENCE public.staff_review_id_seq OWNER TO dev;                                                                                                
 ALTER SEQUENCE public.staff_review_id_seq OWNED BY public.staff_review_meeting.id;                                                                                
 
 ALTER TABLE public.dept_telephone_number OWNER TO dev;
 ALTER SEQUENCE public.dept_telephone_number_id_seq OWNER TO dev;                                                                                       
 ALTER SEQUENCE public.dept_telephone_number_id_seq OWNED BY public.dept_telephone_number.id;                                                              
 
 ALTER SEQUENCE public.mailinglist_id_seq OWNER TO dev;                                                                                                 
 ALTER SEQUENCE public.mailinglist_id_seq OWNED BY public.mailinglist.id;                                                                                  
 
 ALTER TABLE public.building_floor_hid OWNER TO dev;
 ALTER SEQUENCE public.building_floor_hid_building_floor_id_seq OWNER TO dev;                                                                           
 ALTER SEQUENCE public.building_floor_hid_building_floor_id_seq OWNED BY public.building_floor_hid.building_floor_id;                                      
 

 ALTER TABLE public.building_region_hid OWNER TO dev;
 ALTER SEQUENCE public.building_region_hid_building_region_id_seq OWNER TO dev;                                                                         
 ALTER SEQUENCE public.building_region_hid_building_region_id_seq OWNED BY public.building_region_hid.building_region_id;                                  
 

 ALTER TABLE public.cambridge_college OWNER TO dev;
 ALTER SEQUENCE public.cambridge_college_id_seq OWNER TO dev;                                                                                           
 ALTER SEQUENCE public.cambridge_college_id_seq OWNED BY public.cambridge_college.id;                                                                      
 
 ALTER TABLE public.erasmus_socrates_studentship OWNER TO dev;
 ALTER SEQUENCE public.erasmus_socrates_studentship_id_seq OWNER TO dev;                                                                                
 ALTER SEQUENCE public.erasmus_socrates_studentship_id_seq OWNED BY public.erasmus_socrates_studentship.id;                                                
 

 ALTER TABLE public.firstaider_funding OWNER TO dev;
 ALTER SEQUENCE public.firstaider_funding_id_seq OWNER TO dev;                                                                                          
 ALTER SEQUENCE public.firstaider_funding_id_seq OWNED BY public.firstaider_funding.id;                                                                    
 
 
 ALTER TABLE public.gender_hid OWNER TO dev;
 ALTER SEQUENCE public.gender_hid_gender_id_seq OWNER TO dev;                                                                                           
 ALTER SEQUENCE public.gender_hid_gender_id_seq OWNED BY public.gender_hid.gender_id;                                                                      
 
 ALTER TABLE public.group_fileserver OWNER TO dev;
 ALTER SEQUENCE public.group_fileserver_id_seq OWNER TO dev;                                                                                            
 ALTER SEQUENCE public.group_fileserver_id_seq OWNED BY public.group_fileserver.id;                                                                        
 
 
 ALTER TABLE public.licence OWNER TO dev;
 ALTER SEQUENCE public.licence_id_seq OWNER TO dev;                                                                                                     
 ALTER SEQUENCE public.licence_id_seq OWNED BY public.licence.id;                                                                                          
 
 ALTER TABLE public.licence_instance OWNER TO dev;
 ALTER SEQUENCE public.licence_instance_id_seq OWNER TO dev;                                                                                            
 ALTER SEQUENCE public.licence_instance_id_seq OWNED BY public.licence_instance.id;                                                                        
 
 
 ALTER TABLE public.mentor_meeting OWNER TO dev;
 ALTER SEQUENCE public.mentor_meeting_id_seq OWNER TO dev;                                                                                              
 ALTER SEQUENCE public.mentor_meeting_id_seq OWNED BY public.mentor_meeting.id;                                                                            
 
 
 ALTER TABLE public.paper_licence_certificate OWNER TO dev;
 ALTER SEQUENCE public.paper_licence_certificate_id_seq OWNER TO dev;                                                                                   
 ALTER SEQUENCE public.paper_licence_certificate_id_seq OWNED BY public.paper_licence_certificate.id;                                                      
 
 ALTER TABLE public.part_iii_studentship OWNER TO dev; 
 ALTER SEQUENCE public.part_iii_studentship_id_seq OWNER TO dev;                                                                                        
 ALTER SEQUENCE public.part_iii_studentship_id_seq OWNED BY public.part_iii_studentship.id;                                                                
 
 ALTER TABLE public.postgraduate_studentship OWNER TO dev; 
 ALTER SEQUENCE public.postgraduate_studentship_id_seq OWNER TO dev;                                                                                    
 ALTER SEQUENCE public.postgraduate_studentship_id_seq OWNED BY public.postgraduate_studentship.id;                                                        
 
 ALTER TABLE public.postgraduate_studentship_type OWNER TO dev; 
 ALTER SEQUENCE public.postgraduate_studentship_type_id_seq OWNER TO dev;                                                                               
 ALTER SEQUENCE public.postgraduate_studentship_type_id_seq OWNED BY public.postgraduate_studentship_type.id;                                              
 
 ALTER TABLE public.reboot_hid OWNER TO dev;
 ALTER SEQUENCE public.reboot_hid_reboot_id_seq OWNER TO dev;                                                                                           
 ALTER SEQUENCE public.reboot_hid_reboot_id_seq OWNED BY public.reboot_hid.reboot_id;                                                                      
 
 ALTER TABLE public.room_type OWNER TO dev;
 ALTER SEQUENCE public.room_type_id_seq OWNER TO dev;                                                                                                   
 ALTER SEQUENCE public.room_type_id_seq OWNED BY public.room_type.id;                                                                                      
 
 
 ALTER TABLE public.software_package OWNER TO dev;
 ALTER SEQUENCE public.software_package_id_seq OWNER TO dev;                                                                                            
 ALTER SEQUENCE public.software_package_id_seq OWNED BY public.software_package.id;                                                                        
 
 ALTER TABLE public.staff_category OWNER TO dev;
 ALTER SEQUENCE public.staff_category_id_seq OWNER TO dev;                                                                                              
 ALTER SEQUENCE public.staff_category_id_seq OWNED BY public.staff_category.id;                                                                            
 
 
 ALTER TABLE public.unixgroup OWNER TO dev;
 ALTER SEQUENCE public.unixgroup_id_seq OWNER TO dev;                                                                                                   
 ALTER SEQUENCE public.unixgroup_id_seq OWNED BY public.unixgroup.id;                                                                                      
 
 ALTER TABLE public.visitorship OWNER TO dev;
 ALTER SEQUENCE public.visitorship_id_seq OWNER TO dev;                                                                                                 
 ALTER SEQUENCE public.visitorship_id_seq OWNED BY public.visitorship.id;                                                                                  
 

