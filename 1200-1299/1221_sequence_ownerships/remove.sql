 ALTER SEQUENCE public.architectures_id_seq OWNED BY NONE;
 ALTER TABLE public.architectures OWNER TO postgres;
 GRANT ALL ON public.architectures TO dev;
 ALTER SEQUENCE public.architectures_id_seq OWNER TO postgres;
 GRANT ALL ON public.architectures_id_seq TO dev;

 ALTER SEQUENCE public.dns_cnames_seq OWNED BY NONE;

 ALTER SEQUENCE public.dns_anames_id_seq OWNED BY NONE;
 ALTER TABLE public.dns_anames OWNER TO postgres;
 GRANT ALL ON public.dns_anames TO dev;
 ALTER SEQUENCE public.dns_anames_id_seq OWNER TO postgres;
 GRANT ALL ON public.dns_anames_id_seq TO dev;

 ALTER SEQUENCE public.dns_mxrecords_id_seq OWNED BY NONE;
 ALTER TABLE public.dns_mxrecords OWNER TO postgres;
 GRANT ALL ON public.dns_mxrecords TO dev;
 ALTER SEQUENCE public.dns_mxrecords_id_seq OWNER TO postgres;
 GRANT ALL ON public.dns_mxrecords_id_seq TO dev;

 ALTER SEQUENCE public.hardware_id_seq OWNED BY NONE;                                                  
 ALTER SEQUENCE public.hardware_id_seq OWNER TO cen1001;                                            
 GRANT ALL ON public.hardware_id_seq TO dev;                                                         

 ALTER SEQUENCE public.hardwaretype_id_seq OWNED BY NONE;                                                  
 
 ALTER SEQUENCE public.ip_address_id_seq OWNED BY NONE;                                                
 ALTER SEQUENCE public.ip_address_id_seq OWNER TO cen1001;                                          
 GRANT ALL ON public.ip_address_id_seq TO dev;                                                       
 REVOKE USAGE ON public.ip_address_id_seq FROM cos;
 
 ALTER SEQUENCE public.subnet_id_seq OWNED BY NONE;                                                    
 ALTER SEQUENCE public.subnet_id_seq OWNER TO cen1001;                                              
 GRANT ALL ON public.subnet_id_seq TO dev;                                                           
 
 ALTER SEQUENCE public.switch_id_seq OWNED BY NONE;                                                    
 ALTER SEQUENCE public.switch_id_seq OWNER TO cen1001;                                              
 GRANT ALL ON public.switch_id_seq TO dev;                                                           
 
 ALTER SEQUENCE public.switchport_id_seq OWNED BY NONE;                                                
 ALTER SEQUENCE public.switchport_id_seq OWNER TO cen1001;                                          
 GRANT ALL ON public.switchport_id_seq TO dev;                                                       
 
 ALTER SEQUENCE public.system_image_id_seq OWNED BY NONE;                                              
 ALTER SEQUENCE public.system_image_id_seq OWNER TO cen1001;                                        
 GRANT ALL ON public.system_image_id_seq TO dev;                                                     
 
 ALTER SEQUENCE public.cabinet_id_seq OWNED BY NONE;                                                   
 ALTER SEQUENCE public.cabinet_id_seq OWNER TO cen1001;                                             
 GRANT ALL ON public.cabinet_id_seq TO dev;                                                          
 
 ALTER SEQUENCE public.socket_id_seq OWNED BY NONE;                                                    
 ALTER SEQUENCE public.socket_id_seq OWNER TO cen1001;                                              
 GRANT ALL ON public.socket_id_seq TO dev;                                                           
 ALTER TABLE public.socket OWNER TO cen1001;
 GRANT ALL ON TABLE public.socket TO dev;
 
 ALTER SEQUENCE public.person_id_seq OWNED BY NONE;                                                    
 ALTER SEQUENCE public.person_id_seq OWNER TO cen1001;                                              
 GRANT ALL ON public.person_id_seq TO dev;                                                           
 
 ALTER SEQUENCE public.operatingsystem_id_seq OWNED BY NONE;                                           
 ALTER SEQUENCE public.operatingsystem_id_seq OWNER TO cen1001;                                     
 GRANT ALL ON public.operatingsystem_id_seq TO dev;                                                  
 
 ALTER SEQUENCE public.user_account_id_seq OWNED BY NONE;                                              
 ALTER SEQUENCE public.user_account_id_seq OWNER TO cen1001;                                        
 GRANT ALL ON public.user_account_id_seq TO dev;                                                     
 
 ALTER SEQUENCE public.unidentified_software_licence_compliance_declaration_id_seq OWNED BY NONE;      
 ALTER SEQUENCE public.unidentified_software_licence_compliance_declaration_id_seq OWNER TO cen1001;
 GRANT ALL ON public.unidentified_software_licence_compliance_declaration_id_seq TO dev;             
 
 ALTER SEQUENCE public.staff_review_id_seq OWNED BY NONE;                                              
 ALTER SEQUENCE public.staff_review_id_seq OWNER TO cen1001;                                        
 GRANT ALL ON public.staff_review_id_seq TO dev;                                                     
 ALTER TABLE public.staff_review_meeting OWNER TO cen1001;
 GRANT ALL ON public.staff_review_meeting TO dev;
 
 ALTER SEQUENCE public.dept_telephone_number_id_seq OWNED BY NONE;                                     
 ALTER SEQUENCE public.dept_telephone_number_id_seq OWNER TO cen1001;                               
 GRANT ALL ON public.dept_telephone_number_id_seq TO dev;                                            
 ALTER TABLE public.dept_telephone_number OWNER TO cen1001;
 GRANT ALL ON public.dept_telephone_number TO dev;
 
 ALTER SEQUENCE public.mailinglist_id_seq OWNED BY NONE;                                               
 ALTER SEQUENCE public.mailinglist_id_seq OWNER TO cen1001;                                         
 GRANT ALL ON public.mailinglist_id_seq TO dev;                                                      
 
 ALTER SEQUENCE public.building_floor_hid_building_floor_id_seq OWNED BY NONE;
 ALTER SEQUENCE public.building_floor_hid_building_floor_id_seq OWNER TO cen1001;                   
 GRANT ALL ON public.building_floor_hid_building_floor_id_seq TO dev;                                
 ALTER TABLE public.building_floor_hid OWNER TO cen1001;
 GRANT ALL ON TABLE public.building_floor_hid TO dev;
 ALTER SEQUENCE public.building_floor_hid_building_floor_id_seq OWNED BY public.building_floor_hid.building_floor_id;                                      
 
 ALTER SEQUENCE public.building_region_hid_building_region_id_seq OWNED BY NONE;                       
 ALTER TABLE public.building_region_hid OWNER TO cen1001;
 ALTER SEQUENCE public.building_region_hid_building_region_id_seq OWNER TO cen1001;                 
 GRANT ALL ON public.building_region_hid_building_region_id_seq TO dev;                              
 GRANT ALL ON TABLE public.building_region_hid TO dev;
 
 ALTER SEQUENCE public.cambridge_college_id_seq OWNED BY NONE;                                         
 ALTER SEQUENCE public.cambridge_college_id_seq OWNER TO cen1001;                                   
 GRANT ALL ON public.cambridge_college_id_seq TO dev;                                                
 ALTER TABLE public.cambridge_college OWNER TO cen1001;
 GRANT ALL ON TABLE public.cambridge_college TO dev;
 
 ALTER SEQUENCE public.erasmus_socrates_studentship_id_seq OWNED BY NONE;                              
 ALTER SEQUENCE public.erasmus_socrates_studentship_id_seq OWNER TO cen1001;                        
 GRANT ALL ON public.erasmus_socrates_studentship_id_seq TO dev;                                     
 ALTER TABLE public.erasmus_socrates_studentship OWNER TO cen1001;
 GRANT ALL ON TABLE public.erasmus_socrates_studentship TO dev;

 
 ALTER SEQUENCE public.firstaider_funding_id_seq OWNED BY NONE;                                        
 ALTER SEQUENCE public.firstaider_funding_id_seq OWNER TO cen1001;                                  
 GRANT ALL ON public.firstaider_funding_id_seq TO dev;                                               
 ALTER TABLE public.firstaider_funding OWNER TO cen1001;
 GRANT ALL ON TABLE public.firstaider_funding TO dev;
 
 ALTER SEQUENCE public.gender_hid_gender_id_seq OWNED BY NONE;                                         
 ALTER SEQUENCE public.gender_hid_gender_id_seq OWNER TO cen1001;                                   
 GRANT ALL ON public.gender_hid_gender_id_seq TO dev;                                                
 ALTER TABLE public.gender_hid OWNER TO cen1001;
 GRANT ALL ON TABLE public.gender_hid TO dev;
 
 ALTER SEQUENCE public.group_fileserver_id_seq OWNED BY NONE;                                          
 ALTER SEQUENCE public.group_fileserver_id_seq OWNER TO cen1001;                                    
 GRANT ALL ON public.group_fileserver_id_seq TO dev;                                                 
 ALTER TABLE public.group_fileserver OWNER TO cen1001;
 GRANT ALL ON TABLE public.group_fileserver TO dev;
 
 ALTER SEQUENCE public.licence_id_seq OWNED BY NONE;                                                   
 ALTER SEQUENCE public.licence_id_seq OWNER TO cen1001;                                             
 GRANT ALL ON public.licence_id_seq TO dev;                                                          
 ALTER TABLE public.licence OWNER TO cen1001;
 GRANT ALL ON TABLE public.licence TO dev;
 
 ALTER SEQUENCE public.licence_instance_id_seq OWNED BY NONE;                                          
 ALTER SEQUENCE public.licence_instance_id_seq OWNER TO cen1001;                                    
 GRANT ALL ON public.licence_instance_id_seq TO dev;                                                 
 ALTER TABLE public.licence_instance OWNER TO cen1001;
 GRANT ALL ON TABLE public.licence_instance TO dev;
 
 ALTER SEQUENCE public.mentor_meeting_id_seq OWNED BY NONE;                                            
 ALTER SEQUENCE public.mentor_meeting_id_seq OWNER TO cen1001;                                      
 GRANT ALL ON public.mentor_meeting_id_seq TO dev;                                                   
 ALTER TABLE public.mentor_meeting OWNER TO postgres;
 
 ALTER SEQUENCE public.paper_licence_certificate_id_seq OWNED BY NONE;                                 
 ALTER SEQUENCE public.paper_licence_certificate_id_seq OWNER TO cen1001;                           
 GRANT ALL ON public.paper_licence_certificate_id_seq TO dev;                                        
 ALTER TABLE public.paper_licence_certificate OWNER TO cen1001;
 GRANT ALL ON TABLE public.paper_licence_certificate TO dev;
 
 ALTER SEQUENCE public.part_iii_studentship_id_seq OWNED BY NONE;                                      
 ALTER SEQUENCE public.part_iii_studentship_id_seq OWNER TO cen1001;                                
 GRANT ALL ON public.part_iii_studentship_id_seq TO dev;                                             
 ALTER TABLE public.part_iii_studentship OWNER TO cen1001;
 GRANT ALL ON TABLE public.part_iii_studentship TO dev;
 
 ALTER SEQUENCE public.postgraduate_studentship_id_seq OWNED BY NONE;                                  
 ALTER SEQUENCE public.postgraduate_studentship_id_seq OWNER TO cen1001;                            
 GRANT ALL ON public.postgraduate_studentship_id_seq TO dev;                                         
 ALTER TABLE public.postgraduate_studentship OWNER TO cen1001;
 GRANT ALL ON TABLE public.postgraduate_studentship TO dev;
 
 ALTER SEQUENCE public.postgraduate_studentship_type_id_seq OWNED BY NONE;                             
 ALTER SEQUENCE public.postgraduate_studentship_type_id_seq OWNER TO cen1001;                       
 GRANT ALL ON public.postgraduate_studentship_type_id_seq TO dev;                                    
 ALTER TABLE public.postgraduate_studentship_type OWNER TO cen1001;
 GRANT ALL ON TABLE public.postgraduate_studentship_type TO dev;
 
 ALTER SEQUENCE public.reboot_hid_reboot_id_seq OWNED BY NONE;                                         
 ALTER SEQUENCE public.reboot_hid_reboot_id_seq OWNER TO cen1001;                                   
 GRANT ALL ON public.reboot_hid_reboot_id_seq TO dev;                                                
 ALTER TABLE public.reboot_hid OWNER TO cen1001;
 GRANT ALL ON TABLE public.reboot_hid TO dev;
 
 ALTER SEQUENCE public.room_type_id_seq OWNED BY NONE;                                                 
 ALTER SEQUENCE public.room_type_id_seq OWNER TO cen1001;                                           
 GRANT ALL ON public.room_type_id_seq TO dev;                                                        
 ALTER TABLE public.room_type OWNER TO cen1001;
 GRANT ALL ON TABLE public.room_type TO dev;
 
 ALTER SEQUENCE public.software_package_id_seq OWNED BY NONE;                                          
 ALTER SEQUENCE public.software_package_id_seq OWNER TO cen1001;                                    
 GRANT ALL ON public.software_package_id_seq TO dev;                                                 
 ALTER TABLE public.software_package OWNER TO cen1001;
 GRANT ALL ON TABLE public.software_package TO dev;
 
 ALTER SEQUENCE public.staff_category_id_seq OWNED BY NONE;                                            
 ALTER SEQUENCE public.staff_category_id_seq OWNER TO cen1001;                                      
 GRANT ALL ON public.staff_category_id_seq TO dev;                                                   
 ALTER TABLE public.staff_category OWNER TO cen1001;
 GRANT ALL ON TABLE public.staff_category TO dev;
 
 ALTER SEQUENCE public.unixgroup_id_seq OWNED BY NONE;                                                 
 ALTER SEQUENCE public.unixgroup_id_seq OWNER TO cen1001;                                           
 GRANT ALL ON public.unixgroup_id_seq TO dev;                                                        
 ALTER TABLE public.unixgroup OWNER TO cen1001;
 GRANT ALL ON TABLE public.unixgroup TO dev;
 
 ALTER SEQUENCE public.visitorship_id_seq OWNED BY NONE;                                               
 ALTER SEQUENCE public.visitorship_id_seq OWNER TO cen1001;                                         
 GRANT ALL ON public.visitorship_id_seq TO dev;                                                      
 ALTER TABLE public.visitorship OWNER TO cen1001;
 GRANT ALL ON TABLE public.visitorship TO dev;
 

