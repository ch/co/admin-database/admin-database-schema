CREATE OR REPLACE FUNCTION bigendian_to_littleendian(hex_string TEXT)
RETURNS TEXT AS $$
DECLARE
    bin_data BYTEA;
    bin_length INT;
    reversed_bin BYTEA := '';
    i INT;
BEGIN
    -- Convert the hexadecimal string to binary
    bin_data := decode(hex_string, 'hex');

    bin_length := length(bin_data);

    -- Reverse the byte order
    FOR i IN 0..(bin_length - 1) LOOP
        reversed_bin := reversed_bin || substr(bin_data, bin_length - i, 1);
    END LOOP;

    -- Convert the reversed binary data back to a hexadecimal string
    RETURN encode(reversed_bin, 'hex');
END;
$$ IMMUTABLE LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION mifare_id_to_signin(mifare_id BIGINT)
RETURNS TEXT AS $$
BEGIN
    RETURN upper(bigendian_to_littleendian((lpad(to_hex(mifare_id),8,'0'))));
END;
$$ IMMUTABLE LANGUAGE plpgsql;

