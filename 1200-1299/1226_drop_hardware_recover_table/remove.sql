CREATE TABLE  public.hardware_recover
(
    operation character varying COLLATE pg_catalog."default",
    stamp timestamp without time zone,
    username text COLLATE pg_catalog."default",
    tablename text COLLATE pg_catalog."default",
    oldcols text COLLATE pg_catalog."default",
    newcols text COLLATE pg_catalog."default",
    id bigint
)

TABLESPACE pg_default;

ALTER TABLE  public.hardware_recover
    OWNER to cen1001;
