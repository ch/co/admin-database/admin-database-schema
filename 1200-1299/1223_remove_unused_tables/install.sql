DROP TABLE public.dhcpsettings;
DROP SEQUENCE public.dhcpsettings_id_seq;

DROP TABLE public.ldap_attr_mappings;

DROP TABLE public.ldap_entry_objclasses;

DROP TABLE public.ldap_oc_mappings;

DROP TABLE public.parking;
DROP SEQUENCE public.parking_id_seq;

DROP TABLE public.university_term_hid;
