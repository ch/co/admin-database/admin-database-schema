CREATE SEQUENCE public.dhcpsettings_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dhcpsettings_id_seq
    OWNER TO cen1001;

GRANT ALL ON SEQUENCE public.dhcpsettings_id_seq TO cen1001;

GRANT ALL ON SEQUENCE public.dhcpsettings_id_seq TO dev;

CREATE TABLE public.dhcpsettings
(
    id bigint NOT NULL DEFAULT nextval('dhcpsettings_id_seq'::regclass),
    description character varying(500) COLLATE pg_catalog."default",
    wins1 inet,
    wins2 inet,
    bootserver character varying(255) COLLATE pg_catalog."default",
    bootfilename character varying(255) COLLATE pg_catalog."default",
    domainname character varying(255) COLLATE pg_catalog."default",
    leasetime integer,
    dns1 inet,
    dns2 inet,
    dns3 inet,
    CONSTRAINT pk_dhcpsettings PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.dhcpsettings
    OWNER to cen1001;

GRANT ALL ON TABLE public.dhcpsettings TO cen1001;

GRANT ALL ON TABLE public.dhcpsettings TO dev;

COMMENT ON COLUMN public.dhcpsettings.bootfilename
    IS 'The length of this is a conservative guess';

COMMENT ON COLUMN public.dhcpsettings.domainname
    IS 'differs from the one in the subnet as the AD requires we feed some machines a domain name that doesn''t match their ip, eg ucc.cam.ac.uk';

COMMENT ON COLUMN public.dhcpsettings.leasetime
    IS 'seconds, one presumes';

GRANT ALL ON SEQUENCE public.dhcpsettings_id_seq TO dev;


CREATE TABLE public.ldap_entry_objclasses
(
    entry_id integer NOT NULL,
    oc_name character varying(64) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE public.ldap_entry_objclasses
    OWNER to cen1001;

REVOKE ALL ON TABLE public.ldap_entry_objclasses FROM addressbook;

GRANT SELECT ON TABLE public.ldap_entry_objclasses TO addressbook;

GRANT ALL ON TABLE public.ldap_entry_objclasses TO cen1001;


CREATE SEQUENCE public.ldap_oc_mappings_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.ldap_oc_mappings_id_seq
    OWNER TO cen1001;

GRANT ALL ON SEQUENCE public.ldap_oc_mappings_id_seq TO cen1001;

GRANT ALL ON SEQUENCE public.ldap_oc_mappings_id_seq TO dev;

CREATE TABLE public.ldap_oc_mappings
(
    id integer NOT NULL DEFAULT nextval('ldap_oc_mappings_id_seq'::regclass),
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    keytbl character varying(64) COLLATE pg_catalog."default" NOT NULL,
    keycol character varying(64) COLLATE pg_catalog."default" NOT NULL,
    create_proc character varying(255) COLLATE pg_catalog."default",
    delete_proc character varying(255) COLLATE pg_catalog."default",
    expect_return integer NOT NULL,
    CONSTRAINT ldap_oc_mappings_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.ldap_oc_mappings
    OWNER to cen1001;

REVOKE ALL ON TABLE public.ldap_oc_mappings FROM addressbook;

GRANT SELECT ON TABLE public.ldap_oc_mappings TO addressbook;

GRANT ALL ON TABLE public.ldap_oc_mappings TO cen1001;

ALTER SEQUENCE public.ldap_oc_mappings_id_seq
    OWNED BY public.ldap_oc_mappings.id;


CREATE SEQUENCE public.ldap_attr_mappings_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.ldap_attr_mappings_id_seq
    OWNER TO cen1001;

GRANT ALL ON SEQUENCE public.ldap_attr_mappings_id_seq TO cen1001;

GRANT ALL ON SEQUENCE public.ldap_attr_mappings_id_seq TO dev;

CREATE TABLE public.ldap_attr_mappings
(
    id integer NOT NULL DEFAULT nextval('ldap_attr_mappings_id_seq'::regclass),
    oc_map_id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    sel_expr character varying(255) COLLATE pg_catalog."default" NOT NULL,
    sel_expr_u character varying(255) COLLATE pg_catalog."default",
    from_tbls character varying(255) COLLATE pg_catalog."default" NOT NULL,
    join_where character varying(255) COLLATE pg_catalog."default",
    add_proc character varying(255) COLLATE pg_catalog."default",
    delete_proc character varying(255) COLLATE pg_catalog."default",
    param_order integer NOT NULL,
    expect_return integer NOT NULL,
    CONSTRAINT ldap_attr_mappings_pkey PRIMARY KEY (id),
    CONSTRAINT ldap_attr_mappings_oc_map_id_fkey FOREIGN KEY (oc_map_id)
        REFERENCES public.ldap_oc_mappings (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.ldap_attr_mappings
    OWNER to cen1001;

REVOKE ALL ON TABLE public.ldap_attr_mappings FROM addressbook;

GRANT SELECT ON TABLE public.ldap_attr_mappings TO addressbook;

GRANT ALL ON TABLE public.ldap_attr_mappings TO cen1001;

ALTER SEQUENCE public.ldap_attr_mappings_id_seq
    OWNED BY public.ldap_attr_mappings.id;

CREATE SEQUENCE public.parking_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.parking_id_seq
    OWNER TO cen1001;

GRANT ALL ON SEQUENCE public.parking_id_seq TO cen1001;

GRANT ALL ON SEQUENCE public.parking_id_seq TO dev;

CREATE TABLE public.parking
(
    id bigint NOT NULL DEFAULT nextval('parking_id_seq'::regclass),
    car_registration_one character varying(10) COLLATE pg_catalog."default",
    car_registration_two character varying(10) COLLATE pg_catalog."default",
    badge_number character varying(20) COLLATE pg_catalog."default",
    uses_daily boolean DEFAULT false,
    person_id bigint NOT NULL,
    CONSTRAINT parking_pkey PRIMARY KEY (id),
    CONSTRAINT parking_fk_person FOREIGN KEY (person_id)
        REFERENCES public.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.parking
    OWNER to cen1001;


CREATE SEQUENCE public.university_term_hid_university_term_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.university_term_hid_university_term_id_seq
    OWNER TO cen1001;

GRANT ALL ON SEQUENCE public.university_term_hid_university_term_id_seq TO cen1001;

GRANT ALL ON SEQUENCE public.university_term_hid_university_term_id_seq TO dev;

CREATE TABLE public.university_term_hid
(
    university_term_id bigint NOT NULL DEFAULT nextval('university_term_hid_university_term_id_seq'::regclass),
    university_term_hid character varying(20) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT university_term_hid_pkey PRIMARY KEY (university_term_id)
)

TABLESPACE pg_default;

ALTER TABLE public.university_term_hid
    OWNER to cen1001;

REVOKE ALL ON TABLE public.university_term_hid FROM ro_hid;

GRANT ALL ON TABLE public.university_term_hid TO cen1001;

GRANT SELECT ON TABLE public.university_term_hid TO ro_hid;

ALTER SEQUENCE public.university_term_hid_university_term_id_seq
    OWNED BY public.university_term_hid.university_term_id;


INSERT INTO public.university_term_hid ( university_term_hid ) VALUES ( 'Michaelmas' ), ( 'Lent' ), ( 'Easter' );
