CREATE OR REPLACE VIEW apps.signin_system_grouped_cards AS
WITH longest_lasting_current_card_per_person AS (
    SELECT DISTINCT ON (crsid)
        crsid,
        full_name,
        mifare_id,
        expires_at
    FROM public.university_card
    WHERE university_card.expires_at > CURRENT_TIMESTAMP 
        AND university_card.crsid IS NOT NULL
        AND university_card.mifare_id IS NOT NULL
    ORDER BY crsid, expires_at DESC
)
SELECT row_number() OVER () AS id,
    COALESCE(COALESCE(TRIM(COALESCE(p.known_as, p.first_names)::text) || ' '::text, ''::text) || TRIM(p.surname::text), TRIM(n.lookup_visible_name), TRIM(c.full_name), TRIM(c.crsid)) AS "Name",
    TRIM(lower(c.crsid)::character varying || '@cam.ac.uk')::varchar AS "Email",
    NULL::text AS "Mobile",
    NULL::text AS "Role",
    mifare_id_to_signin(c.mifare_id) AS "RFID",
    signin_system_group.name AS group_name
   FROM longest_lasting_current_card_per_person c
     LEFT JOIN person p ON lower(p.crsid::text) = lower(c.crsid)
     LEFT JOIN university_cardholder_detail n ON n.crsid = lower(c.crsid)
     LEFT JOIN public.mm_person_signin_system_group ON p.id = mm_person_signin_system_group.person_id
     LEFT JOIN public.signin_system_group USING (signin_system_group_id)
;
