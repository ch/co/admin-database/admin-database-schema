CREATE OR REPLACE VIEW apps.account_extension_pi_delegate
 AS
 SELECT pi.crsid AS pi_crsid,
    usr.crsid AS delegate_crsid,
    ph.person_hid AS delegate_name
   FROM account_extension_delegates
     RIGHT JOIN person pi ON account_extension_delegates.pi_id = pi.id
     LEFT JOIN person usr ON account_extension_delegates.delegate_id = usr.id
     LEFT JOIN _current_hr_role_for_person lr ON pi.id = lr.person_id
     LEFT JOIN person_hid ph ON account_extension_delegates.delegate_id = ph.person_id
  WHERE lr.predicted_role_status_id::text = 'Current'::text AND (lr.post_category_id = 'sc-1'::text OR lr.post_category_id = 'sc-9'::text OR pi.counts_as_academic = true) AND pi.crsid IS NOT NULL
  UNION
  -- include the IT team for debugging , via the 'cos' role in the database
  SELECT member.rolname::varchar(8) AS pi_crsid,
      NULL::varchar(8) AS delegate_crsid,
      NULL AS delegate_name
  FROM pg_roles cos_role
  JOIN pg_auth_members ON cos_role.oid = pg_auth_members.roleid
  JOIN pg_roles member ON member.oid = pg_auth_members.member
  WHERE cos_role.rolname = 'cos';
