DROP VIEW selfservice."40_Selfservice/Database_Selfservice";

CREATE VIEW selfservice."40_Selfservice/Database_Selfservice"
WITH (
  security_barrier=true
) AS
 SELECT DISTINCT person.id,
    title_hid.abbrev_title AS ro_title,
    person.first_names AS ro_first_names,
    person.surname AS ro_surname,
    person.known_as,
    person.name_suffix AS name_suffix_eg_frs,
    gender_hid.gender_hid AS ro_gender,
    person.previous_surname AS ro_previous_name,
    nationality_string.ro_nationality,
    person.email_address,
    person.hide_email AS hide_email_from_dept_websites,
    ARRAY( SELECT mm.room_id
           FROM mm_person_room mm
          WHERE mm.person_id = person.id) AS room_id,
    ARRAY( SELECT mm.dept_telephone_number_id
           FROM mm_person_dept_telephone_number mm
          WHERE mm.person_id = person.id) AS dept_telephone_number_id,
    mm_person_contact_preference.contact_preference_id,
    mm_person_contact_preference.further_details AS if_contact_preference_is_other_give_details,
    cambridge_college_hid.cambridge_college_hid AS ro_cambridge_college,
    person.cambridge_address::text AS home_address,
    person.cambridge_phone_number AS home_phone_number,
    person.emergency_contact::text AS emergency_contact,
    research_groups_string.ro_research_group,
    post_category_hid.post_category_hid::character varying AS ro_post_category,
    supervisor_hid.supervisor_hid::character varying AS ro_supervisor,
    person.arrival_date AS ro_arrival_date,
    _latest_role.funding_end_date AS ro_funding_end_date,
    _latest_role.estimated_role_end_date AS ro_expected_leaving_date,
    person.crsid AS ro_crsid,
    person.do_not_show_on_website AS hide_from_website,
    person.hide_photo_from_website,
    user_account.chemnet_token AS ro_chemnet_token,
    false AS create_new_token
   FROM person
     LEFT JOIN _current_hr_role_for_person _latest_role ON person.id = _latest_role.person_id
     LEFT JOIN user_account ON user_account.username::text = person.crsid::text
     LEFT JOIN cambridge_college_hid USING (cambridge_college_id)
     LEFT JOIN supervisor_hid USING (supervisor_id)
     LEFT JOIN gender_hid USING (gender_id)
     LEFT JOIN title_hid USING (title_id)
     LEFT JOIN post_category_hid USING (post_category_id)
     LEFT JOIN mm_person_research_group ON mm_person_research_group.person_id = person.id
     LEFT JOIN research_group ON mm_person_research_group.research_group_id = research_group.id
     LEFT JOIN ( SELECT person_1.id AS person_id,
            string_agg(nationality.nationality::text, ', '::text)::character varying AS ro_nationality
           FROM person person_1
             LEFT JOIN mm_person_nationality ON mm_person_nationality.person_id = person_1.id
             LEFT JOIN nationality ON mm_person_nationality.nationality_id = nationality.id
          GROUP BY person_1.id) nationality_string ON nationality_string.person_id = person.id
     LEFT JOIN ( SELECT person_1.id AS person_id,
            string_agg(research_group_1.name::text, ', '::text)::character varying AS ro_research_group
           FROM person person_1
             LEFT JOIN mm_person_research_group mm_person_research_group_1 ON mm_person_research_group_1.person_id = person_1.id
             LEFT JOIN research_group research_group_1 ON mm_person_research_group_1.research_group_id = research_group_1.id
          GROUP BY person_1.id) research_groups_string ON research_groups_string.person_id = person.id
     LEFT JOIN mm_person_contact_preference ON person.id = mm_person_contact_preference.person_id
  WHERE person.crsid::name = "current_user"() AND person.ban_from_self_service <> true
  GROUP BY person.id, title_hid.abbrev_title, gender_hid.gender_hid, cambridge_college_hid.cambridge_college_hid, post_category_hid.post_category_hid, supervisor_hid.supervisor_hid, _latest_role.funding_end_date, _latest_role.estimated_role_end_date, user_account.chemnet_token, nationality_string.ro_nationality, research_groups_string.ro_research_group, mm_person_contact_preference.contact_preference_id, mm_person_contact_preference.further_details;

ALTER TABLE selfservice."40_Selfservice/Database_Selfservice"
    OWNER TO dev;

GRANT SELECT, UPDATE ON TABLE selfservice."40_Selfservice/Database_Selfservice" TO PUBLIC;
GRANT ALL ON TABLE selfservice."40_Selfservice/Database_Selfservice" TO dev;

CREATE OR REPLACE FUNCTION selfservice.selfservice_upd()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF SECURITY DEFINER
    SET search_path=public, pg_temp
AS $BODY$
DECLARE
    _crsid varchar;
BEGIN
    UPDATE person SET
            known_as = NEW.known_as,
            name_suffix = NEW.name_suffix_eg_frs,
            email_address = NEW.email_address,
            hide_email = NEW.hide_email_from_dept_websites,
            cambridge_address = NEW.home_address::varchar(500),
            cambridge_phone_number = NEW.home_phone_number,
            emergency_contact = NEW.emergency_contact::varchar(500),
            do_not_show_on_website = NEW.hide_from_website,
            hide_photo_from_website = NEW.hide_photo_from_website
    WHERE id = OLD.id;
    INSERT INTO public.mm_person_contact_preference
        ( person_id, contact_preference_id, further_details )
        VALUES
        ( OLD.id, NEW.contact_preference_id, NEW.if_contact_preference_is_other_give_details )
        ON CONFLICT (person_id) DO UPDATE SET
            contact_preference_id = EXCLUDED.contact_preference_id,
            further_details = EXCLUDED.further_details;
-- do mm updates
    PERFORM fn_mm_array_update(NEW.dept_telephone_number_id,
                                'mm_person_dept_telephone_number'::varchar,
                                'person_id'::varchar,
                                'dept_telephone_number_id'::varchar,
                                OLD.id);
    PERFORM fn_mm_array_update(NEW.room_id,
                                'mm_person_room'::varchar,
                                'person_id'::varchar,
                                'room_id'::varchar,
                                OLD.id);
-- generate token if needed
    IF NEW.create_new_token THEN
        UPDATE user_account SET chemnet_token=random_string_lower(16) WHERE person_id = OLD.id;
        IF NOT FOUND THEN
            -- Can only add people with CRSIDs
            SELECT crsid FROM person WHERE person.id=v.id INTO _crsid;
            IF FOUND THEN 
                INSERT INTO user_account (username, person_id, chemnet_token) VALUES (_crsid, OLD.id, random_string_lower(16));
            END IF;
        END IF;
    END IF;
RETURN NEW;
END
$BODY$;

CREATE TRIGGER selfservice_trig
    INSTEAD OF UPDATE 
    ON selfservice."40_Selfservice/Database_Selfservice"
    FOR EACH ROW
    EXECUTE FUNCTION selfservice.selfservice_upd();
