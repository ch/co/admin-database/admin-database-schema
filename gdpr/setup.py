from setuptools import setup

setup(
    name="Chemistry database GDPR tools",
    version="1.3",
    py_modules=["chemistry_gdpr"],
    scripts=["gdpr_purge_database", "audit_table_maintenance"],
    author="Catherine Pitt <cen1001@cam.ac.uk>",
    install_requires=["psycopg2>=2.7"],
)
