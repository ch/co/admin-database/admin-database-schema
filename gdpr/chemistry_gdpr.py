"""
Functions and configuration for doing GDPR purges

Configuration items:

dsn -- the DSN of the database to connect to

gdpr_years -- how many years to keep data for

columns_to_keep -- columns to be kept

tables_to_delete_whole_row_in -- tables where whole rows are purged

records_to_prune_sql -- generates list of people to purge data for

Functions:

delete_whole_rows: delete the many-many (join) table rows

purge_data: set the appropriate columns null for a person in a particular table

get_columns_to_purge: collect the list of columns for each table that we can purge from the database

"""

import psycopg2
import psycopg2.sql as sql
import datetime
from dateutil.relativedelta import relativedelta

# configuration
rolename = "gdpr_purge"
hostname = "dbdev.ch.private.cam.ac.uk"
dbname = "chemistry"
gdpr_years = 6
columns_to_keep = {
    "public": {
        "person": set(
            [
                "first_names",
                "surname",
                "date_of_birth",
                "title_id",
                "name_suffix",
                "previous_surname",  # maiden_name is to be renamed to this so keep both
                "maiden_name",
                "known_as",
                "crsid",
                "hide_email",
                "arrival_date",
                "leaving_date",
                "email_address",
                "left_but_no_leaving_date_given",
                "do_not_show_on_website",
                "managed_mail_domain_optout",
                "chem_at_cam",
                "hide_phone_no_from_website",
                "is_spri",
            ]
        ),
        "post_history": set(
            [
                "paid_by_university",
                "chem",
                "intended_end_date",
                "start_date",
                "end_date",
                "job_title",
                "force_role_status_to_past",
                "supervisor_id",
                "staff_category_id",
                "funding_end_date",
            ]
        ),
        "postgraduate_studentship": set(
            [
                "cpgs_or_mphil_date_submission_due",
                "cpgs_or_mphil_date_submitted",
                "cpgs_or_mphil_date_awarded",
                "cpgs_title",
                "date_registered_for_phd",
                "date_phd_submission_due",
                "end_of_registration_date",
                "phd_thesis_title",
                "phd_date_submitted",
                "phd_date_awarded",
                "gaf_number",
                "postgraduate_studentship_type_id",
                "first_supervisor_id",
                "second_supervisor_id",
                "date_withdrawn_from_register",
                "start_date",
                "date_reinstated_on_register",
                "msc_date_submission_due",
                "msc_date_submitted",
                "msc_date_awarded",
                "mphil_title",
                "msc_title",
                "force_role_status_to_past",
                "mphil_date_submission_due",
                "mphil_date_submitted",
                "mphil_date_awarded",
                "funding_end_date",
                "external_co_supervisor",
                "intended_end_date",
                "end_date",
            ]
        ),
        "erasmus_socrates_studentship": set(
            [
                "start_date",
                "intended_end_date",
                "end_date",
                "supervisor_id",
                "force_role_status_to_past",
            ]
        ),
        "part_iii_studentship": set(
            [
                "supervisor_id",
                "start_date",
                "intended_end_date",
                "end_date",
                "force_role_status_to_past",
            ]
        ),
        "visitorship": set(
            [
                "visitor_type_id",
                "host_person_id",
                "start_date",
                "intended_end_date",
                "end_date",
                "force_role_status_to_past",
            ]
        ),
    }
}

# These can optionally have a key called 'query' which is a query to select the
# rows to delete. It is an sql.SQL() that gets passed the schema, tablename,
# key_column, as sql Identifiers and the person.id being purged as an
# sql.Literal. The results from that are fed to 'delete from {schema}.{table} where {key_column} = %s'
# If no query is supplied then then the default is "select {key_column} from
# {schema}.{table} where {key_column} = {person_id}"
tables_to_delete_whole_row_in = {
    "public": {
        "mm_person_room": {
            "key_column": "person_id",
        },
        "mm_person_dept_telephone_number": {
            "key_column": "person_id",
        },
        "mm_research_group_computer_rep": {
            "key_column": "computer_rep_id",
        },
        "mm_mailinglist_exclude_person": {
            "key_column": "exclude_person_id",
        },
        "mm_mailinglist_include_person": {
            "key_column": "include_person_id",
        },
        "mm_member_research_interest_group": {
            "key_column": "member_id",
        },
        "personal_software_licence_compliance_declaration": {
            "key_column": "person_id",
        },
        "person_photo": {
            "key_column": "person_id",
        },
    },
    "registration": {
        "form": {
            "key_column": "_match_to_person_id",
        },
    },
    "leave_recording": {
        "standing_leave": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.standing_leave l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "leave_audit": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.leave_audit l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "role_entitlement_adjustment": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.role_entitlement_adjustment l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "role_enrol_audit": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.role_enrol_audit l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "role_enrol": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.role_enrol l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "pending_leave_request": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.pending_leave_request l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "mm_role_shared_calendar_group": {
            "query": sql.SQL("SELECT l.id FROM leave_recording.mm_role_shared_calendar_group l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "id"
        },
        "role_decoration": {
            "query": sql.SQL("SELECT l.post_id FROM leave_recording.role_decoration l JOIN post_history p ON l.post_id = p.id WHERE p.person_id = {person_id}"),
            "key_column": "post_id"
        },
    }
}

# dict of schemas, each contains dict of tables, each table has a key column and a
# query to use to generate values from the key column to remove. The query can
# contain variables schema, table, key_column; these will be supplied
# when formatting it
additional_pruning_queries = {
    "registration": {
        "form": {
            "query": sql.SQL(
                "SELECT {key_column} FROM {schema}.{table} WHERE registration.state_of_form(uuid) <> 'Complete' AND _form_started < current_date - interval '3 months'"
            ),
            "key_column": "uuid",
        },
    }
}

gdpr_cutoff = datetime.date.today() - relativedelta(years=gdpr_years)

records_to_prune_query = """
-- People who have definitely gone, who have leaving dates earlier than
-- (placeholder) and don't have problematic arrival dates (ie ones newer than
-- leaving date)
    SELECT
        person.id,
        person.first_names,
        person.surname,
        person.leaving_date
    FROM public.person
    LEFT JOIN public._physical_status_v2 ON _physical_status_v2.person_id = person.id
    WHERE 
            _physical_status_v2.status_id = 'Past' 
        AND 
            person.leaving_date IS NOT NULL 
        AND 
            person.leaving_date < {} 
        AND 
            ( (person.leaving_date > person.arrival_date) OR person.arrival_date IS NULL)
UNION
-- People who have definitely gone, who have leaving dates earlier than (placeholder)
-- but also have an arrival date that suggests a problem, ie same as or newer than leaving date
    SELECT
        person.id,
        person.surname,
        person.first_names,
        person.leaving_date
    FROM public.person
    LEFT JOIN public._physical_status_v2 ON _physical_status_v2.person_id = person.id
    WHERE 
            _physical_status_v2.status_id = 'Past' 
        AND 
            person.leaving_date IS NOT NULL 
        AND 
            person.leaving_date < {}
        AND 
            ( (person.leaving_date <= person.arrival_date) )
UNION
-- People who are Past, have no leaving date, but we can tell when they were set to Past
-- and it is before (placeholder)
    SELECT
        person.id,
        person.first_names,
        person.surname,
        last_status_change.stamp::date as leaving_date
    FROM public.person
    LEFT JOIN public._physical_status_v2 ON _physical_status_v2.person_id = person.id
    LEFT JOIN (
        SELECT
            person_id, max(stamp) as stamp
        FROM public._physical_status_changelog 
        GROUP BY person_id
    ) last_status_change ON person.id = last_status_change.person_id
    WHERE 
            _physical_status_v2.status_id = 'Past' 
        AND 
            person.leaving_date IS NULL 
        AND 
            last_status_change.stamp IS NOT NULL 
        AND 
            last_status_change.stamp::date < {}
UNION
-- People who are Past, have no leaving date, and we can't tell when they were set Past
    SELECT
        person.id,
        person.first_names,
        person.surname,
        null::date as leaving_date
    FROM public.person
    LEFT JOIN public._physical_status_v2 ON _physical_status_v2.person_id = person.id
    LEFT JOIN (
        SELECT
            person_id, max(stamp) as stamp
        FROM _physical_status_changelog 
        GROUP BY person_id
    ) last_status_change ON person.id = last_status_change.person_id
    WHERE 
            _physical_status_v2.status_id = 'Past' 
        AND 
            person.leaving_date IS NULL 
        AND 
            last_status_change.stamp IS NULL 
ORDER BY leaving_date DESC NULLS LAST
"""
records_to_prune_sql = sql.SQL(records_to_prune_query).format(
    *(sql.Literal(gdpr_cutoff) * 3)
)

# functions
def delete_whole_rows_for_person(database, Id, debug=False, reverse_file=None):
    """
    Delete whole rows referring to the person.id, returns number of rows deleted

    Takes tables and definitions of how to find rows to delete from tables_to_delete_whole_row_in
    """
    deleted_row_count = 0
    if debug:
        print("Deleting whole rows for person {}".format(Id))
    for schema in tables_to_delete_whole_row_in.keys():
        for table in tables_to_delete_whole_row_in[schema]:
            if not check_table_exists(database, schema, table):
                if debug:
                    print("No such table {}.{}".format(schema,table))
                continue
            deleted_rows = delete_whole_rows(
                database,
                person_id=Id,
                debug=debug,
                reverse_file=reverse_file,
                schema=schema,
                table=table,
                query=tables_to_delete_whole_row_in[schema][table].get(
                    "query",
                    sql.SQL(
                        "SELECT {key_column} from {schema}.{table} where {key_column} = {person_id}"
                    ),
                ),
                key_column=tables_to_delete_whole_row_in[schema][table]["key_column"],
            )
            deleted_row_count += deleted_rows
    return deleted_row_count


def delete_whole_rows(
    database,
    schema,
    table,
    query,
    key_column,
    person_id=None,
    debug=False,
    reverse_file=None,
):
    """Delete whole rows in tables for a person, returns number of rows deleted

    Arguments:
    database -- psycopg2 database connection
    Id -- value of the foreign key person(id) to search for in the table
    debug -- boolean, write out the generated SQL and number of deleted rows
    reverse_file -- if set, a filename to write SQL that reinserts the deleted rows
    """
    cursor = database.cursor()
    deleted_row_count = 0
    if query is None:
        query = sql.SQL(
            "SELECT {key_column} from {schema}.{table} where {key_column} = {person_id}"
        )
    records_to_delete_sql = query.format(
        schema=sql.Identifier(schema),
        table=sql.Identifier(table),
        key_column=sql.Identifier(key_column),
        person_id=sql.Literal(person_id),
    )
    if debug:
        print(cursor.mogrify(records_to_delete_sql).decode("utf-8"))
    cursor.execute(records_to_delete_sql)
    for row in cursor:
        inner_cursor = database.cursor()
        if reverse_file:
            with open(reverse_file, "a") as f:
                select_sql = sql.SQL(
                    "SELECT * FROM {schema}.{table} where {key_column} = %s"
                ).format(
                    schema=sql.Identifier(schema),
                    table=sql.Identifier(table),
                    key_column=sql.Identifier(key_column),
                )
                if debug:
                    print(inner_cursor.mogrify(select_sql, [row[0]]).decode("utf-8"))
                inner_cursor.execute(select_sql, [row[0]])
                for row_to_be_deleted in inner_cursor:
                    reverse_sql = sql.SQL(
                        "INSERT INTO {schema}.{table} ({column_list}) VALUES ({value_list}) ;"
                    ).format(
                        schema=sql.Identifier(schema),
                        table=sql.Identifier(table),
                        column_list=sql.SQL(", ").join(
                            sql.Composed(
                                [sql.Identifier(x[0]) for x in inner_cursor.description]
                            )
                        ),
                        value_list=sql.SQL(", ").join(
                            sql.Composed([sql.Placeholder() for x in row_to_be_deleted])
                        ),
                    )
                    f.write(
                        "-- {}.{} {} {}\n".format(schema, table, key_column, row[0])
                    )
                    try:
                        f.write(
                            cursor.mogrify(reverse_sql, row_to_be_deleted).decode(
                                "utf-8"
                            )
                        )
                    except psycopg2.ProgrammingError:
                        f.write("-- Error: cannot reverse this row")
                    f.write("\n")
        prune_sql = sql.SQL(
            """DELETE FROM {schema}.{table} WHERE {key_column} = %s"""
        ).format(
            schema=sql.Identifier(schema),
            table=sql.Identifier(table),
            key_column=sql.Identifier(key_column),
        )
        if debug:
            print(inner_cursor.mogrify(prune_sql, [row[0]]).decode("utf-8"))
        inner_cursor.execute(prune_sql, [row[0]])
        deleted_row_count += inner_cursor.rowcount
    if debug:
        print("Updated {} rows\n".format(deleted_row_count))
    return deleted_row_count


def purge_data(
    database,
    schema,
    tablename,
    keyname,
    keyvalue,
    columns,
    debug=False,
    reverse_file=None,
    primary_key_name="id",
):
    """
    Purge columns in data rows where keyname has keyvalue, return number of rows changed

    By 'purge' in this case we mean set appropriate columns to null.

    Arguments:
    database -- a psycopg2 database connection
    schema -- the schema to look in
    tablename -- the table to purge
    keyname -- the name of the column to search for records to purge
    keyvalue -- records with keyname set to this value are purged
    columns -- list of columns to be set null
    debug -- boolean, print out the SQL being sent and the number of affected rows
    reverse_file -- if set, a filename to write out SQL that reverses the purge
    primary_key_name -- the primary key of the table, for writing reverse files
    """
    # Don't purge unless there is something to be purged, otherwise we fill up the audit table
    # with no-op updates
    prune_sql = sql.SQL("UPDATE {}.{} SET {} WHERE {} = %s AND ({})").format(
        sql.Identifier(schema),
        sql.Identifier(tablename),
        sql.SQL(", ").join(
            sql.Composed(
                [sql.SQL("{} = NULL").format(sql.Identifier(x)) for x in columns]
            )
        ),
        sql.Identifier(keyname),
        sql.SQL(" OR ").join(
            sql.Composed(
                [
                    sql.SQL("( {} IS NOT NULL )").format(sql.Identifier(x))
                    for x in columns
                ]
            )
        ),
    )
    cursor = database.cursor()
    if reverse_file:
        with open(reverse_file, "a") as f:
            get_values_sql = sql.SQL(
                "SELECT {}, {} FROM {}.{} WHERE {} = %s AND ({})"
            ).format(
                sql.SQL(", ").join(sql.Composed([sql.Identifier(x) for x in columns])),
                sql.Identifier(primary_key_name),
                sql.Identifier(schema),
                sql.Identifier(tablename),
                sql.Identifier(keyname),
                sql.SQL(" OR ").join(
                    sql.Composed(
                        [
                            sql.SQL("( {} IS NOT NULL )").format(sql.Identifier(x))
                            for x in columns
                        ]
                    )
                ),
            )
            cursor.execute(get_values_sql, [keyvalue])
            for row in cursor:
                reverse_sql = sql.SQL("UPDATE {}.{} SET {} WHERE {} = %s ;").format(
                    sql.Identifier(schema),
                    sql.Identifier(tablename),
                    sql.SQL(", ").join(
                        sql.Composed(
                            [
                                sql.SQL("{} = {}").format(
                                    sql.Identifier(x), sql.Placeholder()
                                )
                                for x in columns
                            ]
                        )
                    ),
                    sql.Identifier(primary_key_name),
                )
                f.write(
                    "\n-- {}.{} {} {} {} {}\n".format(
                        schema,
                        tablename,
                        keyname,
                        keyvalue,
                        primary_key_name,
                        row[primary_key_name],
                    )
                )
                f.write(cursor.mogrify(reverse_sql, row).decode("utf-8"))
                f.write("\n")
    if debug:
        print(cursor.mogrify(prune_sql, [keyvalue]).decode("utf-8"))
    cursor.execute(prune_sql, [keyvalue])
    if debug:
        print("Updated {} rows\n".format(cursor.rowcount))
    return cursor.rowcount


def get_columns_to_purge(database):
    "Collect the list of purgable columns for each table that we should be purging, return a dict"
    cursor = database.cursor()
    columns_to_purge = {}
    query = sql.SQL(
        """
SELECT column_name
FROM information_schema.columns
WHERE table_name = %s AND table_schema = %s AND is_nullable = 'YES'
"""
    )
    for schema in columns_to_keep:
        columns_to_purge[schema] = {}
        for table in columns_to_keep[schema]:
            cursor.execute(query, [table, schema])
            all_nullable_columns = set([x[0] for x in cursor.fetchall()])
            columns_to_purge[schema][table] = (
                all_nullable_columns - columns_to_keep[schema][table]
            )
        return columns_to_purge


def check_table_exists(database, schema, table):
    "Return true or false as to whether the schema.table exists"
    cursor = database.cursor()
    query = sql.SQL(
        "SELECT (COUNT(*)=1) as exists FROM information_schema.tables where table_schema = %s and table_name = %s"
    )
    cursor.execute(query, [schema, table])
    return cursor.fetchone()[0]


def run_additional_pruning(database, debug=False, reverse_file=None):
    """
    Run additional pruning queries, for data that has no person(id) foreign key to look up

    An example is incomplete registration forms
    """
    deleted_row_count = 0
    for schema in additional_pruning_queries:
        for table in additional_pruning_queries[schema]:
            deleted_row_count += delete_whole_rows(
                database,
                schema=schema,
                table=table,
                query=additional_pruning_queries[schema][table]["query"],
                key_column=additional_pruning_queries[schema][table]["key_column"],
                debug=debug,
                reverse_file=reverse_file,
            )
    return deleted_row_count
